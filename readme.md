
# Avant projet de microsatellite d'observation

## Ressource list:

### Liste des softwares à installer : 

* [Software - Satorb -  v7.0](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/softs/Satorb/Satorb_V07_setup.exe?inline=false)

* [Software - Simusat - V6.4](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/raw/master/setup/SetupSimusat/Release/SetupSimusat.msi?inline=false)

* [VTS - 3.7.0 "Night"](https://timeloop.fr/vts/download/)

### Documents:

* [BE : Space Systems Introduction](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/docs/BE_Minisat_ENSAM.pdf?inline=false)

* [Support slides: Space Systems Introduction (EN)](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/docs/BE_Minisat_anglais.pdf?inline=false)

* [Cours, part I](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/docs/cours_ENSAM_partI.pdf?inline=false)

* [Cours, part II](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/docs/cours_ENSAM_partII.pdf?inline=false)

* [Aide pour la partie Radiocommunications](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/docs/telecom_puissance.pdf?inline=false)

* [Aide pour la partie Sous-Système Energie](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/docs/Power_aide.pdf?inline=false)



### Other version, other softwares

* [Software - Stela - v3.1.1](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/softs/stela-install-3.1.1/stela-install-3.1.1.jar?inline=false)

* [Python - Anaconda distribution](https://www.anaconda.com/)

* [Software - Simusat (old version,  power simulation) - VB6 - V03](https://gitlab.isae-supaero.fr/saclab/ensam/-/raw/master/softs/oldVersions/Simusat_Vb6_V03_setup.exe?inline=false)

* [Software - Simusat - Stand-alone v6.4.3](https://gitlab.isae-supaero.fr/gt-dcas/simusat-group/simusat-dev/-/packages/25)


<!-- !https://sourceforge.isae.fr/attachments/download/2699/satorb04.jpg! -->

[<img src="./docs/img/satorb04.jpg"/>](./docs/img/satorb04.jpg)


# Repository Tree Organisation:

```
.
├── docs
├── REAMDE.md
├── softs
└── supportLight
```

# Get all GIT repository

```
git clone git@gitlab.isae-supaero.fr:saclab/ensam.git 

```



