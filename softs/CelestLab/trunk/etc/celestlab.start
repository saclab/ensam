//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// function defined to hide variables
// cmd = [] if error found
// ----------------------------------
function [cmd, root_tlbx] = $CL_genLoadCommand()

  cmd = [];
  root_tlbx = [];
  
  vsc = getversion('scilab'); 
  v = vsc(1)*10 + vsc(2);
  
  // Compatible Scilab versions  5.3 <= v < 6.0
  if ~(v < 60 & v >= 53) 
    warning("*** CelestLab not available for this version of Scilab");
    return;
  end

  root_tlbx = dirname(get_absolute_file_path("celestlab.start"));

  // Build command to load functions library
  // ---------------------------------------
  pathmacros = fullfile(root_tlbx, "macros");
   
  // Libraries in sub-directories of directory "macros" 
  // that contain ".sci" files and do not start with a "."
  // are loaded in a variable "CLlib + name of the directory"
  // (shortened if too long)
  cmd = "";
  for d = listfiles(pathmacros)'
    if (isempty(strindex(d,'.'))  &  length(findfiles( fullfile(pathmacros,d), "*.sci")) <> 0)
      libname = stripblanks(part("CLlib" + d, 1:18));
      libpath = fullfile(pathmacros,d);
      cmd = cmd + libname + "=lib(''" + libpath + "'');"; 
    end
  end

endfunction


function $CL_initCelestlab(root_tlbx)

  // Load and add help chapter
  // -------------------------
  if (getscilabmode() == "NW" | getscilabmode() == "STD")
    path_addchapter = fullfile(root_tlbx, "jar");
    if (isdir(path_addchapter) <> [])
      add_help_chapter("CelestLab", path_addchapter, %f);
    end
  end

  // Load CelestLab configuration (config.txt)
  // --> Loaded in global variable %CL__PRIV.PREF
  // ------------------------------------
  CL__configInit();
  
  // Load CelestLab data (*.scd files in HOME/data)
  // --> Loaded in global variable %CL__PRIV.DATA
  // ---------------------------------------------------------
  CL__dataInit();

  // CelestLab Menu (only in standard mode)
  // --------------------------------------
  if (getscilabmode() == "STD" & CL_configGet("DISPLAY_MENU") == "yes")
    CL__menu(); 
  end

endfunction 

[$CL_cmd, $CL_root_tlbx] = $CL_genLoadCommand();

if ($CL_cmd <> [])
  $CL_err = execstr($CL_cmd, "errcatch");

  if ($CL_err <> 0)
    warning("Unable to load CelestLab");
  else
    $CL_initCelestlab($CL_root_tlbx);

    // CelestLab banner (initilize version)
    try
      CL__banner();
    end

  end

end

clear $CL_initCelestlab $CL_genLoadCommand $CL_err $CL_cmd $CL_root_tlbx;
 
// Patch for Scilab 5.5.2 (supposed to work)
// => see http://bugzilla.scilab.org/show_bug.cgi?id=13807  
if (getscilabmode() == "STD")
  a = gda(); 
  a.margins = 0.125 * ones(1,4); 
end



 