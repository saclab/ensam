// ------------------------------
// CL_co_car2sph
// validation:
// CNES - MSLIB FORTRAN 90, Volume T (mt_car_geoc)
// ------------------------------

// Inputs
inputs = struct();
inputs.pos_car = [3842403.1,-5057704.6,577780.5]';
inputs.vit_car = [1000;1000;100];

// Outputs
outputs = struct();
outputs.pos_geoc = [5.3620773780852389;0.090714657345132829;6377951.6860768879];
outputs.vel_geoc = [0.00022060309357970051;1.8332191147172865e-005;-181.48827507221191];
outputs.jacob = [[1.2536312129933909e-007;-8.5923413577160952e-009;0.60245095747401045;-1.7233900533108496e-011;-4.4711468559701072e-012;..
0.00017393323745447189],[9.5239972280361436e-008;1.1309985776789253e-008;-0.7929982616583634;3.052442025700607e-011;..
7.0565550142570826e-013;0.00013422492917674334],[0;1.5614546728099707e-007;0.090590291121411093;0;4.182824564584716e-012;..
1.8256813692726157e-005],[0;0;0;1.2536312129933909e-007;-8.5923413577160952e-009;0.60245095747401045],[0;0;0;9.5239972280361436e-008;..
1.1309985776789253e-008;-0.7929982616583634],[0;0;0;0;1.5614546728099707e-007;0.090590291121411093]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_geoc,vel_geoc,jacob] = CL_co_car2sph(inputs.pos_car,inputs.vit_car);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_geoc, outputs.pos_geoc, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(vel_geoc, outputs.vel_geoc, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "element", prec = 1e-014);
