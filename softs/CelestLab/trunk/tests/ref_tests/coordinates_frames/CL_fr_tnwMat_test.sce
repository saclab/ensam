// ------------------------------
// CL_fr_tnwMat
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
pos_car = [1,0,2]';
vit_car = [2,-1,1]';
inputs.pos_car = [pos_car pos_car]; //to test vectorization
inputs.vit_car = [vit_car vit_car]; //to test vectorization

// Outputs
outputs = struct();
outputs.M = hypermat([3 3 2 ] , [0.81649658092772615,0.21821789023599245,0.53452248382484879,-0.40824829046386307,-0.43643578047198484,..
0.80178372573727319,0.40824829046386307,-0.87287156094396967,-0.2672612419124244,0.81649658092772615,0.21821789023599245,..
0.53452248382484879,-0.40824829046386307,-0.43643578047198484,0.80178372573727319,0.40824829046386307,-0.87287156094396967,..
-0.2672612419124244]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[M] = CL_fr_tnwMat(inputs.pos_car,inputs.vit_car);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M, outputs.M, relative = %t, compar_crit = "element", prec = 1e-014);
