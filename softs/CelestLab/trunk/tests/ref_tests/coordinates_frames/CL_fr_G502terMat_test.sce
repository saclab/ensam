// ------------------------------
// CL_fr_G502terMat
// validation:
// --
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = [20708.001 20708.1];

// Outputs
outputs = struct();
outputs.M = hypermat([3 3 2 ] , [0.98627346428951024,0.16512011881769059,0,-0.16512011881769059,0.98627346428951024,0,0,0,1,..
0.89700113025660477,-0.44202824832625082,0,0.44202824832625082,0.89700113025660477,0,0,0,1]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[M] = CL_fr_G502terMat(inputs.cjd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M, outputs.M, relative = %t, compar_crit = "element", prec = 1e-014);
