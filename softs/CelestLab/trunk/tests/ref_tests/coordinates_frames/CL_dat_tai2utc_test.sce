// ------------------------------
// CL_dat_tai2utc
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
TAI_UTC_DATA = CL_dataGet("TAI_UTC_DATA"); 

jj = TAI_UTC_DATA($-5:$, 1)'; // UTC
nsec = TAI_UTC_DATA($-5:$, 3)'; // TAI - UTC (from jj) 

inputs.cjdtai = [jj + nsec/86400.0 - 0.1, jj + nsec/86400.0 + 0.1]

// Outputs
outputs = struct();
outputs.cjdutc = inputs.cjdtai - [nsec - 1, nsec] / 86400; 

TEST_OK = [];

//=========================
// Compute output
//=========================
[cjdutc] = CL_dat_tai2utc(inputs.cjdtai);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(cjdutc, outputs.cjdutc, relative = %t, compar_crit = "element", prec = 1.e-014);
