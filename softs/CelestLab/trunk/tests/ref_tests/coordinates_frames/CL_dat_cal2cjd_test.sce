// ------------------------------
// CL_dat_cal2cjd
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.year = [1968 1980 1986];
inputs.month = [2 4 2];
inputs.day = [29 5 28];
inputs.hour = [0 12 4];
inputs.minute = [0 30 20];
inputs.second = [0 5 12];

// Outputs
outputs = struct();
outputs.cjd = [6633,11052.520891203703,13207.180694444445];

TEST_OK = [];

//=========================
// Compute output
//=========================
[cjd] = CL_dat_cal2cjd(inputs.year,inputs.month,inputs.day,inputs.hour,inputs.minute,inputs.second);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(cjd, outputs.cjd, relative = %t, compar_crit = "element", prec = 1e-014);
