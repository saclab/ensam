// -------------------------------------------------------
// CL_fr_bodyConvert
// Jupiter: ICRS --> BCF
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Jupiter";
inputs.frame1 = "ICRS";
inputs.frame2 = "BCF";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [409358.13;-2583136.7;5400993.5];
outputs.vel2 = [1645.0629;-5017.8289;4139.0241];
outputs.jacob = [-0.9745505,0.2080647,0.0834285,0.,0.,0.;
-0.2236915,-0.8783724,-0.4224027,0.,0.,0.;
-0.0146058,-0.4303151,0.9025606,0.,0.,0.;
-0.0000393,-0.0001545,-0.0000743,-0.9745505,0.2080647,0.0834285;
0.0001714,-0.0000366,-0.0000147,-0.2236915,-0.8783724,-0.4224027;
1.566D-14,6.932D-14,3.330D-14,-0.0146058,-0.4303151,0.9025606];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
