// ------------------------------
// CL_fr_topoNMat
// validation:
// CNES - MSLIB FORTRAN 90, Volume T (mt_def_topo_N)
// ------------------------------

// Inputs
inputs = struct();
lat = [%pi/2 %pi];
lon = [%pi %pi/2];
alt = [0 0];
inputs.orig = [lat;lon;alt];

// Outputs
outputs = struct();
outputs.M = hypermat([3 3 2 ] , [-7.498798913309288e-033,1,-6.123233995736766e-017,-1.2246467991473532e-016,-6.123233995736766e-017,..
-1,-1,0,1.2246467991473532e-016,1,1.2246467991473532e-016,-6.123233995736766e-017,-1.2246467991473532e-016,1,..
7.498798913309288e-033,6.123233995736766e-017,0,1]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[M] = CL_fr_topoNMat(inputs.orig);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M, outputs.M, relative = %t, compar_crit = "element", prec = 1e-014);
