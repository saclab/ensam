// -------------------------------------------------------
// CL_fr_bodyConvert
// Mars: ICRS --> BCI
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Mars";
inputs.frame1 = "ICRS";
inputs.frame2 = "BCI";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); //TREF
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [89517.649;3578047.9;4816685.6];
outputs.vel2 = [1544.471;5821.2172;3118.9806];
outputs.jacob = [0.6733904,0.7392871,0.,0.,0.,0.;
-0.5894906,0.5369460,0.6034814,0.,0.,0.;
0.4461460,-0.4063786,0.7973771,0.,0.,0.;
4.338D-13,-3.951D-13,-2.524D-29,0.6733904,0.7392871,0.;
4.653D-13,2.090D-13,2.686D-13,-0.5894906,0.5369460,0.6034814;
-3.991D-14,-4.426D-13,-2.033D-13,0.4461460,-0.4063786,0.7973771];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
