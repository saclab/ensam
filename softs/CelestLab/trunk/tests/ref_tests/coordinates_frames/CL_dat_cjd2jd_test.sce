// ------------------------------
// CL_dat_cjd2jd
// validation:
// Manual calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = [18262.5 18000];

// Outputs
outputs = struct();
outputs.jd = [2451545,2451282.5];

TEST_OK = [];

//=========================
// Compute output
//=========================
[jd] = CL_dat_cjd2jd(inputs.cjd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(jd, outputs.jd, relative = %t, compar_crit = "element", prec = 1e-014);
