// =============================================================
// Test CL_fr_bodyConvertMat (all supported body)
//  - Uses reference data (more ifno below)
//  - Tests rotation angle and angular velocity (ICRS => BCF)
//
// CelestLab settings:
//  - test dates: [2010;01;01;00;00;00] [2090;01;01;00;00;00]
//  - timescale: TT (tt_tref == 0)
//  - relative precisions (driven by reference results):
//      angles => 1.e-7
//      angular velocity => 1.e-8
// 
// Note: Jupiter, Saturn => reference angular velocity is less precise (5 significant digits) test precision is modified accordingly 
// =============================================================
//
// =============================================================
// Reference data obtained via SPICE tool - Version 1.1.0 (2916)
// http://wgc.jpl.nasa.gov:8080/webgeocalc/#FrameTransformation
//
// SPICE setting are used in order to generate data
// ---------------------------------------------
// Kernel selection : Solar System Kernel
// Frame 1: EME
// Frame 2: IAU_body_name
// ---------------------------------------------
// Input time
//   Time System: TDB
//   Time Format: Calendar Date and Time
// ---------------------------------------------
// Light time adjustment
//   Light propagation: none
//   Light-time algorithm: -
// ---------------------------------------------
// Orientation Representation
//   Output form: Quaternion, SPICE-style
// ---------------------------------------------
// Angular Velocity Representation
//   Output form: Vector in Frame 1
//   Angular velocity units: radians/second
// ---------------------------------------------
// Error handling: Stop on error
// =============================================================


TEST_OK = [];

// ---------------------------------
// Auxiliary functions
// ---------------------------------

// ---------------------------------
// Reference data for selected body (quaternion and angular velocity) and relative precisions
//
// q: (4x2) rotation quaternion (CL convention)
// omega (3x2) : reference angular velocity (for the two test dates)
// RELTOL_ANG: (1x1) relative precision on angles
// RELTOL_OMEGA: (1x1) relative precision on angular velocities
// ---------------------------------
function [q, omega, RELTOL_ANG, RELTOL_OMEGA] = reference_data(body)

  RELTOL_ANG = 1.e-7;
  RELTOL_OMEGA = 1.e-8;
  
  if (body == "Mercury")
  
    Q = [[0.73447932;-0.21442713;0.12235947;-0.63213075], [0.09743725;-0.07116445;0.23643778;-0.96412591]];
    omega = [[1.13286136E-07;-5.82458054E-07;1.08894672E-06], [1.13032158E-07;-5.82575767E-07;1.08889258E-06]];
    
  elseif (body == "Venus")
  
    Q = [[0.23497000;-0.05666474;0.18971798;-0.95162245], [0.81895888;-0.17047563;0.10070683;-0.53861168]];
    omega = [[-5.59313160E-09;1.16019900E-07;-2.75781861E-07], [-5.59313160E-09;1.16019900E-07;-2.75781861E-07]];
    
  elseif (body == "Mars")
  
    Q = [[0.59185964;0.04999961;-0.31434252;0.74053426], [0.15723453;-0.26768883;0.17294679;-0.93472424]];
    omega = [[3.16238124E-05;-2.88049090E-05;5.65198653E-05], [3.16165761E-05;-2.88841314E-05;5.64834714E-05]];
    
  elseif (body == "Jupiter")
  
    Q = [[0.92826539;-0.20765421;0.07482971;-0.29933862], [0.31482564;-0.07830513;-0.20637369;0.92312676]];
    omega = [[-2.56847857E-06;-7.56723319E-05;0.00015872], [-2.57232137E-06;-7.56737044E-05;0.00015872]];
    RELTOL_OMEGA = 1.e-4; // reference omega => less precise (5 significant figures)
    
  elseif (body == "Saturn")
  
    Q = [[0.45492840;0.05482124;0.01313960;0.88874188], [0.95969970;0.02343420;-0.05130302;-0.27530950]];
    omega = [[1.40017661E-05;1.19947774E-05;0.00016274], [1.40146993E-05;1.19936477E-05;0.00016274]];
    RELTOL_OMEGA = 1.e-4; // reference omega => less precise (5 significant figures)
    
  elseif (body == "Uranus")
  
    Q = [[0.53614017;-0.76587414;-0.21052517;0.28577906], [0.27596121;-0.19653324;0.76958348;-0.54125906]];
    omega = [[2.14622431E-05;9.53208086E-05;2.65006669E-05], [2.14622431E-05;9.53208086E-05;2.65006669E-05]];
    
  elseif (body == "Neptune")
  
    Q = [[0.84637776;-0.39635126;-0.04712779;-0.35260366], [0.82041083;-0.22000003;-0.33161681;0.41055615]];
    omega = [[3.89243764E-05;-6.90863195E-05;7.38183608E-05], [3.93787105E-05;-6.86079566E-05;7.40232562E-05]];
    
  elseif (body == "Sun")
  
    Q = [[0.80362511;-0.21464404;0.07091816;-0.55053177], [0.02554183;-0.06847483;0.21543595;-0.97377932]];
    omega = [[3.50583094E-07;-1.21224099E-06;2.57248466E-06], [3.50583094E-07;-1.21224099E-06;2.57248466E-06]];
    
  elseif (body == "Moon")
  
    Q = [[0.77888192;-0.14987522;-0.13025054;0.59490770], [0.63950807;-0.14219933;0.16308706;-0.73770684]];
    omega = [[6.69618778E-08;-1.03397519E-06;2.45165552E-06], [3.77373180E-09;-1.12366085E-06;2.41312989E-06]];
    
  end
  
  // transpose because reference quaternion expresses the inverse rotation
  q = CL_rot_defQuat(Q)'; 

endfunction

// ---------------------------------
// ang: (1x2) Angle associated to the (composed) rotation q * q' (=> should be 0)
// omega: (3x2) Angular velocity
// ---------------------------------
function [ang, omega] = test(cjd, qref, body)
  
  [M,omega] = CL_fr_bodyConvertMat(body, "ICRS", "BCF", cjd, tt_tref=0);
  q = CL_rot_matrix2quat(M); 
  [ax,ang] = CL_rot_quat2axAng(qref * q');
  
endfunction


// =====================================
// Common entries
// =====================================

cjd = CL_dat_cal2cjd([[2010;01;01;00;00;00], [2090;01;01;00;00;00]]);

bodynames = [ "Mercury", "Venus", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Sun", "Moon"];

for ibody = 1 : size(bodynames, 2)

  [qref, omega_ref,  RELTOL_ANG, RELTOL_OMEGA] = reference_data(bodynames(ibody));
  [ang, omega] = test(cjd, qref, bodynames(ibody));
  
  TEST_OK($+1) = CL__isEqual(ang, zeros(ang), prec = RELTOL_ANG);
  TEST_OK($+1) = CL__isEqual(omega, omega_ref, compar_crit = "norm", prec = RELTOL_OMEGA); 
  
end
