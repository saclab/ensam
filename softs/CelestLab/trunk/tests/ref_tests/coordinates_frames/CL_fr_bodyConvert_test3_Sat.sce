// -------------------------------------------------------
// CL_fr_bodyConvert
// Saturn: BCI --> ICRS
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Saturn";
inputs.frame1 = "BCI";
inputs.frame2 = "ICRS";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [425236.4;495959.75;5965240.8];
outputs.vel2 = [-1100.3184;-2259.3636;6299.5695];
outputs.jacob = [-0.6505803,-0.7546105,0.0854888,0.,0.,0.;
0.7594375,-0.6464452,0.0732349,0.,0.,0.;
0.,0.1125686,0.9936440,0.,0.,0.;
1.512D-13,-1.268D-13,3.128D-14,-0.6505803,-0.7546105,0.0854888;
1.295D-13,1.519D-13,-2.720D-15,0.7594375,-0.6464452,0.0732349;
-3.155D-30,2.198D-14,-2.490D-15,0.,0.1125686,0.9936440];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
