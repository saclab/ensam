// ------------------------------
// CL_fr_inertial2tnw
// validation:
// CNES - MSLIB FORTRAN 90, Volume O (mo_geo_tnw)
// ------------------------------

// Inputs
inputs = struct();
pos_car = [1,1,1]';
vit_car = [1,1,0]';
vect_geo = [1,1,0]';
//to test vectorization:
inputs.pos_car = [pos_car pos_car];
inputs.vit_car = [vit_car vit_car];
inputs.vect_geo = [vect_geo vect_geo];

// Outputs
outputs = struct();
outputs.vect_tnw = [[1.4142135623730949;0;0],[1.4142135623730949;0;0]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[vect_tnw] = CL_fr_inertial2tnw(inputs.pos_car,inputs.vit_car,inputs.vect_geo);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(vect_tnw, outputs.vect_tnw, relative = %t, compar_crit = "element", prec = 1e-014);
