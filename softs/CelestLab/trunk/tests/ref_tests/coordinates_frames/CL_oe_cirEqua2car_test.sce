// ------------------------------
// CL_oe_cirEqua2car
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.cir_equa = [42166.712;-7.9d-6;1.1d-4;1.2d-4;-1.16d-4;5.3] ;

// Outputs
outputs = struct();
outputs.pos_car = [23374.566867873345;-35099.891435266938;-1.5005372312333365];
outputs.vel_car = [80913.500943831401;53890.224898679662;15.852793262924282];
outputs.jacob = [[0.55433695821180806;-0.83240759761555372;-3.5585824933026232e-005;-0.95944759629149423;-0.63901383748725382;..
-0.00018797758363189762],[-71375.534539477245;-19451.443582483687;-10.613735347348483;89696.106705573038;-37465.16522856737;..
5.9089286121430558],[-19454.884464585612;-55122.577939505609;-8.8714760433040425;-37479.153021238213;-89698.749988403317;..
-15.11143190692605],[2.0357937084673523;2.8562621097470537;-35099.891358065237;-3.1256330992911407;-11.15981021005269;..
53890.225186617696],[0.60545625505974376;1.4024740068505315;-23374.56674296753;12.619379767132873;4.8548101117976055;..
-80913.501121821595],[35091.886124073499;23371.991241090276;6.8752978111427199;-53880.655564003056;80908.671867492129;..
3.4588846148062102]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_car,vel_car,jacob] = CL_oe_cirEqua2car(inputs.cir_equa);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_car, outputs.pos_car, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(vel_car, outputs.vel_car, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "element", prec = 1e-014);
