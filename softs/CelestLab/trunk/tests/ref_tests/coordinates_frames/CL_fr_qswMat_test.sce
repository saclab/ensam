// ------------------------------
// CL_fr_qswMat
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
pos_car = [1,0,2]';
vit_car = [2,-1,1]';
inputs.pos_car = [pos_car pos_car]; //to test vectorization
inputs.vit_car = [vit_car vit_car]; //to test vectorization

// Outputs
outputs = struct();
outputs.M = hypermat([3 3 2 ] , [0.44721359549995793,0.71713716560063623,0.53452248382484879,0,-0.59761430466719678,0.80178372573727319,..
0.89442719099991586,-0.35856858280031811,-0.2672612419124244,0.44721359549995793,0.71713716560063623,0.53452248382484879,..
0,-0.59761430466719678,0.80178372573727319,0.89442719099991586,-0.35856858280031811,-0.2672612419124244]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[M] = CL_fr_qswMat(inputs.pos_car,inputs.vit_car);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M, outputs.M, relative = %t, compar_crit = "element", prec = 1e-014);
