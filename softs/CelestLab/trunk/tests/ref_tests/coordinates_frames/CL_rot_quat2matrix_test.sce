// ------------------------------
// CL_rot_quat2matrix
// validation:
// Luca Cerri (DCT/SB/OR)
// ------------------------------

// Inputs
inputs = struct();
inputs.q = [];
inputs.q = CL_rot_defQuat([1 2 3 4;5 6 7 8]')

// Outputs
outputs = struct();
outputs.R = hypermat([3 3 2 ] , [-20,4,22,20,-10,20,10,28,4,-52,4,166,164,-26,52,26,172,4]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[R] = CL_rot_quat2matrix(inputs.q);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(R, outputs.R, relative = %t, compar_crit = "element", prec = 1e-014);
