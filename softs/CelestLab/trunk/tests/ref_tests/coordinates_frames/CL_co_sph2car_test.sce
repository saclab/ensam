// ------------------------------
// CL_co_sph2car
// validation:
// CNES - MSLIB FORTRAN 90, Volume T (mt_geoc_car)
// ------------------------------

// Inputs
inputs = struct();
inputs.pos_geoc = [5.362077;0.090715;6377951.7];
inputs.vel_geoc = [2.2060e-4;1.8332e-5;-181.488];

// Outputs
outputs = struct();
outputs.pos_car = [3842401.0763788284;-5057705.9061507573;577782.67771148216];
outputs.vel_car = [999.98491111260864;999.98743180018107;99.99874549697077];
outputs.jacob = [[5057705.9061507573;3842401.0763788284;0;-999.98743180018107;999.98491111260864;0],[-349522.70543880708;460072.49542456603;..
6351726.9356348626;-161.98503496993081;2.5215488630074105;-191.33367246196931],[0.60245063887498995;-0.79299846471881519;..
0.090590632367360535;0.00017393083623708734;0.00013422298673652431;1.8256622762454957e-005],[0;0;0;5057705.9061507573;..
3842401.0763788284;0],[0;0;0;-349522.70543880708;460072.49542456603;6351726.9356348626],[0;0;0;0.60245063887498995;..
-0.79299846471881519;0.090590632367360535]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_car,vel_car,jacob] = CL_co_sph2car(inputs.pos_geoc,inputs.vel_geoc);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_car, outputs.pos_car, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(vel_car, outputs.vel_car, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "element", prec = 1e-014);
