// -------------------------------------------------------
// CL_fr_bodyConvert
// Moon: BCF --> ICRS
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Moon";
inputs.frame1 = "BCF";
inputs.frame2 = "ICRS";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [249064.45;-2304233.1;5535284.7];
outputs.vel2 = [-998.39598;364.66883;6698.6522];
outputs.jacob = [0.9984229,-0.0502049,0.0251214,0.,0.,0.;
0.0560278,0.9192915,-0.3895691,0.,0.,0.;
-0.0035356,0.3903622,0.9206546,0.,0.,0.;
-0.0000001,-0.0000027,1.068D-09,0.9984229,-0.0502049,0.0251214;
0.0000024,-0.0000001,-5.710D-10,0.0560278,0.9192915,-0.3895691;
0.0000010,1.004D-08,-2.708D-10,-0.0035356,0.3903622,0.9206546];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
