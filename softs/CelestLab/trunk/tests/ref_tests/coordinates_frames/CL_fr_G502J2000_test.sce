// ------------------------------
// CL_fr_G502J2000
// validation:
// Validation with PSIMU
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = [21010 , 21011];
inputs.pos_G50 = [ [3952930.5;3127929.25;4128420.75] , [3945680.5;3125978.25;5687420.75]];

// Outputs
outputs = struct();
outputs.pos_J2000 = [[3920776.3185157105;3172091.4104432459;4125385.7957559484],[3914710.8893263815;3170118.3286939985;5684388.9601391377]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_J2000] = CL_fr_G502J2000(inputs.cjd,inputs.pos_G50);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_J2000, outputs.pos_J2000, relative = %t, compar_crit = "element", prec = 1e-014);
