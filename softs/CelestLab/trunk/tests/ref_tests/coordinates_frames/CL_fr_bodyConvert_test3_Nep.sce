// -------------------------------------------------------
// CL_fr_bodyConvert
// Neptune: BCI --> ICRS
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Neptune";
inputs.frame1 = "BCI";
inputs.frame2 = "ICRS";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [2232840.6;-3759225.8;4110175.7];
outputs.vel2 = [281.11756;-2536.1117;6284.0361];
outputs.jacob = [0.8712291,-0.3344684,0.3592920,0.,0.,0.;
0.4908766,0.5936290,-0.6376870,0.,0.,0.;
0.,0.7319395,0.6813696,0.,0.,0.;
-1.733D-12,-2.146D-12,2.204D-12,0.8712291,-0.3344684,0.3592920;
3.075D-12,-1.091D-12,1.352D-12,0.4908766,0.5936290,-0.6376870;
0.,-9.579D-14,1.029D-13,0.,0.7319395,0.6813696];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
