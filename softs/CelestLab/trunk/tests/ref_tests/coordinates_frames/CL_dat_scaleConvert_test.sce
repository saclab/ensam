// ------------------------------
// CL_dat_scaleConvert
// validation: at cjd UTC = [15887, 16252], the tai-tuc values should be [28, 29]s
// Manual calculation
// ------------------------------

RELATIVE_PRECISION = 1.e-14

// Inputs
inputs = struct();
inputs.ts_in = "UTC";
inputs.ts_out = "TAI";
inputs.cjd = [16251.9, 16252.1];


// Outputs
outputs = struct();
outputs.cjd = inputs.cjd + [28, 29] / 86400;


TEST_OK = [];

//=========================
// Compute output
//=========================
[cjd] = CL_dat_scaleConvert(inputs.ts_in,inputs.ts_out,inputs.cjd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(cjd, outputs.cjd, relative = %t, compar_crit = "element", prec = 1.e-14);
