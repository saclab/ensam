// ------------------------------
// CL_rot_eul2quat
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.phi = [%pi/8 %pi/2];
inputs.theta = [%pi/3 %pi/3];
inputs.psi = [%pi/2 3*%pi/2];

// Outputs
outputs = struct();
outputs.q = []; outputs.q = CL_rot_defQuat([[0.53163102623437331;0.46622789700423017;0.22729202565684348;0.66958071587584478],[-0.68301270189221919;..
-0.18301270189221924;..
-0.68301270189221919;0.18301270189221949]]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[q] = CL_rot_eul2quat(inputs.phi,inputs.theta,inputs.psi);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(q, outputs.q, relative = %t, compar_crit = "element", prec = 1e-014);
