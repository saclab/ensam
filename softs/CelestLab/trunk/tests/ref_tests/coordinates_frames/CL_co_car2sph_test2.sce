// ------------------------------
// CL_co_car2sph_2
// validation:
// CNES - MSLIB FORTRAN 90, Volume T (mt_car_geoc)
// ------------------------------

// Inputs
inputs = struct();
inputs.pos_car2 = [0;0;-5237095.4];

// Outputs
outputs = struct();
outputs.pos_geoc2 = [0;-1.5707963267948966;5237095.4000000004];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_geoc2] = CL_co_car2sph(inputs.pos_car2);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_geoc2, outputs.pos_geoc2, relative = %t, compar_crit = "element", prec = 1e-014);
