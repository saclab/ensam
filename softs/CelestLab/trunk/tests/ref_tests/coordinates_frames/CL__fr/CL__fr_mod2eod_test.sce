// -------------------
// CL__fr_mod2eod
//
// Reference values : IMCCE ephemeris generator
//                    (http://www.imcce.fr/fr/ephemerides/formulaire/form_ephepos.php)
// -------------------

// -------------------
// -------------------
// MOD -> EOD (IMCCE)
// -------------------
// -------------------

TEST_OK = [];

// Date (TT) = 2050/1/1 02:05:05
jd = CL_dat_convert("cal", "jd", [2050;1;1]);
jd = [jd ; CL_hms2sec([02;05;05])/86400];

pos_MOD = [-1.3516455509889 ; -1.3771801935608;  -0.5673611540056] * %CL_au;
vel_MOD = [0.0221924034952;  -0.0078626349303;  -0.0038161921525] * %CL_au / 86400;

args = struct();
args.obliquity_model = "2006";
[M, omega] = CL__fr_mod2eod(jd, args);
[pos_EOD,vel_EOD] = CL_rot_pvConvert(pos_MOD,vel_MOD,M,omega);

pos_EOD_ref = [-1.3516455509889;  -1.4892245148208;   0.0270981377159] * %CL_au;
vel_EOD_ref = [0.0221924034952;  -0.0087317781040;  -0.0003747021947] * %CL_au / 86400;

// Ecart = 0.03 arcsec
ang = CL_unitConvert(CL_vectAngle(pos_EOD,pos_EOD_ref),"rad","arcsec");

// Le modele d'obliquit� de l'IMCCE est IAU80
args.obliquity_model = "1980";
[M, omega] = CL__fr_mod2eod(jd, args);
[pos_EOD,vel_EOD] = CL_rot_pvConvert(pos_MOD,vel_MOD,M,omega);

// Ecart = 10-14 rad --> OK
ang = CL_vectAngle(pos_EOD,pos_EOD_ref);
TEST_OK($+1) = max(ang) < 1e-13;


// 4e-7 --> NOK : l'IMCCE ne convertit pas la vitesse avec le omega...
ecart_vel_rel = CL_norm(vel_EOD-vel_EOD_ref) / CL_norm(vel_EOD_ref);
// 1e-13 --> OK
ecart_vel_rel = CL_norm(M*vel_MOD-vel_EOD_ref) / CL_norm(vel_EOD_ref);
TEST_OK($+1) = max(ecart_vel_rel) < 1e-12;
// ------------------------------

