// -------------------
// CL__fr_cirs2itrs, CL__fr_tirs2itrs
//
// Reference values : SOFA
// -------------------

TEST_OK = [];

// -------------------
// -------------------
// CIRS -> ITRS (SOFA)
// -------------------
// -------------------
// Date (TT) = 2050/1/1 02:05:05
jd = CL_dat_convert("cal", "jd", [2050;1;1])
jd = [jd ; CL_hms2sec([02;05;05])/86400];

// Reference values : SOFA
pos_CIRS = [2000 ; 3000;  4000] ;
pos_ITRS_ref = [935.060390461056; -3482.220720807414; 3999.975114643877];

args = struct();
ut1_utc = -0.1; // s
tai_utc = 35; // s
tt_tai = 32.184; // s
args.ut1_tt = ut1_utc-tai_utc-tt_tai;
args.xp = 4000 /3600 * %CL_deg2rad / 1000; // 4000 mas
args.yp = 400 /3600 * %CL_deg2rad / 1000; // 400 mas

M1 = CL__fr_cirs2tirs(jd,args);
M2 = CL__fr_tirs2itrs(jd,args);

pos_ITRS = M2*M1*pos_CIRS;

// Ecart = 10-10 rad --> OK
ang = CL_vectAngle(pos_ITRS,pos_ITRS_ref);
TEST_OK($+1) = max(ang) < 1e-9;

// NB : matrice TIRS->ITRS
M2_ref = [0.99999999999812 -1.93925472407228e-006 -1.93925472429085e-005
          1.93925472664636e-006 0.999999999810084 -0.659744096285226
          0.751490337540537 0 -0.751490337540537];

