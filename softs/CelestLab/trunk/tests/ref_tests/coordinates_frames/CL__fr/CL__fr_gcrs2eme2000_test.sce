// -------------------
// CL__fr_gcrs2eme2000
//
// Reference values : OREKIT, STELA
// -------------------

TEST_OK = [];

// ---------------------------------
// ---------------------------------
// GCRS -> EME2000 (OREKIT,STELA)
// ---------------------------------
// ---------------------------------
// Date (TT) = 2050/1/1 02:05:05
jd = CL_dat_convert("cal", "jd", [2050;1;1])
jd = [jd ; CL_hms2sec([02;05;05])/86400];

// Reference values : OREKIT
pos_GCRS = [2000 ; 3000;  4000];
vel_GCRS = [2000;  3000;  4000];
pos_EME2000_ref = [2000.0001099002816; 3000.000273807239; 3999.999739694411];
vel_EME2000_ref = [2000.0001099002816; 3000.000273807239; 3999.999739694411];

// NB : 
// Reference values : STELA : same but with less precision!
// pos_EME2000_ref = [2000.0001099; 3000.00027381; 3999.9997397];
// vel_EME2000_ref = [2000.0001099; 3000.00027381; 3999.9997397];

[M, omega] = CL__fr_gcrs2eme2000(jd);

// omega = 0
pos_EME2000 = M * pos_GCRS;
vel_EME2000 = M * vel_GCRS;

// Ecart = 10-13 rad --> OK
ang = CL_vectAngle(pos_EME2000,pos_EME2000_ref);
ang = CL_vectAngle(vel_EME2000,vel_EME2000_ref);
TEST_OK($+1) = max(ang) < 1e-12;
