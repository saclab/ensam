// -------------------
// CL__fr_mod2tod
//
// Reference values : IMCCE ephemeris generator
//                    (http://www.imcce.fr/fr/ephemerides/formulaire/form_ephepos.php)
//                    OREKIT
// -------------------

TEST_OK = [];

// -------------------
// -------------------
// MOD -> TOD (OREKIT)
// -------------------
// -------------------

// Date (TT) = 2050/1/1 02:05:05
jd = CL_dat_convert("cal", "jd", [2050;1;1])
jd = [jd ; CL_hms2sec([02;05;05])/86400];

// Second test with OREKIT reference values
// Taking into account corrections
pos_MOD = [2000 ; 3000;  4000];
vel_MOD = [2000;  3000;  4000];
pos_TOD_ref = [1999.6798357967396; 3000.2386927401476; 3999.9810425666765];
vel_TOD_ref = [1999.6798357967396; 3000.2386927401476; 3999.9810425666765];

args = struct();
args.nutation_model = "1980";
args.ddp80 = 40 /1000 / 3600 * %CL_deg2rad;  // 40 mas = 2e-7 rad
args.dde80 = 1 /1000 / 3600 * %CL_deg2rad;   // 1 mas
[M, omega] = CL__fr_mod2tod(jd, args);

// OREKIT ne convertit pas la vitesse avec le omega...
pos_TOD = M * pos_MOD;
vel_TOD = M * vel_MOD;
// Ecart = 10-10 rad
ang = CL_vectAngle(pos_TOD,pos_TOD_ref);
TEST_OK($+1) = max(ang) < 1e-9;
ang = CL_vectAngle(vel_TOD,vel_TOD_ref);
TEST_OK($+1) = max(ang) < 1e-9;






// -------------------
// -------------------
// MOD -> TOD (PATRIUS 1.2.1) (TestIERS.java)
// -------------------
// -------------------

// Time scales
ut1_utc = 0.2; // s 
tai_utc = 25; // s 
tt_tai = 32.184; //

// Polar correction
xp = -1; // as
yp = 3; // as

// Length of day (not used in CelestLab)
lod = 0.0; // as

// IAU 1980 corrections
dPsi = 0; // as
dEps = 0; // as

// NB: Patrius configured to have no tidal effects, and no libration effects

// Convert everything to radians
as2rad = CL_unitConvert("arcsec","rad", 1);
xp = xp * as2rad;
yp = yp * as2rad;

dPsi = dPsi * as2rad;
dEps = dEps * as2rad;


// Date 1990/3/5 02:05:05 (UTC)
// Convert to TT
jd_tt = CL_dat_convert("cal", "jd", [1990;3;5]);
jd_tt = [jd_tt ; CL_hms2sec([02;05;05])/86400 + (tai_utc + tt_tai)/86400];

// Reference values
// MOD --> TOD
M_ref = [0.999999998070584 -5.6992970068177654E-5 -2.4710986757011613E-5
         5.699214476047074E-5 0.9999999978182401 -3.3397829540398184E-5
         2.471289014459751E-5 3.339642114382545E-5 0.9999999991369759];

omega_ref = [-3.2255755954679194E-13;2.4590896437464305E-12;-5.671560458394299E-12];

// Call CL__fr_mod2tod using IAU1980
args = struct();
args.nutation_model = "1980";
args.ddp80 = dPsi;
args.dde80 = dEps;

[M, omega] = CL__fr_mod2tod(jd_tt, args);

[ax, ang] = CL_rot_quat2axAng(CL_rot_matrix2quat(M*M_ref'));

// Precision matrice = 8e-11
TEST_OK($+1) = ang < 1e-10;

// Precision omega = 3e-6
TEST_OK($+1) = CL_vectAngle(omega,omega_ref) < 1e-5;



