// -------------------
// CL__fr_gcrs2cirs
//
// Reference values : OREKIT, STELA
// -------------------

TEST_OK = [];

// -------------------
// -------------------
// GCRS -> CIRS (OREKIT)
// -------------------
// -------------------
// Date (TT) = 2050/1/1 02:05:05
jd = CL_dat_convert("cal", "jd", [2050;1;1])
jd = [jd ; CL_hms2sec([02;05;05])/86400];

// Reference values : OREKIT
pos_GCRS = [2000 ; 3000;  4000];
vel_GCRS = [2000;  3000;  4000];
pos_CIRS_ref = [1980.4299749194033; 3000.214191618972; 4009.5650535748728];
vel_CIRS_ref = [1980.4299749194033; 3000.214191618972; 4009.5650535748728];

args = struct();
args.model = "classic";
args.dx06 = 0;
args.dy06 = 0;
[M, omega] = CL__fr_gcrs2cirs(jd,args);

// OREKIT ne convertit pas la vitesse avec le omega...
pos_CIRS = M * pos_GCRS;
vel_CIRS = M * vel_GCRS;

// Ecart = 10-9 rad
ang = CL_vectAngle(pos_CIRS,pos_CIRS_ref);
ang = CL_vectAngle(vel_CIRS,vel_CIRS_ref);
TEST_OK($+1) = max(ang) < 1e-8;

// -------------------
// -------------------
// GCRS -> CIRS (SOFA)
// -------------------
// -------------------
// Date (TT) = 2050/1/1 02:05:05
jd = CL_dat_convert("cal", "jd", [2050;1;1])
jd = [jd ; CL_hms2sec([02;05;05])/86400];

// Reference values : SOFA
pos_GCRS = [2000 ; 3000;  4000];
pos_CIRS_ref = [1980.422208723123; 3000.213419943974; 4009.569466910918];

args = struct();
args.model = "classic";
args.dx06 = 400 /3600 * %CL_deg2rad / 1000; // 400 mas
args.dy06 = 40 /3600 * %CL_deg2rad / 1000; // 40 mas
M = CL__fr_gcrs2cirs(jd,args);
pos_CIRS = M * pos_GCRS;

// Ecart = 3.10-12 rad
ang = CL_vectAngle(pos_CIRS,pos_CIRS_ref);
TEST_OK($+1) = max(ang) < 4e-12;



// -------------------
// -------------------
// GCRS -> CIRS (PATRIUS 1.2.1)
// -------------------
// -------------------

// Time scales
ut1_utc = 0.2; // s 
tai_utc = 25; // s 
tt_tai = 32.184; //

// Polar correction
xp = -1; // as
yp = 3; // as

// Length of day (not used in CelestLab)
lod = 0.0; // as

// IAU 2000 corrections
dx06 = 1; // as 
dy06 = -2; // as

// IAU 1980 corrections
dPsi = 1; // as (not used here)
dEps = -2; // as (not used here)

// NB: Patrius configured to have no tidal effects, and no libration effects

// Convert everything to radians
as2rad = CL_unitConvert("arcsec","rad", 1);
xp = xp * as2rad;
yp = yp * as2rad;

dx06 = dx06 * as2rad;
dy06 = dy06 * as2rad;
dPsi = dPsi * as2rad;
dEps = dEps * as2rad;


// Date 1990/3/5 02:05:05 (UTC)
// Convert to TT
jd_tt = CL_dat_convert("cal", "jd", [1990;3;5]);
jd_tt = [jd_tt ; CL_hms2sec([02;05;05])/86400 + (tai_utc + tt_tai)/86400];

// Reference values
M_ref = [0.99999957174784 -1.289134216698727E-8 9.254750869687932E-4
         3.389822577134812E-8 0.9999999997423892 -2.2698470641167818E-5
         -9.25475086437767E-4 2.2698492292462182E-5 0.9999995714902297];

omega_ref = [-3.251288059852135E-13;5.544267519044799E-12;3.3087224502121107E-24];

args = struct();
args.model = "series";
args.dx06 = dx06;
args.dy06 = dy06;

[M, omega] = CL__fr_gcrs2cirs(jd_tt, args);

[ax,ang] = CL_rot_quat2axAng(CL_rot_matrix2quat(M*M_ref'));

// Precision sur M: 1e-15, precision sur omega = 8e-5
TEST_OK($+1) = ang < 1e-15 & CL_vectAngle(omega,omega_ref) < 1e-4;

