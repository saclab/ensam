// ------------------------------
// CL_fr_ter2G50
// validation:
// Validation with PSIMU
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = [21010 , 21011];
inputs.pos_ter = [ [3952930.5;3127929.25;4128420.75] , [3945680.5;3125978.25;5687420.75]];

// Outputs
outputs = struct();
outputs.pos_G50 = [[4186081.5330181196;-2808259.6620968799;4128420.75],[4229587.3369911686;-2729601.726684182;5687420.75]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_G50] = CL_fr_ter2G50(inputs.cjd,inputs.pos_ter);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_G50, outputs.pos_G50, relative = %t, compar_crit = "element", prec = 1e-014);
