// -------------------------------------------------------
// CL_fr_bodyConvert
// Saturn: ICRS --> BCF
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Saturn";
inputs.frame1 = "ICRS";
inputs.frame2 = "BCF";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [412677.14;410525.5;5972609.7];
outputs.vel2 = [1590.3873;-2620.5942;6096.0796];
outputs.jacob = [-0.9963381,0.0048823,0.0853607,0.,0.,0.;
0.0014002,-0.9973028,0.0733841,0.,0.,0.;
0.0854888,0.0732349,0.9936440,0.,0.,0.;
0.0000002,-0.0001633,0.0000120,-0.9963381,0.0048823,0.0853607;
0.0001632,-0.0000008,-0.0000140,0.0014002,-0.9973028,0.0733841;
3.128D-14,-2.720D-15,-2.490D-15,0.0854888,0.0732349,0.9936440];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
