// -------------------------------------------------------
// CL_fr_bodyConvert
// Uranus: ICRS --> BCI
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Uranus";
inputs.frame1 = "ICRS";
inputs.frame2 = "BCI";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [90967.906;5777373.6;-1620055.2];
outputs.vel2 = [-1634.5535;5082.16;-4183.2864];
outputs.jacob = [0.9755767,-0.2196589,0.,0.,0.,0.;
-0.0574997,-0.2553749,0.9651308,0.,0.,0.;
-0.2119996,-0.9415592,-0.2617681,0.,0.,0.;
0.,0.,0.,0.9755767,-0.2196589,0.;
0.,0.,0.,-0.0574997,-0.2553749,0.9651308;
0.,0., 0.,-0.2119996,-0.9415592,-0.2617681];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
