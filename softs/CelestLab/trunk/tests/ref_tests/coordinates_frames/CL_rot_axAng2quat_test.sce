// ------------------------------
// CL_rot_axAng2quat
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.u = [1 2 3;4 5 6]';
inputs.alpha = [%pi/5 %pi/2];

// Outputs
outputs = struct();
outputs.q = [];outputs.q = CL_rot_defQuat([[0.95105651629515353;0.082588265688693124;0.16517653137738625;0.24776479706607937],[0.70710678118654757;..
0.32232918561015206;..
0.40291148201269011;0.48349377841522811]]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[q] = CL_rot_axAng2quat(inputs.u,inputs.alpha);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(q, outputs.q, relative = %t, compar_crit = "element", prec = 1e-014);
