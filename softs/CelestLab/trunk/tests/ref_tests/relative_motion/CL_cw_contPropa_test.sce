// ------------------------------
// CL_cw_contPropa
// validation:
// equivalent scripts
// ------------------------------

// Inputs
inputs = struct();
inputs.rel_date_ini = 11550;
inputs.rel_pos_vel_ini = [-249.99355 ; 0. ; 0.0000435 ; -0.0000003 ; 0. ;0.0000003];
inputs.gama_diff = [0. ; 0. ;0.0003238];
inputs.alt = 450.e3;
inputs.rel_date_end = inputs.rel_date_ini + 3600;
inputs.delta_t = 720;
inputs.ballistic_coef_chaser = 100 * 1 / 20.e3;
inputs.ballistic_coef_target = 200 * 1 / 400.e3;

// Outputs
outputs = struct();
outputs.rel_dates = [11550,12270,12990,13710,14430,15150];
outputs.rel_pos_vel = [[-249.99355;0;4.35e-005;-2.9999999999999999e-007;0;2.9999999999999999e-007],[-206.788530567452;0;79.772334823259172;..
0.17699864025861906;0;0.20988614454472845],[66.159078722763269;0;271.15627684556426;0.60377613161979493;0;0.29307882768765192],..
[660.25564912074219;0;458.18055033061324;1.020797009906055;0;0.19843818881517378],[1471.5165867258142;0;527.55380909735811;..
1.174523690785245;0;-0.015858330383548946],[2264.4876850925657;0;438.30707429013387;0.97327065473832752;0;..
-0.21807849100561016]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[rel_dates,rel_pos_vel] = CL_cw_contPropa(inputs.rel_date_ini,inputs.rel_pos_vel_ini,inputs.gama_diff,inputs.alt,inputs.rel_date_end,inputs.delta_t,inputs.ballistic_coef_chaser,inputs.ballistic_coef_target);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(rel_dates, outputs.rel_dates, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(rel_pos_vel, outputs.rel_pos_vel, relative = %t, compar_crit = "element", prec = 1e-014);
