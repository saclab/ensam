// ------------------------------
// CL_cw_ballisticPropa
// validation:
// equivalent scripts
// ------------------------------

// Inputs
inputs = struct();
inputs.rel_date_ini=0;
inputs.rel_pos_vel_ini=[1;1;1;1;1;1];
inputs.alt=450.e3;
inputs.rel_date_end=500;
inputs.delta_t=100;
inputs.ballistic_coef_chaser = 100 * 1 / 20.e3;
inputs.ballistic_coef_target = 200 * 1 / 400.e3;

// Outputs
outputs = struct();
outputs.rel_dates = [0,100,200,300,400,500];
outputs.rel_pos_vel = [[1;1;1;1;1;1],[111.33461141980622;100.78519715169722;89.633045582719134;1.1981422715771564;0.99362119494055967;..
0.77081883910248761],[238.88048307545398;199.30980159505688;154.84384038446311;1.3438672925627244;0.97481445732167382;..
0.53204386098293155],[378.29958219823305;295.34149547524362;195.82148290426161;1.4353603258685002;0.94381501649229438;0.28666159545778813],..
[524.10616407339467;387.67914155687095;212.05817304819601;1.4714849527522254;0.90101060468072913;0.037741214423509091],..
[670.73538618173848;475.16780670843599;203.35556354918887;1.4517972868214362;0.84693660734971721;..
-0.21160385647649357]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[rel_dates,rel_pos_vel] = CL_cw_ballisticPropa(inputs.rel_date_ini,inputs.rel_pos_vel_ini,inputs.alt,inputs.rel_date_end,inputs.delta_t,inputs.ballistic_coef_chaser,inputs.ballistic_coef_target);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(rel_dates, outputs.rel_dates, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(rel_pos_vel, outputs.rel_pos_vel, relative = %t, compar_crit = "element", prec = 1e-014);
