// ------------------------------
// CL_cw_ballisticPropa_2
// validation:
// equivalent scripts
// ------------------------------

// Inputs
inputs = struct();
inputs.rel_date_ini=0;
inputs.rel_pos_vel_ini=[1;1;1;1;1;1];
inputs.alt=450.e3;
inputs.rel_date_end=500;
inputs.delta_t=100;

// Outputs
outputs = struct();
outputs.rel_dates = [0,100,200,300,400,500];
outputs.rel_pos_vel = [[1;1;1;1;1;1],[111.3451609666744;100.78519715169722;89.632255810461515;1.1983523789732597;0.99362119494055967;..
0.7707951558236148],[238.92215180159164;199.30980159505688;154.83753406242107;1.3442769291793137;0.97481445732167382;0.5319494240911864],..
[378.39135797438871;295.34149547524362;195.80026559857646;1.4359484676631664;0.94381501649229438;0.28645021958532707],..
[524.26442049257344;387.67914155687095;212.00810017159586;1.4722203927785371;0.90101060468072913;0.037368176842147494],..
[670.97288274175708;475.16780670843599;203.25831477593769;1.4526390255565891;0.84693660734971721;..
-0.2121812564760604]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[rel_dates,rel_pos_vel] = CL_cw_ballisticPropa(inputs.rel_date_ini,inputs.rel_pos_vel_ini,inputs.alt,inputs.rel_date_end,inputs.delta_t);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(rel_dates, outputs.rel_dates, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(rel_pos_vel, outputs.rel_pos_vel, relative = %t, compar_crit = "element", prec = 1e-014);
