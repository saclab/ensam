// ------------------------------
// CL_cw_meanChaser
// validation:
// manual calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.Z0 = [30 20 15];
inputs.dX0 = [7 8 9];
inputs.alt = [260 180 300]*1e3;

// Outputs
outputs = struct();
outputs.zm = [-11873.015533745362,-13379.277665517911,-15499.173923899742];

TEST_OK = [];

//=========================
// Compute output
//=========================
[zm] = CL_cw_meanChaser(inputs.Z0,inputs.dX0,inputs.alt);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(zm, outputs.zm, relative = %t, compar_crit = "element", prec = 1e-014);
