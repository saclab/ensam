// ------------------------------
// CL_cw_twoImpulse
// validation:
// Orbital Mechanics for engineering students, H D Curtis, Examples 7.2
// ------------------------------

// Inputs
inputs = struct();
inputs.pv_ini = [20.e3,20.e3,-20.e3,0.02e3,-0.005e3,0.02e3]';
inputs.pv_fin = [0,0,0,0,0,0]';
inputs.alt = 300000;
inputs.t_transfer = 8*3600; // conversion en secondes

// Outputs
outputs = struct();
outputs.delta_vi = [-66.744984965707417;12.957265747859607;-29.274677072283168];
outputs.delta_vf = [0.47003466811861472;24.467546555450504;-25.784957879874085];

TEST_OK = [];

//=========================
// Compute output
//=========================
[delta_vi,delta_vf] = CL_cw_twoImpulse(inputs.pv_ini,inputs.pv_fin,inputs.alt,inputs.t_transfer);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(delta_vi, outputs.delta_vi, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(delta_vf, outputs.delta_vf, relative = %t, compar_crit = "element", prec = 1e-014);
