// ------------------------------
// CL_cw_diffDrag
// validation:
// manual calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.alt = [250000 350000];
inputs.cbal1 = 2.25;
inputs.cbal2 = 2.15;

// Outputs
outputs = struct();
outputs.acc_diff = [-0.00052229535992304524,-0.00013572757309873512];

TEST_OK = [];

//=========================
// Compute output
//=========================
[acc_diff] = CL_cw_diffDrag(inputs.alt,inputs.cbal1,inputs.cbal2);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(acc_diff, outputs.acc_diff, relative = %t, compar_crit = "element", prec = 1e-014);
