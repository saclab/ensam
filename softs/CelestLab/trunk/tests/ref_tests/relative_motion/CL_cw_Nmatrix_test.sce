// ------------------------------
// CL_cw_Nmatrix
// validation:
// equivalent scripts
// ------------------------------

// Inputs
inputs = struct();
inputs.alt = 350000;
inputs.delta_t = 100;

// Outputs
outputs = struct();
outputs.N2 = [[4978.1971771710796;0;-381.08448506032556;99.128077309109486;0;-11.427546990730518],[0;4994.5492942927704;0;0;..
99.782019327277368;0],[381.08448506032556;0;4994.5492942927704;11.427546990730518;0;99.782019327277368]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[N2] = CL_cw_Nmatrix(inputs.alt,inputs.delta_t);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(N2, outputs.N2, relative = %t, compar_crit = "element", prec = 1e-014);
