// ------------------------------
// CL_cw_circularDrift
// validation:
// manual calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.alt = 450000;
inputs.z = [1000 5000];

// Outputs
outputs = struct();
outputs.xp = [1.6784440706109811,8.3922203530549062];

TEST_OK = [];

//=========================
// Compute output
//=========================
[xp] = CL_cw_circularDrift(inputs.alt,inputs.z);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(xp, outputs.xp, relative = %t, compar_crit = "element", prec = 1e-014);
