// ------------------------------
// CL_cross
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.u = [1;0;0];
inputs.v = [0;1;0];

// Outputs
outputs = struct();
outputs.w = [0;0;1];

TEST_OK = [];

//=========================
// Compute output
//=========================
[w] = CL_cross(inputs.u,inputs.v);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(w, outputs.w, relative = %t, compar_crit = "element", prec = 1e-014);
