// ------------------------------
// CL_colMult
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.A = [ 1 2 3];
inputs.B = [ 1 2 3 ; 4 5 6 ; 7 8 9];

// Outputs
outputs = struct();
outputs.C = [[1;4;7],[4;10;16],[9;18;27]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[C] = CL_colMult(inputs.A,inputs.B);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(C, outputs.C, relative = %t, compar_crit = "element", prec = 1e-014);
