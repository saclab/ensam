// ------------------------------
// CL_rMod
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.x = [20 10 5]';
inputs.a = -%pi;
inputs.b = %pi;

// Outputs
outputs = struct();
outputs.y = [1.1504440784612413;-2.5663706143591725;-1.2831853071795862];

TEST_OK = [];

//=========================
// Compute output
//=========================
[y] = CL_rMod(inputs.x,inputs.a,inputs.b);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(y, outputs.y, relative = %t, compar_crit = "element", prec = 1e-014);
