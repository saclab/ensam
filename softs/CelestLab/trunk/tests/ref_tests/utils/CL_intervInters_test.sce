// ------------------------------
// CL_intervInters
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.i1 = [1,5,10;3,6,12];
inputs.i2 = [2,5.5,6.9;4,6.7,15];

// Outputs
outputs = struct();
outputs.ires = [[2;3],[5.5;6],[10;12]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[ires] = CL_intervInters(inputs.i1,inputs.i2);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(ires, outputs.ires, relative = %t, compar_crit = "element", prec = 1e-014);
