// ------------------------------
// CL_stumpS
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.z = [-40,0,200];

// Outputs
outputs = struct();
outputs.s = [1.0780573662223529,0.16666666666666666,0.0046464509736266037];

TEST_OK = [];

//=========================
// Compute output
//=========================
[s] = CL_stumpS(inputs.z);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(s, outputs.s, relative = %t, compar_crit = "element", prec = 1e-014);
