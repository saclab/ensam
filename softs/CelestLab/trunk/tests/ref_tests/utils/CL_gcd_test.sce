// ------------------------------
// CL_gcd
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.u = [10,12,3];
inputs.v = [1,14,5];

// Outputs
outputs = struct();
outputs.maco = [1,2,1];

TEST_OK = [];

//=========================
// Compute output
//=========================
[maco] = CL_gcd(inputs.u,inputs.v);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(maco, outputs.maco, relative = %t, compar_crit = "element", prec = 1e-014);
