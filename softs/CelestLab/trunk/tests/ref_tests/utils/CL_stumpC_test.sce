// ------------------------------
// CL_stumpC
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.z = [-40,0,10];

// Outputs
outputs = struct();
outputs.c = [6.9513921282490845,0.5,0.19997860728793257];

TEST_OK = [];

//=========================
// Compute output
//=========================
[c] = CL_stumpC(inputs.z);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(c, outputs.c, relative = %t, compar_crit = "element", prec = 1e-014);
