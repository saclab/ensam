// ------------------------------
// CL_locationInfo: 
// ------------------------------

TEST_OK = [];
info = "land_ratio"

// Case 1: point in Atlantic Ocean.
lon = -30* %CL_deg2rad;
lat = 20* %CL_deg2rad;
rap_res = 0;

[val] = CL_locationInfo(info,lon,lat)
TEST_OK($+1) = CL__isEqual( rap_res , val , 1e-15);

// Case 2: point in Pacific Ocean.
lon = -100* %CL_deg2rad;
lat = -40* %CL_deg2rad;
rap_res = 0;

[val] = CL_locationInfo(info,lon,lat)
TEST_OK($+1) = CL__isEqual( rap_res , val , 1e-15);

// Case 3: point in Europe.
lon = 100* %CL_deg2rad;
lat = 60* %CL_deg2rad;
rap_res = 1;

[val] = CL_locationInfo(info,lon,lat)
TEST_OK($+1) = CL__isEqual( rap_res , val , 1e-15);


// Case 4: point in Africa.
lon = 20* %CL_deg2rad;
lat = 0* %CL_deg2rad;
rap_res = 1;

[val] = CL_locationInfo(info,lon,lat)
TEST_OK($+1) = CL__isEqual( rap_res , val , 1e-15);

// Case 4: dim lat =/ dim lon.
lon = (30: 1: 40) * %CL_deg2rad; 
lat = (30: 1: 50) * %CL_deg2rad; 
try 
  [val] = CL_locationInfo(info,lon,lat)
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Case 5: all land in Europe.
lon = (19: 1: 44) * %CL_deg2rad; 
lat = (50: 0.04 : 51) * %CL_deg2rad; 
rap_res = ones(1,26);
[val] = CL_locationInfo(info,lon,lat)
TEST_OK($+1) = CL__isEqual( rap_res , val , 1e-15);





