// ------------------------------
// CL_matSort
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.A = [1,3,2;1000,1010,1020;10,80,15];
inputs.num = 2;

// Outputs
outputs = struct();
outputs.B = [[1;10;1000],[3;80;1010],[2;15;1020]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[B] = CL_matSort(inputs.A,inputs.num);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(B, outputs.B, relative = %t, compar_crit = "element", prec = 1e-014);
