// ------------------------------
// CL_vectAngle
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.u = [ [0.5;1;1.2] , [0.5;1;1.2] ];
inputs.v = [ [0.5;1+1e-8;1.2+1e-8] , [0.8;-0.2;0.6] ];

// Outputs
outputs = struct();
outputs.alpha = [2.7317728893744883e-009,0.98838306467357817];

TEST_OK = [];

//=========================
// Compute output
//=========================
[alpha] = CL_vectAngle(inputs.u,inputs.v);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(alpha, outputs.alpha, relative = %t, compar_crit = "element", prec = 1e-014);
