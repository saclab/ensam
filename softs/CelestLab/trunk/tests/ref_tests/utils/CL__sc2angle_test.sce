// ------------------------------
// CL__sc2angle
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.x = [0,-0.25,0.25,-0.25];
inputs.y = [5,0.25,-0.25,-0.25];

// Outputs
outputs = struct();
outputs.angle = [1.5707963267948966,2.3561944901923448,5.497787143782138,3.9269908169872414];

TEST_OK = [];

//=========================
// Compute output
//=========================
[angle] = CL__sc2angle(inputs.x,inputs.y);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(angle, outputs.angle, relative = %t, compar_crit = "element", prec = 1e-014);
