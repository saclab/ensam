// -------------------------------------------------------
// CL_eph_planet
// Earth
//
// The reference values are extracted from the file vsop87.chk available at the URL :
// ftp://ftp.imcce.fr/pub/ephem/planets/vsop87/
// The series used is VSOP87D (heliocentric, spherical coordinates, ecliptic of date)
//
// # Col 1 = Julian date (TDB)
// # Col 2 = longitude l (rad)
// # Col 3 = latitude b (rad)
// # Col 4 = distance r (au)
// # Col 5 = l' (rad/d)
// # Col 6 = b' (rad/d)
// # Col 7 = r' (au/d)
//
// 2451545 1.7519238681 -3.9656e-006 0.9833276819 0.0177924465 1.146e-007 -7.3533e-006 
// 2415020 1.7391225563 -5.679e-007 0.9832689778  0.0177986246 -7.021e-007 -6.3767e-006 
// 2378495 1.7262638916 2.083e-007 0.9832274321   0.0177977841 -2.336e-007 -8.0498e-006 
// 2341970 1.7134419105 2.5051e-006 0.9831498441  0.017794624 5.708e-007 -4.7389e-006 
// 2305445 1.7006065938 -1.6359e-006 0.9831254376 0.01778877 4.115e-007 3.344e-006 
// 2268920 1.687762496 -2.034e-006 0.9830816756   0.0177901891 -4.781e-007 1.36071e-005 
// 2232395 1.6750110961 3.7879e-006 0.9830754409  0.0177958136 -5.221e-007 2.42192e-005 
// 2195870 1.6622048657 1.5133e-006 0.9830942385  0.0178015076 2.953e-007 2.71498e-005 
// 2159345 1.6495143197 -1.3003e-006 0.9830440397 0.0178071396 6.915e-007 2.7596e-005 
// 2122820 1.6367193623 -3.1292e-006 0.9830331815 0.0178036194 -1.563e-007 2.73747e-005 
//
// NB : The reference values were written using %.10f (poor precision)
//      So the number of significant digits in the reference results depend on the absolute value
//      of the result. 
//      For position there is about 10 significant digits (avg value close to 1)
//      For velocity there is about 8 significant digits  (avg value close to 0.01)
//      --> The test will be done at a relative precision of 10-5
// To check the reliability of the results the following test has been performed:
// 1.- cjd = [2000:10:2100]
// 2.- tt_tref =0
// 3.- [pos1,vel1]=CL_eph_planet("Earth",cjd,tt_tref,model="full")
// 4.- [pos2,vel2]=CL_eph_planet("Earth",cjd,tt_tref,model="high")
// 5.- theta1=acos((pos1'*pos2)/(norm(pos1)*norm(pos2))) < 3arcsec
// ------------------------------
// -------------------------------------------------------

RELATIVE_PRECISION = 1e-5;
COMPAR_CRIT = "norm";
 
// Inputs
inputs.planet = "Earth";
jd = [2451545:-36525:2122820];
inputs.cjd = CL_dat_convert("jd","cjd",jd);
inputs.tt_tref = 0;
inputs.accuracy = "high";

// Outputs
pos_sph = [ 1.7519238681 -3.9656e-006 0.9833276819
            1.7391225563 -5.679e-007 0.9832689778 
            1.7262638916 2.083e-007 0.9832274321  
            1.7134419105 2.5051e-006 0.9831498441 
            1.7006065938 -1.6359e-006 0.9831254376
            1.687762496 -2.034e-006 0.9830816756  
            1.6750110961 3.7879e-006 0.9830754409 
            1.6622048657 1.5133e-006 0.9830942385 
            1.6495143197 -1.3003e-006 0.9830440397
            1.6367193623 -3.1292e-006 0.9830331815]';
vel_sph = [ 0.0177924465 1.146e-007 -7.3533e-006  
            0.0177986246 -7.021e-007 -6.3767e-006  
            0.0177977841 -2.336e-007 -8.0498e-006 
            0.017794624 5.708e-007 -4.7389e-006 
            0.01778877 4.115e-007 3.344e-006  
            0.0177901891 -4.781e-007 1.36071e-005 
            0.0177958136 -5.221e-007 2.42192e-005  
            0.0178015076 2.953e-007 2.71498e-005  
            0.0178071396 6.915e-007 2.7596e-005  
            0.0178036194 -1.563e-007 2.73747e-005 ]';

[pos,vel] = CL_co_sph2car(pos_sph,vel_sph);

outputs.pos = %CL_au * pos;
outputs.vel = %CL_au /86400 * vel;
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos,vel] = CL_eph_planet(inputs.planet,inputs.cjd,inputs.tt_tref,inputs.accuracy);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos, outputs.pos, relative = %t, compar_crit = "norm", prec = 1e-005);

TEST_OK($+1) = CL__isEqual(vel, outputs.vel, relative = %t, compar_crit = "norm", prec = 1e-005);
