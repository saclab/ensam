// ------------------------------
// CL_mod_sidTime
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
jj_ut1 = [19500:250:20500];
ss_ut1 = (43200*ones(jj_ut1))/3600/24;
inputs.jj_ut1 = jj_ut1+ss_ut1;

// Outputs
outputs = struct();
outputs.GMST = [1.059276246853345,5.3599742016388543,3.3774868498791788,1.394999498753891,5.6956974554425992];

TEST_OK = [];

//=========================
// Compute output
//=========================
[GMST] = CL_mod_sidTime(inputs.jj_ut1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(GMST, outputs.GMST, relative = %t, compar_crit = "element", prec = 1e-014);
