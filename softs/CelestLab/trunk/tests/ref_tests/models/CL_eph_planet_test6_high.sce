// -------------------------------------------------------
// CL_eph_planet
// Saturn
//
// The reference values are extracted from the file vsop87.chk available at the URL :
// ftp://ftp.imcce.fr/pub/ephem/planets/vsop87/
// The series used is VSOP87D (heliocentric, spherical coordinates, ecliptic of date)
//
// # Col 1 = Julian date (TDB)
// # Col 2 = longitude l (rad)
// # Col 3 = latitude b (rad)
// # Col 4 = distance r (au)
// # Col 5 = l' (rad/d)
// # Col 6 = b' (rad/d)
// # Col 7 = r' (au/d)
//
// 2451545 0.7980038761 -0.0401984149 9.1838483715 0.0006315388 1.0281e-005 -0.000215291 
// 2415020 4.6512836347 0.0192701409 10.0668531997 0.0005251308 -2.04421e-005 3.14346e-005 
// 2378495 2.1956677359 0.0104156566 9.1043068639  0.0006415768 2.71252e-005 0.0001888842 
// 2341970 5.8113963637 -0.0291472787 9.7629994924 0.000558448 -1.81139e-005 -0.0002760239 
// 2305445 3.5217555199 0.0437035058 9.7571035629  0.0005594038 -6.983e-007 0.0002970345 
// 2268920 0.8594235308 -0.0379350088 9.0669212839 0.0006470969 1.41612e-005 -0.0001756487 
// 2232395 4.6913199264 0.0146771898 10.1065692994 0.0005201683 -2.14885e-005 -4.00552e-005 
// 2195870 2.2948875823 0.0178533697 9.1857599537  0.0006290832 2.5198e-005 0.0002663743 
// 2159345 5.8660241564 -0.0333866503 9.592717394  0.0005771902 -1.64313e-005 -0.0003120077 
// 2122820 3.5570108069 0.0435371139 9.8669939498  0.0005458233 -3.363e-006 0.0002502429
//
// NB : The reference values were written using %.10f (poor precision)
//      So the number of significant digits in the reference results depend on the absolute value
//      of the result. 
//      For position there is about 10 significant digits (avg value close to 1)
//      For velocity there is about 7 significant digits  (avg value close to 0.0005)
//      --> The test will be done at a relative precision of 10-4
// To check the reliability of the results the following test has been performed:
// 1.- cjd = [2000:10:2100]
// 2.- tt_tref =0
// 3.- [pos1,vel1]=CL_eph_planet("Saturn",cjd,tt_tref,model="full")
// 4.- [pos2,vel2]=CL_eph_planet("Saturn",cjd,tt_tref,model="high")
// 5.- theta1=acos((pos1'*pos2)/(norm(pos1)*norm(pos2))) < 3arcsec
// ------------------------------
// -------------------------------------------------------

RELATIVE_PRECISION = 1e-4;
COMPAR_CRIT = "norm";
 
// Inputs
inputs.planet = "Saturn";
jd = [2451545:-36525:2122820];
inputs.cjd = CL_dat_convert("jd","cjd",jd);
inputs.tt_tref = 0;
inputs.accuracy = "high";

// Outputs
pos_sph = [ 0.7980038761 -0.0401984149 9.1838483715
            4.6512836347 0.0192701409 10.0668531997
            2.1956677359 0.0104156566 9.1043068639 
            5.8113963637 -0.0291472787 9.7629994924
            3.5217555199 0.0437035058 9.7571035629 
            0.8594235308 -0.0379350088 9.0669212839
            4.6913199264 0.0146771898 10.1065692994
            2.2948875823 0.0178533697 9.1857599537 
            5.8660241564 -0.0333866503 9.592717394 
            3.5570108069 0.0435371139 9.8669939498 ]';
vel_sph = [ 0.0006315388 1.0281e-005 -0.000215291  
            0.0005251308 -2.04421e-005 3.14346e-005  
            0.0006415768 2.71252e-005 0.0001888842 
            0.000558448 -1.81139e-005 -0.0002760239 
            0.0005594038 -6.983e-007 0.0002970345  
            0.0006470969 1.41612e-005 -0.0001756487 
            0.0005201683 -2.14885e-005 -4.00552e-005 
            0.0006290832 2.5198e-005 0.0002663743  
            0.0005771902 -1.64313e-005 -0.0003120077 
            0.0005458233 -3.363e-006 0.0002502429]';

[pos,vel] = CL_co_sph2car(pos_sph,vel_sph);

outputs.pos = %CL_au * pos;
outputs.vel = %CL_au /86400 * vel;
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos,vel] = CL_eph_planet(inputs.planet,inputs.cjd,inputs.tt_tref,inputs.accuracy);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos, outputs.pos, relative = %t, compar_crit = "norm", prec = 1e-004);

TEST_OK($+1) = CL__isEqual(vel, outputs.vel, relative = %t, compar_crit = "norm", prec = 1e-004);
