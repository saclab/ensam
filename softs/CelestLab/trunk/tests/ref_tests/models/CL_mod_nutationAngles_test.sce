// ------------------------------
// CL_mod_nutationAngles
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
jj_tai = [19500];
ss_tai = 43200*ones(jj_tai);
inputs.jj_tai = [jj_tai jj_tai]; //to test vectorization
inputs.ss_tai = [ss_tai ss_tai];
inputs.flag = "s";

// Outputs
outputs = struct();
outputs.NUT = [[-7.4284293949388277e-005;2.1623835667510362e-005],[-7.4284293949388277e-005;2.1623835667510362e-005]];
outputs.F = [[1.9099060090775879;2.4033641940779233;4.7335530109423871;4.7124263233131956;1.0382576145545936],[1.9099060090775879;..
2.4033641940779233;4.7335530109423871;4.7124263233131956;1.0382576145545936]];
outputs.NUTP = [[-2.4406169371758244e-012;1.6280814551328604e-012],[-2.4406169371758244e-012;1.6280814551328604e-012]];
outputs.FP = [[2.639203056061643e-006;1.9909687523436022e-007;2.6724041602421145e-006;2.4626008135916921e-006;-1.0696961162052566e-008],..
[2.639203056061643e-006;1.9909687523436022e-007;2.6724041602421145e-006;2.4626008135916921e-006;..
-1.0696961162052566e-008]];
outputs.NUTPP = [[-2.4113455628877719e-017;-8.6987126653972696e-018],[-2.4113455628877719e-017;-8.6987126653972696e-018]];
outputs.FPP = [[3.104386894037354e-023;-5.3862935480429567e-025;-1.241513871591545e-023;-6.2019973407672824e-024;7.2759701490803236e-024],..
[3.104386894037354e-023;-5.3862935480429567e-025;-1.241513871591545e-023;-6.2019973407672824e-024;..
7.2759701490803236e-024]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[NUT,F,NUTP,FP,NUTPP,FPP] = CL_mod_nutationAngles(inputs.jj_tai,inputs.ss_tai,inputs.flag);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(NUT, outputs.NUT, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(F, outputs.F, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(NUTP, outputs.NUTP, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(FP, outputs.FP, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(NUTPP, outputs.NUTPP, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(FPP, outputs.FPP, relative = %t, compar_crit = "element", prec = 1e-014);
