// -------------------------------------------------------
// CL_ephemJPL_getPVA
// -------------------------------------------------------

// Reference values obtained with Horizons website: http://ssd.jpl.nasa.gov/horizons.cgi#top


// *******************************************************************************
// Ephemeris / WWW_USER Wed Oct 31 03:18:13 2012 Pasadena, USA      / Horizons    
// *******************************************************************************
// Target body name: Jupiter Barycenter (5)          {source: DE405}
// Center body name: Solar System Barycenter (0)     {source: DE405}
// Center-site name: BODY CENTER
// *******************************************************************************
// Start time      : A.D. 2012-Oct-31 08:15:00.0000 CT 
// Stop  time      : A.D. 2012-Nov-01 08:15:00.0000 CT 
// Step-size       : 1440 minutes
// *******************************************************************************
// Center geodetic : 0.00000000,0.00000000,0.0000000 {E-lon(deg),Lat(deg),Alt(km)}
// Center cylindric: 0.00000000,0.00000000,0.0000000 {E-lon(deg),Dxy(km),Dz(km)}
// Center radii    : (undefined)                                                  
// Output units    : AU-D                                                         
// Output format   : 03
// Reference frame : ICRF/J2000.0                                                 
// Output type     : GEOMETRIC cartesian states
// Coordinate systm: Earth Mean Equator and Equinox of Reference Epoch            
// *******************************************************************************
// JDCT 
   // X     Y     Z
   // VX    VY    VZ
   // LT    RG    RR
// *******************************************************************************
// $$SOE
// 2456231.843750000 = A.D. 2012-Oct-31 08:15:00.0000 (CT)
   // 1.869240967874877E+00  4.320107525040921E+00  1.806122949755814E+00
  // -7.099574459128299E-03  2.840184349473858E-03  1.390227846351940E-03
   // 2.911885006298897E-02  5.041772598355823E+00  2.994995780577250E-04
// 2456232.843750000 = A.D. 2012-Nov-01 08:15:00.0000 (CT)
   // 1.862139239827985E+00  4.322942726802247E+00  1.807511094359977E+00
  // -7.103878776962875E-03  2.830218377898949E-03  1.386060951520790E-03
   // 2.912058079231035E-02  5.042072264848452E+00  2.998332817062327E-04
// $$EOE
// *******************************************************************************
// Coordinate system description:

  // Earth Mean Equator and Equinox of Reference Epoch

    // Reference epoch: J2000.0
    // xy-plane: plane of the Earth's mean equator at the reference epoch
    // x-axis  : out along ascending node of instantaneous plane of the Earth's
              // orbit and the Earth's mean equator at the reference epoch
    // z-axis  : along the Earth mean north pole at the reference epoch

// Symbol meaning [1 AU=149597870.691 km, 1 day=86400.0 s]:

    // JDCT     Epoch Julian Date, Coordinate Time
      // X      x-component of position vector (AU)                               
      // Y      y-component of position vector (AU)                               
      // Z      z-component of position vector (AU)                               
      // VX     x-component of velocity vector (AU/day)                           
      // VY     y-component of velocity vector (AU/day)                           
      // VZ     z-component of velocity vector (AU/day)                           
      // LT     One-way down-leg Newtonian light-time (day)                       
      // RG     Range; distance from coordinate center (AU)                       
      // RR     Range-rate; radial velocity wrt coord. center (AU/day)            

// Geometric states/elements have no aberration corrections applied.

 // Computations by ...
     // Solar System Dynamics Group, Horizons On-Line Ephemeris System
     // 4800 Oak Grove Drive, Jet Propulsion Laboratory
     // Pasadena, CA  91109   USA
     // Information: http://ssd.jpl.nasa.gov/
     // Connect    : telnet://ssd.jpl.nasa.gov:6775  (via browser)
                  // telnet ssd.jpl.nasa.gov 6775    (via command-line)
     // Author     : Jon.Giorgini@jpl.nasa.gov
// *******************************************************************************

// Inputs
// Dates (TDB time scale)
inputs.body = "Jupiter";
inputs.cjd = CL_dat_cal2cjd(2012,10,31,08,15,00) : CL_dat_cal2cjd(2012,11,1,08,15,00);
inputs.orig = "solar-sys-bary";
inputs.tt_tref = 0;

// [outputs.pos, outputs.vel] = CL_eph_de405(inputs.body,inputs.cjd ,inputs.orig,inputs.tt_tref);

// Outputs
// Convert from AU to m
outputs.pos_ref = [[1.869240967874877E+00;  4.320107525040921E+00;  1.806122949755814E+00], ...
                   [1.862139239827985E+00;  4.322942726802247E+00;  1.807511094359977E+00]] * 149597870691;
// Convert from AU/day to m/s
outputs.vel_ref = [[-7.099574459128299E-03;  2.840184349473858E-03;  1.390227846351940E-03], ...
                   [-7.103878776962875E-03;  2.830218377898949E-03;  1.386060951520790E-03]] * 149597870691 / 86400;

 

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_ref,vel_ref] = CL_eph_de405(inputs.body,inputs.cjd,inputs.orig,inputs.tt_tref);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_ref, outputs.pos_ref, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(vel_ref, outputs.vel_ref, relative = %t, compar_crit = "element", prec = 1e-014);
