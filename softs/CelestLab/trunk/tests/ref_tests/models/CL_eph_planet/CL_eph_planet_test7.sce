// -------------------------------------------------------
// CL_eph_planet
// Uranus
//
// The reference values are extracted from the file vsop87.chk available at the URL :
// ftp://ftp.imcce.fr/pub/ephem/planets/vsop87/
// The series used is VSOP87D (heliocentric, spherical coordinates, ecliptic of date)
//
// # Col 1 = Julian date (TDB)
// # Col 2 = longitude l (rad)
// # Col 3 = latitude b (rad)
// # Col 4 = distance r (au)
// # Col 5 = l' (rad/d)
// # Col 6 = b' (rad/d)
// # Col 7 = r' (au/d)
//
// 2451545 5.5225485803 -0.0119527838 19.9240482667 0.0001905185 -1.1886e-006 9.77914e-005 
// 2415020 4.3397761173 0.0011570307 18.992716362   0.000209977 -2.8204e-006 0.0001832534 
// 2378495 3.0388348558 0.0132392955 18.2991154397  0.0002257402 -5.801e-007 2.96138e-005 
// 2341970 1.724220472 0.0059836565 18.7966208854   0.000213355 2.5663e-006 -0.0001670846 
// 2305445 0.5223325214 -0.0089983885 19.7819882707 0.0001929802 1.9325e-006 -0.0001352303 
// 2268920 5.6817615582 -0.0129257254 20.0300462993 0.0001888747 -6.99e-007 5.28369e-005 
// 2232395 4.5254482963 -0.001930334 19.2694311058  0.0002036904 -2.7058e-006 0.0001837904 
// 2195870 3.255722172 0.0120919639 18.3948228639   0.0002229616 -1.3148e-006 8.16221e-005 
// 2159345 1.9333853935 0.0088045918 18.5841501334  0.0002185738 2.2193e-006 -0.0001379361 
// 2122820 0.7007226224 -0.0065610611 19.5612078271 0.0001978055 2.3223e-006 -0.0001607791
//
// NB : The reference values were written using %.10f (poor precision)
//      So the number of significant digits in the reference results depend on the absolute value
//      of the result. 
//      For position there is about 10 significant digits (avg value close to 1)
//      For velocity there is about 7 significant digits  (avg value close to 0.0002)
//      --> The test will be done at a relative precision of 10-6
// ------------------------------
// -------------------------------------------------------

RELATIVE_PRECISION = 1e-6;
COMPAR_CRIT = "norm";
 
// Inputs
inputs.planet = "Uranus";
jd = [2451545:-36525:2122820];
inputs.cjd = CL_dat_convert("jd","cjd",jd);
inputs.tt_tref = 0;
inputs.accuracy = "full";

// Outputs
pos_sph = [ 5.5225485803 -0.0119527838 19.9240482667
            4.3397761173 0.0011570307 18.992716362  
            3.0388348558 0.0132392955 18.2991154397 
            1.724220472 0.0059836565 18.7966208854  
            0.5223325214 -0.0089983885 19.7819882707
            5.6817615582 -0.0129257254 20.0300462993
            4.5254482963 -0.001930334 19.2694311058 
            3.255722172 0.0120919639 18.3948228639  
            1.9333853935 0.0088045918 18.5841501334 
            0.7007226224 -0.0065610611 19.5612078271]';
vel_sph = [ 0.0001905185 -1.1886e-006 9.77914e-005  
            0.000209977 -2.8204e-006 0.0001832534  
            0.0002257402 -5.801e-007 2.96138e-005 
            0.000213355 2.5663e-006 -0.0001670846 
            0.0001929802 1.9325e-006 -0.0001352303  
            0.0001888747 -6.99e-007 5.28369e-005 
            0.0002036904 -2.7058e-006 0.0001837904  
            0.0002229616 -1.3148e-006 8.16221e-005  
            0.0002185738 2.2193e-006 -0.0001379361  
            0.0001978055 2.3223e-006 -0.0001607791]';

[pos,vel] = CL_co_sph2car(pos_sph,vel_sph);

outputs.pos = %CL_au * pos;
outputs.vel = %CL_au /86400 * vel;
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos,vel] = CL_eph_planet(inputs.planet,inputs.cjd,inputs.tt_tref,inputs.accuracy);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos, outputs.pos, relative = %t, compar_crit = "norm", prec = 1e-006);

TEST_OK($+1) = CL__isEqual(vel, outputs.vel, relative = %t, compar_crit = "norm", prec = 1e-006);
