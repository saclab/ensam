// -------------------------------------------------------
// CL_eph_planet
// Venus
//
// The reference values are extracted from the file vsop87.chk available at the URL :
// ftp://ftp.imcce.fr/pub/ephem/planets/vsop87/
// The series used is VSOP87D (heliocentric, spherical coordinates, ecliptic of date)
//
// # Col 1 = Julian date (TDB)
// # Col 2 = longitude l (rad)
// # Col 3 = latitude b (rad)
// # Col 4 = distance r (au)
// # Col 5 = l' (rad/d)
// # Col 6 = b' (rad/d)
// # Col 7 = r' (au/d)
//
// 2451545 3.1870221833 0.0569782849 0.7202129253  0.0282472304 -0.0004582174 0.0001057609 
// 2415020 5.9749622238 -0.0591260014 0.7274719359 0.027693229 -9.81975e-005 -7.38187e-005 
// 2378495 2.5083656668 0.0552309407 0.7185473298  0.0283727833 0.0006052624 3.57622e-005 
// 2341970 5.3115708036 -0.0455979904 0.7283407528 0.0275882476 -0.0010411153 6.6108e-006 
// 2305445 1.8291359617 0.0311394084 0.7186375037  0.0283070593 0.0014252119 -5.05361e-005 
// 2268920 4.6495448744 -0.0145437542 0.7273363753 0.0276128079 -0.0015850133 8.7953e-005 
// 2232395 1.1527504143 -0.0054100666 0.7205428514 0.0281308261 0.0016585927 -0.0001198485 
// 2195870 3.9850309909 0.0222342485 0.7247441174  0.0278186029 -0.0015253233 0.0001384965 
// 2159345 0.4804699931 -0.039550525 0.7235430458  0.0279409153 0.0012274064 -0.0001442265 
// 2122820 3.3145399295 0.0505016053 0.7215819783  0.0281206788 -0.0008621804 0.000137335
//
// NB : The reference values were written using %.10f (poor precision)
//      So the number of significant digits in the reference results depend on the absolute value
//      of the result. 
//      For position there is about 10 significant digits (avg value close to 1)
//      For velocity there is about 8 significant digits  (avg value close to 0.01)
//      --> The test will be done at a relative precision of 10-8
// ------------------------------
// -------------------------------------------------------

RELATIVE_PRECISION = 1e-8;
COMPAR_CRIT = "norm";
 
// Inputs
inputs.planet = "Venus";
jd = [2451545:-36525:2122820];
inputs.cjd = CL_dat_convert("jd","cjd",jd);
inputs.tt_tref = 0;
inputs.accuracy = "full";

// Outputs
pos_sph = [ 3.1870221833 0.0569782849 0.7202129253 
            5.9749622238 -0.0591260014 0.7274719359
            2.5083656668 0.0552309407 0.7185473298 
            5.3115708036 -0.0455979904 0.7283407528
            1.8291359617 0.0311394084 0.7186375037 
            4.6495448744 -0.0145437542 0.7273363753
            1.1527504143 -0.0054100666 0.7205428514
            3.9850309909 0.0222342485 0.7247441174 
            0.4804699931 -0.039550525 0.7235430458 
            3.3145399295 0.0505016053 0.7215819783 ]';
vel_sph = [ 0.0282472304 -0.0004582174 0.0001057609 
            0.027693229 -9.81975e-005 -7.38187e-005 
            0.0283727833 0.0006052624 3.57622e-005 
            0.0275882476 -0.0010411153 6.6108e-006 
            0.0283070593 0.0014252119 -5.05361e-005 
            0.0276128079 -0.0015850133 8.7953e-005 
            0.0281308261 0.0016585927 -0.0001198485 
            0.0278186029 -0.0015253233 0.0001384965 
            0.0279409153 0.0012274064 -0.0001442265 
            0.0281206788 -0.0008621804 0.000137335]';

[pos,vel] = CL_co_sph2car(pos_sph,vel_sph);

outputs.pos = %CL_au * pos;
outputs.vel = %CL_au /86400 * vel;
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos,vel] = CL_eph_planet(inputs.planet,inputs.cjd,inputs.tt_tref,inputs.accuracy);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos, outputs.pos, relative = %t, compar_crit = "norm", prec = 1e-008);

TEST_OK($+1) = CL__isEqual(vel, outputs.vel, relative = %t, compar_crit = "norm", prec = 1e-008);
