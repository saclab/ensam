// ------------------------------
// CL__iers_obl2006
// validation:
// SOFA function: obl06
// ------------------------------


// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();
outputs.epsa = [0.40931823759025809,0.40897906711542625];
TEST_OK = [];

//=========================
// Compute output
//=========================
[epsa] = CL__iers_obli2006(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(epsa, outputs.epsa, relative = %t, compar_crit = "element", prec = 1e-014);
