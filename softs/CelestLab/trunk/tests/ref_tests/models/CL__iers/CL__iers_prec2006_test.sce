// ------------------------------
// CL__iers_prec2006
// validation:
// SOFA function: pfw06
// ------------------------------


// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();
outputs.gamb = [-4.8751786173216623e-005,2.5930121807265281e-005];
outputs.phib = [0.409318399222864,0.40897922389843872];
outputs.psib = [-0.024266837219434914,0.01221519647222835];
outputs.epsa = [0.40931823759025809,0.40897906711542625];
TEST_OK = [];

//=========================
// Compute output
//=========================
[gamb,phib,psib,epsa] = CL__iers_prec2006(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(gamb, outputs.gamb, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(phib, outputs.phib, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(psib, outputs.psib, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(epsa, outputs.epsa, relative = %t, compar_crit = "element", prec = 1e-014);
