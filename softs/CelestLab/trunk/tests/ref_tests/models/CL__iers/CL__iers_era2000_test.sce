// ------------------------------
// CL__iers_era2000
// validation:
// SOFA function: era00
// ------------------------------

RELATIVE_PRECISION = 1e-11;

// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();
outputs.era = [2.5613897762297668,0.67990267714178998];
TEST_OK = [];

//=========================
// Compute output
//=========================
[era] = CL__iers_era2000(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(era, outputs.era, relative = %t, compar_crit = "element", prec = 1e-011);
