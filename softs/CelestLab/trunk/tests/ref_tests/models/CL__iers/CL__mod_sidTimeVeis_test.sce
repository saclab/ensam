// ------------------------------
// CL__mod_sidTimeVeis
// validation:
// Old CelestLab function: CL_mod_sidTimeG50
// NB: Relative precision of only 1e-8 because old function 
// took the date in one part (loss of precision)
// ------------------------------


RELATIVE_PRECISION = 1e-8;

// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();
outputs.sidt = [2.5502080881706206,0.66872090534286599];
outputs.sidtdot = [7.2921151467052092e-005,7.2921151467052092e-005];
TEST_OK = [];

//=========================
// Compute output
//=========================
[sidt,sidtdot] = CL__mod_sidTimeVeis(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(sidt, outputs.sidt, relative = %t, compar_crit = "element", prec = 1e-008);

TEST_OK($+1) = CL__isEqual(sidtdot, outputs.sidtdot, relative = %t, compar_crit = "element", prec = 1e-008);
