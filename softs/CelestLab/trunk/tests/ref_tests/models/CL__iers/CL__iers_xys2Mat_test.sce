// ------------------------------
// CL__iers_xys2Mat
// validation:
// SOFA function: c2ixys
// ------------------------------


RELATIVE_PRECISION = 1e-13;

// Inputs
inputs = struct();
inputs.x = 0.0075565;
inputs.y = 0.0021979;
inputs.s = 0.0054685;

// Outputs
outputs = struct();
outputs.M = [0.99995654284193436 -0.0054767637560155236 -0.0075443678573883944
0.0054600123952973645 0.99998258699556908 -0.0022391896508346192
0.0075565000000000007 0.0021979000000000005 0.99996903379221702];
TEST_OK = [];

//=========================
// Compute output
//=========================
[M] = CL__iers_xys2Mat(inputs.x,inputs.y,inputs.s);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M, outputs.M, relative = %t, compar_crit = "element", prec = 1e-013);
