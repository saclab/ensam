// ------------------------------
// CL__iers_bias2006
// validation:
// SOFA function: bi06
// ------------------------------


// Inputs
inputs = struct();
// No inputs!

// Outputs
outputs = struct();
outputs.M = [0.99999999999999412 -7.0783689609715561e-008 8.0562139776131861e-008
            7.0783686946376763e-008 0.99999999999999689 3.3059437354321375e-008
            -8.0562142116200575e-008 -3.3059431692183949e-008 0.99999999999999623];
TEST_OK = [];

//=========================
// Compute output
//=========================
[M] = CL__iers_bias2006();

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M, outputs.M, relative = %t, compar_crit = "element", prec = 1e-014);
