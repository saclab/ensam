// ------------------------------
// CL__iers_fundArgsLS
// validation:
// SOFA functions: fal03, falp03, faf03, fad03, faom03
// ------------------------------

RELATIVE_PRECISION = 2.e-12;
// Because SOFA does the modulo and conversion to radians 
// in another order resulting in numerical errors

// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();

// SOFA uses a modulo, we use CL_rMod (same as pmodulo)      
outputs.Fls = CL_rMod( [  -5.4455870141660121 0.91075858539074006
                          -2.3704826509172605 6.2288529750866006
                          -3.4771275761021005 2.3044502206560384
                          -1.7030706058179961 1.559225439982481
                           4.3119483279254691 -2.1295472901258319] , 2*%pi);
TEST_OK = [];

//=========================
// Compute output
//=========================
[Fls] = CL__iers_fundArgsLS(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(Fls, outputs.Fls, relative = %t, compar_crit = "element", prec = 2e-012);
