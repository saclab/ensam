// ------------------------------
// CL_mod_precessionAngles
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
jj_tai = [19500];
ss_tai = 43200*ones(jj_tai);
inputs.jj_tai = [jj_tai jj_tai];
inputs.ss_tai = [ss_tai ss_tai];
inputs.flag = "s";

// Outputs
outputs = struct();
outputs.K = [[0.00037897252437795468;0.00032935735176035843;0.00037897694011716174],[0.00037897252437795468;0.00032935735176035843;..
0.00037897694011716174]];
outputs.KP = [[3.5430325433573939e-012;3.0791418032661154e-012;3.543115109272191e-012],[3.5430325433573939e-012;3.0791418032661154e-012;..
3.543115109272191e-012]];
outputs.KPP = [[2.9570329954142225e-025;-4.1954368709651254e-025;1.067622757794236e-024],[2.9570329954142225e-025;-4.1954368709651254e-025;..
1.067622757794236e-024]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[K,KP,KPP] = CL_mod_precessionAngles(inputs.jj_tai,inputs.ss_tai,inputs.flag);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(K, outputs.K, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(KP, outputs.KP, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(KPP, outputs.KPP, relative = %t, compar_crit = "element", prec = 1e-014);
