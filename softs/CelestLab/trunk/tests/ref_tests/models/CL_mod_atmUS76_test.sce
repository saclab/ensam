// ------------------------------
// CL_mod_atmUS76
// validation:
// CNES - MSLIB FORTRAN 90, Volume P (mp_atm_US76)
// ------------------------------

// Inputs
inputs = struct();
rayon0 = 6356.766e3;
hpot=19.9e3;
inputs.alt=zeros(1,12);
inputs.alt(1)=120e3;
inputs.alt(2)=86e3;
inputs.alt(3)=80e3;
inputs.alt(4)=(rayon0 * hpot)/(rayon0 - hpot);
inputs.alt(5)=86.5e3;
inputs.alt(6)=100e3;
inputs.alt(7)=115e3;
inputs.alt(8)=-2e3; // out of bounds
inputs.alt(9)=0;
inputs.alt(10)=80e3;
inputs.alt(11)=87501;
inputs.alt(12)=87689;

// Outputs
outputs = struct();
outputs.temp = [360, 186.86720408278993, 198.63857625086885, 216.65000000000001,..
   186.8673, 195.08134433524688, 300, %nan,..
   288.14999999999998, 198.63857625086885, 186.8673, 186.8673];
outputs.pres = [0.0025383254142072962, 0.37337853396939097, 1.0524681108564236, 5561.889873648347,..
   0.34172757377085272, 0.032012784615402252, 0.0040097093277676804, %nan, ..
   101325, 1.0524681108564236, 0.28597697625865859, 0.27683902204123018];
outputs.dens = [2.222262383594059e-008, 6.9577878562557155e-006, 1.845793673317716e-005, 0.089433760661706871,..
   6.3674230390698722e-006, 5.6043936916499593e-007, 4.2887783366702368e-008, %nan,..
   1.2249991558877122, 1.845793673317716e-005, 5.3275374430529715e-006, 5.1570150438800049e-006];

TEST_OK = [];

//=========================
// Compute output
//=========================
[temp,pres,dens] = CL_mod_atmUS76(inputs.alt);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(temp, outputs.temp, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(pres, outputs.pres, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(dens, outputs.dens, relative = %t, compar_crit = "element", prec = 1e-014);
