// ------------------------------
// CL_mod_sidTimeG50
// validation:
// CNES - MSLIB FORTRAN 90, Volume R (mr_tsid_veis)
// ------------------------------

// Inputs
inputs = struct();
ut1_utc = 0.01;
inputs.cjd_ut1 = [215678,215679] + ut1_utc/86400;

// Outputs
outputs = struct();
outputs.tsid = [4.7990033015230438,4.8162054810968584];

TEST_OK = [];

//=========================
// Compute output
//=========================
[tsid] = CL_mod_sidTimeG50(inputs.cjd_ut1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(tsid, outputs.tsid, relative = %t, compar_crit = "element", prec = 1e-014);
