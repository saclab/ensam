// ------------------------------
// CL_mod_planetsG50ecl
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = 25463.325;
inputs.planet_name = "Mercury";

// Outputs
outputs = struct();
outputs.pars = [57909126310.951019;0.20563866064999825;0.12226174480992537;0.50958738616525967;0.83062787548981476;2.1463849085117341]
TEST_OK = [];

//=========================
// Compute output
//=========================
[pars] = CL_mod_planetsG50ecl(inputs.cjd,inputs.planet_name);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pars, outputs.pars, relative = %t, compar_crit = "element", prec = 1e-014);
