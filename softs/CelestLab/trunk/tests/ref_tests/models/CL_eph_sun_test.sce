// ------------------------------
// CL_eph_sun (Meeus only)
// validation:
// Exemple 25.a, page 165, Astronomical Algorithms. J.Meeus - Second Edition
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = CL_dat_cal2cjd(1992,10,13,0,0,0);
inputs.tt_tref = 0;
inputs.frame = "EOD";
inputs.accuracy = "med"; // Corresponds to the full MEEUS model.

// Outputs
outputs = struct();
// Values from the book:
// lon = 199.90988 deg
// lat = 0 deg
// dist = 0.99766 au
outputs.pos_sun = CL_co_sph2car([199.9098726633354 * %CL_deg2rad; 
                                  0 ;
                                  0.997661950005629 * %CL_au]);
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_sun] = CL_eph_sun(inputs.cjd,inputs.tt_tref,inputs.frame,inputs.accuracy);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_sun, outputs.pos_sun, relative = %t, compar_crit = "element", prec = 1e-014);
