// ------------------------------
// CL_mod_equinoxesEquation
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.dPsi = [-7.4284293949388277e-005,-3.3375863488615664e-006];
inputs.eps0 = [0.40908511131303982,0.40907889732985903];
inputs.F5 = [1.0382576145545936,0.11404019731143045];
inputs.dPsip = [-2.4406169371758244e-012,-6.9235201002077694e-012];
inputs.eps0p = [-7.1921086654825995e-014,-7.1921114515378505e-014];
inputs.F5p = [-1.0696961162052566e-008,-1.0696960533382173e-008];

// Outputs
outputs = struct();
outputs.equi = [-6.8143440628371153e-005,-3.0606685595697997e-006];
outputs.equip = [-2.2392982011281994e-012,-6.352386280145059e-012];

TEST_OK = [];

//=========================
// Compute output
//=========================
[equi,equip] = CL_mod_equinoxesEquation(inputs.dPsi,inputs.eps0,inputs.F5,inputs.dPsip,inputs.eps0p,inputs.F5p);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(equi, outputs.equi, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(equip, outputs.equip, relative = %t, compar_crit = "element", prec = 1e-014);
