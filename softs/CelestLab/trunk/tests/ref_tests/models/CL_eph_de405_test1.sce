// -------------------------------------------------------
// CL_ephemJPL_getPVA
// -------------------------------------------------------

// Reference values obtained with Horizons website: http://ssd.jpl.nasa.gov/horizons.cgi#top


// *******************************************************************************
// Ephemeris / WWW_USER Wed Oct 31 03:03:41 2012 Pasadena, USA      / Horizons    
// *******************************************************************************
// Target body name: Mercury (199)                   {source: DE405}
// Center body name: Moon (301)                      {source: DE405}
// Center-site name: BODY CENTER
// *******************************************************************************
// Start time      : A.D. 2012-Oct-31 08:15:00.0000 CT 
// Stop  time      : A.D. 2012-Nov-01 08:15:00.0000 CT 
// Step-size       : 1440 minutes
// *******************************************************************************
// Center geodetic : 0.00000000,0.00000000,0.0000000 {E-lon(deg),Lat(deg),Alt(km)}
// Center cylindric: 0.00000000,0.00000000,0.0000000 {E-lon(deg),Dxy(km),Dz(km)}
// Center radii    : 1737.4 x 1737.4 x 1737.4 km     {Equator, meridian, pole}    
// Output units    : AU-D                                                         
// Output format   : 03
// Reference frame : ICRF/J2000.0                                                 
// Output type     : GEOMETRIC cartesian states
// Coordinate systm: Earth Mean Equator and Equinox of Reference Epoch            
// *******************************************************************************
// JDCT 
   // X     Y     Z
   // VX    VY    VZ
   // LT    RG    RR
// *******************************************************************************
// $$SOE
// 2456231.843750000 = A.D. 2012-Oct-31 08:15:00.0000 (CT)
  // -4.444125970479167E-01 -7.308036544011023E-01 -3.685995177038699E-01
   // 2.026456392964195E-02  1.016447490465244E-02  5.837217801886677E-03
   // 5.379119905889694E-03  9.313657402719973E-01 -1.995527401564514E-02
// 2456232.843750000 = A.D. 2012-Nov-01 08:15:00.0000 (CT)
  // -4.248200321163395E-01 -7.201245095896887E-01 -3.624159831670736E-01
   // 1.889812566840618E-02  1.118298859423938E-02  6.525279397466040E-03
   // 5.263004938634012E-03  9.112610569173214E-01 -2.024262085909678E-02
// $$EOE
// *******************************************************************************
// Coordinate system description:

  // Earth Mean Equator and Equinox of Reference Epoch

    // Reference epoch: J2000.0
    // xy-plane: plane of the Earth's mean equator at the reference epoch
    // x-axis  : out along ascending node of instantaneous plane of the Earth's
              // orbit and the Earth's mean equator at the reference epoch
    // z-axis  : along the Earth mean north pole at the reference epoch

// Symbol meaning [1 AU=149597870.691 km, 1 day=86400.0 s]:

    // JDCT     Epoch Julian Date, Coordinate Time
      // X      x-component of position vector (AU)                               
      // Y      y-component of position vector (AU)                               
      // Z      z-component of position vector (AU)                               
      // VX     x-component of velocity vector (AU/day)                           
      // VY     y-component of velocity vector (AU/day)                           
      // VZ     z-component of velocity vector (AU/day)                           
      // LT     One-way down-leg Newtonian light-time (day)                       
      // RG     Range; distance from coordinate center (AU)                       
      // RR     Range-rate; radial velocity wrt coord. center (AU/day)            

// Geometric states/elements have no aberration corrections applied.

 // Computations by ...
     // Solar System Dynamics Group, Horizons On-Line Ephemeris System
     // 4800 Oak Grove Drive, Jet Propulsion Laboratory
     // Pasadena, CA  91109   USA
     // Information: http://ssd.jpl.nasa.gov/
     // Connect    : telnet://ssd.jpl.nasa.gov:6775  (via browser)
                  // telnet ssd.jpl.nasa.gov 6775    (via command-line)
     // Author     : Jon.Giorgini@jpl.nasa.gov
// *******************************************************************************

// Inputs
// Dates (TDB time scale)
inputs.body = "Mercury";
inputs.cjd = CL_dat_cal2cjd(2012,10,31,08,15,00) : CL_dat_cal2cjd(2012,11,1,08,15,00);
inputs.orig = "Moon";
inputs.tt_tref = 0;

// [outputs.pos, outputs.vel] = CL_eph_de405(inputs.body,inputs.cjd ,inputs.orig,inputs.tt_tref);

// Outputs
// Convert from AU to m
outputs.pos_ref = [[-4.444125970479167E-01; -7.308036544011023E-01; -3.685995177038699E-01], ...
                   [-4.248200321163395E-01; -7.201245095896887E-01; -3.624159831670736E-01]] * 149597870691;
// Convert from AU/day to m/s
outputs.vel_ref = [[2.026456392964195E-02;  1.016447490465244E-02;  5.837217801886677E-03], ...
                   [1.889812566840618E-02;  1.118298859423938E-02;  6.525279397466040E-03]] * 149597870691 / 86400;

 

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_ref,vel_ref] = CL_eph_de405(inputs.body,inputs.cjd,inputs.orig,inputs.tt_tref);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_ref, outputs.pos_ref, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(vel_ref, outputs.vel_ref, relative = %t, compar_crit = "element", prec = 1e-014);
