// ------------------------------
// CL_man_biElliptic
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.ai = 7000000;
inputs.af = 105000000;
inputs.rt = [210000000 210000000];

// Outputs
outputs = struct();
outputs.delta_v = [4028.5171688964447,4028.5171688964447];
outputs.dv1 = [[1.5707963267948966;0;2952.1419690870862],[1.5707963267948966;0;2952.1419690870862]];
outputs.dv2 = [[1.5707963267948966;0;774.95936559927827],[1.5707963267948966;0;774.95936559927827]];
outputs.dv3 = [[4.7123889803846897;0;301.41583421007977],[4.7123889803846897;0;301.41583421007977]];
outputs.anv1 = [0,0];
outputs.anv2 = [3.1415926535897931,3.1415926535897931];
outputs.anv3 = [0,0];

TEST_OK = [];

//=========================
// Compute output
//=========================
[delta_v,dv1,dv2,dv3,anv1,anv2,anv3] = CL_man_biElliptic(inputs.ai,inputs.af,inputs.rt);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(delta_v, outputs.delta_v, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(dv1, outputs.dv1, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(dv2, outputs.dv2, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(dv3, outputs.dv3, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(anv1, outputs.anv1, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(anv2, outputs.anv2, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(anv3, outputs.anv3, relative = %t, compar_crit = "element", prec = 1e-014);
