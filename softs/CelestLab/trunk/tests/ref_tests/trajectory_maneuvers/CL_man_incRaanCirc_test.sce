// ------------------------------
// CL_man_incRaanCirc
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.sma = 7200.e3;
inputs.inci = CL_deg2rad(111);
inputs.raani = CL_deg2rad(12);
inputs.incf = CL_deg2rad(108);
inputs.raanf = CL_deg2rad(15);

// Outputs
outputs = struct();
outputs.dv = [4.7123889803847749;1.5348213286017791;535.22912080807987];
outputs.arg_lat = [2.3767970699565604];

TEST_OK = [];

//=========================
// Compute output
//=========================
[dv,arg_lat] = CL_man_incRaanCirc(inputs.sma,inputs.inci,inputs.raani,inputs.incf,inputs.raanf);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(dv, outputs.dv, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(arg_lat, outputs.arg_lat, relative = %t, compar_crit = "element", prec = 1e-014);
