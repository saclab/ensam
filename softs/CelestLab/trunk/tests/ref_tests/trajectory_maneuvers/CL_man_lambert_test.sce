// ------------------------------
// CL_man_lambert
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.r1 = [ 5000.e3 10000.e3 2100.e3]';
inputs.r2 = [-14600.e3 2500.e3 7000.e3]';
inputs.dt = 3600; //one hour
inputs.sense = 'pro';
inputs.mu = 398600*1000^3;

// Outputs
outputs = struct();
outputs.v1 = [-5992.4946396663972;1925.363415280892;3245.6365284904896];
outputs.v2 = [-3312.4603109367936;-4196.617307926469;-385.28761706810468];

TEST_OK = [];

//=========================
// Compute output
//=========================
[v1,v2] = CL_man_lambert(inputs.r1,inputs.r2,inputs.dt,inputs.sense,inputs.mu);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(v1, outputs.v1, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(v2, outputs.v2, relative = %t, compar_crit = "element", prec = 1e-014);
