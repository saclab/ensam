// ------------------------------
// CL_man_applyDv
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
anm = 4.7323886470463554;
inputs.kep=[0.7e7;0.1e-1;CL_deg2rad(0.3e2);CL_deg2rad(0.9e2);0;anm]
inputs.dv_local=[CL_deg2rad(0.27e3);CL_deg2rad(0.875e2);0.65834152e3]

// Outputs
outputs = struct();
outputs.kep_dv = [7000000.0128901629;0.010000000009206511;0.61086525928398339;1.5707961426681276;0;4.7323888311639175];

TEST_OK = [];

//=========================
// Compute output
//=========================
[kep_dv] = CL_man_applyDv(inputs.kep,inputs.dv_local);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(kep_dv, outputs.kep_dv, relative = %t, compar_crit = "element", prec = 1e-014);
