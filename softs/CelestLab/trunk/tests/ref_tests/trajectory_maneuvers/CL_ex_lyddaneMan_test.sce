// ------------------------------
// CL_ex_lyddaneMan
// validation:
// 
// ------------------------------

// NB: Correction of MSLIB code (osculating term due to J5)
//     ==> relative precision of 10^-9
RELATIVE_PRECISION = 1e-8

// Inputs
inputs = struct();
inputs.t1 = [12584]
inputs.mean_t1 = [42166712;0.0079762146410437083;0.0016690118773186888;3.7716923170037111;5.514734673588543;2.296758316587332];
inputs.t2 = [12590];
inputs.tman = [12587 12589.5];
inputs.dvman = [1 1;1 0;1 1];
inputs.er = 6378136.3;
inputs.mu = 3.986004415e14;
inputs.j1jn = [0.
          0.00108262669059782
          -2.53243547943241e-06
          -1.61933127174051e-06
          -2.27716112895682e-07
          5.39648498594285e-07];

// Outputs
outputs = struct();
outputs.mean_t2 = [42221633.760800488;0.0079810177253720974;0.001672139417420357;3.7759965946441327;5.5122280116295483;8.6579322091664626];
outputs.osc_t2 = [42221615.792364992;0.0079542167280923174;0.0016730011425612347;3.7798253832316222;5.5115475040393056;8.6547846357088059];

TEST_OK = [];

//=========================
// Compute output
//=========================
[mean_t2,osc_t2] = CL_ex_lyddaneMan(inputs.t1,inputs.mean_t1,inputs.t2,inputs.tman,inputs.dvman,inputs.er,inputs.mu,inputs.j1jn);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(mean_t2, outputs.mean_t2, relative = %t, compar_crit = "element", prec = 1e-008);

TEST_OK($+1) = CL__isEqual(osc_t2, outputs.osc_t2, relative = %t, compar_crit = "element", prec = 1e-008);
