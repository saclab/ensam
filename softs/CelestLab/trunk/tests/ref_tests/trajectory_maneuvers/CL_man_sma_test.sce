// ------------------------------
// CL_man_sma
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.a1 = 0.7e7;
inputs.e1 = 0.1;
inputs.a2 = 0.72e7;
inputs.posman = 1;

// Outputs
outputs = struct();
outputs.dv = [1.5707963267948966;0;114.90062256001602];
outputs.anv = [3.1415926535897931];

TEST_OK = [];

//=========================
// Compute output
//=========================
[dv,anv] = CL_man_sma(inputs.a1,inputs.e1,inputs.a2,inputs.posman);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(dv, outputs.dv, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(anv, outputs.anv, relative = %t, compar_crit = "element", prec = 1e-014);
