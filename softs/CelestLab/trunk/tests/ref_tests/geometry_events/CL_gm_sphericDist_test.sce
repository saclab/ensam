// ------------------------------
// CL_gm_sphericDist
// validation:
// manual calculation (%pi)
// ------------------------------

// Inputs
inputs = struct();
//calculation of half equator perimeter (=%pi)
inputs.p1=[0;0]
inputs.p2=[CL_deg2rad(180);0]

// Outputs
outputs = struct();
outputs.sph_dist = [3.1415926535897931];

TEST_OK = [];

//=========================
// Compute output
//=========================
[sph_dist] = CL_gm_sphericDist(inputs.p1,inputs.p2);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(sph_dist, outputs.sph_dist, relative = %t, compar_crit = "element", prec = 1e-014);
