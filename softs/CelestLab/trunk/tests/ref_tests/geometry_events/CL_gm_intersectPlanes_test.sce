// ------------------------------
// CL_gm_intersectPlanes
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.inc1 = CL_deg2rad(98) ;
inputs.raan1 = CL_deg2rad(102) ;
inputs.inc2 = CL_deg2rad(100) ;
inputs.raan2 = CL_deg2rad(126) ;

// Outputs
outputs = struct();
outputs.arg_lat_1 = [1.4546124560384552];
outputs.arg_lat_2 = [1.5211004260712828];

TEST_OK = [];

//=========================
// Compute output
//=========================
[arg_lat_1,arg_lat_2] = CL_gm_intersectPlanes(inputs.inc1,inputs.raan1,inputs.inc2,inputs.raan2);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(arg_lat_1, outputs.arg_lat_1, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(arg_lat_2, outputs.arg_lat_2, relative = %t, compar_crit = "element", prec = 1e-014);
