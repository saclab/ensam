// ------------------------------
// CL_gm_betaEclipse
// validation:
// Manual calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.sma=[%CL_eqRad+350000 %CL_eqRad+400000];
inputs.betaa=CL_deg2rad([10 20]);


// Outputs
outputs = struct();
outputs.semi_ang_ecl = [1.2416432375141413,1.2023526304118171];

TEST_OK = [];

//=========================
// Compute output
//=========================
[semi_ang_ecl] = CL_gm_betaEclipse(inputs.sma,inputs.betaa);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(semi_ang_ecl, outputs.semi_ang_ecl, relative = %t, compar_crit = "element", prec = 1e-014);
