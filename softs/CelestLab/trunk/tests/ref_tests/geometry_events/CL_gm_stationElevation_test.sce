// ------------------------------
// CL_gm_stationElevation
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.pos_sat_ter = [ 6578000:100000:6978000 ; 1:5 ; 1:5];
sta1 = [CL_deg2rad(2);CL_deg2rad(70);200]; // high latitude
sta2 = [CL_deg2rad(20);CL_deg2rad(0);400]; // equator
inputs.stations = [sta1,sta2];
inputs.er=6378136.3;
inputs.apla=1/298.25781;

// Outputs
outputs = struct();
outputs.elev = [[-0.5878891975464664;-0.087445214334618937],[-0.57710721983653857;-0.045182846230905183],[-0.56649730660599218;..
-0.0040111299394387512],[-0.55605770369199359;0.035977720636652277],[-0.54578656428685779;0.074711776789188944]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[elev] = CL_gm_stationElevation(inputs.pos_sat_ter,inputs.stations,inputs.er,inputs.apla);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(elev, outputs.elev, relative = %t, compar_crit = "element", prec = 1e-014);
