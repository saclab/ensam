// ------------------------------
// CL_gm_eclipseCir
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.sma=[1000e3 1234e3]+%CL_eqRad;
inputs.inc=CL_deg2rad(98);
inputs.raan=CL_deg2rad([0 5]);
inputs.alpha_sun = [4.9025716251555895,3.6520035631636762];
inputs.delta_sun = [-0.40260592366392439,-0.21344681020831521];

// Outputs
outputs = struct();
outputs.result = struct("start",struct("pso",[2.1563619905074658,5.5314343205682794] , ..
"ra",[-2.9347288450098006,0.21664874660794708] , ..
"decl",[0.97071378556476406,-0.74269111597570492] , ..
"tlt",[6.0637173522418237,22.877911192094508]) , ..
"end",struct("pso",[2.1563619905074658,7.37701003887689] , ..
"ra",[-2.9347288450098006,-0.17580646784385448] , ..
"decl",[0.97071378556476406,1.075322574737777] , ..
"tlt",[6.0637173522418237,21.378842696361847]) , ..
"sun_orb",struct("alpha",[-0.98523066308232754,-2.9705557810467944] , ..
"delta",[1.2506599382983081,0.44104696555090084]) , ..
"angle",[0,1.8455757183086106] , ..
"duration",[0,1941.4348175352525]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[result] = CL_gm_eclipseCir(inputs.sma,inputs.inc,inputs.raan,inputs.alpha_sun,inputs.delta_sun);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(result, outputs.result, relative = %t, compar_crit = "element", prec = 1e-014);
