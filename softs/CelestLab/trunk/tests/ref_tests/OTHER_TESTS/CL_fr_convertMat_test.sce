// -------------------
// CL_fr_convertMat
// -------------------

TEST_OK = [];



// -------------------
// -------------------
// GCRS -> CIRS,TIRS,ITRS
// Reference: SOFA DOC
// -------------------
// -------------------
// Page 18, chapitre 5.1
// (arcsec->radians)
AS2R = 4.848136811095359935899141e-6;

// Date UTC
cal = [2007;4;5;12;0;0];
cjd = CL_dat_cal2cjd(cal);

// Polar motion (arcsec->radians).
XP = 0.0349282D0 * AS2R;
YP = 0.4833163D0 * AS2R;

// UT1-UTC (s).
ut1_utc = -0.072073685;
tai_utc = 33;
tt_tai = 32.184;

// CIP offsets wrt IAU 2006/2000A (mas->radians).
DX06 = 0.1750D0 * AS2R/1000;
DY06 = -0.2259D0 * AS2R/1000;

// TREF = UTC
ut1_tref = ut1_utc;
tt_tref = tt_tai + tai_utc ;

// Reference results : chapter 5.5 page 25-26
M_GCRS_CIRS_ref = [+0.999999746339445, -0.000000005138822, -0.000712264730072; ...
                   -0.000000026475227, +0.999999999014975, -0.000044385242827; ...
                   +0.000712264729599, +0.000044385250426, +0.999999745354420]

M_GCRS_CIRS = CL_fr_convertMat("GCRS", "CIRS", cjd, ut1_tref, tt_tref, XP, YP, DX06, DY06);
[ax,ang] = CL_rot_quat2axAng(CL_rot_matrix2quat(M_GCRS_CIRS*M_GCRS_CIRS_ref'));
TEST_OK($+1) = max(ang) < 1e-12;

M_GCRS_TIRS_ref = [+0.973104317573127, +0.230363826247709, -0.000703332818845; ...
                   -0.230363798804182, +0.973104570735574, +0.000120888549586; ...
                   +0.000712264729599, +0.000044385250426, +0.999999745354420]

M_GCRS_TIRS = CL_fr_convertMat("GCRS", "TIRS", cjd, ut1_tref, tt_tref, XP, YP, DX06, DY06);
[ax,ang] = CL_rot_quat2axAng(CL_rot_matrix2quat(M_GCRS_TIRS*M_GCRS_TIRS_ref'));
TEST_OK($+1) = max(ang) < 1e-12;

M_GCRS_ITRS_ref = [+0.973104317697535, +0.230363826239128, -0.000703163482198; ...
                   -0.230363800456037, +0.973104570632801, +0.000118545366625; ...
                   +0.000711560162668, +0.000046626403995, +0.999999745754024]

M_GCRS_ITRS = CL_fr_convertMat("GCRS", "ITRS", cjd, ut1_tref, tt_tref, XP, YP, DX06, DY06);
[ax,ang] = CL_rot_quat2axAng(CL_rot_matrix2quat(M_GCRS_ITRS*M_GCRS_ITRS_ref'));
TEST_OK($+1) = max(ang) < 1e-12;








// -------------------
// -------------------
// GCRS -> ITRS (omega)
// Reference: Patrius 1.2.1 (TestIERS.java)
// -------------------

// Time scales
ut1_utc = 0.2; // s 
tai_utc = 25; // s 
tt_tai = 32.184; //

// Polar correction
xp = -1; // as
yp = 3; // as

// Length of day (not used in CelestLab)
lod = 0.0; // as

// IAU 2000 corrections
dx06 = 1; // as 
dy06 = -2; // as

// IAU 1980 corrections
dPsi = 1; // as (not used here)
dEps = -2; // as (not used here)

// NB: Patrius configured to have no tidal effects, and no libration effects

// Convert everything to radians
as2rad = CL_unitConvert("arcsec","rad", 1);
xp = xp * as2rad;
yp = yp * as2rad;

dx06 = dx06 * as2rad;
dy06 = dy06 * as2rad;
dPsi = dPsi * as2rad;
dEps = dEps * as2rad;


// Date 1990/3/5 02:05:05 (UTC)
cjd_utc = CL_dat_convert("cal", "cjd", [1990;3;5;02;05;05]);
tt_utc = tt_tai + tai_utc;
use_interp = %f;
[M, omega] = CL_fr_convertMat("GCRS", "ITRS", cjd_utc, ut1_utc, tt_utc, xp, yp, dx06, dy06, use_interp);

omega_ref = [-6.748703408576967E-8;1.660744463065529E-9;7.292112021963544E-5];

// Precision = 3e-12
TEST_OK($+1) = CL_vectAngle(omega,omega_ref) < 1e-11;




