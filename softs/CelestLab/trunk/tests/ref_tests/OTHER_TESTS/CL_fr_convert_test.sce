// -------------------
// CL_fr_convert
// -------------------

TEST_OK = [];


// -------------------
// -------------------
// G50 -> PEF
// Reference value: CL_fr_G502ter (old CelestLab function)
// -------------------
// -------------------
cjd_utc = [21515.15151211,21515.25];
ut1_utc = [0.1212, 0];

pos_G50 = [2000;3000;4000];
vel_G50 = [2000;3000;4000];
pos_PEF_ref = [[1585.760725239517; -3238.111011421295; 4000], [-592.6289738264634; -3556.513868858294; 4000]];
vel_PEF_ref = [[1585.524598455986; -3238.226646919331; 4000], [-592.8883189129891; -3556.47065367113; 4000]];

[pos_PEF, vel_PEF] = CL_fr_convert("Veis", "PEF", cjd_utc, pos_G50, vel_G50, ut1_utc, 0);

ang = CL_vectAngle(pos_PEF,pos_PEF_ref);
TEST_OK($+1) = max(ang) < 1e-11;

ang = CL_vectAngle(vel_PEF,vel_PEF_ref);
TEST_OK($+1) = max(ang) < 1e-11;





