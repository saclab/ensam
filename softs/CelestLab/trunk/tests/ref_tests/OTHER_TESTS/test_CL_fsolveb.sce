// ---------------------------------------------------------
// CL_fsolveb
// Test with reference results = analytical solution of the equation
// ---------------------------------------------------------

TEST_OK = [];

// Test 1: solves e^x=1 
function [y] = fct1(x, ind, args);
  y = %e.^x-1;
endfunction

sol = CL_fsolveb(fct1, -1, 1, ytol=1e-8);
solref = 0;
TEST_OK($+1) = CL__isEqual(sol,solref,1e-8);


// Test 2: solves sin(x) = 0.5 
function [y] = fct2(x, ind, args)
  y = sin(x) - 0.5;
endfunction

[sol, y] = CL_fsolveb(fct2, -1, 1,ytol=1.e-14);
solref = %pi/6;
TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);


// Test 3: solves sin(x)+e^(x-pi)=0 
function [y] = fct3(x, ind, args)
  y = sin(x) +%e^(x-%pi)-1;
endfunction

[sol, y] = CL_fsolveb(fct3, 1, %pi,ytol=1.e-14); 
solref = %pi;
TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);
