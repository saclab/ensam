// ------------------------------
// CL_op_driftJ2
// validation:
// CNES - MSLIB FORTRAN 90, Volume E (me_deriv_secul_J2)
// ------------------------------

// Inputs
inputs = struct();
inputs.sma = 24435100;
inputs.ecc = 0.73054;
inputs.iinc = 1.570796;

// Optional inputs (names of arguments have to strictly match names of function arguments !)
inputs_opt = struct();
inputs_opt.er = 6378160;
inputs_opt.j2 = 0.10829e-2;

// Outputs
outputs = struct();
outputs.drpom = [-4.2063751820618439e-008];
outputs.drgom = [-2.7492438848464542e-014];
outputs.drM = [0.000165261704849466];
outputs.drpomdaei = [6.025067684280586e-015;-2.6359432166011432e-007;-1.3746219424231535e-013];
outputs.drgomdaei = [3.9379227410416119e-021;-1.7228255815000762e-013;8.412750364127731e-008];
outputs.drMdaei = [-1.0142586244120258e-011;-1.3500047222322542e-007;-5.6321277389208248e-014];

TEST_OK = [];

//=========================
// Compute output
//=========================
[drpom,drgom,drM,drpomdaei,drgomdaei,drMdaei] = CL_op_driftJ2(inputs.sma,inputs.ecc,inputs.iinc,er=inputs_opt.er,j2=inputs_opt.j2);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(drpom, outputs.drpom, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(drgom, outputs.drgom, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(drM, outputs.drM, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(drpomdaei, outputs.drpomdaei, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(drgomdaei, outputs.drgomdaei, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(drMdaei, outputs.drMdaei, relative = %t, compar_crit = "element", prec = 1e-014);
