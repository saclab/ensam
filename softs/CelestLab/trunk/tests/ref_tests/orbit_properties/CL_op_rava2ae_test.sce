// ------------------------------
// CL_op_rava2ae
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.ra = 8008e3;
inputs.va = 7051.6331;

// Outputs
outputs = struct();
outputs.sma = [8000003.973798804];
outputs.ecc = [0.00099950277867161936];

TEST_OK = [];

//=========================
// Compute output
//=========================
[sma,ecc] = CL_op_rava2ae(inputs.ra,inputs.va);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(sma, outputs.sma, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(ecc, outputs.ecc, relative = %t, compar_crit = "element", prec = 1e-014);
