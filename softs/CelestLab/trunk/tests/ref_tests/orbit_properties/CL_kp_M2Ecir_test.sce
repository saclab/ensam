// ------------------------------
// CL_kp_M2Ecir
// validation:
// CNES - MSLIB FORTRAN 90, Volume V (mv_kepler_gene)
// ------------------------------

// Inputs
inputs = struct();
//standard case
ex = 0.005;
ey = 0.005;
pso_M = 1.2566371;
//excentricity near 0
excent = 0.5;
ex(2) = 1.e-12;
ey(2) = 1.e-12;
pso_M(2) = excent - ex(2)*sin(excent) + ey(2)*cos(excent);
//sin(pso_M) near 0 (odd pi multiple)
excent = 1.e-14 + 3*%pi + 0.6;
ex(3) = 0.5*cos(0.6);
ey(3) = 0.5*sin(0.6);
pso_M(3) = excent - ex(3)*sin(excent) + ey(3)*cos(excent);
//sin(pso_M) near 0 (par pi multiple)
excent = 1.e-14 + 2*%pi + 0.6;
ex(4) = 0.5*cos(0.6);
ey(4) = 0.5*sin(0.6);
pso_M(4) = excent - ex(4)*sin(excent) + ey(4)*cos(excent);
//iteratif computation
excent = 0.5;
ex(5) = 0.5*cos(0.6);
ey(5) = 0.5*sin(0.6);
pso_M(5) = excent - ex(5)*sin(excent) + ey(5)*cos(excent);
ex = ex';
ey = ey';
pso_M = pso_M';
// previous bug case :
ex(6) = 0.01;
ey(6) = 0;
pso_M(6) = 0;

inputs.ex = ex;
inputs.ey = ey;
inputs.pso_M = pso_M;

// Outputs
outputs = struct();
outputs.pso_E = [1.2598676346209059,0.49999999999999994,10.02477796076939,6.8831853071795956,0.50000000000000011,0];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pso_E] = CL_kp_M2Ecir(inputs.ex,inputs.ey,inputs.pso_M);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pso_E, outputs.pso_E, relative = %t, compar_crit = "element", prec = 1e-014);
