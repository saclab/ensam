// ------------------------------
// CL_kp_M2E
// validation:
// CNES - MSLIB FORTRAN 90, Volume V (mvi_kepler_hyperb, routine interne)
// ------------------------------

// Inputs
inputs = struct();
inputs.e = [0.7311 0.03 0.03 1.06 1.03 1.06];
inputs.M = [0.048363 0.9 -0.2 0.9 1.3 1.3];

// Outputs
outputs = struct();
outputs.E = [0.17733198875543585,0.9239394613607278,-0.20614050967371411,1.5894688498897291,1.8309419578271515,1.7918003803114042];

TEST_OK = [];

//=========================
// Compute output
//=========================
[E] = CL_kp_M2E(inputs.e,inputs.M);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(E, outputs.E, relative = %t, compar_crit = "element", prec = 1e-014);
