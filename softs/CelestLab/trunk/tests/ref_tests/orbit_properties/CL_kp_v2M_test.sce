// ------------------------------
// CL_kp_v2M
// validation:
// ---------
// ------------------------------

// Inputs
inputs = struct();
inputs.ecc = 0.1;
inputs.v = 0.2;

// Outputs
outputs = struct();
outputs.M = [0.1630134065847178];

TEST_OK = [];

//=========================
// Compute output
//=========================
[M] = CL_kp_v2M(inputs.ecc,inputs.v);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M, outputs.M, relative = %t, compar_crit = "element", prec = 1e-014);
