// ------------------------------
// CL_op_repeat2smaInc
// validation:
// ---------
// ------------------------------

// Inputs
inputs = struct();
inputs.N = 15;
inputs.P = 3;
inputs.Q = 16;
inputs.ecc = 0.01;
inputs.sso = 0;
inputs.incInput = CL_deg2rad(98);
inputs.er = 6378.1363e3;
inputs.mu = 398600.4415e9;
inputs.j2 = 0.00108262669059782;
inputs.rotr_pla = 0.000072921151467052095767345660;
inputs.rotr_pla_sun = 0.000000199099300621696728858545;

// Outputs
outputs = struct();
outputs.sma = [6882824.5403448604];
outputs.inc = [1.7104226669544429];

TEST_OK = [];

//=========================
// Compute output
//=========================
[sma,inc] = CL_op_repeat2smaInc(inputs.N,inputs.P,inputs.Q,inputs.ecc,inputs.sso,inputs.incInput,inputs.er,inputs.mu,inputs.j2,inputs.rotr_pla,inputs.rotr_pla_sun);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(sma, outputs.sma, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(inc, outputs.inc, relative = %t, compar_crit = "element", prec = 1e-014);
