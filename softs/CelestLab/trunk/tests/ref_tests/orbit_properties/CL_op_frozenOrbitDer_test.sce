// ------------------------------
// CL_op_frozenOrbitDer
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.sma = [7000e3 7000e3];
inputs.ecc = 0.2;
inputs.inc = CL_deg2rad(90.4);
inputs.pom = CL_deg2rad(180);
inputs.er = 6378.1363e3;
inputs.mu = 398600.4415e9;
inputs.j1jn =[0 0.00108262669059782 -2.53243547943241e-06];

// Outputs
outputs = struct();
outputs.d_exc_dt = [8.4007847372452088e-010,8.4007847372452088e-010];
outputs.d_pom_dt = [-7.8832449323251631e-007,-7.8832449323251631e-007];

TEST_OK = [];

//=========================
// Compute output
//=========================
[d_exc_dt,d_pom_dt] = CL_op_frozenOrbitDer(inputs.sma,inputs.ecc,inputs.inc,inputs.pom,inputs.er,inputs.mu,inputs.j1jn);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(d_exc_dt, outputs.d_exc_dt, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(d_pom_dt, outputs.d_pom_dt, relative = %t, compar_crit = "element", prec = 1e-014);
