// ------------------------------
// CL_op_ssoJ2_2
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.par_calc = 'a';
inputs.par1 = 0.1e-2;	//excentricity
inputs.par2 = CL_deg2rad(98);	//inclination
inputs.er = 6378.1363e3;
inputs.mu = 398600.4415e9;
inputs.j2 = 0.00108262669059782;
inputs.rotr_pla_sun = 0.000000199099300621696728858545;

// Outputs
outputs = struct();
outputs.par3 = [7031708.0409060707];

TEST_OK = [];

//=========================
// Compute output
//=========================
[par3] = CL_op_ssoJ2(inputs.par_calc,inputs.par1,inputs.par2,inputs.er,inputs.mu,inputs.j2,inputs.rotr_pla_sun);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(par3, outputs.par3, relative = %t, compar_crit = "element", prec = 1e-014);
