// ------------------------------
// CL_op_rpvp2ae
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.rp = 7995000.5;
inputs.vp = 10204.040;

// Outputs
outputs = struct();
outputs.sma = [90385425.452734321];
outputs.ecc = [1.0884545319110199];

TEST_OK = [];

//=========================
// Compute output
//=========================
[sma,ecc] = CL_op_rpvp2ae(inputs.rp,inputs.vp);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(sma, outputs.sma, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(ecc, outputs.ecc, relative = %t, compar_crit = "element", prec = 1e-014);
