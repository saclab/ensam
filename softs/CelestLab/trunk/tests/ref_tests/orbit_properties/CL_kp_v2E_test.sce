// ------------------------------
// CL_kp_v2E
// validation:
// Orbital Mechanics for Engineering Students, H D Curtis, Chapter 3, example 3.2
// ------------------------------

// Inputs
inputs = struct();
inputs.ecc = [0.37255 1.165];
inputs.v = CL_deg2rad([120 120]);

// Outputs
outputs = struct();
outputs.E = [1.728069272925161,1.0411934824134859];

TEST_OK = [];

//=========================
// Compute output
//=========================
[E] = CL_kp_v2E(inputs.ecc,inputs.v);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(E, outputs.E, relative = %t, compar_crit = "element", prec = 1e-014);
