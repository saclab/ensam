// ------------------------------
// CL_kp_E2v
// validation:
// Orbital Mechanics for Engineering Students, H D Curtis, Chapter 3, example 3.2
// ------------------------------

// Inputs
inputs = struct();
inputs.e = [0.37255 1.165];
inputs.E = [3.4794 CL_deg2rad(59)];

// Outputs
outputs = struct();
outputs.v = [3.371175130869565,2.086319561209502];

TEST_OK = [];

//=========================
// Compute output
//=========================
[v] = CL_kp_E2v(inputs.e,inputs.E);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(v, outputs.v, relative = %t, compar_crit = "element", prec = 1e-014);
