//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// ==============================================================
// TEST 
// CL_op_orbGapLof / CL_op_orbGapLofMat
// ==============================================================

TEST_OK = [];
N = 9; // size of vectors

// Reference orbit (keplerian orbital elements)
sma = 7000.e3;
ecc = 1.e-3;
inc = 1.3;
pom = %pi/4;
gom = 2;
anm = linspace(0, 2*%pi, N);

// (small) perturbation of the keplerian elements
dsma  = 100;
decc  = dsma/sma;
dinc  = 1.5*dsma/sma;
dpom  = -2*dsma/sma;
dgom  = -2.5*dsma/sma;
danm  = 3*dsma/sma;

// Keplerian orbital elements
kep = [sma; ecc; inc; pom; gom; anm(1)];
kep = kep * ones(1,N); 
kep(6,:) = anm; 

dkep = [dsma; decc; dinc; dpom; dgom; danm];

// circular elements
// dcir obtained from dkep using jacobian (1st order)
[cir, jac] = CL_oe_kep2cir(kep); 
dcir = jac * dkep; 


// ==============================================================
// TEST1
// Normalized jacobians
// Compare: "c" (reference) and "f" (linearized) 
// "kep", "qsw"
// ==============================================================
M1 = CL_op_orbGapLofMat("kep", kep, "qsw", meth = "c");
M2 = CL_op_orbGapLofMat("kep", kep, "qsw", meth = "f");

TEST_OK($+1) = CL__isEqual(M1-M2, zeros(M1), relative = %f, prec = 1.e-12);


// ==============================================================
// TEST2
// Compare: "c" (reference) and "f" (linearized)
// "kep", "qsw"
// ==============================================================
[dpos, dvel]   = CL_op_orbGapLof("kep", kep, dkep, "qsw", meth = "c");
[dpos2, dvel2] = CL_op_orbGapLof("kep", kep, dkep, "qsw", meth = "f");

TEST_OK($+1) = CL__isEqual(dpos, dpos2, relative = %f, prec = 1.e-1);
TEST_OK($+1) = CL__isEqual(dvel, dvel2, relative = %f, prec = 1.e-3);


// ==============================================================
// TEST3
// Compare: "c" (reference) and "f" (linearized)
// "kep", "qsw"
// ==============================================================
[dpos, dvel]   = CL_op_orbGapLof("kep", kep, dkep, "qsw", meth = "c");
[dpos2, dvel2] = CL_op_orbGapLof("kep", kep, dkep, "qsw", meth = "f1");

TEST_OK($+1) = CL__isEqual(dpos, dpos2, relative = %f, prec = 1.e0);
TEST_OK($+1) = CL__isEqual(dvel, dvel2, relative = %f, prec = 1.e-3);


// ==============================================================
// TEST4
// Compare: "c" (reference) and "f" (linearized)
// "kep", "tnw"
// ==============================================================
[dpos, dvel]   = CL_op_orbGapLof("kep", kep, dkep, "tnw", meth = "c");
[dpos2, dvel2] = CL_op_orbGapLof("kep", kep, dkep, "tnw", meth = "f1");

TEST_OK($+1) = CL__isEqual(dpos, dpos2, relative = %f, prec = 1.e0);
TEST_OK($+1) = CL__isEqual(dvel, dvel2, relative = %f, prec = 1.e-3);


// ==============================================================
// TEST5
// Compare: "c" (reference) and "f1" (linearized, 1st order in ecc)
// "qsw", "kep" and "cir"
// ==============================================================
[dpos, dvel]   = CL_op_orbGapLof("kep", kep, dkep, "qsw", meth = "c");
[dpos2, dvel2] = CL_op_orbGapLof("kep", kep, dkep, "qsw", meth = "f0");
[dpos3, dvel3] = CL_op_orbGapLof("cir", cir, dcir, "qsw", meth = "f0");

// compare kep to reference
TEST_OK($+1) = CL__isEqual(dpos, dpos2, relative = %f, prec = 1.e0);
TEST_OK($+1) = CL__isEqual(dvel, dvel2, relative = %f, prec = 1.e-3);

// compare kep and cir
TEST_OK($+1) = CL__isEqual(dpos2, dpos3, relative = %f, prec = 1.e0);
TEST_OK($+1) = CL__isEqual(dvel2, dvel3, relative = %f, prec = 1.e-3);


// ==============================================================
// TEST6
// Compare: "c" (reference) and "f0" (linearized, order 0 in ecc)
// "tnw", "kep" and "cir"
// ==============================================================
[dpos, dvel]   = CL_op_orbGapLof("kep", kep, dkep, "tnw", meth = "c");
[dpos2, dvel2] = CL_op_orbGapLof("kep", kep, dkep, "tnw", meth = "f0");
[dpos3, dvel3] = CL_op_orbGapLof("cir", cir, dcir, "tnw", meth = "f0");

// compare kep to reference
TEST_OK($+1) = CL__isEqual(dpos, dpos2, relative = %f, prec = 1.e0);
TEST_OK($+1) = CL__isEqual(dvel, dvel2, relative = %f, prec = 1.e-3);

// compare kep and cir
TEST_OK($+1) = CL__isEqual(dpos2, dpos3, relative = %f, prec = 1.e-0);
TEST_OK($+1) = CL__isEqual(dvel2, dvel3, relative = %f, prec = 1.e-3);

