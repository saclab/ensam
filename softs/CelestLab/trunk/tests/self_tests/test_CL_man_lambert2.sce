// ------------------------------
// CL_man_lambert2 : 
// ------------------------------

TEST_OK = [];

//////////////////////////////////////////////////////////////
// Test1: cas nominal with m=2, validation with propagation //
//////////////////////////////////////////////////////////////
// Lambert input data: initial and final positions and flight time
pi = [5000.e3;10000.e3;2100.e3];
pf = [-14600.e3;2500.e3;7000.e3];
delta_t = 40000; // seconds

// Velocity at initial and final position
[vi_lambert, vf_lambert] = CL_man_lambert2(pi, pf, delta_t, m = 2);

// Initial statevector built with pi and vi_lambert
[kepi] = CL_oe_car2kep(pi,vi_lambert);

// Final statevector after propagation
[kepf] = CL_ex_kepler(0,kepi, delta_t/86400);
[Pos_prop_f, vit_prop_f] = CL_oe_kep2car(kepf);

TEST_OK(1+$) = CL__isEqual(abs(Pos_prop_f),abs(pf),1.e-12)& ..
                 CL__isEqual(vit_prop_f,vf_lambert,1.e-12);

//////////////////////////////////////////////////////////////////////////
// Test2: If m=0, CL_man_lambert & CL_man_lambert2 provide same results //
//////////////////////////////////////////////////////////////////////////
dt = 1000; // seconds
kep1 = [7000.e3; 0.1; 1; 0; 0; 0]; // initial statevector
kep2 = CL_ex_kepler(0, kep1, dt/86400); // final statevector after propagation
[p1, v1] = CL_oe_kep2car(kep1); 
[p2, v2] = CL_oe_kep2car(kep2);

[v1b, v2b] = CL_man_lambert2(p1, p2, dt,m = 0);
[vel1b, vel2b] = CL_man_lambert(p1, p2, dt);

TEST_OK(1+$) = CL__isEqual(v1b,vel1b,1.e-10,relative=%f) & CL__isEqual(v2b,vel2b);

////////////////////////////////////////////////////////////////////////
// Test3: m optional argument --> same results if m=0 and not m input //
////////////////////////////////////////////////////////////////////////
dt = 1000; // seconds
kep1 = [7000.e3; 0.1; 1; 0; 0; 0]; 
kep2 = CL_ex_kepler(0, kep1, dt/86400); 
[p1, v1] = CL_oe_kep2car(kep1); 
[p2, v2] = CL_oe_kep2car(kep2);

[v1b_m, v2b_m] = CL_man_lambert2(p1, p2, dt, m = 0);
[v1b, v2b] = CL_man_lambert2(p1, p2, dt);

TEST_OK(1+$) = CL__isEqual(v1b_m,v1b) & CL__isEqual(v2b_m,v2b);

/////////////////////////////////////////////////////////////////////////////
// Test4: argument optional leftbranch=%t and direction= 'retro' and m=3,4 //
/////////////////////////////////////////////////////////////////////////////
// Lambert input data: initial and final positions and flight time
pi = [6300000.;0;0];
pf = [2030507.3; 3465056.3;5396505.5];
delta_t = 100000; // seconds

// m = 3
    // Velocity at initial and final positions
    [vi_lambert, vf_lambert] = CL_man_lambert2(pi, pf, delta_t,direction = 'retro',m = 3, leftbranch = %t);
    // Initial statevector built with pi and vi_lambert
    [kepi] = CL_oe_car2kep(pi,vi_lambert);
    [kepf] = CL_ex_kepler(0,kepi, delta_t/86400);
    [Pos_prop_f, vit_prop_f] = CL_oe_kep2car(kepf);

    TEST_OK(1+$) = CL__isEqual(abs(Pos_prop_f),abs(pf),1.e-10)& ..
                   CL__isEqual(vit_prop_f,vf_lambert,1.e-10);
               
// m = 4
    // Velocity at initial and final position
    [vi_lambert, vf_lambert] = CL_man_lambert2(pi, pf, delta_t,direction = 'retro', m = 4);
    // Initial statevector built with pi and vi_lambert
    [kepi] = CL_oe_car2kep(pi,vi_lambert);
    [kepf] = CL_ex_kepler(0,kepi, delta_t/86400);
    [Pos_prop_f, vit_prop_f] = CL_oe_kep2car(kepf);

    TEST_OK(1+$) = CL__isEqual(abs(Pos_prop_f),abs(pf),1.e-12)& ..
                   CL__isEqual(vit_prop_f,vf_lambert,1.e-12);

//////////////////////////////////////
// Test 5: Flight time verification //
//////////////////////////////////////
// Note: when m>0 the flight time shall be tf > Tmin = 2pi*sqrt(amin^3/mu), where amin = min(ri/2,rf/2)
// The algorithm shall be reviewed because, forcing it to its limits, it does not converge

/////////////////////////////////////////////
// Test 6: Nominal cas with Earth and Mars //
/////////////////////////////////////////////
// Note: Verification by visual inspection
//       The aim of this cas is to reproduce the drawing includes in on-line 
//       help of the function. To consider the test succes it shall display the drawings and 
//       compare them to those of the on-line help. 
au = CL_dataGet("au");
mu_sun = CL_dataGet("body.Sun.mu");
pos_Earth = [-0.8;0.5;0].*au; // Initial position
pos_Mars = [0.5;-1.45;0].*au; // Final position
pos_Sun = [0;0;0].*au;
delta_t0 = 750; // days
t0 = [0:10:delta_t0];
delta_t1 = 750; //days
t1 = [0:10:delta_t1];
m0 = 0;
m1 = 1;


// m0 = 0, prograde 
[vi_lambert0_pro, vf_lambert0_pro] = CL_man_lambert2(pos_Earth, pos_Mars, delta_t0*86400,direction='pro', m = m0, mu = mu_sun);

// [kepi0_pro] = CL_oe_car2kep(pos_Earth,vi_lambert0_pro,mu = mu_sun);

// a0_pro(:,1) = pos_Earth;
// for i=2:size(t0,2)
//     [kepf0_pro] = CL_ex_kepler(t0(i-1),kepi0_pro, t0(i),mu = mu_sun);
//     kepi0_pro = kepf0_pro;
//     [Pos_prop_f0_pro, vit_prop_f0_pro] = CL_oe_kep2car(kepf0_pro,mu = mu_sun);
//     a0_pro(:,i) = Pos_prop_f0_pro;
// end
// Remove comments to plot trajectory and compare to that of on-line help
// plot(a0_pro(1,:)./au,a0_pro(2,:)./au,'cyan');
// plot(pos_Earth(1)/au,pos_Earth(2)/au,'x');
// plot(pos_Mars(1)/au,pos_Mars(2)/au,'x');
// plot(pos_Sun(1)/au,pos_Sun(2)/au,'x');

// m0 =0, retrograde
[vi_lambert0_retro, vf_lambert0_retro] = CL_man_lambert2(pos_Earth, pos_Mars, delta_t0*86400,direction='retro', m = m0, mu = mu_sun);
// [kepi0_retro] = CL_oe_car2kep(pos_Earth,vi_lambert0_retro,mu = mu_sun);
// a0_retro(:,1) = pos_Earth;
// for i=2:size(t0,2)
//     [kepf0_retro] = CL_ex_kepler(t0(i-1),kepi0_retro, t0(i),mu = mu_sun);
//     kepi0_retro = kepf0_retro;
//     [Pos_prop_f0_retro, vit_prop_f0_retro] = CL_oe_kep2car(kepf0_retro,mu = mu_sun);
//     a0_retro(:,i) = Pos_prop_f0_retro;
// end
// Remove comments to plot trajectory and compare to that of on-line help
// plot(a0_retro(1,:)./au,a0_retro(2,:)./au,'red');


// m1 = 1, prograde, right branch
[vi_lambert1_pro_right, vf_lambert1_pro_right] = CL_man_lambert2(pos_Earth, pos_Mars, delta_t1*86400,direction='pro', m = m1,leftbranch=%f, mu =mu_sun);

// [kepi1_pro_right] = CL_oe_car2kep(pos_Earth,vi_lambert1_pro_right,mu = mu_sun);
// a1_pro_right(:,1) = pos_Earth; 
// for i=2:size(t1,2)
//     [kepf1_pro_right] = CL_ex_kepler(t1(i-1),kepi1_pro_right, t1(i),mu = mu_sun);
//     kepi1_pro_right = kepf1_pro_right;
//     [Pos_prop_f1_pro_right, vit_prop_f1_pro_right] = CL_oe_kep2car(kepf1_pro_right,mu = mu_sun);
//     a1_pro_right(:,i) = Pos_prop_f1_pro_right;
// end
// Remove comments to plot trajectory and compare to that of on-line help
// scf();
// plot(a1_pro_right(1,:)./au,a1_pro_right(2,:)./au,'blue');
// plot(pos_Earth(1)/au,pos_Earth(2)/au,'x');
// plot(pos_Mars(1)/au,pos_Mars(2)/au,'x');
// plot(pos_Sun(1)/au,pos_Sun(2)/au,'x');


// m1 = 1, retrograde, right branch
[vi_lambert1_retro_right, vf_lambert1_retro_right] = CL_man_lambert2(pos_Earth, pos_Mars, delta_t1*86400,direction='retro', m = m1,leftbranch=%f, mu =mu_sun);

// [kepi1_retro_right] = CL_oe_car2kep(pos_Earth,vi_lambert1_retro_right,mu = mu_sun);
// a1_retro_right(:,1) = pos_Earth;
// for i=2 :size(t1,2)
//     [kepf1_retro_right] = CL_ex_kepler(t1(i-1),kepi1_retro_right, t1(i),mu = mu_sun);
//     kepi1_retro_right = kepf1_retro_right;
//     [Pos_prop_f1_retro_right, vit_prop_f1_retro_right] = CL_oe_kep2car(kepf1_retro_right,mu = mu_sun);
//     a1_retro_right(:,i) = Pos_prop_f1_retro_right;
// end
// Remove comments to plot trajectory and compare to that of on-line help
// plot(a1_retro_right(1,:)./au,a1_retro_right(2,:)./au,'cyan');


// m1 =1, prograde, left branch
[vi_lambert1_pro_left, vf_lambert1_pro_left] = CL_man_lambert2(pos_Earth, pos_Mars, delta_t1*86400,direction='pro', m = m1,leftbranch=%t, mu =mu_sun);
// [kepi1_pro_left] = CL_oe_car2kep(pos_Earth,vi_lambert1_pro_left,mu = mu_sun);
// a1_pro_left(:,1) = pos_Earth;
// for i=2:size(t1,2)
//     [kepf1_pro_left] = CL_ex_kepler(t1(i-1),kepi1_pro_left, t1(i),mu = mu_sun);
//     kepi1_pro_left = kepf1_pro_left;
//     [Pos_prop_f1_pro_left, vit_prop_f1_pro_left] = CL_oe_kep2car(kepf1_pro_left,mu = mu_sun);
//     a1_pro_left(:,i) = Pos_prop_f1_pro_left;
// end
// Remove comments to plot trajectory and compare to that of on-line help
// plot(a1_pro_left(1,:)./au,a1_pro_left(2,:)./au,'green');


// m1 = 1, retrograde, left branch
[vi_lambert1_retro_left, vf_lambert1_retro_left] = CL_man_lambert2(pos_Earth, pos_Mars, delta_t1*86400,direction='retro', m = m1,leftbranch=%t, mu =mu_sun);
// [kepi1_retro_left] = CL_oe_car2kep(pos_Earth,vi_lambert1_retro_left,mu = mu_sun);
// a1_retro_left = pos_Earth;
// for i=2:size(t1,2)
//     [kepf1_retro_left] = CL_ex_kepler(t1(i-1),kepi1_retro_left, t1(i),mu = mu_sun);
//     kepi1_retro_left=kepf1_retro_left;
//     [Pos_prop_f1_retro_left, vit_prop_f1_retro_left] = CL_oe_kep2car(kepf1_retro_left,mu = mu_sun);
//     a1_retro_left(:,i) = Pos_prop_f1_retro_left;
// end
// Remove comments to plot trajectory and compare to that of on-line help
// plot(a1_retro_left(1,:)./au,a1_retro_left(2,:)./au,'red');


///////////////////////////////////////////////////
// Test7: Nominal case with most used parameters //
///////////////////////////////////////////////////
//Most used parameters
m = 1;
direction = 'pro';
leftbranch = %f;

pi = [5000.e3;10000.e3;2100.e3];
pf = [-14600.e3;2500.e3;7000.e3];
delta_t = 40000; // seconds

// Velocity at initial and final position
[vi_lambert, vf_lambert] = CL_man_lambert2(pi, pf, delta_t, direction, m, leftbranch);

// Initial statevector built with pi and vi_lambert
[kepi] = CL_oe_car2kep(pi,vi_lambert);

// Final statevector after propagation
[kepf] = CL_ex_kepler(0,kepi, delta_t/86400);
[Pos_prop_f, vit_prop_f] = CL_oe_kep2car(kepf);

TEST_OK(1+$) = CL__isEqual(abs(Pos_prop_f),abs(pf),1.e-12)& ..
                 CL__isEqual(vit_prop_f,vf_lambert,1.e-12);