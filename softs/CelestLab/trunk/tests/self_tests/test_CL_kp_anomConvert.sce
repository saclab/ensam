// ------------------------------------------
// Test of CL_kp_anomConvert
// Check consistency only because it only
// calls other tested functions. 
// ------------------------------------------

// Initialization
TEST_OK = [];

// ------------------------------------------
// TEST 1 :  Elliptical orbit
// ------------------------------------------
e = [0, 0.1, 0.1, 0.99];
E = [0, 0.1, 1, 10];

// Check CL_kp_M2E <--> CL_kp_E2M
M = CL_kp_anomConvert("E","M",e, E);
E1 = CL_kp_anomConvert("M","E",e, M);

TEST_OK($+1) = CL__isEqual(E, E1, 1.e-13);

// Check CL_kp_M2v <--> CL_kp_v2M
v = CL_kp_anomConvert("M","v",e,M);
M1 = CL_kp_anomConvert("v","M",e,v);

TEST_OK($+1) = CL__isEqual(M, M1, 1.e-13);
               
// Check CL_kp_E2v <--> CL_kp_v2E
v = CL_kp_anomConvert("E","v",e,E);
E1 = CL_kp_anomConvert("v","E",e, v);

TEST_OK($+1) = CL__isEqual(E, E1, 1.e-13);


// ------------------------------------------
// TEST 2 :  Hyperbolic orbit
// ------------------------------------------
e_ref = [1.01, 2, 2, 10];
E_ref = [0, 0.1, 10, 10];

// Check CL_kp_M2E <--> CL_kp_E2M
M = CL_kp_anomConvert("E","M",e, E);
E1 = CL_kp_anomConvert("M","E",e, M);

TEST_OK($+1) = CL__isEqual(E,E1,1.e-13);

// Check CL_kp_M2v <--> CL_kp_v2M
v = CL_kp_anomConvert("M","v",e,M);
M1 = CL_kp_anomConvert("v","M",e,v);

TEST_OK($+1) = CL__isEqual(M,M1,1.e-13);
               
// Check CL_kp_E2v <--> CL_kp_v2E
v = CL_kp_anomConvert("E","v",e,E);
E1 = CL_kp_anomConvert("v","E",e, v);

TEST_OK($+1) = CL__isEqual(E,E1,1.e-13);


               
