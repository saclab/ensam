// --------------------------------------------
// Validation of low level functions CL__iers_xys_block and CL__iers_xys_interp
// Reference values : SOFA (see test__CL_iers_ref.sce)
// --------------------------------------------



// Utility function : angular distance between 2 matrix
function ang = delta_rot(M1,M2)
  [ax,ang] = CL_rot_quat2axAng( CL_rot_matrix2quat(M1) * CL_rot_matrix2quat(M2'));
endfunction

// Reminder : 1 micro arcsecondes = 1/3600/1e6*%CL_deg2rad radians = 4.848D-12 radians 
//                                = 0.0000309 m = 30 microns at the surface of Earth
MUAS = 1/3600/1e6*%CL_deg2rad;

TEST_OK = [];

// -------------
// TEST : xys_block 
// (Comparison of version block and non block)
// -------------
N = 99;
jd = [2451544 * ones(1,N); linspace(0, 10 * 365, N)]; 
[x,y,s, xdot, ydot, sdot] = CL__iers_xys2006A_cla(jd);
[x2,y2,s2, xdot2, ydot2, sdot2] = CL__iers_xys_block(jd, "classic");
ecart = max( ([x,y,s, xdot, ydot, sdot] - [x2,y2,s2, xdot2, ydot2, sdot2]) ./[x,y,s, xdot, ydot, sdot] );
TEST_OK($+1) = ecart < 1.e-12;


// -------------
// TEST : xys_interp
// (Comparison of version without interpolation and version with interpolation)
// -------------
N = 99;
jd = [2451544 * ones(1,N); linspace(0, 10 * 365, N)]; 

[x,y,s] = CL__iers_xys_interp(jd);
[x2,y2,s2] = CL__iers_xys_block(jd, "classic");
M = CL__iers_xys2Mat(x,y,s);
M2 = CL__iers_xys2Mat(x2,y2,s2);
ecart_ang = delta_rot(M,M2);
// Interpolation should induce errors inferior to 1 micro arcsecond.
TEST_OK($+1) = max(ecart_ang/MUAS) < 1;

