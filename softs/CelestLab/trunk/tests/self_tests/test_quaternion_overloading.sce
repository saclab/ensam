// ------------------------------
// Tests of overloaded operations for quaternions
// ------------------------------

TEST_OK = [];

function test_quaternions(q1,q2)
  N = max(size(q1),size(q2));
  I0 = [];
  I1 = 1;
  IN = 1:N;
  Id = 1:$;
  Ip = :;
  
  // Operations entre un double et un quaternion
  q = q1 .* I0
  q = q1 .* I1
  q = q1 .* IN
  q = I0 .* q1
  q = I1 .* q1
  q = IN .* q1
 
  q = I0 ./ q1
  q = I1 ./ q1
  q = IN ./ q1
  q = q1 ./ I0
  q = q1 ./ I1
  q = q1 ./ IN
  
  q = q1 * I0
  q = q1 * I1
  if (size(q1) <= 1); q = q1 * IN; end
  q = I0 * q1
  q = I1 * q1
  if (size(q1) <= 1); q = IN * q1; end
  
  q = I0 / q1
  q = I1 / q1
  if (size(q1) <= 1);q = IN / q1; end
  q = q1 / I0
  q = q1 / I1
  if (size(q1) <= 1);q = q1 / IN; end
  
  // q = I1 + q1 (non implemente)
  // q = IN + q1
  // q = q1 + I1
  // q = q1 + IN 
  
  // q = I1 - q1
  // q = IN - q1
  // q = q1 - I1
  // q = q1 - IN
  
  // q = q1 + []
  // q = q2 + []
  // q = [] + q1
  // q = [] + q2
  
  // Operations entre deux quaternions
  q = q2 * q1
  q = q1 * q2
  q = q1 + q2
  q = q2 + q1
  q = q1 - q2
  q = q2 - q1
  q = q1 / q2
  q = q2 / q1
  q1 == q2
  // q = q2 .* q1 (non implemente)
  // q = q1 .* q2
  // q = q1 ./ q2
  // q = q2 ./ q1
  
  
  // Operations de concatenation, extraction, insertion
  q = [q1, q2]
  q = q1+q2
  q(I0)
  if (size(q) >= 1); q(I1); end;
  if (size(q) >= 1); q(IN); end;
  q(Id)
  q(Ip) 
  
  // q(I0) = CL__defQuat([], []) // Bug scilab...
  if (size(q) >= 1); q(I1) = CL__defQuat(0, [1;2;3]); end;
  
  if (size(q) >= 1); q(IN) = CL__defQuat(0*ones(1,N), [1;2;3]*ones(1,N)); end;
  q(Id) = CL__defQuat(0*ones(1,N), [1;2;3]*ones(1,N))
  q(Ip) = CL__defQuat(0*ones(1,N), [1;2;3]*ones(1,N))
  
  if (size(q) >= 1); q(IN) = CL__defQuat(0, [1;2;3]); end;
  q(Id) = CL__defQuat(0, [1;2;3])
  q(Ip) = CL__defQuat(0, [1;2;3])
  if (size(q) >= 1); q(I1) = []; end;
  q = q1+q2
  if (size(q) >= 1); q(IN) = []; end;
  q(Id) = []
  q(Ip) = []
  
  
  // Autres operations
  q = q1'
  q = q2'
  sz = size(q1)
  sz = size(q2)
  n = norm(q1)
  n = norm(q2)
  q = inv(q1)
  q = inv(q2)
  

endfunction

r = 0.1;
i = [1;2;3];

// TEST 1 : q1 de taille 1, q2 de taille 1
q1 = CL__defQuat(r, i);
q2 = CL__defQuat(3*r, 2*i);
ierr = execstr("test_quaternions(q1,q2)","errcatch");
TEST_OK($+1) = (ierr == 0);

// TEST 2 : q1 de taille 1, q2 de taille 2
q1 = CL__defQuat(r, i);
q2 = CL__defQuat(3*[r,r], 2*[i,i]);
ierr = execstr("test_quaternions(q1,q2)","errcatch");
TEST_OK($+1) = (ierr == 0);

// TEST 3 : q1 de taille 2, q2 de taille 1
q1 = CL__defQuat(3*[r,r], 2*[i,i]);
q2 = CL__defQuat(r, i);
ierr = execstr("test_quaternions(q1,q2)","errcatch");
TEST_OK($+1) = (ierr == 0);

// TEST 4 : q1 de taille 2, q2 de taille 0
q1 = CL__defQuat(3*[r,r], 2*[i,i]);
q2 = CL__defQuat([], []);
ierr = execstr("test_quaternions(q1,q2)","errcatch");
TEST_OK($+1) = (ierr == 0);

// TEST 5 : q1 de taille 0, q2 de taille 2
q1 = CL__defQuat([], []);
q2 = CL__defQuat(3*[r,r], 2*[i,i]);
ierr = execstr("test_quaternions(q1,q2)","errcatch");
TEST_OK($+1) = (ierr == 0);

// TEST 6 : q1 de taille 0, q2 de taille 0
q1 = CL__defQuat([], []);
q2 = CL__defQuat([], []);
ierr = execstr("test_quaternions(q1,q2)","errcatch");
TEST_OK($+1) = (ierr == 0);


