// ------------------------------
// CL_gm_eclipse: 
// ------------------------------

TEST_OK = [];

alpha_sun = 10 * %CL_deg2rad;
delta_sun = 20 * %CL_deg2rad; 

sma = 5000.e3 + %CL_eqRad;
ecc = 0.1; 
inc = 70 * %CL_deg2rad;
pom = 50 * %CL_deg2rad; 
gom = -20 * %CL_deg2rad;

[result] = CL_gm_eclipse(sma,ecc,inc,pom,gom,alpha_sun,delta_sun);

// verif des limites d'eclipses
anv1 = result.start.pso - pom; 
anm1 = CL_rMod(CL_kp_v2M(ecc, anv1),0,2*%pi); 

anv2 = result.end.pso - pom; 
anm2 = CL_rMod(CL_kp_v2M(ecc, anv2),0,2*%pi); 

param1 = [sma;ecc;inc;pom;gom;anm1]; 
param2 = [sma;ecc;inc;pom;gom;anm2]; 

pos1 = CL_oe_kep2car(param1); 
pos2 = CL_oe_kep2car(param2); 

sun = CL_co_sph2car([alpha_sun; delta_sun; 1]); 

// Test 1 :
// h = distance entre centre planete et droite passant par pos1 et dirigee par sun.
// h1 et h2 doivent etre egaux a %CL_eqRad 
h1 = CL_norm(CL_cross(pos1, sun)) ; 
h2 = CL_norm(CL_cross(pos2, sun)) ;
h = [h1 ; h2];
href = %CL_eqRad * ones(h);
test_ok_1 = CL__isEqual( h , href , 1e-14);

// angles (terre -> sat, sun) doivent etre > %pi/2
ang1 = CL_vectAngle(pos1, sun);
ang2 = CL_vectAngle(pos2, sun);
ang = [ang1 ; ang2];
test_ok_2 = isempty( find( ang <= %pi/2 ));


TEST_OK($+1) = test_ok_1 & test_ok_2;
