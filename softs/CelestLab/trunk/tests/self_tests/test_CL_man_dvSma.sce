// -------------------------------------------------------
// CL_man_dvSma : Cross validation with CL_man_applyDvKep
// -------------------------------------------------------

TEST_OK = [];

/////////////////////////////////////////////////////
// Test 1: Cross validation with CL_man_applyDvKep //
/////////////////////////////////////////////////////
ai = 7200.e3;
af = 7000.e3;
ei = 0.1;
[deltav,dv,anv] = CL_man_dvSma(ai,ei,af);
// Check results :
kep = [ai ; ei ; %pi/2 ; 0 ; 0 ; anv];
kep1 = CL_man_applyDvKep(kep,dv);

TEST_OK($+1) = CL__isEqual(af, kep1(1)) & ...           // sma
                 CL__isEqual(kep(3), kep1(3)) & ...          // inc
                 CL__isEqual(kep(5), CL_rMod(kep1(5),-%pi,%pi));  // gom

////////////////////////////////
// Test 2: optional arguments //
////////////////////////////////
ai = 7200.e3;
af = 7000.e3;
ei = 0.1;
[deltav_per,dv_per,anv_per] = CL_man_dvSma(ai,ei,af,posman="per");
kep_per = [ai ; ei ; %pi/2 ; 0 ; 0 ; anv_per];
kep1 = CL_man_applyDvKep(kep_per,dv_per);

[deltav_apo,dv_apo,anv_apo] = CL_man_dvSma(ai,ei,af,posman="apo");
kep_apo = [ai ; ei ; %pi/2 ; 0 ; 0 ; anv_apo];
kep2 = CL_man_applyDvKep(kep_apo,dv_apo);

TEST_OK($+1) = CL__isEqual(af, kep1(1)) & ...           // sma
                 CL__isEqual(kep_per(3), kep1(3)) & ...          // inc
                 CL__isEqual(kep_per(5), CL_rMod(kep1(5),-%pi,%pi));  // gom
                 
TEST_OK($+1) = CL__isEqual(af, kep2(1)) & ...           // sma
                 CL__isEqual(kep_apo(3), kep2(3)) & ...          // inc
                 CL__isEqual(kep_apo(5), CL_rMod(kep2(5),-%pi,%pi));  // gom

//////////////////////////////////////////////////////////////
// Test 3: The results are set to %nan if the targeted semi //
//         major-axis is unreachable                        //
//////////////////////////////////////////////////////////////
ai = 7200.e3;
af = 100.e3;
ei = 0.1;
[deltav,dv,anv] = CL_man_dvSma(ai,ei,af);

TEST_OK($+1) = CL__isEqual(deltav,%nan) & CL__isEqual(dv(1),%nan) & ...
                 CL__isEqual(dv(2),%nan)  & CL__isEqual(dv(3),%nan) & ...
                 CL__isEqual(anv,%nan);

//////////////////////
// Test 4: res == d //
//////////////////////
ai = 7200.e3;
af = 7000.e3;
ei = 0.1;
man = CL_man_dvSma(ai,ei,af,res="s");
// Check results :
kep = [ai ; ei ; %pi/2 ; 0 ; 0 ; man.anv];
kep1 = CL_man_applyDvKep(kep,man.dv);

TEST_OK($+1) = CL__isEqual(af, kep1(1))      & ...           // sma
                 CL__isEqual(kep(3), kep1(3)) & ...           // inc
                 CL__isEqual(kep(5), CL_rMod(kep1(5),-%pi,%pi));  // gom

////////////////////////////////////////////////
// Test 5: Invalid value for argument ''res'' //
////////////////////////////////////////////////
ai = 7200.e3;
af = 7000.e3;
ei = 0.1;

try
    man = CL_man_dvSma(ai,ei,af, res="p");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

////////////////////////////////////////////////
// Test 6: Invalid number of output arguments //
////////////////////////////////////////////////
ai = 7200.e3;
af = 7000.e3;
ei = 0.1;

try
    [deltav,dv,anv] = CL_man_dvSma(ai,ei,af, res="s");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end 

////////////////////////////////////////////////////////
// Test 7: Invalid input arguments (orbital elements) //
////////////////////////////////////////////////////////
ai = -7200.e3;
af = 7000.e3;
ei = 0.1;

try
    [deltav,dv,anv] = CL_man_dvSma(ai,ei,af);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end 
  
/////////////////////////////////////////
// Test 8: Invalid value for ''posman''//
/////////////////////////////////////////
ai = 7200.e3;
af = 7000.e3;
ei = 0.1;

try
    [deltav,dv,anv] = CL_man_dvSma(ai,ei,af,posman=2);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end 
