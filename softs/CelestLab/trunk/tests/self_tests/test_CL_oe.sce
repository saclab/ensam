// ------------------------------------------
// Consistency test of functions CL_oe_xxx: 
// ------------------------------------------

TEST_OK = [];

// Internal function to check that jacob is (almost) the identity 
// precision is the highest number any term can have (besides the diagonal which should be ones)
// Note: If there are %nan values, they are ignored (i.e they are accepted)
function [is_identity, max_prec] = test_jacob(jacob,precision)
  nd = length(size(jacob)); // number of dimensions
  if (nd == 3)
    N = size(jacob,3);
  else
    N = 1; // if jacob is a matrix
  end
  
  max_prec = 0;
  for k = 1 : N
    // Matrix with only the diagonal
    Idiag = diag(diag(jacob(:,:,k))');
    
    // Jacobian without its diagonal (which should be ones)
    J = jacob(:,:,k)-Idiag;
    
    // Maximum value outside the diagonal
    max_prec = max([max(J),max_prec]);
  end
  
  is_identity = ~(max_prec > precision);
  
endfunction

// ------------------------------------------
// TEST 1 : 
// Orbit without singularities:
// ------------------------------------------
a = 7000.e3;
e = 0.01;
inc = 98*%CL_deg2rad;
pom = 0.4;
gom = 0.2;
anm = 0.3;

kep = [a;e;inc;pom;gom;anm];
cir = [a;e*cos(pom);e*sin(pom);inc;gom;pom+anm];
cireq = [a;e*cos(pom+gom);e*sin(pom+gom);sin(inc/2)*cos(gom);sin(inc/2)*sin(gom);pom+gom+anm];
equin = [a;e*cos(pom+gom);e*sin(pom+gom);tan(inc/2)*cos(gom);tan(inc/2)*sin(gom);pom+gom+anm];

// NB: pos,vel obtained with mu = 398600441500000
pos = [5295184.98096561059; 434636.985178904608 ; 4454347.202808531];
vel = [-4666.48766584575969 ; -1771.29109451389013 ; 5755.58855429330561];

// Check: kep <-> cir
[kep1,jacob] = CL_oe_cir2kep(cir);
[cir1,jacob1] = CL_oe_kep2cir(kep1);
TEST_OK($+1) = CL__isEqual(cir, cir1) & ...
               test_jacob(jacob*jacob1,1e-14);

// Check: kep <-> equin
[kep1,jacob] = CL__oe_equin2kep(equin);
[equin1,jacob1] = CL__oe_kep2equin(kep1);
TEST_OK($+1) = CL__isEqual(equin, equin1) & ...
               test_jacob(jacob*jacob1,1e-14);
               
// Check: kep <-> cireq
[kep1,jacob] = CL__oe_cireq2kep(cireq);
[cireq1,jacob1] = CL__oe_kep2cireq(kep1);
TEST_OK($+1) = CL__isEqual(cireq, cireq1) & ...
               test_jacob(jacob*jacob1,1e-14);
               
               
               
// Check: car <-> cir
[pos1,vel1,jacob] = CL_oe_cir2car(cir);
[cir1,jacob1] = CL_oe_car2cir(pos1,vel1);
TEST_OK($+1) = CL__isEqual(cir, cir1, 1e-13) & ...
               test_jacob(jacob*jacob1,1e-12);
          
// Check: car <-> cireq
[pos1,vel1,jacob] = CL__oe_cireq2car(cireq);
[cireq1,jacob1] = CL__oe_car2cireq(pos1,vel1);
TEST_OK($+1) = CL__isEqual(cireq, cireq1, 1e-13) & ...
               test_jacob(jacob*jacob1,1e-12);
               
// Check: car <-> equin
[pos1,vel1,jacob] = CL__oe_equin2car(equin);
[equin1,jacob1] = CL__oe_car2equin(pos1,vel1);
TEST_OK($+1) = CL__isEqual(equin, equin1) & ...
               test_jacob(jacob*jacob1,1e-12);

               
// Check: car <-> kep
[pos1,vel1,jacob] = CL_oe_kep2car(kep);
[kep1,jacob1] = CL_oe_car2kep(pos1,vel1);
TEST_OK($+1) = CL__isEqual(kep, kep1, 1e-13) & ...
               test_jacob(jacob*jacob1,1e-10);

               

               
// ------------------------------------------
// TEST 2 : 
// Near-circular orbit:
// In these tests, when going to keplerian form pom and M are not defined, only pom+M
// Also some terms of the jacobians are not defined.
// The treshold is at ecc = 1e-10
// ------------------------------------------
e = [1e-9,1e-11,1e-13,1e-15,0];
a = 7000.e3 * ones(e);
inc = 98*%CL_deg2rad * ones(e);
pom = 0.4 * ones(e);
gom = 0.2 * ones(e);
anm = 0.3 * ones(e);

kep = [a;e;inc;pom;gom;anm];
cir = [a;e.*cos(pom);e.*sin(pom);inc;gom;pom+anm];
cireq = [a;e.*cos(pom+gom);e.*sin(pom+gom);sin(inc/2).*cos(gom);sin(inc/2).*sin(gom);pom+gom+anm];
equin = [a;e.*cos(pom+gom);e.*sin(pom+gom);tan(inc/2).*cos(gom);tan(inc/2).*sin(gom);pom+gom+anm];

// NB: pos,vel obtained with mu = 398600441500000
pos = [5371859.5965589676 5371859.6041390235 5371859.6042148238 5371859.6042155819 5371859.6042155884
       448560.68831320928 448560.68968897103 448560.68970272865 448560.68970286631 448560.6897028677
       4465637.4337975923 4465637.4349188795 4465637.4349300917 4465637.4349302035 4465637.4349302053];

vel = [ -4604.8188673829927 -4604.818861333616 -4604.8188612731228 -4604.818861272518  -4604.8188612725116
        -1753.0231390887241 -1753.023137289274 -1753.0231372712794 -1753.0231372710998 -1753.0231372710978
         5715.3716760590596  5715.3716720619623 5715.3716720219927  5715.3716720215934  5715.371672021588];
         
// Check: kep <-> cir
// pom is not defined if e < 10-10, only pom+M is
[kep1,jacob] = CL_oe_cir2kep(cir);
TEST_OK($+1) = CL__isEqual(kep([1,2,3,5],:), kep1([1,2,3,5],:)) & ...
               CL__isEqual(kep(4,:)+kep(6,:), kep1(4,:)+kep1(6,:)) ;
[cir1,jacob1] = CL_oe_kep2cir(kep);

TEST_OK($+1) = CL__isEqual(cir, cir1) & ...
               test_jacob(jacob1*jacob,1e-14);

// Check: kep <-> equin
[kep1,jacob] = CL__oe_equin2kep(equin);
TEST_OK($+1) = CL__isEqual(kep([1,2,3,5],:), kep1([1,2,3,5],:)) & ...
               CL__isEqual(kep(4,:)+kep(6,:), kep1(4,:)+kep1(6,:)) ;
               
[equin1,jacob1] = CL__oe_kep2equin(kep);
TEST_OK($+1) = CL__isEqual(equin, equin1) & ...
               test_jacob(jacob*jacob1,1e-14);
            
// Check: kep <-> cireq
[kep1,jacob] = CL__oe_cireq2kep(cireq);
TEST_OK($+1) = CL__isEqual(kep([1,2,3,5],:), kep1([1,2,3,5],:)) & ...
               CL__isEqual(kep(4,:)+kep(6,:), kep1(4,:)+kep1(6,:)) ;

[cireq1,jacob1] = CL__oe_kep2cireq(kep);
TEST_OK($+1) = CL__isEqual(cireq, cireq1) & ...
               test_jacob(jacob*jacob1,1e-14);
            
         
// Check: car <-> cir
// ex and ey are not stable, they wont be tested here
[pos1,vel1,jacob] = CL_oe_cir2car(cir);
[cir1,jacob1] = CL_oe_car2cir(pos1,vel1);
TEST_OK($+1) = CL__isEqual(cir([1,4,5,6],:), cir1([1,4,5,6],:)) & ...
               test_jacob(jacob*jacob1,1e-8);
            
// Check: car <-> cireq
[pos1,vel1,jacob] = CL__oe_cireq2car(cireq);
[cireq1,jacob1] = CL__oe_car2cireq(pos1,vel1);
TEST_OK($+1) = CL__isEqual(cireq([1,4,5,6],:), cireq1([1,4,5,6],:)) & ...
               test_jacob(jacob*jacob1,1e-8);
              
// Check: car <-> equin
[pos1,vel1,jacob] = CL__oe_equin2car(equin);
[equin1,jacob1] = CL__oe_car2equin(pos1,vel1);
TEST_OK($+1) = CL__isEqual(equin([1,4,5,6],:), equin1([1,4,5,6],:)) & ...
               test_jacob(jacob*jacob1,1e-8);
                            
// Check: car <-> kep
// e is not stable, not tested here
[kep1,jacob1] = CL_oe_car2kep(pos,vel);
TEST_OK($+1) = CL__isEqual(kep([1,3,5],:), kep1([1,3,5],:)) & ...
               CL__isEqual(kep(4,:)+kep(6,:), kep1(4,:)+kep1(6,:)) ;
               
[pos1,vel1,jacob] = CL_oe_kep2car(kep);
TEST_OK($+1) = CL__isEqual([pos;vel], [pos1;vel1]) & ...
               test_jacob(jacob*jacob1,1e-3);
             

               
               
               
// ------------------------------------------
// TEST 3 : 
// Near-equatorial orbit:
// In these tests, when going to keplerian form gom is not defined, gom+anm is defined
// Also some terms of the jacobians are not defined.
// The treshold is at inc = 1e-10
// ------------------------------------------
inc = [1e-9,1e-11,1e-13,1e-15,0];
a = 7000.e3 * ones(inc);
e = 0.05 * ones(inc);
pom = 0.4 * ones(inc);
gom = 0.2 * ones(inc);
anm = 0.3 * ones(inc);

kep = [a;e;inc;pom;gom;anm];
cir = [a;e.*cos(pom);e.*sin(pom);inc;gom;pom+anm];
cireq = [a;e.*cos(pom+gom);e.*sin(pom+gom);sin(inc/2).*cos(gom);sin(inc/2).*sin(gom);pom+gom+anm];
equin = [a;e.*cos(pom+gom);e.*sin(pom+gom);tan(inc/2).*cos(gom);tan(inc/2).*sin(gom);pom+gom+anm];

// NB: pos,vel obtained with mu = 398600441500000
pos = [3978341.1001970884 3978341.1001970884 3978341.1001970884 3978341.1001970884 3978341.1001970884
       5350269.8275360623 5350269.8275360623 5350269.8275360623 5350269.8275360623 5350269.8275360623
       0.0044532462763498754 4.4532462763498758e-005 4.4532462763498759e-007 4.4532462763498758e-009 0];

vel = [-6276.3500893422906 -6276.3500893422906 -6276.3500893422906 -6276.3500893422906 -6276.3500893422906
      4820.134003707205 4820.134003707205 4820.134003707205 4820.134003707205 4820.134003707205
      5.9709705098346787e-006 5.9709705098346784e-008 5.9709705098346783e-010 5.9709705098346793e-012 0];
         
// Check: kep <-> cir
// no problem here
[kep1,jacob] = CL_oe_cir2kep(cir);
[cir1,jacob1] = CL_oe_kep2cir(kep);

TEST_OK($+1) = CL__isEqual(cir, cir1) & ...
               CL__isEqual(kep, kep1) & ...
               test_jacob(jacob*jacob1,1e-14);

// Check: kep <-> equin
// gom undefined --> gom+pom is defined
[kep1,jacob] = CL__oe_equin2kep(equin);
TEST_OK($+1) = CL__isEqual(kep([1,2,3,6],:), kep1([1,2,3,6],:)) & ...
               CL__isEqual(kep(4,:)+kep(5,:), kep1(4,:)+kep1(5,:)) ;
               
[equin1,jacob1] = CL__oe_kep2equin(kep);
TEST_OK($+1) = CL__isEqual(equin, equin1) & ...
               test_jacob(jacob*jacob1,1e-14);
            
// Check: kep <-> cireq
[kep1,jacob] = CL__oe_cireq2kep(cireq);
TEST_OK($+1) = CL__isEqual(kep([1,2,3,6],:), kep1([1,2,3,6],:)) & ...
               CL__isEqual(kep(4,:)+kep(5,:), kep1(4,:)+kep1(5,:)) ;

[cireq1,jacob1] = CL__oe_kep2cireq(kep);
TEST_OK($+1) = CL__isEqual(cireq, cireq1) & ...
               test_jacob(jacob1*jacob,1e-14);
            
         
// Check: car <-> cir
// ex and ey take the value: e*cos(pom+gom) and e*sin(pom+gom)
[pos1,vel1,jacob] = CL_oe_cir2car(cir);
[cir1,jacob1] = CL_oe_car2cir(pos1,vel1);
TEST_OK($+1) = CL__isEqual(cir([1,4],:), cir1([1,4],:)) & ...
               CL__isEqual(atan(cir1(3,:),cir1(2,:)) + cir1(5,:), atan(cir(3,:),cir(2,:)) + cir(5,:) ) & ...
               CL__isEqual(cir1(5,:)+cir1(6,:), cir(5,:) + cir(6,:) ) & ...
               test_jacob(jacob1*jacob,1e-7);
            
// Check: car <-> cireq
[pos1,vel1,jacob] = CL__oe_cireq2car(cireq);
[cireq1,jacob1] = CL__oe_car2cireq(pos1,vel1);
TEST_OK($+1) = CL__isEqual(cireq, cireq1, 1e-13) & ...
               test_jacob(jacob*jacob1,1e-11);
              
// Check: car <-> equin
[pos1,vel1,jacob] = CL__oe_equin2car(equin);
[equin1,jacob1] = CL__oe_car2equin(pos1,vel1);
TEST_OK($+1) = CL__isEqual(equin, equin1,1e-13) & ...
               test_jacob(jacob*jacob1,1e-11);
                            
// Check: car <-> kep
// e is not stable, not tested here
[kep1,jacob1] = CL_oe_car2kep(pos,vel);
TEST_OK($+1) = CL__isEqual(kep([1,3,6],:), kep1([1,3,6],:), 1e-13) & ...
               CL__isEqual(kep(4,:)+kep(5,:), kep1(4,:)+kep1(5,:), 1e-13) ;
               
[pos1,vel1,jacob] = CL_oe_kep2car(kep);
TEST_OK($+1) = CL__isEqual([pos;vel], [pos1;vel1]) & ...
               test_jacob(jacob*jacob1,1e-3);
               
               
               
               

               
// ------------------------------------------
// TEST 4 : 
// Near-equatorial and near circular orbit:
// In these tests:
// - when going to keplerian form pom and gom are not defined, pom+gom+anm is.
// - when going to circular form gom is not defined, gom+anm is.
// Also some terms of the jacobians are not defined.
// The treshold is at inc = 1e-10
// ------------------------------------------
inc = [1e-9,1e-11,1e-13,1e-15,0];
a = 7000.e3 * ones(inc);
e = [1e-9,1e-11,1e-13,1e-15,0];
pom = 0.4 * ones(inc);
gom = 0.2 * ones(inc);
anm = 0.3 * ones(inc);

kep = [a;e;inc;pom;gom;anm];
cir = [a;e.*cos(pom);e.*sin(pom);inc;gom;pom+anm];
cireq = [a;e.*cos(pom+gom);e.*sin(pom+gom);sin(inc/2).*cos(gom);sin(inc/2).*sin(gom);pom+gom+anm];
equin = [a;e.*cos(pom+gom);e.*sin(pom+gom);tan(inc/2).*cos(gom);tan(inc/2).*sin(gom);pom+gom+anm];

// NB: pos,vel obtained with mu = 398600441500000
pos = [4351269.7704968788 4351269.7778206728 4351269.7778939111 4351269.7778946431 4351269.7778946506
5483288.3647257751 5483288.3673657188 5483288.3673921172 5483288.3673923817 5483288.3673923844
0.004509523809520093 4.5095238106524007e-005 4.5095238106637229e-007 4.509523810663837e-009 0];

vel = [-5911.0266084330442 -5911.0266014701601 -5911.0266014005301 -5911.0266013998344 -5911.026601399828
4690.7019472016727 4690.7019444946463 4690.7019444675752 4690.7019444673051 4690.7019444673024
5.7715399056763846e-006 5.7715399016400076e-008 5.7715399015996423e-010 5.77153990159924e-012 0];
         

// Check: kep <-> cir
// pom is not defined if e < 10-10, only pom+M is
[kep1,jacob] = CL_oe_cir2kep(cir);
TEST_OK($+1) = CL__isEqual(kep([1,2,3,5],:), kep1([1,2,3,5],:)) & ...
               CL__isEqual(kep(4,:)+kep(6,:), kep1(4,:)+kep1(6,:)) ;
[cir1,jacob1] = CL_oe_kep2cir(kep);

TEST_OK($+1) = CL__isEqual(cir, cir1) & ...
               test_jacob(jacob1*jacob,1e-14);

// Check: kep <-> equin
// pom,gom undefined --> gom+pom+anm is defined
[kep1,jacob] = CL__oe_equin2kep(equin);
TEST_OK($+1) = CL__isEqual(kep([1,2,3],:), kep1([1,2,3],:)) & ...
               CL__isEqual(kep(4,:)+kep(5,:)+kep(6,:), kep1(4,:)+kep1(5,:)+kep1(6,:)) ;
               
[equin1,jacob1] = CL__oe_kep2equin(kep);
TEST_OK($+1) = CL__isEqual(equin, equin1) & ...
               test_jacob(jacob1*jacob,1e-14);
            
// Check: kep <-> cireq
[kep1,jacob] = CL__oe_cireq2kep(cireq);
TEST_OK($+1) = CL__isEqual(kep([1,2,3],:), kep1([1,2,3],:)) & ...
               CL__isEqual(kep(4,:)+kep(5,:)+kep(6,:), kep1(4,:)+kep1(5,:)+kep1(6,:)) ;

[cireq1,jacob1] = CL__oe_kep2cireq(kep);
TEST_OK($+1) = CL__isEqual(cireq, cireq1) & ...
               test_jacob(jacob1*jacob,1e-14);
       
         
// Check: car <-> cir
// ex and ey take the value: e*cos(pom+gom) and e*sin(pom+gom)
[pos1,vel1,jacob] = CL_oe_cir2car(cir);
[cir1,jacob1] = CL_oe_car2cir(pos1,vel1);
TEST_OK($+1) = CL__isEqual(cir([1,4],:), cir1([1,4],:)) & ...
               CL__isEqual(cir1(5,:)+cir1(6,:), cir(5,:) + cir(6,:) ) & ...
               test_jacob(jacob1*jacob,1e-6);
          
// Check: car <-> cireq
// ex and ey not stable numerically, not tested here
[pos1,vel1,jacob] = CL__oe_cireq2car(cireq);
[cireq1,jacob1] = CL__oe_car2cireq(pos1,vel1);
TEST_OK($+1) = CL__isEqual(cireq([1,4,5,6],:), cireq1([1,4,5,6],:), 1e-13) & ...
               test_jacob(jacob*jacob1,1e-11);

// Check: car <-> equin
// ex and ey not stable numerically, not tested here
[pos1,vel1,jacob] = CL__oe_equin2car(equin);
[equin1,jacob1] = CL__oe_car2equin(pos1,vel1);
TEST_OK($+1) = CL__isEqual(equin([1,4,5,6],:), equin1([1,4,5,6],:),1e-13) & ...
               test_jacob(jacob*jacob1,1e-11);
                         
// Check: car <-> kep
// e is not stable, not tested here
[kep1,jacob1] = CL_oe_car2kep(pos,vel);
TEST_OK($+1) = CL__isEqual(kep([1,3],:), kep1([1,3],:), 1e-13) & ...
               CL__isEqual(kep(4,:)+kep(5,:)+kep(6,:), kep1(4,:)+kep1(5,:)+kep1(6,:)) ;
               
[pos1,vel1,jacob] = CL_oe_kep2car(kep);
TEST_OK($+1) = CL__isEqual([pos;vel], [pos1;vel1]) & ...
               test_jacob(jacob*jacob1,1e-3);