// ------------------------------
// CL_rot_defRot2Ax: 
// ------------------------------

TEST_OK = [];
v = [ 3 ; 1 ; 2 ];
n1 = 1;
n2 = 2;
n3 = 3;

// Test 1: input vector u = 0.
u = [ 0 ; 0 ; 0 ];
try 
  CL_rot_defRot2Ax(u, v, n1, n2);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 2: input vector u = [].
u = [];
try 
  CL_rot_defRot2Ax(u, v, n1, n2);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 3: incorrect value of "numsol"
u = [ 1 ; 2 ; 3 ];

numsol = 3;
try 
  CL_rot_defRot2Ax(u, v, n1, n2,numsol);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 4: input axis number incorrect.
n1 = 4;
try 
  CL_rot_defRot2Ax(u, v, n1, n2);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

n1 = 1;
n2 = 2;

//Test 5: Rotation about X axis then Y
[qa, angsa, Inok] = CL_rot_defRot2Ax(u, v, n1, n2);

// Check results :
v2 = CL_rot_rotVect(qa,u) // == v

TEST_OK($+1) = CL__isEqual( v2 , v , 1e-15);

//Test 6: axes = vectors
u = [ 1 ; 2 ; 3 ];
v = [ 3 ; 1 ; 2 ];
axis1 = [ -2 ; 3 ; 4 ];
axis2 = [ 1 ; 0 ; 1 ];
[qa, angsa, qb, angsb, Inok] = CL_rot_defRot2Ax(u, v, axis1, axis2, numsol=2);

// Check results :
v2a = CL_rot_rotVect(qa,u) // == v
v2b = CL_rot_rotVect(qb,u) // == v

TEST_OK($+1) = CL__isEqual( v2a , v , 1e-14);
TEST_OK($+1) = CL__isEqual( v2b , v , 1e-14);

//Test 7: Rotation about Y axis then Z

[qa, angsa, Inok] = CL_rot_defRot2Ax(u, v, n2, n3);

// Check results :
v2 = CL_rot_rotVect(qa,u) // == v

TEST_OK($+1) = CL__isEqual( v2 , v , 1e-15);

//Test 8: Rotation about Z axis then X

[qa, angsa, Inok] = CL_rot_defRot2Ax(u, v, n3, n1);

// Check results :
v2 = CL_rot_rotVect(qa,u) // == v

TEST_OK($+1) = CL__isEqual( v2 , v , 1e-15);

//Test 9: axes = vectors, doubles, two solutions
u = [ 1.22 ; 2.43 ; 3.44 ];
v = [ 3 ; 1 ; 2 ];
axis1 = [ -2 ; 3 ; 4 ];
axis2 = [ 1 ; 0 ; 1 ];
[qa, angsa, qb, angsb, Inok] = CL_rot_defRot2Ax(u, v, axis1, axis2, numsol=2);

// Check results :
unorm = u./CL_norm(u);
v2a = CL_rot_rotVect(qa,unorm); // == v
v2b = CL_rot_rotVect(qb,unorm); // == v
vnorm = v./CL_norm(v);

TEST_OK($+1) = CL__isEqual( v2a , vnorm , 1e-14);
TEST_OK($+1) = CL__isEqual( v2b , vnorm , 1e-14);
