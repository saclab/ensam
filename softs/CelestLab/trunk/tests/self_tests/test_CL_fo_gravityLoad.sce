//----------------------------------------------------------------------------------------
// CL_fo_gravityLoad
// Manual validation. The structure gravmod_ref has been extracted from data file
// egm96s.scd (CelestLab database)
//----------------------------------------------------------------------------------------

TEST_OK = [];

///////////////////////////////////////////////
// Test 1: comparition with file; incel = %t //
///////////////////////////////////////////////
// Reference Values from data file egm96s.scd
gravmod_ref = struct();
gravmod_ref.mu = 398600441500000;
gravmod_ref.er = 6378136.299999999813735;
gravmod_ref.maxdeg = 70;
gravmod_ref.csnm(1,1)= 1;
gravmod_ref.csnm(2,2)= 0;
gravmod_ref.csnm(3,3)= 0.000001574498314321805 - 0.000000903825600858272*%i;
gravmod_ref.csnm(4,4)= 0.000000100560741318712 + 0.000000197194339916211*%i;
gravmod_ref.csnm(5,5)= -0.000000003983668597122 + 0.000000006525434944316*%i;
gravmod_ref.csnm(6,6)= 0.000000000430763237369 - 0.000000001648175982627*%i;
gravmod_ref.csnm(7,7)= 0.000000000002239165250 - 0.000000000055272500145*%i;
gravmod_ref.csnm(8,8)= 0.000000000000025329838 + 0.000000000000449702731*%i;
gravmod_ref.csnm(9,9)= -0.000000000000157629009 + 0.000000000000153467288*%i;
gravmod_ref.csnm(10,10)= -0.000000000000003696616 + 0.000000000000007451191*%i;
gravmod_ref.csnm(11,11)= 0.000000000000000417331 - 0.000000000000000099499*%i;
gravmod_ref.csnm(12,12)= 0.000000000000000009266 - 0.000000000000000014118*%i;
gravmod_ref.csnm(13,13)= -0.000000000000000000023 - 0.000000000000000000101*%i;
gravmod_ref.csnm(14,14)= -0.000000000000000000022 + 0.000000000000000000025*%i;
gravmod_ref.csnm(15,15)= -7.15814252295141366D-22 - 6.62099522174058480D-23*%i;
gravmod_ref.csnm(16,16)= -9.20548479407464646D-24 - 2.26284704375199792D-24*%i;
gravmod_ref.csnm(17,17)= -5.99945966961698202D-25 + 4.42229747363309280D-26*%i;
gravmod_ref.csnm(18,18)= -1.67732332612072372D-26 - 9.60800022701776895D-27*%i;
gravmod_ref.csnm(19,19)= 4.25258090039636406D-29 - 1.55449350383800008D-28*%i;
gravmod_ref.csnm(20,20)= - 8.63994926048871827D-31 + 1.84042008917134844D-30*%i;
gravmod_ref.csnm(21,21)= 3.71368495203720091D-32 - 1.21331545448326373D-31*%i;
gravmod_ref.csnm(22,22)= 2.02356569634371761D-33 - 9.75652068544803404D-34*%i;
gravmod_ref.csnm(23,23)= -5.22111595612307484D-35 + 1.73725716973887438D-35*%i;
gravmod_ref.csnm(24,24)= 3.92484369239927225D-37 - 1.55822433629694274D-36*%i;
gravmod_ref.csnm(25,25)= 3.25692447172254204D-38 - 9.77228643406185577D-39*%i;
gravmod_ref.csnm(26,26)= 6.08802844540390816D-40 + 2.86701399082293628D-40*%i;
gravmod_ref.csnm(27,27)= 6.89333332356924602D-43 + 2.87852244756309701D-42*%i;
gravmod_ref.csnm(28,28)= 1.74896495485177486D-43 + 1.29764188422297516D-44*%i;
gravmod_ref.csnm(29,29)= 2.66044193999915806D-45 + 2.89723516226119995D-45*%i;
gravmod_ref.csnm(30,30)= 8.94941534465487750D-47 - 3.71927273652156961D-47*%i;
gravmod_ref.csnm(31,31)= 3.14213772519313023D-49 + 9.87761456017701750D-49*%i;
gravmod_ref.csnm(32,32)= -1.79174753720453942D-50 - 2.85888611262975189D-51*%i;
gravmod_ref.csnm(33,33)= 6.51605894405800206D-53 - 2.64119174679202803D-53*%i;
gravmod_ref.csnm(34,34)= -7.27926224135293060D-55 + 4.49218284969513409D-54*%i;
gravmod_ref.csnm(35,35)= -4.82144184577145248D-56 + 2.37276914849498218D-56*%i;
gravmod_ref.csnm(36,36)= -6.35423181291605890D-61 - 5.07758630467724590D-58*%i;
gravmod_ref.csnm(37,37)= 5.37511256426199065D-60 - 8.86552166323922154D-60*%i;
gravmod_ref.csnm(38,38)= 7.51202132409081660D-62 + 4.17145267765062448D-62*%i;
gravmod_ref.csnm(39,39)= 5.09869956300540170D-64 - 3.14522928627342011D-64*%i;
gravmod_ref.csnm(40,40)= 7.36622304901781049D-66 + 2.11993652534848611D-66*%i;
gravmod_ref.csnm(41,41)= 4.48312612037526079D-68 - 1.08692795319317357D-68*%i;
gravmod_ref.csnm(42,42)= 2.17805048002230387D-69 + 5.52942245118144642D-69*%i;
gravmod_ref.csnm(43,43)= -4.63562372733570027D-71 + 1.65089487461319771D-71*%i;
gravmod_ref.csnm(44,44)= -2.25725121492661209D-73 - 6.55112426267238454D-73*%i;
gravmod_ref.csnm(45,45)= 1.99569654103544722D-75 - 7.25201889703511455D-76*%i;
gravmod_ref.csnm(46,46)= -4.31182481800374121D-77 + 1.47325291089449201D-77*%i;
gravmod_ref.csnm(47,47)= 1.57045428964281563D-79 - 4.96809493976245896D-79*%i;
gravmod_ref.csnm(48,48)= -1.20034920860094210D-81 - 8.79761255560172087D-82*%i;
gravmod_ref.csnm(49,49)= 3.60391111509006209D-84 + 1.23491242874238533D-85*%i;
gravmod_ref.csnm(50,50)= 1.84979075769642563D-85 + 9.12498770311021860D-86*%i;
gravmod_ref.csnm(51,51)= 9.57101102228966587D-88 + 5.91716835523462859D-88*%i;
gravmod_ref.csnm(52,52)= -4.31408174018145922D-90 - 2.92361691297775961D-90*%i;
gravmod_ref.csnm(53,53)= -4.02666403531735699D-92 + 9.61889701013499864D-92*%i;
gravmod_ref.csnm(54,54)= 1.43600296533793327D-94 - 6.80962348855182885D-94*%i;
gravmod_ref.csnm(55,55)= 2.68333299689781785D-96 + 5.95964498358063523D-96*%i;
gravmod_ref.csnm(56,56)= -7.61349528077084533D-98 - 1.43360166063204576D-97*%i;
gravmod_ref.csnm(57,57)= -1.6617943380689416D-100 - 1.67471208757276611D-99*%i;
gravmod_ref.csnm(58,58)= -2.3421485141525475D-101 - 2.4084614280748129D-102*%i;
gravmod_ref.csnm(59,59)= 4.8332563843336167D-104 - 1.8875334847338867D-104*%i;
gravmod_ref.csnm(60,60)= 4.8407772270917587D-106 - 1.3257515954642653D-106*%i;
gravmod_ref.csnm(61,61)= -9.3059556282911931D-109 - 3.8903445743918859D-108*%i;
gravmod_ref.csnm(62,62)= 9.4197491487964411D-111 - 6.6589359629701758D-111*%i;
gravmod_ref.csnm(63,63)= -1.7295374442869736D-113 + 3.9313177143692489D-113*%i;
gravmod_ref.csnm(64,64)= -3.8897343166363585D-117 + 2.3127049372246811D-115*%i;
gravmod_ref.csnm(65,65)= -3.8178236986909946D-119 + 3.4630299463427349D-118*%i;
gravmod_ref.csnm(66,66)= -1.1221440729714730D-120 + 1.3843778223795967D-120*%i;
gravmod_ref.csnm(67,67)= 1.2678758001359811D-122 - 7.5428727426253659D-122*%i;
gravmod_ref.csnm(68,68)= 1.1190030741340792D-124 + 2.5656342760700186D-124*%i;
gravmod_ref.csnm(69,69)= 3.4534356247464139D-126 - 3.8722638161345826D-127*%i;
gravmod_ref.csnm(70,70)= 5.5183232656722799D-129 - 2.5506458149941019D-128*%i;
gravmod_ref.csnm(71,71)= 2.4255809062024983D-130 + 3.0919426022672698D-130*%i;
gravmod_ref.csnm(9,12)= 0;
gravmod_ref.csnm(50,13)= - 9.160D-29 + 9.329D-29*%i;

// Results
gravmod = CL_fo_gravityLoad("egm96s.scd", %t);

// Check by manual validation
eps = 1.e-16;
for i = 1:size(gravmod_ref.csnm,1)
    gravmod_res(i) = gravmod.csnm(i,i);
    TEST_OK($+1)= abs(real(gravmod_ref.csnm(i,i))-real(gravmod_res(i))) < eps;
    TEST_OK($+1)= abs(imag(gravmod_ref.csnm(i,i))-imag(gravmod_res(i))) < eps;
end
TEST_OK($+1)=CL__isEqual(gravmod_ref.mu,gravmod.mu);
TEST_OK($+1)=CL__isEqual(gravmod_ref.er,gravmod.er);
TEST_OK($+1)=CL__isEqual(gravmod_ref.maxdeg,gravmod.maxdeg);

///////////////////////////////////////
// Test 2: Optional argument 'incel' //
///////////////////////////////////////
// Reference values are considered the same as those used in first test

// Results: incel = %f, the file is looked for in local path. In other words this test
// may work doesn´t matter which workstation is used, the local path chosen is the
// path where CelestLab database is
fname = fullfile(CL_home(), "data", "potential", "egm96s.scd");
gravmod = CL_fo_gravityLoad(fname, %f);

// Check by manual validation
eps = 1.e-16;
for i = 1:size(gravmod_ref.csnm,1)
    gravmod_res(i) = gravmod.csnm(i,i);
    TEST_OK($+1)= abs(real(gravmod_ref.csnm(i,i))-real(gravmod_res(i))) < eps;
    TEST_OK($+1)= abs(imag(gravmod_ref.csnm(i,i))-imag(gravmod_res(i))) < eps;
end
TEST_OK($+1)=CL__isEqual(gravmod_ref.mu,gravmod.mu);
TEST_OK($+1)=CL__isEqual(gravmod_ref.er,gravmod.er);
TEST_OK($+1)=CL__isEqual(gravmod_ref.maxdeg,gravmod.maxdeg);
