//===================================
// CL_3b_lissajous (reference case)
// This test represents a reference case
//===================================

// --------------------------------------
// Load reference results (=> obtained with "CL_3b_lissajous")
// Check non-regression of newer versions
// --------------------------------------
function [orb, omega, nu] = reference_res()
  
  orb = [1.00985437561605380000 1.01006157007640750000 1.01025436593877330000 1.01004417112458200000 1.00985516114843390000 
         -0.00000024544180856403 0.00063739460816011797 -0.00002782020799775097 -0.00063587859588351699 0.00005479400010294884 
         0.00065789678590257505 0.00000300871630043381 -0.00067677076441850477 -0.00008795690638917163 0.00065176408938340529 
         -0.00000062006216132495 0.00041352374794854669 -0.00002164763111957271 -0.00040492859017184367 0.00002806026311796121 
         0.00133197991668360300 -0.00004830552940718552 -0.00129052647765280360 0.00006495138527067556 0.00132719238336283930 
         0.00000017012680396839 -0.00132545011109743930 -0.00008972874417043459 0.00131823183873309140 0.00018127972814875915];  
   
  omega = 2.05701429569612237;
  nu    = 1.98507496371154701; 
endfunction

// Main Code

TEST_OK = [];

//===================================
// Input parameters
//===================================

// Create the env structure for the Sun-Earth-Moon System 
env = CL_3b_environment("S-EM","L2");
// Amplitude in the "x" direction (adimensional)
Ax = -30e6/env.D;
// Phase in the "x" direction 
phix = 0; // [rad]
// Amplitude in the "z" direction (adimensional)
Az = 100e6/env.D;
phiz = 0; // [rad]

// Precision for convergence
epsilon=1e-10;
// Times at which the orbit is computed
t_orb = linspace(0,180*86400,5) * env.OMEGA;;

// Load reference results
[orb_ref, omega_ref, nu_ref] = reference_res();

//===================================
// Compute orbit
//===================================
[orb, omega, nu] = CL_3b_lissajous(env, Ax, phix, Az, phiz, epsilon, t_orb);

TEST_OK($+1) = CL__isEqual(orb, orb_ref, relative = %f);
TEST_OK($+1) = CL__isEqual(omega, omega_ref);
TEST_OK($+1) = CL__isEqual(nu, nu_ref);
