// ------------------------------
// CL_ex_eckHechInfos:
// Comparaison avec l'extrapolation
// ------------------------------

TEST_OK = [];

// Cas 1
sma = 7.e6;
inc = 1.7;
ecc = 0.01;
mean_cir_t1 = [sma; ecc; 0.0;inc; 0.0; 0.0]; // sma, ex, ey, inc, raan, pso
infos_eh = CL_ex_eckHechInfos(mean_cir_t1);
mean_cir_t1 = [sma; infos_eh.exf; infos_eh.eyf;inc; 0.0; 0.0]; // sma, ex, ey, inc, raan, pso
t1 = 12584;
t2 = 12587:0.2:12588;
[mean_cir_t2,osc_cir_t2] = CL_ex_eckHech(t1,mean_cir_t1,t2);

// Verification de dgomdt par difference finie
dgomdt = diff(mean_cir_t2(5,:));
dgomdt = dgomdt(1) / (86400 *0.2);
TEST_OK($+1) = CL__isEqual(dgomdt, infos_eh.dgomdt, 1e-11);

// Verification de dpsodt par difference finie
dpsodt = diff(mean_cir_t2(6,:));
dpsodt = dpsodt(1) / (86400 *0.2);
TEST_OK($+1) = CL__isEqual(dpsodt, infos_eh.dpsodt, 1e-11);

// Verification que ex point et ey point sont bien constants
dexdt = diff(mean_cir_t2(2,:));
deydt = diff(mean_cir_t2(3,:));
TEST_OK($+1) = CL__isEqual(dexdt, zeros(dexdt));
TEST_OK($+1) = CL__isEqual(deydt, zeros(deydt));
