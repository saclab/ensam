// ------------------------------
// CL_interp: 
// Cross-validation of CL_interpLagrange with n=2 
// and CL_interpLin
// ------------------------------

TEST_OK = [];

xref = linspace(0,1, 1000);
yref = rand(3,1000);

x = rand(1,5000);

y = CL_interpLin(xref,yref,x);
y2 = CL_interpLagrange(xref,yref,x,n=2);

// NB: compare with absolute accuracy
TEST_OK($+1) = CL__isEqual(y,y2,relative=%f);
