// ------------------------------
// CL_rad2deg: 
// ------------------------------

TEST_OK = [];
rad = linspace(0, 3.14159, 4);
deg = CL_rad2deg(rad);

deg_res = %CL_rad2deg * rad;
TEST_OK($+1) = CL__isEqual(deg, deg_res);