// ------------------------------
// CL_cor2cov 
// ------------------------------

TEST_OK = [];

// Cas 1: hypermatrix
nfc=4; nhyp=3;
mat = rand(nfc,nfc,nhyp,key="normal");
covm = mat'*mat;  // symetrical
[corm,sd] = CL_cov2cor(covm);
// Retrieving the covariance :
cov2 = CL_cor2cov(corm,sd) ;

TEST_OK($+1) = CL__isEqual( covm, cov2, 1e-15)

// Cas 2: matrix
nfc=3; 
mat = rand(nfc,nfc,key="normal");
covm = mat'*mat;  // symetrical
[corm,sd] = CL_cov2cor(covm);
// Retrieving the covariance :
cov2 = CL_cor2cov(corm,sd) ;

TEST_OK($+1) = CL__isEqual( covm, cov2, 1e-15)

// Cas 3: ncol =/ nrow
nf=3; nc=2;
mat = rand(nf,nc,key="normal");
corm = mat;
try 
  covm = CL_cor2cov(corm,sd) ;
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  
