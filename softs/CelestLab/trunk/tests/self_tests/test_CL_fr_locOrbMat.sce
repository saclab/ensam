// ------------------------------
// Consistency tests of functions :
// CL_fr_locOrbMat
// CL_rot_defFrameVec
// ------------------------------

function [n1_tab,n2_tab,n3_tab,frame_name] = reperes_direct(type_axes)

  n1_tab = [];
  n2_tab = [];
  n3_tab = [];
  frame_name = [];
  
  if (type_axes == "qsw")
    nom_axes = ["Q","S","W" ;
                "" , "", "";
               "q" ,"s","w"];
  elseif (type_axes == "tnw")
    nom_axes = ["T","N","W";
                "" , "", "";
                "t","n","w"];
  end
  
  n_tab = [ [-3:-1] , [1:3] ];
  M = eye(3,3);
  for n1 = n_tab
    for n2 = n_tab
      for n3 = n_tab
        if (abs(n1) <> abs(n2) & abs(n1) <> abs(n3) & abs(n2) <> abs(n3))
          i = sign(n1)* M(:,abs(n1));
          j = sign(n2)* M(:,abs(n2));
          k = sign(n3)* M(:,abs(n3));
          // Repere direct
          if ( find( CL_cross(i,j) <> k ) == [] )
            n1_tab = [n1_tab, n1];
            n2_tab = [n2_tab, n2];
            n3_tab = [n3_tab, n3];
            frame_name = [frame_name , nom_axes(sign(n1)+2,abs(n1))+nom_axes(sign(n2)+2,abs(n2))+nom_axes(sign(n3)+2,abs(n3)) ];  
          end
        end
      end
    end
  end
endfunction

  
TEST_OK = [];

pos_car = [3500.e3;2500.e3;5800.e3];
vel_car = [1.e3;3.e3;7.e3]; 
cross_car = CL_cross(pos_car,vel_car);

Id = eye(3,3);


// Reperes direct base sur des combinaisons du repere qsw :
type_frame = "qsw";
mat_car = [pos_car, CL_cross(cross_car,pos_car) , cross_car];

[n1,n2,n3,frame_name] = reperes_direct(type_frame);  

for k = 1 : size(n1,2);
  M = CL_fr_locOrbMat(pos_car,vel_car,frame_name(k));
  
  v1 = sign(n1(k)) * mat_car(:,abs(n1(k)));
  v2 = sign(n2(k)) * mat_car(:,abs(n2(k)));
  
  M2 = CL_rot_defFrameVec(v1, v2, 1, 2);
  
  //[u,alpha] = CL_rot_quat2axAng(CL_rot_matrix2quat(M*M2'));
  
  if (CL__isEqual( Id , M*M2') == %t); 
    TEST_OK($+1) = %t;
  else
    TEST_OK($+1) = %f;
  end
end




// Reperes direct base sur des combinaisons du repere tnw :
type_frame = "tnw";
mat_car = [vel_car, CL_cross(cross_car,vel_car) , cross_car];

[n1,n2,n3,frame_name] = reperes_direct(type_frame);  

for k = 1 : size(n1,2);
  M = CL_fr_locOrbMat(pos_car,vel_car,frame_name(k));
  
  v1 = sign(n1(k)) * mat_car(:,abs(n1(k)));
  v2 = sign(n2(k)) * mat_car(:,abs(n2(k)));
  
  M2 = CL_rot_defFrameVec(v1, v2, 1, 2);
  
  //[u,alpha] = CL_rot_quat2axAng(CL_rot_matrix2quat(M*M2'));
  
  if (CL__isEqual( Id , M*M2') == %t); 
    TEST_OK($+1) = %t;
  else
    TEST_OK($+1) = %f;
  end
end







