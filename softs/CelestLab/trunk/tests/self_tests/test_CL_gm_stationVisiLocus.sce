// ---------------------------------------------------------
//
// Consistency tests between CL_gm_stationVisiLocus and CL_fr_topoNMat functions
//
// ---------------------------------------------------------

TEST_OK = [];


//---TEST 1---
// Simple test: one station, one position

// Station position 
station = [0.1 ; 0.4 ; 100]; 

// Conversion from ECF to topoN:
M = CL_fr_topoNMat(station);
pos = [ 1000.e3 ; 6578.e3 ; 2000.e3 ]; 

pos_sta = CL_co_ell2car(station); // station in cartesian coord.
pos_topoN = M * (pos - pos_sta); //  sat position relative to station (Topocentric coordinates)

pos_topoN_sph = CL_co_car2sph(pos_topoN); 

azim = pos_topoN_sph(1); 
elev = pos_topoN_sph(2); 
rsat = CL_norm(pos); 

pos2 = CL_gm_stationVisiLocus(station, azim, elev, rsat);

TEST_OK($+1) = CL__isEqual(pos2, pos, 1.e-14);




 


