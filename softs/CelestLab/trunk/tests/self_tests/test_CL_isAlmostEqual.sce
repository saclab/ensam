//=======================================
// Test CL_isAlmostEqual
// This test represents a reference case
//=======================================

TEST_OK = [];

//===================================
// Case with %nan
//===================================
// 1st verification : A and B1 are different because relative error of 2nd column elements greater than the default value rtol=1e-14
// 2nd verification : A and B1 are equal : %nan in the same place, and elements error less than rtol=1e-12
// 3rd and 4th verification : The same applies to case with crit="CLnorm"
// 5th verifcation : A and B2 are different : %nan in different place and 2nd column elements error greater than rtol
// 6th verifcation : A and B2 are different : although 2nd column elements error less than rtol, %nan in different place
A = [%nan 1.0+1e-13
     2.2  5.0];
B1 = [%nan 1.0
      2.2  5.0];
B2 = [2.2  1.0
      %nan 5.0];

TEST_OK($+1) = (CL_isAlmostEqual(A,B1)==%f) & ..
               (CL_isAlmostEqual(A,B1,rtol=1e-12)==%t) & ..
               (CL_isAlmostEqual(A,B1,crit="CLnorm")==%f) & ..
               (CL_isAlmostEqual(A,B1,rtol=1e-12,crit="CLnorm")==%t) & ..
               (CL_isAlmostEqual(A,B2)==%f) & ..
               (CL_isAlmostEqual(A,B2,rtol=1e-12)==%f);
               
//===================================
// Case with structures
//===================================
// 1st verification : A and B are different, error in field1 elements greater than rtol
// 2nd verification : A and B are equal, error in field1 elements less than rtol
// 3rd verification : A and B2 are different, same elements in all fields, but one additional field in B2, not present in A
A = struct("field1",[5.0, 2.0, 3.5], "field2",3.0,"field3",[1.0, 2.0; 3.1,3.2]);
B1 = struct("field1",[5.0, 2.0+1e-13, 3.5], "field2",3.0,"field3",[1.0, 2.0; 3.1,3.2]);
B2 = struct("field1",[5.0, 2.0, 3.5], "field2",3.0,"field3",[1.0, 2.0; 3.1,3.2], "field4",3.0);

TEST_OK($+1) = (CL_isAlmostEqual(A,B1)==%f) & ..
               (CL_isAlmostEqual(A,B1,rtol=1e-12)==%t) & ..
               (CL_isAlmostEqual(A,B2)==%f);
               
//==============================================
// Case with relative and/or absolute precision
//==============================================
// 1st and 2nd verification: absolute error between A and B = 1e-10
// 3rd and 4th verification: relative error between A and B = 9.9e-11
// 5th verification : If we are within rtol and atol, we are within rtol+atol
// 6th verification : Althoug we are neither within rtol nor within atol, we are within rtol+atol
// 7th verification : atol is decreased, so that we are not within rtol+atol
A = 1.0083285918435272;
B = 1.0083285919435272;

TEST_OK($+1) = (CL_isAlmostEqual(A,B,atol=1e-10)==%f) & ..
               (CL_isAlmostEqual(A,B,atol=1e-9)==%t) & ..
               (CL_isAlmostEqual(A,B,rtol=1e-10)==%t) & ..
               (CL_isAlmostEqual(A,B,rtol=1e-11)==%f) & ..
               (CL_isAlmostEqual(A,B,rtol=1e-10,atol=1e-9)==%t) & ..
               (CL_isAlmostEqual(A,B,rtol=1e-11,atol=1e-10)==%t)& ..
               (CL_isAlmostEqual(A,B,rtol=1e-11,atol=1e-11)==%f);
               
//===================================
// Case with element/CLnorm
//===================================
// Relative error with CL_norm = 7.8e-6 : 1st and 2nd verification
// Relatvie error with element = 0.2    : 3rd and 4th verification
A = [1.00832859184352720000 1.00832860993652270000 1.00832863381948010000 
     0.00000090946122728445 -0.00001645984562231271 -0.00003382882992563837 
     0.00090912948366442899 0.00090912093711474596 0.00090909587336464933 
     0.00000883513462862202 0.00000520063346042454 0.00001556681684803647 
     0.01009723922231842000 0.01009713429170516600 0.01009686421507134300 
     0.00000016730885013309 0.00000976931180597767 0.00001937098351596854];
  
B = [1.00832075609394110000 1.00832074365179420000 1.00832073672218910000 
     0.00000090946122728445 -0.00001828703892090401 -0.00003566437102078844 
     0.00090893092029665308 0.00090892293164349504 0.00090889839028530353 
     0.00000883513462862202 0.00000563069090069400 0.00001242599414500906 
     0.01010202490278091900 0.01010196468371582200 0.01010173936417770900 
     0.00000016730885013309 0.00000945529275217921 0.00001907764515803583];
     
TEST_OK($+1) = (CL_isAlmostEqual(A,B,rtol=1e-5,crit="CLnorm")==%t) & ..
               (CL_isAlmostEqual(A,B,rtol=1e-6,crit="CLnorm")==%f) & ..
               (CL_isAlmostEqual(A,B,rtol=1,crit="element")==%t)& ..
               (CL_isAlmostEqual(A,B,rtol=0.1,crit="element")==%f);
