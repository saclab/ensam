//--------------------------------------------------------------------------
// CL_rot_pvJacobian
//--------------------------------------------------------------------------

TEST_OK = [];

////////////////////////////////////////////////////
// Test1: Validation with a simple transformation //
////////////////////////////////////////////////////
// Note: Rotation around Z-axis

M = CL_rot_angles2matrix(3, %pi/2);
omega = [0; 0; 2*%pi/86400];

// Reference values
[jacob_ref] = [ 0. 1. 0. 0. 0. 0.;  
                 -1. 0. 0. 0. 0. 0.;  
                 0. 0. 1. 0. 0. 0.;  
                 -0.000072722052166430395 0. 0. 0. 1. 0.;  
                 0. -0.000072722052166430395 0. -1. 0. 0.;  
                 0. 0. 0. 0. 0. 1.];

// Result values
[jacob_res] = CL_rot_pvJacobian(M, omega);

// Check
eps = 1.e-12;
for i=1:6
    for j=1:6
        TEST_OK($+1) = abs(jacob_ref(i,j)-jacob_res(i,j)) < eps ;
    end
end
