// ---------------------------------------------------------
// CL_mean
// ---------------------------------------------------------

TEST_OK=[];

// Sinus function averaged on one period 
t = linspace(0, 2*%pi, 201);
mn(1)=CL_mean(sin(t), "trap"); //trapezoidal method
mn(2)=CL_mean(sin(t), "simp"); // simpson's rule
mn(3)=CL_mean(sin(t), "boole"); // boole method

TEST_OK($+1)=CL__isEqual(mn(1),0);
TEST_OK($+1)=CL__isEqual(mn(2),0);
TEST_OK($+1)=CL__isEqual(mn(3),0);

// Linear function y=x in [0,1]
t = linspace(0, 1, 101);
y=t;
mn(1)=CL_mean(y, "trap"); //trapezoidal method
mn(2)=CL_mean(y, "simp"); // simpson's rule
mn(3)=CL_mean(y, "boole"); // boole method

TEST_OK($+1)=CL__isEqual(mn(1),0.5);
TEST_OK($+1)=CL__isEqual(mn(2),0.5);
TEST_OK($+1)=CL__isEqual(mn(3),0.5);