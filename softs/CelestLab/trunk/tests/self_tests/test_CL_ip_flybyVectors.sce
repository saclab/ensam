// =================
// TEST 
// CL_ip_flybyVectors
//
// Test 1:
// 1) compute all results from reference pinfva, vinfva (perpendicular).
// 2) compute all results using position and velocity at periapsis: compare with results of 1).
// 3) compute all results using arrival and departure excess velocity: compare with results of 1).
//
// Test 2:
// 1) compute position and velocity at periapsis from pinfva, vinfva (non perpendicular): compare with results of Test1 1).
// =================

TEST_OK = [];

// =================================
// Test 1
// =================================
pinfva_ref = [1; 2; 3];
vinfva_ref = [1; 1; -1];
mu = 2;

all_res = ["vinfva", "vinfvd", "pinfva", "pinfvd", "posp", "velp"];

// --------------------
// 1) Impact quantities from pinfva vinfva (reference data)
// Nota: Computed quantities become new reference data for the test
// --------------------
[vinfva0, vinfvd0, pinfva0, pinfvd0, posp0, velp0] =  CL_ip_flybyVectors("pvinfa", pinfva_ref, vinfva_ref, all_res, mu = mu);

TEST_OK($+1)  = CL__isEqual(pinfva0, pinfva_ref);
TEST_OK($+1)  = CL__isEqual(vinfva0, vinfva_ref);

// --------------------
// 2) Impact quantities from position and velocity at periapsis
// --------------------
[vinfva, vinfvd, pinfva, pinfvd, posp, velp] = CL_ip_flybyVectors("pv", posp0, velp0, all_res, mu = mu);

TEST_OK($+1)  = CL__isEqual(vinfva, vinfva0);
TEST_OK($+1)  = CL__isEqual(vinfvd, vinfvd0);
TEST_OK($+1)  = CL__isEqual(pinfva, pinfva0);
TEST_OK($+1)  = CL__isEqual(pinfvd, pinfvd0);
TEST_OK($+1)  = CL__isEqual(posp, posp0);
TEST_OK($+1)  = CL__isEqual(velp, velp0);

// --------------------
// 3) Impact quantities from position and velocity at periapsis
// --------------------
[vinfva, vinfvd, pinfva, pinfvd, posp, velp] =  CL_ip_flybyVectors("vvinf", vinfva0, vinfvd0, all_res, mu = mu);

TEST_OK($+1)  = CL__isEqual(vinfva, vinfva0);
TEST_OK($+1)  = CL__isEqual(vinfvd, vinfvd0);
TEST_OK($+1)  = CL__isEqual(pinfva, pinfva0);
TEST_OK($+1)  = CL__isEqual(pinfvd, pinfvd0);
TEST_OK($+1)  = CL__isEqual(posp, posp0);
TEST_OK($+1)  = CL__isEqual(velp, velp0);

// =================================
// Test 2
// =================================
// --------------------
// 1) Position and velocity at periapsis from pinfva, vinfva
// Note: pinfva, vinfva are not perpendicular in this case (pinfva =  pinfva_ref + 3 * vinfva_ref)
// --------------------
[posp, velp] =  CL_ip_flybyVectors("pvinfa", pinfva_ref + 3 * vinfva_ref, vinfva_ref, ["posp", "velp"], mu = mu);

TEST_OK($+1)  = CL__isEqual(posp, posp0);
TEST_OK($+1)  = CL__isEqual(velp, velp0);
