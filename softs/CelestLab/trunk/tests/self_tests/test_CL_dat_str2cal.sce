// ------------------------------
// CL_dat_str2cal: 
// ------------------------------

TEST_OK = [];

// Cas 1: Nominal case.
year = 2012;
month = 12;
day = 2;
hour = 21;
mn = 51;
seconds = 13;

str = CL_dat_cal2str([year;month;day;hour;mn;seconds]);

cal = CL_dat_str2cal(str);

TEST_OK($+1) = CL__isEqual( year , cal(1));
TEST_OK($+1) = CL__isEqual( month , cal(2));
TEST_OK($+1) = CL__isEqual( day , cal(3));
TEST_OK($+1) = CL__isEqual( hour , cal(4));
TEST_OK($+1) = CL__isEqual( mn , cal(5));
TEST_OK($+1) = CL__isEqual( seconds , cal(6));

// Cas 2: Incorrect dimension for str.
str1 = ["2012/1/1 12:0:0.1";"2013/2/26 12:0:0.1"];
try 
  cal = CL_dat_str2cal(str1);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Cas 3: str empty.
str2 = [];
cal = CL_dat_str2cal(str2);
TEST_OK($+1) = (cal == []);

// Cas 4: cal out of limits.
str3 = ["2012/15/21 14:40:0.1"];
cal = CL_dat_str2cal(str3);
TEST_OK($+1) = isnan(cal(2));

// Cas 5: cal out of limits.
str4 = ["2012/12/21 14:40:-0.1"];
cal = CL_dat_str2cal(str4);
TEST_OK($+1) = isnan(cal(6));



