// ----------------------------
// CL_gm_inters3dConesSa :
// ----------------------------

TEST_OK = [];

// Test 1 : alpha <= alpha2 - alpha1: the smaller cone is included in the bigger cone
// In this case the intersection is the smaller cone, so the solid angle is calculated as
// 2*pi*(1-cos(alpha1))
alpha1 = CL_deg2rad(50);
alpha2 = CL_deg2rad(70);
alpha = CL_deg2rad(15);

sang_1 = CL_gm_inters3dConesSa(alpha, alpha1, alpha2);
sang_2 = 2*%pi*(1-cos(alpha1));
TEST_OK($+1) = CL__isEqual(sang_1, sang_2,1e-15);

//Test 2 : alpha > alpha2 + alpha1: there is no intersection
// 2*pi*(1-cos(alpha1))
alpha1 = CL_deg2rad(50);
alpha2 = CL_deg2rad(70);
alpha = CL_deg2rad(140);

sang = CL_gm_inters3dConesSa(alpha, alpha1, alpha2);
TEST_OK($+1) = (sang == 0);

// Test 3 : standard case: alpha < alpha2 + alpha1 & alpha > alpha2 - alpha1
alpha = CL_deg2rad(50);
alpha1 = CL_deg2rad(70);
alpha2 = CL_deg2rad(40);

param1=atan((cos(alpha2)-cos(alpha)*cos(alpha1))/(sin(alpha)*cos(alpha1)));
param2=atan((cos(alpha1)-cos(alpha)*cos(alpha2))/(sin(alpha)*cos(alpha2)));

sang_1 = CL_gm_inters3dConesSa(alpha, alpha1, alpha2);
sang_2 = 2*(acos(sin(param1)/sin(alpha1))-acos(tan(param1)/(tan(alpha1)))...
    *cos(alpha1))+2*(acos(sin(param2)/sin(alpha2))-acos(tan(param2)/...
    (tan(alpha2)))*cos(alpha2));// Solid Angle of Conical Surfaces, Polyhedral Cones,
                                // and Intersecting Spherical Caps, Oleg Mazonka 2011.
                                
TEST_OK($+1) = (sang_1 == sang_2);

// Test 4 : erreur de domain of validity for angles
alpha1 = CL_deg2rad(100);
alpha2 = CL_deg2rad(120);
alpha = CL_deg2rad(190);

try 
  CL_gm_inters3dConesSa(alpha, alpha1, alpha2);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  
