// ------------------------------
// CL_deg2rad: 
// ------------------------------

deg = linspace(0, 180, 4);
rad = CL_deg2rad(deg);

rad_res = %CL_deg2rad * deg;
TEST_OK = CL__isEqual(rad, rad_res);