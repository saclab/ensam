// ------------------------------
// CL_integrate : 
// ------------------------------


TEST_OK = [];

// ---------------------------------------------------
// --- Test 1 : Integration une fois, methode trapeze
// ---------------------------------------------------
N = 100;
n = 1;
x = [[1:N] ; [1:N]];
ydot = rand(2,N);
// NB : yini = 0 par defaut.
y = CL_integrate(x, ydot, meth="lin", n);

yref = zeros(y);
for j = 1 : 2
  for k = 2 : N
    yref(j,k) = inttrap(x(j,1:k),ydot(j,1:k));
  end
end

TEST_OK($+1) = CL__isEqual(y,yref);


// ---------------------------------------------------
// --- Test 2 : Integration une fois, methode step_m
// ---------------------------------------------------
N = 100;
n = 1;
x = [[1:N] ; [1:N]];
ydot = rand(2,N);
// NB : yini = 0 par defaut.
y = CL_integrate(x, ydot, meth="step_m", n);

yref = zeros(y);
for j = 1 : 2
  for k = 2 : N
    yref(j,k) = inttrap(x(j,1:k),ydot(j,1:k));
  end
end

TEST_OK($+1) = CL__isEqual(y,yref);


// ---------------------------------------------------
// --- Test 3 : Integration deux fois, methode lin
// ---------------------------------------------------
N = 100000;
n = 2;
x = [linspace(0,2*%pi,N) ; linspace(0,2*%pi,N)];
ydot2 = sin(2*x);
yini = [ [0;0] , [-1/2;-1/2] ]; // Conditions initiales, y0, ydot0
[y,ydot] = CL_integrate(x, ydot2, yini, meth="lin", n);
ydotref1 = CL_integrate(x, ydot2, yini(:,2), meth="lin", n=1);

ydotref = -1/2 * cos(2*x);
yref = -1/4 * sin(2*x);

TEST_OK($+1) = find(abs(y-yref) > 1e-7) == [];
TEST_OK($+1) = find(abs(ydot-ydotref) > 1e-7) == [];
TEST_OK($+1) = CL__isEqual(ydot,ydotref1);



// ---------------------------------------------------
// --- Test 4 : Integration deux fois, methode step_m
// ---------------------------------------------------
N = 100000;
n = 2;
x = [linspace(0,2*%pi,N) ; linspace(0,2*%pi,N)];
ydot2 = sin(2*x);
yini = [ [0;0] , [-1/2;-1/2] ]; // Conditions initiales, y0, ydot0
[y,ydot] = CL_integrate(x, ydot2, yini, meth="step_m", n);
ydotref1 = CL_integrate(x, ydot2, yini(:,2), meth="step_m", n=1);

ydotref = -1/2 * cos(2*x);
yref = -1/4 * sin(2*x);

TEST_OK($+1) = find(abs(y-yref) > 1e-7) == [];
TEST_OK($+1) = find(abs(ydot-ydotref) > 1e-7) == [];
TEST_OK($+1) = CL__isEqual(ydot,ydotref1);

// ---------------------------------------------------
// --- Test 4 : Integration deux fois, methode step_l
// ---------------------------------------------------
N = 100000;
n = 2;
x = [linspace(0,2*%pi,N) ; linspace(0,2*%pi,N)];
ydot2 = sin(2*x);
yini = [ [0;0] , [-1/2;-1/2] ]; // Conditions initiales, y0, ydot0
[y,ydot] = CL_integrate(x, ydot2, yini, meth="step_l", n);
ydotref1 = CL_integrate(x, ydot2, yini(:,2), meth="step_l", n=1);

ydotref = -1/2 * cos(2*x);
yref = -1/4 * sin(2*x);

TEST_OK($+1) = find(abs(y-yref) > 1e-3) == [];
TEST_OK($+1) = find(abs(ydot-ydotref) > 1e-3) == [];
TEST_OK($+1) = CL__isEqual(ydot,ydotref1);


// ---------------------------------------------------
// --- Test 5 : Verification que la vectorisation fonctionne correctement
// ---------------------------------------------------
meth_tab = ["step_m", "step_l", "lin", "sl4", "bl6", "sspl"];
n_tab = [1 ,2];

N_tab = [0,1,100];
P_tab = [0,1,3];

for meth = meth_tab
  for n = n_tab
    for N = N_tab
      for P = P_tab
        x = 1:N;
        ydotn = rand(1,N);
        [y, ydot] = CL_integrate(x,ydotn,meth=meth);
      end
    end
  end
end



// ---------------------------------------------------
// --- Test 6 : Integration une fois, methode Simpson + Lagrange(sl4)
// ---------------------------------------------------

x=[0:0.001:%pi];
ydot = sin(x);
n=1;
// NB : yini = 0 par defaut.
y = CL_integrate(x, ydot, meth="sl4",n);

yref = zeros(y);
  for k = 2 : length(x)
    yref(k) = inttrap(x(1:k),ydot(1:k));
  end

eps=yref($)-y($);
TEST_OK($+1) = CL__isEqual(y,yref,1e-7);

// ---------------------------------------------------
// --- Test 7 : Integration une fois, methode de Boole (bl6)
// ---------------------------------------------------

x=[0:0.001:%pi];
ydot = cos(x);
n=1;
// NB : yini = 0 par defaut.
y = CL_integrate(x, ydot, meth="bl6",n);

yref = zeros(y);
  for k = 2 : length(x)
    yref(k) = inttrap(x(1:k),ydot(1:k));
  end

eps=yref($)-y($);
TEST_OK($+1) = CL__isEqual(y,yref,1e-7);

// ---------------------------------------------------
// --- Test 8 : Integration une fois, methode de Simpson + splines(sspl)
// ---------------------------------------------------

x=[0:0.001:%pi];
ydot = cos(x);
n=1;
// NB : yini = 0 par defaut.
y = CL_integrate(x, ydot, meth="sspl",n);

yref = zeros(y);
  for k = 2 : length(x)
    yref(k) = inttrap(x(1:k),ydot(1:k));
  end

eps=yref($)-y($);
TEST_OK($+1) = CL__isEqual(y,yref,1e-7);

// ---------------------------------------------------
// --- Test 9 : Integration deux fois, methode sspl
// ---------------------------------------------------
N = 100;
n = 2;
x = [linspace(0,2*%pi,N) ; linspace(0,2*%pi,N)];
ydot2 = sin(2*x);
yini = [ [0;0] , [-1/2;-1/2] ]; // Conditions initiales, y0, ydot0
[y,ydot] = CL_integrate(x, ydot2, yini, meth="sspl", n);
ydotref1 = CL_integrate(x, ydot2, yini(:,2), meth="sspl", n=1);

ydotref = -1/2 * cos(2*x);
yref = -1/4 * sin(2*x);

//TEST_OK($+1) = find(abs(y-yref) > 1e-3) == [];
//TEST_OK($+1) = find(abs(ydot-ydotref) > 1e-3) == [];
TEST_OK($+1) = CL__isEqual(ydot,ydotref1,1e-7);

// ---------------------------------------------------
// --- Test 10 : Integration deux fois, methode bl6
// ---------------------------------------------------
N = 100;
n = 2;
x = [linspace(0,2*%pi,N) ; linspace(0,2*%pi,N)];
ydot2 = sin(2*x);
yini = [ [0;0] , [-1/2;-1/2] ]; // Conditions initiales, y0, ydot0
[y,ydot] = CL_integrate(x, ydot2, yini, meth="bl6", n);
ydotref1 = CL_integrate(x, ydot2, yini(:,2), meth="bl6", n=1);

ydotref = -1/2 * cos(2*x);
yref    = -1/4 * sin(2*x);

//TEST_OK($+1) = find(abs(y-yref) > 1e-3) == [];
//TEST_OK($+1) = find(abs(ydot-ydotref) > 1e-3) == [];
TEST_OK($+1) = CL__isEqual(ydot,ydotref1,1e-7);
