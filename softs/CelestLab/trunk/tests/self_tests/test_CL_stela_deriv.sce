//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// ------------------------------
// CL_stela_deriv
// Only checks the correct behaviour when changing some arguments
// (frame or type of orbital elements) 
// ------------------------------

TEST_OK = [];

// generate reference orbit (ECI)
kep0 = [7.e6; 1.136e-3; 1.7085241; %pi/2; 1; 2];
cjd0 = 20000;
cjd = cjd0 + (0:10); // days
kep = CL_ex_propagate("eckhech", "kep", cjd0, kep0, cjd, "m");

cst = CL_stela_getConst(); 

[pv, dpvdkep] = CL_oe_convert("kep", "pv", kep, mu=cst.mu);

// STELA model parameters 
params = CL_stela_params();


//=================
// TEST 1 
// Coherence between various frames
//=================

// Derivatives (ECI)
dpvdt = CL_stela_deriv("pv", cjd, pv, params, frame = "ECI", res = "m");

// Conversion ECI -> MOD
[pos_mod, vel_mod, jacob] = CL_fr_convert("ECI", "MOD", cjd, pv(1:3,:), pv(4:6,:));

dpvdt_mod = CL_stela_deriv("pv", cjd, [pos_mod; vel_mod], params, frame = "MOD", res = "m");

// Time derivatives in MOD using jacobian matrix (dpv_mod = J * dpv_eci)
dpvdt_mod2 = jacob * dpvdt;

// relative error
TEST_OK($+1) = CL__isEqual(dpvdt_mod2, dpvdt_mod, prec = 1.e-12);


//=================
// TEST2 
// Coherence between various orbital elements types
//=================

// kep / pv
dkepdt = CL_stela_deriv("kep", cjd, kep, params, res = "m");

dpvdt  = CL_stela_deriv("pv", cjd, pv, params, res = "m");

dpvdt2 = dpvdkep * dkepdt;

// relative error
TEST_OK($+1) = CL__isEqual(dpvdt, dpvdt2, prec = 1.e-10);
