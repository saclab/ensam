// ------------------------------
// CL_oe_kep2cir
// ------------------------------

TEST_OK = [];

// Verification que kep->cir identique a kep->car->cir
// Orbite quelconque
kep = [ 7078.e3; 0.001 ; 98*%CL_deg2rad ; 0.2 ; 0.4 ; 3 ] * ones(1,2);
[cir,jac1] = CL_oe_kep2cir(kep);
[pos,vel,jac2] = CL_oe_kep2car(kep);
[cir2,jac3] = CL_oe_car2cir(pos,vel);
TEST_OK($+1) = CL__isEqual(cir,cir2,1e-12) & CL__isEqual(jac1 , jac3*jac2, 1e-8) ;