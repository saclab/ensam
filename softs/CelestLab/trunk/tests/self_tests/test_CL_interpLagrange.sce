// ------------------------------
// CL_interpLagrange : 
// ------------------------------


TEST_OK = [];

////////////////////////////////////////////////////////
// Test 1 :(exact interpolation), default mode //
////////////////////////////////////////////////////////

// Reference abscissae
xref = 1:6; 
// Reference ordinates
yref = [xref; xref.^2; xref.^3];
// Interpolation points 
x = 0:0.5:7; 
[y] = CL_interpLagrange(xref, yref, x, n=4);

// Expected value at x
y_expected = [x; x.^2; x.^3]; 
I = find(x < xref(1) | x > xref($));

// if x is not in [x_ref(1), x_ref($)] => y_expected(x) = %nan  
y_expected(:,I) = %nan;

TEST_OK($+1) = CL__isEqual(y,y_expected);

/////////////////////////////////////////////////////////
// Test 2 :(Exact interpolation), opt = 1 (strict) //
// strict => (= optimal) mode. The allowed range for x is [xref(n/2), xref($-n/2+1)]
/////////////////////////////////////////////////////////
xref = 1:6; 
yref = [xref; xref.^2; xref.^3]; 
x = 0:0.5:7; 
[y] = CL_interpLagrange(xref, yref, x, n=4, opt=1);

// Expected value at x
y_expected = [x; x.^2; x.^3];

// Number of interpolation points 
n = 4;
I = find(x < xref(n/2) | x > xref($ - n/2 +1));
// if x is not in [xref(n/2), xref($ - n/2 +1)] => y_expected(x) = %nan  
y_expected(:,I) = %nan;

TEST_OK($+1) = CL__isEqual(y,y_expected);

///////////////////////////////////////
// Test 3 :(perfo et effet de "opt") //
///////////////////////////////////////
Nref = 101; 
N = 100001; 

xref = linspace(0,1,Nref); 
yref = [sin(2*%pi*xref); cos(2*%pi*xref)]; 

x = linspace(0,1,N); 

[y] = CL_interpLagrange(xref, yref, x, opt=0); 
TEST_OK($+1) = CL__isEqual(y,[sin(2*%pi*x); cos(2*%pi*x)], 1e-10);

[y] = CL_interpLagrange(xref, yref, x, opt=1);
I = find(~isnan(y(1,:)));
TEST_OK($+1) = CL__isEqual(y(:,I),[sin(2*%pi*x(I)); cos(2*%pi*x(I))], 5e-12);

/////////////////////////////////////////////////////////////////////////////
// Test 4: Cross-validation of CL_interpLagrange with n=2 and CL_interpLin //
/////////////////////////////////////////////////////////////////////////////
xref = linspace(0,1, 1000);
yref = rand(3,1000);

x = rand(1,5000);
y = CL_interpLin(xref,yref,x);
[y2,y2dot] = CL_interpLagrange(xref,yref,x,n=2,nder=1);

//// NB: compare with absolute accuracy
TEST_OK($+1) = CL__isEqual(y,y2,relative=%f);

/////////////////////////////////////////////////
// Test 5: Invalid value for opt (opt = [0,1]) //
/////////////////////////////////////////////////
xref = 1:6; 
yref = [xref; xref.^2; xref.^3]; 
x = 0:0.5:7;
try
    [y] = CL_interpLagrange(xref, yref, x, n=4, opt=2);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

/////////////////////////////////////////////
// Test 6: Invalid value for n (n = [2,6]) //
/////////////////////////////////////////////
xref = 1:6; 
yref = [xref; xref.^2; xref.^3]; 
x = 0:0.5:7;
try
    [y] = CL_interpLagrange(xref, yref, x, n=20, opt=1);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

///////////////////////////////////////////////////
// Test 7: Invalid value for nder (nder = [0,1]) //
///////////////////////////////////////////////////
xref = 1:6; 
yref = [xref; xref.^2; xref.^3]; 
x = 0:0.5:7;
try
    [y] = CL_interpLagrange(xref, yref, x, n=4, nder=2);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

//////////////////////////////////////////////////////
// Test 8: Wrong input argument size (xref or yref) //
//////////////////////////////////////////////////////
xref = linspace(0,1,100)';
yref = [xref; xref.^2; xref.^3]; 
x = 0:0.5:7;
try
    [y] = CL_interpLagrange(xref, yref, x, n=4);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

////////////////////////////////////////////////////////////////////
// Test 9: Insufficient number of values in xref (should be >= n) //
////////////////////////////////////////////////////////////////////
xref = 1:6; 
yref = [xref; xref.^2; xref.^3]; 
x = 0:0.5:7;
try
    [y] = CL_interpLagrange(xref, yref, x, n=10, nder=1);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

//////////////////////////////////////////
// Test 10: N == 0, where N = size(x,2) //
//////////////////////////////////////////
xref = 1:6; 
yref = [xref; xref.^2; xref.^3]; 
x = [];

[y] = CL_interpLagrange(xref, yref, x, n=4, opt=1);
[yref] = [];
TEST_OK($+1) = CL__isEqual([y],[yref]);

////////////////////////////////////////////////
// Test 11: Abscissae not strictly increasing //
////////////////////////////////////////////////
xref = [4,2,1,3]; 
yref = [xref; xref.^2; xref.^3]; 
x = 0:0.5:7;
try
    [y] = CL_interpLagrange(xref, yref, x, n=4, nder=1);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

////////////////////////////////
// Test 12: derivate function //
////////////////////////////////
xref = linspace(0,1,101); 
yref = [sin(xref); cos(xref)];
x = linspace(0,1,1001);
[y, ydot] = CL_interpLagrange(xref,yref,x, opt=1);
I = find(~isnan(ydot(1,:)));

TEST_OK($+1) = CL__isEqual(ydot(:,I),[cos(x(I));-sin(x(I))],5e-12);

//////////////////////////////////////////////////////////////////////////////////
// Test 13: Exact interpolation of a polynome of ordre n if n+1 points are used //
//////////////////////////////////////////////////////////////////////////////////
xref = 1:6; 
yref = xref.^3; 
x = 0:0.5:7; 
[y, ydot] = CL_interpLagrange(xref, yref, x, n=4);
xref = [%nan, %nan, [1:0.5:6] , %nan, %nan];

TEST_OK($+1) = CL__isEqual(y,xref.^3);
TEST_OK($+1) = CL__isEqual(ydot,3*xref.^2);


