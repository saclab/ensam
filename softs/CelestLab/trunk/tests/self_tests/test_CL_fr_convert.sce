// ------------------------------
// CL_fr_convert:
// Tests fonctionnels:
// - Verification de la bonne prise en compte des arguments optionnels
// - Pour cette fonction on valide par rapport a CL_fr_convertMat
// ------------------------------

TEST_OK = [];

// Fonction interne pour simplifier le test
// ATTENTION: utilise les variables pos1,vel1,M,omega et pos2_ref,vel2_ref
//            sans les passer explicitement dans l'interface!!
function OK = verif()
  N = size(omega,2);
  [pos2,vel2] = CL_rot_pvConvert(pos1*ones(1,N),vel1*ones(1,N),M,omega);
  OK = CL__isEqual(pos2, pos2_ref) & ...
       CL__isEqual(vel2, vel2_ref);
endfunction

// Appels via la syntaxe "key=value"
pos1 = [7000.e3; 100 ; 200];
vel1 = [1.e3; 1e3 ; 7e3];
cjd = [100, 22645, 50000];
frame1 = "GCRS";
frame2 = "ITRS";

// Test standard
[M, omega] = CL_fr_convertMat(frame1, frame2, cjd);
[pos2_ref, vel2_ref] = CL_fr_convert(frame1, frame2, cjd, pos1, vel1);
TEST_OK($+1) = verif();

// Test en omettant la vitesse
[M, omega] = CL_fr_convertMat(frame1, frame2, cjd);
[pos2_ref] = CL_fr_convert(frame1, frame2, cjd, pos1);
TEST_OK($+1) = verif();

// Changement de ut1_tref 
[M, omega] = CL_fr_convertMat(frame1, frame2, cjd, ut1_tref=[100,101,102]);
[pos2_ref, vel2_ref] = CL_fr_convert(frame1, frame2, cjd, pos1, vel1, ut1_tref=[100,101,102]);
TEST_OK($+1) = verif();

// Changement de tt_tref 
[M, omega] = CL_fr_convertMat(frame1, frame2, cjd, tt_tref=100);
[pos2_ref, vel2_ref] = CL_fr_convert(frame1, frame2, cjd, pos1, vel1, tt_tref=100);
TEST_OK($+1) = verif();

// Changement de xp
[M, omega] = CL_fr_convertMat(frame1, frame2, cjd, xp=100);
[pos2_ref, vel2_ref] = CL_fr_convert(frame1, frame2, cjd, pos1, vel1, xp=100);
TEST_OK($+1) = verif();

// Changement de yp
[M, omega] = CL_fr_convertMat(frame1, frame2, cjd, yp=100);
[pos2_ref, vel2_ref] = CL_fr_convert(frame1, frame2, cjd, pos1, vel1, yp=100);
TEST_OK($+1) = verif();

// Changement de dX
[M, omega] = CL_fr_convertMat(frame1, frame2, cjd, dX=0.01);
[pos2_ref, vel2_ref] = CL_fr_convert(frame1, frame2, cjd, pos1, vel1, dX=0.01);
TEST_OK($+1) = verif();

// Changement de dY
[M, omega] = CL_fr_convertMat(frame1, frame2, cjd, dY=0.01);
[pos2_ref, vel2_ref] = CL_fr_convert(frame1, frame2, cjd, pos1, vel1, dY=0.01);
TEST_OK($+1) = verif();

// Changement de use_interp
[M, omega] = CL_fr_convertMat(frame1, frame2, cjd, use_interp=%f);
[pos2_ref, vel2_ref] = CL_fr_convert(frame1, frame2, cjd, pos1, vel1, use_interp=%f);
TEST_OK($+1) = verif();

// Test sans nommer les arguments
// Note: no more random values as parameters (may generate an error)

// Arbitrary values (no physical meaning)
ut1_tref = 10;
tt_tref = 5;
xp = 0.1;
yp = 0.2;
dX = 0.3;
dY = 0.4;
use_interp = %t;
[M, omega] = CL_fr_convertMat(frame1, frame2, cjd, ut1_tref, tt_tref, xp, yp, dX, dY, use_interp);
[pos2_ref, vel2_ref] = CL_fr_convert(frame1, frame2, cjd, pos1, vel1, ut1_tref, tt_tref, xp, yp, dX, dY, use_interp);
TEST_OK($+1) = verif();