//====================
// TEST CL_ip_flybyParams
//
// - Test all possibilities input_par => output_par  
// - Test limit cases:
//   rp = 0, turnang = 0, turnang = %pi, dinf = 0, 
//====================

TEST_OK = [];

sma = 3.1;
ecc = 2.2;
v   = 0.1;
mu  = 2.2;

// Characteristics of the reference orbit
ref = CL_kp_characteristics(sma, ecc, v, mu = mu);
vinf = ref.vinf;
rp_ref = ref.rp;
dinf_ref = ref.d_asymt;
// Formula not used in the code
turnang_ref = acos(dinf_ref / (sma * ecc)) * 2;

// --------------------
// rp to (turnang, dinf)
// --------------------
[turnang, dinf] = CL_ip_flybyParams("vinf", vinf, "rp", rp_ref, ["turnang","dinf"], mu = mu);
TEST_OK($+1) = CL__isEqual(dinf, dinf_ref);
TEST_OK($+1) = CL__isEqual(turnang, turnang_ref);

// --------------------
// dinf to (turnang, rp)
// --------------------
[turnang, rp] = CL_ip_flybyParams("vinf", vinf, "dinf", dinf_ref, ["turnang", "rp"], mu = mu);
TEST_OK($+1) = CL__isEqual(turnang, turnang_ref);
TEST_OK($+1) = CL__isEqual(rp, rp_ref);

// --------------------
// turnang to (rp, dinf)
// --------------------
[rp, dinf] = CL_ip_flybyParams("vinf", vinf, "turnang", turnang_ref, ["rp", "dinf"], mu = mu);
TEST_OK($+1) = CL__isEqual(rp, rp_ref);
TEST_OK($+1) = CL__isEqual(dinf, dinf_ref);

// ====================
// Limit cases
// ====================

// --------------------
// rp = 0
// --------------------
[dinf, turnang] = CL_ip_flybyParams("vinf", vinf, "rp", 0, ["dinf", "turnang"], mu = mu);
if (and([isnan(dinf), isnan(turnang)]))
  TEST_OK($+1) = %t;
else
  TEST_OK($+1) = %f;
end

// --------------------
// turnang = 0
// --------------------
[dinf, rp] = CL_ip_flybyParams("vinf", vinf, "turnang", 0, ["dinf", "rp"], mu = mu);
if (and([isnan(dinf), isnan(rp)]))
  TEST_OK($+1) = %t;
else
  TEST_OK($+1) = %f;
end
// --------------------
// turnang = %pi
// --------------------
[dinf, rp] = CL_ip_flybyParams("vinf", vinf, "turnang", %pi, ["dinf", "rp"], mu = mu);
if (and([isnan(dinf), isnan(rp)]))
  TEST_OK($+1) = %t;
else
  TEST_OK($+1) = %f;
end

// --------------------
// dinf = 0
// --------------------
[turnang, rp] = CL_ip_flybyParams("vinf", vinf, "dinf", 0, ["turnang", "rp"], mu = mu);
if (and([isnan(turnang), isnan(rp)]))
  TEST_OK($+1) = %t;
else
  TEST_OK($+1) = %f;
end
