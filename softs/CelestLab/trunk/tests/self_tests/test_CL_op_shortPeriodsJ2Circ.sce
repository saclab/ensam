// ------------------------------
// CL_op_shortPeriodsJ2Circ: 
// Comparaison avec Eckstein-Hechler avec j2 seulement.
//
// NB: Les termes en J2 carr� ne sont pas pris en compte dans CL_op_shortPeriodsJ2Circ
// --> Le facteur qu'il manque vaut (1+3/2*%CL_j1jn(2)*(%CL_eqRad/sma)^2*(3-4*sin(inc)^2)) <= 1.5e-3
// d'ou erreur relative de 1.5e-3
// ------------------------------

TEST_OK = [];

sma = 7.e6;
ex = 0;
ey = 0;
inc = CL_deg2rad(98);
raan = 0;
pso = 1;
dcirc = CL_op_shortPeriodsJ2Circ(sma, inc, pso);

j1jn = %CL_j1jn(1:2);
t1 = 0;
mean_cir_t1 = [sma;ex;ey;inc;raan;pso]
t2 = t1;
[mean_cir_t2,osc_cir_t2] = CL_ex_eckHech(t1,mean_cir_t1,t2,j1jn = j1jn);

dcirc_res = osc_cir_t2 - mean_cir_t2;

TEST_OK($+1) = CL__isEqual( dcirc_res, dcirc , 1.5e-3);
