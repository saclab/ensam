// ------------------------------
// CL_ex_propagation
// Comparison of generic function with each native function
// ------------------------------

function [TEST_OK] = lance_test()

  TEST_OK = %t;
  for type_oe = type_oe_tab
    oe0 = CL_oe_convert("kep", type_oe, kep0);
    
    for k = 1 : size(mod_tab,"*")
    
      mod = mod_tab(k);
      
      // --------------------------------
      // Result using "natural" functions
      // --------------------------------
      
      // Conversion to the natural type of orbital elements
      if (mod == "eckhech")
        type_oe_nat = "cir";
      else
        type_oe_nat = "kep";
      end
      
      oe0_nat = CL_oe_convert(type_oe,type_oe_nat, oe0);
      
      // osc = mean if central or j2sec
      if (mod == "central")
         [mean_oe_nat] = CL_ex_kepler(t0, oe0_nat, t);
         osc_oe_nat = mean_oe_nat; 
      elseif (mod == "j2sec")
         [mean_oe_nat] = CL_ex_secularJ2(t0, oe0_nat, t);
         osc_oe_nat = mean_oe_nat; 
      elseif (mod == "eckhech")
         [mean_oe_nat, osc_oe_nat] = CL_ex_eckHech(t0, oe0_nat, t);
      elseif (mod == "lydsec")
         [mean_oe_nat, osc_oe_nat] = CL_ex_lyddane(t0, oe0_nat, t);
      elseif (mod == "lydlp")
         [mean_oe_nat, osc_oe_nat] = CL_ex_lyddaneLp(t0, oe0_nat, t);
      else
         error("Invalid model"); 
      end
      
      // Conversion back to the original orbital elements
      osc_oe = CL_oe_convert(type_oe_nat,type_oe, osc_oe_nat);
      mean_oe = CL_oe_convert(type_oe_nat,type_oe, mean_oe_nat);
      
      // --------------------------------
      // Result using "natural" functions
      // --------------------------------
      [mean_oe_res, osc_oe_res] = CL_ex_propagate(mod, type_oe, t0, oe0, t, "mo");
      
      // Comparison
      if (~CL__isEqual(mean_oe,mean_oe_res) | ~CL__isEqual(osc_oe,osc_oe_res))
        TEST_OK = %f;
      end    
    
    end
  end
endfunction


type_oe_tab = ["kep", "cir"];
mod_tab = ["central", "j2sec", "lydsec", "lydlp", "eckhech"];

// Test 1 : One orbit, several output dates
kep0 = [7000.e3; 0.001; 1; 3; 2; 1];

// Random dates
t0 = 0;
t = linspace(0,%pi, 100);

TEST_OK = [];

TEST_OK($+1) = lance_test();


// Test 2 : Several orbits, one output date
// Take eccentricity values in [0, 0.1[
kep0 = [ [7000.e3; 0.001; 1; 3; 2; 1], ...
         [15000.e3; 0.05; 1; 300; 20; 100], ...
         [45000.e3; 0.07; 1; 300; 20; 100], ...
         [75000.e3; 0.1; 1; 300; 20; 100]];

// Random dates
t0 = 0;
t = 1;

TEST_OK = [];

TEST_OK($+1) = lance_test();



