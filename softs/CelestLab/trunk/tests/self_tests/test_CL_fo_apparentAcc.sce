//----------------------------------------------------------
// CL_fo_apparentAcc
// Manual validation.
//----------------------------------------------------------

TEST_OK = [];

///////////////////////////////
// Test 1: manual validation //
///////////////////////////////
// Reference values
pos_ref = [6378.e3; 0; 0];
vel_ref = [0; 7000.; 0];
omega_ref = [0; 0; 1] * 5.e-12; // 1 deg / century
acc_ref = [0.000000070000000159450;0;0];

// Result values
acc_res = CL_fo_apparentAcc(pos_ref, vel_ref, omega_ref);

// Check
TEST_OK($+1)= CL__isEqual(acc_ref,acc_res);

////////////////////////////////////////////////
// Test 2: manual validation (vectorial form) //
////////////////////////////////////////////////
// Reference values
pos_ref = [[6378.e3; 0; 0],[5000.e3; 2000.e3; 0],[6000.e3; 1000.e3; 800.e3]];
vel_ref = [[0.; 7000.; 0.],[100.; 1000.; 0.],[0.; 2000.;0.]];
omega_ref = [[0; 0; 1],[0; 0; 1],[0; 0; 1]]* 5.e-12; // 1 deg / century
acc_ref = [[0.00000007000000015945;0.000000010000000125;0.00000002000000015],..
          [0.;-0.00000000099999995;0.000000000000000025],[0.;0.;0.]]';

// Result values
acc_res = CL_fo_apparentAcc(pos_ref, vel_ref, omega_ref);

// Check
TEST_OK($+1)= CL__isEqual(acc_ref,acc_res);

///////////////////////////////////
// Test 3: If omega = 0, acc = 0 //
///////////////////////////////////
pos = [6378.e3; 0; 0];
vel = [0; 7000.; 0];
omega = [0; 0; 0];

// Result values
acc = CL_fo_apparentAcc(pos, vel, omega);

// Check: If omega = 0, acc = 0 
TEST_OK($+1) = CL__isEqual(acc,zeros(omega));

///////////////////////////////////////////////////////////////////////////////////////////
// Test 4: If vel = 0, acc shall be the same to the acc obtained from CL_fo_centrifugAcc //
///////////////////////////////////////////////////////////////////////////////////////////
pos = [6378.e3; 0; 0];
vel = [0;0 ; 0];
omega = [0; 0; 1] * 5.e-12; // 1 deg / century

// Result values
apparent_Acc = CL_fo_apparentAcc(pos, vel, omega);
centrifug_Acc = CL_fo_centrifugAcc(pos, omega);

// Check: f vel = 0, apparent_Acc = centrifug_Acc
TEST_OK($+1) = CL__isEqual(apparent_Acc,centrifug_Acc);
