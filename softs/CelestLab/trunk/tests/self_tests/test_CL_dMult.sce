// ------------------------------
// CL_dMult: 
// ------------------------------

TEST_OK = [];

A = [1,2,3; 4,5,6; 7,8,9];

B1 = [2,4,6];

B2 = [2;4;6];

// Test 1 :  (A*B1 == B1*A) 
TEST_OK($+1) = CL__isEqual( CL_dMult(A, B1) , CL_dMult(B1,A) , 1e-15);

// Test 2 : A*[] == []
TEST_OK($+1) = ( CL_dMult(A, []) == [] );

// Test 3 : [] * A == []
TEST_OK($+1) = ( CL_dMult([], A) == [] );

// Test 4 : [] * [] == []
TEST_OK($+1) = ( CL_dMult([], []) == [] );

// Test 5 : erreur de dimension
try 
  CL_dMult(A, [B1;B1]);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 6 : erreur de dimension
try 
  CL_dMult(A, [B2;B2]);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 7 : erreur de dimension
try 
  CL_dMult([B1;B1], A);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 8 : erreur de dimension
try 
  CL_dMult(A, [B2;B2]);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  
