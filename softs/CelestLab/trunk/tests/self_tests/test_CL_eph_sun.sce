// ------------------------------
// CL_eph_sun: 
// Validation of derivatives with finite differences
// ------------------------------

TEST_OK = [];


cjd = 0;
dt = 200;  // seconds
dtd = dt / 86400; // days
tt_tref = 0;
frame = "EOD";

models = ["med", "high"];

for model = models
  // Reference velocity
  [pos_sun_dt, vel_sun_ref] = CL_eph_sun(cjd+dtd/2,tt_tref,frame,model);

  // Compute velocity with finite differences
  [pos_sun] = CL_eph_sun(cjd,tt_tref,frame,model);
  [pos_sun_dt] = CL_eph_sun(cjd+dtd,tt_tref,frame,model);
  vel_sun = (pos_sun_dt-pos_sun) ./ dt;

  TEST_OK($+1) = CL__isEqual(vel_sun, vel_sun_ref,1e-8);
end
