// ------------------------------
// CL_intervUnion : 
// ------------------------------


TEST_OK = [];


// Verifie de facon systematique (avec des boucles) que l'union de deux ensembles d'intervalles est bien calculee
// - i1 = un ensemble d'intervalles (2xN1)
// - i2 = un ensemble d'intervalles (2xN1)
// - ires = l'ensemble d'intervalles des unions (2xN)
function OK = verif_union(i1,i2 , ires)

  OK = %t;

  i = [i1, i2];
  // Pour chaque intervalle initial (de i1 ou i2)
  for j = 1 : size(i,2)
    // Verification que chaque point (le debut et la fin) appartiennent bien a ires
    if( find( (i(1,j) <= ires(2,:) & i(1,j) >= ires(1,:)) & ...
              (i(2,j) <= ires(2,:) & i(2,j) >= ires(1,:)) ) == [] )
      OK = %f;
    end
  end
  
  // Verification que chaque point (le debut et la fin) de ires, n'existe pas plusieurs fois
  for j = 1 : size(ires,"*")
    I = find( ires(j) == ires )
    if( length(I) ~= 1 )
      OK = %f;
    end
  end
  
endfunction


// Tirage aleatoire de N intervalles compris entre 0 et val_max
// La taille de chaque intervalle est au maximum de taille_max.
function ires = gen_intervalle_aleat(N,val_max,taille_max)
  deb = floor( rand(1,N)*val_max );
  fin = deb + floor( rand(1,N)*taille_max )
  ires = CL_intervUnion([deb ; fin]); // pour s'assurer que tous les intervalles sont disjoints
endfunction


// --- Test 1 : Cas speciaux choisis:
// - 2 intervalles egaux
// - debut d'un intervalle de i1 = fin d'un intervalle de i2
// - fin d'un intervalle de i1 = debut d'un intervalle de i2
// - un intervalle de i2 entierement inclus dans i1
// - un intervalle de i1 entierement inclus dans i2
// - chevauchement d'intervalles 2 a 2
i1 = [ [0;1] , [3;4]   , [10;11] , [12;15] , [20;21] , [100;102] , [103;106] , [107;111] ];
i2 = [ [0;1] , [1.5;3] , [4;4.2] , [13;14] , [19;22] , [101;104] , [105;108] , [109;112]];
ires = CL_intervUnion(i1,i2);
TEST_OK($+1) = verif_union(i1,i2,ires);


// --- Test 2 : Intervalles aleatoires
N = 100;
val_max = 1000;
taille_max = 10;

i1 = gen_intervalle_aleat(N,val_max,taille_max);
i2 = gen_intervalle_aleat(N,val_max,taille_max);
ires = CL_intervUnion(i1,i2);
TEST_OK($+1) = verif_union(i1,i2,ires);