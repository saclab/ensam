// ------------------------------
// CL_man_dvHohmann : 
// ------------------------------

TEST_OK = [];

///////////////////////////////////////////////////////////////////
// Test 1 : Nominal cas, cross validation with CL_man_applyDvKep //
///////////////////////////////////////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
af = 7000.e3;
[deltav,dv1,dv2,anv1,anv2] = CL_man_dvHohmann(ai,af);

// Check results:
kep = [ai ; 0 ; %pi/2 ; 0 ; 0.1 ; anv1];
kep1 = CL_man_applyDvKep(kep,dv1);
kep1(6) = anv2;
kep2 = CL_man_applyDvKep(kep1,dv2);

TEST_OK($+1) = CL__isEqual(af, kep2(1)) & ...              // sma
                 CL__isEqual(kep(2), kep2(2), 1e-14) & ... // ecc
                 CL__isEqual(kep(3), kep2(3)) & ...         // inc
                 CL__isEqual(kep(5), kep2(5));              // gom

/////////////////////////////////////////////////////
// Test 2: Nominal cas with vector input arguments //
/////////////////////////////////////////////////////
// 7200km to 7000km :
ai = [7200.e3, 7000.e3, 6800.e3];
af = [7000.e3, 6800.e3, 6600.e3];
[deltav,dv1,dv2,anv1,anv2] = CL_man_dvHohmann(ai,af);

kep = [ai ; zeros(ai) ; %pi/2*ones(ai) ; zeros(ai) ; 0.1*ones(ai) ; anv1];
kep1 = CL_man_applyDvKep(kep,dv1);
kep1(6,:) = anv2;
kep2 = CL_man_applyDvKep(kep1,dv2);

TEST_OK($+1) = CL__isEqual(af, kep2(1,:)) & ...                // sma
                 CL__isEqual(kep(2,:), kep2(2,:), 1e-14) & ... // ecc
                 CL__isEqual(kep(3,:), kep2(3,:)) & ...         // inc
                 CL__isEqual(kep(5,:), kep2(5,:));              // gom

//////////////////////////////////
// Test 3: Nominal cas res == s //
//////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
af = 7000.e3;

man = CL_man_dvHohmann(ai,af,res="s");

// Check results:
kep = [ai ; 0 ; %pi/2 ; 0 ; 0.1 ; man.anv1];
kep1 = CL_man_applyDvKep(kep,man.dv1);
kep1(6) = man.anv2;
kep2 = CL_man_applyDvKep(kep1,man.dv2);

TEST_OK($+1) = CL__isEqual(af, kep2(1)) & ...              // sma
                 CL__isEqual(kep(2),kep2(2),1.e-14) & ...  // ecc 
                 CL__isEqual(kep(3), kep2(3)) & ...         // inc
                 CL__isEqual(kep(5), kep2(5));               // gom

////////////////////////////////////////////////
// Test 4: Invalid value for argument ''res'' //
////////////////////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
af = 7000.e3;

try
    man = CL_man_dvHohmann(ai,af,res="p");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

////////////////////////////////////////////////
// Test 5: Invalid number of output arguments //
////////////////////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
af = 7000.e3;

try
    [deltav,dv1,dv2,anv1,anv2] = CL_man_dvHohmann(ai,af,res="s");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

///////////////////////////////////////////////////////////////////////////////////
// Test 6: DeltaV is the sum of the norms of the velocity increments dv1 and dv2 //
///////////////////////////////////////////////////////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
af = 7000.e3;

[deltav_res,dv1_res,dv2_res,anv1,anv2] = CL_man_dvHohmann(ai,af);

dv1_ref = sqrt(dv1_res(1)^2 + dv1_res(2)^2 + dv1_res(3)^2);
dv2_ref = sqrt(dv2_res(1)^2 + dv2_res(2)^2 + dv2_res(3)^2);
deltav_ref = dv1_ref + dv2_ref;

TEST_OK(1+$) = CL__isEqual(deltav_ref,deltav_res);

////////////////////////////////////////////////////////////////////
// Test 7: Invalid input arguments (ai and af shall be positives) //
////////////////////////////////////////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
af = -7000.e3;

try
    CL_man_dvHohmann(ai,af);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end