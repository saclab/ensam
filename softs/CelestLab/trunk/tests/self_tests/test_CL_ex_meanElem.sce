// -------------------------------------------------------
// CL_ex_meanElem : Cross validation with CL_ex_propagate
// -------------------------------------------------------

TEST_OK = [];

//////////////////////////////////////////////////////////////////////
// Test 1: Cross validation with CL_ex_propagate for all the models //
//////////////////////////////////////////////////////////////////////
mean_cir0 = [7.e6; 0; 1.e-3; 1; 0.2; 0.3];
t0 = 0; 
t = 0; 
eps = 1.e-12; 
 
Models = ["central", "j2sec", "lydsec", "lydlp", "eckhech"];  
 
for k = 1: size(Models, "*")
    mod = Models(k);
    osc_cir = CL_ex_propagate(mod,"cir",t0,mean_cir0,t,res="o");
    mean_cir = CL_ex_meanElem(mod,"cir",osc_cir) // => mean_cir
    TEST_OK(1+$) = CL__isEqual(mean_cir0,mean_cir,eps); 
end

/////////////////////////////////////////////////////////
// Test 2: Model lydsec with circular orbital elements //
/////////////////////////////////////////////////////////
// Note: With this tests it is checked that the function changes into the 
//       natural orbital elements needed by each method.
mean_cir0 = [7.e6; 0; 1.e-3; 1; 0.2; 0.3];
t0 = 0; 
t = 0; 
eps = 1.e-12;
mod = "lydsec";
nat_type = "cir"; // 'kep' is the natural type orbital elements for Lydsec method

osc_kep = CL_ex_propagate(mod,nat_type,t0,mean_cir0,t,res="o");
mean_oe = CL_ex_meanElem(mod,nat_type,osc_kep) // => mean_kep
TEST_OK(1+$) = CL__isEqual(mean_oe,mean_cir0,eps); 

//////////////////////////////////////////////
// Test 3: Invalid type of orbital elements //
//////////////////////////////////////////////
osc_kep = [7.e6; 1.e-3; 1; %pi/2; 0.1; 0.2];

try 
    CL_ex_meanElem("eckhech","ellip",osc_kep);
    TEST_OK(1+$) = %f;
catch
    TEST_OK(1+$) = %t;
end

////////////////////////////////
// Test 4: Invalid model name //
////////////////////////////////
osc_kep = [7.e6; 1.e-3; 1; %pi/2; 0.1; 0.2];

try 
    CL_ex_meanElem("EckHech","kep",osc_kep);
    TEST_OK(1+$) = %f;
catch
    TEST_OK(1+$) = %t;
end

///////////////////////////////////////
// Test 5: Invalid size for %CL_j1jn //
///////////////////////////////////////
osc_kep0 = [7.e6; 0; 1.e-3; 1; 0.2; 0.3];
mod = "lydsec";
nat_type = "kep";
function [test_ok] = test()
  // Changes the size of %CL_j1jn
  
  %CL_j1jn(3) = 1;
  
  try 
      CL_ex_meanElem(mod,nat_type,osc_kep0);
      test_ok = %f;
  catch
      test_ok = %t;
  end
  
endfunction 

TEST_OK($+1) = test();