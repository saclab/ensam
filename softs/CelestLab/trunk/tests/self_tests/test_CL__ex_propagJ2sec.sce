// Verification of CL__ex_propagJ2sec function
// Procedure of verification:
//   1) Extrapolate initial orbital elements to times tf with CL_ex_propagate
//   2) Transform initial orbital elements to keplerian with CL_oe_convert
//   3) Extrapolate keplerian elements to times tf with CL_ex_secularJ2
//   4) Transform extrapolated keplerian elements to initial elements type
//   5) Compare results of steps 1) and 4)

TEST_OK = [];
rtol = 1.e-14;

// 1 initial time and N final times
t0 = 1;
tf = [2, 3, 4, 5];


// TEST 1 : keplerien elements
kep0 = [7000.e3; 0.1; 1; 2; 3; 0.5];

kepf1 = CL_ex_propagate("j2sec", "kep", t0, kep0, tf, "m");
kepf2 = CL_ex_secularJ2(t0, kep0, tf);
TEST_OK($+1) = CL_isAlmostEqual(kepf1, kepf2, rtol=rtol);


// TEST 2 : circular elements
cir0 = [7000.e3; 0.1; 0.2; 1; 2; 3];
kep0 = CL_oe_convert('cir','kep',cir0);

cirf1 = CL_ex_propagate("j2sec", "cir", t0, cir0, tf, "m");
kepf = CL_ex_secularJ2(t0, kep0, tf);
cirf2 = CL_oe_convert('kep','cir',kepf);
TEST_OK($+1) = CL_isAlmostEqual(cirf1, cirf2, rtol=rtol);


// TEST 3 : near-circular near equatorial elements
cireq0 = [7000.e3; 0.1; 0.2; 0.3; 0.4; 1];
kep0 = CL_oe_convert('cireq','kep',cireq0);

cireqf1 = CL_ex_propagate("j2sec", "cireq", t0, cireq0, tf, "m"); 
kepf = CL_ex_secularJ2(t0, kep0, tf);
cireqf2 = CL_oe_convert('kep','cireq',kepf); 
TEST_OK($+1) = CL_isAlmostEqual(cireqf1, cireqf2, rtol=rtol);


// TEST 4 : equinoctial elements
equin0 = [7000.e3; 0.1; 0.2; 0.3; 0.4; 1];
kep0 = CL_oe_convert('equin','kep',equin0);

equinf1 = CL_ex_propagate("j2sec", "equin", t0, equin0, tf, "m"); 
kepf = CL_ex_secularJ2(t0, kep0, tf);
equinf2 = CL_oe_convert('kep','equin',kepf); 
TEST_OK($+1) = CL_isAlmostEqual(equinf1, equinf2, rtol=rtol);


// N initial and final times
t0 = [1, 1, 1, 1] ;
tf = [2, 3, 4, 5];
N = size(t0, 2);

// TEST 5 : keplerien elements
kep0 = [7000.e3; 0.1; 1; 2; 3; 0.5];

kepf1 = CL_ex_propagate("j2sec", "kep", t0, kep0, tf, "m");
kepf2 = CL_ex_secularJ2(t0, kep0, tf);
TEST_OK($+1) = CL_isAlmostEqual(kepf1, kepf2, rtol=rtol);


// TEST 6 : circular elements
cir0 = [7000.e3; 0.1; 0.2; 1; 2; 3];
kep0 = CL_oe_convert('cir','kep',cir0);

cirf1 = CL_ex_propagate("j2sec", "cir", t0, cir0, tf, "m");
kepf = CL_ex_secularJ2(t0, kep0, tf);
cirf2 = CL_oe_convert('kep','cir',kepf);
TEST_OK($+1) = CL_isAlmostEqual(cirf1, cirf2, rtol=rtol);


// TEST 7 : near-circular near equatorial elements
cireq0 = [7000.e3; 0.1; 0.2; 0.3; 0.4; 1];
kep0 = CL_oe_convert('cireq','kep',cireq0);

cireqf1 = CL_ex_propagate("j2sec", "cireq", t0, cireq0, tf, "m"); 
kepf = CL_ex_secularJ2(t0, kep0, tf);
cireqf2 = CL_oe_convert('kep','cireq',kepf);
TEST_OK($+1) = CL_isAlmostEqual(cireqf1, cireqf2, rtol=rtol);


// TEST 8 : equinoctial elements
equin0 = [7000.e3; 0.1; 0.2; 0.3; 0.4; 1];
kep0 = CL_oe_convert('equin','kep',equin0);

equinf1 = CL_ex_propagate("j2sec", "equin", t0, equin0, tf, "m"); 
kepf = CL_ex_secularJ2(t0, kep0, tf);
equinf2 = CL_oe_convert('kep','equin',kepf);
TEST_OK($+1) = CL_isAlmostEqual(equinf1, equinf2, rtol=rtol);


