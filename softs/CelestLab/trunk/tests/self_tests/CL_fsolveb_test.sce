// ---------------------------------------------------------
// CL_fsolveb
// Test with reference results = analytical solution of the equation
// ---------------------------------------------------------

TEST_OK = [];

/////////////////////////////////////////////////////////
// Test 1: solves e^x=1 with all the methods available //
/////////////////////////////////////////////////////////
function [y] = fct1(x, ind, args);
  y = %e.^x-1;
endfunction

    // meth = "s" : usual secant method
    sol = CL_fsolveb(fct1, -1, 1, ytol=1e-8,meth="s");
    solref = 0;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-8);

    // meth = "d" : dichotomy
    sol = CL_fsolveb(fct1, -1, 1, ytol=1e-8, meth="d");
    solref = 0;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-8);

    // meth = "ds" : dichotomy + secant method at each iteration
    sol = CL_fsolveb(fct1, -1, 1, ytol=1e-8, meth="ds");
    solref = 0;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-8);

    // meth = "s2" : variant of secant method 
    sol = CL_fsolveb(fct1, -1, 1, ytol=1e-8, meth="s2");
    solref = 0;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-8);
    
    // meth = "s3" : variant of secant method
    sol = CL_fsolveb(fct1, -1, 1, ytol=1e-8, meth="s3");
    solref = 0;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-8);

////////////////////////////////////////////////////////////////
// Test 2: solves sin(x) = 0.5 with all the methods available //
////////////////////////////////////////////////////////////////
//funcprot(0)
function [y] = fct2(x, ind, args);
  y = sin(x) - 0.5;
endfunction

    // meth = "s" : usual secant method
    [sol, y] = CL_fsolveb(fct2, -1, 1,ytol=1.e-14,meth="s");
    solref = %pi/6;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);
    
    // meth = "d" : dichotomy 
    [sol, y] = CL_fsolveb(fct2, -1, 1,ytol=1.e-8,meth="d");
    solref = %pi/6;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-8);
    
    // meth = "ds" : dichotomy + secant method at each iteration
    [sol, y] = CL_fsolveb(fct2, -1, 1,ytol=1.e-14,meth="ds");
    solref = %pi/6;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);
    
    // meth = "s2" : variant of secant method
    [sol, y] = CL_fsolveb(fct2, -1, 1,ytol=1.e-14,meth="s2");
    solref = %pi/6;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);
    
    // meth = "s3" : variant of secant method
    [sol, y] = CL_fsolveb(fct2, -1, 1,ytol=1.e-14,meth="s3");
    solref = %pi/6;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);

/////////////////////////////////////////////////////////////////////
// Test 3: solves sin(x)+e^(x-pi)=0 with all the methods available //
/////////////////////////////////////////////////////////////////////
//funcprot(0)
function [y] = fct3(x, ind, args);
  y = sin(x) +%e^(x-%pi)-1;
endfunction
    // meth = "s" : usual secant method
    [sol, y] = CL_fsolveb(fct3, 1, %pi,ytol=1.e-14,meth="s"); 
    solref = %pi;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);
    
    // meth = "d" : dichotomy 
    [sol, y] = CL_fsolveb(fct3, -2*%pi, 2*%pi,ytol=1.e-14,meth="d"); 
    solref = %pi;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);
    
    // meth = "ds" : dichotomy + secant method at each iteration
    [sol, y] = CL_fsolveb(fct3, -%pi, %pi,ytol=1.e-14,meth="ds"); 
    solref = %pi;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);
    
    // meth = "s2" : variant of secant method
    [sol, y] = CL_fsolveb(fct3, 1, %pi,ytol=1.e-14,meth="s2"); 
    solref = %pi;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);
    
    // meth = "s3" : variant of secant method
    [sol, y] = CL_fsolveb(fct3, 1, %pi,ytol=1.e-14,meth="s3"); 
    solref = %pi;
    TEST_OK($+1) = CL__isEqual(sol,solref,1e-14);

///////////////////////////////////////////
// Test 4: error if invalid argument opt //
///////////////////////////////////////////
function [y] = fct4(x, ind, args);
  y = %e.^x-1;
endfunction

try
    [sol, y] = CL_fsolveb(fct4, -1, 1, ytol=1e-8 ,meth="s", opt=2);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

////////////////////////////
// Test 5: invalid method //
////////////////////////////
function [y] = fct5(x, ind, args);
  y = sin(x) - 0.5;
endfunction

try
    [sol, y] = CL_fsolveb(fct5, -1, 1,ytol=1.e-14,meth="s4");
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

////////////////////////////////////////////////
// Test 6: check sizes are compatible (or []) //
////////////////////////////////////////////////
function [y] = fct6(x, ind, args)
  y = sin(x) - 0.5;
endfunction

try
    [sol, y] = CL_fsolveb(fct6,ytol=1.e-14,meth="s");
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

/////////////////////////////////////
// Test 7: initial interval stored //
/////////////////////////////////////
function [y] = fct7(x, ind, args);
  y = %e.^x-1;
endfunction

sol = CL_fsolveb(fct7, 1, -1, ytol=1e-8,meth="s");
solref = 0;
TEST_OK($+1) = CL__isEqual(sol,solref,1e-8);

////////////////////////////////////////////////////////////////
// Test 8: if y1 and y2 not with opposite signs (or x1 == x2) //
// => result is %nan                                          //
////////////////////////////////////////////////////////////////
//funcprot(0)
function [y] = fct8(x, ind, args);
  y = sin(x) - 0.5;
endfunction

[sol, y, info] = CL_fsolveb(fct8, 0.5, -1,ytol=1.e-14,meth="d");
solref = 1;
TEST_OK($+1) = CL__isEqual(info,solref,1e-14);

///////////////////////////////////////////////
// Test 9: Invalid size for ''x1'' or ''x2'' //
///////////////////////////////////////////////
function [y] = fct9(x, ind, args);
  y = sin(x) - 0.5;
endfunction

try
    [sol, y, info] = CL_fsolveb(fct9, [], -1,ytol=1.e-14);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

//////////////////////////////////////////////////////////////////
// Test 10: y and y1 have same sign (on same side) for meth = s //
//////////////////////////////////////////////////////////////////
function [y] = fct10(x, ind, args);
    y = x.^2 + x -1;
endfunction

sol = CL_fsolveb(fct10, 1, -1, ytol=1e-8,meth="s");
solref = (sqrt(5)-1)/2;
TEST_OK($+1) = CL__isEqual(sol,solref,1e-8);

////////////////////////////////////////////////
// Test 11: input arguments in vectorial form //
////////////////////////////////////////////////
//funcprot(0)
function [y] = fct11(x, ind, args);
    y = sin(x) - args(ind);
endfunction

n = 5; 
args = (1:n) / 10; 
x1 = -1 * ones(1,n);
x2 =  1 * ones(1,n);

[sol,y] = CL_fsolveb(fct11, x1, x2, args, ytol=1.e-14);
solref = [0.1001674, 0.2013579, 0.3046927, 0.4115168, %pi/6];
TEST_OK($+1) = CL__isEqual(sol,solref,1e-6);

clear fct1 fct2 fct3 fct4 fct5 fct6 fct7 fct8 fct9 fct10 fct11
clear y ind args n x1 x2 sol solref info
