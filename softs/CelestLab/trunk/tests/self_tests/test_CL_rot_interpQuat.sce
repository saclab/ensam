// --------------------------------------
// CL_rot_interpQuat: test de cohérence
// --------------------------------------

TEST_OK = [];

///////////////////////////////////////////////
// Test 1: Invalid number of input arguments //
///////////////////////////////////////////////
t_ref = [1:10];
q_ref = CL_rot_angles2quat([3,1,3],[t_ref; t_ref; t_ref]);

try
    q = CL_rot_interpQuat(t_ref,q_ref);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

/////////////////////////////////////
// Test 2: Invalid input arguments //
/////////////////////////////////////
t_ref = [1:10];
q_ref = [1:30];
t = [1.5:8.5];
try
    q = CL_rot_interpQuat(t_ref,q_ref,t);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

/////////////////////////////////////////////
// Test 3: Invalid method of interpolation //
/////////////////////////////////////////////
t_ref = [1:10];
q_ref = CL_rot_angles2quat([3,1,3],[t_ref;t_ref.^2;cos(t_ref)]);
t = [1.5:8.5];
try
    q = CL_rot_interpQuat(t_ref,q_ref,t,"lagrange");
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

/////////////////////////////////////////////
// Test 4: Invalid size for t_ref or q_ref //
/////////////////////////////////////////////
t_ref = 5;
q_ref = CL_rot_angles2quat([3,1,3],[t_ref;t_ref.^2;cos(t_ref)]);
t = 2;
try
    q = CL_rot_interpQuat(t_ref,q_ref,t);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

//////////////////////////////////////////////////////////
// Test 5: Invalid size for t_ref or t (number of rows) //
//////////////////////////////////////////////////////////
t_ref = [1:10];
q_ref = CL_rot_angles2quat([3,1,3],[t_ref;t_ref.^2;cos(t_ref)]);
t = [1.5:8.5]';

try
    q = CL_rot_interpQuat(t_ref,q_ref,t);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

////////////////////////////////
// Test 6: Functionality test //
//////////////////////////////// 

// Creation of a quaternion as a function of time from a simple rotation around
// z axis
function q = F(t)
    q = CL_rot_angles2quat(3,t.*(%pi/180))
endfunction
t_ref = [30:5:60];
q_ref = F(t_ref);
t = 52.5;
q = CL_rot_interpQuat(t_ref.*(%pi/180),q_ref,t*(%pi/180));

//Once the quaternion is obtained by interpolating q_ref at the requested time,
//the value of the reference quaternion at that time shall be the same as the 
//calculated one.
q_ref_t = F(t);
                          
TEST_OK($+1) = CL__isEqual(q,q_ref_t);
