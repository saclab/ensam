// ------------------------------
// CL_man_dvInc : 
// ------------------------------

TEST_OK = [];

// --- Test 1 : Nominal case.
ai = 7200.e3;
ei = 0.1;
inci = 1;
pomi = 1.4;
incf = 1.1;
[deltav,dv,anv] = CL_man_dvInc(ai,ei,inci,pomi,incf,posman="dn");

// Check results:
anm = CL_kp_v2M(ei,anv);
kep = [ai ; ei ; inci ; pomi ; 0 ; anm];
kep1 = CL_man_applyDvKep(kep,dv);

TEST_OK($+1) = CL__isEqual(ai, kep1(1)) & ...        // sma
               CL__isEqual(ei, kep1(2), 1e-14) & ... // ecc
               CL__isEqual(incf, kep1(3)) & ...      // inc
               CL__isEqual(pomi, kep1(4), 1e-14) & ...      // pom
               CL__isEqual(kep(5), CL_rMod(kep1(5),-%pi,%pi));  // gom

// Test 2 :input parameter "res" = s. 
  [std] = CL_man_dvInc(ai,ei,inci,pomi,incf,posman=0,res = "s")
  TEST_OK($+1) = CL__isEqual(deltav, std.deltav);
  TEST_OK($+1) = CL__isEqual(dv, std.dv);
  TEST_OK($+1) = CL__isEqual(anv, std.anv);
  
// Test 3 : erreur in input parameter "res".
try 
  [deltav,dv,anv] = CL_man_dvInc(ai,ei,inci,pomi,incf,res="p")
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 4 : erreur in type of output parameters.
try 
  [deltav,dv,anv] = CL_man_dvInc(ai,ei,inci,pomi,incf,res="s")
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 5: erreur in input parameter "ai".
try 
  [deltav,dv,anv] = CL_man_dvInc(-1,ei,inci,pomi,incf,res="d")
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 6 : erreur in input parameters "icheck" and "incf".
try 
  [deltav,dv,anv] = CL_man_dvInc(ai,ei,inci,pomi,-1,icheck=%t)
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 7 : erreur in input parameter "posman".
try 
  [deltav,dv,anv] = CL_man_dvInc(ai,ei,inci,pomi,incf,posman=2)
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  



