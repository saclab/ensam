// ------------------------------
// CL_rot_contQuat : 
// ------------------------------

TEST_OK = [];

///////////////////////////////////////////////////////////////////////////
// Test1: Nominal cas --> check that the behavior of the function is the //
//                        expected one, assuming that the result is      //
//                        correct                                        //
///////////////////////////////////////////////////////////////////////////
alpha = 0 : 0.3 : 5;

// From angle the associated rotation matrix is calculated, then the quaternion
// from the matrix. This will lead into some jumps in the continuity of the 
// quaternions
q = CL_rot_matrix2quat(CL_rot_angles2matrix([1,2], [alpha; 2*alpha]));

// Calculating directly the quaternion from angles of rotation, a continous 
// evolution of the quaternions is obtained
q1 = CL_rot_angles2quat([1,2],[alpha; 2*alpha]);

// To make a quaternion continuous:
qc = CL_rot_contQuat(q); 

TEST_OK($+1)= CL__isEqual(qc,q1);

////////////////////////////////////////////////////
// Test2: Quaternions with at least one component //
//        equal to %nan are not modified          //
////////////////////////////////////////////////////
q1=CL_rot_defQuat(1,0,0,0);
q2=CL_rot_defQuat(0.4741599,0.2590347,0.7384603,0.4034227);
q3=CL_rot_defQuat(- 0.2248451,- 0.3501755,0.4912955,0.7651474);
q4=CL_rot_defQuat(0.0700293,%nan,- 0.0099824,- 0.1407665);
q5=CL_rot_defQuat(- 0.2720117,0.5943565,- 0.314941,0.6881586);
q6=CL_rot_defQuat(- 0.2272541,0.1697639,0.7682361,- 0.5738895);

q = [q1,q2,q3,q4,q5,q6];
qc = CL_rot_contQuat(q);

TEST_OK(1+$)=CL__isEqual(q(4),qc(4));

//////////////////////////////////////////////////////////////
// Test3: Wrong type of input argument. Quaternion expected //
//////////////////////////////////////////////////////////////
q1= [1,0,0,0];
q2 = [0.4741599,0.2590347,0.7384603,0.4034227];
q3 = [-0.2248451,-0.3501755,0.4912955,0.7651474];
q4 = [0.0700293,0.9875126,-0.0099824,-0.1407665];
q5 = [-0.2720117,0.5943565,-0.3149410,0.6881586];
q6 = [-0.2272541,0.1697639,0.7682361,-0.5738895];

// This is not a CLquat type data, even if they have
// similar structure to that of quaternions; however they
// don't have the quaternion attribute --> TO USE 
// CL_rot_defQuat for quaternion structures
q = [q1,q2,q3,q4,q5,q6]; 

try 
    qc = CL_rot_contQuat(q);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

////////////////////////////////////////////
// Test4: Wrong number of input arguments //
////////////////////////////////////////////
try 
    CL_rot_contQuat();
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end