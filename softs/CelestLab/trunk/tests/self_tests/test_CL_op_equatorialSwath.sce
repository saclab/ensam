// ------------------------------
// CL_op_equatorialSwath : 
// ------------------------------



TEST_OK = [];

// Test 1 : 50000 random cases
N = 50000;

sma = grand(1, N, "unf", 6400.e3, 80000.e3); 
inc = grand(1, N, "unf", 0, %pi); 
cen = grand(1, N, "unf", 0, %pi); 

dlon = CL_op_equatorialSwath("cen2dlon", sma, inc, cen);
cen2 = CL_op_equatorialSwath("dlon2cen", sma, inc, dlon);

I = find(isnan(cen2) | isnan(dlon));
cen(I) = %nan;

TEST_OK($+1) = CL__isEqual(cen,cen2,1e-6);


// Test 2 : %nan in input values
sma = [12000.e3 , %nan , 8000.e3]; 
inc = 0.1; 
cen = 0.1; 

dlon = CL_op_equatorialSwath("cen2dlon", sma, inc, cen);
cen2 = CL_op_equatorialSwath("dlon2cen", sma, inc, dlon);
TEST_OK($+1) = CL__isEqual(cen2, [0.1, %nan, 0.1],1e-13);


// Test 3 : [] in input values
sma = []; 
inc = 0.1; 
cen = 0.1; 

dlon = CL_op_equatorialSwath("cen2dlon", sma, inc, cen);
cen2 = CL_op_equatorialSwath("dlon2cen", sma, inc, dlon);
TEST_OK($+1) = CL__isEqual(cen2, []);

