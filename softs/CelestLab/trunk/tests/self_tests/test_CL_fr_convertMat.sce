// ------------------------------
// CL_fr_convertMat:
// Tests fonctionnels:
// - Verification de la bonne prise en compte des arguments optionnels
// ------------------------------

TEST_OK = [];


// Fonction interne pour calculer l'angle entre 2 matrices
function ang = angmat(M1,M2)
  [ax,ang] = CL_rot_quat2axAng(CL_rot_matrix2quat(M1*M2'));
endfunction

global %CL__PRIV; 
if (~exists("%CL_rotrBody")); %CL_rotrBody = %CL__PRIV.DATA.rotrBody; end

// Appels via la syntaxe "key=value"
cjd = [100, 22645, 50000];
frame1 = "GCRS";
frame2 = "ITRS";

// Changement de ut1_tref --> le resultat est identique sur Z
//                            l'angle entre les 2 vecteurs vaut (a s prime pres!) 100*%CL_rotrBody
[M_ref,om_ref] = CL_fr_convertMat(frame1, frame2, cjd);
[M, om] = CL_fr_convertMat(frame1, frame2, cjd, ut1_tref=100);
TEST_OK($+1) = CL__isEqual(M(3,:,:), M_ref(3,:,:)) & ...
               CL__isEqual(angmat(M,M_ref),100*%CL_rotrBody*ones(1,3),1e-11);

// Changement de tt_tref --> le resultat est modifie.
//                           Au pire on s'attend a modifier le resultat de la 
//                           derive due � la precession (50as/an) + la derive due a la nutation (20as/an)
[M_ref,om_ref] = CL_fr_convertMat(frame1, frame2, cjd);
[M, om] = CL_fr_convertMat(frame1, frame2, cjd, tt_tref=1000);
TEST_OK($+1) = find(angmat(M,M_ref) > 1000 * CL_unitConvert(70,"arcsec","rad")/(365*86400)) == [];

// Changement de xp --> le resultat est modifie de xp        
[M_ref,om_ref] = CL_fr_convertMat(frame1, frame2, cjd);
[M, om] = CL_fr_convertMat(frame1, frame2, cjd, xp=1);
TEST_OK($+1) = CL__isEqual(angmat(M,M_ref), 1*ones(1,3));

// Changement de yp --> le resultat est modifie de yp        
[M_ref,om_ref] = CL_fr_convertMat(frame1, frame2, cjd);
[M, om] = CL_fr_convertMat(frame1, frame2, cjd, yp=1);
TEST_OK($+1) = CL__isEqual(angmat(M,M_ref), 1*ones(1,3));

// Changement de dX --> le resultat est modifie de environ dX        
[M_ref,om_ref] = CL_fr_convertMat(frame1, frame2, cjd);
[M, om] = CL_fr_convertMat(frame1, frame2, cjd, dX=0.01);
TEST_OK($+1) = find(angmat(M,M_ref) > 1.5*0.01) == [];

// Changement de dY --> le resultat est modifie de environ dY        
[M_ref,om_ref] = CL_fr_convertMat(frame1, frame2, cjd);
[M, om] = CL_fr_convertMat(frame1, frame2, cjd, dY=0.01);
TEST_OK($+1) = find(angmat(M,M_ref) > 1.5*0.01) == [];

// Changement de use_interp --> le resultat est modifie de 1 micro arcseconde au pire        
[M_ref,om_ref] = CL_fr_convertMat(frame1, frame2, cjd);
[M, om] = CL_fr_convertMat(frame1, frame2, cjd, use_interp=%f);
TEST_OK($+1) = find(angmat(M,M_ref) > CL_unitConvert(1e-6,"arcsec","rad")) == [];