// ----------------------------------------------------------------------------------------
// CL_eph_planet
// Tests to catch possible errors
// Internal coherence test
// ----------------------------------------------------------------------------------------

TEST_OK = [];

/////////////////////////////////////////////////////
// Test 1: Unrecognized planet name [model="high"] //
/////////////////////////////////////////////////////
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
try
    [pos,vel] = CL_eph_planet("mars",cjd,model = "high");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

//////////////////////////////////////////////////////////////////////
// Test 2: Unrecognized planet name (case sensitive!) [model="med"] //
//////////////////////////////////////////////////////////////////////
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
try
    CL_eph_planet("mars",cjd,model = "med");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

////////////////////////////////
// Test 3: Unrecognized model //
////////////////////////////////
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
try
    CL_eph_planet("Mars",cjd,model = "low");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

///////////////////////////////////////////////
// Test 4: Invalid number of input arguments //
///////////////////////////////////////////////
try
    CL_eph_planet("Mars");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

//////////////////////////////////////////
// Test 5: Test at TT date (tt_ref = 0) //
//////////////////////////////////////////
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
[pos1,vel1] = CL_eph_planet("Mars",cjd);

tt_tref = 0;
[pos2,vel2] = CL_eph_planet("Mars",cjd,tt_tref)

TEST_OK(1+$)= pos1(1)<>pos2(1) & vel1(1)<>vel2(1);
TEST_OK(1+$)= pos1(2)<>pos2(2) & vel1(2)<>vel2(2);
TEST_OK(1+$)= pos1(3)<>pos2(3) & vel1(1)<>vel2(3);
