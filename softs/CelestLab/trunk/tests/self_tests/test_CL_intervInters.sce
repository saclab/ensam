// ------------------------------
// CL_intervInters : 
// ------------------------------


TEST_OK = [];

// Intersection entre 2 intervalles elementaires : 
// - i1 = un intervalle (2x1)
// - i2 = un intervalle (2x1)
// - ires = intervalle de l'intersection (2x1 ou [] si pas d'intersection)
function ires = calc_inters_elem(i1,i2)

  if ( (i1(1) <= i2(1) & i1(2) <= i2(1)) |   ...
      (i1(1) >= i2(2) & i1(2) >= i2(2)) )
    ires = []; // pas d'intersection
  else
    ires = [ max(i1(1), i2(1)) ;
             min(i1(2), i2(2)) ];
  end
endfunction

// Calcul, de facon systematique (avec des boucles) de l'intersection de deux ensembles d'intervalles
// - i1 = un ensemble d'intervalles (2xN1)
// - i2 = un ensemble d'intervalles (2xN1)
// - ires = l'ensemble d'intervalles des intersections (2xN)
function ires = calc_inters(i1,i2)

  N1 = size(i1,2);
  N2 = size(i2,2);
  
  ires = [];
  // On prend tous les intervalles deux a deux
  for j = 1 : N1
    for k = 1 : N2
      ires = [ires, calc_inters_elem(i1(:,j),i2(:,k)) ];
    end
  end
  
endfunction


// Tirage aleatoire de N intervalles compris entre 0 et val_max
// La taille de chaque intervalle est de taille comprise entre 1 et taille_max.
function ires = gen_intervalle_aleat(N,val_max,taille_max)
  deb = floor( rand(1,N)*val_max );
  fin = deb + ceil( rand(1,N)*taille_max )
  ires = CL_intervUnion([deb ; fin]); // pour s'assurer que tous les intervalles sont disjoints
endfunction


// --- Test 1 : Cas speciaux choisis:
// - 2 intervalles egaux
// - debut d'un intervalle de i1 = fin d'un intervalle de i2
// - fin d'un intervalle de i1 = debut d'un intervalle de i2
// - un intervalle de i2 entierement inclus dans i1
// - un intervalle de i1 entierement inclus dans i2
i1 = [ [0;1] , [3;4] , [10;11] , [12;15] , [20;21]];
i2 = [ [0;1] , [1.5;3] , [4;4.2] , [13;14] , [19;22]];
ires = CL_intervInters(i1,i2);
ires2 = calc_inters(i1,i2);
TEST_OK($+1) = CL__isEqual(ires,ires2);


// --- Test 2 : Intervalles aleatoires
N = 100;
val_max = 1000;
taille_max = 10;

i1 = gen_intervalle_aleat(N,val_max,taille_max);
i2 = gen_intervalle_aleat(N,val_max,taille_max);

ires = CL_intervInters(i1,i2);
ires2 = calc_inters(i1,i2);

TEST_OK($+1) = CL__isEqual(ires,ires2);