// ------------------------------
// CL_ip_escapeDv: 
// Verification du DV de liberation en appliquant le DV sur l'orbite elliptique
// et en verifiant qu'on obtient la bonne orbite hyperbolique
// ------------------------------

TEST_OK = [];

// Cas 1 : Escape from LEO circular orbit 
sma = 6700.e3;
ecc = 0.01;
vinf = 6.e3;
v = linspace(0,2*%pi,20);

// Computing maneuver
[dv,rph,tanoh] = CL_ip_escapeDv(sma, ecc, vinf, v);

// Apply the DV:
M = CL_kp_v2M(ecc,v);
DV = [dv; zeros(dv); zeros(dv)]; // in tnw frame (dv is tangential)
kep = [sma*ones(M); ecc*ones(M); 1*ones(M); 2*ones(M) ;3*ones(M) ; M];
kep_dv = CL_man_applyDvKep(kep,DV, "tnw");

// New semi-major axis and eccentricity
sma_dv = kep_dv(1,:);
ecc_dv = kep_dv(2,:);

// Check true hyperbolic anomaly 
// (compare in absolute precision because close to 0 the relative precision is not good)
tanoh_ref = CL_kp_M2v(ecc_dv, kep_dv(6,:));
TEST_OK($+1) = CL__isEqual(tanoh,tanoh_ref,1e-13,%f);

// Check Vinf and periapsis radius of hyperbolic orbit
res = CL_kp_characteristics(sma_dv,ecc_dv,tanoh);
TEST_OK($+1) = CL__isEqual(res.vinf,vinf*ones(v));
TEST_OK($+1) = CL__isEqual(res.rp,rph);

// Cas 2: Negative semi-axis.
SmaEll  = [-350.e3 , %CL_eqRad+700.e3];
try
  [DVManEscape, RadPerigHyp,TAnomHyp] = CL_ip_escapeDv(SmaEll, EccEllip, Vinfinity);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  
 
//Cas 3: Negative Eccentricity.
SmaEll = [%CL_eqRad+350.e3 , %CL_eqRad+700.e3];
EccEllip    = [0.0 , -1.0];
try
  [DVManEscape, RadPerigHyp,TAnomHyp] = CL_ip_escapeDv(SmaEll, EccEllip, Vinfinity);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

//Cas 4: Negative V-infinity.
EccEllip  = [0.0 , 0.0];
Vinfinity = [-5 , 6] * 1.e3;
try
  [DVManEscape, RadPerigHyp,TAnomHyp] = CL_ip_escapeDv(SmaEll, EccEllip, Vinfinity);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  
