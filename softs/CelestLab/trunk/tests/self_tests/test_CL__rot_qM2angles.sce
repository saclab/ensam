// ------------------------------
// Consistency tests of functions :
// CL_rot_qM2angles
// CL_rot_angles2quat
// ------------------------------

TEST_OK = [];

tabnaxes_euler = [ 
1,2,1; 
1,3,1; 
2,1,2; 
2,3,2; 
3,1,3; 
3,2,3 
]' ;


tabnaxes_cardan = [ 
1,2,3; 
1,3,2; 
2,3,1; 
2,1,3; 
3,1,2; 
3,2,1; 
]' ;


function [is_equal,err_rel_max] = lance_cas(tabnaxes,angles,prec)

err_rel_max = 0;
is_equal = %t;

for i = 1:size(tabnaxes,2)
  naxes = tabnaxes(:,i)'; 
  q = CL_rot_angles2quat(naxes, angles); 
  angles2 = CL__rot_qM2angles(q, naxes);
  [is_equal_tmp,err_rel_max_tmp] = CL__isEqual(angles,angles2,prec);
  err_rel_max = max( err_rel_max_tmp , err_rel_max);
  if (is_equal_tmp == %f); is_equal = %f; end;
end

endfunction


// Test 1 : random angles (Cardan)
// N = 10; 
// angle1 = (rand(1:N)-0.5)*2*%pi; // -pi -> pi
// angle2 = (rand(1:N)-0.5)*%pi; // -pi/2 -> pi/2
// angle3 = (rand(1:N)-0.5)*2*%pi; // -pi -> pi
angles = [  
  -1.6834237  -1.6887712  -1.7815139   2.4089028   0.9582705  -1.2088278   2.7203781  -1.7932161  -1.1772051  -0.8693660  
  -0.6527392   0.2086799  -0.0545154  -0.5272476   0.2937687   0.0048197  -0.1983641  -0.7247262   0.4164950  -0.2978374  
   2.6293295  -2.8668074  -0.1140342  -1.4831107  -0.5352622  -1.3782179  -2.3373082   1.7486913  -1.8101666  -2.4370247];
[is_equal,err_rel_max] = lance_cas(tabnaxes_cardan,angles , 1e-13);
TEST_OK($+1) = is_equal;

// Test 2 : random angles (Euler)
// N = 10;
// angle1 = (rand(1:N)-0.5)*2*%pi; // -pi -> pi
// angle2 = rand(1:N)*%pi;         // 0 -> pi
// angle3 = (rand(1:N)-0.5)*2*%pi; // -pi -> pi
angles = [    
  1.1667221  -2.1795008   1.238322    2.1460335  -0.5893472  -0.5687379   2.3776364  -2.4263402  -1.886       0.3887160  
  1.8523387   2.1532412   2.797973    1.5840579   1.0975516   1.2169835   2.8974591   2.980801    1.079243    1.1812761  
  1.4708563  -1.4980612  -0.0040879  -1.4837249   0.1593184   0.2363922  -2.3876572  -1.7239154   0.8005363   1.6389265]; 
[is_equal,err_rel_max] = lance_cas(tabnaxes_euler,angles , 1e-13);
TEST_OK($+1) = is_equal;

// Test 3 : angles close to singularities (Cardan) 
TEST_OK($+1) = %t;
x = [ %pi/2 - [0, 1e-15, 1e-14, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5 ], ..
      %pi/2 + [0, 1e-15, 1e-14, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5 ] ]; 

angles = [ 3 * ones(x) ; x ; 2 * ones(x) ];
for i = 1:size(tabnaxes_cardan,2)
  naxes = tabnaxes_cardan(:,i)'; 
  q = CL_rot_angles2quat(naxes, angles); 
  angles2 = CL__rot_qM2angles(q, naxes);
  q2 = CL_rot_angles2quat(naxes, angles2); 

  qi = q * q2'; 
  I = find(qi.r < 0); 
  if (I <> []); qi(I) = -qi(I); end
  if (max(abs(qi.r -1), abs(qi.i(1,:)), abs(qi.i(2,:)), abs(qi.i(3,:))) > 1.e-5); TEST_OK($) = %f; end 
end


// Test 4 : angles close to singularities (Euler) 
TEST_OK($+1) = %t;
x = [ [0, 1e-15, 1e-14, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5 ], .. 
     %pi - [0, 1e-15, 1e-14, 1e-14, 1e-13, 1e-12, 1e-11, 1e-10, 1e-9, 1e-8, 1e-7, 1e-6, 1e-5 ] ]; 
angles = [ 3 * ones(x) ; x ; 2 * ones(x) ];
for i = 1:size(tabnaxes_euler,2)
  naxes = tabnaxes_euler(:,i)'; 
  q = CL_rot_angles2quat(naxes, angles); 
  angles2 = CL__rot_qM2angles(q, naxes);
  q2 = CL_rot_angles2quat(naxes, angles2); 

  qi = q * q2'; 
  I = find(qi.r < 0); 
  if (I <> []); qi(I) = -qi(I); end
  if (max(abs(qi.r -1), abs(qi.i(1,:)), abs(qi.i(2,:)), abs(qi.i(3,:))) > 1.e-5); TEST_OK($) = %f; end 

end




