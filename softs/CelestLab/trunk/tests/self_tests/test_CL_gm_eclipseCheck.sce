// ------------------------------
// CL_gm_eclipseCheck: 
// ------------------------------

TEST_OK = [];

// ----------------------------
// Test 1
// Fraction of Sun eclipsed by Earth (from an observer in space) 
// The example observer will be a GEO satellite:
// ----------------------------

// Sun and Earth
er_sun = CL_dataGet("body.Sun.eqRad");
er_earth = %CL_eqRad; 
pos_sun = %CL_au * [1; 0; 0];
pos_earth = [0; 0; 0];

// Observer (theoritical circular orbit)
theta = linspace(2.9897255, 3.2934598, 1000);
r = 42160.e3; // orbit's radius [m] 
pos_obs = r * [cos(theta); sin(theta); zeros(theta)];
vel_obs = sqrt(%CL_mu/r) * [-sin(theta); cos(theta); zeros(theta)];

// Use CL_gm_eclipse to find starting and ending pso of eclipse
[kep,jacob] = CL_oe_car2kep(pos_obs,vel_obs);
sun_sph = CL_co_car2sph(pos_sun);
alpha_sun = sun_sph(1);
delta_sun = sun_sph(2);
res = CL_gm_eclipse(kep(1),kep(2),kep(3),kep(4),kep(5),alpha_sun,delta_sun);



// Check that there is an eclipse at these pso
pos_obs_start = r * [cos(res.start.pso); sin(res.start.pso); 0];
rat_ecl_start = CL_gm_eclipseCheck(pos_obs_start, pos_sun, pos_earth, er_sun, er_earth);
pos_obs_end = r * [cos(res.end.pso); sin(res.end.pso); 0];
rat_ecl_end = CL_gm_eclipseCheck(pos_obs_end, pos_sun, pos_earth, er_sun, er_earth);

TEST_OK($+1) = rat_ecl_start > 0 & rat_ecl_end > 0;

// ----------------------------
// Test 2
// error if radii are not positive
// ---------------------------- 
theta = linspace(2.9897255, 3.2934598, 1000);
r = 42160.e3; // orbit's radius [m] 
pos_obs = r * [cos(theta); sin(theta); zeros(theta)]; // satellite position
pos1 = %CL_au * [1; 0; 0]; // position object 1
sr1 = -2000.e3;
pos2 = [0; 0; 0]; // position object 2
sr2 = -5000.e3;

try 
  CL_gm_eclipseCheck(pos_obs, pos1, pos2, sr1, sr2);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  


// ----------------------------
// Test 2
// error if observer is inside both spheres
// ----------------------------
theta = linspace(2.9897255, 3.2934598, 1000);
r = 1000.e3; // orbit's radius [m] 
pos_obs = r * [cos(theta); sin(theta); zeros(theta)]; // satellite position
pos1 = %CL_au * [1; 0; 0]; // position object 1
sr1 = 2000.e3;
pos2 = [0; 0; 0]; // position object 2
sr2 = 5000.e3;

try 
  CL_gm_eclipseCheck(pos_obs, pos1, pos2, sr1, sr2)
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

