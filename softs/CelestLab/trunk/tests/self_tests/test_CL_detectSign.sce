//----------------
// CL_detectSign
// Check intervals are correct 
// Test1: sign = positive  (interpolated data)
// Test2: sign = positive  (external function) 
// Test3: sign = negative  (interpolated data)
//----------------

TEST_OK = [];
 
// ===========================================================
// Test1
// - sgn == "+"
// - Comparison with analytical solution (number and value of solutions)
// ===========================================================

x = linspace(0, 10, 100);
y = sin(x);

interv_ref = [[0; %pi], [2*%pi; 3*%pi]];

// Return intervals where y is negative
sgn = "+";
// absolute precision
ytol =  1.e-6;

[interv] = CL_detectSign(x, y, sgn, ytol = ytol);

// Check that we have 2 intervals
N = size(interv, 2); 
TEST_OK($+1) = CL__isEqual(N, size(interv_ref, 2));

// Check that computed solution are accurate enough
TEST_OK($+1) = CL__isEqual(interv, interv_ref, prec = ytol);


// ===========================================================
// Test2
// - sgn == "+"
// - External function ("fct") is used (y = fct(x))
// - Comparison with analytical solution (number and value of solutions)
// ===========================================================

x = linspace(0, 10, 100);

function [y] = fct(x)
  y = sin(x);
endfunction

// Return intervals where y is positive
sgn = "+";
// absolute precision
ytol =  1.e-6;

[interv] = CL_detectSign(x, list(fct), sgn, ytol = ytol);

// Check that we have 2 intervals
N = size(interv, 2); 
TEST_OK($+1) = CL__isEqual(N, size(interv_ref, 2));

// Check that computed solution are accurate enough
TEST_OK($+1) = CL__isEqual(interv, interv_ref, prec = ytol);


// ===========================================================
// Test3
// - sgn == "-"
// - Comparison with analytical solution (number and value of solutions)
// ===========================================================

x = linspace(0, 10, 100);
y = sin(x);

interv_ref = [[%pi; 2*%pi], [3*%pi; x($)]];

// Return intervals where y is negative
sgn = "-";
// absolute precision
ytol =  1.e-6;

[interv] = CL_detectSign(x, y, sgn, ytol = ytol);

// Check that we have 1 interval
N = size(interv, 2);
Nref = size(interv_ref, 2);
TEST_OK($+1) = CL__isEqual(N, Nref);

// Check that computed solution are accurate enough
TEST_OK($+1) = CL__isEqual(interv, interv_ref, prec = ytol);

