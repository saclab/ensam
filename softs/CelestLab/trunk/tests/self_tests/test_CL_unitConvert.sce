// ------------------------------
// CL_unitConvert: 
// ------------------------------

TEST_OK = [];
m_val=2;
s_val=2;
day_val = 2;
kg_val=2;
rad_val=3.14159;
deg_val = 30;
kgms2_val= 2;
Hz_val=2;
arcsec_val=50;

[km_out] = CL_unitConvert(m_val,"m","km");
km_res = m_val * 0.001;
TEST_OK($+1) = CL__isEqual( km_out, km_res );

[au_out] = CL_unitConvert(m_val,"m","au");
au_res = m_val/%CL_au ; 
TEST_OK($+1) = CL__isEqual( au_out, au_res);

[h_out] = CL_unitConvert(s_val,"s","h");
h_res = s_val/3600;
TEST_OK($+1) = CL__isEqual( h_out, h_res);

[mn_out] = CL_unitConvert(s_val,"s","mn");
mn_res = s_val/60;
TEST_OK($+1) = CL__isEqual( mn_out, mn_res);

[day_out] = CL_unitConvert(s_val,"s","day")
day_res = s_val/86400;
TEST_OK($+1) = CL__isEqual( day_out, day_res);

[yr_out] = CL_unitConvert(day_val,"day","yr");
yr_res = day_val/365.25;
TEST_OK($+1) = CL__isEqual(yr_out , yr_res);

[g_out] = CL_unitConvert(kg_val,"kg","g");
g_res = 1000*kg_val;
TEST_OK($+1) = CL__isEqual( g_out , g_res);

[deg_out] = CL_unitConvert(rad_val,"rad","deg");
deg_res = rad_val/(%pi/180);
TEST_OK($+1) = CL__isEqual( deg_out , deg_res);

[N_out] = CL_unitConvert(kgms2_val,"kg*m/s^2","N");
N_res = 1 * kgms2_val;
TEST_OK($+1) = CL__isEqual( N_out , N_res);

[kHz_out] = CL_unitConvert(Hz_val,"Hz","kHz");
kHz_res = 0.001 * Hz_val;
TEST_OK($+1) = CL__isEqual( kHz_out , kHz_res);

[arcmin_out] = CL_unitConvert(deg_val,"deg","arcmin");
arcmin_res =  deg_val/(1/60) ;
TEST_OK($+1) = CL__isEqual( arcmin_out , arcmin_res);

[arcsec_out] = CL_unitConvert(deg_val,"deg","arcsec");
arcsec_res =  deg_val/(1/3600);
TEST_OK($+1) = CL__isEqual( arcsec_out , arcsec_res);

[mas_out] = CL_unitConvert(arcsec_val,"arcsec","mas");
mas_res = arcsec_val/(1/1000);
TEST_OK($+1) = CL__isEqual( mas_out , mas_res);
