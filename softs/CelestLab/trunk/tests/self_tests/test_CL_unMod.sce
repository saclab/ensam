// ------------------------------
// CL_unMod
// ------------------------------

TEST_OK=[];

///////////////////////////////////////////////////////////////////////////
// Test 1 : Nominal case finely sampled with discontinous linear function //
///////////////////////////////////////////////////////////////////////////
x1=[-1:0.001:1];
x2=[1:0.001:3];
x3=[3:0.001:5];
x =[x1,x2,x3];

// Discontinuous Linear function
y=repmat(x1,1,3);

// Removing the discontinuity 
res=CL_unMod(y,2);
// Result Check
TEST_OK($+1)=CL__isEqual(res,x);

//////////////////////////////////////////////////////////////////////////
// Test 2 : Nominal case finely sampled with discontinous sinus function //
//////////////////////////////////////////////////////////////////////////
x1=[-1:0.001:1];

// Discontinuous sinus function
y=repmat(sin(x1),1,3);
jump=abs(sin(x1(1))-sin(x1($)));

// Removing the discontinuity 
res=CL_unMod(y,jump);

// Reference Data
ref=[sin(x1),sin(x1)+jump,sin(x1)+2*jump];
// Result Check
TEST_OK($+1)=CL__isEqual(res,ref);

//////////////////////////////////////////////////////////////////////////
// Test 3 : Nominal case finely sampled with discontinous sinus function //
//////////////////////////////////////////////////////////////////////////
x1=[-1:0.5:1];

// Discontinuous sinus function
y=repmat(sin(x1),1,3);
jump=abs(sin(x1(1))-sin(x1($)));

// Removing the discontinuity 
res=CL_unMod(y,jump);

// Reference Data
ref=[sin(x1),sin(x1)+jump,sin(x1)+2*jump];
// Result Check
TEST_OK($+1)=CL__isEqual(res,ref);

//////////////////////////////////////////////////
// Test 4: Invalid parameter a (it shall be >0) //
//////////////////////////////////////////////////
x1 = [-1:0.2:5];
y = repmat(x1,1,3);
a = [0 -3];
try
    CL_unMod(y, a(1));
    TEST_OK($+1)=%f;
catch
    TEST_OK($+1)=%t;
end
try
    CL_unMod(y, a(2));
    TEST_OK($+1)=%f;
catch
    TEST_OK($+1)=%t;
end

/////////////////////////////////////////////////////////////////////
// Test 5: Optional argument x shall be [] or an increasing vector //
/////////////////////////////////////////////////////////////////////
// Test 5.1: in case of being [], the result shall be the same as if no x argument
//           is used
x1 = [-1:0.2:5];
y = repmat(x1,1,3);
a = 2;
x = [];

yc = CL_unMod(y,a);
yc2 = CL_unMod(y,a,x);
TEST_OK($+1) = CL__isEqual(yc,yc2);

// Test 5.2: is case of being a vector, it shall be an increasing vector
x1 = [-1:0.2:5];
y = repmat(x1,1,3);
a = 2;
x1 = [1:1:31];
x2 = [-29:1:1];
x3 = x1;
x =[x1,x2,x3];
slope = 1;
try
    CL_unMod(y, a, x, slope);
    TEST_OK($+1)=%f;
catch
    TEST_OK($+1)=%t;
end

///////////////////////////////////////////////////////////////////
// Test 6: Inconsistent value for x and slope optional arguments //
///////////////////////////////////////////////////////////////////
// Note: both arguments shall be either [] either <>[]
x1 = [-1:0.2:5];
y = repmat(x1,1,3);
a = 2;
slope = 1; // same result if x <> [] and slope = []
try
    CL_unMod(y, a, slope);
    TEST_OK($+1)=%f;
catch
    TEST_OK($+1)=%t;
end

/////////////////////////////
// Test 7: y contains %nan //
/////////////////////////////
a = 2;
x1 = [-1:0.2:5];

// Discontinuous Linear function
y1 = repmat(x1,1,3);
// Discontinuous Linear function with %nan
y = [%nan, %nan, %nan, y1, %nan, %nan, %nan];

// Removing the discontinuity 
res1 = CL_unMod(y1,a);
res = CL_unMod(y,a);
res1_with_nan = [%nan %nan %nan res1 %nan %nan %nan];

// Result Check
TEST_OK($+1)=CL__isEqual(res,res1_with_nan);

/////////////////////////////////////////////////////////
// Test 8: Argument optional 'slope' in vectorial form //
/////////////////////////////////////////////////////////
x = 0:0.2:30;
y = CL_rMod(x, 0, 2*%pi);
x = x(1:20:$);
y = y(1:20:$);
slope = ones(1,8);

TEST_OK($+1) = CL__isEqual(CL_unMod(y, 2*%pi, x, slope)-x,zeros(1,8));
