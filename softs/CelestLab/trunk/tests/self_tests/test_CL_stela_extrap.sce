//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// ------------------------------
// TEST CL_stela_extrap
//
// 1 - kep0 (ECI) -> propagation   => kep
//     kep0 (ECI) -> conversion to MOD -> propagation -> conversion to ECI => kep2
//     kep2 and kep should be identical (because STELA converts to MOD in all cases) 
//     (NB: reference frame for propagation is MOD)

// 2 - kep0 (ECI) -> propagation   => kep
//     kep0 (ECI) -> conversion to "cir" -> propagation -> conversion to kep => kep2
// ------------------------------

TEST_OK = [];

// Common input data

// Initial time (cjd, TREF)
cjd0 = 20000;

// Keplerian mean orbital elements (ECI)
kep0 = [7.e6; 1.e-3; 98*(%pi/180); %pi/2; 0.5; 1];

// Final dates
cjd = cjd0 + (0: 10);

// STELA model parameters (default values)
cst = CL_stela_getConst(); 
params = CL_stela_params();
params.ref_frame = "MOD";

// Propagation (ECI frame) - reference results
kep = CL_stela_extrap("kep", cjd0, kep0, cjd, params, frame = "ECI", res = ["m"]);

//=====================
// Propagation with orbital state in MOD 
//=====================

function [kep2] = TEST1()
  [pos_eci0, vel_eci0] = CL_oe_kep2car(kep0, mu=cst.mu);
  [pos_mod0, vel_mod0] = CL_fr_convert("ECI", "MOD", cjd0, pos_eci0, vel_eci0);
  kep0_mod             = CL_oe_car2kep(pos_mod0, vel_mod0, mu=cst.mu);

  kep2                   = CL_stela_extrap("kep", cjd0, kep0_mod, cjd, frame = "MOD", res = ["m"]);
  [pos_mod2, vel_mod2]   = CL_oe_kep2car(kep2, mu=cst.mu);
  [pos_eci2, vel_eci2]   = CL_fr_convert("MOD", "ECI", cjd, pos_mod2, vel_mod2);
  kep2 = CL_oe_car2kep(pos_eci2, vel_eci2, mu=cst.mu);
  kep2(6,:) = CL_rMod(kep2(6,:), kep(6,:) - %pi, kep(6,:) + %pi);
endfunction

kep2 = TEST1(); 
dkep = (kep2-kep) ./ (max(abs(kep), "c") * ones(cjd)); 

TEST_OK($+1) = CL__isEqual(dkep, zeros(dkep), relative = %f, prec = 1.e-11);


//=====================
// Propagation with orbital state as "cir" type 
//=====================

function [kep2] = TEST2()
  cir0 = CL_oe_kep2cir(kep0);
  cir = CL_stela_extrap("cir", cjd0, cir0, cjd,frame = "ECI", res = ["m"]);

  kep2 = CL_oe_cir2kep(cir);
  kep2(6,:) = CL_rMod(kep2(6,:), kep(6,:) - %pi, kep(6,:) + %pi);
endfunction 

kep2 = TEST2(); 
dkep = (kep2-kep) ./ (max(abs(kep), "c") * ones(cjd)); 

TEST_OK($+1) = CL__isEqual(dkep, zeros(dkep), relative = %f, prec = 1.e-11);

