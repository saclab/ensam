// ------------------------------
// Consistency of CL_rot_quat2axAng and CL_rot_axAng2quat 
// ------------------------------

TEST_OK = [];


// --- TEST 1 : Standard cases
ax = [1;0;0];
N = 100;
ang = linspace(-2*%pi,2*%pi,N);

q = CL_rot_axAng2quat(ax,ang);
[ax2,ang2] = CL_rot_quat2axAng(q);
q2 = CL_rot_axAng2quat(ax2,ang2);

qt = q'*q2;

// qt should be identity
TEST_OK($+1) = CL__isEqual(ones(qt.r),abs(qt.r)) & ...
               CL__isEqual(zeros(qt.i),qt.i) ;
 

// --- TEST 2 : Accuracy test
ax = [1;0;0];
eps = 1e-14;
ang = [eps, -eps, %pi/2-eps, %pi/2+eps, %pi-eps, %pi+eps, 3*%pi/2-eps, 3*%pi/2+eps];

q = CL_rot_axAng2quat(ax,ang);
[ax2,ang2] = CL_rot_quat2axAng(q);
q2 = CL_rot_axAng2quat(ax2,ang2);

qt = q'*q2;

// qt should be identity
TEST_OK($+1) = CL__isEqual(ones(qt.r),abs(qt.r)) & ...
               CL__isEqual(zeros(qt.i),qt.i) ;
 
 
 
// --- TEST 3 : Special case q = [0;0;0;0]
q = CL__defQuat(0,[0;0;0]);
[ax,ang] = CL_rot_quat2axAng(q);

// ax and ang should be %nan
TEST_OK($+1) = find(~isnan([ax;ang])) == [] ;


// --- TEST 4 : Special case q = []
q = CL__defQuat([],[]);
[ax,ang] = CL_rot_quat2axAng(q);

// ax and ang should be []
TEST_OK($+1) = (ax == []) & (ang == []) ;