// ------------------------------
// CL__kp_M2Ehyp: 
// ------------------------------

TEST_OK = [];

//Case 1 :.
M = (45:5:135);
M = CL_deg2rad(M);
ecc = 1.2.*ones(M);
[E] = CL__kp_M2Ehyp(ecc,M);

M_res = ecc.*sinh(E) - E;

TEST_OK($+1) = CL__isEqual( M , M_res , 1e-15);