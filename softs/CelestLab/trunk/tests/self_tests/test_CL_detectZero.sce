//----------------
// CL_detectZero
// Check correct detection of zero crossings
//
// Test1: with interpolated data (all zeros) 
// Test2: with external function called (y = fct(x))
// Test3: interpolated data (zeros such that direction = increasing) 
// Test4: interpolated data (zeros such that direction = decreasing)
//----------------

TEST_OK = [];
 
// ===========================================================
// Test1
// y = sin(x)
// - Comparison with analytical solution (number and value of solutions)
// ===========================================================

x = linspace(0, 10, 100);
y = sin(x);

xref = [0, %pi, 2*%pi, 3*%pi];

// absolute precision
ytol =  1.e-6;

[xsol, ysol] = CL_detectZero(x, y, ytol = ytol, direct  = "any");

// Check that we have 4 zeros
N = length(xsol); 
TEST_OK($+1) = CL__isEqual(N, length(xref));

// Check that computed solution are accurate enough
TEST_OK($+1) = CL__isEqual(xsol, xref, prec = ytol);
TEST_OK($+1) = CL__isEqual(ysol, zeros(ysol), prec = ytol);


// ===========================================================
// Test2
// - y = sin(x) => using external function
// - Comparison with analytical solution (number and value of solutions)
// ===========================================================

x = linspace(0, 10, 100);

function [y] = fct1(x)
  y = sin(x);
endfunction

[xsol, ysol] = CL_detectZero(x, list(fct1), ytol = ytol);

// Check that we have 4 zeros
N = length(xsol); 
TEST_OK($+1) = CL__isEqual(N, length(xref));

// Check that computed solution are accurate enough
TEST_OK($+1) = CL__isEqual(xsol, xref, prec = ytol);
TEST_OK($+1) = CL__isEqual(ysol, zeros(ysol), prec = ytol);


// ===========================================================
// Test3
// - y = sin(x)
// - Comparison with analytical solution (number and value of solutions)
// - direct == "incr"
// ===========================================================

x = linspace(0, 10, 100);
y = sin(x);

xref = [0, 2*%pi];

// absolute precision
ytol =  1.e-6;

[xsol, ysol] = CL_detectZero(x, y, ytol = ytol, direct = "incr");

// Check that we have 2 zeros
N = length(xsol); 
TEST_OK($+1) = CL__isEqual(N, length(xref));

// Check that computed solution are accurate enough
TEST_OK($+1) = CL__isEqual(xsol, xref, prec = ytol);
TEST_OK($+1) = CL__isEqual(ysol, zeros(ysol), prec = ytol);


// ===========================================================
// Test4
// - y = sin(x) => using external function
// - Comparison with analytical solution (number and value of solutions)
// - direct == "decr"
// ===========================================================

x = linspace(0, 10, 100);
y = sin(x);

xref = [%pi, 3*%pi];

// absolute precision
ytol =  1.e-6;

[xsol, ysol] = CL_detectZero(x, y, ytol = ytol, direct = "decr");

// Check that we have 2 zeros
N = length(xsol); 
TEST_OK($+1) = CL__isEqual(N, length(xref));

// Check that computed solution are accurate enough
TEST_OK($+1) = CL__isEqual(xsol, xref, prec = ytol);
TEST_OK($+1) = CL__isEqual(ysol, zeros(ysol), prec = ytol);


// ===========================================================
// Test4
// - y = sin(x) - 0.99 => using external function
// - Comparison with exact solution (number and value of solutions)
// - Test improvement to increase detection accuracy
// - direct == "decr"
// ===========================================================

x = linspace(0, 10, 60);

function [y] = fct2(x, args)
  y = sin(x) - args.a;
endfunction

args.a = 0.99;
xref = asin(args.a);
xref = [xref, %pi - xref, 2*%pi + xref, 3*%pi - xref];

// absolute precision
ytol =  1.e-8;

[xsol, ysol] = CL_detectZero(x, list(fct2, args), ytol = ytol);

// Check that we have 2 zeros
N = length(xsol); 
TEST_OK($+1) = CL__isEqual(N, length(xref));

// Check that computed solution are accurate enough
TEST_OK($+1) = CL__isEqual(xsol, xref, prec = ytol);
TEST_OK($+1) = CL__isEqual(ysol, zeros(ysol), prec = ytol);
