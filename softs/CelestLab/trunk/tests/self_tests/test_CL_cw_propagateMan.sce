// ------------------------------
// CL_cw_propagateMan : 
// ------------------------------

TEST_OK = [];

////////////////////////////////////////////////////
// Test 1 : Cross validation with CL_cw_propagate //
////////////////////////////////////////////////////
// Note1: If the manoeuvres are void, the resultats are idéntical
// Note2: The optional argument acc has been settled in order to verify that 
//       it is at least well taken into account
alt = 450.e3;
t0 = 0;
pv0 = [1;0;0;1;0;0];
t = (100:100:500)/86400;
tman = [150/86400 300/86400];
dvman = [[0;0;0] [0;0;0]];
acc = [1;0;0];

pv_man = CL_cw_propagateMan(t0, pv0, t, alt, acc, tman, dvman);
pv = CL_cw_propagate(t0, pv0, t, alt, acc);

// Check: as the manoeuvres are zero, both functions shall provide same results
TEST_OK($+1)= CL__isEqual(pv,pv_man,1.e-12);

////////////////////////////////////////////////////////////
// Test 2 : Verify when manoeuvres are taken into account //
////////////////////////////////////////////////////////////
alt = 450.e3;
t0 = 0;
pv0 = [1;0;0;1;0;0];
t = (100:100:500)/86400;
tman = 150/86400;
tman_t0 = 0/86400;
tman_tf = 500/86400;
tman_outside = -100/86400;
tman_outside2 = 600/86400;
dvman = [1;0;0];

//pv = CL_cw_propagateMan(t0, pv0, t, alt,[], tman, dvman)

// Manoeuvre applied at the begining of the interval of time
pv_t0 = CL_cw_propagateMan(t0, pv0, t, alt,[], tman_t0, dvman);

pv0_t0 = pv0 + [0;0;0;[dvman]]; // The initial state vector for the propagator shall
                                // take into account the deltaV
pv = CL_cw_propagate(t0, pv0_t0, t, alt, []);
TEST_OK($+1)= CL__isEqual(pv,pv_t0);

// Manoeuvre applied at the end of the interval of time
pv_tf = CL_cw_propagateMan(t0, pv0, t, alt,[], tman_tf, dvman);

pv = CL_cw_propagate(t0, pv0, t, alt, []);
pv(:,$) = pv(:,$) + [0;0;0;[dvman]]; // The final state vector for the propagator 
                                     // shall take into account the deltaV
TEST_OK($+1)= CL__isEqual(pv,pv_tf);

// Manouevre outside the interval of time after final time
pv_outside = CL_cw_propagateMan(t0, pv0, t, alt,[], tman_outside, dvman);
pv = CL_cw_propagate(t0, pv0, t, alt, []);

// Manoeuvres applied outside the 
// interval time of the propagation
// are ignored; therefore, same results
// obtained when propagating with CL_cw_propagate
TEST_OK($+1)= CL__isEqual(pv,pv_outside); 


// Manouevre outside the interval of time after final time
pv_outside2 = CL_cw_propagateMan(t0, pv0, t, alt,[], tman_outside2, dvman);
pv = CL_cw_propagate(t0, pv0, t, alt, []);

TEST_OK($+1)= CL__isEqual(pv,pv_outside2);