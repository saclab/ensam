// ------------------------------
// CL_man_dvIncRaanCirc : 
// ------------------------------

TEST_OK = [];

// --- Test 1 : 
sma = 7200.e3;
inci = 1;
raani = 2;
incf = 1.1;
raanf = 1.9;
[deltav,dv,pso] = CL_man_dvIncRaanCirc(sma,inci,raani,incf,raanf)
kep = [sma;0;inci;0;raani;pso];
kep1 = CL_man_applyDvKep(kep,dv);


TEST_OK($+1) = CL__isEqual(sma, kep1(1)) & ...           // sma
               CL__isEqual(kep(2), kep1(2), 1e-14) & ... // ecc
               CL__isEqual(incf, kep1(3)) & ...          // inc
               CL__isEqual(raanf, CL_rMod(kep1(5),-%pi,%pi));  // gom

