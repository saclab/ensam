// ----------------------------
// CL_graduate :
// ----------------------------

TEST_OK = [];

// ---------------------------------
// Test 1 : [vmin, vmax] >= [xmin, xmax]
// ---------------------------------
xmin = 0;
xmax = 20;
[vmin, vmax, nb1, nb2] = CL_graduate(xmin,xmax);

TEST_OK($+1) = (vmin >= xmin) & (vmax >= xmax);




// ---------------------------------
// Test 2 : 
// Check that : 
// step1 = (vmax-vmin) / nb1 = K*10^n with n an integer and K an integer amongst [1 2 3 5 6]
// nb2 is an integer in [1 2 3 4 5 6 12]
// ---------------------------------
N = 1000;
xmin_tab = rand(1,N)*100;
xmax_tab = rand(1,N)*100;
ns1_tab = rand(1,N)*15;
ns2_tab = rand(1,N)*15;

OK = %t;
for k = 1 : N
  xmin = xmin_tab(k);
  xmax = xmax_tab(k);
  ns1 = ns1_tab(k);
  ns2 = ns2_tab(k);
  [vmin, vmax, nb1, nb2] = CL_graduate(xmin,xmax,ns1,ns2);

  // Main step
  step1 = (vmax-vmin) / nb1;
  n = floor(log10(step1));
  K = step1 / 10^n; // should be an integer
  K = round(K); // just to make sure it is an integer

  if (find(K == [1 2 3 5 6 10]) == [])
    OK = %f;
  end
  
  // Sub-step
  if (find(nb2 == [1 2 3 4 5 6 10 12]) == [])
    OK = %f;
  end
  if (OK == %f); pause; end;
end
TEST_OK($+1) = OK;




// ---------------------------------
// Test 3 : if [xmi, xma]=[0,7], [vmin,vmax, nb1, nb2]=[0, 7, 7, 5]
// ---------------------------------
xmi = 0;
xma = 7;
[vmin, vmax, nb1, nb2] = CL_graduate(xmi,xma);

TEST_OK($+1) = CL__isEqual(vmin,0) &...
               CL__isEqual(vmax,7) &...
               CL__isEqual(nb1,7) &...
               CL__isEqual(nb2,5);
     
// ---------------------------------
// Test 4 : if xmin,xmax,ns1,ns2 = 0,3,3,3
// ---------------------------------

[vmin, vmax, nb1, nb2] = CL_graduate(0,3,3,3);

TEST_OK($+1) = CL__isEqual(vmin,0) &...
               CL__isEqual(vmax,3) &...
               CL__isEqual(nb1,3) &...
               CL__isEqual(nb2,2);


// ---------------------------------
// Test 5 : check the procedure in case of ampl <= 0
// ---------------------------------
xmi = 0;
xma = 0;

[vmin, vmax, nb1, nb2] = CL_graduate(xmi,xma);

TEST_OK($+1) = CL__isEqual(vmax-vmin,2.4);
