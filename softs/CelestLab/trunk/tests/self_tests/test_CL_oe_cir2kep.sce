// ------------------------------
// CL_oe_kep2cir
// ------------------------------

TEST_OK = [];

// Verification que cir->kep identique a cir->car->kep
// Orbite quelconque
cir = [ 7078.e3; 0.001 ; 0.002 ;  98*%CL_deg2rad ; 0.2 ; 3 ] * ones(1,2);
[kep,jac1] = CL_oe_cir2kep(cir);
[pos,vel,jac2] = CL_oe_cir2car(cir);
[kep2,jac3] = CL_oe_car2kep(pos,vel);
TEST_OK($+1) = CL__isEqual(kep,kep2,1e-12) & CL__isEqual(jac1 , jac3*jac2, 1e-8) ;