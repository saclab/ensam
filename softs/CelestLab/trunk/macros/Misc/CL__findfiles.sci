//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [files] = CL__findfiles(path,filespec)
// Returns all of the absolute file paths (directories are not returned) found in 
// the directory 'path' and its sub-directories.
//
// Note : Sub-directories starting with "." (hidden directories) are ignored
//
// path : (string) (optional) default is current path (1x1)
// filespec: optionnal file spec (ex : "*.sce"), default is "*" (1x1)
// files: (string) files found (1xN)
  
  if (~exists('path','local')); path = pwd(); end
  if (~exists('filespec','local')); filespec = "*"; end
   
  // files directly in 'path'
  files_tmp = findfiles(path,filespec);
  if (files_tmp <> [])
    files_tmp = path + filesep() + files_tmp;
    files =  files_tmp( ~isdir(files_tmp) )';
  else
    files =  [];
  end
    
  
  // sub-directories that do no start with "." (hidden directories)
  files_tmp = findfiles(path,"*");
  I = find( part(files_tmp,1) == "."); files_tmp(I) = [];
  if (files_tmp ~= [])
    files_tmp = path + filesep() + files_tmp;
    directories = files_tmp( isdir(files_tmp))' ;
    if (directories ~= [])
      for directory = directories
        [files_tmp] = CL__findfiles(directory,filespec);
        files = [files , files_tmp];
      end
    end
  end
  
endfunction
