//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [] = CL__execTests()
// Execute all the validation tests of CelestLab
//
// The validation tests are located in two main folders:
// 1) CL_home()/tests/ref_tests : Contains reference tests
//    These tests consist of reference inputs, and reference outputs
//    The test is considered passed if the difference between the reference output 
//    and the output is less than a given treshold (given in absolute or relative precision).
//    
//    These tests are scripts whose file names all end with ".sce" (standard scripts) 
//    The only requirement for these scripts is that they must define an array TEST_OK that contains booleans.
//    If all the booleans of this array are %T, the test is considered passed.
//
// 2) CL_home()/tests/self_tests : Contains self-validation tests.
//    These tests are autonomous and consist mainly of cross-validation/consistency tests between functions
//    and more generally these tests do not require any reference results.
//
//    These tests are scripts whose file names shall be *.sce.
//    The only requirement for these scripts is that they must define an array TEST_OK that contains booleans.
//    If all the booleans of this array are %T, the test is considered passed.
//
// Additional note
// - All the sub-folders are scanned for tests.

 
// Main function to execute a test
// fpath: (string) Full file path (1x1)
// TEST_OK: Array of booleans, %t if test ok (1xN)
// err: [] if no error, <> [] otherwise 
function [TEST_OK, err] = CL__executeTest(fpath)
  err = []; 
  ier = exec(fpath, 'errcatch', -1);
  if (ier <> 0)
    err = 1;
    TEST_OK = %f;
  end
endfunction
  
  
// -----------------------------
// MAIN FUNCTION  
// -----------------------------
  
mode(-1);

// Change verbose and warning mode to silent
// (will be restored at the end of the function)
warning_mode = CL_configGet("WARNING_MODE");
verbose_mode = CL_configGet("VERBOSE_MODE");
CL_configSet("WARNING_MODE", "silent");
CL_configSet("VERBOSE_MODE", "silent");

// Initialize main CelestLab constants locally
CL_init(%f);


// ---------------------------------------------------
// Execute all tests and print on the screen 
// and in the log file (all_msg) the results
// ---------------------------------------------------
dir_tests = fullfile(CL_home(), "tests");
log_file = fullfile(CL_home(), "test_log.txt"); 

msglog = ["--- Results for all tests "; ..
           msprintf("--- Test directory = %s", dir_tests); "" ] ;
           
mprintf("%s\n", msglog);

fpaths = gsort(CL_path("*.sce", dir_tests, "all"), "lc", "i");
fnames = getrelativefilename(repmat(dir_tests, size(fpaths)), fpaths);

tests_not_OK = []; 

for k = 1 : size(fpaths,2)
  fpath = fpaths(k);
  
  fname = fnames(k);

  mprintf("=> %s\n", fname);

  [TEST_OK, err] = CL__executeTest(fpath);
  
  msg = ""; 
  
  if (err <> [])
    msg = msprintf("%s: UNEXPECTED ERROR", fname); 
    mprintf("%s\n", msg);
    tests_not_OK = [test_not_OK; fname]; 
 
  else
    I = find(~TEST_OK); 
    
    if (I == []); 
      msg = msprintf("%s", fname);
      
    else
      msg = msprintf("%s: NOT OK (output #%d)", fname, I(1));
      mprintf("%s\n", msg);
      tests_not_OK = [tests_not_OK; fname]; 

    end
    
  end
  
  msglog = [msglog; msg];

end


// ------------
// Conclusion
// ------------

if (tests_not_OK == []); 
  msg = "--- Summary: all tests are OK "; 
else
  msg = ["--- Summary: one or more test is not OK: "; tests_not_OK]; 
end

mprintf("\n");
mprintf("%s\n", msg);
msglog = [msglog; ""; msg]; 

// save messages in log file
mputl(msglog, log_file);


// Restore preferences
CL_configSet("WARNING_MODE", warning_mode);
CL_configSet("VERBOSE_MODE", verbose_mode);


endfunction
