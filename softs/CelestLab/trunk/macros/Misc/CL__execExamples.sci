//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Fonction a utiliser pour les developpeurs uniquement.
// Cette fonction sert a lancer tous les examples pour verifier qu'ils fonctionnent
// dirpath_sci : (optionnel, string) Repertoire racine. Par defaut = CL_home()/macros.
function [] = CL__execExamples(dirpath_sci)

  // extrait les ligne de "// Examples" dans un tableau de chaines
  // lignes terminees par ".." : concatenees
  function  [lignes] = extrait_exemple(nomfic)

    lignes = [];
    txt = mgetl(nomfic);
    
    // I = numero de la ligne telle que "// Examples" est en debut de ligne
    I = grep (txt, "/^\/\/ Examples/", flag = "r"); 
    
    if (I == []); return; end

    numlig = I(1)+1;  // numero de la ligne courante
       
    fin_example = %f;   
    while(fin_example == %f)
      if(part(txt(numlig),1:2) ~= "//")
        fin_example=%t;
      else
        taille_ligne = length(txt(numlig));
        new_ligne = stripblanks(part(txt(numlig),3:taille_ligne)); // retire les blancs a la fin de la numlig
        taille_ligne = length(new_ligne);
        if(taille_ligne > 0)
          // Tant qu'il y a ".." a la fin de la numlig
          while(part(new_ligne,taille_ligne-1:taille_ligne) == "..")
            new_ligne = part(new_ligne,1:length(new_ligne)-2); // on retire le .. a la fin
            numlig = numlig + 1 ;
            new_ligne = new_ligne + stripblanks(part(txt(numlig),3:length(txt(numlig)))); // on rajoute la numlig suivante
            taille_ligne = length(new_ligne);
         end
        end
        lignes = [lignes ; new_ligne];
      end
      numlig = numlig + 1 ;
    end

  endfunction

  function  [ierr] = exec_lignes(lignes)
    ierr = execstr(lignes,'errcatch','m');
  endfunction
    
  // dirpath_sci = CL_home()/macros  
  if (~exists('dirpath_sci','local'))   
    path_tmp  = get_function_path('CL__execExamples');
    dirpath_sci = strncpy( path_tmp, length(path_tmp)-length('\misc\CL__execExamples.sci') );  
  end

  mprintf("Lancement de tous les Examples pour verification du bon fonctionnement\n");
  
  exemples_NOK = [];
  
  // sci_files = CL_*.sci and not CL__*.sci
  prefix = "CL";
  sci_files = CL__findfiles(dirpath_sci,prefix+"_*.sci");
  files_not_wanted = CL__findfiles(dirpath_sci,prefix+"__*.sci");
  sci_files = setdiff(sci_files,files_not_wanted);

  for nfic = 1 : size(sci_files,2)
    
    [lignes] = extrait_exemple(sci_files(nfic));

    if (lignes == []); continue; end;
    
    [path,filename,ext] = fileparts(sci_files(nfic));
    mprintf("Exemple en cours : %s ...\n", filename);
    
    [ierr] = exec_lignes(lignes)
    
    if (ierr <> 0)
      exemples_NOK = [exemples_NOK ; filename];
    end
    close(); // supprime l'eventuelle fenetre qui s'est ouverte
    
  end
  
  
  
  if (exemples_NOK == [])
    mprintf("\n=> Tous les exemples sont OK\n");
  else
    mprintf("\n=> Exemples non OK pour les fonctions :\n");
    for k = 1 : size(exemples_NOK,"*")
      mprintf(exemples_NOK(k) +"\n");
    end
  end
  

endfunction
