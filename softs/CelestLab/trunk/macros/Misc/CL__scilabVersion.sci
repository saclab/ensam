//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Retourne la version de Scilab sous la forme d'un entier
// Par exemple 5.5.1 => 551 

function [num] = CL__scilabVersion()

	sciver = getversion("scilab"); // => 4 numbers
    num = sciver(1)*100 + sciver(2)*10 + sciver(3); 
  
endfunction
