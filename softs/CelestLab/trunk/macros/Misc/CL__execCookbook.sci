//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Fonction a utiliser pour les developpeurs uniquement.
// Cette fonction sert a lancer tous les examples du "cookbook"
// dirpath : (optionnel, string) Repertoire racine. 
// Par defaut: dirpath = CL_home()/help/en_US/About

function CL__execCookbook(dirpath)

// extrait les lignes executables de fname
// et concatene tout
function [txt, err] = getcode(fname)
  txt = []; 
  err = 0; 
  
  lignes = mgetl(fname); 
  
  I1 = grep(lignes, "<programlisting"); 
  I2 = grep(lignes, "/programlisting>");
  
  if (length(I1) <> length(I2))
    mprintf("*** Anomaly in file %s\n", fname); 
    err = -1; 
    return; 
  end
  
  txt = []; 
  for k = 1:length(I1)
    txt = [txt; lignes(I1(k)+1:I2(k)-1)];
  end
  
endfunction 


// execute le code txt
function [err] = execcode(txt)
  err = 0; 
  if (txt == []); return; end

  ftmp = fullfile(TMPDIR, "CL_cookbook_code.sce"); 
  mputl(txt, ftmp);

  err = exec(ftmp, "errcatch"); 
  
  xdel(winsid()); 
  mdelete(ftmp); 
   
endfunction 


// extrait lignes de code de fname et execute
function [err] = getexec(fname)
  [txt, err] = getcode(fname); 
  
  if (txt == [] | err <> 0)
    return; 
  end
  
  mprintf("\n-----------------------------------------\n"); 
  mprintf("-> Execution de %s\n", fname); 
  err = execcode(txt); 
  if (err <> 0)
    printf("*** Error in executing %s\n", fname); 
  end
    
endfunction

// ----------------------------------------
// MAIN 
// ----------------------------------------
if (~exists("dirpath", "local"))
  dirpath = fullfile(CL_home(), "help", "en_US", "About"); 
end

fnames = CL_path("*.xml", dirpath, "all"); 
err = zeros(fnames); 

// execution
for k = 1 : size(fnames, "*")
  err(k) = getexec(fnames(k)); 
end

mprintf("\n"); 

// synthesis
I = find(err <> 0); 
if (I == [])
  mprintf("=> All examples are OK\n"); 
else
  mprintf("=> Some examples are not OK:\n"); 
  for k = I
    mprintf("-> %s\n", fnames(k)); 
  end
end

endfunction 






