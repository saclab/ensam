//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function longueur_examples(max_length,CL_path)
  // Fonction a utiliser pour les developpeurs uniquement.
  //
  // Cette fonction sert a renvoyer la taille des lignes exemples.

  if (~exists('CL_path','local'))   
    path_tmp  = get_function_path('longueur_examples');
    CL_path = strncpy( path_tmp, length(path_tmp)-length('\macros\misc\longueur_examples.sci') );  
  end

  mprintf("Verification du nombre de caracteres des lignes dans Examples\n");
  
  
  [path,filename] = get_fic_sci(CL_path)
  
  for nfic = 1 : size(path,2)
    path_sci = path(nfic);
    fic_sci = path_sci + filename(nfic) + ".sci";
    current_max_length = 0;
    txt = mgetl(fic_sci);
    fin_example = %f
    ligne = 1;
    while(fin_example == %f & ligne < size(txt,'*'))
      res = grep(txt(ligne),"// Examples");
      ligne = ligne + 1 ;
      if(res ~= [])
        while(fin_example == %f)
          if(part(txt(ligne),1:2) ~= "//")
            fin_example=%t;
          end
          if(length(txt(ligne)) > current_max_length)
            current_max_length = length(txt(ligne))
          end
          ligne = ligne + 1 ;
        end  
      end
    end
      
    if(current_max_length > max_length)
      mprintf("Example trop long ("+string(current_max_length)+") pour : "+fic_tmp+"\n");
    end
            
  end

endfunction
