//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.




// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2008-2009 - T. Pettersen
// Copyright (C) 2010      - DIGITEO - Allan CORNET
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//==============================================================================
function help_from_sci2(funname,helpdir)
// Generate help files (XML) from the head comments section of a .sci source file.
//
// Calling Sequence
// help_from_sci(funname,helpdir)
//
// Parameters
// funname: the name of a single .sci source file to be processed.
// helpdir: path where the .xml help file will be created.
//
// Description
// help_from_sci is a revised version of the help_skeleton function.
// Its objective is to generate .xml help files based on the head comments section
// of .sci source files.
//
// In order for help_from_sci to format the .xml file properly the
// head comments section should comply with some simple formatting rules.
//
// The first comment line following the function definition should contain a short description
// of the function.
//
// The remaining comments are formatted according to the following (optional) headlines:
// "Calling Sequence", "Parameters", "Description", "Examples", "See also", "Used functions",
// "Authors" and "Bibliography".
//
// The following guidelines should be used when writing the source code comments:
// <itemizedlist>
//    <listitem><literal>Calling Sequence</literal> - one example pr. line.</listitem>
//    <listitem><literal>Parameters</literal> - separate parameter name and
//    description by a ":". Keep the description of each parameter on the same line.</listitem>
//    <listitem><literal>Description</literal> - formatting of the text can be done
//    using XML commands. Compare the output of head_comments("help_from_sci") with help("help_from_sci")
//    to get some hints.
//    Adding an empty comment line in the Description section is interpreted as the
//    start of a new paragraph.</listitem>
//    <listitem><literal>See also</literal> - list one function name pr line.</listitem>
//    <listitem><literal>Authors</literal> - write one author on each line following
//    the Authors headline. Use ";" to separate the authors name
//    from any add additional information.</listitem>
//    <listitem><literal>Bibliography</literal> - write one reference pr line
//    following the References headline.</listitem>
// </itemizedlist>
//
//  Examples
// help_from_sci("D:\PROGRAM\Celestlab\trunk\macros\coordinates_frames\CL_co_car2ell","D:\PROGRAM\Celestlab\trunk\\help\en_US\coordinates_frames\")
//
// See also
// help_from_sci
//
// Authors
// Original code : T. Pettersen ; top@tpett.com
// Modified by CNES - DCT/SB

function s = date_str()
  date_tmp = clock();
  s = msprintf("%04d/%02d/%02d", date_tmp(1), date_tmp(2), date_tmp(3));
endfunction


out = tokens(pathconvert(funname), filesep());
out = out($);
out = tokens(out,".");
out = out(1);      // remove .sci (...wont work for fil.name.sci)

verno = ver();
verno = verno(1,2);
 
helptxt = [
"<?xml version=""1.0"" encoding=""UTF-8""?>"
""
"<!--"
" * "
" * This help file was generated from "+out+".sci using help_from_sci2()."
" * "
" -->"
""
"<refentry version=""5.0-subset Scilab"" xml:id="""+out+""" xml:lang=""en"""
"          xmlns=""http://docbook.org/ns/docbook"""
"          xmlns:xlink=""http://www.w3.org/1999/xlink"""
"          xmlns:svg=""http://www.w3.org/2000/svg"""
"          xmlns:ns3=""http://www.w3.org/1999/xhtml"""
"          xmlns:mml=""http://www.w3.org/1998/Math/MathML"""
"          xmlns:db=""http://docbook.org/ns/docbook"">"
""
"  <info>"
"    <pubdate>$LastChangedDate: " + date_str() + " $</pubdate>"
"  </info>"
""
];

if isempty(strindex(funname, ".sci")) then funname = funname + ".sci"; end;
if isempty(fileinfo(funname)) then
  error(sprintf(gettext("%s: The file %s does not exist.\n"),"help_from_sci",funname));
end;
f = mopen(funname, "rt");
if isempty(f) then
  error(sprintf(gettext("%s: Cannot open file %s.\n"), "help_from_sci", funname + ".sci"));
end
line = " ";
doc = [];

while isempty(strindex(line, "function ")) & ~meof(f), line = mgetl(f, 1); end

line = mgetl(f,1);
line = replaceTabBySpace(line);
short_descr = stripblanks(strsubst(line, "//", ""), %T);
helptxt = [helptxt;
"  <refnamediv>"
"    <refname>"+out+"</refname><refpurpose>"+short_descr+"</refpurpose>"
"  </refnamediv>"
""];

cmds = ["CALLING SEQUENCE", "PARAMETERS", "DESCRIPTION", "EXAMPLES", "SEE ALSO", ..
        "AUTHORS", "BIBLIOGRAPHY", "USED FUNCTIONS"];

doing = "search";
i = strindex(line, "//");
line = mgetl(f, 1);
line = replaceTabBySpace(line);
// Continue until empty line or end of file or a scilab command line (Bug#5487)
while (~isempty(stripblanks(line)) & ~meof(f)) & ~isempty(regexp(stripblanks(line),"/^\/\/*/"))
  if stripblanks(line) == "//" then
    if doing == "Description" then
      in = "new_descr_param";
    else
      in = "";
    end
  else
    in = strsplit(line, i(1) + 1);
    in = stripblanks(in(2));
    if (doing ~= "Examples") then // Replacing characters like <, > or & should not be done in the Examples
      in = strsubst(in, "&", "&amp;"); // remove elements that make xml crash.
      in=strsubst(in,'&amp;#','&#'); // If it was an HTML entity, leave it as it was.
      in = strsubst(in, "< ", "&lt; ");
      if strindex(in ,"<") then if isempty(regexp(in, "/\<*[a-z]\>/")) then in = strsubst(in, "<", "&lt;"); end; end
      in = strsubst(in, " >", " &gt;");
      if strindex(in, ">") then if isempty(regexp(in, "/\<*[a-z]\>/")) then in = strsubst(in, ">", "&gt;"); end; end
      in = strsubst(in, "<b>" , "<emphasis role=""bold"">" );
      in = strsubst(in, "</b>" , "</emphasis>" );
      in = strsubst(in, "<b>" , "<emphasis role=""bold"">" );
      in = strsubst(in, "</b>" , "</emphasis>" );
      in = strsubst(in, "<p>" , "<para>" );
      in = strsubst(in, "</p>" , "</para>" );
    end
  end

  IN = convstr(in, "u");
  if find(cmds == IN) then
    [add_txt, doing] = change_activity(doing, in);
    helptxt = [helptxt; add_txt];
  else
    if doing == "Calling Sequence" then
      helptxt = [helptxt;"   " + in];
    elseif doing == "Parameters" then
      i = strindex(in, ":");
      //if ~isempty(i) then   // MODIF CODE ORIGINAL
        if length(in) > i(1) then
          in = strsplit(in,i(1));
          par_name = in(1);
          par_descr = in(2);
        else
          par_name = in;
          par_descr = " ";
        end
        helptxt = [helptxt; "   <varlistentry><term>" + par_name + "</term>"];
        helptxt = [helptxt;"      <listitem><para>" + par_descr + "</para></listitem></varlistentry>"];
      //end  // MODIF CODE ORIGINAL
    elseif doing == "Description" & in == "new_descr_param" then
      helptxt = [helptxt];
    elseif doing == "Description" then
      helptxt = [helptxt; in];
    elseif doing == "Examples" & convstr(in, "u") ~= "EXAMPLES" then
      helptxt = [helptxt; in];
    elseif doing == "See also" & convstr(in, "u") ~= "SEE ALSO" & ~isempty(stripblanks(in)) then
      str = stripblanks(in);
      i = strindex(str, " ");
      if i <> [] then
          str = stripblanks(strsplit(str, i(1)));
      else
          str = [str str];
      end
      helptxt = [helptxt; "   <member><para><link linkend=""" + str(1) + """>" + str(2) + "</link></para></member>"];
    elseif doing == "Authors" & convstr(in, "u") ~= "AUTHORS" & ~isempty(stripblanks(in)) then
      [name, ref] = chop(in, ";");
      if isempty(ref) then
        helptxt = [helptxt; "   <para>" + name + "</para>"];
      else
        helptxt = [helptxt; "   <para>" + name + "</para><listitem><para>" + ref + "</para></listitem>"];
      end
    elseif doing == "Bibliography" & convstr(in, "u") ~= "BIBLIOGRAPHY" & ~isempty(stripblanks(in)) then
      helptxt = [helptxt;"   <listitem><para>" + in + "</para></listitem>"];
    elseif doing == "Used functions" & convstr(in, "u") ~= "USED FUNCTIONS" & ~isempty(stripblanks(in)) then
      helptxt = [helptxt;"   <para>" + in + "</para>"];
    end
  end
  line = mgetl(f,1);
  line = replaceTabBySpace(line);
  i = strindex(line, "//");
end

helptxt = [helptxt; change_activity(doing, "FINISHED")];
mclose(f);

if ~isempty(helpdir) then
  fnme = pathconvert(helpdir, %t, %f) + out + ".xml";
  answ = 1;
  if isfile(fnme) then  // file exists...
    answ = messagebox(fnme + " exists!", "Warning - help_from_sci", "warning", ["Create anyway" "Skip file"], "modal");
  end
  if answ == 1 then
    mputl(helptxt, fnme);
    helptxt = fnme;
  else
    printf(gettext("%s: File skipped %s."), "help_from_sci", out + ".xml");
    helptxt = "";
  end
end


endfunction
//==============================================================================
function [head, tail] = chop(str, tok)
  i = regexp(str, "/" + tok + "/", "o");
  if isempty(i) then
    head = str;
    tail = [];
  else
    head = part(str, 1:i - 1);
    tail = part(str, i + 1:length(str));
  end
endfunction
//==============================================================================
function strOut = replaceTabBySpace(strIn)
  strOut = strsubst(strIn, ascii(9), part(" ",1:4));
endfunction
//==============================================================================
function [txt, doing] = change_activity(currently_doing, start_doing)
  doing = start_doing;
  select convstr(currently_doing,"u")
    case "CALLING SEQUENCE" then
      txt = ["   </synopsis>"; "</refsynopsisdiv>"];
    case "PARAMETERS" then
      txt = ["   </variablelist>"; "</refsection>"];
    case "DESCRIPTION" then
      txt = ["</refsection>"];
    case "EXAMPLES" then
      txt = ["   ]]></programlisting>"; "</refsection>"];
    case "SEE ALSO" then
      txt = ["   </simplelist>"; "</refsection>"];
    case "AUTHORS" then
      txt = ["     </listitem></itemizedlist>"; "</refsection>"];
    case "BIBLIOGRAPHY" then
      txt = ["     </itemizedlist>"; "</refsection>"];
    case "USED FUNCTIONS" then
      txt = ["</refsection>"];
    else
      txt = "";
  end

  select convstr(start_doing, "u"),
      case "CALLING SEQUENCE"
      txt = [txt; ""; "<refsynopsisdiv>"; "   <title>Calling Sequence</title>"; "   <synopsis>"];
    case "PARAMETERS"
      txt = [txt; ""; "<refsection>"; "   <title>Parameters</title>"; "   <variablelist>"];
    case "DESCRIPTION"
      txt = [txt; ""; "<refsection>"; "   <title>Description</title>"];
    case "EXAMPLES"
      txt = [txt; ""; "<refsection>"; "   <title>Examples</title>"; "   <programlisting role=""example""><![CDATA["];
    case "SEE ALSO"
      txt = [txt; ""; "<refsection>"; "   <title>See also</title>"; "   <simplelist type=""inline"">"];
    case "AUTHORS"
      txt = [txt; ""; "<refsection>"; "   <title>Authors</title>"; "   <itemizedlist><listitem>"];
    case "BIBLIOGRAPHY"
      txt = [txt; ""; "<refsection>"; "   <title>Bibliography</title>" ; "   <itemizedlist>"];
    case "USED FUNCTIONS"
      txt = [txt; ""; "<refsection>"; "   <title>Used functions</title>"];
    case "FINISHED"
      txt = [txt; "</refentry>"];
  end
endfunction
//==============================================================================

function gen_doc_xml(dirpath,prefix)
  // Script to generate XML files from all main .sci files in directory macros 
  if (~exists('dirpath','local'))  
    path_tmp  = get_function_path('gen_doc_xml');
    dirpath = strncpy( path_tmp, length(path_tmp)-length('\macros\misc\gen_doc_xml.sci') );  
  end
  
  if (~exists('prefix','local')); prefix = "CL"; end
  
  mprintf("Generation des fichiers XML a partir des fichiers .sci\n");

  // First step : delete all old XML files
  // NB: delete only XML files that start with "prefix_"
  dirhelp = fullfile(dirpath,"help");
  xml_files = CL__findfiles(dirhelp,prefix+"_*.xml");
  for xml_file = xml_files
    mdelete(xml_file);
  end
  
  // sci_files = sci files that start with prefix_ and not with prefix__
  dirmacros = fullfile(dirpath,"macros");
  sci_files = CL__findfiles(dirmacros,prefix+"_*.sci");
  files_not_wanted = CL__findfiles(dirmacros,prefix+"__*.sci");
  sci_files = setdiff(sci_files,files_not_wanted);
  
  // For each sci file, replace "macros" with "help/en_US" and build the xml there.
  // additionnaly, if the help directory doesnt exist : it is created and a file CHAPTER is created there too
  for sci_fic = sci_files
    [path_sci,filename] = fileparts(sci_fic);
    path_xml = strsubst(path_sci,"macros","help"+filesep()+"en_US");
    
    // If help directory doesnt exist : create it
    if( ~ isdir(path_xml) )
      mprintf("Creation of directory : %s\n", path_xml)
      mkdir(path_xml);
    end
    
    // If CHAPTER file doesnt exist : create it
    if ( ~ isfile(fullfile(path_xml , "CHAPTER")) )
      mprintf("Creation of file : %s (contains the name of help chapter)\n", fullfile(path_xml , "CHAPTER"));
      mputl("title = " + basename(fullfile(path_sci)) , fullfile(path_xml , "CHAPTER"));
    end
    
    // Delete old xml file
    mdelete( fullfile(path_xml , filename + ".xml") );
    
    // Create new xml file
    help_from_sci2(fullfile(path_sci,filename),path_xml);
  end

endfunction

