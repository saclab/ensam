//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Fpl, Fpldot] = CL__iers_fundArgsPL(jd, cder)
// Compute the fundamental arguments for planetary nutation (IERS2010).
//
// Calling Sequence
// [Fpl, Fpldot] = CL__iers_fundArgsPL(jd [,cder])
//
// Description
// <itemizedlist><listitem>
// Compute the fundamental arguments of planetary nutation <b>Fpl</b> and their time derivatives :
// Mean longitude of Mercury, Venus, Earth, Mars, Jupiter, Saturn, Uranus, Neptune + 
// General accumulated precession in longitude
// 
// Note : used in nutation and series (X,Y,s) computations
//
// </listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// cder: (boolean, optional) Option to compute derivative. If cder is %f, the derivative will be set to []. Default is %t. (1x1)
// Fpl: Arguments of the planetary nutation [rad] (9xN)
// Fpldot: Derivatives of arguments of the planetary nutation [rad/s] (9xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010

// Declarations:


// Code:

if (~exists("cder","local")); cder = %t; end;
if (argn(1) <= 1); cder = %f; end;

// Julian centuries since J2000.0
t = ((jd(1,:) - 2451545) + jd(2,:)) / 36525;

N = length(t);

// ---------------------------------------------
// Fundamental arguments for the planetary nutation (eq 5.44, p68, paragraph 5.7.3)
// ---------------------------------------------
Fpl = zeros(9,N);

// Mean longitude of Mercury
Fpl(1,:) = 4.402608842 + 2608.7903141574 * t; // radians

// Mean longitude of Venus
Fpl(2,:) = 3.176146697 + 1021.3285546211 * t;

// Mean longitude of Earth
Fpl(3,:) = 1.753470314 + 628.3075849991 * t;

// Mean longitude of Mars
Fpl(4,:) = 6.203480913 + 334.0612426700 * t; 

// Mean longitude of Jupiter
Fpl(5,:) = 0.599546497 + 52.9690962641 * t;

// Mean longitude of Saturn
Fpl(6,:) = 0.874016757 + 21.3299104960 * t; 

// Mean longitude of Uranus
Fpl(7,:) = 5.481293872 + 7.4781598567 * t;

// Mean longitude of Neptune
Fpl(8,:) = 5.311886287 + 3.8133035638 * t;

// General accumulated precession in longitude
Fpl(9,:) = (0.02438175 + 0.00000538691 * t) .* t;

Fpl = CL_rMod(Fpl, 2*%pi);


// ------------------
// Time derivatives
// ------------------
Fpldot = [];
if (cder)

  Fpldot = zeros(9,N);
  
  Fpldot(1,:) = 2608.7903141574 ; // rad/century
  Fpldot(2,:) = 1021.3285546211 ;
  Fpldot(3,:) = 628.3075849991 ;
  Fpldot(4,:) = 334.0612426700 ; 
  Fpldot(5,:) = 52.9690962641 ;
  Fpldot(6,:) = 21.3299104960 ; 
  Fpldot(7,:) = 7.4781598567 ;
  Fpldot(8,:) = 3.8133035638 ;
  Fpldot(9,:) = 0.02438175 + 0.00000538691 * (2*t);
  
  // Convert from rad/century to rad/s
  Fpldot = Fpldot / (36525 * 86400);
end

endfunction
