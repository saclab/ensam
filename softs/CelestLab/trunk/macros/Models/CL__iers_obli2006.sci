//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [epsa, epsadot] = CL__iers_obli2006(jd, cder)
// Mean obliquity of the ecliptic (IAU 2006 model)
//
// Calling Sequence
// [epsa, epsadot] = CL__iers_obli2006(jd [,cder])
//
// Description
// <itemizedlist><listitem>
// <p>Compute the mean obliquity of the ecliptic (IAU 2006 model).</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// cder: (boolean, optional) Option to compute derivative. If cder is %f, the derivative will be set to []. Default is %t. (1x1)
// epsa: Mean obliquity of the ecliptic [rad] (1xN)
// epsadot: Time derivatives of mean obliquity of the ecliptic [rad/s] (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010


// Declarations:


// Code:

if (~exists("cder","local")); cder = %t; end;
if (argn(1) <= 1); cder = %f; end;

ARCSEC_TO_RAD = %pi / (180*3600);

// Julian centuries since J2000.0
t = ((jd(1,:) - 2451545) + jd(2,:)) / 36525;

        
// eq 5.40, p65, paragraph 5.6.4 (with eps0 found in table 1.1, p18, paragraph 1.2)
epsa = (84381.406 + (-46.836769 + ( -0.0001831 + (0.00200340 + (-0.000000576 + ...
       (-0.0000000434) .* t) .* t) .* t) .* t) .* t) * ARCSEC_TO_RAD;

       
// ---------------
// Time derivative   
// ---------------
epsadot = [];   
if (cder)
  epsadot = (-46.836769 + (2*-0.0001831 + (3* 0.00200340  + (4*-0.000000576 + ...
            (5*-0.0000000434) .* t) .* t) .* t) .* t) * ARCSEC_TO_RAD / (36525*86400);
end 
 
endfunction
