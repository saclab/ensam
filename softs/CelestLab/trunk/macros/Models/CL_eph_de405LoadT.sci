//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [ephem] = CL_eph_de405LoadT(cjd, tt_tref)
// Loads DE405 ephemeris files (for a time span)
//
// Calling Sequence
// ephem = CL_eph_de405LoadT(cjd [, tt_tref])
//
// Description
// <itemizedlist><listitem>
// <p>Loads DE405 ephemeris files for a given time span.</p>
// <p>The time span is defined by the interval containing all the dates in <b>cjd</b>.</p>
// <p>The result is returned in a Scilab stucture <b>ephem</b> that can be used by the function
// <link linkend="CL_eph_de405">CL_eph_de405</link>.</p>
// <p></p></listitem>
// <listitem>
// <p><b>Note</b>: </p>
// <p>The format of the ephemeris file is specific to CelestLab. </p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// cjd: Modified (1950.0) julian days (Time scale: TREF) (1xN)
// tt_tref: (optional) TT-TREF [seconds]. Default is %CL_TT_TREF. (1xN or 1x1)
// ephem: Scilab structure containing the ephemeris data
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_eph_de405
// CL_eph_de405Load
//
// Examples
// cjd_min = CL_dat_cal2cjd(1980, 1, 1); // TREF
// cjd_max = CL_dat_cal2cjd(2020, 1, 1); // TREF
// ephem = CL_eph_de405LoadT([cjd_min, cjd_max]);
// 
// // Use it! 
// cjd = linspace(cjd_min, cjd_max, 5);
// [pos, vel, acc] = CL_eph_de405("Mars", cjd, "Earth", ephem=ephem); 
// [pos, vel, acc] = CL_eph_de405("Mars", cjd, "Earth"); // => same, but slower

// Internal function to load the descriptor of ephemeris files (located in data folder)
function [desc] = de405LoadT_loadDesc(fpath)
    if (~isfile(fpath))
      CL__error("File " + fpath + " not found");
    end
    load(fpath, "desc");
endfunction

// Arguments checking
if (~exists("tt_tref", "local")); tt_tref = CL__dataGetEnv("TT_TREF"); end

CL__checkInputs(cjd,1, tt_tref,1);

// Convert from TREF to TDB
// (Actually to TT: small approximation)
cjd = cjd + tt_tref/86400;

// load descriptor
fpath = fullfile(CL_home(), "data", "ephem", "de405", "desc.dat");
desc = de405LoadT_loadDesc(fpath); 

cjd_min = min(cjd); 
cjd_max = max(cjd); 

// k1:k2 = indices of files that contain data for the interval: [cjd_min, cjd_max]
k1 = find(cjd_min >= desc.cjd_begin & cjd_min < desc.cjd_end); 
if (k1 == []); k1 = 1; end

k2 = find(cjd_max > desc.cjd_begin & cjd_max <= desc.cjd_end); 
if (k2 == []); k2 = length(desc.cjd_begin); end

if (k1 == k2)
  // simple ephemeris structure (type 1)
  ephem = CL_eph_de405Load(desc.fname(k1), %t);
  ephem.ephem_type = 1; 
  
else
  // multiple ephemeris structure (type 2)
  ephem = struct(); 
  
  ephem.desc = desc; 
  for name = matrix(fieldnames(desc), 1, -1)
    ephem.desc(name) = ephem.desc(name)(k1:k2); 
  end

  // include ephemeris structures: 
  ephem.eph = list(); 
  for k = k1 : k2
    ephem.eph($+1) = []; // because of Scilab bug in previous versions
    ephem.eph($) = CL_eph_de405Load(desc.fname(k), %t);
  end
  
  ephem.ephem_type = 2; 

end

endfunction
