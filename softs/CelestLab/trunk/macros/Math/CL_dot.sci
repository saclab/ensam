//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [d] = CL_dot(u,v)
// Dot product of column vectors
//
// Calling Sequence
// d = CL_dot(u,v)
// d = CL_dot(u)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the dot product of column vectors (or matrices or hypermatrices considered as sets
// of column vectors). </p>
// <p></p>
// <p>The matrices or hypermatrices are automatically expanded and resized: the dimensions
// with size 1 are expanded to the maximum size between the 2 matrices (or hypermatrices). 
// However, the sizes for the 1st dimension (number of rows) must be identical unless one of the 2 arguments
// is the empty matrix. </p> 
// <p></p>
// <p>By convention: CL_dot(u) = CL_dot(u,u). </p>
// <p></p>
// </listitem>
// <listitem>
// <p><b>Notes</b>: </p>
// <p>- If one of the arguments is [], the result is also []. </p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// u: Matrix or hypermatrix (NxPx... or Nx1x... etc...)
// v: (optional) Matrix or hypermatrix (NxPx... or Nx1x... etc...)
// d: Dot product of u and v (1xPx...)
//
// See also
// CL_dMult
// CL_norm
//
// Authors
// CNES - DCT/SB
//
// Examples
// // u and v: matrices
// u = [[1; 2], [3; 4]];
// v = [[-1; 5], [-6; 2]];
// CL_dot(u,v)
//
// // u: column vector and v: matrix 
// u = [1; 2; 3];
// v = [[1; 2; 3], [3; 4; 5]];
// CL_dot(u,v)
// 
// // u: column vector and v: hypermatrix 
// u = [1; 2; 3];
// v = ones(3, 2, 4);
// CL_dot(u,v)
// 
// // u: matrix and v: hypermatrix 
// u = [[1; 2; 3], [3; 4; 5]];
// v = ones(3, 2, 4);
// CL_dot(u,v)

// Declarations:


// Code:

rhs = argn(2); // number of input arguments

if (rhs == 0)
  CL__error("Wrong number of input arguments");
end

if (rhs == 1) 
  // only one input argument (u) 
  v = u; 
end

// check number of rows
// same number of rows expected (or one is 0)
if (size(u,1) <> size(v,1) & min(size(u)) <> 0 & min(size(v)) <> 0) 
  CL__error("Inconsistent sizes (number of rows)");
end

// size adjustment + dot product 
[u, v] = CL__adjustSizes(u, v); 

if (u == [] | v == [])
  d = []; 
else
  d = sum(u .* v, 1); 
end

endfunction


