//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [v] = CL__axisPerp(u)
// finds an axis perpendicular to u 
// returns [] if u is []
  v = [];
  if (u == []); return; end 
  x = CL_cross(u,[1;0;0]); 
  y = CL_cross(u,[0;1;0]); 
  v = x; 
  I = find(CL_dot(y) > CL_dot(x)); 
  v(:,I) = y(:,I);
endfunction
