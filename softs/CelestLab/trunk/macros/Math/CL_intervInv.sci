//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [ires] = CL_intervInv(i1,i2)
// Inversion of a a sets of intervals (complementary intervals)
//
// Calling Sequence
// ires = CL_intervInv(i1,i2)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the complementary set of intervals in an interval</p>
// <p>The intervals are gathered in a set (i2). </p>
// <p>A real number x belongs to the complementary of i2 (inside i1) if it belongs to i1, and does not belong to any interval from i2.</p>
// <inlinemediaobject><imageobject><imagedata fileref="invInterval.gif"/></imageobject></inlinemediaobject>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> - The intervals in i2 should have empty intersections (use CL_intervUnion if needed). </p>
// <p> - Sets of intervals may be empty. CL_intervInv(i1,[]) = i1. CL_intervInv([],i2) = []. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// i1: Interval [start ; end] (2x1)
// i2: Set of intervals [start ; end] (2xN)
// ires: Complementary intervals of i2 inside i1 (2xM)
//
// See also
// CL_intervInters
// CL_intervUnion
// CL_intervDiff
//
// Authors
// CNES - DCT/SB
//
// Examples
// i1=[ 0 ; 20 ];
// i2=[ [-4;0.5] , [2;4] , [5.5;5.7] , [6.6;15]];
// ires = CL_intervInv(i1,i2);

// Code:

if (i1 <> [])
  if (~isequal(size(i1), [2,1]))
    CL__error("Interval i1 must be of size (2x1)");
  end
else
  ires = [];
  return;
end

if (i2 <> [])
  if (size(i2,1) <> 2)
    CL__error("Interval i2 must be of size (2xN)");
  end
else
  ires = i1;
  return;
end

// Remove intervals strictly outside i1
I = find(i2(2,:) < i1(1) | i2(1,:) > i1(2));
i2(:,I) = [];

// Sort i2 intervals by starting date
i2 = CL_sortMat(i2, i2(1,:));

// Complementary ("inversed") intervals :
ires = [ [i1(1) ; i2(1,1)], [i2(2,1:$-1) ; i2(1,2:$)], [i2(2,$) ; i1(2)] ] ;

// Remove intervals of length <= 0
I = find(ires(2,:) - ires(1,:) <= 0);
ires(:,I) = [];


endfunction
