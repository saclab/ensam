//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mjd] = CL_dat_cjd2mjd(cjd)
// Modified (1950.0) Julian day to modified Julian day - DEPRECATED
//
// Calling Sequence
// mjd = CL_dat_cjd2mjd(cjd)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_dat_convert">CL_dat_convert</link></p>
// <p></p></listitem>
// <listitem>
// <p>Converts a modified (1950.0) Julian day (<b>cjd</b>: number of days elapsed since 1950.0) 
// into a modified Julian day (<b>mjd</b>: Julian day - 2400000.5)</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Dates and time scales">Dates and time scales</link> for more details.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd: (real) Modified (1950.0) Julian day (1xN)
// mjd: (real) Modified Julian day (1xN)
//
// See also
// CL_dat_mjd2cjd
// CL_dat_cjd2cal
// CL_dat_cjd2jd
//
// Authors
// CNES - DCT/SB
//
// Examples
// cjd=[20000.256,20001.897];
// mjd = CL_dat_cjd2mjd(cjd);
// cjd2 = CL_dat_mjd2cjd(mjd);

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

[lhs,rhs]=argn();
if rhs~=1 then CL__error("Wrong number of input arguments"), end

mjd = cjd - (2400000.5 - 2451545) - 18262.5;

endfunction
