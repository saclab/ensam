//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [gmst,gmstp] = CL_mod_sidTime(cjd_ut1)
// Greenwich mean sidereal time (IERS 1996) - DEPRECATED
//
// Calling Sequence
// [gmst,gmstp] = CL_mod_sidTime(cjd_ut1)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_mod_siderealTime">CL_mod_siderealTime</link></p>
// <p></p></listitem>
// <listitem>
// <p>Computes the Greenwich mean sidereal time <b>gmst</b> (IERS standard 1996).</p>
// <p>It also optionally computes the first derivative of the sidereal time : <b>gmstp</b>.
// (this value is not a constant)</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cjd_ut1: Modified (1950.0) Julian day, in UT1 time scale (1xN)
// gmst : Greenwich mean sidereal time [rad]
// gmstp : Greenwich mean sidereal time first derivative [rad/s]
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) IERS Conventions (1996), Dennis D. McCarthy
//
// See also
// CL_mod_sidTimeG50
//
// Examples
// cjd_utc = CL_dat_cal2cjd(2008,10,25,15,30,25)  // 25 oct 2008 at 15h30min25s
// sec_ut1_minus_utc = 0.2;
// cjd_ut1 = cjd_utc + sec_ut1_minus_utc/86400.;  // ut1 time
// gmst = CL_mod_sidTime(cjd_ut1);
// [gmst,gmstp] = CL_mod_sidTime(cjd_ut1);
//
// // With first time derivative :
// [gmst,gmstp] = CL_mod_sidTime(21915.121212)
//

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

[lhs rhs]=argn(0);
compute_GMSTP=%f;
if rhs~=1 CL__error("check number of input arguments"); end;
if lhs==2 compute_GMSTP=%t; end;

sec_ut1 = ( cjd_ut1-int(cjd_ut1) )*24*3600;
cjd_ut1 = floor(cjd_ut1);

A0 = 24110.54841;
A1 = 8640184.812866;
A2 = 0.093104;
A3 = -6.2d-6;

J2000 = 36525. / 2.; // 1er janv 2000 12h.
TU = (cjd_ut1 + sec_ut1/86400 - J2000) / 36525;


// greenwich mean sidereal time of 0h UT1
//gmst = A0 + A1*TU + A2*TU.^2 + A3*TU.^3;
gmst = ((A3*TU + A2).*TU + A1).*TU + A0;
// on rajoute les secondes UT1
//gmst=modulo(gmst,86400) + modulo(sec_ut1);
gmst = gmst + sec_ut1;
gmst = modulo(gmst,86400)*2*%pi/86400;

if compute_GMSTP
  // gmstp = A1 +2*A2*TU  + 3*A3*TU^2
  gmstp = (A1 + TU.* (2*A2  + 3*A3*TU)) /36525 *2*%pi/86400; // rad/jour
  gmstp = (2*%pi + gmstp) / 86400; // rad/sec
end



endfunction
