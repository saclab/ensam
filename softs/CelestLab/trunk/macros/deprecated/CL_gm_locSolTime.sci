//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function locSolTime = CL_gm_locSolTime(pos,sun_dir)
// Local solar time - DEPRECATED
//
// Calling Sequence
// locSolTime = CL_gm_locSolTime(pos,sun_dir)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p></p></listitem>
// <listitem>
// <p>Computes the local solar time (<b>locSolTime</b>) 
// at any location, knowing the position (<b>pos</b>) and the Sun
// direction (<b>sun_dir</b>), both supposed defined in the 
// same reference frame.</p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> - A local time equal to 12h means a right ascension equal to the Sun's right ascension. </p>
// <p> - "Local time" here means "true local time". </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// pos: Position vector [m] (3xN)
// sun_dir: Sun direction vector in the same frame as pos (3xN)
// locSolTime: Local solar time, angle in ]-pi, pi] (0 rad for 12h(noon))  [rad] (1xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// cjd = CL_dat_cal2cjd(2008,10,25);
// // sat position in G50 :
// pos = [ 7378.e3 ; 0 ; 0];
// // Sun position in G50 :             
// [sun_dir,rs] = CL_mod_moonSunG50(cjd,'s');
//
// // True local solar time (angle)
// locSolTime = CL_gm_locSolTime(pos,sun_dir)
//
// // True local solar time (hours)   
// locSolTime_hours = (locSolTime+%pi)*24/(2*%pi)   

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

locSolTime = atan(pos(2,:),pos(1,:)) - atan(sun_dir(2,:),sun_dir(1,:));

i1 = locSolTime <= -%pi
i2 = locSolTime > %pi
locSolTime(i1) = locSolTime(i1) + 2*%pi
locSolTime(i2) = locSolTime(i2) - 2*%pi

endfunction
