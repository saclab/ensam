//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Y] = CL__3b_integratorFix(Y0, t0, t, f, MU)
// Author:
// B. Meyssignac (CNES DCT/SB/MO)
// AL: adaptations (simpler)
//
// Integration of motion (imposed output times)
// Inputs: 
// Y0: initial position and velocity (6x1)
// t0: initial time
// t: output times (1xN)
// f: function to integrate: Ydot = f(t,Y,MU). 
// Outputs: 
// Y: trajectory (position and velocity) (6xN)


// Code:

// Note: %ODEOPTIONS(1) == 1 
// => generate results at wanted times
%ODEOPTIONS = [1,0,0,%inf,0,2,500000,12,5,0,-1,-1];

rtol = 1.e-8 * ones(Y0);
atol = 1.e-9 * ones(Y0);

Y = ode(Y0,t0,t,rtol,atol,list(f, MU));

endfunction
