//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [X,M] = CL__3b_monodromy(X0,tint,MU)
// Computes the monodromy matrix at tint from X0

// Inputs: 
// X0: initial point (columns of 6 dimension (position+velocity))
//     (initial time is 0)
// tint: Integration time (1x1)
// MU: mass ratio (see env structure)
// Outputs:  
// X: position and velocity at tint (6x1) 
// M: Monodromy matrix (6x6)
//
// Author:
// R. Alacevich (Stage CNES DCT/SB/MO)
// B. Meyssignac (CNES DCT/SB/MO)


// Initialisation

// Declarations:


// Code:

Id = eye(6,6);
Z0 = [X0; matrix(Id,36,1)];

// Integration 
Z = CL__3b_integratorFix(Z0, 0, tint, CL__3b_RHSDP, MU);

X = Z(1:6);
M = matrix(Z(7:$), 6, 6);

endfunction
