//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Y,t] = CL__3b_integratorVar(Y0,t0,dint,f,MU)
// Author:
// B. Meyssignac (CNES DCT/SB/MO)
// AL: adaptations (simpler)
//
// Integration of motion (variable number of output times)
// Inputs: 
// Y0: initial position and velocity (6x1)
// t0: initial time
// dint: max integration time relative to t0 (can be >= 0 or <= 0)
// f: function to integrate: Ydot = f(t,Y,MU). 
// Outputs: 
// Y: trajectory (position and velocity) (6xN)
// t: associated times (1xN)


// Code:

// Note: %ODEOPTIONS(1) == 2 
// => ode generates integration output times
%ODEOPTIONS = [2,0,0,%inf,0,2,500000,12,5,0,-1,-1];

rtol = 1.e-8 * ones(Y0);
atol = 1.e-9 * ones(Y0);

Yt = ode(Y0, t0, t0+dint, rtol, atol, list(f, MU));
Y = Yt(2:$,:);
t = Yt(1,:);

endfunction
