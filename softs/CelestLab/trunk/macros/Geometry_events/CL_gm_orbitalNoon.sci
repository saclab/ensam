//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pso_orb_noon] = CL_gm_orbitalNoon(inc,raan,alpha_sun,delta_sun)
// Argument of latitude at the position of orbital noon 
//
// Calling Sequence
// pso_orb_noon = CL_gm_orbitalNoon(inc,raan,alpha_sun,delta_sun)
//
// Description
// <itemizedlist><listitem>
// <p>Orbital noon refers to the position in the orbit where the angle between the satellite radius vector 
// and the Sun direction is minimal. </p>
// <p>In other words, it is the orbital position where the satellite radius vector crosses 
// the projection of the Sun onto the orbit plane.</p> 
// <p></p>
// <p> This function computes the argument of latitude at the position of orbital noon 
// (<b>pso_orb_noon</b>) as a function of the orbit's right ascension 
// of ascending node (<b>raan</b>) and inclination 
// (<b>inc</b>), 
// and the Sun's right ascension (<b>alpha_sun</b>) and declination 
// (<b>delta_sun</b>). </p>
// <inlinemediaobject><imageobject><imagedata fileref="orbitalNoon.gif"/></imageobject></inlinemediaobject>
// </listitem>
// </itemizedlist>
//
// Parameters
// inc: Orbit inclination [rad] (1xN)
// raan: Orbit right ascension of ascending node [rad] (1xN)
// alpha_sun: Sun right ascension [rad] (1xN)
// delta_sun: Sun declination [rad] (1xN)
// pso_orb_noon: Argument of latitude(w+M) at the position of orbital noon [rad] (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_gm_raan2beta
// CL_gm_beta2raan
// CL_gm_eclipseCir
//
// Examples
// cjd = 21482;
// raan = 0;
// sma = 7300.e3;
// inc = CL_deg2rad(98);
// pos_sun = CL_eph_sun(cjd);
// pos_sun_sph = CL_co_car2sph(pos_sun);
// alpha_sun = pos_sun_sph(1);
// delta_sun = pos_sun_sph(2);
// pso_orb_noon = CL_gm_orbitalNoon(inc,raan,alpha_sun,delta_sun)
//
// // Comparison with CL_gm_eclipseCir :
// [result] = CL_gm_eclipseCir(sma,inc,raan,alpha_sun,delta_sun)
// pso_midnight = (result.start.pso + result.end.pso ) / 2 
// pso_noon = pso_midnight + %pi


// Declarations:


// Code:

[alpha2,delta2] = CL_gm_inertial2orbitSph(inc,raan,alpha_sun,delta_sun);
pso_orb_noon = alpha2;

endfunction
