//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [betaa] = CL_gm_raan2beta(inc,raan,alpha_sun,delta_sun)
// Right ascension of ascending node to beta angle
//
// Calling Sequence
// betaa = CL_gm_raan2beta(inc,raan,alpha_sun,delta_sun)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the beta angle (angle between the orbit plane and the Sun direction) form 
// the right ascension of the ascending node. </p>
// <p></p>
// <p> <inlinemediaobject><imageobject><imagedata fileref="beta.gif"/></imageobject></inlinemediaobject></p>
// <p></p></listitem>
// <listitem>
// <p><b>Note:</b></p>
// <p>- The beta angle is a signed quantity. If it is positive then the Sun direction is less
// than 90 degrees from the orbit's angular momentum vector (vector perpendicular to the
// orbit plane and oriented according to right hand rule). </p>
// <p>- Be careful about the order of arguments: inclination, raan... </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// inc: Orbit inclination [rad] (1xN)
// raan: Orbit right ascension of ascending node [rad] (1xN)
// alpha_sun: Sun right ascension [rad] (1xN)
// delta_sun: Sun declination [rad] (1xN)
// betaa: Beta angle [rad] (1xN)
//
// See also
// CL_gm_beta2raan
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Get alpha_sun and delta_sun
// cjd = 20050;
// pos_sun = CL_eph_sun(cjd);
// pos_sun_sph = CL_co_car2sph(pos_sun);
// alpha_sun = pos_sun_sph(1);
// delta_sun = pos_sun_sph(2);
// raan = %pi/4;
// inc = CL_deg2rad(98.7);
// betaa = CL_gm_raan2beta(inc,raan,alpha_sun,delta_sun)

// Declarations:


// Code:

// check inputs 
I = find(inc < 0 | inc > %pi + 2*%eps)
if (I <> [])
  CL__error("Invalid inclination"); 
end

[inc,raan,alpha_sun,delta_sun] = CL__checkInputs(inc,1,raan,1,alpha_sun,1,delta_sun,1); 

sin_betaa = cos(delta_sun).*sin(inc).*sin(raan-alpha_sun) + sin(delta_sun).*cos(inc); 

betaa = real(asin(sin_betaa));

I = find(abs(sin_betaa) > 1); 
betaa(I) = %nan; 


endfunction
