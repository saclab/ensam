//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [area] = CL_stela_area(desc, typ, varargin)
// Computes the area projected perpendicularly to a given direction
//
// Calling Sequence
// CL_stela_area(desc, "tumb", nbp [, opts])
// CL_stela_area(desc, "spin", direction, axis, nbp [, opts])
// CL_stela_area(desc, "fix", direction [, opts])
// CL_stela_area(desc, "fixr", direction, rotdesc [, opts])
//
// Description
// <p>CL_stela_area computes the projected area of an object perpendicularly to a given direction.</p>
// <p>Three modes are possible: "tumb" (tumbling mode), "spin" (spinning mode) and "fix" (fixed direction).
// In "fixed" mode, additional rotations of one or more sub-parts can be defined via the rotdesc input parameter</p>
// <p></p>
//
// <p> The <b>rotdesc</b> input parameter allowing the definition of rotations of 
// sub-parts rotations is a list of structures, each structure containing the following fields:</p>
// <p>- <b>id</b>: (string) Name of sub-part.</p>
// <p>- <b>axis</b>: Rotation axis in object's frame. (3xN or 3x1)</p>
// <p>- <b>ang</b>: Rotation angle (rad). (1xN or 3x1)</p>
// <p>- <b>rcen</b>: (optional) Rotation center in sub-part's frame. (origin of frame = reference position of sub-part). Default = [0;0;0]. (3xN or 3x1)</p>
//
// <p>The <b>opt</b> structure contains the (optional) following fields:</p> 
// <p>- <b>resol</b>: (integer, optional) Resolution (total number of "pixels"). Default is 10000. (1x1)</p>
// <p>- <b>savePrefix</b>: (string, optional) Begining of path of saved XML files. Default is "" (no file saved). (1x1)</p>
//
// Parameters
// desc: (string or struct) Path of XML file or object description in Scilab form.
// direction: Projection direction in object frame. (3xN or 3x1)
// nbp: (integer) Number of projection planes for tumbling or spinning modes. (1x1)
// axis: Rotation axis (for spinning mode). (3xN or 3x1)
// rotdesc: (list) List of structures describing rotations of sub-parts.
// opts: (struct) Option structure.
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_stela_objLoad
//
// Examples
// // XML description
// desc = fullfile(CLx_setting_get("STELA_ROOT"), "examples/example_shap.xml");
// opts = struct("resol", 10000); 
//
// // Example 1 (no rotations of sub-parts)
// alpha = linspace(0, 2*%pi, 10); 
// direction = [cos(alpha); zeros(alpha); sin(alpha)]; 
// area = CL_stela_area(desc, "fix", direction, opts); 
//
// // Example 2 (with rotations of sub-parts)
// alpha = linspace(0, 2*%pi, 10); 
// direction = [0;0;1]; 
// rotdesc = list(struct("id", "GS +Y1", "axis", [0;1;0], "ang", alpha)); 
// area = CL_stela_area(desc, "fixr", direction, rotdesc, opts);
//
// // Example 3 (argument = object structure)
// alpha = linspace(0, 2*%pi, 10); 
// direction = [cos(alpha); zeros(alpha); sin(alpha)]; 
// obj = CL_stela_objLoad(desc); 
// area = CL_stela_area(obj, "fix", direction, opts); 


  // check extension module
  CL__checkExtensionModule();

  // call CLx function (which does all the checks)
  area = CLx_stela_area(desc, typ, varargin);
  
endfunction
