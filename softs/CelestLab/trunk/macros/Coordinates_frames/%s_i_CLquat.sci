//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q(I) = [] 
// I = integers (row vector) or ":" or 1:$ or $ or etc ...

function [q] = %s_i_CLquat(I,a,q)

if (a <> [])
  CL__error("Invalid rhs (should be [])");
end

if (typeof(I) == "implicitlist" | typeof(I) == "size implicit" | typeof(I) == "polynomial" | isequal(I,:))
  N = size(q(I));
else
  if (I <> [] & (size(I,1) > 1 | min(I) <= 0.5 | max(I) >= size(q)+0.5))
    CL__error("Invalid index"); 
  end
  N = size(I, 2); 
end

if (N == 0 | size(q) == 0)
  return; // no change
end

r = q.r;
i = q.i;
r(I) = [];
i(:,I) = [];
q = CL__defQuat(r, i);
  
endfunction
