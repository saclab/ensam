//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// eq = (q1 == q2)

function [eq] = %CLquat_o_CLquat(q1,q2)

if (size(q1) <> size(q2))
  eq = %f; 

else
  cond_neq = ( q1.r <> q2.r | ..
             q1.i(1,:) <> q2.i(1,:) | ..
             q1.i(2,:) <> q2.i(2,:) | ..
             q1.i(3,:) <> q2.i(3,:) ); 

  if (find(cond_neq) <> [])
    eq = %f; 
  else
    eq = %t; 
  end

end

endfunction
