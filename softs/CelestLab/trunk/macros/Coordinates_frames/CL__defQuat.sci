//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [q] = CL__defQuat(r,i)
// Quaternion definition from real and imaginary components
//
// Calling Sequence
// q = CL__defQuat(r,i)
//
// Description
// <itemizedlist>
// <listitem>
// Returns a quaternion defined by its real or imaginary components. 
// </listitem>
// <listitem>
// Notes: 
// <para> - The quaternion as defined is NOT normalized. </para>
// <para> - CL__defQuat([],[]) => [] </para>
// <para> - It's the only function that defines a quaternion structure (mlist) </para>
// </listitem>
// </itemizedlist>
//
// Parameters
// r: Real part (1xN) 
// i: Imaginary part (3xN)
// q : Quaternion (dim N)
//
// Authors
// CNES - DCT/SB
//

sr = size(r);
si = size(i); 

if ~( (sr(1) == 0 & si(1) == 0) | ..
      (sr(1) == 1 & si(1) == 3 & sr(2) == si(2)) ) 
   CL__error("Invalid arguments sizes"); 
end

q = mlist(['CLquat','r','i'], r, i);

endfunction
