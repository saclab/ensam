//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M, omega] = CL__fr_cirs2veis(jd, args, comega)
// CIRS to Gamma50 (Veis) frame transformation matrix and angular velocity
//
// Calling Sequence
// [M, omega] = CL__fr_cirs2veis(jd [,args, comega])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>M</b> from CIRS to 
// Gamma50 (Veis) reference frame.</p>
// <p> By convention, <b>M</b> multiplied by coordinates relative to CIRS yields coordinates relative to Gamma50 Veis frame. </p>
// <p>Optionaly computes the angular velocity vector <b>omega</b> of Veis relative to CIRS, with coordinates relative to CIRS.
// See <link linkend="Data types">Data types</link> for more details on the definition of angular velocity vectors and frame transformation matrix.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// <p>Note : The transformation is a single rotation around Z axis of (era+sprime)-tsid_veis</p>
// <p>sprime should be computed with a TT time scale and era and tsid_veis with a UT1 time scale 
// but in practice, using TT for both leads to a 0.0002 microarseconds error.</p>
// <p>Note : args appears in the calling sequence but is not used.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// args: Not used.
// comega: (boolean, optional) Option to compute omega. If comega is %f, omega will be set to []. Default is %t. (1x1)
// M: transformation matrix (3x3xN)
// omega: (optional) Angular velocity vector [rad/s] (3xN)
//
// Authors
// CNES - DCT/SB

// Declarations:


// Code:

if (~exists("comega","local")); comega = %t; end;
if (argn(1) <= 1); comega = %f; end;

// Note :
// Rigorously, the date used for the sidereal times (ERA and Veis) should be UT1 dates.
// In practice, tsid - (era+sprime) = -2306.400390''-0.011473''t , with t = centuries (TT) from J2000
// Using TT instead of UT1, the error is about 60 seconds on the date, which gives :
// 60/(36525*86400)*0.011473'' = 2e-10 arcseconds = 0.0002 microarcseconds
// So the error being negligible, we use the TT dates in this function.

[tsid,tsiddot] = CL__mod_sidTimeVeis(jd, comega);
[era, eradot] = CL__iers_era2000(jd, comega);
[sprime, sprimedot] = CL__iers_sp2000(jd, comega);

M = CL_rot_angles2matrix(3, (era+sprime)-tsid);

// Angular velocity vector
omega = [];
if (comega)
  N = size(jd,2);
  omega = zeros(3,N);
  omega(3,:) = (eradot+sprimedot)-tsiddot;
end

endfunction
