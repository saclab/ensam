//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M, omega] = CL__fr_mod2eod(jd,args, comega)
// Mean of date (MOD) to Ecliptic Of Date (EOD) transformation matrix and angular velocity vector
//
// Calling Sequence
// [M, omega] = CL__fr_mod2eod(jd,args [,comega])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>M</b> from MOD to EOD</p>
// <p>By convention, multiplying <b>M</b> by coordinates relative to MOD yields coordinates relative to EOD.</p> 
// <p>Optionaly computes the angular velocity vector <b>omega</b> of EOD relative to MOD, with coordinates relative to MOD.
// See <link linkend="Data types">Data types</link> for more details on the definition of angular velocity vectors and frame transformation matrix.</p>
// <p></p></listitem>
// <p>The transformation consists of a single rotation around the X-axis of the mean 
// obliquity of the ecliptic.</p>
// <p>args is a structure containing one field:</p> 
// <p>- "obliquity_model": (string) Available values are :
// <p>-          - "2006" for IAU2006 obliquity (see CL__iers_obli2006).</p>
// <p>-          - "1980" for IAU1980 obliquity (see CL__iers_obli1980).</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// args: Not used.
// comega: (boolean, optional) Option to compute omega. If comega is %f, omega will be set to []. Default is %t. (1x1)
// M: Transformation matrix (3x3xN)
// omega: (optional) Angular velocity vector [rad/s] (3xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010

// Declarations:

// Code:

if (~exists("comega","local")); comega = %t; end;
if (argn(1) <= 1); comega = %f; end;

if ~isstruct(args); CL__error("args should be a structure"); end;

if (~isfield(args,"obliquity_model")); CL__error("Field(s) missing in args"); end;
if (args.obliquity_model <> "2006" & args.obliquity_model <> "1980")
  CL__error("Invalid obliquity model"); 
end


if (args.obliquity_model == "2006")
  [epsa, epsadot] = CL__iers_obli2006(jd, comega);
elseif (args.obliquity_model == "1980")
  [epsa, epsadot] = CL__iers_obli1980(jd, comega);
end

M = CL_rot_angles2matrix(1,epsa);

// Angular velocity
omega = [];
if (comega)
  omega = CL_rot_angVelocity(1, epsa, epsadot);
end

endfunction
