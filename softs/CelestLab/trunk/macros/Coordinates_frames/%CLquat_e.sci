//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q2 = q1(I)
// I = integers (row vector) or : or 1: $ or $ or etc... - can be []

function [q2] = %CLquat_e(I,q1)

if ~(typeof(I) == "implicitlist" | typeof(I) == "size implicit" | typeof(I) == "polynomial" | isequal(I,:))
  if (I <> [] & (size(I,1) > 1 | min(I) <= 0.5 | max(I) >= size(q1)+0.5))
    CL__error("Invalid index"); 
  end
end

q2 = CL__defQuat(q1.r(1,I), q1.i(1:3,I));

endfunction
