//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q2 = q1 .* a 
// a = scalar or row vector 

function [q2] = %CLquat_x_s(q1,a)

N1 = size(q1);
sa = size(a); 
N = max(N1, sa(2)); 

if (sa(1) > 1 | (sa(2) > 1 & N1 > 1 & sa(2) <> N1))
  CL__error("Invalid arguments sizes"); 
end

if (sa(2) < N); 
  a = a * ones(1,N); 
end

if (N1 < N) 
  q1.r = q1.r * ones(1,N);
  q1.i = q1.i * ones(1,N);
end

q2 = CL__defQuat(a .* q1.r, (ones(3,1)*a) .* q1.i);

endfunction
