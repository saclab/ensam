//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M, omega] = CL_fr_locOrbMat(frame_name, pos, vel, acc)
// Inertial frame to custom local orbital frame transformation matrix
//
// Calling Sequence
// [M] = CL_fr_locOrbMat(pos, vel, frame_name)
// [M, omega] = CL_fr_locOrbMat(frame_name, pos, vel [, acc])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>M</b> from 
// the inertial reference frame to a custom local orbital frame.</p>
// <p>The inertial frame is implicitly the frame relative to which the satellite's
// position and velocity are defined. The local frame is built using these 
// position and velocity vectors. </p>
// <p></p>
// <p><b>frame_name</b> is the name of the local frame. It can be: </p>
// <p>- 1) derived from "qsw" : "qsw", "swq", "qSW", ... </p>
// <p>- 2) derived from "tnw" : "tnw", "nwt", "tNW", ... </p>
// <p>- 3) an alias to a frame from 1) or 2): "lvlh" (same as "sWQ") or "rtn" (same as "qsw") </p> 
// <p> For cases 1 and 2, if 'a' (lower case!) is the name of some axis, 'A' (upper case!) 
// is the name of the same axis in the opposite direction. The reference frame that results 
// should be direct. </p>
// <p> Example: "qWS" means: x-axis = q, y-axis = -w, z-axis = -s</p>
// <p></p>
// <p> The angular velocity vector of the local frame with respect to the inertial frame
// is optionally computed. </p>
// <p> A rigourous computation requires the acceleration to be provided. But if the local frame is derived 
// from "qsw", an approximate value can be obtained using acc = [0;0;0]. 
// omega is then equal to: pos ^ vel / ||pos||^2. This result is exact for a keplerian motion.  </p> 
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p> 
// <p> - By convention, multiplying <b>M</b> by coordinates
// relative to the inertial frame yields coordinates relative to the local frame. </p>
// <p> - For the computation of omega to be correct, vel should be the 1st time derivative 
// of pos, and acc should be the first time derivative of vel. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Local frames">Local frames</link> for  more details on the definition of local frames.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// pos: Satellite's position vector relative to the inertial frame [m] (3xN or 3x1)
// vel: Satellite's velocity vector relative to the inertial frame [m/s] (3xN or 3x1)
// acc: (optional) Satellite's acceleration vector relative to the inertial frame or empty. [m/s] (3xN or 3x1)
// frame_name: (string) Name of local frame (1x1)
// M: Inertial to local orbital frame transformation matrix (3x3xN)
// omega: Angular velocity vector of local frame / inertial frame - components in inertial frame (3xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fr_qswMat
// CL_fr_tnwMat
//
// Examples
// pos = [3500.e3; 2500.e3; 5800.e3];
// vel = [1.e3; 3.e3; 7.e3];
//
// // Example 1: Standard 'qsw' local frame: 
// M = CL_fr_locOrbMat("qsw", pos, vel)
// M = CL_fr_locOrbMat("rtn", pos, vel) // => same
// M = CL_fr_qswMat(pos, vel) // => same 
//
// // Example 2: 'lvlh' (same as (s, -w, -q)): 
// M = CL_fr_locOrbMat("lvlh", pos, vel)
// M = CL_fr_locOrbMat("sWQ", pos, vel) // => same
// M = CL_fr_lvlhMat(pos, vel) // => same 
//
// // Example 3: comparison with CL_rot_defFrameVec: 
// M = CL_fr_locOrbMat("qWs", pos, vel )
// M = CL_rot_defFrameVec(pos, vel, 1, 3) // => same 
// 
// // Example 4: computation of omega (keplerian motion): 
// // NB: [0;0;0] only valid because local frame is derived from qsw.
// [M, omega] = CL_fr_locOrbMat("qsw", pos, vel, [0;0;0])
// // NB: gravitational constant is implicit 
// [M, omega] = CL_fr_locOrbMat("tnw", pos, vel, CL_fo_centralAcc(pos))


// Declarations:

// low level computation of omega
// all vectors : 3xN
// frame_type: 1 => qsw axes, 2 => tnw axes
function [omega] = locOrbMat_omega(pos, vel, acc, frame_type)

  // computation for qsw
  C = CL_cross(pos, vel); 
  omega0 = CL_dMult(C, 1 ./ CL_dot(pos)); 

  // correction for qsw frame: acc taken into account
  delta_qsw = CL_dMult(CL_dot(C, acc) ./ CL_dot(C), pos); 

  // omega qsw/inertial
  omega = omega0 + delta_qsw; 

  // omega tnw/inertial : add component // pos ^ vel 
  if (frame_type == 2); 
    delta_tnw = CL_dMult(CL_cross(vel, acc - CL_cross(omega, vel)), 1 ./ CL_dot(vel)); 
    omega = omega + delta_tnw; 
  end

endfunction


// Code:
if (~exists("acc", "local")); acc = []; end

// error checking 
if (argn(2) < 3)
  CL__error("Invalid number of input arguments"); 
end

// Compatibility with previous interface 
if (typeof(vel) == "string")
  [pos, vel, frame_name] = (frame_name, pos, vel); 
end

if (typeof(frame_name) <> "string" | size(frame_name, "*") <> 1)
  CL__error("Invalid type or size for argument: frame_name"); 
end

// Special cases 
if (frame_name == "lvlh"); frame_name = "sWQ"; end
if (frame_name == "rtn"); frame_name = "qsw"; end

// check size / resize
[pos, vel, acc, N] = CL__checkInputs(pos, 3, vel, 3, acc, 3); 

// check if omega should be computed
// (computed if omega <> [] et at least 2 output arguments) 
omega = []; 
compute_omega = %t; 
if (acc == [] | argn(1) < 2)
  compute_omega = %f; 
end

// --------------------------------------------------------
// CODE FOR GENERATING ALL THE COMBINATIONS (numbers and names)
// // solutions avec axe1 = 1
// axes = [ [1;2;3], [1;3;-2], [1;-2;-3], [1; -3; 2] ]; 
// // + circ. permutations : 1=>2, 1=>3
// axes = [axes, sign(axes) .* (pmodulo(abs(axes),3)+1), sign(axes) .* (pmodulo(abs(axes)+1,3)+1)  ]; 
// // + opposite signs : [1,2,3] => [-1,-2,3] 
// axes = [axes, [-axes(1:2,:); axes(3,:)] ]; 
// // fprintfMat("axes.txt", axes, "%3g")
//
// // calcul noms
// lettres_tnw = ['W', 'N', 'T', '', 't', 'n', 'w']; 
// lettres_qsw = ['W', 'S', 'Q', '', 'q', 's', 'w']; 
// noms_tnw = lettres_tnw(axes(1,:)+4) + lettres_tnw(axes(2,:)+4) + lettres_tnw(axes(3,:)+4) 
// noms_qsw = lettres_qsw(axes(1,:)+4) + lettres_qsw(axes(2,:)+4) + lettres_qsw(axes(3,:)+4) 
// strcat("""" + noms_qsw + """", ' ')
// strcat("""" + noms_tnw + """", ' ')
// --------------------------------------------------------

// axes numbers for tnw or qsw => see above
axes = [
  1   1   1   1   2   2   2   2   3   3   3   3  -1  -1  -1  -1  -2  -2  -2  -2  -3  -3  -3  -3; .. 
  2   3  -2  -3   3   1  -3  -1   1   2  -1  -2  -2  -3   2   3  -3  -1   3   1  -1  -2   1   2; .. 
  3  -2  -3   2   1  -3  -1   3   2  -1  -2   1   3  -2  -3   2   1  -3  -1   3   2  -1  -2   1  .. 
]; 

names_qsw = [ "qsw" "qwS" "qSW" "qWs" "swq" "sqW" "sWQ" "sQw" "wqs" "wsQ" "wQS" "wSq" ..
              "QSw" "QWS" "QsW" "Qws" "SWq" "SQW" "SwQ" "Sqw" "WQs" "WSQ" "WqS" "Wsq" ]; 
names_tnw = [ "tnw" "twN" "tNW" "tWn" "nwt" "ntW" "nWT" "nTw" "wtn" "wnT" "wTN" "wNt" ..
              "TNw" "TWN" "TnW" "Twn" "NWt" "NTW" "NwT" "Ntw" "WTn" "WNT" "WtN" "Wnt" ]; 

types_qsw = 1 * ones(axes(1,:)); 
types_tnw = 2 * ones(axes(1,:)); 

// axes and names for qsw and tnw (qsw first)
axes = [axes, axes]; 
frame_names = [names_qsw, names_tnw]; 
frame_type = [types_qsw, types_tnw]; 

k = find(frame_names == frame_name);
if (k == [])
  CL__error("Invalid frame name");
end

if (frame_type(k) == 1)
  // Frame type = qsw
  [P] = CL_fr_qswMat(pos, vel);
elseif (frame_type(k) == 2)
  // Frame type = tnw
  [P] = CL_fr_tnwMat(pos, vel);
end

// Sign of axes
sa1 = sign(axes(1,k));
sa2 = sign(axes(2,k));
sa3 = sign(axes(3,k));

// Indices of axes
ia1 = abs(axes(1,k));
ia2 = abs(axes(2,k));
ia3 = abs(axes(3,k));

// hypermat (leftest index first)
M = hypermat([3, 3, N], [sa1*P(ia1,1,:); sa2*P(ia2,1,:); sa3*P(ia3,1,:); ...
                         sa1*P(ia1,2,:); sa2*P(ia2,2,:); sa3*P(ia3,2,:); ...
                         sa1*P(ia1,3,:); sa2*P(ia2,3,:); sa3*P(ia3,3,:)]);
if (N == 1)
  M = M(:,:,1);
end

// omega
if (compute_omega)
  omega = locOrbMat_omega(pos, vel, acc, frame_type(k)); 
end

endfunction
