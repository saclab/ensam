//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// tle2 = tle1(I)
// I = integers (row vector) - can be []

function [tle2] = %CLtle_e(I,tle1)

CL__tle_checkValid(tle1); 

// q(:) <-> q(1:$)
if (isequal(I,:))
  I = 1:$;
end

// NB : I is of type "size implicit" if it contains the $ operator (ex : 1:$)
if (typeof(I) <> "size implicit")
  sI = size(I);
  N = size(tle1);

  if (I <> [] & (sI(1) > 1 | max(I) >= N+0.5 | min(I) <= 0.5))
    CL__error("Invalid index"); 
  end
end

[tle2] = CL__tle_create(); 

names = fieldnames(tle2)'; 

for name = names
  tle2(name) = tle1(name)(I); 
end
             
endfunction
