//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// tle2(I) = tle1 
// I = integers (row vector)
// I can be empty in theory (but it does not work)

function [tle2] = %CLtle_i_CLtle(I,tle1,tle2)

CL__tle_checkValid(tle1); 
CL__tle_checkValid(tle2); 

N1 = size(tle1);
N2 = size(tle2);

// tle(:) <-> tle(1:$)
if (isequal(I,:))
  I = 1:$;
end

// NB : I is of type "size implicit" if it contains the $ operator (ex : 1:$)
if (typeof(I) <> "size implicit")
  sI = size(I);
  if (sI(1) > 1 | min(I) <= 0.5 | max(I) >= N2+0.5)
    CL__error("Invalid index"); 
  end
  
else
  sI = size(tle2(I));
  if (sI == 0)
    I = [];
    sI = [0, 0];
  else
    sI = [1,sI];
  end
end



if (sI(1) == 0 | N2 == 0)
  return; // no change

else

  if (N1 <> 1 & N1 <> sI(2))
    CL__error("Invalid size for tle1"); 
  end

  names = fieldnames(tle2)'; 

  if (N1 < sI(2))
    for name = names
      tle1(name) = repmat(tle1(name), 1, sI(2)); 
    end
  end

  for name = names
    tle2(name)(I) = tle1(name); 
  end

end

endfunction

