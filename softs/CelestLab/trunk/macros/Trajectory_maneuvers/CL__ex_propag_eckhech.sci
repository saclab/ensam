//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_cir_t2,osc_cir_t2] = CL__ex_propag_eckhech(t1,mean_cir_t1,t2,er,mu,j1jn,compute_osc)
// Eckstein-Hechler propagation model
//
// Calling Sequence
// [mean_cir_t2,osc_cir_t2] = CL__ex_propag_eckhech(t1,mean_cir_t1,t2,er,mu,j1jn,compute_osc)
//
// Description
// <itemizedlist><listitem>
// <p>Propagates orbital elements using Eckstein-Hechler analytical model. </p>
// <p></p></listitem>
// <listitem>
// <p><b>Important note:</b></p>
// <p>The original algorithm has been modified so that the perigee of a frozen orbit remains perfectly constant over time. 
// This resulted in a change of the mean eccentricity vector (x component) of about 4.e-7, which is small compared to the 
// model's accuracy. </p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// t1: Initial time [days] (1x1 or 1xN)
// mean_cir_t1: Mean orbital elements at time t1 (6x1 or 6xN). 
// t2: Final time [days] (1xN or 1x1). 
// er: Equatorial radius [m]. 
// mu: Gravitational constant [m^3/s^2]. 
// j1jn: Vector of zonal harmonics. 
// compute_osc: True if osculating elements are computed
// mean_cir_t2: Mean orbital elements at t2  (6xN)
// osc_cir_t2: Osculating orbital elements at t2 (6xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Example 1: one orbit, several final time instants:
// t1 = 12584;
// mean_cir_t1 = [7.e6; 0; 1.e-3; 1; 0.1; 0.2]; 
// t2 = 12587:1:12590
// [mean_cir_t2,osc_cir_t2] = CL__ex_propag_eckhech(t1,mean_cir_t1,t2)
//
// // Example 2: one orbit, several final time instants:
// t1 = 12584;
// mean_cir_t1 = [7.e6; 0; 1.e-3; 0.1; 0.2; 0.3] * [1,1]; 
// t2 = [12587, 12588];
// [mean_cir_t2,osc_cir_t2] = CL__ex_propag_eckhech(t1,mean_cir_t1,t2)

// Check number of input arguments
if (argn(2) <> 7)
  CL__error("Invalid number of input arguments"); 
end

// osculating elements: not computed if not necessary
lhs = argn(1);
compute_osc = %t;
if (lhs < 2); compute_osc = %f; end

// Propagate using Eckstein-Hechler model
numres = 1; 
if (compute_osc); numres = 2; end

mean_cir_t2 = []; 
osc_cir_t2 = []; 
[mean_cir_t2, osc_cir_t2] = CL__ex_eckHech(t1, mean_cir_t1, t2, er, mu, j1jn, numres); 

endfunction
