//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [info] = CL__ex_getInfo_j2sec(mean_kep, er, mu, j2)
// Information about "secular J2" orbit propagation model
//
// Calling Sequence
// info = CL__ex_getInfo_j2sec(mean_kep, er, mu, j2)
//
// Description
// <itemizedlist><listitem>
// <p>Computes data related to "secular J2" orbit propagation model.</p>
// <p> These data are: </p>
// <p> - Eccentricity vector of frozen orbit </p>
// <p> - Secular drifts of some angular orbital elements </p>
// <p></p>
// <p>The output is a structure with the following fields:</p>
// <p> - info.eccf: Mean eccentricity of frozen orbit</p>
// <p> - info.pomf: Mean argument of periapsis of frozen orbit</p>
// <p> - info.dgomdt: Mean secular drift of right ascension of ascending node [rad/s]</p>
// <p> - info.dpsodt: Mean secular drift of mean argument of latitude [rad/s]</p>
// <p></p></listitem>
// <listitem> 
// <p><b>Note:</b></p>  
// <p>The outputs only depend on semi major-axis, eccentricity and inclination. </p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// mean_kep: Mean orbital elements (6xN). 
// er: Equatorial radius [m] 
// mu: Gravitational constant [m^3/s^2]. 
// j2: (optional) (Unnormalized) zonal coefficient (second zonal harmonic). 
// info: (structure) Data for the given orbital elements (containing (1xN) vectors)
//
// Authors
// CNES - DCT/SB
//
// Examples
// er = 6378136.3; 
// mu = 3.98600442e+14; 
// j2 = 0.001082626613; 
// 
// mean_kep = [7.e6; 1.e-2; 1.7; 0; 0; 0 ]; // sma, ecc, inc... 
// info = CL__ex_getInfo_j2sec(mean_kep, er, mu, j2);

// Check number of input arguments
if (argn(2) <> 4)
  CL__error("Invalid number of input arguments"); 
end

[dpomdt,dgomdt,danmdt] = CL_op_driftJ2(mean_kep(1,:), mean_kep(2,:), mean_kep(3,:), er, mu, j2); 

info = struct();
info.eccf = zeros(dgomdt); 
info.pomf = zeros(dgomdt); 
info.dgomdt = dgomdt; 
info.dpsodt = dpomdt + danmdt; 

endfunction



