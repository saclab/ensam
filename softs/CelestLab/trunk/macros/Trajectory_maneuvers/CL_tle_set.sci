//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


function [tle] = CL_tle_set(tle, varargin)
// Change values in a TLE structure 
//
// Calling Sequence
// tle2 = CL_tle_set(tle, fieldname, value, fieldname, value...)
//
// Description
// <itemizedlist>
// <listitem>
// <p>This function can be used to (safely) change the values of some fields in a TLE structure. </p>
// <p>No unit conversion takes place. </p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// tle: TLE structure (size=N)
// fieldname: (string) Name of field in TLE structure (1x1)
// value: Value of TLE field (1x1 or 1xN)
// tle2: Resulting TLE structure (size=N)
//
// Authors
// CNES - DCT/SB/MS
//
// See also
// CL_tle_update
//
// Examples
// tle = CL_tle_new(); 
// // Change some values
// tle = CL_tle_set(tle, "epoch_cjd", CL_dat_cal2cjd(2013,1,1,0,0,0), ..
// "ecc", 0.1, "inc", 1, "argp", 0.2, "raan", 0.3, "ma", 0.4, "n", 1.e-3, "bstar", 1.e-4)



// check validity of TLE struct
CL__tle_checkValid(tle); 

nb = size(varargin); 
if (modulo(nb,2) <> 0)
  CL__error("Wrong number of input arguments"); 
end

N = size(tle); 
names = fieldnames(tle);

for k = 1 : round(nb/2)

  name = varargin(2*k-1); 
  val = varargin(2*k); 

  if (find(name == names) == [])
    CL__error("Invalid field name (" + name + ")"); 
  end

  if (size(val,1) <> 1)
    CL__error("Invalid value size for " + name + " (one row expected)"); 
  end

  nr = size(val,2); 
  if (nr <> 1 & nr <> N)
    CL__error("Invalid value size for " + name + " (wrong number of columns)"); 
  end

  if (nr == 1)
    val = repmat(val, 1, N);  
  end

  tle(name) = val; 

end


endfunction
