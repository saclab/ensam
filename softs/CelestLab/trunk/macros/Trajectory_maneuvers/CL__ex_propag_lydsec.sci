//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_kep_t2,osc_kep_t2] = CL__ex_propag_lydsec(t1,mean_kep_t1,t2,er,mu,j1jn,compute_osc)
// Orbit propagation using Lyddane model (mean elements = secular)
//
// Calling Sequence
// [mean_kep_t2,osc_kep_t2] = CL__ex_propag_lydsec(t1,mean_kep_t1,t2,er,mu,j1jn,compute_osc)
//
// Description
// <itemizedlist><listitem>
// <p>Propagates orbital elements using Lyddane analytical model. </p>
// <p>The mean elements for this model include secular effects only. </p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- There can be 1 or N initial times, and 1 or N final times. </p>
// <p> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn".</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// t1: Initial time [days] (1x1 or 1xN)
// mean_kep_t1: Mean orbital elements at time t1 (6x1 or 6xN).  
// t2: Final time [days] (1xN or 1x1).  
// er: Equatorial radius [m]. 
// mu: Gravitational constant [m^3/s^2]. 
// j1jn: Vector of zonal harmonics. (Nzx1)
// compute_osc: True if osculating elements are computed
// mean_kep_t2: Mean orbital elements at t2 (6xN)
// osc_kep_t2: Osculating orbital elements at t2 (6xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// er = 6378136.3; 
// mu = 3.98600442e+14; 
// j1jn = [0; 0.001082626613; -0.000002532393; -0.000001619137; -0.000000227742; 0.000000538219]; 
// 
// // Example 1: one orbit, several final time instants:
// t1 = 12584
// mean_kep_t1 = [7.e6; 0; 1.e-3; %pi/2; 0.1; 0.2]; 
// t2 = 12587:1:12590
// [mean_kep_t2,osc_kep_t2] = CL__ex_propag_lydsec(t1, mean_kep_t1, t2, er, mu, j1jn, %t)
//
// // Example 2: several orbits, several final time instants:
// t1 = 12584
// mean_kep_t1 = [7.e6; 0; 1.e-3; %pi/2; 0.1; 0.2] * [1,1]; 
// t2 = [12587, 12588];
// [mean_kep_t2,osc_kep_t2] = CL__ex_propag_lydsec(t1, mean_kep_t1, t2, er, mu, j1jn, %t)


// Check number of input arguments
if (argn(2) <> 7)
  CL__error("Invalid number of input arguments"); 
end

// osculating elements: not computed if not necessary
lhs = argn(1); 
compute_osc = %t; 
if (lhs < 2); compute_osc = %f; end

// Propagate using mean (=secular) elements
numres = 1; 
if (compute_osc); numres = 3; end

mean_kep_t2 = []; 
osc_kep_t2 = []; 
[mean_kep_t2, X, osc_kep_t2] = CL__ex_lyddane(t1, mean_kep_t1, t2, er, mu, j1jn, numres); 

endfunction
