//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [tle] = CL_tle_update(tle, cjd, pos, vel, frame, ut1_tref, tt_tref)
// Update TLE orbital state 
//
// Calling Sequence
// tle2 = CL_tle_update(tle, cjd, pos, vel [, frame, ut1_tref, tt_tref])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Resets the state of a TLE structure using the specified date and (osculating) position and velocity. </p>
// <p></p>
// <p>In a standard situation, the input position and velocity are converted from the specified reference 
// <b>frame</b> to TEME. The input times are considered to be given in the TREF time scale and are converted 
// to UTC internally. Note that if the frame is not a true equator frame, the transformation may not as accurate as should be
// as the frame transformation arguments that can be provided are limited to ut1_tref and tt_tref. </p>
// <p></p>
// <p>It is also possible to give <b>frame</b> a special value: "native". It means that reference frame AND 
// time scale are supposed to be those natively used for TLEs: TEME and UTC repectively. No conversion is performed. </p> 
// </listitem>
// </itemizedlist>
// 
// Parameters
// tle: TLE structure (size N)
// cjd: TLE epoch (modified Julian days from 1950.0, TREF time scale except if frame == "native") (1xN)
// pos: Osculating position vector in specified frame (3xN)
// vel: Osculating velocity vector relative to specified frame (3xN)
// frame: Frame for position and velocity, see above for details. Default is "ECI". 
// ut1_tref: (optional) UT1-TREF [seconds]. Default is %CL_UT1_TREF (1xN or 1x1)
// tt_tref: (optional) TT-TREF [seconds]. Default is %CL_TT_TREF. (1xN or 1x1)
//
// Authors
// CNES - DCT/SB/MS
//
// See also
// CL_tle_set
//
// Examples
// tle = CL_tle_new(); 
// cjd = CL_dat_cal2cjd(2013,1,1); // TREF
// pos_eci = [7000.e3; 0; 0]; 
// vel_eci = [0; 7000; 0]; 
// tle = CL_tle_update(tle, cjd, pos_eci, vel_eci); 
// // Check result
// [pos, vel] = CL_tle_genEphem(tle, cjd) 


// Declarations:

// Code
if (~exists("frame", "local")); frame="ECI"; end
if (~exists("ut1_tref", "local")); ut1_tref = CL__dataGetEnv("UT1_TREF"); end 
if (~exists("tt_tref", "local")); tt_tref = CL__dataGetEnv("TT_TREF"); end 

// TLE propagation options from configuration
whichconst = CL_configGet("TLE_WHICHCONST"); 
opsmode = CL_configGet("TLE_OPSMODE");

// check validity of TLE struct
CL__tle_checkValid(tle); 

if (typeof(frame) <> "string" | size(frame, "*") <> 1)
  CL__error("Invalid argument type or size for frame"); 
end

// Check t, pos, vel sizes (number of rows)
CL__checkInputs(cjd, 1, pos, 3, vel, 3); 

N = size(tle); 

// check number of columns (same dimension for all args)
if (size(cjd,2) <> N | size(pos,2) <> N | size(vel,2) <> N) 
  CL__error("Invalid size of input arguments");  
end

// Nothing: empty TLE
if (N == 0)
  return;  
end

// Converts position and velocity to TEME
if (frame <> "TEME" & frame <> "native")
  [pos, vel] = CL_fr_convert(frame, "TEME", cjd, pos, vel, ut1_tref=ut1_tref, tt_tref=tt_tref); 
end

// converts cjd to UTC time scale (TLE time scale)
if (frame <> "native")
  cjd = CL_dat_scaleConvert("TREF", "UTC", cjd, ut1_tref, tt_tref); 
end

// call low level function
tle = CL__tle_sgp4Update(tle, cjd, pos, vel, whichconst, opsmode); 

     
endfunction

