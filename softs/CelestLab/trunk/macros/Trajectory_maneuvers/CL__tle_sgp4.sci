//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pos, vel] = CL__tle_sgp4(tle, cjd, whichconst, opsmode)
// TLE propagation (SGP4/SDP4 models) - low level function
//
// Calling Sequence
// [pos_teme, vel_teme] = CL__tle_sgp4(tle, cjd, whichconst, opsmode)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Propagates the TLE state to the specified dates and returns the corresponding (osculating) position and velocity relative to TEME.</p> 
// <p>No conversion of reference frame or time scale takes place.</p> 
// </listitem>
// <p></p>
// <listitem>
// <p><b>Notes</b>: </p>
// <p>In most situations, the function <link linkend="CL_tle_genEphem">CL_tle_genEphem</link> should be preferred. </p>
// </listitem>
// </itemizedlist>
// 
// Parameters
// tle: Structure containing one TLE (size: 1x1 or 1xN)
// cjd: Propagation date (Julian days from 1950.0, same time scale as for TLE epoch (UTC)) (1xN)
// whichconst: Set of constants to be used: "wgs72old", "wgs72", "wgs84". Preferred value is "wgs72"
// opsmode: Mode of operation "a" (afspc mode) or "i" (improved). Preferred value is "a"
// pos_teme: Position vectors in TEME frame [m] (3xN)
// vel_teme: Velocity vectors relative to TEME frame [m/s] (3xN)
//
// Examples
// two_lines = [..
// "1 00005U 58002B   00179.78495062  .00000023  00000-0  28098-4 0  4753"; ..
// "2 00005  34.2682 348.7242 1859667 331.7664  19.3264 10.82419157413667" ];
// tle = CL_tle_parse(two_lines); 
// cjd = tle.epoch_cjd + (0 : 60 : 600) / 86400;  
// [pos, vel] = CL__tle_sgp4(tle, cjd)
//
// See also
// CL_tle_genEphem
//
// Authors
// CNES - DCT/SB/MS

// check presence of extension module
CL__checkExtensionModule(); 

// check validity of TLE struct
CL__tle_checkValid(tle); 

Ntle = size(tle); 

if ~(Ntle == 1 | (Ntle > 1 & (size(cjd,2) == Ntle | size(cjd,2) == 1)))
  CL__error("Invalid input arguments");  
end

if (size(cjd,2) == 1)
  cjd = cjd * ones(1,Ntle); 
end

N = size(cjd, 2); 
pos = %nan *ones(3,N); 
vel = %nan * ones(3,N); 
delta_t = (cjd - tle.epoch_cjd) * 86400; 

I = find(tle.status == 0); 
if (I <> [])
  epoch_cjd = tle.epoch_cjd(I); 
  ecc = tle.ecc(I); 
  inc = tle.inc(I); 
  argp = tle.argp(I); 
  raan = tle.raan(I); 
  ma = tle.ma(I);
  mm = tle.n(I); 
  bstar = tle.bstar(I); 
  if (Ntle == N); delta_t = delta_t(I); end 
 
  [p,v] = CLx_tle_sgp4(epoch_cjd, ecc, inc, argp, raan, ma, mm, bstar, delta_t, whichconst, opsmode); 

  if (Ntle == N) 
    pos(:,I) = p; 
    vel(:,I) = v; 
  else
    pos = p; 
    vel = v; 
  end

end
      
endfunction
