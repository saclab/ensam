//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [tle] = CL__tle_sgp4Update(tle, cjd, pos, vel, whichconst, opsmode)
// Update TLE orbital state - low level function
//
// Calling Sequence
// tle2 = CL__tle_sgp4Update(tle, cjd, pos_teme, vel_teme [, whichconst, opsmode])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Initializes the orbital state of a TLE structure using date and (osculating) position and velocity. </p>
// <p>No conversion of reference frame or time scale takes place.</p> 
// </listitem>
// <p></p>
// <listitem>
// <p><b>Note</b>: </p>
// <p>In most situations, the function <link linkend="CL_tle_setState">CL_tle_setState</link> should be preferred. </p>
// </listitem>
// </itemizedlist>
// 
// Parameters
// tle: TLE structure (size: N)
// cjd: Date (Julian days from 1950.0, same time scale as for TLE epoch (UTC)) (1xN)
// pos_teme: Position Vector in TEME frame (3xN)
// vel_teme: Velocity Vector relative to TEME frame (3xN)
// whichconst: Set of constants to be used: "wgs72old", "wgs72", "wgs84". Preferred value is "wgs72"
// opsmode: Mode of operation "a" (afspc mode) or "i" (improved). Preferred value is "a"
//
// Examples
// tle = CL_tle_new(); 
// epoch_cjd = CL_dat_cal2cjd(2013,1,1); // UTC
// pos_teme = [7000.e3; 0; 0]; 
// vel_teme = [0; 7000; 0]; 
// tle = CL__tle_sgp4Update(tle, epoch_cjd, pos_teme, vel_teme) 
// // Check
// [pos, vel] = CL__tle_sgp4(tle, epoch_cjd)
//
// See also
// CL_tle_setState
//
// Authors
// CNES - DCT/SB/MS

// mean elements => osculating 
// (called by CL__ex_inverse)
function [kepo] = tle_fct_extrap(kepm, I, args)

  ecc = kepm(2,:);
  inc = kepm(3,:); 
  argp = kepm(4,:); 
  raan = kepm(5,:); 
  ma = kepm(6,:); 
  mm = sqrt(args.mu ./ kepm(1,:).^3); 
  delta_t = 0; 
  
  [pos, vel, kepo] = CLx_tle_sgp4(args.epoch_cjd(I), ecc, inc, argp, raan, ma, mm, args.bstar, delta_t, args.whichconst, args.opsmode); 
  // already computed !
  // kepo = CL_oe_car2kep(pos, vel, args.mu); 

endfunction


// check presence of extension module
CL__checkExtensionModule(); 

// check validity of TLE struct
CL__tle_checkValid(tle); 

// Set of constants for SGP4
cst = CLx_tle_getConst(whichconst); 

// wanted osc elements
kepo_ref = CL_oe_car2kep(pos, vel, cst.mu); 

// arguments for inverse function
args = struct(); 
args.mu = cst.mu; 
args.bstar = tle.bstar; 
args.epoch_cjd = cjd; 
args.whichconst = whichconst; 
args.opsmode = opsmode; 

[kepm] = CL__ex_inverse("kep", kepo_ref, tle_fct_extrap, args); 

ecc = kepm(2,:);
inc = kepm(3,:); 
argp = kepm(4,:); 
raan = kepm(5,:); 
ma = kepm(6,:); 
mm = sqrt(cst.mu ./ kepm(1,:).^3); 

tle = CL_tle_set(tle, "epoch_cjd", cjd, "ecc", ecc, "inc", inc, "argp", argp, "raan", raan, "ma", ma, "n", mm, "ndot", 0, "nddot", 0); 

endfunction
