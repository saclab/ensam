//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// n = size(tle)
// NB: structure should be valid

function [n] = %CLtle_size(tle)

if (typeof(tle) <> "CLtle")
  CL__error("Invalid argument type, TLE structure expected")
end

try
  n = length(tle.status);
catch
  CL__error("Invalid argument, TLE structure expected")
end

endfunction
