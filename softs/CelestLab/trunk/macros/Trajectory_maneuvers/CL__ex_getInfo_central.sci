//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [info] = CL__ex_getInfo_central(mean_kep, mu)
// Informations about Lyddane orbit propagation analytical model
//
// Calling Sequence
// info = CL__ex_getInfo_central(mean_kep, mu)
//
// Description
// <itemizedlist><listitem>
// <p>Computes data related to "central" orbit propagation analytical model.</p>
// <p> These data are: </p>
// <p> - Eccentricity vector of frozen orbit </p>
// <p> - Secular drifts of some angular orbital elements </p>
// <p></p>
// <p>The output is a structure with the following fields:</p>
// <p> - info.eccf: Mean eccentricity of frozen orbit</p>
// <p> - info.pomf: Mean argument of periapsis of frozen orbit</p>
// <p> - info.dgomdt: Secular drift of mean right ascension of ascending node [rad/s]</p>
// <p> - info.dpsodt: Secular drift of mean argument of latitude [rad/s]</p>
// <p></p></listitem>
// <listitem> 
// <p><b>Note:</b></p>  
// <p>The outputs only depend on semi major-axis, eccentricity and inclination. </p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// mean_kep: Mean orbital elements (6xN). 
// mu: Gravitational constant [m^3/s^2] 
// info: (structure) Data for the given orbital elements (containing (1xN) vectors)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Example :
// mu = 3.98600442e+14; 
// mean_kep = [7.e6; 1.e-2; 1.7; 0; 0; 0 ]; // sma, ecc, inc... 
// info = CL__ex_getInfo_central(mean_kep, mu);

// Check number of input arguments
if (argn(2) <> 2)
  CL__error("Invalid number of input arguments"); 
end

N = size(mean_kep, 2) 

info = struct();
info.eccf = zeros(1,N); 
info.pomf = zeros(1,N); 
info.dgomdt = zeros(1,N); 
info.dpsodt = sqrt(mu ./ mean_kep(1,:).^3); 

endfunction



