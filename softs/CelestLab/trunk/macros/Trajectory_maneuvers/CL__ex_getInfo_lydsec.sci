//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [info] = CL__ex_getInfo_lydsec(mean_kep, er, mu, j1jn)
// Informations about Lyddane orbit propagation analytical model
//
// Calling Sequence
// info = CL__ex_getInfo_lydsec(mean_kep, er, mu, j1jn)
//
// Description
// <itemizedlist><listitem>
// <p>Computes data related to Lyddane orbit propagation analytical model.</p>
// <p> These data are: </p>
// <p> - Eccentricity vector of frozen orbit </p>
// <p> - Secular drifts of some angular orbital elements </p>
// <p></p>
// <p>The output is a structure with the following fields:</p>
// <p> - info.eccf: Mean eccentricity of frozen orbit</p>
// <p> - info.pomf: Mean argument of periapsis of frozen orbit</p>
// <p> - info.dgomdt: Secular drift of mean right ascension of ascending node [rad/s]</p>
// <p> - info.dpsodt: Secular drift of mean argument of latitude [rad/s]</p>
// <p></p></listitem>
// <listitem> 
// <p><b>Note:</b></p>  
// <p>The outputs only depend on semi major-axis, eccentricity and inclination. </p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// mean_kep: Mean orbital elements (6xN). 
// er: Equatorial radius [m] 
// mu: Gravitational constant [m^3/s^2] 
// j1jn: Vector of zonal coefficients J1 to Jn (Nzx1)
// info: (structure) Data for the given orbital elements (containing (1xN) vectors)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Example :
// er = 6378136.3; 
// mu = 3.98600442e+14; 
// j1jn = [0; 0.001082626613; -0.000002532393; -0.000001619137; -0.000000227742; 0.000000538219]; 
// mean_kep = [7.e6; 1.e-2; 1.7; 0; 0; 0 ]; // sma, ecc, inc... 
// info = CL__ex_getInfo_lydsec(mean_kep, er, mu, j1jn);

// Check number of input arguments
if (argn(2) <> 4)
  CL__error("Invalid number of input arguments"); 
end

// Ensure that j1jn has at least 5 elements (fill with zeros)
j1jn = [matrix(j1jn, 1, -1), zeros(1,5)];


// *** CODE TAKEN FROM CL_ex_lyddane *** 
// NB: results only depend on sma, ecc and inc (mean elements)

function [dpomdt,dgomdt,danmdt] = secu_drift_lyd_inf()
  // determination argument du perigee po1 :
  h22 = -35 + 24*tt1 + 25*(tt1.^2) + (90-192*tt1-126*(tt1.^2)) .* (c1.^2)+(385+360*tt1+45*(tt1.^2)) .* (c1.^4);
  h41 = 21 - 9*(tt1.^2)+(-270 + 126*(tt1.^2)) .* (c1.^2) + (385 - 189*(tt1.^2)) .* (c1.^4);
  dpomdt = nmoy .* ( 1.5 * h2 .* ( -dc + h2 .* h22/16 ) + (5/16) * h4 .* h41 ); 

  // determination ascension droite du noeud go1 :
  h22 = (-5 + 12*tt1 + 9 * (tt1.^2)) .* c1 + (-35 -36*tt1 -5*(tt1.^2)) .* (c1.^3);
  h41 = (5 - 3*(tt1.^2)) .* c1 .* (3 - 7*(c1.^2));
  dgomdt = nmoy .* ( 3 * h2 .* (-c1 + h2 .* h22/8 ) + 5 * h4 .* h41/4 ) ;    

  // determination anomalie moyenne am1 :
  h22 = -15 + 16*tt1 + 25*(tt1.^2) + (30 - 96*tt1 - 90*(tt1.^2)) .* (c1.^2) + (105 + 144*tt1 + 25 *(tt1.^2)) .* (c1.^4);
  h41 = (3 - 30*(c1.^2) + 35*(c1.^4));
  danmdt = nmoy .* ( 1 + 1.5 * h2 .* ( tt1 .* (-1 + 3*(c1.^2)) + h2 .* tt1 .* h22/16 ) + 15 * h4 .* tt1 .* e2 .* h41/16 ) ;
endfunction


// DEBUT DU MODELE
// ===============

// constantes liees a 1/2 grand-axe, eccentricite et inclinaison
// ============================================================
q = er ./ mean_kep(1,:); 

g2 = j1jn(2) * (q^2) / 2;

e2 = mean_kep(2,:).^2
tt1 = sqrt(1-e2);

h2 = g2 ./ (tt1.^4);
h3 = -j1jn(3) * (q^3) ./ (tt1.^6);
h4 = -j1jn(4) * (q^4) * (3/8) ./ (tt1.^8);
h5 = -j1jn(5) * (q^5) ./ (tt1.^10);

nmoy = sqrt(mu ./ mean_kep(1,:)) ./ mean_kep(1,:);

c1 = cos(mean_kep(3,:));
dc = 1 - 5 * (c1.^2) ;

s1 = sin(mean_kep(3,:));

r1 = 4 + 3 * e2;
r2 = 4 + 9 * e2;
r4 = e2 .* (c1.^6) ./ dc.^2;


// calcul des termes seculaires en J2,J2 * * 2,J4
// ===========================================
[dpomdt,dgomdt,danmdt] = secu_drift_lyd_inf(); 

info = struct();
info.eccf = zeros(dpomdt); 
info.pomf = zeros(dpomdt); 
//info.dpomdt = dpomdt; 
info.dgomdt = dgomdt; 
info.dpsodt = dpomdt + danmdt; 

endfunction



