//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Checks a TLE structure: 
// - correct type (CLtle)
// - correct number of fields and correct names
// - correct type for each field
// - same dimension (1xN) for each field

function [valid] = CL__tle_checkValid(tle)

valid = %t; 

if (typeof(tle) <> "CLtle")
  CL__error("Invalid argument type"); 
  return;  
end

N = size(tle); 

// valid field names
NAMES = ["satnum", "classif", "intldesg", "ephtype", "elnum", "epoch_cjd", "revnum", "ecc", "inc", "argp", "raan", "ma", "n", "ndot", "nddot", "bstar", "desc", "status" ]; 
TYPES = ["constant", "string", "string", "constant", "constant", "constant", "constant", "constant", "constant", "constant", "constant", "constant", "constant", "constant", "constant", "constant", "string", "constant" ]; 

names = matrix(fieldnames(tle), 1, -1); 

for (name = NAMES)
  if (find(name == names) == [])
    CL__error("Field ''" + name + "'' not found in TLE struct"); 
    return; 
  end
end

for (name = names)
  I = find(name == NAMES); 
  if (length(I) <> 1)
    CL__error("Unexpected field (''" + name + "'') in TLE struct"); 
    return; 
  end
  // check size
  sz = size(tle(name)); 
  if ((N == 0 & (sz(1) <> 0 | sz(2) <> 0)) | (N > 0 & (sz(1) <> 1 | sz(2) <> N)))
    CL__error("Invalid size for field ''" + name + "'' in TLE struct"); 
    return; 
  end
end

if (N == 0)
  return; 
end

for (name = names)
  I = find(name == NAMES); 

  // check type
  if (typeof(tle(name)) <> TYPES(I))
    CL__error("Invalid type for field ''" + name + "'' in TLE struct"); 
    return; 
  end
end

endfunction

