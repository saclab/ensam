//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


function [bal] = CL_tle_getBalCoef(tle)
// Ballistic coefficient from bstar 
//
// Calling Sequence
// bal = CL_tle_getBalCoef(tle)
//
// Description
// <itemizedlist>
// <listitem>
// <p>This function computes the ballistic coefficient (cd*A/m) from bstar. </p>
// <p> The balistic coefficient is computed by: </p>
// <p> bal = 12.741621 * bstar (bal in m^2/kg)</p>
// <p>The Earth radius value used is that from the "wgs72" set of constants. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// tle: TLE structure (size N)
// bal: Ballistic coefficient = Cd*A/M [m^2/kg] (1xN)
//
// Authors
// CNES - DCT/SB/MS
//
// Examples
// str = [..
// "1 00005U 58002B   00179.78495062  .00000023  00000-0  28098-4 0  4753"; ..
// "2 00005  34.2682 348.7242 1859667 331.7664  19.3264 10.82419157413667" ];
// tle = CL_tle_parse(str); 
// bal = CL_tle_getBalCoef(tle)


// check validity of TLE struct
CL__tle_checkValid(tle); 

// reference density (kg/m^3)
// rho0 = 2.461e-5; 
// Earth radius in km
// erkm = 6378.135; 
// bal = 2 * bstar / (rho0 * erkm) = bstar * C

C = 12.741621; 

bal = tle.bstar * C; 

endfunction
