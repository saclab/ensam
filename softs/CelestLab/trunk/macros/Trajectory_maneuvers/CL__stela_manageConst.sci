//  Copyright (c) CNES 2013
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

function [] = CL__stela_manageConst()
// Send CelestLab Constants to STELA
//
// Calling Sequence
// CL__stela_manageConst()
//
// Description
// <itemizedlist>
// <listitem>
// <p>Sends CelestLab constants to STELA. </p>
// <p></p>
// </listitem>
// <listitem>
// <p>See the <link linkend="STELA">STELA</link> page for more details.</p>
// <p></p>
// </listitem>
// </itemizedlist>
// 
// Authors
// CNES - DCT/SB/MS 
//
// Examples
// CL__stela_manageConst()

// Declarations:
global %CL__PRIV; 

// check extension module
CL__checkExtensionModule(); 

// Declarations:

// Get all available constants
if (~isfield(%CL__PRIV, "STELA_CONST"))
  %CL__PRIV.STELA_CONST = []; 
end

stela_const = CL_configGet("STELA_WHICHCONST"); 

if (%CL__PRIV.STELA_CONST <> stela_const)
  if (stela_const == "CL")
    // pass CelestLab constants to STELA
    cst = struct(); 

    cst.mu = CL_dataGet("mu"); 
    cst.er = CL_dataGet("eqRad"); 
    cst.obla = CL_dataGet("obla"); 
    cst.j1jn = CL_dataGet("j1jn"); 
    cst.cs1nm = CL_dataGet("cs1nm"); 
    cst.muSun = CL_dataGet("body.Sun.mu");
    cst.muMoon = CL_dataGet("body.Moon.mu");
    cst.srp_1au = CL_dataGet("totalSolarIrradiance") / CL_dataGet("lightSpeed");
    cst.au = CL_dataGet("au"); 

    CLx_stela_setConst(cst); 

  elseif (stela_const == "ST")
    // reset STELA constants
    CLx_stela_setConst(); 

  else
    CL__error("Invalid value for configuration parameter"); 
  end
  
  %CL__PRIV.STELA_CONST = stela_const; 
  
end
  

endfunction
