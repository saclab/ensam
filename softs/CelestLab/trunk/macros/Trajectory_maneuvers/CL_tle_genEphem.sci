//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [pos, vel] = CL_tle_genEphem(tle, cjd, frame, ut1_tref, tt_tref)
// Ephemeris generation from TLEs (SGP4/SDP4 propagation)
//
// Calling Sequence
// [pos, vel] = CL_tle_genEphem(tle, cjd [, frame, ut1_tref, tt_tref])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Generates an ephemeris (osculating position and velocity) from TLEs. </p>
// <p></p>
// <p>There can be 2 cases: </p>
// <p>- either there is only one element in the TLE structure and any number of propagation times, </p>
// <p>- or there are several elements and as many propagation times. In this case, each element in the TLE structure 
// is propagated to the corresponding time. </p>
// <p></p>
// <p>In a standard situation, the output position and velocity are converted to the specified reference 
// <b>frame</b>. The input times are considered to be given in the TREF time scale and are converted 
// to UTC internally (UTC as defined at TLE epoch). Note that if the frame is not a true equator
// frame, the transformation may not be as accurate as should be as the frame transformation
// arguments that can be provided are limited to ut1_tref and tt_tref. </p>
// <p></p>
// <p>It is also possible to give <b>frame</b> a special value: "native". It means that reference frame AND 
// time scale are supposed to be those natively used for TLEs: TEME and UTC repectively. No conversion is performed. </p> 
// </listitem>
// </itemizedlist>
// 
// Parameters
// tle: TLE structure (size 1 or N)
// cjd: Propagation date (modified julian days from 1950.0, TREF time scale except if frame == "native") (1xN or 1x1)
// frame: Output frame, see above for details. Default is "ECI". 
// ut1_tref: (optional) UT1-TREF [seconds]. Default is %CL_UT1_TREF (1xN or 1x1)
// tt_tref: (optional) TT-TREF [seconds]. Default is %CL_TT_TREF (1xN or 1x1)
// pos: Osculating position vector relative to the specified frame [m] (3xN)
// vel: Osculating velocity vector relative to the specified frame [m/s] (3xN)
//
// Authors
// CNES - DCT/SB/MS
//
// See also
// CL_tle_update
//
// Examples
// str = [..
// "1 00005U 58002B   00179.78495062  .00000023  00000-0  28098-4 0  4753"; ..
// "2 00005  34.2682 348.7242 1859667 331.7664  19.3264 10.82419157413667" ];
// tle = CL_tle_parse(str); 
// cjd = round(tle.epoch_cjd) : round(tle.epoch_cjd)+5; // TREF
// [pos, vel] = CL_tle_genEphem(tle, cjd) // ECI


// Declarations:

// Code
if (~exists("frame", "local")); frame="ECI"; end
if (~exists("ut1_tref", "local")); ut1_tref = CL__dataGetEnv("UT1_TREF"); end 
if (~exists("tt_tref", "local")); tt_tref = CL__dataGetEnv("TT_TREF"); end 

// TLE propagation options from configuration
whichconst = CL_configGet("TLE_WHICHCONST"); 
opsmode = CL_configGet("TLE_OPSMODE");


// check validity of TLE struct
CL__tle_checkValid(tle); 

if (typeof(frame) <> "string" | size(frame, "*") <> 1)
  CL__error("Invalid argument type or size for frame"); 
end

N = size(tle); 

pos = []; 
vel = []; 

if (size(cjd,1) > 1)
  CL__error("Invalid size for argument cjd"); 
end

// Nothing done if empty sizes
if (N == 0 | size(cjd, 1) == 0); return; end

// converts cjd to UTC time scale (UTC as defined at tle epoch) 
// if "native" => supposed to be already UTC
cjd_utc = cjd;
if (frame <> "native")
  cjd_epoch_tref = CL_dat_scaleConvert("UTC", "TREF", tle.epoch_cjd, ut1_tref, tt_tref);
  cjd_utc = cjd + (tle.epoch_cjd - cjd_epoch_tref); // scale = UTC at epoch
end

// propagates (result in TEME)
[pos, vel] = CL__tle_sgp4(tle, cjd_utc, whichconst, opsmode); 

// converts to desired frame (cjd: TREF)
if (frame <> "TEME" & frame <> "native")
  [pos, vel] = CL_fr_convert("TEME", frame, cjd, pos, vel, ut1_tref=ut1_tref, tt_tref=tt_tref); 
end

      
endfunction

