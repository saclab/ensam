//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_cir] = CL__ex_convMean_eckhech(osc_cir, er, mu, j1jn)
// Eckstein-Hechler propagation model - mean elements 
//
// Calling Sequence
// mean_cir = CL__ex_convMean_eckhech(osc_cir,er,mu,j1jn)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the mean orbital elements from the osculating orbital elements, using Eckstein-Hechler model. </p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// osc_cir: Osculating circular-adapted elements [sma;ex;ey;inc;raan;pom+anm] (6xN)
// er: Equatorial radius [m]. 
// mu: Gravitational constant [m^3/s^2]. 
// j1jn: Vector of zonal harmonics. (Nzx1)
// mean_cir: Mean circular-adapted elements [sma;ex;ey;inc;raan;pom+anm] (6xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// er = 6378136.3; 
// mu = 3.98600442e+14; 
// j1jn = [0; 0.001082626613; -0.000002532393; -0.000001619137; -0.000000227742; 0.000000538219]; 
// mean_cir = [7.e6; 0; 1.e-3; 1; 0.2; 0.3] 
// [mean_cir2,osc_cir] = CL__ex_propag_eckhech(0,mean_cir,0,er,mu,j1jn,%t); 
// CL__ex_convMean_eckhech(osc_cir) // => mean_cir
//

// Check number of input arguments
if (argn(2) <> 4)
  CL__error("Invalid number of input arguments"); 
end

// Mean => osculating
// NB: I is not used
function [osc_cir] = eh1_fct_osc(mean_cir, I, args)
  [X, osc_cir] = CL__ex_eckHech(0, mean_cir, 0, args.er, args.mu, args.j1jn, 2); 
endfunction 

// Arguments for eh1_fct_osc
args = struct(); 
args.er = er; 
args.mu = mu; 
args.j1jn = j1jn; 

// Compute mean elements from osculating elements
mean_cir = CL__ex_inverse("cir", osc_cir, eh1_fct_osc, args); 

endfunction
