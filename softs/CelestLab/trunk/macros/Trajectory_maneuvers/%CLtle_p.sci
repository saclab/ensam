//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// disp(tle) si taille 1

function [] = %CLtle_p(tle)

// check structure is valid
CL__tle_checkValid(tle); 

if (size(tle) == 1)
  mprintf("TLE struct contains 1 element: \n"); 
  mprintf("-> satnum = %d\n", tle.satnum); 
  mprintf("-> classif = ""%s""\n", tle.classif); 
  mprintf("-> intldesg = ""%s""\n", tle.intldesg); 
  mprintf("-> ephtype = %d\n", tle.ephtype); 
  mprintf("-> elnum = %d\n", tle.elnum); 
  scal = ""; 
  if (~isnan(tle.epoch_cjd)); 
    scal = "(" + CL_dat_cal2str(CL_dat_cjd2cal(tle.epoch_cjd)) + ")"; 
  end
  mprintf("-> epoch_cjd = %.15g  %s\n", tle.epoch_cjd, scal); 
  mprintf("-> revnum = %d\n", tle.revnum); 
  mprintf("-> ecc = %.15g\n", tle.ecc); 
  mprintf("-> inc (rad) = %.15g\n", tle.inc); 
  mprintf("-> argp (rad) = %.15g\n", tle.argp); 
  mprintf("-> raan (rad) = %.15g\n", tle.raan); 
  mprintf("-> ma (rad) = %.15g\n", tle.ma); 
  mprintf("-> n (rad/s) = %.15g\n", tle.n); 
  mprintf("-> ndot (rad/s^2) = %.15g\n", tle.ndot); 
  mprintf("-> nddot (rad/s^3) = %.15g\n", tle.nddot); 
  mprintf("-> bstar (Earth radii^-1) = %.15g\n", tle.bstar); 
  mprintf("-> desc = ""%s""\n", tle.desc); 
  mprintf("-> status = %.15g\n", tle.status); 

elseif (size(tle) == 0)
  mprintf("TLE struct contains 0 element.\n"); 

else
  mprintf("TLE struct contains %d elements: \n", size(tle)); 
  names = fieldnames(tle)';
  for name = names
    mprintf("-> %s = ...\n", name); 
  end

end

endfunction
