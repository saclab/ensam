//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [is_equal,max_err] = CL__isEqual(A,B, prec,relative,compar_crit)
// Compares objects with a relative precision
//
// Parameters
// A: First object to be compared (the result)
// B: Second object to be compared (the reference)
// prec: (optional) Relative or absolute precision to compare objects. Default is 1e-14 (1x1)
// relative: (optional, boolean) %t to compare with relative precision, %f to compare with absolute precision. Default is %t.
// compar_crit: (optional, string) "element" to compare element by element, "norm" to compare the norm of the columns. Default is "element".
// is_equal: (boolean) %t if A and B are equal, %f otherwise (1x1)
// max_err: Maximum relative or absolute error (1x1)


// Interface to call the CELESTLAB function CL_isAlmostEqual.
// The format of this function keeps the format of the old CL__isEqual
// that is replaced by CL_isAlmostEqual. So, compatibility is maintained
// with existing tests in the meanwhile of the new function deployment.
// Input arguments of the old function are converted to the arguments
// needed in the call of CL_isAlmostEqual function.

// If optional arguments do not exist, they take the default values
if (~exists("relative","local")); relative = %t; end;
if (~exists("prec","local")); prec = 1e-14; end;
if (~exists("compar_crit","local")); compar_crit = "element"; end;

// Arguments relative and prec converted to rtol and atol
if (relative) then
    rtol = prec;
    atol = 0;
else
    rtol=0;
    atol=prec;
end

// Nomenclature change
if compar_crit=="norm" then
    compar_crit="CLnorm"
end

// No max_error is computed in the new function. Set to 0
max_err=0;

// Call the new function
is_equal = CL_isAlmostEqual(A,B,rtol=rtol,atol=atol,crit=compar_crit);

endfunction
