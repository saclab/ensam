//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [val] = CL__dataGetEnv(name, index, internal)
// Retrieve the value of CelestLab data (%CL_xxx or %CL__PRIV.DATA.xxx) 
//
// Calling Sequence
// [val] = CL__dataGetEnv(name [, index, internal])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Retrieves the value of CelestLab data. </p>
// <p>name can be given as several strings: [name1, name2, ...] corresponds to name1.name2... </p>
// <p>If a variables named "%CL_" + exists, its value (or one subfield) is returned
// except if internal is %t. 
// <p>if an index is given the requested value, name should then be a 1-dimensional vector. The element
// number "index" is then returned. </p>
// <p></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// name: (string) Data name(s) - not empty (1xN)
// index: (integer, optional) Index (1xP)
// internal: (boolean, optional) %t if variable %CL_xxx is ignored (1xP)
// val: Data value(s) 
//
// Authors
// CNES - DCT/SB
//
// Examples
// muSun = CL__dataGetEnv(["body", "Sun", "mu"]);
// au = CL__dataGetEnv("au");
// body = CL__dataGetEnv("body");
// j1jn = CL__dataGetEnv("j1jn");
// j2 = CL__dataGetEnv("j1jn", 2);
//

// NOTE: error checking limited to minimize execution time. 

// Declarations:
global %CL__PRIV;

// Code:
if (~exists("index", "local"))
  index = 0; 
end
if (~exists("internal", "local"))
  internal = %f; 
end

varname = "%CL_" + name(1); 

if (exists(varname) & ~internal)
  [val, err] = evstr(varname); 
  if (err <> 0)
    CL__error("Unable to retrieve the value of the variable: " + varname);
  end
  // test for security (same size as interval data expected)
  if (~and(size(val) == size(%CL__PRIV.DATA(name(1)))))
    CL__error("Invalid size for the variable: " + varname);
  end
else
  val = %CL__PRIV.DATA(name(1)); 
end

N = size(name, "*"); 

if (N > 1)
  for k = 2 : N
    if (~isfield(val, name(k)))
      CL__error("Field not found in data structure: " + name(k)); 
    end
    val = val(name(k)); 
  end
end

if (index <> 0)
  n = size(val, "*"); 
  if (find(index > n) <> [])
    CL__error("Invalid index");
  end
  val = val(index); 
end

endfunction
