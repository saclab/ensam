//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mat,I] = CL_sortMat(mat,u, direction);
// Matrix rows or columns sorting
//
// Calling Sequence
// [mat2,I] = CL_sortMat(mat [,u ,direction]);
//
// Description
// <itemizedlist><listitem>
// <p>Sorts the rows or columns of a matrix according to the sorting order of a vector.</p>
// <p></p>
// <p>- If <b>u</b> is a column vector, the rows of <b>mat</b> will be sorted in the same order as the rows of <b>u</b>.</p>
// <p>- If <b>u</b> is a row vector, the columns of <b>mat</b> will be sorted in the same order as the columns of <b>u</b>.</p>
// <p>- If <b>u</b> is empty (or absent), the values of <b>mat</b> will be sorted if mat is a row or column vector.</p>
// <p></p>
// <p>The sorting order is determined by <b>direction</b> (increasing by default). </p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// mat: Matrix (PxN)
// u: (optional) Row or column vector (1xN or Px1) or empty. Default is empty
// direction: (string, optional) Sorting order: "i" = increasing, "d" = decreasing. Default is "i" (1x1)
// mat2: Sorted matrix (PxN)
// I: Sorting index (u_sorted = u(I)) (1xN or Px1)
//
// Authors
// CNES - DCT/SB
//
// Examples
// mat = [1,3,2,4; 1000,1010,1020,990; 10,80,15,45];
//
// // Re-arrange "mat" rows using "u" for sorting
// u = [3;4;2];
// [mat2,I] = CL_sortMat(mat,u);
// mat(I,:) - mat2  // => 0
// 
// // Sort 2nd row of "mat" decreasingly, other rows sorted accordingly
// [mat2, I] = CL_sortMat(mat, mat(2,:), "d");
// mat(:,I) - mat2  // => 0
// 
// // Sort a row or column vector (increasing order by default)
// vec = mat(:); 
// [vec2, I] = CL_sortMat(vec);
// vec(I) - vec2  // => 0


// Declarations:


// Code:
if (~exists("u","local")); u = []; end;
if (~exists("direction","local")); direction = "i"; end;

// Handle [] case
if (mat == [] & u == [])
  I = [];
  return;  // < -- RETURN !
end

// type of u: row or column vector
if (size(u,1) == 1 & size(u,2) == size(mat,2))
  type_u = "row"
  
elseif (size(u,2) == 1 & size(u,1) == size(mat,1))
  type_u = "col"
  
elseif (u == [])
  if (size(mat,1) == 1)
    type_u = "row"
    u = mat(1,:); 
  elseif (size(mat,2) == 1)
    type_u = "col"
    u = mat(:,1); 
  else
    CL__error("Invalid size for vector u");
  end
  
else
  CL__error("Invalid size for vector u");
end    
        
// u is a row vector
if (type_u == "row")
  [u,I] = gsort(u, "c", direction);
  mat = mat(:,I);
  
// u is a column vector
else
  [u,I] = gsort(u, "r", direction);
  mat = mat(I,:);
  
end


endfunction

