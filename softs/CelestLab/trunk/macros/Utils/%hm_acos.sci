//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [A] = %hm_acos(A)
// Acos for an hypermatrix = acos of each element
// NOTA: %hm_acos has no default definition in Scilab

// Declarations:

// Code:
A(:)= acos(A(:));

endfunction
