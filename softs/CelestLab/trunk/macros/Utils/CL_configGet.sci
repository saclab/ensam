//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [val] = CL_configGet(name)
// Get the value of configuration parameters
//
// Calling Sequence
// val = CL_configGet(name)
//
// Description
// <itemizedlist>
// <listitem>
// <p>This function gets the value of a configuration parameter. </p>
// <p>For compatibility reasons, the name of the parameter is case insensitive, 
// but uppercase characters should be preferred. </p>
// <p></p>
// </listitem>
// <listitem>
// <p>See <link linkend="Configuration">Configuration</link> for the list 
// of available configuration parameters.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// name: (string) Name of configuration parameter. (1x1)  
// val: (string) Value of configuration parameter. (1x1)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_configSet
//
// Examples
// CL_configGet("WARNING_MODE")
// CL_configGet("ECI_FRAME")

// Declarations:
global %CL__PRIV;

// Code:
if (typeof(name) <> "string" | size(name, "*") <> 1)
  CL__error("Invalid name (string expected)");
end

// Upper case for compatibility 
name = convstr(name, "u"); 

// NB: %CL__PRIV.PREF must have been created before.
// Done by CL__configInit in etc/celestlab.start file

// Check that %CL__PRIV is a structure and %CL__PRIV.PREF exists
if (~isstruct(%CL__PRIV)); 
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab."); 
end
if (~isfield(%CL__PRIV, "PREF"))
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab.")
end

if (~isfield(%CL__PRIV.PREF, name))
  CL__error("Requested configuration parameter does not exist")
end

val = %CL__PRIV.PREF(name);

endfunction
