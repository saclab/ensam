<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 Copyright (c) CNES  2008
 
 This software is part of CelestLab, a CNES toolbox for Scilab
 
 This software is governed by the CeCILL license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 'http://www.cecill.info'.
 -->
<!--
 * 
 * Orbit Propagation.
 * 
 -->
<refentry version="5.0-subset Scilab" xml:id="Propagation models" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 24-Jan-2013 $</pubdate>
  </info>

  <refnamediv>
    <refname>Orbit propagation models</refname>
    <refpurpose>Description of orbit propagation models available in CelestLab</refpurpose>
  </refnamediv>

  <refsection>
    <title>Overview of orbit propagation models</title>
    <para>A few orbit propagation models exist in CelestLab. Below is the complete list. </para>
    
    <itemizedlist>
    
    <listitem><para><emphasis role="bold">Keplerian  model</emphasis> (effect of central force)</para>
      <para>This is the most simple model, accounting only for the central attraction of the main body. </para>
      <para> The main asset of this model is that it can be used for elliptical and hyperbolic orbits.</para>
    </listitem>
    <para/>     
      
    <listitem><para><emphasis role="bold">Secular J2 model </emphasis> (secular effects due to J2)</para>
	    <para>This model only includes the secular effects on argument of perigee, right ascension of ascending 
      node and mean anomaly due to J2. It is valid for elliptical (including circular) orbits only. </para>
      <para>The main asset (in comparison with more accurate models) is that the model can be used for 
      any inclination (including near the critical inclination) and for equatorial orbits. </para>
    </listitem>
    <para/>  
    
    <listitem><para><emphasis role="bold">Eckstein-Hechler model </emphasis></para>
      <para>This model takes into account the effects of the zonal harmonics up to J6 (no tesseral term
      are considered).</para>
      <para>It is adapted to nearly circular orbits (eccentricity less than 0.05 and at most 0.1) and 
      does not work for inclinations close to the critical inclinations (~63.43 deg and ~116.57 deg).</para>
      <para>The mean elements include secular and long-period effects. </para>
      <para>Note: The CelestLab version of the model includes a small adjustment to the mean eccentricity vector so that
      the eccentricity of a frozen orbit remains perfectly constant. </para>
      <para/>
      <para><emphasis role="italic">References </emphasis> </para>
      <para>1) A reliable derivation of the perturbations due to any zonal and tesseral harmonics 
      of the geopotential for nearly-circular satellite orbits, M. C. Eckstein, F. Hechler, ESRO SR-13-1970, 
      ESA, Darmstadt, Germany, 1970. </para>
      <para>2) MSLIB FORTRAN 90, CNES, Volume E (me_eck_hech).</para>
    </listitem>
    <para/>  
    
    <listitem><para><emphasis role="bold">Lyddane model</emphasis></para>
	    <para>This model, based on Brouwer theory modified by Lyddane, takes into account 
      the effects of the zonal harmonics up to J5.</para>
      <para>It is useable for orbits with an eccentricity smaller than 0.9, and does not 
      work for inclinations close to the critical inclinations (~63.43 deg and 
      ~116.57 deg).</para>
		  <para/>
      <para>There are 2 models derived from Lyddane in CelestLab: </para>
      <para>- The original Lyddane model for which the mean elements are orbital elements 
      that vary secularly in time. </para>
      <para>- A second Lyddane model for which the mean elements include secular
      and long-period effects (the mean elements are then comparable with those defined by the Eckstein-Hechler
      model). </para> 
      <para/>
      <para><emphasis role="italic">References </emphasis></para>
      <para>1) Solution of the problem of artificial satellite theory without drag, 
      D. Brouwer, 1959, The Astronomical Journal - 64-1.</para>
      <para>2) Small eccentricities or inclinations in the Brouwer theory of artificial satellite motion, 
      R.H. Lyddane, 1963, The Astronomical Journal - 68-8. </para>
      <para>3) MSLIB FORTRAN 90, CNES, Volume E (me_lyddane).</para>
    </listitem>

    </itemizedlist>

    <para><emphasis role="bold">Note</emphasis>: 
    All the propagation models described above are accessible through the functions: 
    <link linkend="CL_ex_propagate">CL_ex_propagate</link>, <link linkend="CL_ex_osc2mean">CL_ex_osc2mean</link> and other dedicated ones. </para>
    <para/> 

    <para>Other orbit propagation models (available through CelestLabX) are listed below. </para>

    <itemizedlist>

    <listitem><para><emphasis role="bold">STELA propagation model</emphasis></para>
	    <para>This model, based on a semi-analytic extrapolation method, takes into account 
      the effects of Earth, Sun and Moon gravity, atmospheric drag and solar radiation
      pressure.</para>
		  <para/>
      <para>The effects of the forces are averaged over one orbit (either analytically or
      numerically) before being integrated. The integration is therefore fast and is 
      suited for long-term propagation. </para>
      <para/>
      <para><emphasis role="italic">References </emphasis></para>
      <para>1) Long term orbit propagation techniques developed in the frame of the French Space Act, H. Fraysse et al., 2011, 22nd ISSFD.</para>
 		  <para/>
      <para>The version available in CelestLab is an interface to the original Java code. 
      For more information, see the <link linkend="STELA">Stela page</link>. </para>
    </listitem>
    <para/> 
    
    <listitem><para><emphasis role="bold">SGP4/SDP4 model (TLE propagation)</emphasis></para>
	    <para>The model (named SGP4 although it is originally refered to as SGP4 or SDP4
      depending on the value of the orbit's period) is used to propagate NORAD Two Line
      Elements (TLE). </para>
      <para> The model is based on Brouwer theory. </para>
      <para> The effects of atmospheric drag are taken into account through a power density
      function. </para>
      <para/>
      <para><emphasis role="italic">References </emphasis></para>
      <para>1) Spacetrack report #3: Models for propagation of NORAD Element Sets, F.R. Hoots, R.L. Roehrich, 1980.</para>
      <para>2) Revisiting Spacetrack Report #3: Rev 2, D.A. Vallado, P. Crawford, R. Hujsack and T.S. Kelso, 2006, AIAA-2006-6753-Rev2.</para>
 		  <para/>
      <para>The version available in CelestLab is an interface to Vallado's C++ code. For more information, 
      see the <link linkend="Two Line Elements">TLE page</link>. </para>
    </listitem>
    <para/>
    
    </itemizedlist>
  </refsection>
       
  
</refentry>
