<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 Copyright (c) CNES  2008
 
 This software is part of CelestLab, a CNES toolbox for Scilab
 
 This software is governed by the CeCILL license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 'http://www.cecill.info'.
 -->
<!--
 * 
 * Introduction to CelestLab 
 * 
 -->
<refentry version="5.0-subset Scilab" xml:id="CelestLab" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 01-Dec-2012 $</pubdate>
  </info>

  <refnamediv>
    <refname>Overview of CelestLab</refname>
    <refpurpose>Overview of CelestLab</refpurpose>
  </refnamediv>

  <refsection>
    <title>Introduction to CelestLab</title>

    <inlinemediaobject>
        <imageobject>
          <imagedata fileref="images/Entete.png"></imagedata>
        </imageobject>
      </inlinemediaobject>

    <para>CelestLab is a Scilab toolbox for Space Flight Dynamics. 
    It has been developed by CNES-DCT/SB (Centre National d&#8217;Etudes Spatiales - 
    French Space Agency) for mission analysis purposes.</para>

    <para>CelestLab can be used for trajectory analysis and orbit
    design for various types of space missions. It allows engineers to perform tasks 
    such as: orbit propagation, attitude computation, elementary manoeuvre computation, 
    change of reference frame, change of coordinate systems, ...</para>

    <para>The functions are arranged into several categories:</para>

    <itemizedlist>
      <listitem> 
        <emphasis role="bold">Coordinates and frames</emphasis>:
         Definition of reference frames, time scales, types of coordinate systems,... 
      </listitem>

      <listitem>
        <emphasis role="bold">Geometry and events</emphasis>:
         Computation of orbital geometry and orbital events (ground station visibility, ...) 
      </listitem>

      <listitem>
        <emphasis role="bold">Interplanetary</emphasis>:
         Interplanetary and 3-body problem   
      </listitem>

      <listitem>        
        <emphasis role="bold">Math</emphasis>:
         Mathematical functions 
      </listitem>

      <listitem>        
        <emphasis role="bold">Models</emphasis>:
         Models for atmospheric density, Sun and Moon position,... 
      </listitem>

      <listitem>
        <emphasis role="bold">Orbit Properties</emphasis>:
         Orbit properties (Sun synchronicity, repeating ground tracks, frozen orbits, 
         drift of orbital elements, ...) 
      </listitem>

      <listitem>
        <emphasis role="bold">Relative motion</emphasis>:
         Relative trajectory analysis using Clohessy Whiltshire formalism 
      </listitem>

      <listitem>
        <emphasis role="bold">Trajectory and maneuvers</emphasis>:
         Orbit propagation and maneuver computation 
      </listitem>

      <listitem>
        <emphasis role="bold">Utilities</emphasis>:
         Miscellaneous and multi-purpose functions 
      </listitem>
      </itemizedlist>
      <para/>
  </refsection>

  <refsection>
    <title>Installing and loading CelestLab</title>

    <para> CelestLab can be installed in two different (and equivalent) ways: </para>
    <itemizedlist>
      <listitem>
        <emphasis role="bold">by using ATOMS</emphasis>:
        <para>Simply execute the command: </para>
        <para><literal>atomsInstall("celestlab");</literal></para>
      <para/>
      </listitem>

      <listitem>
        <emphasis role="bold">manually</emphasis>:
        <para> 1) Unzip/copy the celestLab directory to a location of your choice. 
         Let's call <literal><emphasis role="italic">CelestLabRoot</emphasis></literal> the new CelestLab path. </para>
        <para> If the distribution only contains source files, create the library by executing the command: </para>
        <para><literal>exec(fullfile(<emphasis role="italic">CelestLabRoot</emphasis>, "builder.sce")); </literal></para>
        <para> 2) Load CelestLab by executing the command: </para>
        <para><literal>exec(fullfile(<emphasis role="italic">CelestLabRoot</emphasis>, "loader.sce")); </literal></para>
        <para>(For convenience, you can add this latter line in the <literal>scilab.ini</literal> file.) </para>
      </listitem>
    </itemizedlist>

    <para>Loading CelestLab makes one (global) variable appear: %CL__PRIV. It is intended
	  for CelestLab internal use only. </para>
    
    <para/>

  </refsection>

  <refsection>
    <title>Using CelestLab </title>

	<para>All CelestLab functions are directly useable after CelestLab has been loaded; 
	no special action is required. You can browse the help files (in particular the "Introduction" section), 
  and run some of the examples. You can also type &#8220;<literal>help <emphasis role="italic">function
  </emphasis></literal>&#8221; (e.g. &#8220;<literal>help <emphasis role="italic">CL_dat_convert</emphasis></literal>&#8221;)  
	if you know what you're looking for (this is not specific to CelestLab). </para>
	
    <para>Another good way to start is to try the demos. Select "demos" in the CelestLab menu, then select one "topic" 
	and one "application". Each demo corresponds to one single script that can be used as an example (although some of them 
	are not that simple). </para>

   
	<para>It is also possible to bring some useful (local) variables into existence (commonly used quantities or conversion 
	factors, values of optional arguments) by just typing <literal><link linkend="CL_init">CL_init()</link></literal>.</para>
	
	<para>Note:</para>
	<itemizedlist>
    <listitem>
	  <para><literal>CL_init</literal> is only a convenience function. CelestLab is fully functional even 
          if <literal>CL_init</literal> has not been called. </para>
	  </listitem>
    <listitem><para>A call to <literal>CL_init</literal> can be added to the <literal>scilab.ini</literal> file in order to 
	            make the variables exist each time celestLab is loaded.</para>
	  </listitem>
	</itemizedlist>

  <para/>
  </refsection>

  <refsection>
    <title>CelestLab's extension module</title>

    <para> Some functions in CelestLab require CelestLab's extension module 
    to be installed. This is the case in particular for: </para>
  	<itemizedlist>
      <listitem>
      <para>all the features related to STELA (propagation model or area computation), </para>
	    </listitem>
	    <listitem>
      <para>some features related to TLEs (propagation, constants used). </para>
      </listitem>
	  </itemizedlist>

    <para> If CelestLabX has not been installed, an error is raised by the corresponding
    functions. </para>
    
    <para> CelestLabX can be downloaded from Scilab Atoms web page:   
    <ulink url="http://atoms.scilab.org/toolboxes/celestlabx">
                http://atoms.scilab.org/toolboxes/celestlabx</ulink>. </para>
    <para/>
  </refsection>

  
  <refsection>
    <title>More about CelestLab...</title>

    <para> To get the latest version of CelestLab or post comments, go to: 
           <ulink url="http://atoms.scilab.org/toolboxes/celestlab">
                       http://atoms.scilab.org/toolboxes/celestlab</ulink>. </para>
    <para/>
  </refsection>

</refentry>




