<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_ip_sphereInfluence.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_ip_sphereInfluence" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_ip_sphereInfluence</refname><refpurpose>Radius of sphere of influence</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   rsoi1 = CL_ip_sphereInfluence(mu1,mu2,dist)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the radius of the sphere of influence around one celestial body. </para>
<para>The sphere of influence (SOI) is the spherical region around a celestial body where
the gravitational effect of other bodies can be neglected. </para>
<para> This is usually used to describe the region around planets in the solar system
where it is possible to neglect the gravitational effect of the Sun. </para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>mu1:</term>
      <listitem><para> Gravitational constant of body 1 (lightest) [km^3/s^2] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>mu2:</term>
      <listitem><para> Gravitational constant of body 2 (heaviest) [km^3/s^2] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>dist:</term>
      <listitem><para> Distance between the two bodies [m] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>rsoi1:</term>
      <listitem><para> Radius of sphere of influence around body 1 [m] (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <itemizedlist>
   <listitem><para>1) Orbital Mechanics for engineering students, H. D. Curtis, chapter 8 (interplanetary trajectories)</para></listitem>
     </itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Example 1: Sphere of influence of the Earth
mu1 = CL_dataGet("body.Earth.mu");
mu2 = CL_dataGet("body.Sun.mu");
dist = 1 * CL_dataGet("au");

rsoi1 = CL_ip_sphereInfluence(mu1,mu2,dist)

// Example 2: Sphere of influence of all planets in the Solar system
bodies = CL_dataGet("body");
mu1 = [bodies.Mercury.mu, bodies.Venus.mu, bodies.Earth.mu,..
bodies.Mars.mu, bodies.Jupiter.mu, bodies.Saturn.mu,..
bodies.Uranus.mu, bodies.Neptune.mu];
mu2 = bodies.Sun.mu;
dist = [0.39, 0.73, 1, 1.52, 5.2, 9.5, 19.2, 30.7] * CL_dataGet("au");

rsoi1 = CL_ip_sphereInfluence(mu1,mu2,dist)
   ]]></programlisting>
</refsection>
</refentry>
