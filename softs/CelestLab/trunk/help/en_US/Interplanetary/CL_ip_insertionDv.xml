<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_ip_insertionDv.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_ip_insertionDv" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_ip_insertionDv</refname><refpurpose>Delta-V needed for an insertion around a planet</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [dv,ecc,tanoe] = CL_ip_insertionDv(vinf,rph,sma [,tanoh,mu])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the delta-v needed for the transfer from an hyperbolic orbit to an elliptical orbit.</para>
<para>The insertion maneuver is tangential, i.e. the delta-V is parallel to the velocity vector on the hyperbolic orbit. </para>
<para>The initial orbit is defined by its velocity at infinity (<emphasis role="bold">vinf</emphasis>) and periapsis radius (<emphasis role="bold">rph</emphasis>).</para>
<para>The final orbit is defined by its semi major axis(<emphasis role="bold">sma</emphasis>).</para>
<para>The true anomaly of the maneuver on the initial orbit (<emphasis role="bold">tanoh</emphasis>) can optionally be specified. By default tanoh=0 (meaning 'at the periapsis').</para>
<para>The planet is defined by its gravitational constant mu (default is %CL_mu)</para>
<para></para>
<para>Note: If the final orbit is cicular, the periapsis is by convention that of the
hyperbolic orbit. </para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>vinf:</term>
      <listitem><para> Velocity on hyperbolic orbit at infinity [m/s]. (1xN)</para></listitem></varlistentry>
   <varlistentry><term>rph:</term>
      <listitem><para> Periapsis radius of hyperbolic orbit [m]. (1xN)</para></listitem></varlistentry>
   <varlistentry><term>sma:</term>
      <listitem><para> Semi-major axis of target (elliptical) orbit [m]. (1xN)</para></listitem></varlistentry>
   <varlistentry><term>tanoh:</term>
      <listitem><para> (optional) True anomaly of the maneuvre (on the hyperbolic orbit) [rad]. Default value is 0. (1xN)</para></listitem></varlistentry>
   <varlistentry><term>mu:</term>
      <listitem><para> (optional) gravitational constant [m3/s2]. Default value is %CL_mu.</para></listitem></varlistentry>
   <varlistentry><term>dv:</term>
      <listitem><para> Norm of the delta-v. (1xN)</para></listitem></varlistentry>
   <varlistentry><term>ecc:</term>
      <listitem><para> Eccentricity of the final (elliptical) orbit. (1xN)</para></listitem></varlistentry>
   <varlistentry><term>tanoe:</term>
      <listitem><para> True anomaly on the elliptical orbit at the time of the maneuver [rad]. (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Insertion around Earth:
vinf = [5 , 6] * 1.e3;
eqRad = 6378.e3;
rph = [eqRad+250.e3 , eqRad+500.e3];
sma = [eqRad+350.e3 , eqRad+700.e3];
[dv, ecc, tanoe] = CL_ip_insertionDv(vinf,rph, sma)

// Insertion around Mars:
eqRad = CL_dataGet("body.Mars.eqRad");
mu = CL_dataGet("body.Mars.mu");

vinf = [5 , 6] * 1.e3;
rph = [eqRad+250.e3 , eqRad+500.e3];
sma = [eqRad+350.e3 , eqRad+700.e3];
tanoh = [0, 0.1]; // radians
[dv,ecc,tanoe] = CL_ip_insertionDv(vinf,rph,sma,tanoh,mu)

   ]]></programlisting>
</refsection>
</refentry>
