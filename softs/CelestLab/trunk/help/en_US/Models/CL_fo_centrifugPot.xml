<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_fo_centrifugPot.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_fo_centrifugPot" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_fo_centrifugPot</refname><refpurpose>Apparent potential due to the centrigural force (non-inertial frame)</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [pot] = CL_fo_centrifugPot(pos, omega)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist>
<listitem>
<para>Potential due to the centrifugal force (rotating frame). </para>
<para>By definition, the acceleration derived from the potential is +grad(potential).</para>
<para></para></listitem>
<listitem>
<para>Notes:</para>
<para>- The origin for the position vector must be the central body (there is no acceleration of the origin
of the frame considered).</para>
<para>- The coordinates frame can be any frame.</para>
<para>- omega is the angular velocity vector of the (current) frame with respect to a reference (inertial)
frame. </para>
<para></para></listitem>
<listitem>
<para>See <link linkend="Force models">Force models</link> for more details.</para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>pos:</term>
      <listitem><para> Position vector [m]. (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>omega:</term>
      <listitem><para> Angular velocity vector of current frame with respect to reference frame [rad/s]. (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>pot:</term>
      <listitem><para> Potential [m^2/s^2]. (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_fo_centrifugAcc">CL_fo_centrifugAcc</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
pos = [6378.e3; 0; 0];
omega = [0; 0; 1] * 2.e-7; // ~ 1 deg / day
CL_fo_centrifugPot(pos, omega)
   ]]></programlisting>
</refsection>
</refentry>
