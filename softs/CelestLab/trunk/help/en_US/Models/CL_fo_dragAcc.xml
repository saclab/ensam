<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_fo_dragAcc.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_fo_dragAcc" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_fo_dragAcc</refname><refpurpose>Acceleration due to atmospheric drag</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [acc] = CL_fo_dragAcc(vel, rho, coefd)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist>
<listitem>
<para>Acceleration due to atmospheric drag.</para>
<para>It is parallel to the velocity vector (opposite direction), and is proportional to the velocity
norm squared.</para>
<para></para>
<para>The drag coefficient (<emphasis role="bold">coefd</emphasis>) is defined by: coefd = cd * area / mass (with cd around 2.7).</para>
<para></para></listitem>
<listitem>
<para>Notes:</para>
<para>- The velocity vector (<emphasis role="bold">vel</emphasis>) is the velocity relative to the atmosphere.</para>
<para>- The coordinates frame can be any frame.</para>
<para></para></listitem>
<listitem>
<para>See <link linkend="Force models">Force models</link> for more details.</para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>vel:</term>
      <listitem><para> Velocity vector (relative to the atmosphere) [m/s]. (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>rho:</term>
      <listitem><para> Density of the atmosphere [kg/m^3]. (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>coefd:</term>
      <listitem><para> Drag coefficient (cd*area/mass) [m^2/kg]. (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>acc:</term>
      <listitem><para> Acceleration [m/s^2]. (3xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
mu = CL_dataGet("mu");
eqRad = CL_dataGet("eqRad");
alt = (400:20:800)*1.e3; // altitude
vel = [sqrt(mu./(eqRad+alt)); zeros(alt); zeros(alt)];
rho = CL_mod_atmUS76Density(alt);
coefd = 2.7 * 10 ./ 1000;
acc = CL_fo_dragAcc(vel, rho, coefd)
scf();
plot(alt/1000, CL_norm(acc));
   ]]></programlisting>
</refsection>
</refentry>
