<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_fo_solidTidesPot.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_fo_solidTidesPot" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_fo_solidTidesPot</refname><refpurpose>Potential due to gravity (solid tides)</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [pot] = CL_fo_solidTidesPot(pos, pos_b, mu_b [, deg, er, lcoefs])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist>
<listitem>
<para>Potential due to solid tides caused by a perturbing body.</para>
<para>By definition, the acceleration derived from the potential is +grad(potential). </para>
<para></para>
<para>The computation is based on the Love model.
The selection of the degree is done via the input parameter <emphasis role="bold">deg</emphasis>. </para>
<para></para></listitem>
<listitem>
<para>Notes: </para>
<para>- The coordinates frame can be any frame. </para>
<para>- The origin of the frame must be the central body. </para>
<para>- Explicit formulas are used for degrees 2 and 3. </para>
<para></para></listitem>
<listitem>
<para>See <link linkend="Force models">Force models</link> for more details.</para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>pos:</term>
      <listitem><para> Position vector (from central body) [m]. (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>pos_b:</term>
      <listitem><para> Position vector of perturbing body (from central body) [m]. (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>mu_b:</term>
      <listitem><para> Gravitational constant of perturbing body [m^3/s^2]. (1x1)</para></listitem></varlistentry>
   <varlistentry><term>deg:</term>
      <listitem><para> (integer, optional) Expansion degree of the Love model. Default is -1 which means "biggest possible"</para></listitem></varlistentry>
   <varlistentry><term>er:</term>
      <listitem><para> (optional) Equatorial radius of central body [m]. Default value is %CL_eqRad. (1x1)</para></listitem></varlistentry>
   <varlistentry><term>lcoefs:</term>
      <listitem><para> (optional) Tidal Love coefficients: [k1(=0); k2; ...]. Default value is %CL_tidalLoveCoefs. (Px1)</para></listitem></varlistentry>
   <varlistentry><term>pot:</term>
      <listitem><para> Potential [m^2/s^2]. (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
pos = [7000.e3; 0; 0]; // ECI
pos_moon = CL_eph_moon(CL_dat_cal2cjd(2000,3,21)); // ECI
mu_moon = CL_dataGet("body.Moon.mu");
CL_fo_solidTidesPot(pos, pos_moon, mu_moon)
   ]]></programlisting>
</refsection>
</refentry>
