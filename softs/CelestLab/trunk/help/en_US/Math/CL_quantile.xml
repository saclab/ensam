<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_quantile.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_quantile" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_quantile</refname><refpurpose>Quantiles of a data set (inverse of cumulative distribution function)</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   x = CL_quantile(X,p)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Evaluates quantiles of a data set.</para>
<para>Let F be the empirical cumulative distribution function associated with the data set <emphasis role="bold">X</emphasis>.
The quantity (x) that is computed is such that: </para>
<para>F(x) = p (p: probability in [0,1]). </para>
<para></para>
<para>The method used to calculate sample quantiles is the same as in R-project (method number 5):</para>
<para>For a data set containing N elements, quantiles are computed as follows:</para>
<para>1) The sorted values in X are considered as the (0.5/N), (1.5/N),
..., ((N-0.5)/N) quantiles.</para>
<para>2) Linear interpolation is used to compute quantiles for
probabilities between (0.5/N) and ((N-0.5)/N)</para>
<para>3) The minimum or maximum values in X are assigned to quantiles
for probabilities outside that range.</para>
<para></para>
<para>Note: This method is used in Matlab's quantile function as well.</para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>X:</term>
      <listitem><para> Data set (1xN)</para></listitem></varlistentry>
   <varlistentry><term>p:</term>
      <listitem><para> Probabilities (1xP)</para></listitem></varlistentry>
   <varlistentry><term>x:</term>
      <listitem><para> Quantiles (1xP)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <itemizedlist>
   <listitem><para>1) R: A Language and Environment for Statistical Computing; http://cran.r-project.org/doc/manuals/fullrefman.pdf.</para></listitem>
     </itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Random gaussian sample
N = 10000;
m = 0;  // mean
sd = 1; // standard deviation
X = grand(1,N,"nor",m,sd);

// Probability values
p = [0.6827, 0.9545, 0.9973];
x = CL_quantile(abs(X),p); // close to [1,2,3]*sd

   ]]></programlisting>
</refsection>
</refentry>
