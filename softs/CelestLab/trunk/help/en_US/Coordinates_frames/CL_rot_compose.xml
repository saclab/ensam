<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_rot_compose.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_rot_compose" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_rot_compose</refname><refpurpose>Matrix and angular velocity vector resulting from frame composition</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [M,omega] = CL_rot_compose(M1,omega1,dir1, M2,omega2,dir2, ... Mn,omegan,dirn)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>This function computes the matrix and angular velocity vector resulting
from several successive frame transformations.</para>
<para>For each elementary transformation, 3 entries are expected : </para>
<para>- Mk: Frame transformation matrix (framei to framej).</para>
<para>- omegak: Angular velocity vector (framej / framei with coordinates in framei.</para>
<para>- dirk: Direction of the kth transformation: 1=same direction as for the combined transformation; -1=opposite direction</para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>M1,M2,...Mn:</term>
      <listitem><para> Frame transformation matrices (3x3xN or 3x3)</para></listitem></varlistentry>
   <varlistentry><term>omega1,omega2,...omegan:</term>
      <listitem><para> Angular velocity vectors [rad/s] (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>dir1,dir2,...dirn:</term>
      <listitem><para> Directions of the transformations (1x1)</para></listitem></varlistentry>
   <varlistentry><term>M:</term>
      <listitem><para> Resulting transformation matrix (3x3xN)</para></listitem></varlistentry>
   <varlistentry><term>omega:</term>
      <listitem><para> Resulting angular velocity vector [rad/s] (3xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_rot_pvConvert">CL_rot_pvConvert</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Composition of two transformations:
// We know: frameA -> frameB
// M1 is such that: X_relative_to_frameB = M1*(X_relative_to_frameA)
// om1 = omega(frameB/frameA) with coordinates in frameA
M1 = CL_rot_angles2matrix(3, %pi/2)
om1 = [0;0;0.01];

// We know: frameC -> frameB
// M2 is such that: X_relative_to_frameB = M2*(X_relative_to_frameC)
// om2 = omega(frameB/frameC) with coordinates in frameC
M2 = CL_rot_angles2matrix(2, %pi/2)
om2 = [0;0.01;0];

// We compute: frameA -> frameC <=> frameA -> frameB -> frameC
// M is such that: X_relative_to_frameC = M*(X_relative_to_frameA)
// omega = omega(frameC/frameA) with coordinates in frameA
[M,omega] = CL_rot_compose(M1,om1,1, M2,om2,-1);
   ]]></programlisting>
</refsection>
</refentry>
