<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_oe_cir2car.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_oe_cir2car" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_oe_cir2car</refname><refpurpose>Circular to cartesian orbital elements</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [pos,vel,jacob] = CL_oe_cir2car(cir [,mu])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Converts orbital elements adapted to near-circular orbits to
cartesian orbital elements.</para>
<para>The transformation jacobian is optionally computed.</para>
<para>See <link linkend="Orbital elements">Orbital elements</link> for more details. </para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>cir:</term>
      <listitem><para> Orbital elements adapted to near-circular orbits [sma;ex;ey;inc;gom;pso] [m,rad] (6xN)</para></listitem></varlistentry>
   <varlistentry><term>mu :</term>
      <listitem><para> (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>pos:</term>
      <listitem><para> Position vector [x;y;z] [m] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>vel:</term>
      <listitem><para> Velocity vector [vx;vy;vz] [m/s] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>jacob:</term>
      <listitem><para> (optional) Transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_oe_car2cir">CL_oe_car2cir</link></para></member>
   <member><para><link linkend="CL_oe_cir2kep">CL_oe_cir2kep</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// // Example 1
cir = [7000.e3; 0.1; 0.2; 1; 2; 3];
[pos, vel] = CL_oe_cir2car(cir);

// Example 2
cir = [7000.e3; 0.1; 0.2; 1; 2; 3];
[pos, vel, jacob1] = CL_oe_cir2car(cir);
[cir2, jacob2] = CL_oe_car2cir(pos, vel);
cir2 - cir // => zero
jacob2 * jacob1 // => identity
   ]]></programlisting>
</refsection>
</refentry>
