<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_fr_bodyConvert.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_fr_bodyConvert" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_fr_bodyConvert</refname><refpurpose>Position and velocity transformation from one body frame to another</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [pos2,vel2,jacob] = CL_fr_bodyConvert(body,frame1,frame2,cjd,pos1, [,vel1,tt_tref])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Converts position and (optionally) velocity vectors relative to "frame1" to position and (optionally)
velocity vectors relative to "frame2". </para>
<para>The jacobian of the transformation is optionally computed.</para>
<para></para></listitem>
<listitem>
<para>Available frames are : </para>
<para>    'ICRS': International Celestial Reference System</para>
<para>    'BCI': Body Centered Inertial</para>
<para>    'BCF': Body Centered body Fixed</para>
<para></para>
<para>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</para>
<para></para>
<para>Available bodies are: "Mercury","Venus","Mars","Jupiter","Saturn","Uranus", "Neptune", "Sun" and "Moon" </para>
<para></para></listitem>
<listitem>
<para>Notes : </para>
<para>- The date "cjd" is relative to the TREF time scale. </para>
<para>- The frame names and body names are case sensitive</para>
<para>- If only the position needs to be converted, vel1 can be omitted or set to []. (vel2 will then be set to []).</para>
<para>- Earth is not a valid body for this function. See CL_fr_convert.</para>
<para>- GCRS (identical to ICRS in CelestLab) is also accepted as a frame name.</para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>body:</term>
      <listitem><para> (string) Name of the body. ("Mercury","Venus","Mars","Jupiter","Saturn","Uranus", "Neptune", "Sun" or "Moon") (1x1)</para></listitem></varlistentry>
   <varlistentry><term>frame1:</term>
      <listitem><para> (string) Name of the initial frame. (1x1)</para></listitem></varlistentry>
   <varlistentry><term>frame2:</term>
      <listitem><para> (string) Name of the final frame. (1x1)</para></listitem></varlistentry>
   <varlistentry><term>cjd:</term>
      <listitem><para> Modified (1950.0) julian day (Time scale: TREF) (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>pos1:</term>
      <listitem><para> Position vector in initial frame. (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>vel1:</term>
      <listitem><para> Velocity vector in initial frame. (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>tt_tref:</term>
      <listitem><para> (optional) TT-TREF [seconds]. Default is %CL_TT_TREF. (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>pos2:</term>
      <listitem><para> Position vector in final frame. (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>vel2:</term>
      <listitem><para> (optional) Velocity vector in final frame. (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>jacob:</term>
      <listitem><para> (optional) Jacobian of the transformation (6x6xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <itemizedlist>
   <listitem><para>Report of the IAU/IAG working group on cartographic coordinates and rotational elements: 2009</para></listitem>
     </itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_mod_IAUBodyAngles">CL_mod_IAUBodyAngles</link></para></member>
   <member><para><link linkend="CL_fr_bodyConvertMat">CL_fr_bodyConvertMat</link></para></member>
   <member><para><link linkend="CL_fr_convert">CL_fr_convert</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);

// ICRS to BCF
pos_EME2000 = [1e5;3e4;6e6];
vel_EME2000 = [-1e3;3e3;6e3];
[pos_BCF,vel_BCF] = CL_fr_bodyConvert("Mars","ICRS","BCI",cjd,pos_EME2000,vel_EME2000);
   ]]></programlisting>
</refsection>
</refentry>
