<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_rot_interpQuat.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_rot_interpQuat" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_rot_interpQuat</refname><refpurpose>Interpolation of quaternions</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   q = CL_rot_interpQuat(t_ref,q_ref,t [,method])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Given reference quaternions <emphasis role="bold">q_ref</emphasis> at reference times <emphasis role="bold">t_ref</emphasis>,
the function computes interpolated quaternions at times <emphasis role="bold">t</emphasis>.</para>
<para>Only one <emphasis role="bold">method</emphasis> of interpolation is avalaible at the moment: Slerp method (shortest path).</para>
<para></para>
<para>Notes:</para>
<para> - <emphasis role="bold">t_ref</emphasis> must be sorted in strictly increasing order.</para>
<para> - Output quaternions corresponding to values of <emphasis role="bold">t</emphasis> outside of [<emphasis role="bold">t_ref</emphasis>(1),<emphasis role="bold">t_ref</emphasis>($)] are set to %nan.</para>
<para></para><para></para></listitem>
<listitem>
<para>See <link linkend="Data types">Data types</link> for more details on quaternions. </para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>t_ref:</term>
      <listitem><para> Reference times (1xN with N&gt;=2)</para></listitem></varlistentry>
   <varlistentry><term>q_ref:</term>
      <listitem><para> Reference quaternions at times t_ref (size N with N&gt;=2)</para></listitem></varlistentry>
   <varlistentry><term>t:</term>
      <listitem><para> Requested times (1xP).</para></listitem></varlistentry>
   <varlistentry><term>method:</term>
      <listitem><para> (string, optional) Name of the method. Only "slerp" is available. Default is "slerp". (1x1)</para></listitem></varlistentry>
   <varlistentry><term>q :</term>
      <listitem><para> Interpolated quaternions at times t. (size P)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_rot_defQuat">CL_rot_defQuat</link></para></member>
   <member><para><link linkend="CL_rot_quatSlerp">CL_rot_quatSlerp</link></para></member>
   <member><para><link linkend="CL_rot_matrix2quat">CL_rot_matrix2quat</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
t_ref = [1:10];
q_ref = CL_rot_angles2quat([3,1,3],[t_ref;t_ref.^2;cos(t_ref)]);
t = [1.5:8.5];
q = CL_rot_interpQuat(t_ref,q_ref,t)
   ]]></programlisting>
</refsection>
</refentry>
