<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_co_car2sph.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_co_car2sph" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_co_car2sph</refname><refpurpose>Cartesian coordinates to spherical coordinates</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [pos_sph, jacob] = CL_co_car2sph(pos_car)
   [pos_sph, pos_sph_dot, jacob] = CL_co_car2sph(pos_car [,vel_car])
   [pv_sph, jacob] = CL_co_car2sph(pv_car)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Converts cartesian coordinates into spherical coordinates.</para>
<para>Optionally converts time derivatives of cartesian coordinates into time derivatives of spherical coordinates.</para>
<para/><inlinemediaobject><imageobject><imagedata fileref="spherical_coord.gif"/></imageobject></inlinemediaobject>
<para></para></listitem>
<listitem>
<para><emphasis role="bold">Notes:</emphasis></para>
<para> - Longitudes of points along the Z-axis are arbitrarily set to 0. </para>
<para> - The jacobian is computed if the corresponding output argument exists. </para>
<para> - Elements of the jacobian that cannot be computed (on the Z-axis) are set to %nan. </para>
<para> - Beware that the 3rd spherical coordinate is a radius and not an altitude. </para>
<para> - Input arguments cannot be named. </para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>pos_car :</term>
      <listitem><para> [x;y;z] Position vector in cartesian coordinates [m] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>vel_car :</term>
      <listitem><para> (optional) [vx;vy;vz] Velocity vector in cartesian coordinates [m/s] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>pos_sph :</term>
      <listitem><para> [lon;lat;r] Position vector in spherical coordinates [rad,m](3xN)</para></listitem></varlistentry>
   <varlistentry><term>pos_sph_dot :</term>
      <listitem><para> (optional) [d(lon)/dt;d(lat)/dt;d(r)/dt] Time derivatives of spherical coordinates [rad/s,m/s] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>pv_car :</term>
      <listitem><para> [pos_car; vel_car] (6xN)</para></listitem></varlistentry>
   <varlistentry><term>pv_sph :</term>
      <listitem><para> [pos_sph; pos_sph_dot] (6xN)</para></listitem></varlistentry>
   <varlistentry><term>jacob :</term>
      <listitem><para> (optional) Transformation jacobian (3x3xN) or (6x6xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <itemizedlist>
   <listitem><para>1) Mecanique Spatiale, Cnes - Cepadues Editions, Tome I, section 3.2.3 (Les reperes de l'espace et du temps, Relations entre les coordonnees)</para></listitem>
   <listitem><para>2) CNES - MSLIB FORTRAN 90, Volume T (mt_car_geoc)</para></listitem>
     </itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_co_sph2car">CL_co_sph2car</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Example 1
pos_car = [3842403.1; -5057704.6; 577780.5];
[pos_sph] = CL_co_car2sph(pos_car)

// Example 2
vel_car = [1000; 1000; 100];
[pos_sph,pos_sph_dot,jacob] = CL_co_car2sph(pos_car,vel_car)
   ]]></programlisting>
</refsection>
</refentry>
