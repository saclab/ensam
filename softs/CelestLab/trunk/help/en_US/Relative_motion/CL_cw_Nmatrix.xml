<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_cw_Nmatrix.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_cw_Nmatrix" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_cw_Nmatrix</refname><refpurpose>Clohessy-Wiltshire N transformation Matrix</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   N = CL_cw_Nmatrix(alt,delta_t [,er,mu])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the Clohessy-Wiltshire <emphasis role="bold">N</emphasis> transformation matrix.</para>
<para>The local orbital reference frame tied to the target is LVLH (See <link linkend="CL_fr_lvlhMat">CL_fr_lvlhMat</link>).</para>
<para><inlinemediaobject><imageobject><imagedata fileref="cw.gif"/></imageobject></inlinemediaobject></para>
<para></para>
<para><emphasis role="bold">Notes:</emphasis></para>
<para>In the above formula: </para>
<para>- theta is the phase angle of the target (= "reference") satellite = omega * delta_t, where
omega is the target (= "reference") mean motion. </para>
<para>- delta gamma = difference of acceleration between the chaser and the target (gamma chaser minus gamma target)
relative to LVLH local frame (of the target). </para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>alt:</term>
      <listitem><para>  Target altitude (semi-major axis minus equatorial radius) [m] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>delta_t:</term>
      <listitem><para> Time step [s] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>er:</term>
      <listitem><para> (optional) Equatorial radius [m] (default is %CL_eqRad)</para></listitem></varlistentry>
   <varlistentry><term>mu:</term>
      <listitem><para> (optional) Gravitational constant [m^3/s^2] (default is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>N:</term>
      <listitem><para> Clohessy-Wiltshire "N" transformation matrix (6x6xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_cw_Mmatrix">CL_cw_Mmatrix</link></para></member>
   <member><para><link linkend="CL_cw_propagate">CL_cw_propagate</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <itemizedlist>
   <listitem><para>1) Mecanique spatiale, CNES - Cepadues 1995, Tome II, chapter 16 (Le rendez-vous orbital)</para></listitem>
     </itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
[N]=CL_cw_Nmatrix(350000,100)

   ]]></programlisting>
</refsection>
</refentry>
