<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_man_biElliptic.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_man_biElliptic" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_man_biElliptic</refname><refpurpose>Bi-elliptic transfer - DEPRECATED</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [delta_v,dv1,dv2,dv3,anv1,anv2,anv3]=CL_man_biElliptic(ai,af,rt [,mu])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>This function is deprecated. </para>
<para>Replacement function: <link linkend="CL_man_dvBiElliptic">CL_man_dvBiElliptic</link></para>
<para></para></listitem>
<listitem>
<para>Computes the maneuvers of a bi-elliptical transfer from a circular
orbit with semi-major axis <emphasis role="bold">ai</emphasis>
to a circular orbit with semi-major axis <emphasis role="bold">af</emphasis>.</para>
<para> The apogee radius of the elliptical transfer orbit is
<emphasis role="bold">rt</emphasis>. </para>
<para><emphasis role="bold">delta-v</emphasis> is the sum of the norms
of the velocity increments.</para>
<para>Velocity increments are expressed in spherical coordinates in the "qsw" local orbital frame. </para>
<para><inlinemediaobject><imageobject><imagedata fileref="bielliptic.gif"/></imageobject></inlinemediaobject></para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>ai :</term>
      <listitem><para> Semi-major axis of initial circular orbit  [m] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>af :</term>
      <listitem><para> Semi-major axis of final circular orbit [m] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>rt :</term>
      <listitem><para> Radius at the position of the second maneuver [m] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>mu :</term>
      <listitem><para> (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>delta_v :</term>
      <listitem><para> Total |delta-V| (=|dv1|+|dv2|+|dv3]) [m/s] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>dv1:</term>
      <listitem><para> First delta-V, in spherical coordinates in the "qsw" frame [lambda;phi;|dv|] [rad,rad,m/s] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>dv2:</term>
      <listitem><para> Second delta-V, in spherical coordinates in the "qsw" frame [lambda;phi;|dv|] [rad,rad,m/s] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>dv3:</term>
      <listitem><para> Third delta-V, in spherical coordinates in the "qsw" frame [lambda;phi;|dv|] [rad,rad,m/s] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>anv1:</term>
      <listitem><para> True anomaly at the position of the 1st maneuver: initial orbit is circular so this is an arbitrary value of 0  (1xN)</para></listitem></varlistentry>
   <varlistentry><term>anv2:</term>
      <listitem><para> True anomaly at the position of the 2nd maneuver (either 0 or pi) [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>anv3:</term>
      <listitem><para> True anomaly at the position of the 3rd maneuver (either 0 or pi) [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <itemizedlist>
   <listitem><para>1) Orbital Mechanics for engineering students, H D Curtis, Chapter 6</para></listitem>
     </itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_man_hohmann">CL_man_hohmann</link></para></member>
   <member><para><link linkend="CL_man_hohmannG">CL_man_hohmannG</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// 7000 km to 98 000km through a 280 000 transfer orbit:
ai = 7000.e3;
af = 98000.e3;
rt = 280000.e3;
[delta_v,dv1,dv2,dv3,anv1,anv2,anv3]=CL_man_biElliptic(ai,af,rt)
// Check results :
kep = [ai ; 0 ; %pi/2 ; 0 ; 0 ; anv1];
kep1 = CL_man_applyDv(kep,dv1);
kep1(6) = anv2;
kep2 = CL_man_applyDv(kep1,dv2);
kep2(6) = anv3;
kep3 = CL_man_applyDv(kep2,dv3)

// Same example with a Hohmann transfer:
// more expensive !
[delta_v,dv1,dv2,anv1,anv2] = CL_man_hohmann(ai,af)
   ]]></programlisting>
</refsection>
</refentry>
