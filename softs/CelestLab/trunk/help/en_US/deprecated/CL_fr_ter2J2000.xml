<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_fr_ter2J2000.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_fr_ter2J2000" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_fr_ter2J2000</refname><refpurpose>Terrestrial frame to EME2000 (J2000) vector transformation - DEPRECATED</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [pos_J2000,vel_J2000,jacob] = CL_fr_ter2J2000(cjd,pos_ter [,vel_ter,ut1_utc,xp,yp,dPsi,dEps,conv_iers])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>This function is deprecated. </para>
<para>Replacement function: <link linkend="CL_fr_convert">CL_fr_convert</link></para>
<para></para></listitem>
<listitem>
<para>Converts position and (optionally) velocity vectors from the terrestrial ("Earth fixed") reference frame to EME2000.</para>
<para>The jacobian of the transformation is optionally computed.</para>
<para></para></listitem>
<listitem>
<para>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>cjd:</term>
      <listitem><para> modified julian day from 1950.0 (UTC) (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>pos_ter:</term>
      <listitem><para> position vector relative to terrestrial frame [m] (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>vel_ter:</term>
      <listitem><para> (optional) velocity vector relative to terrestrial frame [m/s] (3xN or 3x1)</para></listitem></varlistentry>
   <varlistentry><term>ut1_utc :</term>
      <listitem><para> (optional) ut1-utc [seconds] (default is 0) (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>xp :</term>
      <listitem><para> (optional) x polar coordinate [radians] (default is 0) (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>yp :</term>
      <listitem><para> (optional) y polar coordinate [radians] (default is 0) (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>dPsi :</term>
      <listitem><para> (optional) Nutation corrections [radians] (default is 0) (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>dEps :</term>
      <listitem><para> (optional) Nutation corrections [radians] (default is 0) (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>conv_iers :</term>
      <listitem><para> (optional) Convention IERS. Only iers 1996 (Lieske/Wahr) is implemented (default is "iers_1996")</para></listitem></varlistentry>
   <varlistentry><term>pos_J2000:</term>
      <listitem><para> position vector relative to EME2000 [m] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>vel_J2000:</term>
      <listitem><para> (optional) velocity vector relative to EME2000 [m/s] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>jacob:</term>
      <listitem><para> (optional) jacobian of the transformation (6x6xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Bibliography</title>
   <itemizedlist>
   <listitem><para>1) IERS Conventions (1996), Dennis D. McCarthy</para></listitem>
   <listitem><para>2) Explanatory Supplement to the Astronomical Almanac, Seidelman (1992)</para></listitem>
     </itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_fr_ter2J2000Mat">CL_fr_ter2J2000Mat</link></para></member>
   <member><para><link linkend="CL_fr_J20002ter">CL_fr_J20002ter</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Position
pos_ter = [7000.e3; 0; 0];
cjd = [21010 , 21011];
pos_J2000 = CL_fr_ter2J2000(cjd, pos_ter);

// Position, velocity and jacobian
pos_ter = [7000.e3; 0; 0];
vel_ter = [0; 7000; 0];
cjd = [21010 , 21011];
[pos_J2000, vel_J2000, jacob] = CL_fr_ter2J2000(cjd,[pos_ter,pos_ter],[vel_ter,vel_ter])
   ]]></programlisting>
</refsection>
</refentry>
