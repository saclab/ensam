<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_gm_stationElevation.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_gm_stationElevation" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_gm_stationElevation</refname><refpurpose>Elevation of any object as seen from ground stations - DEPRECATED</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   elev = CL_gm_stationElevation(pos_ter,stations [,er,obla])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the elevations (<emphasis role="bold">elev</emphasis>) of any object as seen from
one or several locations (<emphasis role="bold">stations</emphasis>).</para>
<para>The position of the object (<emphasis role="bold">pos_ter</emphasis>) is given in cartesian coordinates
in a frame tied to the planet (that is in the same frame as <emphasis role="bold">stations</emphasis>). </para>
<para>See below for detailed examples</para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>pos_ter:</term>
      <listitem><para> positions in the rotating frame, in cartesian coordinates [X;Y;Z] [m] (3xM)</para></listitem></varlistentry>
   <varlistentry><term>stations:</term>
      <listitem><para> stations positions in the same rotating frame, in elliptical (geodetic) coordinates [long,lat,alt] [rad,m] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>er :</term>
      <listitem><para> (optional) planet equatorial radius (default is %CL_eqRad) [m] (1x1)</para></listitem></varlistentry>
   <varlistentry><term>obla :</term>
      <listitem><para> (optional) planet oblateness (default is %CL_obla) (1x1)</para></listitem></varlistentry>
   <varlistentry><term>elev:</term>
      <listitem><para> elevations from each stations and each object position. elev(row=i,column=j) is the elevation of object j from station i. [rad] (NxM)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_ev_visibilityExtr">CL_ev_visibilityExtr</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Secular J2 propagation (in ECI frame)
cjd0 = 21915;
pas = 10.0 / 86400.0;
cjd = cjd0 : pas : cjd0+1;
kep0 = [7070.e3 ; 0.001 ; CL_deg2rad([98;90;10;15])];
kep = CL_ex_secularJ2(cjd0, kep0, cjd);

// Conversion to terrestrial frame (ECF frame)
[pos_car,vel_car] = CL_oe_kep2car(kep);
pos_ter = CL_fr_convert("ECI", "ECF", cjd, pos_car);

// Stations definition
sta1 = [CL_deg2rad(2);CL_deg2rad(70);200]; // high latitude
sta2 = [CL_deg2rad(20);CL_deg2rad(0);400]; // equator
stations = [sta1,sta2];

// Elevations computation
[elev] = CL_gm_stationElevation(pos_ter,stations);

// Elevation in function of time
scf();
plot2d((cjd-cjd0)*24,CL_rad2deg(elev(1,:)),2) //station 1
plot2d((cjd-cjd0)*24,CL_rad2deg(elev(2,:)),3) //station 2

// Visibility duration if station's mask is 5 degrees :
ind_1 = find(elev(1,:) > CL_deg2rad(5));  //station 1
ind_2 = find(elev(2,:) > CL_deg2rad(5));  //station 2
dur_1_minutes = pas*length(ind_1)*1440.0
dur_2_minutes = pas*length(ind_2)*1440.0

   ]]></programlisting>
</refsection>
</refentry>
