<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_stela_deriv.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_stela_deriv" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_stela_deriv</refname><refpurpose>Orbital elements time derivatives using STELA</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [result1, ...] = CL_stela_deriv(type_oe, cjd, mean_oe, params, res [, frame, ut1_tref, tt_tref])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist>
<listitem>
<para>Computes the orbital time derivatives due to various perturbations using STELA. </para>
<para></para>
<para>The results are described by <emphasis role="bold">res</emphasis>: </para>
<para>- "m": Derivatives of mean orbital elements (6xN) </para>
<para>- "i": Information data (structure) </para>
<para></para>
<para>The input orbital elements are internally converted to the
adequate type and frame before the call to STELA. The results are then converted to
the same type and frame. </para>
<para></para>
</listitem>
<listitem>
<para><emphasis role="bold">Warning</emphasis>:</para>
<para>The dynamic motion (that is the apparent acceleration) of the chosen reference frame with respect to
CIRS (which is the frame in which STELA integrates the motion) is not included in the derivatives.
The results must then be used with caution if the chosen frame is not ECI. This may change in future
versions. </para>
<para></para>
</listitem>
<listitem>
<para><emphasis role="bold">Notes</emphasis>:</para>
<para>- The integration time step parameter (in the "params" structure) is not used but must exist. </para>
<para></para>
</listitem>
<listitem>
<para>See the <link linkend="STELA">STELA</link> page for more details.</para>
<para></para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>type_oe:</term>
      <listitem><para> (string) Type of orbital elements: "kep", "cir", "cireq", "equin", "pv".</para></listitem></varlistentry>
   <varlistentry><term>cjd:</term>
      <listitem><para> Reference dates (CJD, time scale: TREF) (1xN)</para></listitem></varlistentry>
   <varlistentry><term>mean_oe:</term>
      <listitem><para> Mean orbital elements for the reference orbit (6xN)</para></listitem></varlistentry>
   <varlistentry><term>params:</term>
      <listitem><para> (structure) Propagation model parameters.</para></listitem></varlistentry>
   <varlistentry><term>res:</term>
      <listitem><para> (string) Wanted results: "m", "i".</para></listitem></varlistentry>
   <varlistentry><term>frame:</term>
      <listitem><para> (string, optional) Input/output frame. Default is "ECI"</para></listitem></varlistentry>
   <varlistentry><term>ut1_tref:</term>
      <listitem><para> (optional) UT1-TREF [seconds]. Default is %CL_UT1_TREF (1x1)</para></listitem></varlistentry>
   <varlistentry><term>tt_tref:</term>
      <listitem><para> (optional) TT-TREF [seconds]. Default is %CL_TT_TREF (1x1)</para></listitem></varlistentry>
   <varlistentry><term>result1, ...:</term>
      <listitem><para> Time derivatives or information data</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB/MS</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Generate reference orbit (Sun-synchronous, MLTAN=14h, frame: ECI)
kep0 = [7.e6; 1.136e-3; 1.7085241; %pi/2; 0; 0];
cjd0 = 20000;
cjd = cjd0 + (0:365); // days, time scale: TREF
kep = CL_ex_propagate("eckhech", "kep", cjd0, kep0, cjd, "m");

// STELA model parameters (default values)
params = CL_stela_params();

// Time derivatives
[dkepdt, info] = CL_stela_deriv("kep", cjd, kep, params, ["m", "i"]);

// Plot inclination derivative (deg/year)
scf();
plot(cjd, dkepdt(3,:) * (180/%pi) * 86400 * 365.25);
   ]]></programlisting>
</refsection>
</refentry>
