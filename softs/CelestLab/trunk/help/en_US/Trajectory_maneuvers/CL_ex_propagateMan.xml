<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_ex_propagateMan.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_ex_propagateMan" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_ex_propagateMan</refname><refpurpose>Orbit propagation with maneuvers (all analytical models)</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [result1, result2] = CL_ex_propagateMan(mod,type_oe,t1,mean_oe_t1,t2,tman,dvman,dvframe,res [,er,mu,j1jn])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Propagates orbital elements using one analytical model while taking maneuvers (velocity increments)
into account. </para>
<para></para>
<para>The available propagation models are: </para>
<para>"central": Central force (osculating elements = mean elements) </para>
<para>"j2sec": Secular effects of J2 (osculating elements = mean elements by convention)</para>
<para>"lydsec": Lyddane (mean elements include secular effects only) </para>
<para>"lydlp": Lyddane (mean elements include secular and long period effects) </para>
<para>"eckhech": Eckstein-Hechler  (mean elements include secular and long period effects) </para>
<para></para>
<para>Maneuvers are defined by: </para>
<para>- <emphasis role="bold">tman</emphasis>: time of the maneuver.</para>
<para>- <emphasis role="bold">dvman</emphasis> = [delta-Vx;delta-Vy;delta-Vz]: delta-V components in the chosen frame. </para>
<para>- <emphasis role="bold">dvframe</emphasis>: name of the frame in which the components are given. The name can be
any name compatible with <link linkend="CL_fr_locOrbMat">CL_fr_locOrbMat</link>, or "inertial". </para>
<para></para></listitem>
<listitem>
<para><emphasis role="bold">Notes</emphasis>:</para>
<para>- If one maneuver occurs at one of the final times, the output orbital elements
include the effect of the maneuver at that time.</para>
<para>- The final times as well as the maneuver times should be sorted in increasing order.</para>
<para>- t1 should be smaller than tman(k) for any k.</para>
<para>- t1 should be smaller than t2(k) for any k.</para>
<para></para></listitem>
<listitem>
<para>See <link linkend="Propagation models">Propagation models</link> for more details.</para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>mod:</term>
      <listitem><para> (string) Model name: "central", "j2sec", "lydsec", "lydlp", "eckhech". (1x1)</para></listitem></varlistentry>
   <varlistentry><term>type_oe:</term>
      <listitem><para> (string) Type of orbital elements used for input/output: "kep", "cir", "cireq" or "equin" (1x1)</para></listitem></varlistentry>
   <varlistentry><term>t1:</term>
      <listitem><para> Initial time [days] (1x1)</para></listitem></varlistentry>
   <varlistentry><term>mean_oe_t1:</term>
      <listitem><para> Mean orbital elements at time t1 (6x1 or 6xN)</para></listitem></varlistentry>
   <varlistentry><term>t2:</term>
      <listitem><para> Final time [days] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>tman:</term>
      <listitem><para> Maneuver times [days] (1xP)</para></listitem></varlistentry>
   <varlistentry><term>dvman:</term>
      <listitem><para> Delta-V with components in specified frame [dvx;dvy;dvz] [m/s] (3xP)</para></listitem></varlistentry>
   <varlistentry><term>dvframe:</term>
      <listitem><para> (string) Frame for the DV components: any local orbital frame or "inertial". (1x1)</para></listitem></varlistentry>
   <varlistentry><term>res:</term>
      <listitem><para> (string) Type of output (mean or osculating): "m", "o", "mo", "om"  (1x1)</para></listitem></varlistentry>
   <varlistentry><term>er:</term>
      <listitem><para> (optional) Equatorial radius [m] (default is %CL_eqRad)</para></listitem></varlistentry>
   <varlistentry><term>mu:</term>
      <listitem><para> (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>j1jn:</term>
      <listitem><para> (optional) Vector of zonal harmonics. Default is %CL_j1jn (Nz x 1)</para></listitem></varlistentry>
   <varlistentry><term>result1, result2:</term>
      <listitem><para> Mean or osculating orbital elements at t2 (6xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_ex_propagate">CL_ex_propagate</link></para></member>
   <member><para><link linkend="CL_fr_locOrbMat">CL_fr_locOrbMat</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Initial state (mean keplerian elements)
t0 = 0; // days
kep0 = [8.e6; 0.2; 60*(%pi/180); 0; 0; 0];
// Final times
t = t0 + (0 : 0.001 : 3.5); // days
// Maneuvers
tman = t0 + [1.5, 2.5]; // days
dvman = [[10; 0; 0], [0; 0; 150]];
dvframe = "tnw";

// Propagation including maneuver effects (result = osculating elements)
kep = CL_ex_propagateMan("lydsec", "kep", t0, kep0, t, tman, dvman, dvframe, "o");

// Plot semi-major axis (blue) and inclination (red)
scf();
plot(t, kep(1,:) / 1000, "b");
scf();
plot(t, kep(3,:) * (180/%pi), "r");
   ]]></programlisting>
</refsection>
</refentry>
