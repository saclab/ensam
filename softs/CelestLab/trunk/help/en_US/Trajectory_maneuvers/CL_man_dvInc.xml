<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_man_dvInc.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_man_dvInc" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_man_dvInc</refname><refpurpose>Inclination maneuver (elliptical orbits)</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [deltav,dv,anv] = CL_man_dvInc(ai,ei,inci,pomi,incf [,posman,icheck,mu])
   man = CL_man_dvInc(ai,ei,inci,pomi,incf [,posman,icheck,mu], res="s")
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the velocity increment needed to change the inclination.</para>
<para> The maneuver consists in making the orbit rotate around the line of nodes, thus changing the velocity direction at one of the nodes. </para>
<para>The output argument <emphasis role="bold">dv</emphasis> is the velocity increment vector in the "qsw" local orbital frame. </para>
<para><emphasis role="bold">anv</emphasis> is the true anomaly at the maneuver position.</para>
<para>The optional flag <emphasis role="bold">posman</emphasis> can be used to define the position of the maneuver (maneuver at ascending node, descending node, or "best choice"). </para>
<para>The optional flag <emphasis role="bold">icheck</emphasis> is used to enforce no checking
on the final inclination value. If the targetted inclination is less
than 0 or more than pi, the ascending node will rotate 180 degrees.
If icheck is true, the right ascension of the ascending node is not changed by the maneuver. </para>
<para>If the argument <emphasis role="bold">res</emphasis> is present and is equal to "s", all the output data are returned in a structure. </para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>ai :</term>
      <listitem><para> Semi major-axis [m] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>ei :</term>
      <listitem><para> Eccentricity (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>inci :</term>
      <listitem><para> Initial inclination [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>pomi :</term>
      <listitem><para> Argument of periapsis [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>incf :</term>
      <listitem><para> Final inclination [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>posman :</term>
      <listitem><para> (optional) Flag specifying the position of the maneuver: 1 or "an" -&gt; ascending node, -1 or "dn" -&gt; descending node, 0 or "opt" -&gt; minimal |dv|. Default is at the ascending node. (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>icheck:</term>
      <listitem><para> (optional, boolean) Flag specifying if incf must be checked in the standard range for inclination values ([0, pi]). Default is %t. (1x1)</para></listitem></varlistentry>
   <varlistentry><term>mu :</term>
      <listitem><para> (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>res :</term>
      <listitem><para> (string, optional) Type of output: "d" or "s" for . Default is "d".</para></listitem></varlistentry>
   <varlistentry><term>deltav :</term>
      <listitem><para> Norm of velocity increment. [m/s] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>dv :</term>
      <listitem><para> Velocity increment (in cartesian coordinates) in the "qsw" local frame [m/s] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>anv :</term>
      <listitem><para> True anomaly at maneuver position [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>man :</term>
      <listitem><para> Structure containing all the output data.</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_man_dvIncRaanCirc">CL_man_dvIncRaanCirc</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
ai = 7200.e3;
ei = 0.1;
inci = 1;
pomi = 1.4;
incf = 1.1;
[deltav,dv,anv] = CL_man_dvInc(ai,ei,inci,pomi,incf,posman="an")

// Check results:
anm = CL_kp_v2M(ei,anv);
kep = [ai ; ei ; inci ; pomi ; 0 ; anm];
kep1 = CL_man_applyDvKep(kep,dv)
   ]]></programlisting>
</refsection>
</refentry>
