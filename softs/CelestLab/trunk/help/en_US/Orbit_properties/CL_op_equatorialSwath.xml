<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_op_equatorialSwath.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_op_equatorialSwath" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_op_equatorialSwath</refname><refpurpose>Intersection of swath with equator (circular orbit and field of view)</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   ang2 = CL_op_equatorialSwath(cmd,sma,inc,ang1 [,er,mu,j2,rotr_pla])
   longitude_span = CL_op_equatorialSwath("cen2dlon",sma,inc,center_angle [,er,mu,j2,rotr_pla])
   center_angle = CL_op_equatorialSwath("dlon2cen",sma,inc,longitude_span [,er,mu,j2,rotr_pla])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the (half) length of the portion of the equator seen by the satellite
as the satellite crosses the equator.</para>
<para>There are 2 possibilities.</para>
<para>- The field of view (defined by a center angle) is known: the span in longitude
is then computed
(cmd should be "cen2dlon").</para>
<para>- The span in longitude is known: the field of view (defined by a center angle)
is then computed (cmd should be "dlon2cen").</para>
<para></para></listitem>
<listitem>
<para><emphasis role="bold">Notes:</emphasis></para>
<para>- The planet is assumed spherical, and the orbit circular. </para>
<para>- The effects of J2 and Earth rotation are taken into account. </para>
<para>- The input center angle and span in longitude should be in [0,pi] and [0, 2*pi] respectively. </para>
<para></para></listitem>
<listitem>
<para><emphasis role="bold">Warning:</emphasis></para>
<para>There are two temporary limitations: </para>
<para>- Center angle must be less than pi/2. </para>
<para>- Semi major axis and inclination must be such that there is less
than one revolution of the satellite per (orbital) day. </para>
<para> Otherwise, the value %nan is returned. </para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>cmd :</term>
      <listitem><para> (string) Action required: "cen2dlon" or "dlon2cen"</para></listitem></varlistentry>
   <varlistentry><term>sma :</term>
      <listitem><para> Semi major axis [m] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>inc :</term>
      <listitem><para> Inclination [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>ang1 :</term>
      <listitem><para> Input angle: center angle or half longitude span [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>er :</term>
      <listitem><para> (optional) Equatorial radius [m] (default is %CL_eqRad)</para></listitem></varlistentry>
   <varlistentry><term>mu :</term>
      <listitem><para> (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)</para></listitem></varlistentry>
   <varlistentry><term>j2 :</term>
      <listitem><para> (optional) Second zonal harmonic (default is %CL_j1jn(2))</para></listitem></varlistentry>
   <varlistentry><term>rotr_pla :</term>
      <listitem><para> (optional) Rotation rate of the planet (default is %CL_rotrBody)</para></listitem></varlistentry>
   <varlistentry><term>ang2 :</term>
      <listitem><para> Output angle (half longitude span or center angle) [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_gm_visiParams">CL_gm_visiParams</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
sma = 10000.e3;
inc = 0.5; // rad
dlon = CL_op_equatorialSwath("cen2dlon", sma, inc, 0.3)
cen = CL_op_equatorialSwath("dlon2cen", sma, inc, dlon)
   ]]></programlisting>
</refsection>
</refentry>
