<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_kp_anomConvertCir.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_kp_anomConvertCir" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_kp_anomConvertCir</refname><refpurpose>Conversion of anomaly (circular or equinoctical elements)</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [pso2] = CL_kp_anomConvertCir(type_anom1, type_anom2, ex, ey, pso1)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Converts anomaly from one type to another, for circular or equinoctial orbital elements</para>
<para></para>
<para>The components of the eccentricity vector <emphasis role="bold">ex</emphasis>, <emphasis role="bold">ey</emphasis> are of the form: </para>
<para> ex = e*cos(A) </para>
<para> ey = e*sin(A) </para>
<para>Where: </para>
<para> A = w (argument of perigee) for circular orbital elements, or </para>
<para> A = W + w (RAAN + argument of perigee) for equinoctial orbital elements. </para>
<para></para>
<para>The "pso" quantity that can be converted is: </para>
<para>- pso = A + M if type_anom = "M" </para>
<para>- pso = A + v if type_anom = "v" </para>
<para>- pso = A + E if type_anom = "E" </para>
<para></para>
<para>Note: </para>
<para>- Only elliptical orbits are handled (eccentricity &lt; 1) </para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>type_anom1:</term>
      <listitem><para> (string) Type of input anomaly (1x1)</para></listitem></varlistentry>
   <varlistentry><term>type_anom2:</term>
      <listitem><para> (string) Type of output anomaly (1x1)</para></listitem></varlistentry>
   <varlistentry><term>ex:</term>
      <listitem><para> x component of eccentricity vector (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>ey:</term>
      <listitem><para> y component of eccentricity vector (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>pso1:</term>
      <listitem><para> Input anomaly (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>pso2:</term>
      <listitem><para> Output anomaly (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Mean argument of latitude to true argument of latitude:
ex = 0.1;
ey = 0.2;
psoM = [-0.1, 10.2]; // rad
psov = CL_kp_anomConvertCir("M", "v", ex, ey, psoM)

// Consistency test:
psoM - CL_kp_anomConvertCir("v", "M", ex, ey, psov) // => 0
   ]]></programlisting>
</refsection>
</refentry>
