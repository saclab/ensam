<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_gm_intersectPlanes.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_gm_intersectPlanes" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_gm_intersectPlanes</refname><refpurpose>Intersection of 2 orbit planes</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [pso1,pso2,inters] = CL_gm_intersectPlanes(inc1,raan1,inc2,raan2)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the argument of latitude at the intersection of 2 orbit planes.</para>
<para>The orbit planes are defined by their inclinations and right ascensions of ascending node </para>
<para></para></listitem>
<listitem>
<para><emphasis role="bold">Notes:</emphasis></para>
<para> - Only one solution is computed (such that pso1 and pso2
belong to [0, pi]). The arguments of latitude for the second solution are pso1+pi and pso2+pi respectively.</para>
<para> - If the orbits are circular, then <emphasis role="bold">pso1</emphasis> and
<emphasis role="bold">pso2</emphasis> define the positions in the orbits where the orbit paths intersect. </para>
<para> - If the planes are identical, the number of intersections is infinite.
The output argument (<emphasis role="bold">inters</emphasis>) is then set to 0, otherwise it is equal to 1.</para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>inc1:</term>
      <listitem><para> Inclination of orbit 1 [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>raan1:</term>
      <listitem><para> Right ascension of ascending node of orbit 1 [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>inc2:</term>
      <listitem><para> Inclination of orbit 2 [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>raan2:</term>
      <listitem><para> Right ascension of ascending node of orbit 2 [rad] (1xN or 1x1)</para></listitem></varlistentry>
   <varlistentry><term>pso1:</term>
      <listitem><para> Argument of latitude in orbit 1 where the 2 planes intersect [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>pso2:</term>
      <listitem><para> Argument of latitude in orbit 2 where the 2 planes intersect [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>inters:</term>
      <listitem><para> Flag indicating if the planes intersect (inters = 1) or is there exists an infinity of intersections (inters=0) (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
inc1 = 1; // rad
raan1 = 2;
inc2 = 0.1;
raan2 = 1.9;
[pso1,pso2] = CL_gm_intersectPlanes(inc1,raan1,inc2,raan2)

   ]]></programlisting>
</refsection>
</refentry>
