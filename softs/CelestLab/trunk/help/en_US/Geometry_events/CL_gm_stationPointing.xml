<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_gm_stationPointing.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_gm_stationPointing" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_gm_stationPointing</refname><refpurpose>Spherical coordinates of object in local frame of location.</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   [res1,res2,...] = CL_gm_stationPointing(stations,pos [, res,er,obla])
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the spherical coordinates (elevation, West azimuth, distance)
of any object as seen from one or several locations (<emphasis role="bold">stations</emphasis>). The locations are defined by
their elliptical ("geodetic") coordinates. </para>
<para>The positions (<emphasis role="bold">pos</emphasis>) are given in cartesian coordinates in the same frame as <emphasis role="bold">stations</emphasis>. </para>
<para></para>
<para>The outputs can be described by the <emphasis role="bold">res</emphasis> argument: </para>
<para> - "azim" : azimuth (positive towards West)</para>
<para> - "elev" : elevation </para>
<para> - "dist" : distance </para>
<para> - "s" : structure containing the fields "azim", "elev", "dist" </para>
<para> - "l" : list of spherical coordinates for each station </para>
<para></para>
<para></para></listitem>
<listitem>
<para>See <link linkend="Local frames">Local frames</link> for more details on the definition of local frames.</para>
<para></para></listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>stations:</term>
      <listitem><para> Stations positions in elliptical (geodetic) coordinates [long,lat,alt] [rad,m] (3xP)</para></listitem></varlistentry>
   <varlistentry><term>pos:</term>
      <listitem><para> Positions of object in the same frame as "stations", in cartesian coordinates [m] (3xN)</para></listitem></varlistentry>
   <varlistentry><term>res:</term>
      <listitem><para> (string) vector of names: "azim", "elev", "dist", "s" or "l". Default is ["azim", "elev", "dist"] (1xQ)</para></listitem></varlistentry>
   <varlistentry><term>er :</term>
      <listitem><para> (optional) Planet's equatorial radius. Default is %CL_eqRad [m] (1x1)</para></listitem></varlistentry>
   <varlistentry><term>obla :</term>
      <listitem><para> (optional) Planet's oblateness. Default is %CL_obla (1x1)</para></listitem></varlistentry>
   <varlistentry><term>res1, res2...:</term>
      <listitem><para> results: Elevation [rad] (PxN), azimuth [rad] (PxN), distance [m] (PxN), structure (PxN fields) or list</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_gm_stationVisiLocus">CL_gm_stationVisiLocus</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// satellite positions
pos = [[7.e6; 0; 0], [7.e6; 1.e3; 0], [7.e6; 2.e3; 0]];
// ground stations (geodetic coordinates)
stations = [[0;0;0], [0.1; 0.2; 0]]
// Coordinates in topocentric North frame (size of results: 2x3)
[azim,elev,dist] = CL_gm_stationPointing(stations, pos);
// List of 3x1 coordinates
l = CL_gm_stationPointing(stations, pos, res="l");
   ]]></programlisting>
</refsection>
</refentry>
