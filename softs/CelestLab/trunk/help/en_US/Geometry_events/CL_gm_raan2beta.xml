<?xml version="1.0" encoding="UTF-8"?>

<!--
 * 
 * This help file was generated from CL_gm_raan2beta.sci using help_from_sci2().
 * 
 -->

<refentry version="5.0-subset Scilab" xml:id="CL_gm_raan2beta" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">

  <info>
    <pubdate>$LastChangedDate: 2017/02/06 $</pubdate>
  </info>

  <refnamediv>
    <refname>CL_gm_raan2beta</refname><refpurpose>Right ascension of ascending node to beta angle</refpurpose>
  </refnamediv>



<refsynopsisdiv>
   <title>Calling Sequence</title>
   <synopsis>
   betaa = CL_gm_raan2beta(inc,raan,alpha_sun,delta_sun)
   
   </synopsis>
</refsynopsisdiv>

<refsection>
   <title>Description</title>
<itemizedlist><listitem>
<para>Computes the beta angle (angle between the orbit plane and the Sun direction) form
the right ascension of the ascending node. </para>
<para></para>
<para> <inlinemediaobject><imageobject><imagedata fileref="beta.gif"/></imageobject></inlinemediaobject></para>
<para></para></listitem>
<listitem>
<para><emphasis role="bold">Note:</emphasis></para>
<para>- The beta angle is a signed quantity. If it is positive then the Sun direction is less
than 90 degrees from the orbit's angular momentum vector (vector perpendicular to the
orbit plane and oriented according to right hand rule). </para>
<para>- Be careful about the order of arguments: inclination, raan... </para>
</listitem>
</itemizedlist>
</refsection>

<refsection>
   <title>Parameters</title>
   <variablelist>
   <varlistentry><term>inc:</term>
      <listitem><para> Orbit inclination [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>raan:</term>
      <listitem><para> Orbit right ascension of ascending node [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>alpha_sun:</term>
      <listitem><para> Sun right ascension [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>delta_sun:</term>
      <listitem><para> Sun declination [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term>betaa:</term>
      <listitem><para> Beta angle [rad] (1xN)</para></listitem></varlistentry>
   <varlistentry><term></term>
      <listitem><para> </para></listitem></varlistentry>
   </variablelist>
</refsection>

<refsection>
   <title>See also</title>
   <simplelist type="inline">
   <member><para><link linkend="CL_gm_beta2raan">CL_gm_beta2raan</link></para></member>
   </simplelist>
</refsection>

<refsection>
   <title>Authors</title>
   <itemizedlist><listitem>
   <para>CNES - DCT/SB</para>
     </listitem></itemizedlist>
</refsection>

<refsection>
   <title>Examples</title>
   <programlisting role="example"><![CDATA[
// Get alpha_sun and delta_sun
cjd = 20050;
pos_sun = CL_eph_sun(cjd);
pos_sun_sph = CL_co_car2sph(pos_sun);
alpha_sun = pos_sun_sph(1);
delta_sun = pos_sun_sph(2);
raan = %pi/4;
inc = CL_deg2rad(98.7);
betaa = CL_gm_raan2beta(inc,raan,alpha_sun,delta_sun)
   ]]></programlisting>
</refsection>
</refentry>
