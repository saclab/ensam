#ifdef __cplusplus
extern "C" {
#endif
#include <mex.h> 
#include <sci_gateway.h>
#include <api_scilab.h>
#include <MALLOC.h>
static int direct_gateway(char *fname,void F(void)) { F();return 0;};
extern Gatefunc sci_CLx_tle_intern_sgp4;
extern Gatefunc sci_CLx_tle_intern_getconst;
extern Gatefunc sci_CLx_tle_intern_unkozai;
extern Gatefunc sci_CLx_intern_installed;
static GenericTable Tab[]={
  {(Myinterfun)sci_gateway,sci_CLx_tle_intern_sgp4,"CLx_tle_intern_sgp4"},
  {(Myinterfun)sci_gateway,sci_CLx_tle_intern_getconst,"CLx_tle_intern_getconst"},
  {(Myinterfun)sci_gateway,sci_CLx_tle_intern_unkozai,"CLx_tle_intern_unkozai"},
  {(Myinterfun)sci_gateway,sci_CLx_intern_installed,"CLx_intern_installed"},
};
 
int C2F(celestlabx_cpp)()
{
  Rhs = Max(0, Rhs);
  if (*(Tab[Fin-1].f) != NULL) 
  {
     if(pvApiCtx == NULL)
     {
       pvApiCtx = (StrCtx*)MALLOC(sizeof(StrCtx));
     }
     pvApiCtx->pstName = (char*)Tab[Fin-1].name;
    (*(Tab[Fin-1].f))(Tab[Fin-1].name,Tab[Fin-1].F);
  }
  return 0;
}
#ifdef __cplusplus
}
#endif
