// This file is released under the 3-clause BSD license. See COPYING-BSD.

function builder_gateway_cpp()

    // If loaded, unload gateway before build it
    [loaded, id] = c_link("celestlabx_cpp");
    if loaded then
        ulink(id);
    end

    gateway_cpp_dir = get_absolute_file_path("builder_gateway_cpp.sce");

    table_functions = ..
      ["CLx_tle_intern_sgp4", "sci_CLx_tle_intern_sgp4"; ..
       "CLx_tle_intern_getconst", "sci_CLx_tle_intern_getconst"; ..
       "CLx_tle_intern_unkozai", "sci_CLx_tle_intern_unkozai"; ..
       "CLx_intern_installed", "sci_CLx_intern_installed"];

    src_functions = ..
      ["sci_CLx_tle_intern_sgp4.cpp", ..
       "sci_CLx_tle_intern_getconst.cpp", ..
       "sci_CLx_tle_intern_unkozai.cpp", ..
       "sci_CLx_intern_installed.cpp"];

    includes_src_cpp = "-I""" + gateway_cpp_dir + "../../src/cpp""";

    tbx_build_gateway( ..
      "celestlabx_cpp", ..
      table_functions, ..
      src_functions, ..
      gateway_cpp_dir, ..
      "../../src/cpp/libCLx_tle", ..
      "", ..
      includes_src_cpp);

endfunction

builder_gateway_cpp();
clear builder_gateway_cpp;
