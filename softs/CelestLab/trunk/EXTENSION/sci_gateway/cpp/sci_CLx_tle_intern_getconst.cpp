//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

extern "C"
{
/* ==================================================================== */
  #include "api_scilab.h"
  #include "Scierror.h"
  #include "MALLOC.h"
  #include <localization.h>
  #include "tle_interf.h"

  static int getInputInteger(int argPos, int *piValue, char *fname);
  static int setOutputDouble(int argPos, double dValue, char *fname);
  static int setOutputInteger(int argPos, int iValue, char *fname);
/* ==================================================================== */
  int sci_CLx_tle_intern_getconst(char *fname, int fname_len)
  {
    int nconst = 0;
    double er = 0;
    double mu = 0;
    double j2 = 0;
    double j3 = 0;
    double j4 = 0;
    int err = 0;

    /* Check the number of input argument */
    CheckInputArgument(pvApiCtx, 1, 1);

    /* Check the number of output argument */
    CheckOutputArgument(pvApiCtx, 5, 6);

    // Input arguments
    getInputInteger(1, &nconst, fname);

    // Call
    CLx_tle_getconst(&nconst, &er, &mu, &j2, &j3, &j4, &err);

    // Outtput arguments
    if (setOutputDouble(1, er, fname))
        return 0;

    if (setOutputDouble(2, mu, fname))
        return 0;

    if (setOutputDouble(3, j2, fname))
        return 0;

    if (setOutputDouble(4, j3, fname))
        return 0;

    if (setOutputDouble(5, j4, fname))
        return 0;

    if (setOutputInteger(6, err, fname))
        return 0;

    return 0;
  }
} /* extern "C" */
/* ==================================================================== */

static int getInputInteger(int argPos, int *piValue, char *fname)
{
    int *piAddressVar = NULL;
    SciErr sciErr;

    sciErr = getVarAddressFromPosition(pvApiCtx, argPos, &piAddressVar);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (!isIntegerType(pvApiCtx, piAddressVar))
    {
        Scierror(999, "%s: Wrong type for input argument #%d: An integer expected.\n", fname, argPos);
        return 1;
    }
    if (getScalarInteger32(pvApiCtx, piAddressVar, piValue))
    {
        Scierror(999, "%s: Wrong size for input argument #%d: A scalar expected.\n", fname, argPos);
        return 1;
    }
    return 0;
}

static int setOutputDouble(int argPos, double dValue, char *fname)
{
    if (createScalarDouble(pvApiCtx, nbInputArgument(pvApiCtx) + argPos, dValue))
    {
        Scierror(999, _("%s: Memory allocation error.\n"), fname);
        return 1;
    }
    AssignOutputVariable(pvApiCtx, argPos) = nbInputArgument(pvApiCtx) + argPos;
    return 0;
}

static int setOutputInteger(int argPos, int iValue, char *fname)
{
    if (createScalarInteger32(pvApiCtx, nbInputArgument(pvApiCtx) + argPos, iValue))
    {
        Scierror(999, _("%s: Memory allocation error.\n"), fname);
        return 1;
    }
    AssignOutputVariable(pvApiCtx, argPos) = nbInputArgument(pvApiCtx) + argPos;
    return 0;
}