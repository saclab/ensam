//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

extern "C"
{
/* ==================================================================== */
#include "api_scilab.h"
#include "Scierror.h"
#include "MALLOC.h"
#include <localization.h>
#include "tle_interf.h"

static int getInputMatrixDouble(int argPos, double **pdValues, char *fname);
static int getInputInteger(int argPos, int *piValue, char *fname);
static int setOutputMatrixInteger(int argPos, int *piValues, int iRows, int iCols, char *fname);
static int setOutputMatrixDouble(int argPos, double *pdValues, int iRows, int iCols, char *fname);
static int setOutputInteger(int argPos, int iValue, char *fname);
static void freeMatrices(double* pos, double* vel, double* kep, int* status);
/* ==================================================================== */
int sci_CLx_tle_intern_sgp4(char *fname, int fname_len)
{
    int opt = 0;
    double *cjd = NULL;
    double *ecc = NULL;
    double *inc = NULL;
    double *argp = NULL;
    double *raan = NULL;
    double *ma = NULL;
    double *mm = NULL;
    double *bstar = NULL;
    int nconst = 0;
    int nopsmode = 0;
    double *delta_t = NULL;
    int nb = 0;
    double *pos = NULL;
    double *vel = NULL;
    double *kep = NULL;
    int *status = NULL;
    int err = 0;

    /* Check the number of input argument */
    CheckInputArgument(pvApiCtx, 13, 13);

    /* Check the number of output argument */
    CheckOutputArgument(pvApiCtx, 3, 5);

    // Input arguments
    if (getInputInteger(1, &opt, fname))
        return 0;

    if (getInputMatrixDouble(2, &cjd, fname))
        return 0;

    if (getInputMatrixDouble(3, &ecc, fname))
        return 0;

    if (getInputMatrixDouble(4, &inc, fname))
        return 0;

    if (getInputMatrixDouble(5, &argp, fname))
        return 0;

    if (getInputMatrixDouble(6, &raan, fname))
        return 0;

    if (getInputMatrixDouble(7, &ma, fname))
        return 0;

    if (getInputMatrixDouble(8, &mm, fname))
        return 0;

    if (getInputMatrixDouble(9, &bstar, fname))
        return 0;

    if (getInputInteger(10, &nconst, fname))
        return 0;

    if (getInputInteger(11, &nopsmode, fname))
        return 0;

    if (getInputMatrixDouble(12, &delta_t, fname))
        return 0;

    if (getInputInteger(13, &nb, fname))
        return 0;

    pos = (double*) MALLOC(3 * nb * sizeof(double));
    vel = (double*) MALLOC(3 * nb * sizeof(double));
    kep = (double*) MALLOC(6 * nb * sizeof(double));
    status = (int*) MALLOC(nb * sizeof(int));
    CLx_tle_sgp4(&opt, cjd, ecc, inc, argp, raan, ma, mm, bstar, &nconst, &nopsmode, delta_t, &nb, pos, vel, kep, status, &err);

    // Output arguments
    if (setOutputMatrixDouble(1, pos, 3, nb, fname))
    {
        freeMatrices(pos, vel, kep, status);
        return 0;
    }

    if (setOutputMatrixDouble(2, vel, 3, nb, fname))
    {
        freeMatrices(pos, vel, kep, status);
        return 0;
    }

    if (setOutputMatrixDouble(3, kep, 6, nb, fname))
    {
        freeMatrices(pos, vel, kep, status);
        return 0;
    }

    if (setOutputMatrixInteger(4, status, 1, nb, fname))
    {
        freeMatrices(pos, vel, kep, status);
        return 0;
    }

    if (setOutputInteger(5, err, fname))
        return 0;

    freeMatrices(pos, vel, kep, status);

    return 0;
}
} /* extern "C" */
/* ==================================================================== */

static int getInputMatrixDouble(int argPos, double **pdValues, char *fname)
{
    int *piAddressVar = NULL;
    SciErr sciErr;
    int iRows = 0;
    int iCols = 0;

    sciErr = getVarAddressFromPosition(pvApiCtx, argPos, &piAddressVar);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (!isDoubleType(pvApiCtx, piAddressVar))
    {
        Scierror(999, "%s: Wrong type for input argument #%d: A double expected.\n", fname, argPos);
        return 1;
    }
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVar, &iRows, &iCols, pdValues);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return sciErr.iErr;
    }
    return 0;
}

static int getInputInteger(int argPos, int *piValue, char *fname)
{
    int *piAddressVar = NULL;
    SciErr sciErr;

    sciErr = getVarAddressFromPosition(pvApiCtx, argPos, &piAddressVar);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (!isIntegerType(pvApiCtx, piAddressVar))
    {
        Scierror(999, "%s: Wrong type for input argument #%d: An integer expected.\n", fname, argPos);
        return 1;
    }
    if (getScalarInteger32(pvApiCtx, piAddressVar, piValue))
    {
        Scierror(999, "%s: Wrong size for input argument #%d: A scalar expected.\n", fname, argPos);
        return 1;
    }
    return 0;
}

static int setOutputMatrixDouble(int argPos, double *pdValues, int iRows, int iCols, char *fname)
{
    SciErr sciErr;
    sciErr = createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + argPos, iRows, iCols, pdValues);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return sciErr.iErr;
    }
    AssignOutputVariable(pvApiCtx, argPos) = nbInputArgument(pvApiCtx) + argPos;
    return 0;
}

static int setOutputMatrixInteger(int argPos, int *piValues, int iRows, int iCols, char *fname)
{
    SciErr sciErr;
    sciErr = createMatrixOfInteger32(pvApiCtx, nbInputArgument(pvApiCtx) + argPos, iRows, iCols, piValues);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return sciErr.iErr;
    }
    AssignOutputVariable(pvApiCtx, argPos) = nbInputArgument(pvApiCtx) + argPos;
    return 0;
}

static int setOutputInteger(int argPos, int iValue, char *fname)
{
    if (createScalarInteger32(pvApiCtx, nbInputArgument(pvApiCtx) + argPos, iValue))
    {
        Scierror(999, _("%s: Memory allocation error.\n"), fname);
        return 1;
    }
    AssignOutputVariable(pvApiCtx, argPos) = nbInputArgument(pvApiCtx) + argPos;
    return 0;
}

static void freeMatrices(double* pos, double* vel, double* kep, int* status)
{
    FREE(pos);
    FREE(vel);
    FREE(kep);
    FREE(status);
}

