//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Check name / value of configuration parameter 
// (check name/value comply with description) 
// returns: ok if valid
function [ok, errmsg] = CLx__configCheckVal(config_desc, name, val)

  // check value has correct type
  function [ok] = checkType(val, typ)
    ok = %f; 
    if (typ == "integer")
      if (typeof(val) == "constant")
        ok = (val == round(val) & real(val) == val); 
      end
    elseif (typ == "double")
      if (typeof(val) == "constant")
        ok = (real(val) == val); 
      end
    else
      ok = (typeof(val) == typ); 
    end
  endfunction
  
  // check value is an accepted value
  // accval: array of accepted values or []
  function [ok] = checkVal(val, accval)
    if (accval == [])
      ok = %t; // no check
    else
      ok = (find(val == accval) <> []); 
    end
  endfunction

  // Default returned values
  ok = %f; 
  errmsg = ""; 
  
  
  // index of configuration parameter
  k = find(name == config_desc.names);  
  if (k == [])
    errmsg = "Invalid name for configuration parameter: " + name; 
    return; 
  end
   
  // check case of empty value (simplifies the rest of the code)
  // as [] is of type "constant" is Scilab
  if (val == [])
    if (~config_desc.accemptyval(k))
      errmsg = "Invalid value for configuration parameter: " + name;
    else
      ok = %t; 
    end
    return; 
  end
  
  // NB: now value is not empty
  
  // check size
  if (size(val, "*") <> 1)
    errmsg = "Invalid size for configuration parameter: " + name;
    return; 
  end

  // check type
  if (~checkType(val, config_desc.types(k)))
    errmsg = "Invalid type for configuration parameter: " + name;
    return; 
  end

  // check value 
  if (~checkVal(val, config_desc.accval(k)))
    errmsg = "Invalid value for configuration parameter: " + name;
    return; 
  end

  ok = %t; 

endfunction

