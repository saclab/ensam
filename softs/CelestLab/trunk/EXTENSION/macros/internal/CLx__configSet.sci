//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Sets the value of a configuration parameter (internal use)
// NB: Uses %CLx_PRIV directly
// Error raised if name is not found in %CLx__PRIV.PREF
// Warning: value not checked  
 
function CLx__configSet(name, val)
  global %CLx__PRIV; 
  
  if (typeof(name) <> "string" | size(name, "*") <> 1)
    CLx__error("Invalid type or size for argument name"); 
  end

  if (~isfield(%CLx__PRIV, "PREF"))
    CLx__error("Internal configuration structure not initialized"); 
  end

  if (~isfield(%CLx__PRIV.PREF, name))
    CLx__error("Parameter " + name + " not found in internal structure"); 
  end
  
  %CLx__PRIV.PREF(name) = val; 

endfunction

