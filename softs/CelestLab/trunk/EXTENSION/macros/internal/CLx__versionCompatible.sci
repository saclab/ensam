//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Compare version with reference version
// version should be >= reference version.  
// Versions are given as 3 (at least) integers (sizes = 1xn, n >= 3)
// The "nd" first digits should be identical 
// 3 digits at most are compared. 
// nd: integer between 0 and 3 (included)
// ok: %t if numversion is compatible with numversionref (version number equal or larger)
  
function [ok] = CLx__versionCompatible(numversion, numversionref, nd)
  ok = %t; 
  
  if (nd > 0)
    if (find(numversion(1:nd) <> numversionref(1:nd)) <> [])
      ok = %f; 
    end
  end
  
  if (ok & nd < 3)
    if (numversion(nd+1) < numversionref(nd+1))
      ok = %f; 
    end
  end
  
endfunction
