//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

// Write an error message
// message : string (1x1)
function CLx__error(message)
    // Replace % by %% so that error function works
    message = strsubst(message, "%", "%%");

    errMsg = "*** CelestLabX: " + message;
    error(errMsg);
endfunction
