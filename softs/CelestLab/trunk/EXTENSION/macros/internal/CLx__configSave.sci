//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Saves internal configuration data in file
// Only stores parameters present in configuration description

function CLx__configSave(fname)

  if (typeof(fname) <> "string" | size(fname, "*") <> 1 | stripblanks(fname, %t) == "")
    CLx__error("Invalid file name"); 
  end
 
  str = ["// CelestLabX configuration file"; "// "]; 
  
  config_desc = CLx__configGetDesc(); 
  
  fname_default = fullfile(CLx_home(), "config_default.txt"); 
  conf_default = CLx__configLoad(fname_default); 
    
  for (k = 1 : size(config_desc.names, "*"))
    name = config_desc.names(k); 
    
    // write current value if indicator "writechange" is true
    if (config_desc.writechange(k))
      val = CLx__configGet(name); 
    else
      val = conf_default(name); 
    end
    
    if (val == [])
      str($+1) = msprintf("conf.%s = []", name); 

    elseif (config_desc.types(k) == "integer")
      str($+1) = msprintf("conf.%s = %d", name, val); 
      
    elseif (config_desc.types(k) == "boolean")
      if (val == %t)
        strval = "%t"; 
      else
        strval = "%f"; 
      end
      str($+1) = msprintf("conf.%s = %s", name, strval); 
      
    elseif (config_desc.types(k) == "string")
      // quotes: doubled
      val = strsubst(val, "''", "''''"); 
      val = strsubst(val, '""', '""""'); 
      str($+1) = msprintf("conf.%s = ""%s""", name, val); 

    elseif (config_desc.types(k) == "double")
      str($+1) = msprintf("conf.%s = %.16g", name, val); 

    else
      CLx__error("Unexpected type in configuration description"); 
    end
    
  end
  
  mputl(str, fname); 
  
endfunction

