//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// add parameter in Java "jparams" object
// check type and size
// paramType: "double", "integer", "boolean" or "string"
// paramVal can be a vector if paramType = "string"

function CLx__setParam(jparams, paramName, paramType, paramVal)
 
	if (paramType == "double")
    if (size(paramVal,"*") <> 1 | typeof(paramVal) <> "constant")
      CLx__error(msprintf("Incorrect type or size for Parameter %s", paramName));
    end
    if (paramVal <> real(paramVal))
      CLx__error(msprintf("Incorrect value for Parameter %s", paramName));
    end
		jinvoke(jparams, "addDouble", paramName, paramVal);
    
	elseif (paramType == "integer") 
    if (size(paramVal,"*") <> 1 | typeof(paramVal) <> "constant")
      CLx__error(msprintf("Incorrect type or size for Parameter %s", paramName));
    end
    if (paramVal <> real(paramVal) | paramVal <> round(paramVal))
      CLx__error(msprintf("Incorrect value for Parameter %s", paramName));
    end
		jinvoke(jparams, "addInteger", paramName, paramVal);
    
	elseif (paramType == "string") then
    if (typeof(paramVal) <> "string")
      CLx__error(msprintf("Incorrect type for Parameter %s", paramName));
    end
		if (size(paramVal,'*') == 1)
			jinvoke(jparams, "addString", paramName, paramVal);
		else
			jinvoke(jparams, "addStringArray", paramName, paramVal);
		end
    
	elseif (paramType == "boolean") then
    if (size(paramVal,"*") <> 1 | typeof(paramVal) <> "boolean")
      CLx__error(msprintf("Incorrect type or size for Parameter %s", paramName));
    end
		jinvoke(jparams, "addBoolean", paramName, paramVal);
    
	else
		CLx__error(msprintf("Unknown type %s for param %s", paramType, paramName));
	end
  
endfunction