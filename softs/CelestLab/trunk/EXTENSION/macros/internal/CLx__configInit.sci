//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [updated] = CLx__configInit()
// Loads CelestLabx's configuration parameters
//
// Calling Sequence
// CLx__configInit()
//
// Description
// <itemizedlist>
// <listitem>
// <p>Loads CelestLab's configuration parameters from a file.</p>
// <p>Note: This function initializes the global variable %CLx__PRIV.DATA.PREF and 
// is supposed to be run only once, when CelestLabX starts.</p>
// </listitem>
// </itemizedlist>
//
// Authors
// CNES - DCT/SB
//

// Declarations:
clearglobal %CLx__PRIV; 
global %CLx__PRIV;

// Code:

updated = %f; 

// loads configuration 
// => try celestlabx_config.txt in SCIHOME
// => if does not work, copy default preference file and try again

prefname = fullfile(SCIHOME, "celestlabx_config.txt"); 
defprefname = fullfile(CLx_home(), "config_default.txt"); 

// if config_default is more recent than the file in SCIHOME
// (after an update) => remove celestlabx_config.txt in SCIHOME
if (newest(prefname, defprefname) == 2)
  if (isfile(prefname)); mdelete(prefname); end
end

conf = CLx__configLoad(prefname); 

if (conf == [] & ~updated)
  if (copyfile(defprefname, prefname) == 1) // 1: copy OK
    updated = %t; // configuration file created / updated 
    conf = CLx__configLoad(prefname); 
  end
end

if (conf == [])
  mprintf("\n---------------------------------------------------------\n"); 
  mprintf("*** CelestLabX configuration file cannot be loaded. \n"); 
  mprintf("---------------------------------------------------------\n\n"); 
  CLx__error("Error loading configuration file"); 
end

// Initialize %CLx__PRIV if it did not exist
// (NB: if a global variable does not exist, Scilab initializes it with [])
if (isempty(%CLx__PRIV)); 
  %CLx__PRIV = struct(); 
end

// Check that %CLx__PRIV is a structure
if (~isstruct(%CLx__PRIV)); 
  CLx__error("Invalid global variable %CLx__PRIV. Restart CelestLab."); 
end

// Set the configuration parameters structure (as a whole)
%CLx__PRIV.PREF = conf;


// ---------------------------------------
// Additional processing
// Create all additional variable in configuration structure here. 
// ---------------------------------------

clversion = CLx__configGet("CL_VERSION"); 
numversion = CLx__extractVersion(clversion, 1); // strict mode

if (numversion == [])
  CLx__error("Invalid CelestLab Version in configuration file"); 
end

// add celestlab vesion as numbers in config structure
%CLx__PRIV.PREF.CL_VERSION_NUM = numversion; 

// add stela availability indicator in config structure
%CLx__PRIV.PREF.STELA_AVAILABLE = %f;

endfunction
