//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Check Stela is available
// Returns: %t if Stela is available
// available <=> STELA_ROOT <> [] and availability indicator is %t

function [available] = CLx__stelaAvailable()
  
  STELA_ROOT = CLx__configGet("STELA_ROOT"); 
  STELA_AVAILABLE = CLx__configGet("STELA_AVAILABLE");
  
  available = ((STELA_ROOT <> []) & (STELA_AVAILABLE == %t)); 
     
endfunction
