//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Converts the params (Scilab) structure into a "jparams" (Java) object
// params: model parameters for CLx_stela_extrap, CLx_stela_deriv ...

function [jparams] = CLx__createStelaParams(params)
  // initialize java structure
  jimport fr.cnes.celestlab.celestlabx.generic.Params;
  jparams = jnewInstance("fr.cnes.celestlab.celestlabx.generic.Params");

  // Standard parameters
  // (should exist in java structure)
  Params1 = [["mass"; "double"], ["central_enabled"; "boolean"], ["zonal_enabled"; "boolean"], ..
             ["zonal_maxDeg"; "integer"], ["tesseral_enabled"; "boolean"], ["tesseral_maxDeg"; "integer"], ..
             ["tesseral_minPeriod"; "double"], ["thirdbody_enabled"; "boolean"], ["thirdbody_bodies"; "string"], ..
             ["drag_enabled"; "boolean"], ["drag_coefType"; "string"], ..
             ["drag_area"; "double"], ["drag_coef"; "double"], ["drag_solarActivityType"; "string"], ..
             ["drag_solarActivityFlux"; "double"], ["drag_solarActivityAp"; "double"], ["srp_enabled"; "boolean"], ..
             ["srp_area"; "double"], ["srp_coef"; "double"], ["ref_frame"; "string"], ["integrator_step"; "double"]]; 
  
  // Optional parameters 
  Params2 = [["drag_atmosphericModel"; "string"], ..
             ["srp_nbQuadPoints"; "integer"], ["drag_nbQuadPoints"; "integer"], ["drag_nbComputeSteps"; "integer"], ..
             ["appAcc_nbQuadPoints"; "integer"], ["thirdbody_degree"; "integer"], ["reentryAltitude"; "double"], ..
             ["drag_cookWallTemp"; "double"], ["drag_cookAccomodCoef"; "double"]]; 
             
  // More advanced parameters 
  // NB: all may not be not documented (volontarily)
  // WARNING: changing path of physical constants file is risky! 
  Params2 = [ Params2, ..
              [["solarActivityFile"; "string"], ..
               ["aeroCoefFile"; "string"], ..
               ["physicalConstantsFile"; "string"]] ]; 
                       
  AllParams = [Params1, Params2]; 

  // Check type of "params" argument
  if (typeof(params) <> "st")
    CLx__error("Incorrect type for argument params: not a structure");
  end

  // fieldnames in params
  names = matrix(fieldnames(params), 1, -1); 

  // check field names are valid names
  unknown_names = setdiff(names, AllParams(1,:));  
  if (unknown_names <> [])
    CLx__error("Incorrect fieldname(s) in params structure: " + strcat(unknown_names, " "));
  end
       
  // Advanced parameters (optional)
  // if parameter exists and value is "" => use standard path (in default location)  
  // NB: not necessary to change value for solar activity or aero coef => value remains ""
  NAMES = ["physicalConstantsFile", "solarActivityFile", "aeroCoefFile"]; 
  FILENAMES = ["stela_physical_parameters", "", ""]; // relative to STELA_ROOT 
  
  // create local copy of params
  params = params; 

  for (k = 1 : size(NAMES,2))
    name = NAMES(k);   
    if (isfield(params, name))
      value = stripblanks(params(name), %t); 
      if (value == "")
        params(name) = FILENAMES(k); 
      end
    end
  end

  
  // Process all fieldnames defined in "params" structure
  // => add them is "jparams"
  for (k = 1 : size(names, 2))
    I = find(names(k) == AllParams(1,:)); 
    CLx__setParam(jparams, AllParams(1,I), AllParams(2,I), params(names(k)));
  end
    
endfunction








