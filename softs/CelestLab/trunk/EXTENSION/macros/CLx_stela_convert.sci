//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

//==================================================================================================
// This function converts orbital elements using STELA (mean <-> osc)
// Parameters:
// - direction: "mean2osc" or "osc2mean"
// - cjd: epoch (CJD, TREF time scale) (1xN)
// - x: orbital elements (CelestLab type: "cireq"), Frame = MOD (6xN)
// - params: model description (struct)
// - ut1_tref: UT1 minus TREF (1x1)
// - tt_tref: TT minus TREF (1x1)
// Returns:
// - xconv: converted orbital elements (6xN double)
// - info: informations about the computing (struct):
//    - status: (integer) 0 if OK, -1 if an error occurred 
//    - status_msg: (string) "OK" if the computing was completed successfully, "Error" if error occured (string)
//    - err_msg: (string) error message 
//    - sim_xml: (string, Nx1) simulation parameters in STELA XML format 
//==================================================================================================
function [xconv, info] = CLx_stela_convert(direction, cjd, x, params, ut1_tref, tt_tref)
    xconv = [];
    info = struct("status", 0, "status_msg", "OK", "sim_xml", "", "err_msg", "");

    // Check STELA is available
    if (~CLx__stelaAvailable())
      CLx__error("STELA is not available");
    end
    
    // check sizes: cjd=1xN and x=6xN
    if (size(cjd,1) <> 1 | size(x,1) <> 6 | size(cjd,2) <> size(x,2))
        CLx__error("Invalid arguments sizes (time or state)");
    end
    if (size(ut1_tref,"*") <> 1 | size(tt_tref,"*") <>1)
        CLx__error("Invalid arguments sizes (ut1_tref or tt_tref)");
    end

    // Get Java object gathering the model parameters
    jparams = CLx__createStelaParams(params);

    // Create conversion instance
    jimport fr.cnes.celestlab.celestlabx.stela.Conversion;
    jconvert = jnewInstance(Conversion);

    // Run conversion
    jinformations = jinvoke(jconvert, "convert", direction, cjd, x, jparams, ut1_tref, tt_tref);

    // Get status
    status = jinvoke(jinformations, "getStatus");

    errorMessage = jinvoke(jinformations, "getErrorMessage");
    simulationXML = jinvoke(jinformations, "getSimulationXML");
    if (status == 0) statusMsg = "OK"; else statusMsg = "Error"; end

    info = struct("status", double(status), ..
                  "status_msg", statusMsg, ..
                  "sim_xml", simulationXML', ..
                  "err_msg", errorMessage);

    // If extrapolation succeeded
    if (info.status >= 0) 
        // Get the converted parameters
        xconv = jinvoke(jconvert, "getResults", jvoid);
    end
    
    // Clear memory - should not be necessary
    jremove(Conversion, jconvert, jinformations); 


endfunction


