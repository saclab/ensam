//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Internal function:
// Computes semi major axis (brouwer convention) from mean motion. 
//
// mm: mean motion (rad/s) (1xN)
// ecc: eccentricity (1xN)
// inc: inclination (rad) (1xN)
// whichconst: "wgs72old" or "wgs72" or "wgs84"
// sma: semi major axis (m) (1xN)

function [sma] = CLx_tle_unkozai(mm, ecc, inc, whichconst)

whichconst_names = ["wgs72old", "wgs72", "wgs84"];
whichconst_values = [721, 72, 84];

// converts values for whichconst 
I = find(whichconst_names == whichconst);
if (I == []); CLx__error("Bad value for whichconst"); end
nconst = whichconst_values(I);

// check sizes (only the number of elements)
nb = length(mm);
if (size(ecc, "*") <> nb | size(inc, "*") <> nb)
  CLx__error("Invalid argument sizes");
end

// check values 
if (find(mm <= 0 | ecc < 0 | ecc >= 1 | inc < 0 | inc > %pi+2*%eps) <> [])
  CLx__error("Invalid argument values");
end 

// Note: n: mean motion consistent with sma (brouwer) - NOT USED
[sma, n, ierr] = CLx_tle_intern_unkozai( ..
      mm, ..
      ecc, ..
      inc, ..
      int32(nconst), ..
      int32(nb));

if (ierr <> 0)
  CLx__error("Unable to un-kozai");
end

endfunction

