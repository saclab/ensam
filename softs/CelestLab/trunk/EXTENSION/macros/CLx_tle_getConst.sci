//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Internal function:
// Get SGP4 contants
//
// whichconst: "wgs72old" or "wgs72" or "wgs84";
// units = USI
function cst = CLx_tle_getConst(whichconst)
    whichconst_names = ["wgs72old", "wgs72", "wgs84"];
    whichconst_values = [721, 72, 84];

    // converts values for whichconst and opsmode
    I = find(whichconst_names == whichconst);
    if (I == []); CLx__error("Bad value for whichconst"); end
    nconst = whichconst_values(I);

    [er, mu, j2, j3, j4, ierr] = CLx_tle_intern_getconst(int32(nconst));

    cst = struct( ..
      "mu", mu, ..
      "er", er, ..
      "j2", j2, ..
      "j3", j3, ..
      "j4", j4);

    if (ierr <> 0)
      CLx__error("Unable to retrieve SGP4 constants");
    end
endfunction

