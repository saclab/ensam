//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Register Stela install into the toolbox
// path_stela : Stela installation directory path
// Equivalent to CLx_setting_set("STELA_ROOT", path_stela)
// This function does nothing if "path_stela" is identical to the path already recorded

function CLx_stela_register(path_stela)
  path_stela_old = CLx_setting_get("STELA_ROOT"); 
    
  if (path_stela <> path_stela_old)
    CLx_setting_set("STELA_ROOT", path_stela);
    
    if (path_stela <> [])
      mprintf("\n=> Stela path registered!\n"); 
    end
    
    mprintf("=> Restart Scilab/CelestLabX for the new setting to be taken into account\n\n");  
    
  end
endfunction
