//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

// Returns a structure contains main constants
// Units are USI (m, rad, s)
// - au: astronomical unit
// - srp_1au: SRP at 1 au
// - er: equatorial radius
// - obla: oblateness
// - mu, muSun, muMoon: gravitational constant for Earth, Sun, Moon
// - j1jn (Nx1):  j1jn(n) <=> Jn
// - cs1nm (NxN, complex): cs1nm(n,m) <=> Cnm + %i * Snm
//
// If opt = 0 => returns all available constants
// If opt = 1 => return "mu" only (faster)

function [cst] = CLx_stela_getConst(opt)

    // Check STELA is available
    if (~CLx__stelaAvailable())
      CLx__error("STELA is not available");
    end

    jimport fr.cnes.celestlab.celestlabx.stela.PhysicalConsts;
    
    if (opt <> 0 & opt <> 1)
        CLx__error("Invalid value for opt");
    end

    jphy = jnewInstance(PhysicalConsts);

    if (opt == 0)
      mu = jinvoke(jphy, "getVal", "mu");
      er = jinvoke(jphy, "getVal", "er");
      obla = jinvoke(jphy, "getVal", "obla");
      muSun = jinvoke(jphy, "getVal", "muSun");
      muMoon = jinvoke(jphy, "getVal", "muMoon");
      srp_1au = jinvoke(jphy, "getVal", "srp_1au");
      au = jinvoke(jphy, "getVal", "au");

      j1jn = jinvoke(jphy, "getJ1Jn", jvoid); 
      c1nm = jinvoke(jphy, "getC1nm", jvoid); 
      s1nm = jinvoke(jphy, "getS1nm", jvoid); 
      
      cst = struct( ..
        "mu", mu, ..
        "er", er, ..
        "obla", obla, ..
        "j1jn", matrix(j1jn, -1, 1), ..
        "cs1nm", c1nm + %i * s1nm, ..
        "muSun", muSun, ..
        "muMoon", muMoon, ..
        "srp_1au", srp_1au, ..
        "au", au ..
      );

    else
      mu = jinvoke(jphy, "getVal", "mu");
      cst = struct("mu", mu); 
      
    end
    
endfunction
