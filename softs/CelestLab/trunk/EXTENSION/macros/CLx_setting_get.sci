//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Get the value of a CelestLabX configuration parameter 
// Only those present in the configuration file are accepted
// name: (string) parameter name
// returns: (type depends on parameter) Value of parameter, if name is a valid name  

function [value] = CLx_setting_get(name)

  // check argument type / size
  if (typeof(name) <> "string" | size(name, "*") <> 1)
    CLx__error("Invalid type or size for argument name"); 
  end

  // Check name exists in configuration description  
  config_desc = CLx__configGetDesc(); 
 
  if (find(name == config_desc.names) == [])
    CLx__error("Invalid configuration parameter name"); 
  end
  
  // value from internal structure
  value = CLx__configGet(name); 
  
endfunction


