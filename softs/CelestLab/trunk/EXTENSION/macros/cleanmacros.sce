// ====================================================================
// This file is released under the 3-clause BSD license. See COPYING-BSD.
// ====================================================================
function cleanmacros()

    libpath = get_absolute_file_path("cleanmacros.sce");
    exec(fullfile(libpath, "internal", "cleanmacros.sce"));
    
endfunction

cleanmacros();
clear cleanmacros; // remove cleanmacros on stack

// ====================================================================
