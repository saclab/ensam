//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Sets the value of a configuration parameter 
// Only those present in the configuration file are accepted
// name: (string) parameter name
// value: (any type) must be consitent with the configuration description

function CLx_setting_set(name, value)

  // check argument type / size
  if (typeof(name) <> "string" | size(name, "*") <> 1)
    CLx__error("Invalid type or size for argument name"); 
  end

  // Check name exists in configuration description 
  // and that the value can be changed   
  config_desc = CLx__configGetDesc(); 
  
  if (find(name == config_desc.names & config_desc.settable) == [])
    CLx__error("Invalid configuration parameter name: " + name); 
  end
  
  // check the value
  if (~CLx__configCheckVal(config_desc, name, value))
    CLx__error("Invalid value for configuration parameter: " + name); 
  end
  
  // Specific actions for some parameters
  // Note: the values should be correct (type value)

  if (name == "LOG_VERBOSITY") 
    jimport fr.cnes.celestlab.celestlabx.logging.LoggingConfigurator;
    jinvoke(LoggingConfigurator, "setLogVerbosity", value);
        
  elseif (name == "STELA_LOG") 
    jimport fr.cnes.celestlab.celestlabx.logging.LoggingConfigurator;
    jinvoke(LoggingConfigurator, "setLogStela", value);
        
  elseif (name == "LOG_FILE") 
    jimport fr.cnes.celestlab.celestlabx.logging.LoggingConfigurator;
    jinvoke(LoggingConfigurator, "setLogFile", value);
        
  elseif (name == "STELA_ROOT" & value <> []) 
    // NB: stela not initialized => Scilab has to be restarted
    if (~CLx__stelaPathValid(value))
      CLx__error("Invalid STELA path"); 
    end  
  end

  // Update internal data
  previous_value = CLx__configGet(name); 
  CLx__configSet(name, value); 
  
  // Update configuration file if necessary   
  // NB: value is of size 1x1 => test correct
  if (value <> previous_value) 
  
    fname = fullfile(SCIHOME, "celestlabx_config.txt"); 
    CLx__configSave(fname); 
    
    // if STELA ROOT changed => STELA not available 
    if (name == "STELA_ROOT")
      CLx__configSet("STELA_AVAILABLE", %f); 
    end
  end
    
endfunction

