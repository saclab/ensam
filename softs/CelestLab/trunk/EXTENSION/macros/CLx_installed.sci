//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Check that CelestLabX is installed (true if this function can be called) and that 
// the CelestLab version is correct. 
// clversion: (optional) CelestLab version of caller: 3 or more digits (size = 1xn with n>=3) 
// returns %t if installation OK. 
//
// This function is expected to be called by CelestLab to ensure CelestLabX is correctly installed, 
// that is, is compatible with CelestLab. 

function [installed] = CLx_installed(clversion)

  installed = %f; 
  
  if (argn(2) == 1)
    if (typeof(clversion) <> "constant" | size(clversion,1) <> 1 | ..
        size(clversion,2) < 3) 
      CLx__error("Incorrect argument value"); 
    end
   
    // check CelestLab Version
    // compares clversion with version recorded in CelestLabX configuration
    // it is expected that clversion >= clversion_config
    
    nd = CLx__configGet("CL_VERSION_NCHECK"); 
    clversion_config = CLx__configGet("CL_VERSION_NUM"); 
    
    if (~CLx__versionCompatible(clversion, clversion_config, nd))
      installed = %f;
      return; 
    end
    
  end

  // check dynamic libraries OK 
  err = execstr("CLx_intern_installed()", "errcatch");

  if (err == 0)
    installed = %t;
  else
    installed = %f;
  end

endfunction
