//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

// Register constants in STELA
// Constants: same as returned by CLx_stela_getconst
// if 0 arguments => reset STELA constants (from file) 
//
// Units are USI (m, rad, s)
// - au: astronomical unit
// - srp_1au: SRP at 1 au
// - er: equatorial radius
// - obla: oblateness
// - mu, muSun, muMoon: gravitational constant for Earth, Sun, Moon
// - j1jn (Nx1):  j1jn(n) <=> Jn
// - cs1nm (NxN, complex): cs1nm(n,m) <=> Cnm + %i * Snm

function [] = CLx_stela_setConst(cst)

    // Check STELA is available
    if (~CLx__stelaAvailable())
      CLx__error("STELA is not available");
    end

    jimport fr.cnes.celestlab.celestlabx.stela.PhysicalConsts;

    // check input arguments
    nbargs = argn(2); 
    
    if (nbargs > 1) 
        CLx__error("Invalid number of arguments");
    end
    
    if (nbargs == 1)
      if (typeof(cst) <> "st")
        CLx__error("Invalid type for argument cst");
      end
    end

    // create object instance
    jphy = jnewInstance(PhysicalConsts);

    if (nbargs == 0) 
      // no arguments => reset
      jinvoke(jphy, "reset", jvoid); 

    else
      // TODO: check existence and type of fields
      // TODO: better error control

      jinvoke(jphy, "putVal", "mu", cst.mu);
      jinvoke(jphy, "putVal", "er", cst.er);
      jinvoke(jphy, "putVal", "obla", cst.obla);
      jinvoke(jphy, "putVal", "muSun", cst.muSun);
      jinvoke(jphy, "putVal", "muMoon", cst.muMoon);
      jinvoke(jphy, "putVal", "srp_1au", cst.srp_1au);
      jinvoke(jphy, "putVal", "au", cst.au);
    
      jinvoke(jphy, "putJ1Jn", cst.j1jn);
      jinvoke(jphy, "putCS1nm", real(cst.cs1nm), imag(cst.cs1nm));

      // Update STELA constants
      jinvoke(jphy, "update", jvoid); 
    end
    
endfunction

