//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


//==================================================================================
// obj = CLx_stela_areaLoad(desc)
// 
// NB: dimensions:
// - cuboid: length (y), height (z), depth (x)
// - rectangle: length (y), width (x)
// - sphere: radius
// - triangle: leftlength (y), rightlength (y), height (x)
// - TruncatedCone: bottomRadius (xy), topRadius (xy), height (z)
//
// In structure "obj": 
// - z_axis <=> orientation
// - x_axis : X axis of sub-part frame
// - orig <=> position 
// 
// Examples:
//
// desc = fullfile(CLx_setting_get("STELA_ROOT"), "examples/example_shap.xml");
// obj = CLx_stela_areaLoad(desc) 
//
//==================================================================================

function [obj] = CLx_stela_areaLoad(desc)
      
    obj = struct();  
    
    // Check STELA is available
    if (~CLx__stelaAvailable())
      CLx__error("STELA is not available");
    end

    // Get the area computation instance
    jimport fr.cnes.celestlab.celestlabx.area.AreaScene;
    jimport fr.cnes.celestlab.celestlabx.area.AreaInfo;

    jareascene = jnewInstance(AreaScene);

    // Load descriptor
    jinfo = jinvoke(jareascene, "load", desc);
    status = jinvoke(jinfo, "getStatus", jvoid);
    if (status <> 0)
      msg = jinvoke(jinfo, "getMsg", jvoid);
      CLx__error(msg); 
    end

    // get id 
    obj.id = jinvoke(jareascene, "getShapeIds", jvoid); 
    obj.shap = jinvoke(jareascene, "getShapeTypes", jvoid); 
    obj.dim = jinvoke(jareascene, "getShapeDimensions", jvoid); 
    obj.z_axis = jinvoke(jareascene, "getShapeOrientations", jvoid); 
    obj.x_axis = jinvoke(jareascene, "getShapeXaxes", jvoid); 
    obj.orig = jinvoke(jareascene, "getShapePositions", jvoid); 

endfunction


