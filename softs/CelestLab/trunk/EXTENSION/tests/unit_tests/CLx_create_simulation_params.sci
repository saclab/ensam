//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Parameters
params = struct();

// mass
params.mass = 1200.0;

// atmospheric drag
params.drag_enabled = %t;
params.drag_coefType = "constant";
params.drag_atmosphericModel = "NRLMSISE-00";
params.drag_area = 15.0;
params.drag_coef = 2.0;

// Solar activity
params.drag_solarActivityType = "constant";
params.drag_solarActivityFlux = 130.0;
params.drag_solarActivityAp = 10.0;

// Third body
params.thirdbody_enabled = %t;
params.thirdbody_bodies = ["Sun", "Moon"];

// earth perturbation
// gravitation: central force
params.central_enabled = %t;
// zonals
params.zonal_enabled = %t;
params.zonal_maxDeg = 10;
// tesserals
params.tesseral_enabled = %t;
params.tesseral_maxDeg = 7;
params.tesseral_minPeriod = 5 * 86400;

// SRP
params.srp_enabled = %t;
params.srp_area = 15.0;
params.srp_coef = 1.7;

// reference frame
params.ref_frame = "ICRF"; 

// Advanced parameters
params.integrator_step = 86400;

params.reentryAltitude = 70000.0;
params.srp_nbQuadPoints = 13;
params.drag_nbQuadPoints = 23;
params.drag_nbComputeSteps = 1;


// Time scale offsets: TT - TREF and UT1 - TREF (sec)
tt_tref = 67.184;
ut1_tref = 10;
