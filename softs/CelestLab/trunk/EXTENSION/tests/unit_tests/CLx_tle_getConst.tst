//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

cst = CLx_tle_getConst("wgs72old");

epsilon = 1D-15;
assert_checkalmostequal(cst.er, 6378135., epsilon);
assert_checkalmostequal(cst.mu, 398600.79964E9, epsilon);
assert_checkalmostequal(cst.j2, 0.001082616, 0, epsilon);
assert_checkalmostequal(cst.j3, -0.00000253881, 0, epsilon);
assert_checkalmostequal(cst.j4, -0.00000165597, 0, epsilon);
