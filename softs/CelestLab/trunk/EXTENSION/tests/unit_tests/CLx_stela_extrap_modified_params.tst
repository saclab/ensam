//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Internal function

function [] = check_impactParam(params2)
  // extrapolates using input STELA parameter and check that results are different from those computed with a reference parameter file
  // WARNING: reference results are computed once for all OUTSIDE this function [x1,xosc1] to speed up the test
  
  [x2, xosc2, dx2dx0, info] = CLx_stela_extrap(cjd0, x0, delta_t, params2, 0, ut1_tref, tt_tref);

  // Starts from 2nd colums since initial state is supposed to be the same!
  Ix = find(x1(:,2:$) == x2(:,2:$));
  assert_checkequal(Ix, []);

  
endfunction

//===================
// This test verifies that extrapolation parameters are well taken into account
// (i.e. a change in a parameter changes the output of the function)
//===================

tlbx_path = CLx_home();
exec(fullfile(tlbx_path, "tests", "unit_tests", "CLx_create_extrapolation_inputdata.sci"));
exec(fullfile(tlbx_path, "tests", "unit_tests", "CLx_extrap_utils.sci"));


// To restart each test with default values
params_backup = params;

// Extrapolation results corresponding to reference parameters (computed once for all)
[x1, x1osc, dx1dx0, info] = CLx_stela_extrap(cjd0, x0, delta_t, params, 0, ut1_tref, tt_tref);


//===================
// Change thirdbody_bodies
//===================
params.thirdbody_bodies = "Sun";

check_impactParam(params);

//===================
// Change drag_coefType
//===================

params = params_backup;
params.drag_coefType = "cook";

check_impactParam(params);

//===================
// Change drag_enabled 
//===================

params = params_backup;
params.drag_enabled = %f;

check_impactParam(params);

//===================
// Change ref_frame ("MOD")
//===================

params = params_backup;
params.ref_frame = "MOD";

check_impactParam(params);

//===================
// Change ref_frame ("CIRS")
//===================

params = params_backup;
params.ref_frame = "CIRS";

check_impactParam(params);

//===================
// Change thirdbody_enabled 
//===================

params = params_backup;
params.thirdbody_enabled = %f;

check_impactParam(params);

//===================
// Change srp_enabled 
//===================

params = params_backup;
params.srp_enabled = %f;

check_impactParam(params);

//===================
// Change mass value
//===================

params = params_backup;
params.mass = 10000;

check_impactParam(params);

//===================
// Change drag_area
//===================

params = params_backup;
params.drag_area = 30;

check_impactParam(params);

//===================
// Change drag_solarActivityType
//===================

params = params_backup;
params.drag_solarActivityType = "variable";

check_impactParam(params);