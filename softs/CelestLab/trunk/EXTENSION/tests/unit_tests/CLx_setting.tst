//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Save original settings
// Warning: if the test is interrupted before it ends
// default settings will not be restored

origStelaRoot = CLx_setting_get("STELA_ROOT");
origLogVerbosity = CLx_setting_get("LOG_VERBOSITY");
origLogStela = CLx_setting_get("STELA_LOG");
origLogFile = CLx_setting_get("LOG_FILE");

// Create a temporary folder (../stela/lib)
expectedStelaRoot = fullfile(TMPDIR, "stela");
mkdir(fullfile(expectedStelaRoot, "lib"));

// Create a jar file for test purposes (so that the stela/lib folder is accepted as a STELA_ROOT)
// => copy stela-precessing*.jar" as this is the one used for version detection
jarfile = listfiles(fullfile(CLx_home(), "thirdparty", "stela", "stela-processing*.jar"));
assert_checktrue(size(jarfile,"*") == 1);
copyfile(jarfile, fullfile(expectedStelaRoot, "lib"));

//=======================
// Check CLx_setting_set & et CLx_setting_get
//=======================

CLx_setting_set("STELA_ROOT", expectedStelaRoot);
stelaRoot = CLx_setting_get("STELA_ROOT");
assert_checkequal(stelaRoot, expectedStelaRoot);

CLx_setting_set("LOG_VERBOSITY", "WARN");
verbosity = CLx_setting_get("LOG_VERBOSITY");
assert_checkequal(verbosity, "WARN");

CLx_setting_set("STELA_LOG", %T);
logStela = CLx_setting_get("STELA_LOG");
assert_checkequal(logStela, %T);

CLx_setting_set("STELA_LOG", %F);
logStela = CLx_setting_get("STELA_LOG");
assert_checkequal(logStela, %F);

CLx_setting_set("LOG_FILE", fullfile(TMPDIR, "foo.log"));
logFile = CLx_setting_get("LOG_FILE");
assert_checkequal(logFile, fullfile(TMPDIR, "foo.log"));

// Check CLx_setting_reset
CLx_setting_reset();
verbosity = CLx_setting_get("LOG_VERBOSITY");
assert_checkequal(verbosity, "FATAL");
logStela = CLx_setting_get("STELA_LOG");
assert_checkequal(logStela, %f);
logFile = CLx_setting_get("LOG_FILE");
assert_checkequal(logFile, "");

// Restore original settings
CLx_setting_set("STELA_ROOT", origStelaRoot);
CLx_setting_set("LOG_VERBOSITY", origLogVerbosity);
CLx_setting_set("STELA_LOG", origLogStela);
CLx_setting_set("LOG_FILE", origLogFile);

