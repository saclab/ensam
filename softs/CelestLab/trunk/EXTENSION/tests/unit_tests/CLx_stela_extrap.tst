//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// ========================
// Test CLx_stela_extrap
//
// Tests:
// output state vector: mean equinoctical eleament
// output state transition matrix
//
// Nota: reference data obtained using STELA GUI (see file "procedure.txt")
// ========================

tlbx_path = CLx_home();
exec(fullfile(tlbx_path, "tests", "unit_tests", "CLx_create_extrapolation_inputdata.sci"));
exec(fullfile(tlbx_path, "tests", "unit_tests", "CLx_extrap_utils.sci"));

[x, xosc, dxd0, info] = CLx_stela_extrap(cjd0, x0, delta_t, params, res=1, ut1_tref, tt_tref);

checkExtrapolationResOK(info); 

// results obtained using STELA GUI
expected_size = 6;

// mean orbital elements (type == cireq)
expected_last_x    = [24678.10549617032*1000 0.7159941386228384 0.0443796214177053 0.08719532548933541 -0.00555467906976616 147.7691636806852*%pi/180]';

expected_last_xosc = [];

// state transition matrix
// to manually check difference => dif = expected_last_dxd0 - dxd0(:,:,$)

expected_last_dxd0 = [ 
    1.00007786403  0.00000000038  -0.00000000617  0.00000000005   0.00000000080  -0.00000856773; 
  -6840.457546790  0.98230090991   0.32019590586 -0.00275718456  -0.03317161296   0.57879633774; 
   22.43564222287  -0.06070312018  0.99791681057  0.00028558948  -0.00015570607   0.00038978813; 
   -1.86083085296  0.00417430363  -0.06422256626  0.99812939081  -0.06209636624  -0.13661211290; 
   -2.11667802760  0.00036237725   0.00112210435  0.06667263385   0.99792363104   0.00022288830; 
    0.00000000000  0.00000000000   0.00000000000  0.00000000000   0.00000000000   1.00000000000; 
  -19.38924525441  -0.00000022327  0.00000001708 -0.00000000046  -0.00000000391  0.00008058558; 
   -0.01727413507  0.00000642502  -0.00002014621 -0.00000095581  -0.00000008280  0.00003601107]'; 

checkExtrapolationResult(expected_size, x, expected_last_x, dxd0, expected_last_dxd0);
