//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [celestLabXLib, celestLabX_internalLib] = startCelestLabX()
    mode(-1);
    lines(0);
   
    mprintf("Start CelestLabX\n");
    celestLabXlib = [];
    celestLabX_internalLib = [];

    // Check Scilab's version 5.5 (for JIMS)
    try
        v = getversion("scilab");
        if (v(2) < 5) then
            error(gettext("Scilab 5.5 or more is required."));
        end
    catch
        error(gettext("Cannot get Scilab version."));
    end

    if (getscilabmode() == "NWNI") then
        // NWNI mode => no Java !    
        error(gettext("CelestLabX does not run in NWNI mode."));
    end;

    tlbx_etc_path = get_absolute_file_path('celestlabx.start');
    tlbx_root_path = fullpath(fullfile(tlbx_etc_path, '..'));

    // Load macros
    mprintf("\tLoad macros\n");
    tlbx_macro_path = fullfile(tlbx_root_path, "macros");
    celestLabXLib = lib(tlbx_macro_path);
    celestLabX_internalLib = lib(fullfile(tlbx_macro_path, "internal"));

    // Silent mode
    verbose = ilib_verbose();
    ilib_verbose(0);

    // Load gateways
    mprintf("\tLoad gateways\n");
    exec(fullfile(tlbx_root_path, "sci_gateway", "loader_gateway.sce"));

    // Load Configuration (settings)
    // => Initializes global variable %CLx__PRIV
    // NB: must be done after everything else is initialized  
    mprintf("\tLoad configuration");
    updated = CLx__configInit(); 
    if (updated)
      mprintf(" => (Warning) Set to default\n"); 
    else
      mprintf("\n"); 
    end

    // Load Java packages (JARs)
    mprintf("\tLoad Java packages\n");

    // CelestlabX JARs
    tlbx_jar_path = fullfile(tlbx_root_path, "jar");
    jars = CLx__findfiles(tlbx_jar_path, "jar", "_help.jar");
    javaclasspath(fullfile(tlbx_jar_path, jars));
      
    // Get value of STELA_ROOT from internal structure
    STELA_ROOT = CLx__configGet("STELA_ROOT");

    if (STELA_ROOT == []) then
          
      // Thirdparty JARs (included in CelestLabX) 
      tlbx_thirdparty_path = fullfile(tlbx_root_path, "thirdparty");
      thirdparty_jars = CLx__findfiles(tlbx_thirdparty_path, "jar", "_help.jar");
      javaclasspath(fullfile(tlbx_thirdparty_path, thirdparty_jars));
    
      mprintf("\tLoad STELA => (Warning) STELA not registered\n");
      
      // Initialize the Java interface
      CLx__initJava();
    
    else
      [valid, versionloaded] = CLx__stelaPathValid(STELA_ROOT); 
      
      if (valid)
        // Load STELA JARs (from STELA_ROOT)
        stela_lib_path = fullfile(STELA_ROOT, "lib");
        stela_jars = CLx__findfiles(stela_lib_path, "jar", "_help.jar");
        javaclasspath(fullfile(stela_lib_path, stela_jars));

        mprintf("\tLoad STELA => Version %s\n", versionloaded); 

      else
        mprintf("\tLoad STELA => Invalid STELA path: %s\n", STELA_ROOT);
        
      end
        
      // Thirdparty JARs from CelestLabX (excluding names that contain stela)
      tlbx_thirdparty_path = fullfile(tlbx_root_path, "thirdparty");
      thirdparty_jars = CLx__findfiles(tlbx_thirdparty_path, "jar", "stela");
      javaclasspath(fullfile(tlbx_thirdparty_path, thirdparty_jars));

      // Initialize the Java interface 
      CLx__initJava();
        
      // Initialize the STELA engine
      if (valid)
        CLx__initStela();
      end
      
    end

    // Load help
    mprintf("\tLoad help\n");
    tlbx_jar_path = fullfile(tlbx_root_path, "jar");
    if (isdir(tlbx_jar_path) <> []) then
        add_help_chapter("CelestLabX", tlbx_jar_path, %F);
    end

    // Display version
    // tlbx_version = mgetl(fullfile(tlbx_root_path, "VERSION"));
    // mprintf("\tVersion: %s\n", tlbx_version);

    // Restore original verbosity
    ilib_verbose(verbose);
endfunction


if (~isdef("celestLabXLib")) then
  [celestLabXLib, celestLabX_internalLib] = startCelestLabX();
else
  warning("CelestLabX library has already been loaded");
end

clear startCelestLabX;
