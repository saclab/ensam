//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

mode(-1);
lines(0);

function main_builder()

  TOOLBOX_NAME  = "celestlabx";
  TOOLBOX_TITLE = "CelestLabX";

  toolbox_dir = get_absolute_file_path("builder.sce");

  if (haveacompiler())
    tbx_builder_src(toolbox_dir);
    tbx_builder_gateway(toolbox_dir);
  else
    warning("Compiler not found => skip generation of libraries"); 
  end  
  tbx_builder_macros(toolbox_dir);
  tbx_builder_help(toolbox_dir);
  tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
  tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

endfunction

main_builder();

clear main_builder;
