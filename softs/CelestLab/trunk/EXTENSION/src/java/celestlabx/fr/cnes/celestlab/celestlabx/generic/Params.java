//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

package fr.cnes.celestlab.celestlabx.generic;

import org.apache.log4j.Logger;

import java.util.HashMap;
import java.util.Arrays;

/**
 * Class Params
 * This class encapsulates a list of parameters (key/value pairs)
 */
public class Params
{
    private HashMap<String, Object[]> params;

    private static final Logger logger = Logger.getLogger(Params.class);

    /**
     * Constructor
     */
    public Params()
    {
        params = new HashMap<String, Object[]>();
    }

    /**
     * Adds an integer parameter
     * @param name name of parameter {@link String)
     * @param value value of parameter {@link Integer)
     */
    public void addInteger(String name, Integer value)
    {
        Object[] objArray = { value };
        params.put(name, objArray);
    }

    /**
     * Returns the value of an integer parameter
     * @param name name of parameter {@link String)
     * @return value value of parameter {@link Integer)
     * @throws ParamException
     */
    public Integer getInteger(String name) throws ParamException
    {
        Integer[] integers = getIntegerArray(name);
        if ((integers != null) && (integers.length > 0))
        {
            Integer anInt = integers[0];
            logger.debug(String.format("Parameter %s : %s", name, anInt));
            return anInt;
        }
        else
            throw new ParamException(name, "no value");
    }

    /**
     * Returns the value of an integer array parameter
     * @param name name of parameter {@link String)
     * @return value value of parameter {@link Integer) array
     * @throws ParamException
     */
    public Integer[] getIntegerArray(String name) throws ParamException
    {
        Object[] objectArray = get(name);
        try
        {
            return Arrays.copyOf(objectArray, objectArray.length, Integer[].class);
        }
        catch(ArrayStoreException e)
        {
            throw new ParamException(name, "Cannot get parameter, check parameter type is Integer");
        }
    }

    /**
     * Adds a string parameter
     * @param name name of parameter {@link String)
     * @param value value of parameter {@link String)
     */
    public void addString(String name, String value)
    {
        Object[] objArray = { value };
        params.put(name, objArray);
    }

    /**
     * Adds a string array parameter
     * @param name name of parameter {@link String)
     * @param value value of parameter {@link String) array
     */
    public void addStringArray(String name, String[] value)
    {
        params.put(name, value);
    }

    /**
     * Returns the value of a string parameter
     * @param name name of parameter {@link String)
     * @return value value of parameter {@link String)
     * @throws ParamException
     */
    public String getString(String name) throws ParamException
    {
        String[] strings = getStringArray(name);
        if ((strings != null) && (strings.length > 0))
        {
            String aString = strings[0];
            logger.debug(String.format("Parameter %s : %s", name, aString));
            return aString;
        }
        else
            throw new ParamException(name, "no value");
    }

    /**
     * Returns the value of a string array parameter
     * @param name name of parameter {@link String)
     * @return value value of parameter {@link String) array
     * @throws ParamException
     */
    public String[] getStringArray(String name) throws ParamException
    {
        Object[] objectArray = get(name);
        try
        {
            String[] strings = Arrays.copyOf(objectArray, objectArray.length, String[].class);
            StringBuilder stringBuilder = new StringBuilder();
            for (String aString : strings)
                stringBuilder.append(aString + " ");
            logger.debug(String.format("Parameter %s : %s", name, stringBuilder.toString()));
            return strings;
        }
        catch(ArrayStoreException e)
        {
            throw new ParamException(name, "Cannot get parameter, check parameter type is String");
        }
    }

    /**
     * Adds a double parameter
     * @param name name of parameter {@link String)
     * @param value value of parameter {@link Double)
     */
    public void addDouble(String name, Double value)
    {
        Object[] objArray = { value };
        params.put(name, objArray);
    }

    /**
     * Returns the value of a double parameter
     * @param name name of parameter {@link String)
     * @return value value of parameter {@link Double)
     * @throws ParamException
     */
    public Double getDouble(String name) throws ParamException
    {
        Double[] doubles = getDoubleArray(name);
        if ((doubles != null) && (doubles.length > 0))
        {
            Double aDouble = doubles[0];
            logger.debug(String.format("Parameter %s : %s", name, aDouble));
            return aDouble;
        }
        else
            throw new ParamException(name, "no value");
    }

    /**
     * Returns the value of a double array parameter
     * @param name name of parameter {@link String)
     * @return value value of parameter {@link Double) array
     * @throws ParamException
     */
    public Double[] getDoubleArray(String name) throws ParamException
    {
        Object[] objectArray = get(name);
        try
        {
            return Arrays.copyOf(objectArray, objectArray.length, Double[].class);
        }
        catch(ArrayStoreException e)
        {
            throw new ParamException(name, "Cannot get parameter, check parameter type is Double");
        }
    }

    /**
     * Adds a boolean parameter
     * @param name name of parameter {@link String)
     * @param value value of parameter {@link Boolean)
     */
    public void addBoolean(String name, Boolean value)
    {
        Object[] objArray = { value };
        params.put(name, objArray);
    }

    /**
     * Returns the value of a boolean parameter
     * @param name name of parameter {@link String)
     * @return value value of parameter {@link Boolean)
     * @throws ParamException
     */
    public Boolean getBoolean(String name) throws ParamException
    {
        Boolean[] booleans = getBooleanArray(name);
        if ((booleans != null) && (booleans.length > 0))
        {
            Boolean aBoolean = booleans[0];
            logger.debug(String.format("Parameter %s : %s", name, aBoolean));
            return aBoolean;
        }
        else
            throw new ParamException(name, "no value");
    }

    /**
     * Returns the value of a boolean array parameter
     * @param name name of parameter {@link String)
     * @return value value of parameter {@link Boolean) array
     * @throws ParamException
     */
    public Boolean[] getBooleanArray(String name) throws ParamException
    {
        Object[] objectArray = get(name);
        try
        {
            return Arrays.copyOf(objectArray, objectArray.length, Boolean[].class);
        }
        catch(ArrayStoreException e)
        {
            throw new ParamException(name, "Cannot get parameter, check parameter type is Boolean");
        }
    }

    /**
     * Returns the type of a parameter
     * @param name of parameter {@link String)
     * @return the type of parameter {@link String)
     * @throws ParamException
     */
    public String getType(String name) throws ParamException
    {
        Object[] objectArray = get(name);
        if (objectArray.length > 0)
            return objectArray[0].getClass().getName().replaceFirst("java.lang.", "");
        else
            throw new ParamException(name, "no value");
    }

    public boolean exists(String name) 
    {
        if (params.containsKey(name))
            return true;
        else
            return false;
    }

    private Object[] get(String name) throws ParamException
    {
        if (params.containsKey(name))
            return params.get(name);
        else
            throw new ParamException(name, "not found");
    }
}
