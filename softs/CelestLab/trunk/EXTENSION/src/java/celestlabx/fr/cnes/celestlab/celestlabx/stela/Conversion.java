//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.stela;

import fr.cnes.celestlab.celestlabx.generic.Params;
import fr.cnes.los.stela.commons.logic.date.StelaDate;
import fr.cnes.los.stela.commons.model.ephemeris.Bulletin;
import fr.cnes.los.stela.commons.model.ephemeris.FrameTypes;
import fr.cnes.los.stela.commons.model.ephemeris.NatureTypes;
import fr.cnes.los.stela.commons.model.ephemeris.Type8PosVel;

import org.apache.log4j.Logger;


/**
 *  This class provides an interface with Scilab for mean <-> osculating conversion using Stela
 *  2 main interfaces: 
 *  - convert
 *  - getResults
 */
public class Conversion
{
    private static final Logger logger = Logger.getLogger(Conversion.class);

    private Simulation simulation; // simulation context
    private double[][] res = new double[0][0]; // transformed orbital elements

    /**
     * Constructor
     */
    public Conversion()
    {
        simulation = new Simulation();
    }

    /**
     * This method performs the conversion.
     * @param cmd: command: "mean2osc" or "osc2mean"
     * @param cjd: Date, (CJD, time scale: TREF)
     * @param x: (mean or osculating) orbital elements ("cireq" type, CelestLab convention)
     * @param params: structure containing the model parameters
     * @param ut1_tref: UT1 minus TREF (s)
     * @param tt_tref: TT minus TREF (s)
     * @return info: status of the conversion
     */
    
    public Information convert(String cmd, double[] cjd, double[][] x, Params params,
		double ut1_tref, double tt_tref) {
    	
      Information info = _convert(cmd, cjd, x, params, ut1_tref, tt_tref); 
      simulation.clean();
      return info; 

    }

	// same interface - low level	
    private Information _convert(String cmd, double[] cjd, double[][] x, Params params,
		double ut1_tref, double tt_tref) 
    {
        logger.debug("Entering compute function");
        
        if (!cmd.equals("mean2osc") & !cmd.equals("osc2mean"))
           return new Information(simulation, -1, "Invalid value for cmd");
        
        if (params == null)
            return new Information(simulation, -1, "params argument is null");
        
        if (cjd == null || cjd.length == 0)
            return new Information(simulation, -1, "cjd argument is null or empty");
        
        if (x == null || x.length == 0)
            return new Information(simulation, -1, "x argument is null or empty");
        
        if (x.length != cjd.length)
            return new Information(simulation, -1, "cjd and x don't have the same size");

        // type of initial orbital state (MEAN or OSCULATING)
        NatureTypes type_init; 
        if (cmd.equals("mean2osc")) {
        	type_init = NatureTypes.MEAN; 
        }
        else {
        	type_init = NatureTypes.OSCULATING; 
        }
        
        // initialisation of simulation context (data, models...)
        Information info = simulation.initialize(params, ut1_tref, tt_tref);
        
        if (info.getStatus() != 0)
            return info;

        int size = cjd.length;
        res = new double[size][6];
        
        try
        {
            for (int i = 0; i < size; i++) {
            	Bulletin bulletin = makeBulletin(cjd[i], x[i], type_init); 
            	if (type_init == NatureTypes.MEAN) 
            		bulletin = simulation.mean2osc(bulletin);
            	else 
            		bulletin = simulation.osc2mean(bulletin);
            	
            	res[i] = getOrbElem(bulletin); 
            }
        }
        
        catch(Exception e)
        {
            String errorMessage = String.format("Error while converting orbital elements: %s", e.getMessage());
            return new Information(simulation, -1, errorMessage);
        }

        return new Information(simulation, 0, "");
    }

    
    /**
     * Return converted orbital elements
     */
    public double[][] getResults()
    {
        return res;
    }
    
	/**
	 * Make a "Bulletin" from date and orbital state 
	 * @param cjd : date (TREF time scale)
	 * @param x : orbital elements ("cireq" type, CelestLab convention, frame = CIRF)
	 * @param nature: MEAN or OSCULATING
	 * @return the Bulletin
	 * NB: time scale: converted to UT1 (Stela convention)
	 */
	private Bulletin makeBulletin(double cjd, double[] x, NatureTypes type)
	{
		double ut1_tref = simulation.getUT1minusTREF(); 
		// Date is set to UT1 time scale
		StelaDate stelaDate = new StelaDate(cjd + ut1_tref / 86400);

		Type8PosVel param = new Type8PosVel(x[0], x[5], x[1], x[2], x[3], x[4],
				type, FrameTypes.CIRF);

		return new Bulletin(stelaDate, param);
	}
	
	/**
	 * Return Orbital elements from "bulletin"
	 * returned data: "cireq" type, CelestLab convention
	 */
	private double[] getOrbElem(Bulletin bulletin)
	{
		double[] p = bulletin.getPosVel().getOrbParams();
		// change order: STELA convention -> CelestLab convention
		return new double[] {p[0], p[2], p[3], p[4], p[5], p[1]};
	}

    
}
