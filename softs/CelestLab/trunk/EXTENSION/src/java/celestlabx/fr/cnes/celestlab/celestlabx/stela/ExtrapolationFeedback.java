//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.stela;

import fr.cnes.los.stela.elib.business.framework.simulation.IExtrapolationFeedback;

import org.apache.log4j.Logger;

/** 
 * Class ExtrapolationFeedBack
 * This class gives the feedback on the extrapolation progression
 */
public class ExtrapolationFeedback implements IExtrapolationFeedback
{
    private static final Logger logger = Logger.getLogger(ExtrapolationFeedback.class);

    /**
     * Constructor
     */
    ExtrapolationFeedback() {}

    /**
     * Log the extrapolation feedback
     * @param extrapolation progression percentage
     */
    @Override
    public void sendProgress(int progress)
    {
        if (progress % 5 == 0)
            logger.info(String.format("Extrapolation: %s%% done", progress));
    }

    @Override
    public void simulationUpdatedForGUI()
    {
    }
}