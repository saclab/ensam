//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.area;

import org.apache.commons.math3.geometry.euclidean.threed.Rotation;
import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

// Class used to compute the effect an additional rotation to a shape
// - shape frame nominally described by: orientation vector, angle, position (STELA convention)
// - rotation applied => new orientation vector, angle, position
// additional rotation defined by rotation axis, angle and center

class AreaTransform 
{
	private Vector3D Zref = Vector3D.PLUS_K; // (0,0,1)
	private Vector3D Xref =  Vector3D.PLUS_I; // (1,0,0)

	// Parameters describing the transformation
	// = STELA parameters describing the shape
	private Vector3D _orient; // = Z axis
	private double _ang; 
	private Vector3D _position;  // relative to body frame

	// Transformation derived quantities
	private Vector3D _X;  // X axis of "shape" frame
	private Rotation _rotation; // rotation from body frame to sub-part frame


	// Mimics calculations made by STELA
	// The transformation defined by orientation vector, angle, position (STELA convention)
	// The transformation transforms [0,0,1] -> orientation, and then applies the rotation 
	// of angle ang around orientation
	// position is not affected by the rotations. 
	AreaTransform(Vector3D orientation, double ang, Vector3D position) 
	{

		_orient = orientation; 
		_ang = ang; 

		// see: StelaShape.applyRotationPoint
		if (orientation.dotProduct(orientation) != 0) {
			Rotation rot1 = new Rotation(Zref, orientation);
			Vector3D x1 = rot1.applyTo(Xref); 

			Rotation rot2 = new Rotation(orientation, ang);
			_X = rot2.applyTo(x1); 
			_rotation = rot2.applyTo(rot1); // combined rot rot1->rot2

		}
		else {
			_X = Xref; 
			_rotation = Rotation.IDENTITY; 

		}

		_position = position; 

	}

	// Another way to define the same info
	// initialization with orientation (Z axis), X axis and position
	AreaTransform(Vector3D orientation, Vector3D X, Vector3D position) 
	{

		_orient = orientation; 

		// see: StelaShape.applyRotationPoint
		if (orientation.dotProduct(orientation) != 0) {
			Rotation rot1 = new Rotation(Zref, orientation);
			Vector3D x1 = rot1.applyTo(Xref); 
			
			Vector3D u1 = Vector3D.crossProduct(orientation, x1); 
			Vector3D u =  Vector3D.crossProduct(orientation, X); 
			
			double ang = Vector3D.angle(u1, u); 
			if (Vector3D.dotProduct(Vector3D.crossProduct(u1, u), orientation) < 0) ang = -ang; 

			Rotation rot2 = new Rotation(orientation, ang);
			
			_X = rot2.applyTo(x1); 
			_rotation = rot2.applyTo(rot1); // combined rot rot1->rot2
			_ang = ang; 

		}
		else {
			_X = Xref; 
			_rotation = Rotation.IDENTITY; 
			_ang = 0; 

		}

		_position = position; 

	}

	// apply new rotation R(axis, ang) to transformation
	// axis is defined relatively to the body frame
	// rcen = rotation center in sub-part frame (origin = origin of sub-part)
	AreaTransform applyRot(Vector3D axis, double ang, Vector3D rcen) 
	{

		Rotation rot; 
		if (Vector3D.dotProduct(axis, axis) != 0)
			rot = new Rotation(axis, ang);
		else
			rot = Rotation.IDENTITY; 

		Vector3D X = rot.applyTo(_X);  // New X axis
		Vector3D Z = rot.applyTo(_orient); // New Z axis

		Rotation rot1 = new Rotation(Zref, Z);
		Vector3D X1 = rot1.applyTo(Xref); 

		// ang: rotation: X1 => X
		ang = Vector3D.angle(X1, X); 
		if (Vector3D.dotProduct(Vector3D.crossProduct(X1,X), Z) < 0) ang = -ang; 

		// combined transformation
		Vector3D dpos = _rotation.applyTo(rcen); // => coord in body frame
		Vector3D ddpos = dpos.subtract(rot.applyTo(dpos)); 
		return new AreaTransform(Z, ang, _position.add(ddpos)); 
	}


	Vector3D getOrientation() {
		return _orient;
	}

	double getAngle() {
		return _ang;
	}

	Vector3D getXaxis() {
		return _X;
	}

	Vector3D getPosition() {
		return _position;
	}

}
