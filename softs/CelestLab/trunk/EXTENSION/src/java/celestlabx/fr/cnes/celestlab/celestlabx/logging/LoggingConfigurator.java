//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

package fr.cnes.celestlab.celestlabx.logging;

import org.apache.log4j.Logger;
import org.apache.log4j.Level;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.FileAppender;

/**
 * Class LoggingConfigurator
 * This class is used to configure the logs (verbosity, log file path, ...)
 */
public class LoggingConfigurator
{
    private static final Logger logger = Logger.getLogger(LoggingConfigurator.class);

    private static LoggingConfigurator instance;

    private boolean logStela;
    FileAppender logFileAppender;

    /**
     * Returns the singleton instance
     */
    public static LoggingConfigurator getInstance()
    {
        if (instance == null)
            instance = new LoggingConfigurator();
        return instance;
    }

    private LoggingConfigurator()
    {
        logStela = false;
        logFileAppender = new FileAppender();
    }

    /**
     * Sets the log verbosity
     * @param levelStr verbosity level (ALL, TRACE, DEBUG, INFO, WARN, ERROR, FATAL, OFF)
     */
    public static void setLogVerbosity(String levelStr)
    {
        getInstance().setLogLevel(Level.toLevel(levelStr));
    }

    /**
     * Returns the log verbosity
     * @return the log verbosity {@link String)
     */
    public static String getLogVerbosity()
    {
        return getInstance().getLogLevel().toString();
    }

    /**
     * Returns the activation of pulling up of STELA logs
     * @return a boolean, true if STELA logs are pulled up with the CelestLabX logs, false otherwise
     */
    public static boolean getLogStela()
    {
        return getInstance().logStela;
    }

    /**
     * Activates or not the pulling up of STELA logs
     * @return a boolean, true if STELA logs are pulled up with the CelestLabX logs, false otherwise
     */
    public static void setLogStela(boolean value)
    {
        logger.debug(value?"Enable STELA logs":"Disable STELA logs");
        getInstance().logStela = value;
    }

    /**
     * Sets the log file path
     * @param filePath log file path, if empty no log file will be created
     */
    public static void setLogFile(String filePath)
    {
        if (filePath != null && !filePath.isEmpty())
            getInstance().enableLogFile(filePath);
        else
            getInstance().disableLogFile();
    }

    /**
     * Returns the log file path
     * @return the log file path {@link String)
     */
    public static String getLogFile()
    {
        return getInstance().getLogFilePath();
    }

    /**
     * Sets the log verbosity level
     * @param level verbosity level {@link org.apache.log4j.Level}
     */
    void setLogLevel(Level level)
    {
        Logger.getRootLogger().setLevel(level);
        logger.debug(String.format("Set log verbosity to %s", level.toString()));
    }

    /**
     * Returns the log verbosity level
     * @param level log verbosity level {@link org.apache.log4j.Level}
     */
    Level getLogLevel()
    {
        return Logger.getRootLogger().getLevel();
    }

    /**
     * Activates the file logging, use a Log4j FileAppender
     * @param filePath log file path {@link String)
     */
    void enableLogFile(String filePath)
    {
        logger.debug(String.format("Enable logging to file: %s", filePath));

        // Remove FileAppender if it already exists
        Logger rootLogger = Logger.getRootLogger();
        if (rootLogger.isAttached(logFileAppender))
            rootLogger.removeAppender(logFileAppender);

        // Recreate a FileAppender at specified file path
        logFileAppender = null;
        try
        {
            // Format of file log: "[date] - [source] - [level]: [message]"
            PatternLayout layout = new PatternLayout("%d - %c - %-5p: %m%n");
            logFileAppender = new FileAppender(layout, filePath);
            rootLogger.addAppender(logFileAppender);
        }
        catch(Exception e)
        {
            logger.error(String.format("Cannot create log file %s: %s", filePath, e.getMessage()));
        }
    }

    /**
     * Disables the logging to file, detachs the Log4J FileAppender
     */
    void disableLogFile()
    {
        logger.debug("Disable logging to file");
        Logger rootLogger = Logger.getRootLogger();
        if (rootLogger.isAttached(logFileAppender))
        {
            rootLogger.removeAppender(logFileAppender);
        }
    }

    /**
     * Returns the log file path
     * @return the log file path {@link String)
     */
    String getLogFilePath()
    {
        if (logFileAppender != null)
            return logFileAppender.getFile();
        else
            return "";
    }
}
