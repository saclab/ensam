//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.stela;

import fr.cnes.celestlab.celestlabx.generic.Params;
import fr.cnes.celestlab.celestlabx.generic.ParamException;
import fr.cnes.los.stela.commons.model.AdvancedParameters;
import fr.cnes.los.stela.commons.model.DefaultValues;
import fr.cnes.los.stela.commons.model.atmosmodel.AtmosphericModelType;
import fr.cnes.los.stela.commons.model.dragcoeff.DragCoef;
import fr.cnes.los.stela.commons.model.ephemeris.FrameTypes;
import fr.cnes.los.stela.commons.model.solaractivity.ISolarActivity;
import fr.cnes.los.stela.commons.model.solaractivity.SolarActType;
import fr.cnes.los.stela.elib.business.implementation.solaractivity.constantmodel.model.ConstantSolarActivityModelFactory;
import fr.cnes.los.stela.elib.business.implementation.solaractivity.variablemodel.model.VariableSolarActivityModelFactory;
import fr.cnes.los.stela.elib.business.implementation.dragcoef.constant.model.ConstantDragCoef;
import fr.cnes.los.stela.elib.business.implementation.dragcoef.variable.model.VariableDragCoef;
import fr.cnes.los.stela.elib.business.implementation.dragcoef.cook.CookDragCoef;

import org.apache.log4j.Logger;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * Class SimulationData
 * This class is used to initialize the data coming from Scilab
 * (simulation parameters, ut1-tref, tt-tref) 
 * No getter/setter for this class: direct access from the "Simulation" class
 * (for simplification reasons) 
 */
class SimulationData
{
	private static final Logger logger = Logger.getLogger(SimulationData.class);

	// data necessary for a defining a simulation (models, forces, satellite properties)
	double ut1_tref; // UT1 minus TREF (as in CelestLab)
	double tt_tref;  // TT minus TREF  (as in CelestLab)

	// satellite properties
	double mass;
	double drag_area; 
	DragCoef drag_coef; 
	double srp_area; 
	double srp_coef; 

	AtmosphericModelType atmosModelType; 
	ISolarActivity solarActivity; 
	FrameTypes ref_frame; // reference frame: CIRF or MOD or ICRF

	// perturbations taken into account 
	boolean central_enabled; 
	boolean drag_enabled; 
	boolean srp_enabled; 
	boolean moon_enabled; 
	boolean sun_enabled; 
	boolean zonal_enabled; 
	boolean tesseral_enabled; 
	boolean appAcc_enabled; // NB: set automatically 

	// degre / order of Earth potential 
	int zonal_maxDeg; 
	int tesseral_maxDeg; 
	double tesseral_minPeriod; 

	// integrator (mandatory)
	double integrator_step; 

	// advanced parameters 

	int drag_nbQuadPoints; 
	int drag_nbComputeSteps; 
	int srp_nbQuadPoints; 
	int appAcc_nbQuadPoints; 

	int thirdBody_degree; 

	double reentryAltitude; 
	double drag_cookWallTemp; 
	double drag_cookAccomodCoef; 
	
	// data files
	String physicalConstantsFile = ""; 
	String solarActivityFile = ""; 
	String aeroCoefFile = ""; 
	
	// true if solarActivityFile has been copied
    boolean solarActivityFileDup = false; 

	/**
	 * Constructor
	 * Does nothing
	 */
	SimulationData()
	{
	}

	/**
	 * Initialises the parameters of the simulation
	 * Note the "reference" time scale in STELA is UT1
	 * The simulation uses the STELA "GTO" model
	 * @param params: structure containing the extrapolation parameters
	 * All parameters should be present
	 * @param ut1_tref: UT1 minus TREF (s)
	 * @param tt_tref: TT minus TREF (s)
	 * @return info: status (error message) 
	 * @throws Exception
	 */
	void initialize(Params params, double param_ut1_tref, double param_tt_tref) throws Exception
	{
		// integrator
		integrator_step = params.getDouble("integrator_step");

		// time
		ut1_tref = param_ut1_tref; 
		tt_tref = param_tt_tref; 

		// object's properties
		mass = params.getDouble("mass");
		drag_area = params.getDouble("drag_area");
		drag_coef = paramsDragCoef(params); 
		srp_area = params.getDouble("srp_area"); 
		srp_coef = params.getDouble("srp_coef"); 

		// solar activity 
		solarActivity = paramsSolarActivity(params); 

		// reference frame
		ref_frame = paramsRefFrame(params); 

		central_enabled = params.getBoolean("central_enabled"); 
		drag_enabled = params.getBoolean("drag_enabled"); 
		srp_enabled = params.getBoolean("srp_enabled"); 
		moon_enabled = paramsMoonSwitch(params); 
		sun_enabled = paramsSunSwitch(params); 
		zonal_enabled = params.getBoolean("zonal_enabled"); 
		tesseral_enabled = params.getBoolean("tesseral_enabled"); 

		appAcc_enabled = true; 
		if (ref_frame == FrameTypes.CIRF) appAcc_enabled = false; 

		// number of coefficients
		zonal_maxDeg = params.getInteger("zonal_maxDeg"); 
		tesseral_maxDeg = params.getInteger("tesseral_maxDeg"); 
		tesseral_minPeriod = params.getDouble("tesseral_minPeriod"); 

		// advanced parameters
	
		if (params.exists("drag_atmosphericModel"))
			atmosModelType = paramsDragAtmosphericModel(params); 
		else
			atmosModelType = AtmosphericModelType.MSIS00; 

		if (params.exists("drag_nbQuadPoints"))
			drag_nbQuadPoints = params.getInteger("drag_nbQuadPoints"); 
		else
			drag_nbQuadPoints = DefaultValues.GTO_QUADRATURE_POINTS; 

		if (params.exists("srp_nbQuadPoints"))
			srp_nbQuadPoints = params.getInteger("srp_nbQuadPoints"); 
		else
			srp_nbQuadPoints = DefaultValues.GTO_SRP_QUADRATURE_POINTS; 

		if (params.exists("appAcc_nbQuadPoints"))
			appAcc_nbQuadPoints = params.getInteger("appAcc_nbQuadPoints"); 
		else
			appAcc_nbQuadPoints = AdvancedParameters.COMPL_ACC_QUADRATURE_POINTS; 

		if (params.exists("drag_nbComputeSteps"))
			drag_nbComputeSteps = params.getInteger("drag_nbComputeSteps"); 
		else
			drag_nbComputeSteps = DefaultValues.GTO_ATMOSDRAG_RECOMPUTE_PERIOD_IN_STEPS; 

		if (params.exists("thirdbody_degree"))
			thirdBody_degree = params.getInteger("thirdbody_degree"); 
		else
			thirdBody_degree = AdvancedParameters.THIRD_BODY_POT_DEGREE_GTO; 

		if (params.exists("adv.reentryAltitude"))
			reentryAltitude = params.getDouble("adv.reentryAltitude"); 
		else
			reentryAltitude = DefaultValues.REENTRY_ALTITUDE_GTO; 

		if (params.exists("adv.drag_cookWallTemp"))
			drag_cookWallTemp = params.getDouble("adv.drag_cookWallTemp"); 
		else
			drag_cookWallTemp = AdvancedParameters.COOK_WALL_TEMPERATURE; 

		if (params.exists("adv.drag_cookAccomodCoef"))
			drag_cookAccomodCoef = params.getDouble("adv.drag_cookAccomodCoef"); 
		else
			drag_cookAccomodCoef = AdvancedParameters.COOK_ACCOMODATION; 

		
		if (params.exists("physicalConstantsFile")) {
			physicalConstantsFile = params.getString("physicalConstantsFile"); 
		}
		
		if (params.exists("solarActivityFile")) {
			solarActivityFile = params.getString("solarActivityFile"); 
			
			// Copy of file if extension is "act"
			// + Ap values are rounded to integers
			// if errors or file doesn't exist => does nothing
			if (drag_enabled && solarActivity.getSolActType() == SolarActType.VARIABLE &&
			       extension(solarActivityFile).equals(".act")) {
				String tmp = copySolarActivityFile(solarActivityFile); 
				if (tmp != null) {
					solarActivityFile = tmp; 
					solarActivityFileDup = true; 
				}
			}
		}			

		if (params.exists("aeroCoefFile")) {
			aeroCoefFile = params.getString("aeroCoefFile"); 
		}

	}

	
	/**
	 * cleaning
	 */
	void clean() 
	{
		if (solarActivityFileDup) {
			File file = new File(solarActivityFile); 
			file.delete(); 
		}
	}

	
	// ----------------------------------------------
	// Utilities (internal functions) : 
	// extract information from "params" structure
	// ----------------------------------------------

	/**
	 * Extract solar activity
	 */
	private ISolarActivity paramsSolarActivity(Params params) throws Exception
	{
		ISolarActivity solarActivity;
		String dragSolarActivityType = params.getString("drag_solarActivityType");
		if (dragSolarActivityType.equals("constant")) {
			solarActivity = new ConstantSolarActivityModelFactory().createSolarActivity();
			solarActivity.setConstantAP(params.getDouble("drag_solarActivityAp"));
			solarActivity.setConstantF107(params.getDouble("drag_solarActivityFlux"));
			return solarActivity;
		}
		else if (dragSolarActivityType.equals("variable")) {
			solarActivity = new VariableSolarActivityModelFactory().createSolarActivity();
			return solarActivity;
		}
		else throw new ParamException("drag_solarActivityType",
				String.format("unknown value '%s'", dragSolarActivityType));
	}

	/**
	 * Extract atmospheric model
	 */
	private AtmosphericModelType paramsDragAtmosphericModel(Params params) throws ParamException
	{
		String dragAtmosphericModelStr = params.getString("drag_atmosphericModel");
		if (dragAtmosphericModelStr.equals("NRLMSISE-00"))
			return AtmosphericModelType.MSIS00;
		else
			throw new ParamException("drag_atmosphericModel",
					String.format("unknown value '%s'", dragAtmosphericModelStr));
	}

	/**
	 * Extract drag coefficient
	 */
	private DragCoef paramsDragCoef(Params params) throws ParamException
	{
		String dragCoefType = params.getString("drag_coefType");
		if (dragCoefType.equals("variable"))
			return new VariableDragCoef();
		else if (dragCoefType.equals("constant"))
			return new ConstantDragCoef(params.getDouble("drag_coef"));
		else if (dragCoefType.equals("cook"))
			return new CookDragCoef();
		else
			throw new ParamException("drag_coefType",
					String.format("unknown value '%s'", dragCoefType));
	}

	/**
	 * Extract Moon switch
	 */
	private boolean paramsMoonSwitch(Params params) throws ParamException
	{
		String[] bodies = paramsThirdBodyBodies(params);
		boolean moonSwitch = (bodies != null) && (Arrays.asList(bodies).contains("moon"));
		logger.debug(String.format("Moon switch : %s", moonSwitch));
		return moonSwitch;
	}

	/**
	 * Extract Sun switch
	 */
	private boolean paramsSunSwitch(Params params) throws ParamException
	{
		String[] bodies = paramsThirdBodyBodies(params);
		boolean sunSwitch = (bodies != null) && (Arrays.asList(bodies).contains("sun"));
		logger.debug(String.format("Sun switch : %s", sunSwitch));
		return sunSwitch;
	}

	/**
	 * Extract list of bodies for 3rd body perturbation
	 */
	private String[] paramsThirdBodyBodies(Params params) throws ParamException
	{
		String[] bodies = null;
		if (params.getBoolean("thirdbody_enabled")) {
			bodies = params.getStringArray("thirdbody_bodies");
			for (int i = 0; i < bodies.length; i++) { 
				String body = bodies[i].toLowerCase();
				if (!body.equals("moon") && !body.equals("sun")) { 
					throw new ParamException("thirdbody_bodies",
							String.format("unknown value '%s'", bodies[i]));
				}
				bodies[i] = body;
			}
		}
		return bodies;
	}

	/**
	 * Extract reference frame from parameters
	 */
	private FrameTypes paramsRefFrame(Params params) throws ParamException
	{
		String frame_name = params.getString("ref_frame"); 
		FrameTypes frame_type = FrameTypes.CIRF; 

		if (frame_name.equals("CIRS") || frame_name.equals("CIRF") || frame_name.equals("ECI")) {
			frame_type = FrameTypes.CIRF; 
		}
		else if (frame_name.equals("MOD")) {
			frame_type = FrameTypes.CELESTIAL_MEAN_OF_DATE; 
		}
		else if (frame_name.equals("ICRS") || frame_name.equals("ICRF") || 
				frame_name.equals("GCRS") || frame_name.equals("GCRF")) {
			frame_type = FrameTypes.ICRF; 
		}
		else {
			throw new ParamException("ref_frame",
					String.format("Not allowed frame '%s'", frame_name));
		}
		return frame_type;
	}
	
	
	/**
	 * extension of path ("." included) 
	 * (should go somewhere else...)
	 */
	private String extension(String path)
	{
	
      String name = path; 
	  String suffix = ""; 
	  int index; 
	    
	  index = name.lastIndexOf("/");
	  if (index >= 0) name = path.substring(index+1); 
   
      index = name.lastIndexOf(".");
	  if (index >= 0) suffix = name.substring(index); 
	  
	  return suffix; 
    }

	/**
	 * copy of solar activity file ("act" format)
	 * (temporary ?)
	 */
	private String copySolarActivityFile(String path) 
	{
					
		BufferedReader input = null; 
		BufferedWriter output = null; 
		File tmpfile = null; 
		
		try {
            tmpfile = File.createTempFile("solarActivity", ".tmp"); 
			input = new BufferedReader(new FileReader(path));
            output = new BufferedWriter(new FileWriter(tmpfile));
            
            output.write("# version 1\n"); 
            
			String line; 
      	    StringBuffer sb = new StringBuffer(); 


			while ((line = input.readLine()) != null) {
              if (line.startsWith("#")) continue; 
              
              sb.setLength(0); 
              StringTokenizer strToken = new StringTokenizer(line, " ");
        	    
              if (strToken.countTokens() == 11) { 

                  for (int k = 0; k < 3; k++) { 
                     sb.append(strToken.nextToken()); 
                     sb.append(" "); 
                  } 

                  for (int k = 0; k < 8; k++) {
                     int val = (int) Math.round(Double.parseDouble(strToken.nextToken()));
                     sb.append(val); 
                     sb.append(" "); 
                  }
              }
              else {
            	sb = new StringBuffer(line); 
              }   
            
              output.write(sb.toString()); 
              output.newLine(); 
			}
			
		} catch (IOException e1) {
			if (tmpfile != null) tmpfile.delete(); 
			tmpfile = null; 
						
		} finally {
            // Finalization
				try {
					if (input != null) input.close();
		            if (output != null) output.close();
				} catch (IOException e) {
				} 	
		}
		
		String path_copy = null; 
		if (tmpfile != null) path_copy = tmpfile.getPath(); 

		return path_copy; 
	}


}
