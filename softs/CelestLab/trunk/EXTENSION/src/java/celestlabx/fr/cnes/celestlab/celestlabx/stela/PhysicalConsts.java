//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.stela;

import java.util.HashMap;
import java.util.NavigableMap;
import java.util.TreeMap;

import fr.cnes.los.stela.commons.model.PhysicalConstants;

/** 
 * Class PhysicalConsts
 * This object gathers some physical constants used by STELA
 * 
 * Units are USI (m, rad, s)
 * - astronomical unit
 * - SRP at 1 au
 * - equatorial radius
 * - oblateness
 * - gravitational constant for Earth, Sun, Moon
 * - j1jn (Nx1):  index 0 => order 1 
 * - cs1nm (NxN): index 0 => degree/order 1
 */
public class PhysicalConsts {
  static {
	  // load STELA constants; 
	  PhysicalConstants.init();
  }
  
  // max size for zonal or tesseral coeff (for put)
  final int MAXSIZE = 15; 

  // zonal and tesseral coefficients
  // index 0 <=> order 1: _j1jn[0] <=> J1, _c1nm[0,1] <=> C12 
  private double[] j1jn;
  private double[][] c1nm, s1nm;

  // key/value pairs for physical constants
  private HashMap<String, Double> keyval; 
  
  
  // constructor: initializes data (in particular for potential)
  public PhysicalConsts() {
	keyval = new HashMap<String, Double>(); 
	keyval.put("au", PhysicalConstants.ASTRO_UNIT); 
	keyval.put("srp_1au", PhysicalConstants.C0); 
	keyval.put("er", PhysicalConstants.EARTH_RADIUS); 
	keyval.put("obla", 1.0 / PhysicalConstants.FLAT_INVERSE); 
	keyval.put("mu", PhysicalConstants.MU); 
	keyval.put("muMoon", PhysicalConstants.MU_MOON); 
	keyval.put("muSun", PhysicalConstants.MU_SUN); 

    getZonalCoefficients();
    getTesseralCoefficients();
  }

  // returns one parameter value 
  public double getVal(String name) {
	double val = Double.NaN; 
	try {
		val = keyval.get(name); 
	}
    catch (Exception e) {
    }
	return val; 
  }

  // returns all zonal terms  
  public double[] getJ1Jn() {
    return j1jn;
  }

  // returns all tesseral Cnm terms  
  public double[][] getC1nm() {
    return c1nm;
  }

  // returns all tesseral Snm terms  
  public double[][] getS1nm() {
    return s1nm;
  }

  // returns %t if OK (name exists)
  public boolean putVal(String name, double val) {
	// check value exit 
	if (keyval.containsKey(name)) { 
		keyval.put(name, val); 
		return true; 
    }
	else
		return false; 
  }
  
  // returns %t if OK (name exists)
  // size limited to MAXSIZE 
  public boolean putJ1Jn(double [] xj1jn) {
	int size = Math.min(xj1jn.length, MAXSIZE); 
	
	j1jn = new double[MAXSIZE]; 
	for (int i = 0; i < size; i++) {
		j1jn[i] = xj1jn[i]; 
	}
	return true; 
  }

  
  // returns %t if OK (name exists)
  // size limited to MAXSIZE 
  public boolean putCS1nm(double [][] xc1nm, double [][] xs1nm) {
	if (xc1nm.length != xs1nm.length) {
		return false; 
	}
	
	int size = Math.min(Math.max(xc1nm.length, xs1nm.length), MAXSIZE); 
	
	c1nm = new double[size][size]; 
	s1nm = new double[size][size]; 
	
	for (int i = 0; i < size; i++) {
		for (int j = 0; j < size; j++) {
			if (i < xc1nm.length && j < xc1nm[i].length) 
				c1nm[i][j] = xc1nm[i][j]; 
			if (i < xs1nm.length && j < xs1nm[i].length) 
				s1nm[i][j] = xs1nm[i][j]; 
		}
	}

	return true; 
  }

  
  // Update Stela constants
  // all constants should be initialized
  public void update() {
	  NavigableMap<Integer, Double> ZONAL_COEF;
	  NavigableMap<Integer, NavigableMap<Integer, double[]>> TESSERAL_COEF;
	  
	  // Zonal coef
      ZONAL_COEF = new TreeMap<Integer, Double>();
      
      for (int i = 0; i < j1jn.length; i++) {
        ZONAL_COEF.put(i+1, j1jn[i]);
      }
      
	  // Tesseral coef
      TESSERAL_COEF = new TreeMap<Integer, NavigableMap<Integer, double[]>>();
      
      for (int i = 0; i < c1nm.length; i++) {
    	  NavigableMap<Integer, double[]> t = new TreeMap<Integer, double[]>();
    	  
    	  for (int j = 0; j < c1nm[i].length; j++) {
    		  double[] val = new double[2]; 
    		  val[0] = c1nm[i][j]; 
    		  val[1] = s1nm[i][j]; 
    		  t.put(j+1, val);
    	  }
    	  
    	  TESSERAL_COEF.put(i+1, t);
      }

      // Update Stela constants
	  PhysicalConstants.ASTRO_UNIT = keyval.get("au"); 
	  PhysicalConstants.C0 = keyval.get("srp_1au"); 
	  PhysicalConstants.EARTH_RADIUS = keyval.get("er"); 
	  PhysicalConstants.FLAT_INVERSE = 1.0 / keyval.get("obla"); 
	  PhysicalConstants.MU = keyval.get("mu"); 
	  PhysicalConstants.MU_MOON = keyval.get("muMoon"); 
	  PhysicalConstants.MU_SUN = keyval.get("muSun"); 
	  
	  PhysicalConstants.ZONAL_COEF = ZONAL_COEF; 
	  PhysicalConstants.TESSERAL_COEF = TESSERAL_COEF; 
  }

  // reset stela constants
  public void reset() {
	  // load STELA constants from file; 
	  PhysicalConstants.init();
  }

  // Initializes zonal coefficients 
  private void getZonalCoefficients() {
	final int nmax = PhysicalConstants.getMaxZonalCoefficient(); 

    j1jn = new double[nmax]; 

    // tries all degrees from 1 to NMAX 
    // as there is no way to get the max number of elements from STELA
    for (int n = 1; n <= nmax; n++) {
      try {
        j1jn[n-1] = PhysicalConstants.getZonalCoefficient(n);
      } 
      catch (Exception e) {
      }
    }
  }

  // Initializes zonal coefficients (max degree/order = NMAX)
  private void getTesseralCoefficients() {
	final int[] NMMAX = PhysicalConstants.getMaxTesseralCoefficient(); 

    int nmax = NMMAX[0]; 
    int mmax = NMMAX[1]; 

    // set the "nmax x mmax" coefficients 
    c1nm = new double[nmax][mmax];
    s1nm = new double[nmax][mmax];

    double [] val; 
    for (int n = 1; n <= nmax; n++) {
      for (int m = 1; m <= mmax; m++) {
          try {
              val = PhysicalConstants.getTesseralCoefficient(n,m);
              c1nm[n-1][m-1] = val[0];
              s1nm[n-1][m-1] = val[1];
          } 
          catch (Exception e) { 
          }
      }
    }
  }

}
