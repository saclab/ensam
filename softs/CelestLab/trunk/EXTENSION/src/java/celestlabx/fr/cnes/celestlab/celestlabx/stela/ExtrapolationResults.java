//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.stela;

import fr.cnes.los.stela.commons.model.ephemeris.Bulletin;
import fr.cnes.los.stela.elib.business.framework.ephemeris.model.TransitionMatrixEphemeris;
import fr.cnes.los.stela.elib.business.implementation.simulation.gtosimulation.GTOSimulation;

import org.apache.log4j.Logger;

import java.util.ArrayList;

/**
 * Class ExtrapolationResults
 * This class encapsulates the extrapolation results
 */
public class ExtrapolationResults
{
    private static final Logger logger = Logger.getLogger(ExtrapolationResults.class);

    private GTOSimulation gtosim;
    private double[] delta_t;
    private double[][] meanParamMatrix;

    /**
     * Constructor
     * @param simulation the STELA simulation instance
     * @param delta_t the time offsets from intial date at which the results are computed
     */
    public ExtrapolationResults(Simulation simulation, double[] wanted_delta_t)
    {
        gtosim = simulation.getGTOSimulation();
        delta_t = wanted_delta_t;
    }

    /**
     * Returns the mean orbital elements 
     * @return the mean orbital elements ("cireq" type, "CelestLab" convention)
     */
    public double[][] getMeanParamMatrix()
    {
        return meanParamMatrix;
    }
  	
    /**
     * Computes the mean and osculating parameter result matrices
     * Results set to NaN if not available
     */
    void computeParamMatrices() 
	{
        logger.debug("Computing mean and osculating elements...");

        int size = delta_t.length;
        meanParamMatrix = new double[size][6];

        double[] NaNvector = { Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN, Double.NaN };

        ArrayList<Bulletin> ephemerisList = gtosim.getEphemerisManager().getEphemerisList();
        double step = gtosim.getIntegratorStep();

        for (int i = 0; i < size; i++)
        {
            int k = (int) Math.round(delta_t[i] / step);
            if (k < ephemerisList.size())
            {
                Bulletin bulletin = ephemerisList.get(k);

                // Equinoctial mean params
                meanParamMatrix[i] = getOrbElem(bulletin);
            }
            else
            {
                meanParamMatrix[i] = NaNvector;
            }
        }
	}

    /**
     * Returns the transition matrix
     * NB: each transition matrix is a column vector
     * @return the transition matrix
     * @throws Exception
     */
    public double[][] getTransitionMatrix() 
    {
        logger.debug("Extracting transition matrices...");

        int size = delta_t.length;
        double[][] transitionMatrix = new double[8*size][6];

        ArrayList<TransitionMatrixEphemeris> matrixList =
            gtosim.getEphemerisManager().getTransitionMatrixDataList();
        double step = gtosim.getIntegratorStep();

        for (int i = 0; i < size; i++)
        {
            int k = (int) Math.round(delta_t[i] / step);

            int offset = 8 * i;

            if (k < matrixList.size())
            {

                TransitionMatrixEphemeris matrix = matrixList.get(k);
                double[][] m = matrix.getTransitionMatrix();
                double[][] m2 = { {m[0][0], m[2][0], m[3][0], m[4][0], m[5][0], m[1][0]},
                                  {m[0][2], m[2][2], m[3][2], m[4][2], m[5][2], m[1][2]},
                                  {m[0][3], m[2][3], m[3][3], m[4][3], m[5][3], m[1][3]},
                                  {m[0][4], m[2][4], m[3][4], m[4][4], m[5][4], m[1][4]},
                                  {m[0][5], m[2][5], m[3][5], m[4][5], m[5][5], m[1][5]},
                                  {m[0][1], m[2][1], m[3][1], m[4][1], m[5][1], m[1][1]},
                                  {m[0][6], m[2][6], m[3][6], m[4][6], m[5][6], m[1][6]},
                                  {m[0][7], m[2][7], m[3][7], m[4][7], m[5][7], m[1][7]} };

                for (int y = 0; y < 8; y++)
                {
                    for (int x = 0; x < 6; x++)
                    {
                        transitionMatrix[offset + y][x] = m2[y][x];
                    }
                }
            }
            else
            {
                for (int y = 0; y < 8; y++)
                {
                    for (int x = 0; x < 6; x++)
                    {
                        transitionMatrix[offset + y][x] = Double.NaN;
                    }
                }
            }
      	}
        return transitionMatrix;
   	}


	/**
	 * Return Orbital elements from "bulletin"
	 * returned data: "cireq" type, CelestLab convention
	 */
	private double[] getOrbElem(Bulletin bulletin)
	{
		double[] p = bulletin.getPosVel().getOrbParams();
		// change order: STELA convention -> CelestLab convention
		return new double[] {p[0], p[2], p[3], p[4], p[5], p[1]};
	}
}
