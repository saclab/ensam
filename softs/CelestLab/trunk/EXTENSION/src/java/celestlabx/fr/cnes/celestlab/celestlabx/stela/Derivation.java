//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.stela;

import fr.cnes.celestlab.celestlabx.generic.Params;

import fr.cnes.los.stela.commons.logic.date.StelaDate;
import fr.cnes.los.stela.commons.model.ephemeris.Bulletin;
import fr.cnes.los.stela.commons.model.ephemeris.FrameTypes;
import fr.cnes.los.stela.commons.model.ephemeris.NatureTypes;
import fr.cnes.los.stela.commons.model.ephemeris.Type8PosVel;
import fr.cnes.los.stela.elib.business.implementation.diffeq.GTODiffEq;
import fr.cnes.los.stela.elib.business.implementation.diffeq.data.GTODiffEqData;

import org.apache.log4j.Logger;

/**
 * Class Derivation
 * This class provides an interface with Scilab for computing the time derivatives using Stela
 */
public class Derivation
{
    private static final Logger logger = Logger.getLogger(Derivation.class);

    private Simulation simulation; // simulation context
    private double[][] dxdt; // results

    /**
     * Constructor
     */
    public Derivation()
    {
        simulation = new Simulation();
    }

    /**
     * This method computes the derivatives of satellite state at given time steps.
     * @param cjd: epochs (CJD, TREF time scale)
     * @param x: mean orbital elements ("cireq" type, CelestLab convention)
     * @param params: structure containing the model parameters
     * @param res UNUSED
     * @param ut1_tref: UT1 minus TREF (s)
     * @param tt_tref: TT minus TREF (s)
     * @return info: information about the computation
     * @throws Exception
     */
    
    public Information derivate(double[] cjd, double[][] x, Params params,
		int res, double ut1_tref, double tt_tref) 
    {
      Information info = _derivate(cjd, x, params, res, ut1_tref, tt_tref); 
		  simulation.clean();
		  return info; 
    }

    
	// same interface - low level	
    private Information _derivate(double[] cjd, double[][] x, Params params,
		int res, double ut1_tref, double tt_tref) 
    {
        logger.debug("Entering derivate function");

        if (params == null)
            return new Information(simulation, -1, "params argument is null");
        if (cjd == null || cjd.length == 0)
            return new Information(simulation, -1, "cjd argument is null or empty");
        if (x == null || x.length == 0)
            return new Information(simulation, -1, "x argument is null or empty");

        Information informations = simulation.initialize(params, ut1_tref, tt_tref);
        
        if (informations.getStatus() != 0)
            return informations;
        
        boolean centralEnabled = simulation.getCentralEnabled();

        // Set simulation initial state (1st orbital state)
        // NB: not mandatory for computation of derivatives
		Bulletin bulletin_init = makeBulletin(cjd[0], x[0]); 
		simulation.setInitialState(bulletin_init);
        
        int size = cjd.length;
        dxdt = new double[size][6];
        
        try
        {
            for (int i = 0; i < size; i++)
            {
                Bulletin bulletin = makeBulletin(cjd[i], x[i]);

                GTODiffEq eqdiff = simulation.getEqDiff(bulletin); 
                
                // Compute sum of derivatives
                double[] p = new double[6];
                eqdiff.computeStelaDerivatives((Type8PosVel)bulletin.getPosVel(), bulletin.getDate(), p);

                // Get results for each force
                GTODiffEqData eqdata = eqdiff.getDiffEqData(bulletin);
                double[][] data = eqdata.getData();

                // Substract central force (index 1 of data) if not wanted
                if (!centralEnabled)
                {
                    for (int j = 0; j < 6; j++)
                        p[j] -= data[1][j];
                }

                // generates results with "CelestLab" convention order
                dxdt[i] = new double[] {p[0], p[2], p[3], p[4], p[5], p[1]};
            }

        }
        
        catch(Exception e)
        {
            String errorMessage = String.format("Error while computing derivatives: %s", e.getMessage());
            return new Information(simulation, -1, errorMessage);
        }

        return new Information(simulation, 0, "");
    }

	
    /**
     * Returns the result derivative matrix
     * @return the derivative matrix
     */
    public double[][] getDerivativeMatrix()
    {
        return dxdt;
    }
    
	/**
	 * Make a "Bulletin" from date and position/velocity in CIRF
	 * @param cjd : date (TREF time scale)
	 * @param x : orbital elements ("cireq" type, CelestLab convention, frame = CIRF)
	 * @param nature: "MEAN" 
	 * @return the Bulletin
	 * NB: time scale: converted to UT1 (Stela convention)
	 */
	private Bulletin makeBulletin(double cjd, double[] x)
	{
		double ut1_tref = simulation.getUT1minusTREF(); 
		// Date is set to UT1 time scale
		StelaDate stelaDate = new StelaDate(cjd + ut1_tref / 86400);

		Type8PosVel param = new Type8PosVel(x[0], x[5], x[1], x[2], x[3], x[4],
				NatureTypes.MEAN, FrameTypes.CIRF);

		return new Bulletin(stelaDate, param);
	}
	
}