//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.stela;

import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;
import java.util.List;
import java.util.ArrayList;

/** 
 * Class Informations
 * This class encpasulates the status of a simulation: return code, error message, simulation parameters XML
 */
public class Information
{
    private int _status;
    private String _errorMessage;
    private List<String> _simXML;

    private static final Logger logger = Logger.getLogger(Information.class);

    /**
     * Constructor
     * @param simulation the Simulation instance
     * @param status status of simulation: 0 (OK), 1 (Decayed), -1 (Error),
     */
    public Information(Simulation simulation, int status, String errorMessage)
    {
        _status = status;
        _errorMessage = errorMessage;
        try {
        	_simXML = createXML(simulation); 
        }
        catch (Exception e) {
        	_simXML = new ArrayList<String>(); 
        }
    }

    
    /**
     * Constructor
     * @param simulation the Simulation instance
     * @param status status of simulation: 0 (OK), 1 (Decayed), -1 (Error),
     */
    public Information(int status, String errorMessage)
    {
        _status = status;
        _errorMessage = errorMessage;
       	_simXML = new ArrayList<String>(); 
    }

    /**
     * Constructor (variant)
     * @param simulation the Simulation instance
     * @param status status of simulation: 0 (OK), 1 (Decayed), -1 (Error),
     */
    public Information(int status, String errorMessage, List<String> simXML)
    {
        _status = status;
        _errorMessage = errorMessage;
       	_simXML = simXML; 
    }

    /**
     * Returns the status of the simulation
     * @return the simulation status {@link Integer} : 0 (OK), 1 (Decayed), -1 (Error)
     */
    public int getStatus()
    {
        return _status;
    }

    /**
     * Returns the message associated with the error that occured during the simulation
     * @return the error message {@link String}
     */
    public String getErrorMessage()
    {
        return _errorMessage;
    }
    
    /**
     * Returns the simulation parameters XML in STELA XML format
     * @return the simulation parameters XML content {@link String}
     * @throws Exception
     */
    public List<String> getSimulationXML() 
    {
        return _simXML;
    }

    
    /**
     * Returns the simulation parameters XML in STELA XML format
     * @return the simulation parameters XML content {@link String}
     * @throws Exception
     */
    private List<String> createXML(Simulation simulation) throws Exception
    {
        // Save XML informations to temp file
        File file = File.createTempFile("simulation", ".xml");
        String fileName = file.getAbsolutePath();
        simulation.save(fileName);

        // Load XML
        List<String> simulationXML = new ArrayList<String>();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        try
        {
            String line = bufferedReader.readLine();

            while (line != null)
            {
                simulationXML.add(line);
                line = bufferedReader.readLine();
            }
        }
        catch(Exception e)
        {
            logger.error(String.format("Error while loading temporary simulation XML file %s",
                fileName, e.getMessage()));
            throw e;
        }
        finally
        {
            bufferedReader.close();
        }

        // delete tmp file
        file.delete(); 
        
        return simulationXML;
    }
}
