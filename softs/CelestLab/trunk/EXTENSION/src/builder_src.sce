//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function builder_src()
  langage_src = ["cpp", "java"];
  path_src = get_absolute_file_path("builder_src.sce");
  tbx_builder_src_lang(langage_src, path_src);
endfunction

builder_src();
clear builder_src;
