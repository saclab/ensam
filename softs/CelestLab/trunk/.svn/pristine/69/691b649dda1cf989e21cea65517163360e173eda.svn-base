//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Sun beta angle (evolution over time): 
//> The orbit is circular. 
//> The angle is nearly constant for Sun synchronous orbits (but not quite). 
//> Variations are due to the yearly variations of the Sun direction.  
//
// Author: A. Lamy
// -----------------------------------------------------------


alt = 700.e3;  // altitude (m)
sso = 1; 
inc = 0; 
hloc0 = 0:3:22; // local time (h)
T = CL_unitConvert(1,"yr","day"); // simu duration (days)

desc_param = list(..
  CL_defParam("Altitude", alt, units=['m','km'], valid='$x>=0'),..
  CL_defParam("Sun-synchronous orbit? (1=yes, 0=no)", sso, id='$sso', accv=[0,1]),..
  CL_defParam("Inclination", inc, units=['rad','deg'], id='$inc', valid='$sso == 1 | ($inc>0 & $inc < 180)'),..
  CL_defParam("Values of initial mean local time of asc. node", hloc0, units=['h'], dim=[1, 20], valid='$x>=0 & $x<=24'),..
  CL_defParam("Simulation time length", T , units=['day','yr'], valid='$x>0 & $x<=10'),..
  CL_defParam("Show eclipse limit (1=yes, 0=no)", 1 , accv=[0,1])..
);

[alt, sso, inc, hloc0, T, show_ecl] = CL_inputParam(desc_param);

// -----------------------------------------------------------
// Computation
// -----------------------------------------------------------

// Initial date
cal = CL_dat_now("cal"); 
cjd0 = CL_dat_cal2cjd(cal(1),1,1,0,0,0);   // initial date

sma = alt + %CL_eqRad; // semi major axis
ecc = 0; // eccentricity
if (sso == 1); inc = CL_op_ssoJ2('i', sma, ecc); end

[pomdot, gomdot, anmdot] = CL_op_driftJ2(sma, ecc, inc);  // compute drifts due to j2

t = 0:ceil(T); // relative dates
pos_sun = CL_eph_sun(cjd0+t);  //compute sun position starting at t0 (in ECI)
pos_sun_sph = CL_co_car2sph(pos_sun); // spherical coordinates

// beta angle for each initial local time 
result = []; 

for k = 1:length(hloc0)
  gom0 = CL_op_locTime(cjd0, 'mlh', hloc0(k), 'ra');  // initial right ascension of RAAN
  gom = gom0 + gomdot*t*86400; // raan =f(t)
  betaa = CL_gm_raan2beta(inc, gom, pos_sun_sph(1,:), pos_sun_sph(2,:));
  result = [result; betaa]; 
end


// -----------------------------------------------------------
// Plot
// -----------------------------------------------------------

f = scf(); //create figure
f.visible = "on";
f.immediate_drawing="off";

nb = length(hloc0);
f.color_map = jetcolormap(min(max(nb,3),100));
Noir = addcolor([0,0,0]);

a = gca();

for k = 1:length(hloc0)
  plot(t, result(k,:) * %CL_rad2deg);
  h = CL_g_select(gce(), "Polyline");
  h.thickness = 2;
  h.foreground = k; 
end

CL_g_tag(a,0);

// limit angle for eclipse
if (show_ecl == 1)
  beta_ang_lim = asin(%CL_eqRad / sma);
  plot(t, beta_ang_lim*ones(t) * %CL_rad2deg, "k");
  plot(t, -beta_ang_lim*ones(t) * %CL_rad2deg,"k");
end

CL_g_tag(a,1);

a.title.text = "Evolution of beta angle";
a.x_label.text = msprintf("Days from January 1st (%d)", cal(1));
a.y_label.text = "Beta angle (deg)";

a.data_bounds = [t(1), max(a.y_ticks.locations(1), -90); 
                 t($), min(a.y_ticks.locations($), 90)]; 
a.tight_limits = "on";

h = CL_g_select(a, "Polyline", 1);
h.thickness = 3;
h.line_style = 3;

CL_g_legend(a, header = "Initial MLTAN (h)", str=string(hloc0));

CL_g_stdaxes(a);

f.immediate_drawing="on";
f.visible = "on";


