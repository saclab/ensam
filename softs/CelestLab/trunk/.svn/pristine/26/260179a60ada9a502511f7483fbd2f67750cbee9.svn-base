<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 Copyright (c) CNES  2008
 
 This software is part of CelestLab, a CNES toolbox for Scilab
 
 This software is governed by the CeCILL license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 'http://www.cecill.info'.
 -->
<!--
 * 
 * Reference frames.
 * 
 -->
<refentry version="5.0-subset Scilab" xml:id="Reference frames" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 27-Oct-2009 $</pubdate>
  </info>

  <refnamediv>
    <refname>Reference frames</refname>
    <refpurpose>Reference frames</refpurpose>
  </refnamediv>


  <refsection>
    <title>Reference frames available in CelestLab</title>
    <para>The following frames are defined in CelestLab:</para>
    <itemizedlist>
      <listitem><emphasis role="bold">ITRS</emphasis>: International Terrestrial Reference System </listitem>
      <listitem><emphasis role="bold">TIRS</emphasis>: Terrestrial Intermediate Reference System </listitem>
      <listitem><emphasis role="bold">CIRS</emphasis>: Celestial Intermediate Reference System </listitem>
      <listitem><emphasis role="bold">GCRS</emphasis>: Geocentric Celestial Reference System </listitem>
      <listitem><emphasis role="bold">ICRS</emphasis>: International Celestial Reference System </listitem>
      <listitem><emphasis role="bold">EME2000</emphasis>: Earth Mean Equator and Equinox at epoch J2000.0 </listitem>
      <listitem><emphasis role="bold">TOD</emphasis>: (mean equinox) True (equator) Of Date </listitem>
      <listitem><emphasis role="bold">MOD</emphasis>: (mean equinox) Mean (equator) Of Date </listitem>
      <listitem><emphasis role="bold">TEME</emphasis>: True Equator Mean Equinox of Date (used for TLEs) </listitem>
      <listitem><emphasis role="bold">EOD</emphasis>: (mean equinox mean) Ecliptic of Date </listitem>
      <listitem><emphasis role="bold">Veis</emphasis>: Gamma50 Veis </listitem>
      <listitem><emphasis role="bold">PEF</emphasis>: Pseudo Earth Fixed (identical to ITRS with zero-values polar coordinates) </listitem>
    </itemizedlist>
	
    <para>Note: PEF (Pseudo Earth Fixed) is defined for instance in <emphasis role="literal">Implementation Issues Surrounding the New IAU
Reference Systems for Astrodynamics</emphasis> by Vallado et al. </para>

    <para/>
    
    <para>In addition, CelestLab defines (see below for details): </para>
	<itemizedlist>
    <listitem><emphasis role="bold">ECI</emphasis> (Earth Centered Inertial) and 
	          <emphasis role="bold">ECF</emphasis> (Earth Centered (Earth) Fixed). </listitem>
    <listitem><emphasis role="bold">BCI</emphasis> (Body Centered Inertial) and 
	          <emphasis role="bold">BCF</emphasis> (Body Centered (Body) Fixed). </listitem>
	</itemizedlist>
	

	<para/>

  <imageobject><imagedata fileref="images/reference_frames.gif"/></imageobject>
  <para>The conversion from one frame to another can be done by the functions:  
  <link linkend="CL_fr_convert">CL_fr_convert</link> and 
	<link linkend="CL_fr_convertMat">CL_fr_convertMat</link>.
	Specific functions are defined for body centered frames (see below).</para>
	<para/>

	<para><emphasis role="bold">Important remark</emphasis>: 
  "Veis", "CIRS", "TOD", "TIRS", "PEF and "TEME" share the same (true) equatorial plane. 
  "Veis" and "TEME" (for instance) can be considered as modernized versions of the respective frames. 
  Their definitions might be slightly different from what can be found in other libraries. </para>

    <para/>
  </refsection>
  
  <refsection>
    <title>Models used for frame conversions</title>
	
    <para>The models used for frame conversions in CelestLab are those 
    recommended by the IERS 2010 convention (IERS Technical Note No. 36: <ulink url="http://www.iers.org/nn_11216/IERS/EN/Publications/TechnicalNotes/tn36.html">
	  http://www.iers.org/nn_11216/IERS/EN/Publications/TechnicalNotes/tn36.html</ulink>):</para>
	
    <itemizedlist>
    
    <listitem><para>
	  <emphasis role="bold">GCRS &#8594; ICRS</emphasis>: </para><para>
	  ICRS is considered as identical to GCRS in CelestLab.
	  </para><para/></listitem>
    
      <listitem><para>
	  <emphasis role="bold">GCRS &#8594; EME2000</emphasis>: </para><para>
	  The transformation from GCRS to EME2000 is a constant rotation (IAU2006 model). 
    <para> The rotation angle is close to 6.4E-6 deg, which corresponds to about 0.7m at 1 Earth radius. </para>   
	  </para><para/></listitem>
	  
      <listitem><para>
	  <emphasis role="bold">GCRS &#8594; MOD</emphasis>: </para><para>
	  The transformation from GCRS to MOD is a [3,1,3,1] rotation considering Fukushima-Williams 
	  precession angles (IAU2006 model).
	  </para><para/></listitem>
	  
      <listitem><para>
	  <emphasis role="bold">MOD &#8594; EOD</emphasis>: </para><para>
	  The transformation from MOD to EOD is a rotation about the X axis of the mean obliquity (IAU2006 model).
	  </para><para/></listitem>
	  
      <listitem><para>
	  <emphasis role="bold">MOD &#8594; TOD</emphasis>: </para><para>
	  The transformation from MOD to TOD is a [1,3,1] rotation using (IAU2006/2000A model) nutation angles. 
	  </para><para/></listitem>
	  
      <listitem><para>
	  <emphasis role="bold">GCRS &#8594; CIRS</emphasis>: </para><para>
	  The transformation from GCRS to CIRS can be done in several ways. 
      The method implemented in CelestLab is the one using the classical angles (IAU2006/2000A Model, CIO based). 
	  For computation efficiency and for dates in the time period [2000, 2100], the results are obtained by 
	  interpolation of pre-computed data. 
      Another method (IAU2006/2000A Model, CIO based, using XY series) is implemented in CelestLab but not available 
	  by default (See remark below).
	  </para><para/></listitem>
      
	  <listitem><para>
	  <emphasis role="bold">CIRS &#8594; TIRS</emphasis>: </para><para>
	  The transformation from CIRS to TIRS is a rotation around the Z axis of Earth 
	  Rotation Angle (ERA) (IAU2000 model).
    <para>Ocean tidal and libration effects in UT1 are ignored in CelestLab. The amplitude of the effect is a few microseconds on UT1.
    See chapter 5.5.3 of IERS 2010 conventions (IERS Technical Note No. 36) for more details.</para>
	  </para><para/></listitem>
	  
      <listitem><para>
	  <emphasis role="bold">TIRS &#8594; PEF</emphasis>: </para><para>
	  The transformation from TIRS to PEF is a rotation about the Z axis of the TIO locator (s') angle.
      The TIO locator is obtained from polar motion observations by numerical integration, and so is in essence unpredictable.
      However, it is dominated by a secular drift of 47 microarcseconds per century which is the approximation 
	  implemented (IAU2000 model).
	  </para><para/></listitem>
	  
      <listitem><para>
	  <emphasis role="bold">PEF &#8594; ITRS</emphasis>: </para><para>
	  The transformation from PEF to ITRS is a [2,1] rotation of polar motion angles (-xp,-yp). 
    The polar motion angles are unpredictable and do not have any model associated with them (*).
    Typical values range from 0.1 to 0.5 arcseconds.</para>
    <para>They can be retrieved in IERS bulletins at:
	   <ulink url="http://www.iers.org/IERS/EN/Publications/Bulletins/bulletins.html">
	  http://www.iers.org/IERS/EN/Publications/Bulletins/bulletins.html</ulink>.</para>
    <para/>
    <para>*: Polar motion is also affected by two (small) effects for which correction models exist but they are ignored in CelestLab.
    <para> - Diurnal and semi-diurnal variations due to the oceanic tides, with amplitudes of a fraction of 
             milliarcsecond.</para> 
    <para> - Libration in polar motion with an amplitude of about 10 microarcseconds.</para>
    <para>See chapter 5.5.1 of IERS 2010 conventions (IERS Technical Note No. 36) for more details.</para>
	  </para><para/></listitem>
      
	  <listitem><para>
	  <emphasis role="bold">Veis &#8594; PEF</emphasis>: </para><para>
	  The transformation from Veis to PEF is a rotation about the Z axis of the Veis sidereal time. 
	  (Reference: Georges Veis - the system reference, in Sao special report 200, 1st volume, 
	  1966 Veis sidereal time).
	  </para><para/></listitem>
	  
      <listitem><para>
	  <emphasis role="bold">TEME of date &#8594; PEF</emphasis>: </para><para>
	  The transformation from TEME (of date) to PEF is a rotation about the Z axis of the Greenwich mean sidereal time 
	  (IAU1982 model).
	  </para><para/></listitem>           
      
	  
   	</itemizedlist>

      <para><emphasis role="bold">Remark:</emphasis> 
	  Other IAU models are in fact available in CelestLab. The corresponding functions are not documented in the help files, 
    may change without notice, but could still be useful occasionally. The available models are: IAU2000 frame bias, 
	  IAU76 precession, IAU80 obliquity, IAU80 nutation and the IAU2006/2000A Model, CIO based, using XY series 
    model for GCRS &#8594; CIRS.</para>
    
    <para/>
  </refsection>

  <refsection>
    <title><emphasis role="bold">ECI</emphasis> and <emphasis role="bold">ECF</emphasis> frames</title>
	
    <para>For the analysis of orbits around Earth, it is convenient to use 2 particular reference frames: one (nearly) inertial and one 
	(nearly) tied to Earth so that the conversion from one of them to the other can be done by a single rotation around the Z axis. 
	In CelestLab, these 2 frames are called <emphasis role="bold">ECI</emphasis> (Earth Centered Inertial)
	and <emphasis role="bold">ECF</emphasis> (Earth Centered (Earth) Fixed) respectively. </para>
	<para><emphasis role="bold">ECI</emphasis> is typically used for the definition of orbits. 
	<emphasis role="bold">ECF</emphasis> is suitable for visibility calculations with ground locations for instance.</para>

	<para>The Default setting is such that:</para>
    <itemizedlist>
	<listitem><emphasis role="bold">ECI</emphasis> is identical to <emphasis role="bold">CIRS</emphasis>.</listitem>
    <listitem><emphasis role="bold">ECF</emphasis> is identical to <emphasis role="bold">TIRS</emphasis>.</listitem>
    </itemizedlist>

	<para>For compliance reasons with previous practices, it is possible to change this default setting so that: </para>
    <itemizedlist>
	<listitem><emphasis role="bold">ECI</emphasis> is identical to <emphasis role="bold">Veis</emphasis>.</listitem>
    <listitem><emphasis role="bold">ECF</emphasis> is identical to <emphasis role="bold">PEF</emphasis>.</listitem>
    </itemizedlist>

	<para>See <link linkend="Configuration">Configuration</link> for more details on how to configure preferences in CelestLab</para>
     
	<para/>

  </refsection>

  <refsection>
    <title><emphasis role="bold">BCI</emphasis> and <emphasis role="bold">BCF</emphasis> frames</title>
	
    <para>CelestLab defines body centered frames which are called: </para>
    <itemizedlist>
	<listitem><emphasis role="bold">BCI</emphasis>: Body Centered Inertial.</listitem>
	<listitem><emphasis role="bold">BCF</emphasis>: Body Centered (Body) Fixed.</listitem>
    </itemizedlist>

	<para>Frames are defined for all solar system planets except Earth, plus Moon and Sun. 
	Earth is excluded in order to avoid confusion with Earth centered frames. </para>

    <para>The conversion from one frame to another can be done by the functions: 
    <link linkend="CL_fr_bodyConvert">CL_fr_bodyConvert</link> and 
	<link linkend="CL_fr_bodyConvertMat">CL_fr_bodyConvertMat</link>.</para>
     
  </refsection>

  
  <para/>

</refentry>
