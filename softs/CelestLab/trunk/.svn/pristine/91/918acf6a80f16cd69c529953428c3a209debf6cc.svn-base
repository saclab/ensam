//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [varargout] = CL_dat_jd2cal(jd)
// Julian day to calendar date - DEPRECATED
//
// Calling Sequence
// [year,month,day,hour,minute,second] = CL_dat_jd2cal(jd)
// [cal] = CL_dat_jd2cal(jd) // cal=[year;month;day;hour;minute;second]
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_dat_convert">CL_dat_convert</link></p>
// <p></p></listitem>
// <listitem>
// <p>Converts a Julian day into a calendar date.</p> 
// <p>The output arguments are either: </p>
// <p> - year, month, day, hour, minute, second: 6 output arguments are expected.  </p>
// <p> - cal: colum vector containing year, month, day, hour, minute, second. </p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>- Years are counted astronomically. The year 1-BC is 0, 2-BC is -1, ... </p>
// <p>- The dates are relative to the Julian calendar before 4th october 1582 24h (Julian calendar), and to the Gregorian calendar after 5th October 1582 0h (Julian calendar), that is, after 15th october 1582 0h (Gregorian calendar). </p>
// <p>- Julian day 0 is 1st January -4712 12h (Julian calendar). </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Dates and time scales">Dates and time scales</link> for more details.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// jd: Julian day (number of days since 1st January -4712, 12h) (1xN)
// year: Year (1xN)
// month: Month [1..12] (1xN)
// day: Day [1..31] (1xN)
// hour: Hours (1xN)
// minute: Minutes (1xN)
// second: Seconds (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Jean Meeus - Astronomical Algorithms - 1991
// 2) IMCCE: <ulink url="http://www.imcce.fr">http://www.imcce.fr</ulink>
//
// See also
// CL_dat_cal2jd
// CL_dat_cal2cjd
//
// Examples
// jd1 = 2451544.5;
// [year,month,day,hour,minute,second] = CL_dat_jd2cal(jd1)
// d = CL_dat_jd2cal(jd1)
// jd2  = 2455248.002604167;
// [year,month,day,hour,minute,second] = CL_dat_jd2cal(jd2)
// d = CL_dat_jd2cal(jd2)
// [year,month,day,hour,minute,second] = CL_dat_jd2cal([jd1,jd2])
// d = CL_dat_jd2cal([jd1,jd2])

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

[lhs, rhs] = argn();

if (lhs <> 1 & lhs <> 6)
  CL__error("Wrong number of output arguments: 1 or 6 expected.");
end
if (rhs <> 1)
  CL__error("Wrong number of input arguments");
end


I = find(jd < 0);
if ~isempty(I)
  CL__error("Julian day cannot be negative");
end


// algorithm (see Meeus) 
// Note on int: int(1.5)=1, int(-1.5)=-1

jd = jd + 0.5; 
Z = int(jd); 
F = jd - Z;

A = Z; 
ALPHA = int((Z - 1867216.25)/36524.25);
k = find(Z >= 2299161); 
A(k) = Z(k) + 1 + ALPHA(k) - int(ALPHA(k)/4);         

B = A + 1524;
C = int((B - 122.1)/365.25);
D = int(365.25 * C);
E = int((B - D)/30.6001);

RD = B - D - int(30.6001*E) + F; // decimal day

day = floor(RD);

month = E - 1; 
k = find(E == 14 | E == 15); 
month(k) = E(k) - 13;   

year = C - 4716; 
k = find(month == 1 | month == 2); 
year(k) = C(k) - 4715; 


rh = (RD-day)*24;
hour = floor(rh);
minute = floor((rh-hour)*60); 
second = ((rh-hour)*60 - minute )*60; 


// output

if (lhs == 1)
  varargout(1) = [year;month;day;hour;minute;second];

else
  varargout(1) = year;
  varargout(2) = month;
  varargout(3) = day;
  varargout(4) = hour;
  varargout(5) = minute;
  varargout(6) = second;
end

endfunction
