// ------------------------------
// CL_cw_propagate : 
// ------------------------------

TEST_OK = [];

///////////////////////////////////////////////////
// Test1: validation with TRDV tool (ATV-CC FDS) //
///////////////////////////////////////////////////
t0 = 2600/86400 // days
t = 3200/86400 // days

// TRDV reference data
acc_ref = [0.00285349; -7.911e-05; -0.00354753]; // differential inertial acceleration
mu_ref = 3.9860047e+14;
er_ref = 6378137;
omega_ref = 0.00113386544;
alt_ref = ((mu_ref/omega_ref^2)^(1/3))-er_ref; // m
   // Initial statevector
pv0 = [-17710.1758;136.12911;5936.18619;10.0176205;0.189734193;0.814482829];
   // Reference statevector (obtained from TRDV)
pv_ref = [-11212.927170246;197.3839289449;5576.9947124105;10.915164894437;0.0065118798333346;-2.3565332866864];

pv = CL_cw_propagate(t0, pv0, t, alt_ref, acc_ref,er_ref ,mu_ref);

TEST_OK($+1) = CL__isEqual(pv,pv_ref,1.e-13);

///////////////////////////////////////////////////
// Test2: CL_cw_propagate allows backpropagation //
///////////////////////////////////////////////////
alt = 450.e3;
t0 = 3000/86400;
pv0 = [1;0;0;1;0;0];
t1 = [100, 200, 300, 400, 500]/86400; // Increasing time vector
t2 = [200, 400, 100, 500, 300]/86400; // Random time vector

pv_t1 = CL_cw_propagate(t0, pv0, t1, alt);
pv_t2 = CL_cw_propagate(t0, pv0, t2, alt);

TEST_OK($+1) = CL__isEqual(pv_t1(:,1),pv_t2(:,3)) & ..
                 CL__isEqual(pv_t1(:,2),pv_t2(:,1)) & ..
                 CL__isEqual(pv_t1(:,3),pv_t2(:,5)) & ..
                 CL__isEqual(pv_t1(:,4),pv_t2(:,2)) & ..
                 CL__isEqual(pv_t1(:,5),pv_t2(:,4));

///////////////////////////////
// Test3: acc optional input //
///////////////////////////////
alt = 450.e3;
t0 = 0;
pv0 = [1;0;0;1;0;0];  
t = (100:100:500)/86400;
acc = zeros(3,1);

pv = CL_cw_propagate(t0, pv0, t, alt);
pv_acc = CL_cw_propagate(t0, pv0, t, alt,acc);

TEST_OK ($+1) = CL__isEqual(pv,pv_acc); 

///////////////////////////////////////////////
// Test4: not input dates --> pv shall be [] //
///////////////////////////////////////////////
alt = 450.e3;
t0 = 0;
pv0 = [1;0;0;1;0;0];  
t = [];

pv = CL_cw_propagate(t0, pv0, t, alt);

TEST_OK($+1)=CL__isEqual(pv,[]);
