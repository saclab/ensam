//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [desc] = CL__configGetDesc()
// Returns a structure with configuration information
//
// Calling Sequence
// [desc] = CL__configGetDesc()
//
// Description
// <itemizedlist>
// <listitem>
// <p>The structure fields are: </p>
// <p>- names = vector of config options</p>
// <p>- accval = list of vectors of possible values</p>
// <p>- defval = vector of default values </p>
// <p>- settable = vector of indicators: changeable or not by CL_configSet</p>
// <p>- editable = vector of indicators: changeable or not by CL_editConfigFile</p>
// </listitem>
// </itemizedlist>
//
// Authors
// CNES - DCT/SB
//

// Declarations:

// Code:

desc = struct(); 

// names of possible options
desc.names = ["DISPLAY_MENU", "WARNING_MODE", "VERBOSE_MODE", "WARN_DEPRECATED", ..
              "ECI_FRAME", "TLE_WHICHCONST", "TLE_OPSMODE", "STELA_WHICHCONST"]; 

// names of possible values for each option
desc.accval = list(["yes", "no"], ["standard", "silent", "error"], ["standard", "silent"], ..
              ["yes", "no"], ["CIRS", "Veis"], ["wgs72", "wgs72old", "wgs84"], ["afspc", "impr"], ..
              ["CL", "ST"]); 

// default values 
desc.defval = ["yes", "standard", "standard", "yes", "CIRS", "wgs72", "a", "CL"]; 

// changeable by configSet ? 
desc.settable = [%f, %t, %t, %t, %f, %t, %t, %t]; 

// changeable by configFileEdit ? 
desc.editable = [%f, %t, %t, %t, %t, %t, %t, %t]; 

 
endfunction
