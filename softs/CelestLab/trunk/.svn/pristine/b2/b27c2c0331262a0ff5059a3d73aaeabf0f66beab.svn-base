//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Loads configuration file and returns a structure of (key, value) pairs
// => returns [] if at least one name or value is not correct
// NB: generates no error, returns error message in 2nd argument. 
// fname: name of configuration file to be read
// confst: structure of configuration parameters, [] is not OK. 
// errmsg: [] unless there is an error. 
function [confst, errmsg] = CLx__configLoad(fname)
confst = []; 
errmsg = []; 

// function to hide possibly unexpected variables in config file
// (config file assumed to contain lines such as: conf.xxx = value. Other lines: ignored) 
// returns [] if error occurred
function [conf] = configLoad(fname)
  conf = struct(); 
  err = exec(fname, 'errcatch', -1);
  if (err <> 0)
    conf = [];  
    return; 
  end
endfunction

// get the configuration description
config_desc = CLx__configGetDesc(); 

if (~isfile(fname))
  errmsg = "Configuration file not found";
  return; 
end

// Intialize configuration structure
conf = configLoad(fname); 
if (conf == [])
  errmsg = "Error loading configuration file"; 
  return; 
end

// field names in conf
conf_names = matrix(fieldnames(conf),1,-1); 

// Check that all configuration parameters exist in conf and vice-versa
if (setdiff(config_desc.names, conf_names) <> [] | ..
    setdiff(conf_names, config_desc.names) <> []) 
  errmsg = "Invalid configuration file (inconsistent parameter names)";
  return; 
end


// Check the values
for (name = conf_names)
  [ok, errmsg] = CLx__configCheckVal(config_desc, name, conf(name)); 
  if (~ok)
    return; 
  end
end

confst = conf; 

endfunction
