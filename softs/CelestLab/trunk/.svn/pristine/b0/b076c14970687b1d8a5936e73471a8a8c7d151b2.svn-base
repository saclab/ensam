// ------------------------------
// CL_rot_defRot1Ax: 
// ------------------------------

TEST_OK = [];

d = [ 2 ; 1 ; 2 ];

// Test 1: input vector u = 0.
u = [ 0 ; 0 ; 0 ];
try 
  CL_rot_defRot1Ax(u, 1, d, %pi/2);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 2: input vector u = [].
u = [];
try 
  CL_rot_defRot1Ax(u, 1, d, %pi/2);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 3: incorrect value of "numsol"
u = [ 1 ; 2 ; 3 ];

numsol = 3;
try 
  CL_rot_defRot1Ax(u, 1, d, %pi/2, numsol);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 4: incorrect value of "axis"
axis = 4;
try 
  CL_rot_defRot1Ax(u, axis, d, %pi/2);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 5: incorrect value of "target_ang"
target_ang = -1.0;
try 
  CL_rot_defRot1Ax(u, 1, d, target_ang);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

//Test 6: Rotation about x-axis that transforms
// u into a vector perpendicular to d.

[q, ang, Inok] = CL_rot_defRot1Ax(u, 1, d, %pi/2);

// Check result :
v = CL_rot_rotVect(q,u);
res1=CL_vectAngle(v,d)  // == %pi/2

TEST_OK($+1) = CL__isEqual( res1 , %pi/2 , 1e-15);

//Test 7: Rotation about x-axis that transforms
// u into a vector perpendicular to d with 2 solutions.
numsol = 2;
[q, ang, r, angr, Inok] = CL_rot_defRot1Ax(u, 1, d, %pi/2,numsol);

d2q = CL_rot_rotVect(q,u)
res1=CL_vectAngle(v,d) // == %pi/2
d2r = CL_rot_rotVect(r,u)
res2=CL_vectAngle(v,d) // == %pi/2

TEST_OK($+1) = CL__isEqual( res1 , %pi/2 , 1e-15);
TEST_OK($+1) = CL__isEqual( res2 , %pi/2 , 1e-15);


//Test 8: Rotation about an axis that transforms u
// into a vector as close as possible to d.
axis = [ -2 ; 3 ; 4 ];
[q, ang, Inok] = CL_rot_defRot1Ax(u, axis, d, 0);

// Check result :
ang = CL_vectAngle(d,CL_rot_rotVect(q,u))
angmin_approx = min(CL_vectAngle(d,CL_rot_rotVect( ..
CL_rot_axAng2quat(axis,linspace(0,2*%pi,200)),u)))

TEST_OK($+1) = CL__isEqual( ang , angmin_approx , 1e-4);

//Test 9: Rotation about y-axis that transforms
// u into a vector with 60 deg. to d.

[q, ang, Inok] = CL_rot_defRot1Ax(u, 2, d, CL_deg2rad(60));

// Check result :
v = CL_rot_rotVect(q,u);
res1=CL_vectAngle(v,d)  // == 60 deg

TEST_OK($+1) = CL__isEqual( res1 , CL_deg2rad(60), 1e-15);

//Test 9: Rotation about z-axis that transforms
// u into a vector with 30 deg. to d.

[q, ang, Inok] = CL_rot_defRot1Ax(u, 3, d, CL_deg2rad(30));

// Check result :
v = CL_rot_rotVect(q,u);
res1=CL_vectAngle(v,d)  // == 30 deg

TEST_OK($+1) = CL__isEqual( res1 , CL_deg2rad(30), 1e-15);
