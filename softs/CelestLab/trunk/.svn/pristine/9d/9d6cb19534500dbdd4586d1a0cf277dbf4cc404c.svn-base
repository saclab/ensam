//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M] = CL_fr_locOrbMat(pos_car,vel_car,frame_name)
// Inertial frame to custom local orbital frame transformation matrix
//
// Calling Sequence
// M = CL_fr_locOrbMat(pos_car,vel_car,frame_name)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>M</b> from 
// the inertial reference frame to a custom 'qsw' or 'tnw' local orbital frame.</p>
// <p> The inertial frame is implicitly the frame relative to which the satellite's
// position and velocity are defined. The local frame is built using these 
// position and velocity vectors. </p>
// <p><b>frame_name</b> is the name of the local
// frame. The name is a 3-letter word with letters among the following: 'q', 's', 'w', 't', 'n', 
// lower case or upper case. If 'a' (lower case!) is the name of some axis, 'A' (upper case!) 
// is the name of the same axis in the opposite direction. </p>
// <p> Example: "qWS" means: x-axis = q, y-axis = -w, z-axis = -s</p>
// <p> The reference frame that results should be direct. </p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p> 
// <p> By convention, <b>M</b> multiplied by coordinates
// relative to the inertial frame yields coordinates relative to the local frame. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Local frames">Local frames</link> for  more details on the definition of local frames.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// pos_car: satellite's position vector relative to the inertial frame [m] (3xN)
// vel_car: satellite's velocity vector relative to the inertial frame [m/s] (3xN)
// frame_name: (string) name of local frame 
// M : inertial to local orbital frame transformation matrix (3x3xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fr_qswMat
// CL_fr_tnwMat
//
// Examples
// pos_car = [[3500.e3;2500.e3;5800.e3] , [4500.e3;2100.e3;6800.e3]];
// vel_car = [[1.e3;3.e3;7.e3] , [2.e3;3.e3;6.e3]];
//
// // Example 1: Standard 'qsw' local frame: 
// M = CL_fr_locOrbMat(pos_car,vel_car,"qsw")
// M = CL_fr_qswMat(pos_car,vel_car) // => same 
//
// // Example 2: 'lvlh' (same as (s, -w, -q)): 
// M = CL_fr_locOrbMat(pos_car,vel_car,"sWQ")
// M = CL_fr_lvlhMat(pos_car,vel_car) // => same 
//
// // Example 3: comparison with CL_rot_defFrameVec: 
// M = CL_fr_locOrbMat(pos_car,vel_car,"qWs")
// M = CL_rot_defFrameVec(pos_car,vel_car,1,3) // => same 


// Declarations:


// Code:

[lhs,rhs] = argn()
if rhs~=3 then CL__error('Invalid number of input arguments'); end

N = size(pos_car,2);

// --------------------------------------------------------
// CODE FOR GENERATING ALL THE COMBINATIONS (numbers and names)
// // solutions avec axe1 = 1
// axes = [ [1;2;3], [1;3;-2], [1;-2;-3], [1; -3; 2] ]; 
// // + circ. permutations : 1=>2, 1=>3
// axes = [axes, sign(axes) .* (pmodulo(abs(axes),3)+1), sign(axes) .* (pmodulo(abs(axes)+1,3)+1)  ]; 
// // + opposite signs : [1,2,3] => [-1,-2,3] 
// axes = [axes, [-axes(1:2,:); axes(3,:)] ]; 
// // fprintfMat("axes.txt", axes, "%3g")
//
// // calcul noms
// lettres_tnw = ['W', 'N', 'T', '', 't', 'n', 'w']; 
// lettres_qsw = ['W', 'S', 'Q', '', 'q', 's', 'w']; 
// noms_tnw = lettres_tnw(axes(1,:)+4) + lettres_tnw(axes(2,:)+4) + lettres_tnw(axes(3,:)+4) 
// noms_qsw = lettres_qsw(axes(1,:)+4) + lettres_qsw(axes(2,:)+4) + lettres_qsw(axes(3,:)+4) 
// strcat("""" + noms_qsw + """", ' ')
// strcat("""" + noms_tnw + """", ' ')
// --------------------------------------------------------

// axes numbers for tnw or qsw => see above
axes = [
  1   1   1   1   2   2   2   2   3   3   3   3  -1  -1  -1  -1  -2  -2  -2  -2  -3  -3  -3  -3; .. 
  2   3  -2  -3   3   1  -3  -1   1   2  -1  -2  -2  -3   2   3  -3  -1   3   1  -1  -2   1   2; .. 
  3  -2  -3   2   1  -3  -1   3   2  -1  -2   1   3  -2  -3   2   1  -3  -1   3   2  -1  -2   1  .. 
]; 

names_qsw = [ "qsw" "qwS" "qSW" "qWs" "swq" "sqW" "sWQ" "sQw" "wqs" "wsQ" "wQS" "wSq" ..
              "QSw" "QWS" "QsW" "Qws" "SWq" "SQW" "SwQ" "Sqw" "WQs" "WSQ" "WqS" "Wsq" ]; 
names_tnw = [ "tnw" "twN" "tNW" "tWn" "nwt" "ntW" "nWT" "nTw" "wtn" "wnT" "wTN" "wNt" ..
              "TNw" "TWN" "TnW" "Twn" "NWt" "NTW" "NwT" "Ntw" "WTn" "WNT" "WtN" "Wnt" ]; 

types_qsw = 1 * ones(axes(1,:)); 
types_tnw = 2 * ones(axes(1,:)); 

// axes and names for qsw and tnw (qsw first)
axes = [axes, axes]; 
frame_names = [names_qsw, names_tnw]; 
type_frame = [types_qsw, types_tnw]; 

k = find(frame_names == frame_name);
if (k == [])
  CL__error("Unrecognized frame name");
end

// Frame type = qsw
if (type_frame(k) == 1)
  [P] = CL_fr_qswMat(pos_car,vel_car);
// Frame type = tnw
elseif (type_frame(k) == 2)
  [P] = CL_fr_tnwMat(pos_car,vel_car);
end

// Signes des axes
sa1 = sign(axes(1,k));
sa2 = sign(axes(2,k));
sa3 = sign(axes(3,k));

// Indices des axes
ia1 = abs(axes(1,k));
ia2 = abs(axes(2,k));
ia3 = abs(axes(3,k));

// hypermat remplit element par element en faisant varier l'indice de gauche d'abord :
M = hypermat([3 3 N] , [sa1*P(ia1,1,:); sa2*P(ia2,1,:); sa3*P(ia3,1,:); ...
                        sa1*P(ia1,2,:); sa2*P(ia2,2,:); sa3*P(ia3,2,:); ...
                        sa1*P(ia1,3,:); sa2*P(ia2,3,:); sa3*P(ia3,3,:)]);
if (N == 1)
    M = M(:,:,1);
end

endfunction
