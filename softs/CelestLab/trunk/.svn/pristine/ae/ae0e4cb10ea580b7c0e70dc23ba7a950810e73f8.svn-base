//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Celestial body phases as seen from Earth (Moon or Venus). 
//> 
//> The results are computed each day at 12h TREF (~UTC) for the Earth center. 
//> The shape of the body is directly related to the Earth-body-Sun angle. 
//> The projection plane (in which the moon is represented) is such that: 
//> Z axis (perpendicular to projection plane): parallel to Earth->body direction
//> X axis: in the Earth-body-Moon plane. 
//
// Author: A. Lamy
// -----------------------------------------------------------

// ------------------------
// Internal function 
// ------------------------
// x, y: where to plot (center) - plot units
// r: radius - plot units
// theta: Earth-body-Sun angle (rad)
function plot_body(x, y, r, theta)
  N = 40; 
  Y1 = cos(linspace(%pi, 0, N/2)); 
  Y2 = cos(linspace(0, %pi, N/2)); 
  X1 = sqrt(1 - Y1.^2); 
  X2 = sqrt(1 - Y2.^2); 

  sgn = ones(theta), 
  I = find(theta <= 0); 
  sgn(I) = -1; 
  // if theta close to pi => modify value as numerical errors => bad result
  I = find(abs(CL_rMod(theta-%pi, -%pi, %pi)) <= 1.e-6);
  theta(I) = %pi + 1.e-6;   

  pts_Y = CL_dMult(r, [Y1, Y2]' * ones(theta)); 

  // edge
  pts_X = [CL_dMult(r .* ones(theta), X1'); -CL_dMult(r .* ones(theta), X2')]; 
  xpolys(pts_X + repmat(x, N, 1), pts_Y + repmat(y, N, 1), color("grey70") * ones(x)); 
  
  // terminator
  pts_X = [CL_dMult(sgn .* r, X1'); -CL_dMult(sgn .* r .* cos(theta), X2')]; 
  xfpolys(pts_X + repmat(x, N, 1), pts_Y + repmat(y, N, 1), -color("yellow") * ones(x)); 
  e = CL_g_select(gce(), "Polyline"); 
  e.foreground = -1; 
endfunction


// ------------------------
// Input of parameters
// ------------------------
cal_now = CL_dat_now("cal"); 
year = cal_now(1); 
ibody = 3; 

desc_param = list(..
   CL_defParam("Celestial body: 1=Mercury, 2=Venus, 3=Moon, 4=Mars", ibody, accv=1:4), ..
   CL_defParam("Year", year, valid='$x>=1800 & $x <=2200') ..
);

[ibody, year] = CL_inputParam(desc_param);

BODIES = ["Mercury", "Venus", "Moon", "Mars"]; 
body = BODIES(ibody); 

// ------------------------
// Computation
// ------------------------
days = 1 : 31;
months = 1 : 12;

[Days, Months] = ndgrid(days, months); 
all_days = matrix(Days, 1, -1); 
all_months = matrix(Months, 1, -1); 

// cjd: 0h (TREF)
cjd = CL_dat_cal2cjd(year, all_months, all_days); 
cal = CL_dat_cjd2cal(cjd); 

// remove "invalid" dates (although results would be valid)
I = find(cal(2,:) <> all_months | cal(3,:) <> all_days); 
cjd(I) = []; 
all_days(I) = []; 
all_months(I) = []; 

// positions from Earth in EOD (slightly more efficient and accurate)
// each day at 12h00 TREF (=> +0.5)
pos_sun = CL_eph_sun(cjd+0.5, frame="EOD"); 
if (body == "Moon")
  pos_body = CL_eph_moon(cjd+0.5, frame="EOD"); 
else // Venus
  // Earth->body = Earth->Sun + Sun->body
  pos_body = pos_sun + CL_eph_planet(body, cjd+0.5);
end
    
// Earth-body-Sun angle
ems_angle = CL_vectAngle(-pos_body, pos_sun-pos_body); 

// add sign : +1 if Sun on the right (Waxing body) 
I = find(CL_dot(CL_cross(pos_sun, pos_body), [0;0;1]) < 0); 
ems_angle(I) = -ems_angle(I); 


// ------------------------
// Plot
// ------------------------
f = scf(); 
f.axes_size = [1000, 420]; 
f.visible = "on"; 
f.immediate_drawing = "off"; 


// background
a = gca(); 
a.background = color("grey20");
a.clip_state = "clipgrf"; 

// length inside each cell
L = 0.9; 

// Plot body
plot_body(all_days, all_months, L/2, ems_angle); 

// Adjustments
a.data_bounds = [0.5, 0.5; 31.5, 12.5]; 
a.tight_limits = "on"; 

// axes_visible: necessary as no "plot"
a.axes_visible = ["on", "on", "off"];

month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']; 

a.x_ticks = tlist(["ticks", "locations", "labels"], 1:31, string(1:31));
a.y_ticks = tlist(["ticks", "locations", "labels"], 1:12, month_names); 

a.title.text = sprintf("%s phases - %d", body, year); 

CL_g_stdaxes(a); 
a.grid = [-1, -1, -1]; 
a.sub_ticks = [0, 0]; 
a.font_size = 2; 
a.isoview = "on"; 
a.margins = [0.05, 0.05, 0.125, 0.1]; 

f.immediate_drawing = "on"; 
f.visible = "on"; 
