// ----------------------------------------------------------------------------------------
// CL__fr_fastConvert
// Tests de comparaison entre les changements de reperes simplifies
// et les changements de reperes complets.
// ----------------------------------------------------------------------------------------

TEST_OK = [];
// Ecart angulaire entre 2 matrices (arcsec)
function ang = ecart_ang(M1,M2)
  [ax,ang] = CL_rot_quat2axAng(CL_rot_matrix2quat(M1*M2'));
  ang = CL_unitConvert(ang,"rad","arcsec");
endfunction

// Reconstruction de la matrice de passage 
// a partir de la fonction de conversion position/vitesse
function M = build_M(frame1, frame2, cjd)
  N = size(cjd,2);
  p1 = CL__fr_fastConvert(frame1, frame2,cjd, [1;0;0]*ones(1,N));
  p2 = CL__fr_fastConvert(frame1, frame2,cjd, [0;1;0]*ones(1,N));
  p3 = CL__fr_fastConvert(frame1, frame2,cjd, [0;0;1]*ones(1,N));
  
  M = hypermat([3,3,N], [p1;p2;p3]);
endfunction


// Dates in [2000-2100]
jd_deb = CL_dat_convert("cal","jd", [2000;1;1]);
jd_fin = CL_dat_convert("cal","jd", [2100;1;1]);
jd = linspace(jd_deb,jd_fin, 100);

cjd = CL_dat_convert("jd","cjd", jd);

ut1_tref = 0;
tt_tref = 0;


frames = ["GCRS", "EOD", "MOD", "CIRS", "Veis"];
[i,j] = ndgrid(1:5, 1:5);

N = size(i,"*");

ang_max = 0;

// Test for all the combinations of frames
for k = 1 : N
  frame1 = frames(i(k));
  frame2 = frames(j(k));
  M = build_M(frame1, frame2, jd);
  Mref = CL_fr_convertMat(frame1,frame2,cjd, ut1_tref, tt_tref);
  ang = max(ecart_ang(M,Mref));  // arcseconds
  ang_max = max(ang,ang_max); 
end

// The precisions are the following in the [2000-2100] period
// MOD <-> EOD	0.00182
// EOD <-> GCRS  0.000576
// GCRS <-> CIRS  0.855
// CIRS <-> VEIS  4.45e-005

// Test is OK if all conversions are below 1 arcseconds
  TEST_OK($+1) = ang_max < 1;
  
  
// Test 1: cas vel1<>[] rotation around Z
pos1 = [3500.e3; 2500.e3; 5800.e3];
vel1 = [1.e3; 3.e3; 7.e3];
cjd = CL_dat_convert("cal","cjd", [2020;1;1]);
jd = CL_dat_convert("cal","jd", [2020;1;1]);
[pos_CIRS_1,vel_CIRS_1] = CL_fr_convert("Veis","CIRS",cjd, pos1, vel1);
[pos_CIRS_2,vel_CIRS_2] = CL__fr_fastConvert("Veis","CIRS",jd, pos1, vel1);

TEST_OK($+1)= CL__isEqual(pos_CIRS_1(1),pos_CIRS_2(1),1.e-10) & ...
              CL__isEqual(pos_CIRS_1(2),pos_CIRS_2(2),1.e-10) & ...
              CL__isEqual(pos_CIRS_1(3),pos_CIRS_2(3),1.e-10) & ...
              CL__isEqual(vel_CIRS_1(1),vel_CIRS_2(1),1.e-10) & ...
              CL__isEqual(vel_CIRS_1(2),vel_CIRS_2(2),1.e-10) & ...
              CL__isEqual(vel_CIRS_1(3),vel_CIRS_2(3),1.e-10);


// Test 2: cas vel1<>[] rotation around X
pos1 = [3500.e3; 2500.e3; 5800.e3];
vel1 = [1.e3; 3.e3; 7.e3];
cjd = CL_dat_convert("cal","cjd", [2020;1;1]);
jd = CL_dat_convert("cal","jd", [2020;1;1]);
[pos_EOD_1,vel_EOD_1] = CL_fr_convert("MOD","EOD",cjd, pos1, vel1);
[pos_EOD_2,vel_EOD_2] = CL__fr_fastConvert("MOD","EOD",jd, pos1, vel1);

TEST_OK($+1)= CL__isEqual(pos_EOD_1(1),pos_EOD_2(1),1.e-10) & ...
              CL__isEqual(pos_EOD_1(2),pos_EOD_2(2),1.e-10) & ...
              CL__isEqual(pos_EOD_1(3),pos_EOD_2(3),1.e-10) & ...
              CL__isEqual(vel_EOD_1(1),vel_EOD_2(1),1.e-10) & ...
              CL__isEqual(vel_EOD_1(2),vel_EOD_2(2),1.e-10) & ...
              CL__isEqual(vel_EOD_1(3),vel_EOD_2(3),1.e-10);
              
              
// Test 3: Invalid number of input arguments
try
    pos2 = CL__fr_fastConvert();
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end


// Test 4: Unrecognized frame names
jd = [21915,21916];
pos1 = rand(3,2);
try
    pos2 = CL__fr_fastConvert("EODS","GCRS",jd,pos1,[]);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end
