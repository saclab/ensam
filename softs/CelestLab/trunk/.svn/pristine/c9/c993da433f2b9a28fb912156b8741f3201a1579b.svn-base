//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [val] = CL_dataGet(name, local)
// Retrieve the value of CelestLab data
//
// Calling Sequence
// [val] = CL_dataGet(name [, local])
// [val] = CL_dataGet()
//
// Description
// <itemizedlist>
// <listitem>
// <p>Retrieves the values of CelestLab data. </p>
// <p>The argument <b>local</b> specifies if local data values can be returned
// instead of internal ones: </p>
// <p>- <b>local</b> = %f : internal (predefined) values are always returned. </p>
// <p>- <b>local</b> = %t : local data values (values of variables whose name
// begin with "%CL_") are returned if these data exist. This is the default 
// behaviour. </p>
// <p></p>
// <p>Notes:</p>
// <p>- The names of CelestLab data that can be found through the CelestLab menu - tab 
// "Data files (predefined variables)". The data files (*.scd) where the data are defined
// are located in the folder: CL_home()/data. </p>
// <p>- Any field is accessible. For instance CL_dataGet("body") returns the whole "body" structure,
// CL_dataGet("body.Sun.mu") returns only the value of the field "body.Sun.mu. "</p>
// <p>- If a local variable (%CL_xxx) exists, its value should have a same type and size as 
// the corresponding internal (predefined) value.</p>
// <p>- CL_dataGet() returns the whole (internal) data structure. The argument "local" is
// ignored. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// name: (optional, string) Data name. Default is "" (all data structure) (1x1)
// local: (optional, boolean) %t if local variable is returned if it exists. Default is %t (1x1)
// val: Value of the field (can be a structure or a matrix of any size)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_init
//
// Examples
// muSun = CL_dataGet("body.Sun.mu");
// au = CL_dataGet("au");
// body = CL_dataGet("body");
// 
// CL_dataGet("mu", local=%f) // => default value
// %CL_mu = CL_dataGet("body.Sun.mu"); // change local change
// CL_dataGet("mu") // => local value
// %CL_mu = CL_dataGet("mu", local=%f) // reset to default value
// 
// // Retrieve CelestLab's whole (internal) data structure:
// s = CL_dataGet()


// Declarations:
global %CL__PRIV;

// Code:

if (argn(2) == 0 | (~exists("name", "local"))); name = ""; end
if (~exists("local", "local")); local = %t; end
  
if (typeof(name) <> "string" | size(name,"*") <> 1)
  CL__error("Invalid type or size for argument name"); 
end

name = stripblanks(name, %t); 

// initialize effname = effective name
// + effname_ref = name in internal data structure

if (name == "")
  // internal values returned
  effname_ref = "%CL__PRIV.DATA"; 
  effname = effname_ref; 
  
else
  // determine base name (name of main variable) 
  n = strcspn(name, ".(");  // char up to "." or "(" not included 
  namepart1 = part(name, 1 : n); 
  namepart2 = part(name, n+1 : length(name)); 
  rootvar = "%CL_" + namepart1; 

  effname_ref = "%CL__PRIV.DATA." + name;
  if (exists(rootvar) & local)
    effname = rootvar + namepart2; 
  else
    effname = effname_ref; 
  end
end

// Return variable
// NB: Do not use val = %CL__PRIV.DATA(name) because name can be name1.name2.name3 ...

cmd = "val = " + effname; 
if (effname <> effname_ref)
  cmd = cmd + "; val_ref = " + effname_ref; 
end

ier = execstr(cmd, "errcatch");
if (ier <> 0)
  CL__error("Requested data not found: " + name);
end

if (effname <> effname_ref)
  // local data => check type and size
  if ((~and(size(val) == size(val_ref))) | typeof(val) <> typeof(val_ref))
    CL__error("Invalid size or type for " + effname);
  end
end

endfunction
