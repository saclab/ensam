//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.stela;

import fr.cnes.celestlab.celestlabx.generic.Params;
import fr.cnes.los.stela.commons.logic.date.StelaDate;
import fr.cnes.los.stela.commons.logic.exception.CommonException;
import fr.cnes.los.stela.commons.model.AdvancedParameters;
import fr.cnes.los.stela.commons.model.DefaultValues;
import fr.cnes.los.stela.commons.model.Prop;
import fr.cnes.los.stela.commons.model.atmosmodel.IAtmosphericModel;
import fr.cnes.los.stela.commons.model.ephemeris.Bulletin;
import fr.cnes.los.stela.commons.model.ephemeris.ModelType;
import fr.cnes.los.stela.commons.model.ephemeris.FrameTypes;
import fr.cnes.los.stela.commons.model.ephemeris.OrbitTypes;
import fr.cnes.los.stela.commons.model.spaceobject.SpaceObject;
import fr.cnes.los.stela.commons.model.thirdbody.IThirdBody;
import fr.cnes.los.stela.elib.business.implementation.noninertial.NonInertialContribution;
import fr.cnes.los.stela.elib.business.implementation.simulation.gtosimulation.GTOSimulation;
import fr.cnes.los.stela.elib.business.implementation.simulation.gtosimulation.GTOSimulationBuilder;
import fr.cnes.los.stela.elib.business.framework.simulation.SimulationTools;
import fr.cnes.los.stela.elib.business.implementation.solarradiationpressure.GTOSolarRadiationPressure;
import fr.cnes.los.stela.elib.business.implementation.thirdbody.ThirdBodyPerturbation;
import fr.cnes.los.stela.elib.business.implementation.thirdbody.moon.model.MoonOrbit;
import fr.cnes.los.stela.elib.business.implementation.thirdbody.sun.model.SunOrbit;
import fr.cnes.los.stela.elib.business.implementation.atmosmodel.msis00.MSIS00Adapter;
import fr.cnes.los.stela.elib.business.implementation.atmosphericdrag.AtmosphericDrag;
import fr.cnes.los.stela.elib.business.implementation.diffeq.GTODiffEq;
import fr.cnes.los.stela.elib.business.implementation.earthpotential.TesseralHarmonics;
import fr.cnes.los.stela.elib.business.implementation.earthpotential.ZonalHarmonics;
import fr.cnes.los.stela.processing.flightdynamics.BulletinNatureConverter;

import org.apache.log4j.Logger;


/**
 * Class Simulation
 * This class is used to initialize the simulation context: 
 * - simulation data
 * - (Stela) GTOSimulation object for propagation  
 * - (Stela) GTODiffEq object for time derivatives computation
 * - Some utility functions that are data/model dependent (osc2mean...) 
 * 
 * There is no propagation functions directly provided by this object. 
 * 
 * Note the "reference" time scale in STELA is UT1 (but is TREF in CelestLab interface). 
 * The simulation uses the STELA "GTO" model
 * All orbital elements are "type8" <=> "cireq" in CelestLab. 
 * Integration frame is CIRF. 
 * 
 */
class Simulation
{
	private static final Logger logger = Logger.getLogger(Simulation.class);

	// simulation data
	private SimulationData data; 
	
	// STELA GTO simulation object
	private GTOSimulation gtosim; 

	// Thirdbody for mean <-> osculating conversion and for EqDiff creation
	private IThirdBody moon = null; 
	private IThirdBody sun = null; 

	// object (physical properties)
	private SpaceObject spaceObject = null; 

	// additional objects (perturbation objects for GTODiffEq)
	private ZonalHarmonics zonalPerturbation = null; 
	private TesseralHarmonics tesseralPerturbation = null; 
	private GTOSolarRadiationPressure srpPerturbation = null; 
	private ThirdBodyPerturbation sunPerturbation = null; 
	private ThirdBodyPerturbation moonPerturbation = null; 
	private AtmosphericDrag dragPerturbation = null; 
	private NonInertialContribution appAccPerturbation = null; 


	/**
	 * Constructor
	 * Note: moon, sun, spaceObject: not initialised now. 
	 */
	Simulation()
	{
		data = new SimulationData(); 
		gtosim = (GTOSimulation) new GTOSimulationBuilder().buildSimulation();
		gtosim.setStatisticalAnalysis(false);
	}

	/**
	 * Initializes the simulation context
	 * @param params: structure containing the extrapolation parameters
	 * @param ut1_tref: UT1 minus TREF (s)
	 * @param tt_tref: TT minus TREF (s)
	 * @return info: status of the extrapolation
	 * @throws Exception
	 */
	Information initialize(Params params, double ut1_tref, double tt_tref) 
	{
		int status = 0;
		String errMsg = "";

		try
		{
			// reset internal constants before (possibly) affecting default values 
			// to variables
			// NB: solar activity and areo coef : reset to default values 
			// (and will be reloaded) 
			// Important : physical constants not re-initialized  
			DefaultValues.init(); 
			AdvancedParameters.init(); 
						
			data.initialize(params, ut1_tref, tt_tref); 
			
			// physical constants file 
			// Warning: if value is changed cannot be reset to default value
			if (!data.physicalConstantsFile.trim().isEmpty()) { 
				Prop.setStelaPhysicalConstants(data.physicalConstantsFile); 
			}
			
			// solar activity file is reloaded if name is given or if reload wanted
			if (!data.solarActivityFile.trim().isEmpty()) { 
				Prop.setStelaSolarActivity(data.solarActivityFile); 
			}

			// aero coef file is reloaded if name is given or if reload wanted
			if (!data.aeroCoefFile.trim().isEmpty()) { 
				Prop.setStelaDragCoefficient(data.aeroCoefFile); 
			}
		
			initGTOSimulation(); 
		}
		catch (Exception e)
		{
			status = -1;
			errMsg = e.getMessage();
			logger.error(e.getMessage());
		}

		return new Information(this, status, errMsg);
	}

	
	/**
	 * cleaning
	 */
	void clean() 
	{
		data.clean(); 
	}


	/**
	 * returns a eqDiff instance
	 * @param bulletin date + orbital elements
	 */

	GTODiffEq getEqDiff(Bulletin bulletin) throws Exception
	{       
		StelaDate stela_date = bulletin.getDate(); 

		moon.updatePosition(stela_date); 
		sun.updatePosition(stela_date); 

		GTODiffEq eqDiff = new GTODiffEq(bulletin, sun, moon, 
				zonalPerturbation, tesseralPerturbation, sunPerturbation,
				moonPerturbation, dragPerturbation, srpPerturbation, appAccPerturbation);

		return eqDiff;
	}

	/**
	 * Save the simulation parameters in XML file
	 * @param fileName simulation parameters XML file name
	 * @throws Exception
	 */
	void save(String fileName) throws Exception
	{
		try
		{
			SimulationTools.saveXmlFormat(fileName, gtosim);
		}
		catch(CommonException e)
		{
			logger.error(String.format(
					"Error while saving simulation XML file %s : %s", fileName, e.getMessage()));
			throw e;
		}
	}

	/**
	 * Returns the (low level) STELA GTOSimulation instance
	 */
	GTOSimulation getGTOSimulation()
	{
		return gtosim;
	}


	/**
	 * Sets the initial state for propagation
	 * @param bulletin: orbvital state
	 * NB: should be: mean elements, type8, frame = CIRF)
	 */
	void setInitialState(Bulletin bulletin)
	{
		// Initial bulletin
		logger.debug("setInitialState: Bulletin = " + bulletin.toString());
		getGTOSimulation().getEphemerisManager().setInitialState(bulletin);
	}

	/**
	 * Convert to osculating parameters
	 * @return the "converted" Bulletin
	 * @throws Exception 
	 */
	Bulletin mean2osc(Bulletin bulletin_mean) throws Exception 
	{
		Bulletin bulletin = bulletin_mean.clone();  
		StelaDate stela_date = bulletin.getDate(); 

		// update sun/moon position. Necessary ? 
		moon.updatePosition(stela_date); 
		sun.updatePosition(stela_date); 

		BulletinNatureConverter.convertToOsculating(bulletin, ModelType.GTO, moon, sun, 
				spaceObject, data.sun_enabled, data.moon_enabled, data.srp_enabled, data.zonal_maxDeg); 

		return bulletin; 
	}
	
	/**
	 * Convert to osculating parameters
	 * @return the "converted" Bulletin
	 * @throws Exception 
	 */
	Bulletin osc2mean(Bulletin bulletin_osc) throws Exception 
	{
		Bulletin bulletin = bulletin_osc.clone();  
		StelaDate stela_date = bulletin.getDate(); 

		// update sun/moon position. Necessary ? 
		moon.updatePosition(stela_date); 
		sun.updatePosition(stela_date); 

		BulletinNatureConverter.convertToMean(bulletin, ModelType.GTO, moon, sun, 
				spaceObject, data.sun_enabled, data.moon_enabled, data.srp_enabled, data.zonal_maxDeg); 

		return bulletin; 
	}


	/**
	 * @return the value of UT1-TREF from the simulation data
	 */
	double getUT1minusTREF()
	{
		return data.ut1_tref;
	}

	/**
	 * @return the value of TT-UT1 from the simulation data
	 */
	double getTTminusUT1()
	{
		return data.tt_tref - data.ut1_tref;
	}

	/**
	 * @return the value of "central_enabled" from the simulation data
	 */

	boolean getCentralEnabled()
	{
		return data.central_enabled;
	}

	/**
	 * Initialises the GTOSimulation using the simulation data
	 * Warning: changes some global variables ! 
	 */
	private void initGTOSimulation() 
	{
		spaceObject = new SpaceObject("object", data.mass,
				data.drag_area, data.srp_area, OrbitTypes.GTO, data.srp_coef, data.drag_coef);

		// 
		gtosim.setEphemerisStep(data.integrator_step);
		gtosim.setIntegratorStep(data.integrator_step);

		// time offsets: TT - UT1 = (TT - TREF) - (UT1 - TREF)
		gtosim.setTtMinusUT1(data.tt_tref - data.ut1_tref);

		// Space object
		gtosim.setSpaceObject(spaceObject);

		gtosim.setAtmosModelType(data.atmosModelType);

		// Solar activity
		gtosim.setSolarActivity(data.solarActivity);

		// Drag perturbtion
		gtosim.setDragSwitch(data.drag_enabled);
		gtosim.setDragQuadraturePoints(data.drag_nbQuadPoints);
		gtosim.setAtmosRecomputeSteps(data.drag_nbComputeSteps);

		// Solar radiation pressure (SRP)
		gtosim.setSrpSwitch(data.srp_enabled);
		gtosim.setSrpQuadraturePoints(data.srp_nbQuadPoints);

		// Third body
		gtosim.setMoonSwitch(data.moon_enabled);
		gtosim.setSunSwitch(data.sun_enabled);
		
		// Zonal (no switch is Stela !) 
		if (data.zonal_enabled) 
			gtosim.setZonalOrder(data.zonal_maxDeg);
		else
			gtosim.setZonalOrder(0);

		// Tesseral 
		gtosim.setTesseralSwitch(data.tesseral_enabled);
		gtosim.setTesseralOrder(data.tesseral_maxDeg);

		int nbIntegrationStepTesseral = (int) Math.ceil(data.tesseral_minPeriod / data.integrator_step);
		gtosim.setNbIntegrationStepTesseral(nbIntegrationStepTesseral);

		// Reentry altitude
		gtosim.setReentryAltitude(data.reentryAltitude);

		// ----------------------------
		// Initialises Sun and Moon objects using "correct" TT-UT1 value
		// ----------------------------
		double tt_minus_ut1 = getTTminusUT1(); 

		moon = new MoonOrbit(tt_minus_ut1);
		sun = new SunOrbit(tt_minus_ut1);

		// ----------------------------
		// Creates perturbation objects
		// Used for GTODiffEq creation
		// ----------------------------
		if (data.zonal_enabled) {
			boolean j22_included = true; 
			zonalPerturbation = new ZonalHarmonics(data.zonal_maxDeg, j22_included); 
		}

		if (data.tesseral_enabled) {
			tesseralPerturbation = new TesseralHarmonics(data.integrator_step, nbIntegrationStepTesseral, data.tesseral_maxDeg);
		}

		if (data.sun_enabled) {
			sunPerturbation = new ThirdBodyPerturbation(data.thirdBody_degree);
		}

		if (data.moon_enabled) {
			moonPerturbation = new ThirdBodyPerturbation(data.thirdBody_degree);
		}

		if (data.drag_enabled) {
			// NB: only MSIS2000 is available - no test
			final IAtmosphericModel atmosModel = new MSIS00Adapter();
			dragPerturbation = new AtmosphericDrag(spaceObject, data.drag_nbQuadPoints, atmosModel, data.solarActivity);
		}

		if (data.srp_enabled) {
			srpPerturbation = new GTOSolarRadiationPressure(spaceObject, data.srp_nbQuadPoints);
		}

		if (data.appAcc_enabled) {
			FrameTypes integration_frame  = FrameTypes.CIRF; 
			FrameTypes reference_frame  = data.ref_frame; 
			appAccPerturbation = new NonInertialContribution(data.appAcc_nbQuadPoints, integration_frame, reference_frame);
		}


		// additional initialisations (global variables)
		// if cannot be set directly
		// or may be necessary for a correct behaviour... 
		
		// DefaultValues.GTO_QUADRATURE_POINTS = data.drag_nbQuadPoints; 
		// DefaultValues.GTO_SRP_QUADRATURE_POINTS = data.srp_nbQuadPoints; 
		AdvancedParameters.REFERENCE_SYSTEM = data.ref_frame; 
		AdvancedParameters.COMPL_ACC_QUADRATURE_POINTS = data.appAcc_nbQuadPoints; 
		DefaultValues.GTO_ATMOSDRAG_RECOMPUTE_PERIOD_IN_STEPS = data.drag_nbComputeSteps; 
		AdvancedParameters.THIRD_BODY_POT_DEGREE_GTO = data.thirdBody_degree; 
		DefaultValues.REENTRY_ALTITUDE_GTO = data.reentryAltitude; 
		AdvancedParameters.COOK_WALL_TEMPERATURE = data.drag_cookWallTemp; 
		AdvancedParameters.COOK_ACCOMODATION = data.drag_cookAccomodCoef; 

	}

}
