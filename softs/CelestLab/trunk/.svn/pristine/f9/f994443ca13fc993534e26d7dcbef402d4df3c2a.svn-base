//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [p, pdot, p2dot] = CL_lagrangeFitPoly(xref, yref)
// Lagrange polynomial
//
// Calling Sequence
// [p, pdot, p2dot] = CL_lagrangeFitPoly(xref, yref)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the Lagrange polynomial passing through reference abscissae and ordinates.</p>
// <p></p>
// <p><b>xref</b> and <b>yref</b> describe the reference abscissae and ordinates. 
// Each column in xref and yref define a different set of points. The
// polynomial p(k) is the polynomial that fits the points P=(x,y) such that
// x=xref(:,k) and y=y(:,k). The (maximum) degree of the polynomial is  
// equal to the number of rows in x and y minus 1. </p>
// <p></p>
// <p>The function also optionally returns the first and second derivative of the polynomial.</p>
// <p></p>
// <p><b>Note</b>:</p>
// <p>If the polynomial cannot be computed (because the abscissae are not all different)
// a constant polynomial equal to %nan is returned. The derivatives are then set to
// the same polynomial (%nan), which is not necessarily what the Scilab "derivat" 
// function would do.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// xref: Reference abscissae. (PxN)
// yref: Corresponding ordinates. (PxN)
// p: Lagrange polynomials. (1xN) 
// pdot: First derivative of the polynomials. (1xN) 
// p2dot: Second derivative of the polynomials. (1xN) 
//
// Authors
// CNES - DCT/SB 
//
// See also
// CL_interpLagrange
//
// Examples
// // Generate a degree 2 Lagrange polynomial
// xref = [-1; 0; 2]; 
// yref = 1 - xref + xref.^2; 
// p = CL_lagrangeFitPoly(xref, yref) // => 1 - x + x^2
//
// // Vectorized form
// xref = [[-1; 0; 2], [-2; 1; 4]]; 
// yref = 1 - xref + xref.^2; 
// p = CL_lagrangeFitPoly(xref, yref) // => 1 - x + x^2 (2 times)

// Declarations:

// Code:
// number of args (left/right)
[lhs,rhs] = argn(); 

// Default output
p = []; 
pdot = []; 
p2dot = []; 

if (rhs <> 2)
  CL__error("Invalid number of input arguments"); 
end

// check that xref and yref have same size
if (or(size(xref) <> size(yref)))
  CL__error("Invalid size for xref or yref"); 
end

if (size(xref, "*") == 0)
  return; // RETURN (empty inputs => empty outputs)
end

n = size(xref, 1); // number of points
N = size(xref, 2); 

// x: polynomial variable
// p => polynomial equal to 0 
// X => [x, x, x, ...] (polynomial variable, N times)
x = poly(0, "x");
p = zeros(1,N) * x; 
X = ones(1,N) * x; 
 
// invalidpol == 1 => polynomial is invalid
invalid_pol = zeros(1,N); 

if (n == 1)
  // special case for n == 1 as algorithm would not work (C1 == []) 
  p = p + yref(1,:); 
  
else
  C1 = ones(n-1, 1); 
  
  for i = 1 : n
    k = [1:i-1, i+1:n]; 
    den = prod(C1 * xref(i,:) - xref(k,:), "r"); 
    I = find(den == 0); 
    if (I <> [])
	  // Case of invalid polynomial (all abscissae not different)
	  // => den set to 1 to avoid division by 0 (and polynomial set as "invalid")
	  den(I) = 1; 
	  [I_i,I_j] = ind2sub(size(den), I); 
	  invalid_pol(I_j) = 1; 
	end
    p = p + yref(i,:) .* (prod(C1 * X - xref(k,:), "r") ./ den);
  end
  
end

// If there exist invalid polynomial 
// => change value to pnan = polynamial returning %nan
pnan = poly(%nan, "x", "coeff");

Inan = find(invalid_pol == 1); 
p(Inan) = pnan; 

// First derivative
if (lhs >= 2)
  pdot = derivat(p); 
  pdot(Inan) = pnan; 
end

// Second derivative
if (lhs >= 3)
  p2dot = derivat(pdot); 
  p2dot(Inan) = pnan; 
end

endfunction
