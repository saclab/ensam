//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Builds a JAR package from a Java source directory
// The source directory must be a subdirectory of the module 'src\java' directory
// The source directory structure must also respect the package hierarchy
// The JAR will be generated in the module 'jar' directory
// -param package_src_dir: name of subdirectory containing the sources of packages
// -param package_name: name of package to build
// -return dest_jar_path: the full path of generated JAR file
function dest_jar_path = build_jar(package_src_dir, package_name)
    src_java_dir = get_absolute_file_path("builder_java.sce");

    // Create the 'jar' folder if it does not exist
    dest_jar_dir = fullpath(fullfile(src_java_dir, "../../jar"));
    if ~isdir(dest_jar_dir) then
        mkdir(dest_jar_dir)
    end

    dest_jar_path = fullfile(dest_jar_dir, package_name + ".jar");
    package_src_path = fullfile(src_java_dir, package_src_dir);

    orig_dir = pwd();
    cd(package_src_path);
    ilib_build_jar(dest_jar_path, package_name, package_src_path);
    cd(orig_dir);
endfunction

// Main function
function builder_java()
    // Add depending JARS to classpath needed for building
    src_java_dir = get_absolute_file_path("builder_java.sce");
    thirdparty_jar_dir = fullpath(fullfile(src_java_dir, "../../thirdparty"));
    stela_jar_dir = fullpath(fullfile(src_java_dir, "../../thirdparty/stela"));

    // Add STELA compilation JARs (note: running time JARs will be loaded from STELA installation)
    stela_jar_paths = listfiles(stela_jar_dir, "*.jar");
    javaclasspath(fullfile(stela_jar_dir, stela_jar_paths));

    // Add thirdparty JARs
    thirdparty_jar_paths = listfiles(thirdparty_jar_dir, "*.jar");
    javaclasspath(fullfile(thirdparty_jar_dir, thirdparty_jar_paths));

    // Build the JARs
    build_jar("celestlabx", "fr.cnes.celestlab.celestlabx");
endfunction

builder_java();

clear builder_java;
clear get_java_dirs();
clear build_jar;

