//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [kep_t2,cov_t2]=CL_dsp_kepCovPropa(t1,kep_t1,cov_t1,t2, mu)
// Covariance propagation - DEPRECATED
//
// Calling Sequence
// [kep_t2, cov_t2] = CL_dsp_kepCovPropa(t1, kep_t1, cov_t1, t2 [, mu])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_ex_kepler">CL_ex_kepler</link> and  
// compute propagated covariance matrix by : cov_t2 = J * CL_dMult(cov_t1,ones(J)) * J'. </p>
// <p></p>
// </listitem>
// <listitem>
// <p>Performs the propagation of orbital elements and associated covariance matrix 
// using a Keplerian model. </p> 
// <p>Given a set of orbital elements <b>kep_t1</b> and the associated covariance 
// matrix (<b>cov_t1</b>) at time t1, the function computes the orbital elements 
// <b>kep_t2</b> and the associated covariance matrix (<b>cov_t2</b>) at time <b>t2</b>. </p>
// <p></p>
// <p><inlinemediaobject><imageobject><imagedata fileref="kepCovPropa.gif"/></imageobject></inlinemediaobject></p>
// <p></p>
// </listitem>
// <listitem>
// <p><b>Note</b>: </p>
// <p>This function works for any type of orbit and any type of orbital elements 
// ("kep", "cir", "cireq" or "equin"). </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// t1: Initial time [days] (1x1)
// kep_t1: Keplerian elements at time t1 [sma;ecc;inc;pom;gom;anm] (6x1)
// cov_t1: Covariance matrix at time t1 (6x6)
// t2: Final time(s) [days] (1xN)
// mu : (optional) Gravitational constant [m^3/s^2]. Default value is %CL_mu
// kep_t2: Keplerian elements at t2 [sma;ecc;inc;pom;gom;anm] (6xN)
// cov_t2: Covariance matrix at time t2 (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_kepler
//
// Examples
// // Propagation of covariance matrix (Keplerian elements)
// t0 = 0;
// sma = 7.e6; 
// kep0 = [sma; 0.05; 1; 0; 0; 0]; 
// cov0 = diag([100; 1.e-6; 1.e-6; 1.e-6; 1.e-6; 1.e-3].^2); 
// t = t0 + (0 : 300 : 86400) / 86400; 
// [kept, covt] = CL_dsp_kepCovPropa(t0, kep0, cov0, t);
//
// scf(); 
// plot(t, sma * matrix(sqrt(covt(6,6,:)), 1, -1)); 
// 
// // Derivation of position covariance matrix in tnw frame
// [pos, vel, jac] = CL_oe_kep2car(kept); 
// M = CL_fr_tnwMat(pos, vel); 
// covpv = jac * covt * jac'; 
// covploc = M * covpv(1:3,1:3,:) * M'; 
// 
// plot(t, matrix(sqrt(covploc(1,1,:)), 1, -1), "r");
//

// Declarations:


// Code:
if (~exists("mu", "local")); mu = CL__dataGetEnv("mu"); end

// Consistency checks
if (size(t1,"*") <> 1)
  CL__error("Invalid size for argument t1"); 
end
if (size(kep_t1,1) <> 6 | size(kep_t1,2) <> 1)
  CL__error("Invalid size for argument kep_t1"); 
end
if (size(cov_t1,1) <> 6 | size(cov_t1,2) <> 6)
  CL__error("Invalid size for argument cov_t1"); 
end
if (size(t2,1) > 1)
  CL__error("Invalid size for argument t2"); 
end
N = size(t2,2); 

// Special case 
if (N == 0)
  kep_t2 = []; 
  cov_t2 = []; 
  return; 
end

// State vector and covariance matrix at t2 
[kep_t2, jac] = CL_ex_kepler(t1, kep_t1, t2, mu);
cov_t2 = jac * matrix(repmat(cov_t1,1,N),6,6,N) * jac';

endfunction
