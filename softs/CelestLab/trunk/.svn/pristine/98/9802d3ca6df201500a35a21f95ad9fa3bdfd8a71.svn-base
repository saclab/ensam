//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [obj] = CL_stela_objLoad(desc)
// Loads an objects's description for the area computation
//
// Calling Sequence
// [obj] = CL_stela_objLoad(desc)
//
// Description
// <p>CL_stela_objLoad loads an object from a file in XML format created by STELA.</p>
// <p>The result (<b>obj</b>) is a structure containing the following fields:</p>
// <p>- <b>id</b>: identidier of the sub-part.</p>
// <p>- <b>shap</b>: Type of sub-part: Cuboid, Sphere, Rectangle, Triangle, TruncatedCone.</p>
// <p>- <b>dim</b>: Dimensions of sub-part (see below).</p>
// <p>- <b>z_axis</b>: z axis of sub-part's frame in objects's frame.</p>
// <p>- <b>x_axis</b>: x axis of sub-part's frame in objects's frame.</p>
// <p>- <b>orig</b>: origin of sub-part's frame in objects's frame.</p>
// <p></p>
//
// <p> The object's dimensions are defined as follows (increasing row indices):</p>
// <p>- <b>Cuboid</b>: length (y), height (z), depth (x).</p>
// <p>- <b>Rectangle</b>: length (y), width (x).</p>
// <p>- <b>Sphere</b>: radius.</p>
// <p>- <b>Triangle</b>: leftLength (y), rightLength (y), height (x).</p>
// <p>- <b>TruncatedCone</b>: bottomRadius (xy), topRadius (xy), height (z).</p>
// <p></p>
//
// Parameters
// desc: (string) Path of XML file.
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_stela_area
//
// Examples
// // XML description of object 
// desc = fullfile(CLx_setting_get("STELA_ROOT"), "examples/example_shap.xml");
// // Load object
// obj = CL_stela_objLoad(desc)


  // check extension module
  CL__checkExtensionModule();
  
  // call CLx function (which does all the checks)
  obj = CLx_stela_areaLoad(desc);
  
endfunction
