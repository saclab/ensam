<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 Copyright (c) CNES  2008
 
 This software is part of CelestLab, a CNES toolbox for Scilab
 
 This software is governed by the CeCILL license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 'http://www.cecill.info'.
 -->
<!--
 * 
 * Dates and time
 * 
 -->
<refentry version="5.0-subset Scilab" xml:id="Dates and time scales" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 27-Oct-2009 $</pubdate>
  </info>

  <refnamediv>
    <refname>Dates and time scales</refname>
    <refpurpose>Dates and time scales</refpurpose>
  </refnamediv>


  <refsection>
    <title>Dates</title>

    <para>The date types that can be handled in CelestLab are: </para>
    
    <itemizedlist>
    <listitem>
	<para><emphasis role="bold">JD (Astronomical Julian date)</emphasis>: 
	Number of (decimal) days elapsed since January 1st, -4712 12h (julian calendar)</para>
	<para/></listitem>
    
    <listitem>
	<para><emphasis role="bold">MJD (Modified Julian date)</emphasis>: 
	Number of (decimal) days elapsed since November 17th, 1858 0h. (MJD = JD - 2400000.5)</para>
	<para/></listitem>
    
    <listitem>
	<para><emphasis role="bold">CJD (another modified Julian date)</emphasis>: 
	Number of (decimal) days elapsed since January 1st, 1950 0h (CJD = JD - 2433282.5)</para>
	<para/></listitem>
    
    <listitem>
	<para><emphasis role="bold">CAL (Calendar date)</emphasis>: [year;month;day;hour;minute;second]</para>
    <para>- Years are counted astronomically. The year 1-BC is 0, 2-BC is -1, ...</para>
    <para>- The dates are relative to the Julian calendar before 4th october 1582 24h (Julian calendar), 
            and to the Gregorian calendar after 5th October 1582 0h (Julian calendar), that is, 
            after 15th october 1582 0h (Gregorian calendar). </para>
    <para>- Julian day 0 is 1st January -4712 12h (Julian calendar). </para>
    <para/>
    <para><emphasis role="italic">References</emphasis></para>
    <para>Jean Meeus - Astronomical Algorithms - 1991</para>
    <para/>
    </listitem>
    </itemizedlist>
    
    
    <para>Most time arguments in CelestLab functions are defined as <emphasis role="bold">CJD</emphasis>.</para>
    <para>Date types can be converted by the function <link linkend="CL_dat_convert">CL_dat_convert</link>.</para>
    
    <para/>

  </refsection>
    
  <refsection>
    <title>Definition of time scales</title>
    
    <para>The time scales most often seen are the following: </para>
	
    <itemizedlist>
    <listitem><para><emphasis role="bold">TAI (Temps Atomique International or 
	International Atomic Time)</emphasis>: </para>
    <para>This uniform time scale is produced by the BIPM (Bureau International des poids et mesures) in Paris.
    It is based on the readings of approximately 150 atomic clocks across the world.</para>
    <para/></listitem>
    
    <listitem><para><emphasis role="bold">UT1 (Universal Time)</emphasis>:</para>
    <para>UT1 is a measure of the orientation of the Earth. It is related to the Greenwich mean sidereal time 
	(GMST) by a conventional relationship. </para>
    <para/></listitem>

    <listitem><para><emphasis role="bold">UTC (Coordinated Universal Time)</emphasis>:</para>
    <para>This is the international standard on which civil time is based.</para>
    <para>It is a <emphasis role="bold">non-continuous</emphasis> time scale, deduced from <emphasis role="bold">
	TAI</emphasis> so as not to lose the relation between time and Earth's orientation (defined by <emphasis role="bold">UT1</emphasis>). 
    <emphasis role="bold">UTC</emphasis> and <emphasis role="bold">TAI</emphasis> differ by a whole number of seconds. Leap 
	seconds are introduced whenever <emphasis role="bold">UTC</emphasis> deviates by more than 0.9 second from 
	<emphasis role="bold">UT1</emphasis>. (typically every few years)</para>
	<para></para>
    <para>The quantity UT1-UTC is available in IERS bulletin A and B and the quantity UTC-TAI is available in IERS bulletin C. 
    IERS bulletins can be retrieved at: <ulink url="http://www.iers.org/IERS/EN/Publications/Bulletins/bulletins.html">http://www.iers.org/IERS/EN/Publications/Bulletins/bulletins.html</ulink></para>
    <para/></listitem>

    <listitem><para><emphasis role="bold">TT (Terrestrial Time)</emphasis>:</para>
    <para>This time scale is a modern astronomical time standard defined by the International Astronomical Union
	primarily for time-measurements of astronomical observations made from the surface of the Earth.</para>
    <para>It is defined by: <emphasis role="bold">TT</emphasis> = <emphasis role="bold">TAI</emphasis> + 32.184 seconds.</para>
    <para>This time scale was previously known as <emphasis role="bold">TDT</emphasis> (Terrestrial Dynamical Time) and 
     <emphasis role="bold">ET</emphasis> (Ephemeris Time).</para>
    <para/></listitem>
    
    <listitem><para><emphasis role="bold">TCB (Barycentric Coordinate Time)</emphasis>:</para>
    <para>This is a coordinate time standard intended to be used as the independent variable of time for all calculations 
    pertaining to orbits of planets, asteroids, comets, and interplanetary spacecraft in the Solar system. 
    It is equivalent to the proper time experienced by a clock at rest in a coordinate frame co-moving with the barycenter 
    of the Solar system: that is, a clock that performs exactly the same movements as the Solar system but is outside 
    the system's gravity well. It is therefore not influenced by the gravitational time dilation caused by the Sun 
    and the rest of the system.</para>
    <para/>
    <para>Note: <emphasis role="bold">TCB</emphasis> is now the coordinate time scale recommanded for use in the Barycentric 
	Celestial Reference System. However, TDB is still used most of the time (e.g. for ephemerides in the Solar system).</para>
    <para/></listitem>
    
    <listitem><para><emphasis role="bold">TDB (Barycentric Dynamical Time)</emphasis>:</para>
    <para>This is a relativistic coordinate time scale, intended for astronomical use as a 
    time standard to take account of time dilation when calculating orbits and astronomical ephemerides of planets, 
    asteroids, comets and interplanetary spacecraft in the Solar System. <emphasis role="bold">TDB</emphasis> is now (since 2006) 
	defined as a linear scaling of <emphasis role="bold">TCB</emphasis>, and a feature that distinguishes 
	<emphasis role="bold">TDB</emphasis> from <emphasis role="bold">TCB</emphasis> is that <emphasis role="bold">TDB</emphasis>, 
    when observed from the Earth's surface, has a difference from <emphasis role="bold">TT</emphasis> that is about as small as 
    can be practically arranged with consistent definition: the differences are mainly periodic, and overall will 
    remain at less than 2 milliseconds for several millennia.</para>
	<para/>
	<para>Note: <emphasis role="bold">TDB</emphasis> is not explictly defined in CelestLab. It is considered (generally) 
	acceptable to use TT instead. </para>

    <para/></listitem>
    </itemizedlist>

    <para>Time scales can be converted by the function <link linkend="CL_dat_scaleConvert">CL_dat_scaleConvert</link>.</para>
    <para/>

  </refsection>
  
  <refsection>
    <title>TREF time scale</title>

	<para>When time scale matters, time arguments are given relative to a specific time scale 
	called <emphasis role="bold">TREF</emphasis>.
	<emphasis role="bold">TREF</emphasis> is defined implicitly in relation to 
	<emphasis role="bold">UT1</emphasis> and <emphasis role="bold">TT</emphasis>. </para>
	
    <para>The default setting is such that: </para>
	<itemizedlist>
    <listitem>
	  <para><emphasis role="bold">TREF</emphasis> is the same as <emphasis role="bold">UTC</emphasis> at the time 
	  of CelestLab release, and varies as <emphasis role="bold">TAI</emphasis>,</para>
	</listitem>
	<listitem>
	  <para><emphasis role="bold">UT1</emphasis> is the same as <emphasis role="bold">UTC</emphasis> at the time 
	  of CelestLab release, and <emphasis role="bold">UT1-TREF</emphasis> is constant.</para>
	</listitem>
    </itemizedlist>

	<para>It can be changed if necessary: </para>
    <itemizedlist>
    <listitem>
	  <para>by modifying (or initializing) the constants <emphasis role="bold">%CL_TT_TREF</emphasis> (<emphasis role="bold">TT</emphasis> minus 
	  <emphasis role="bold">TREF</emphasis>) and <emphasis role="bold">%CL_UT1_TREF</emphasis> (<emphasis role="bold">UT1</emphasis> minus 
	  <emphasis role="bold">TREF</emphasis>). This will impact all subsequent calls to CelestLab functions.</para>
	</listitem>
    <listitem>
	  <para>by using the optional arguments <emphasis role="bold">ut1_tref</emphasis> and 
	  <emphasis role="bold">tt_tref</emphasis> whenever these arguments are available.</para>
	</listitem>
    </itemizedlist>
	

    <para/>
  </refsection>
  
  <para/>
</refentry>
