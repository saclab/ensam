//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Deformation of a conic sensor.
//> The tilt angle is the angle between the vertical and the cone axis.
//> The 2 curves represent the semi major axis and semi minor axis
//> of the intersection of the cone with the horizontal plane at altitude 0.
//>
//> Note: valid for small cone opening angles. 
//
// Author: A. Lamy
// -----------------------------------------------------------


alt = 700.e3;
tiltmin = 0;
tiltmax = 40 * %CL_deg2rad;

desc_param = list(..
  CL_defParam("Altitude", alt, units=['m', 'km'], id='$alt', valid='$alt >= 0'),..
  CL_defParam("Tilt angle to vertical - min", tiltmin, units=['rad', 'deg'], id='$tiltmin', valid='$tiltmin >= 0'),..
  CL_defParam("Tilt angle to vertical - max", tiltmax,  units=['rad', 'deg'], id='$tiltmax', valid='$tiltmax > $tiltmin')..
);

[alt, tiltmin, tiltmax] = CL_inputParam(desc_param);

elevmin = 5 * %CL_deg2rad;
tiltmax1 = CL_gm_visiParams(alt+%CL_eqRad,%CL_eqRad,'elev',elevmin,'sat'); // max angle for intersection
tiltmax = min(tiltmax, tiltmax1);

nbpts = 100;
ang = linspace(tiltmin,tiltmax,nbpts);

[K1,K2] = CL_gm_pixelSize(alt*ones(ang), ang);

// -----------------------------------------------------------
// plot
// -----------------------------------------------------------

f=scf();
f.visible="on";
f.immediate_drawing="off";

f.color_map = jetcolormap(10);
Noir = addcolor([0,0,0]);
Blanc = addcolor([1,1,1]);
Coul1 = 9;
Coul2 = 2;

a=gca();

plot2d(ang * %CL_rad2deg, K1, style=Coul1);
plot2d(ang * %CL_rad2deg, K2, style=Coul2);

// legend(['along tilt direction','perp. to tilt direction'], pos="in_upper_left", boxed=%t);
CL_g_legend(a, ['along tilt dir.','perp. to tilt dir.']);

// general setting
CL_g_stdaxes(a);
a.title.text = "Enlargment factor (Altitude=" + string(alt/1000) + "km)";
a.x_label.text = "Tilt angle to vertical (deg)";
a.grid_position = "background";

// adjustments
h = CL_g_select(a, "Legend");
h.background = Blanc;
h.font_size = 3;

h = CL_g_select(a, "Polyline");
h.thickness=2;

f.immediate_drawing="on";
f.visible="on";

