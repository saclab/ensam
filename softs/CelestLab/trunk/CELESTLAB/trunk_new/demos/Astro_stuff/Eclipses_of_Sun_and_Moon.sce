//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Date/time of eclipse of the Sun by the Moon or the Moon 
//> by the Earth for a given year.  
//> 
//> The meaning of the terms used is the following: 
//> - partial eclipse: the occulting body (Moon or Earth) partly hides the 
//>   occulted body (Sun or Moon), but is not completely inside the outer 
//>   limits of the occulted body. 
//> - full eclipse: the occulting body either completely hides the occulted
//>   body (total eclipse) or lies inside the outer limits of the occulted 
//>   body, leaving some parts visible (annular eclipse).  
//> 
//> Notes: 
//> - All bodies are assumed spherical. 
//> - Eclipse start/end times are rounded-off to the nearest minute. 
//> - Eclipses of very short length may not be detected.   
//> - It is not computed: 
//>   -> For which locations on Earth the eclipse is visible, 
//>   -> If a "full" eclipse is total or annular. 
//
// Author: A. Lamy
// -----------------------------------------------------------

// ========================
// Internal functions
// ========================

// intervals where z <= 0
// z, ang_sun_moon: see below
// all sizes: 1xN
function [interv, body] = compute_interv(cjd, z, ang_sun_moon)
  // find when z is negative
  interv = CL_detectSign(cjd, z, "-"); 
  body = []; 
  
  // round off interval
  interv = round(interv*1440)/1440; 
  
  I = find(interv(2,:) - interv(1,:) <= 0); 
  interv(:,I) = []; 
  if (interv == []); return; end
  
  td = CL_dat_cal2str(CL_dat_cjd2cal(interv(1,:))); 
  tf = CL_dat_cal2str(CL_dat_cjd2cal(interv(2,:))); 

  // ang > 0 => eclipse of Moon
  // ang < 0 => eclipse of Sun
  ang = CL_interpLagrange(cjd, ang_sun_moon-%pi/2, (interv(1,:) + interv(2,:))/2); 
  body = emptystr(ang); 
  body(:) = "Sun";
  I = find(ang >= 0); 
  body(I) = "Moon"; 
endfunction 

// print intervals
// leg (string): legend
function print_res(interv, leg); 
  mprintf("\n%s\n", leg);
  if (interv == [])
	mprintf("None\n"); 
  else
    str1 = strsubst(CL_dat_cal2str(CL_dat_cjd2cal(interv(1,:))), ":00.000", ""); 
	str2 = strsubst(CL_dat_cal2str(CL_dat_cjd2cal(interv(2,:))), ":00.000", ""); 
	mprintf("%s  =>  %s\n", str1', str2'); 
  end
endfunction

// plot intervals
// cjd0: date of beginning of year
function [e, leg] = plot_rect(cjd0, interv, icol, leg)
  e = [];
  if (interv == []); leg = []; return; end

  interv = interv - cjd0; 
  // select days with intersection with interv to increase efficiency
  days = unique(matrix(floor(interv), 1, -1)); 
  for d = days
    I = CL_intervInters(interv, [d; d+1]); 
	for (k = 1:length(I(1,:)))
      h = (I(:,k) - d)*24; 
      rect = [d; h(2); 1; h(2)-h(1)]; 
      xrects(rect, icol);  
	  // add plot for legend
	  plot([d, d+1, d+1, d], [h(1), h(1), h(2), h(2)]); 
	  e = CL_g_select(gce(), "Polyline"); 
	  e.line_mode = "on"; 
	  e.thickness = 4; 
	  e.foreground = icol; 
	  e.background = icol; 
    end
  end
  // check if something plotted
  if (e == []); leg = []; end
endfunction


// ========================
// MAIN 
// ========================

// ------------------------
// Input of parameters
// ------------------------
cal_now = CL_dat_now("cal"); 
year = cal_now(1); 

desc_param = list(..
   CL_defParam("Year", year, valid='$x>=1800 & $x <=2200') ..
);

[year] = CL_inputParam(desc_param);


// ------------------------
// Computation
// ------------------------
cjd0 = CL_dat_cal2cjd(year, 1, 1); 
cjdf = CL_dat_cal2cjd(year+1, 1, 1); 

cjd = cjd0 + (cjdf-cjd0) * linspace(0,1,18000); // step ~30min

// Body position from Earth (frame=EOD => faster)
pos_moon = CL_eph_moon(cjd, frame="EOD"); 
pos_sun = CL_eph_sun(cjd, frame="EOD"); 

// Body radii (m)
Rsun = CL_dataGet("body.Sun.eqRad"); 
Rearth = CL_dataGet("body.Earth.eqRad"); 
Rmoon = CL_dataGet("body.Moon.eqRad"); 

// A: apex of Sun-Earth umbra cone (in the direction opposite Sun)
// (cone tangent to Earth and Sun)
// NB: assumes Rsun > Rearth
// x = distance from Earth center to A 
x = CL_norm(pos_sun) * (Rearth / (Rsun-Rearth)); 
A = CL_dMult(CL_unitVector(pos_sun), -x); 

// apparent radii from A: 
app_radius_moon = asin(Rmoon ./ CL_norm(pos_moon - A)); 
app_radius_earth = asin(Rearth ./ CL_norm(A)); 

// angle (from A) betwen A->Earth and A->Moon
// NB: Earth = [0;0;0]
ang = CL_vectAngle(-A, pos_moon-A); 

// angle from Earth between Sun and Moon
ang_sun_moon = CL_vectAngle(pos_sun, pos_moon);

// quantities used for detection
z1 = ang - app_radius_moon - app_radius_earth; 
z2 = ang + app_radius_moon - app_radius_earth; 

// To simplify the algorithm, we check that: 
// 1) app_radius_moon < app_radius_earth (from A)
// 2) moon always between A and Sun 
// (this should be true) 
I = find(app_radius_moon >= app_radius_earth | CL_dot(pos_moon-A, -A) <= 0); 
if (I <> [])
  Error("*** Unexpected situation encountered"); 
end

// intervals where z <= 0: 
// z1 <= 0 => partial eclipse
// z2 <= 0 => full eclipse (total or annular) 
[interv1, body1] = compute_interv(cjd, z1, ang_sun_moon); 
[interv2, body2] = compute_interv(cjd, z2, ang_sun_moon); 

// intervals for Moon and Sun
// (part = partial or full) 
ecl_sun_part = interv1(:,find(body1=="Sun")); 
ecl_sun_full = interv2(:,find(body2=="Sun")); 
ecl_moon_part = interv1(:,find(body1=="Moon")); 
ecl_moon_full = interv2(:,find(body2=="Moon")); 


// ------------------------
// Display results
// ------------------------
mprintf("\n------------------------\n");
mprintf("Eclipses in %d (TREF):\n", year);

print_res(ecl_sun_part, "Sun - partial or full:"); 
print_res(ecl_sun_full, "Sun - full:"); 
print_res(ecl_moon_part, "Moon - partial or full:"); 
print_res(ecl_moon_full, "Moon - full:"); 


// ------------------------
// Plot
// ------------------------
f = scf(); 
f.visible = "on"; 
f.immediate_drawing = "off"; 

a = gca(); 
a.data_bounds = [0, 0; cjdf-cjd0, 24]; 
a.tight_limits = "on"; 
a.axes_visible = ["on", "on", "off"];

ent = []; 
leg = []; 

[ent($+1), leg($+1)] = plot_rect(cjd0, ecl_sun_part, color("gold"), "Sun - partial"); 
[ent($+1), leg($+1)] = plot_rect(cjd0, ecl_sun_full, color("red"), "Sun - full"); 
[ent($+1), leg($+1)] = plot_rect(cjd0, ecl_moon_part, color("green"), "Moon - partial");
[ent($+1), leg($+1)] = plot_rect(cjd0, ecl_moon_full, color("blue"), "Moon - full"); 

CL_g_legend(ent, leg); 

x = [CL_dat_cal2cjd(year, 1:12, 1), cjdf] - cjd0; 
x_ticks = (x(1:$-1) + x(2:$))/2; 
month_names = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']; 

a.x_ticks = tlist(["ticks", "locations", "labels"], x_ticks, month_names); 
a.y_ticks = tlist(["ticks", "locations", "labels"], 0:2:24, string(0:2:24)); 

plot([x;x], [0;24]*ones(x)); 
e = CL_g_select(gce(), "Polyline"); 
e.foreground = color("grey30");
plot([0;cjdf-cjd0], repmat(0:2:24,2,1)); 
e = CL_g_select(gce(), "Polyline"); 
e.foreground = color("grey30");

a.title.text = "Sun and Moon eclipses in " + sprintf("%d", year); 
a.x_label.text = "Month"; 
a.y_label.text = "Time (hours, TREF)"; 

CL_g_stdaxes(a); 
a.grid = [-1, -1]; 
a.sub_ticks = [0, 0]; 

f.immediate_drawing = "on"; 
f.visible = "on"; 
