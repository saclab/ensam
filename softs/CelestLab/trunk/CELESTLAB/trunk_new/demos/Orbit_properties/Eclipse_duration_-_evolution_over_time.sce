//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Eclipse length (evolution over time) 
//> The orbit is circular. 
//
// Author: A. Lamy
// -----------------------------------------------------------

alt = 700.e3;  // altitude (m)
sso = 1; 
inc = 0; 
hloc0 = 6:12; // initial local time (h)
T = CL_unitConvert(1,"yr","day"); // simu duration (days)

desc_param = list(..
  CL_defParam("Altitude of circular orbit", alt, units=['m','km'], valid='$x>=0'),..
  CL_defParam("Sun-synchronous orbit? (1=yes, 0=no)", sso, id='$sso', accv=[0,1]),..
  CL_defParam("Inclination", inc, units=['rad','deg'], id='$inc', valid='$sso == 1 | ($inc>=0 & $inc <= 180)'),..
  CL_defParam("Values of initial mean local time of asc. node", hloc0, units=['h'], dim=[1, 20], valid='$x>=0 & $x<=24'),..
  CL_defParam("Simulation time length", T , units=['day','yr'], valid='$x>0 & $x<=10')..
);

[alt, sso, inc, hloc0, T] = CL_inputParam(desc_param);

// -----------------------------------------------------------
// Computation
// -----------------------------------------------------------

// Initial date
cal = CL_dat_now("cal"); 
cjd0 = CL_dat_cal2cjd(cal(1),1,1,0,0,0);   // initial date

sma = alt + %CL_eqRad; // semi major axis
ecc = 0; // eccentricity
if (sso == 1); inc = CL_op_ssoJ2('i', sma, ecc); end

[pomdot, gomdot, anmdot] = CL_op_driftJ2(sma, ecc, inc);  // compute drifts due to j2

t = 0 : ceil(T); // relative dates
pos_sun = CL_eph_sun(cjd0+t);  //compute sun position starting at t0 (in ECI)
pos_sun_sph = CL_co_car2sph(pos_sun); // spherical coordinates

// beta angle for each initial local time 
result = []; 

for k = 1:length(hloc0)
  gom0 = CL_op_locTime(cjd0, 'mlh', hloc0(k), 'ra');  // initial right ascension of RAAN
  gom = gom0 + gomdot * (t*86400); // raan =f(t)
  betaa = CL_gm_raan2beta(inc, gom, pos_sun_sph(1,:), pos_sun_sph(2,:));
  ecl_dur = 2 * CL_gm_betaEclipse(sma, betaa) / (pomdot + anmdot); // seconds
  result = [result; ecl_dur]; 
end

// -----------------------------------------------------------
// Plot
// -----------------------------------------------------------

f = scf(); 
f.visible = "on";
f.immediate_drawing="off";

nb = length(hloc0);
f.color_map = jetcolormap(min(max(nb,3),100));
Noir = addcolor([0,0,0]);

a = gca();

for k = 1:length(hloc0)
  plot(t, result(k,:) / 60);
  h = CL_g_select(gce(), "Polyline");
  h.thickness = 2;
  h.foreground = k; 
end

a.title.text = "Time spent in Earth shadow";
a.x_label.text = msprintf("Days from January 1st (%d)", cal(1));
a.y_label.text = "Duration (min)";

a.data_bounds = [t(1), min(result/60) * 0.98; t($), max(result/60) * 1.02]; 
a.tight_limits = "on";

CL_g_legend(a, header = "Initial MLTAN (h)", str=string(hloc0));

CL_g_stdaxes(a);

f.immediate_drawing="on";
f.visible = "on";


