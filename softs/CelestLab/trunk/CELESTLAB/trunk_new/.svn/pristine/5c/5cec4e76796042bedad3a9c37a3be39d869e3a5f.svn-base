<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 Copyright (c) CNES  2008
 
 This software is part of CelestLab, a CNES toolbox for Scilab
 
 This software is governed by the CeCILL license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 'http://www.cecill.info'.
 -->
<!--
 * 
 * CelestLab configuration (preferences)
 * 
 -->
<refentry version="5.0-subset Scilab" xml:id="Configuration" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 01-Dec-2012 $</pubdate>
  </info>

  <refnamediv>
    <refname>Configuration</refname>
    <refpurpose>Configuration of CelestLab</refpurpose>
  </refnamediv>

  <refsection>
    <title>Configuration overview</title>
    
    <para>A few configuration parameters exist in CelestLab. They are used to set global properties such as 
    how warning messages are handled or how the ECI frame is defined. </para>
    <para>The configuration values are initially loaded from CelestLab configuration file: 
    <literal>celestlab_config.txt</literal> located in <literal>SCIHOME</literal> if it exists and is valid, 
    <literal>config_default.txt</literal> located in CelestLab home directory otherwise. </para>
	  <para> The parameter values can then be retrieved by <link linkend="CL_configGet">CL_configGet</link> and modified by
    <link linkend="CL_configSet">CL_configSet</link>. </para>
    <para> To update the configuration file, see "Edit configuration file" in CelestLab menu. </para>
    <para/>
  </refsection>
   

  <refsection>
    <title>Configuration parameters</title>
    <para> The configuration options are the following: </para>
    <itemizedlist>
      <listitem>
        <emphasis role="bold">DISPLAY_MENU</emphasis> (not changeable*) :
        <itemizedlist>
          <listitem><emphasis role="bold">yes</emphasis> : The CelestLab menu is displayed if Scilab is launched in 'standard' mode (default).</listitem>
          <listitem><emphasis role="bold">no</emphasis> : The CelestLab menu is not displayed.</listitem>
        </itemizedlist> 
      </listitem>

      <listitem>
	    <emphasis role="bold">WARNING_MODE</emphasis> :
        <itemizedlist>
          <listitem><emphasis role="bold">standard</emphasis> : Warning messages are printed in the console (default).</listitem>
          <listitem><emphasis role="bold">silent</emphasis> : Warning messages are not printed.</listitem>
          <listitem><emphasis role="bold">error</emphasis> : Warning messages are turned into error messages.</listitem>
        </itemizedlist> 
      </listitem>

      <listitem>
	    <emphasis role="bold">VERBOSE_MODE</emphasis> :
        <itemizedlist>
          <listitem><emphasis role="bold">standard</emphasis> : Additional messages are printed in the console (default).</listitem>
          <listitem><emphasis role="bold">silent</emphasis> : Additional messages are not printed.</listitem>
        </itemizedlist> 
      </listitem>
      
      <listitem>
	    <emphasis role="bold">WARN_DEPRECATED</emphasis> :
        <itemizedlist>
          <listitem><emphasis role="bold">yes</emphasis> : Deprecated functions warning messages are printed (once) in the console (default).</listitem>
          <listitem><emphasis role="bold">no</emphasis> : Deprecated functions warning messages are not printed.</listitem>
        </itemizedlist> 
      </listitem>

      <listitem>
	    <emphasis role="bold">ECI_FRAME</emphasis> (not changeable*) :
        <itemizedlist>
          <listitem><emphasis role="bold">CIRS</emphasis> : The ECI frame is CIRS, the ECF frame is TIRS (default).</listitem>
          <listitem><emphasis role="bold">Veis</emphasis> : The ECI frame is Veis, the ECF frame is PEF.</listitem>
        </itemizedlist> 
      </listitem>
      
      <listitem>
	    <emphasis role="bold">TLE_WHICHCONST</emphasis> : Set of constants used by TLE functions
        <itemizedlist>
          <listitem><emphasis role="bold">wgs72</emphasis> : The set of constants used by TLE functions is consistent 
          with wgs72 (default).</listitem>
          <listitem><emphasis role="bold">wgs72old</emphasis> : The set of constants used by TLE functions is consistent 
          with wgs72 - older values.</listitem>
          <listitem><emphasis role="bold">wgs84</emphasis> : The set of constants used by TLE functions is consistent 
          with wgs84.</listitem>
        </itemizedlist> 
      </listitem>

      <listitem>
	    <emphasis role="bold">TLE_OPSMODE</emphasis> : Mode of operation for TLE functions
        <itemizedlist>
          <listitem><emphasis role="bold">afspc</emphasis> : Processing compatible with AFSPC (default).</listitem>
          <listitem><emphasis role="bold">impr</emphasis> : Improved processing (see Vallado code for details).</listitem>
        </itemizedlist> 
      </listitem>
      
      <listitem>
	    <emphasis role="bold">STELA_WHICHCONST</emphasis> : Set of physical constants used by 
      STELA orbit propagation model
        <itemizedlist>
          <listitem><emphasis role="bold">CL</emphasis> : The constants used are the same as in CelestLab.</listitem>
          <listitem><emphasis role="bold">ST</emphasis> : The constants used are the ones defined in STELA's configuration directory (default).</listitem>
        </itemizedlist> 
      </listitem>

    </itemizedlist>
     <para>*: not changeable by <link linkend="CL_configSet">CL_configSet</link> function.</para>
    <para/>
  </refsection>
  
</refentry>
