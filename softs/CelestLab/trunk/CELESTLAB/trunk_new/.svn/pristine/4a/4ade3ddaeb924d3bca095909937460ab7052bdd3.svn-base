// ----------------------------------------------------------------------------------------
// CL_fr_bodyConvert
// Test for catching possible errors
// Internal coherence test
// ----------------------------------------------------------------------------------------

TEST_OK = [];

// Define TREF
%CL_TT_TREF = 67.184; 

/////////////////////////////////////////
// Test1: Not enough arguments (min 5) //
/////////////////////////////////////////

try 
    CL_fr_bodyConvert("Mars","ICRS","BCI");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

////////////////////////////////////////////
// Test2: Invalid input argument ''body'' //
////////////////////////////////////////////
Mars = [];
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
pos_EME2000 = [1e5;3e4;6e6];
vel_EME2000 = [-1e3;3e3;6e3];
try
    [pos2,vel2,jacob] = CL_fr_bodyConvert(Mars,"ICRS","BCI",cjd,pos_EME2000,vel_EME2000);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

//////////////////////////////////////////////
// Test3: Invalid input argument ''frame1'' //
//////////////////////////////////////////////
ICRS = [];
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
pos_EME2000 = [1e5;3e4;6e6];
vel_EME2000 = [-1e3;3e3;6e3];
try
    [pos2,vel2,jacob] = CL_fr_bodyConvert("Mars",ICRS,"BCI",cjd,pos_EME2000,vel_EME2000);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

//////////////////////////////////////////////
// Test4: Invalid input argument ''frame2'' //
//////////////////////////////////////////////
BCI = [];
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
pos_EME2000 = [1e5;3e4;6e6];
vel_EME2000 = [-1e3;3e3;6e3];
try
    [pos2,vel2,jacob] = CL_fr_bodyConvert("Mars","ICRS",BCI,cjd,pos_EME2000,vel_EME2000);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

/////////////////////////////////////
// Test 5: Internal coherence test //
/////////////////////////////////////
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
pos_EME2000 = [1e5;3e4;6e6];
vel_EME2000 = [-1e3;3e3;6e3];

[pos_BCI,vel_BCI,jacob_BCI] = CL_fr_bodyConvert("Mars","ICRS","BCI",cjd,pos_EME2000,vel_EME2000);
[pos_BCF,vel_BCF,jacob_BCF] = CL_fr_bodyConvert("Mars","BCI","BCF",cjd,pos_BCI,vel_BCI);
[pos_ICRS,vel_ICRS,jacob_ICRS] = CL_fr_bodyConvert("Mars","BCF","ICRS",cjd,pos_BCF,vel_BCF);

TEST_OK(1+$) = CL__isEqual(pos_EME2000(1),pos_ICRS(1))& CL__isEqual(pos_EME2000(2),pos_ICRS(2))& ...
               CL__isEqual(pos_EME2000(3),pos_ICRS(3));
TEST_OK(1+$) = CL__isEqual(vel_EME2000(1),vel_ICRS(1))& CL__isEqual(vel_EME2000(2),vel_ICRS(2))& ...
               CL__isEqual(vel_EME2000(3),vel_ICRS(3));
               
//////////////////////////////////////////////////
// Test 6: Optional argument velocity not asked //
//////////////////////////////////////////////////
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
pos_EME2000 = [1e5;3e4;6e6];

[pos_BCI] = CL_fr_bodyConvert("Mars","ICRS","BCI",cjd,pos_EME2000);
[pos_ICRS] = CL_fr_bodyConvert("Mars","BCI","ICRS",cjd,pos_BCI);

TEST_OK(1+$) = CL__isEqual(pos_EME2000(1),pos_ICRS(1))& CL__isEqual(pos_EME2000(2),pos_ICRS(2))& ...
               CL__isEqual(pos_EME2000(3),pos_ICRS(3));