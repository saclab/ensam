<?xml version="1.0" encoding="ISO-8859-1"?>
<!--
 Copyright (c) CNES  2008
 
 This software is part of CelestLab, a CNES toolbox for Scilab
 
 This software is governed by the CeCILL license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 'http://www.cecill.info'.
 -->
<!--
 * 
 * Ephemerides.
 * 
 -->
<refentry version="5.0-subset Scilab" xml:id="Ephemerides" xml:lang="en"
          xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:ns3="http://www.w3.org/1999/xhtml"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:db="http://docbook.org/ns/docbook">
  <info>
    <pubdate>$LastChangedDate: 27-Oct-2009 $</pubdate>
  </info>

  <refnamediv>
    <refname>Ephemerides for celestial bodies</refname>
    <refpurpose>Ephemerides for solar system bodies</refpurpose>
  </refnamediv>

  <refsection>
    <title>Overview of ephemerides models</title>
    <para>There are several ephemerides models available in CelestLab to compute the position and velocity
    of bodies of the solar system:</para>
    <itemizedlist>
    <listitem><para><emphasis role="bold">Meeus</emphasis> model for the Sun</para>
	    <para>This is a very simple analytical model.</para>
      <para>The origin is the Earth and the frame is the ecliptic of date (EOD).</para>
      <para><emphasis role="italic">Reference: Astronomical Algorithms J.Meeus - Second edition 1998, Chapter 25</emphasis></para>
    </listitem>
    <para/>          
    <listitem><para><emphasis role="bold">Meeus</emphasis> model for the Moon</para>
	    <para>This is an analytical model of medium complexity.</para>
      <para>The maximum number of harmonics to compute the longitude, latitude and distance is 62, 66 and 46.</para>
      <para>The origin is the Earth and the frame is the ecliptic of date (EOD).</para>
      <para><emphasis role="italic">Reference: Astronomical Algorithms J.Meeus - Second edition 1998, Chapter 47</emphasis></para>
    </listitem>
    <para/>          
    <listitem><para><emphasis role="bold">VSOP87</emphasis> model for solar system bodies</para>
	    <para>This is an analytical model of high complexity.</para>
      <para>The full model uses thousands of harmonics for each variable (longitude, latitude, distance).</para>
      <para>The origin is the Sun and the frame is the ecliptic of date (EOD).</para>
      <para><emphasis role="italic">Reference: Planetary Theories in rectangular and spherical variables: VSOP87 solution. Bretagnon P., Francou G.</emphasis></para>
    </listitem>
    <para/>          
    <listitem><para><emphasis role="bold">ELP/MPP02</emphasis> model for the Moon</para>
	    <para>This is an analytical model of high complexity.</para>
      <para>The full model uses dozens of thousands of harmonics for each variable (longitude, latitude, distance).</para>
      <para>The origin is the Earth and the frame is the ecliptic of date (EOD).</para>
      <para><emphasis role="italic">Reference: LUNAR SOLUTION ELP version ELP/MPP02 - Jean CHAPRONT and Gerard FRANCOU 
            Observatoire de Paris - SYRTE department - UMR 8630/CNRS - October 2002</emphasis></para>
    </listitem>
    <para/>    
    <listitem><para><emphasis role="bold">JPL's DE405</emphasis> ephemerides for solar system bodies</para>
      <para>The ephemerides are obtained by interpolation in pre-computed data stored in binary files, 
            and thus are available only for a limited period of time ([1960-2200] by default, the 
            maximum time span being [1600-2200]). The binary files are Scilab binary files made from 
            the original ascii files that can be downloaded from:   
            <ulink url="ftp://ssd.jpl.nasa.gov/pub/eph/planets/ascii/de405">
            ftp://ssd.jpl.nasa.gov/pub/eph/planets/ascii/de405</ulink>. </para>            
      <para><emphasis role="italic">Reference: <ulink url="http://ssd.jpl.nasa.gov/?ephemerides">NASA/JPL ephemerides</ulink></emphasis></para>
    </listitem>
    
    </itemizedlist>
    
    <imageobject><imagedata fileref="images/ephemerides_table_1.gif" /></imageobject>
   
    <para>General information about the models complexity and accuracy is given in the following table :</para>
    <imageobject><imagedata fileref="images/ephemerides_table_2.gif" /></imageobject>
    
    <para>See CelestLab functions :  
    <link linkend="CL_eph_moon">CL_eph_moon</link>, 
	  <link linkend="CL_eph_sun">CL_eph_sun</link>, 
    <link linkend="CL_eph_planet">CL_eph_planet</link> and
    <link linkend="CL_eph_de405">CL_eph_de405</link>.
	  </para>
    
    <para/>
  </refsection>

	
  <refsection>
    <title>Planet barycenters</title>
    
    <para>All the models are compared to DE405 although DE405 gives the position of 
     the barycenter of the planet and its satellites (except for Earth). 
     In practice it doesnt matter because the accuracy of the models are below the angular 
     separation of the planet and the barycenter of the planet: </para>
     <imageobject><imagedata fileref="images/ephemerides_bary.gif" /></imageobject>
    <para/>
    
  </refsection>
    
  
</refentry>
