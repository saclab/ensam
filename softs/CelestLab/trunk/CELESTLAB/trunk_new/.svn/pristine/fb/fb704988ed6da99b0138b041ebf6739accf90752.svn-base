//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
// ========================
// Test CLx_stela_convert
//
// Tests:
// mean => osculating
// osculating => mean
//
// Nota: reference data obtained using STELA GUI (see file "procedure.txt")
// ========================
tlbx_path = CLx_home();
exec(fullfile(tlbx_path, "tests", "unit_tests", "CLx_create_extrapolation_inputdata.sci"));
//==================
// Test mean => osc
//==================
x_mean = [[24678136.0 0.7172341 0.0 0.0874887 0.0 0.0]',..
          [24678.10549617032*1000 0.7159941386228384 0.0443796214177053 0.08719532548933541 -0.00555467906976616 147.7691636806852*%pi/180]'];
// Convert to osculating elements
x_osc = CLx_stela_convert("mean2osc", cjd0 + delta_t([1,$])/86400, x_mean, params, ut1_tref, tt_tref); 
// Expected osculating elements (type == cireq)
expected_xosc   =  [[24751.998735646495*1000 0.7182370270987048 2.017576758330134E-7 0.08752782659036383 2.435279176584039E-7 359.9998726161926*%pi/180]',..
                    [24673.648650598523*1000 0.7159145697387739 0.044573330439039355 0.08719419611699264 -0.005579660315111052 147.78477851745353*%pi/180]'];
epsilon = 1.e-11;
assert_checkalmostequal(x_osc, expected_xosc, epsilon, epsilon);
//==================
// Test osc => mean
//==================
x_osc   =  [[24751.998735646495*1000 0.7182370270987048 2.017576758330134E-7 0.08752782659036383 2.435279176584039E-7 359.9998726161926*%pi/180]',..
           [24673.648650598523*1000 0.7159145697387739 0.044573330439039355 0.08719419611699264 -0.005579660315111052 147.78477851745353*%pi/180]'];
// Convert to mean elements
x_mean = CLx_stela_convert("osc2mean", cjd0 + delta_t([1,$])/86400, x_osc, params, ut1_tref, tt_tref); 
// Expected mean elements (type == cireq)
expected_xmean = [[24678136.0 0.7172341 0.0 0.0874887 0.0 0.0]',..
                 [24678.10549617032*1000 0.7159941386228384 0.0443796214177053 0.08719532548933541 -0.00555467906976616 147.7691636806852*%pi/180]'];
epsilon = 1.e-11;
assert_checkalmostequal(x_mean, expected_xmean, epsilon, epsilon);
