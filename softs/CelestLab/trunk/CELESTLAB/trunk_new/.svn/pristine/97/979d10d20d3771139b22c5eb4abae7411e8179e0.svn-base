//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [tle] = CL_tle_setElem(tle, type_oe, cjd, mean_oe, frame, ut1_tref, tt_tref)
// Sets TLE orbital state 
//
// Calling Sequence
// [tle] = CL_tle_setElem(tle, type_oe, cjd, mean_oe [, frame, ut1_tref, tt_tref])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Modifies the mean orbital elements in a TLE structure. </p>
// <p>The input orbital elements can be given in various forms: Keplerian elements, position and velocity... and in any frame. </p>
// <p></p>
// <p>In a standard situation, the orbital elements are converted from the specified reference <b>frame</b> to TEME. The epoch is converted from the TREF time scale to UTC. Note that if the frame is not a true equator frame, the transformation may not as accurate as should be as the frame transformation arguments that can be provided are limited to ut1_tref and tt_tref. </p>
// <p></p>
// <p>It is also possible to give <b>frame</b> a special value: "native". It means that the input reference frame AND 
// time scale are those natively used for TLEs: TEME and UTC repectively. No conversion is performed. </p> 
// </listitem>
// </itemizedlist>
// 
// Parameters
// tle: TLE structure (size N)
// type_oe: (string) Type of output orbital elements: "kep", "cir", "cireq", "equin" or "pv" (1x1)
// cjd: TLE epoch (modified julian date from 1950.0, TREF time scale except if frame == "native") (1xN or 1x1)
// mean_oe: Mean orbital elements (of type given by "type_oe") relative to the specified frame [m, m/s, rad] (6xN)
// frame: (string, optional) Output frame, see above for details. Default is "ECI". 
// ut1_tref: (optional) UT1-TREF [seconds]. Default is %CL_UT1_TREF (1xN or 1x1)
// tt_tref: (optional) TT-TREF [seconds]. Default is %CL_TT_TREF (1xN or 1x1)
//
// Authors
// CNES - DCT/SB/MS
//
// See also
// CL_tle_getElem
//
// Examples
// tle = CL_tle_new(); 
// // cjd in TREF, mean Keplerian elements relative to ECI
// cjd = 18000; 
// mean_kep = [7200.e3; 0.1; 1; 0; 0; 0]; 
// tle = CL_tle_setElem(tle, "kep", cjd, mean_kep) 
// // Check results
// [mean_kep2, cjd2] = CL_tle_getElem(tle, "kep")


// Declarations:

// Code
if (~exists("frame", "local")); frame="ECI"; end
if (~exists("ut1_tref", "local")); ut1_tref = CL__dataGetEnv("UT1_TREF"); end 
if (~exists("tt_tref", "local")); tt_tref = CL__dataGetEnv("TT_TREF"); end 

// TLE propagation options from configuration
whichconst = CL_configGet("TLE_WHICHCONST"); 
opsmode = CL_configGet("TLE_OPSMODE");

// Orbital elements types
Types_oe = [ "kep", "cir", "cireq", "equin", "pv" ]; 

// check validity of TLE struct
CL__tle_checkValid(tle); 

if (typeof(frame) <> "string" | size(frame, "*") <> 1)
  CL__error("Invalid argument type or size for frame"); 
end

if (typeof(type_oe) <> "string" | size(type_oe, "*") <> 1)
  CL__error("Invalid argument type or size for type_oe"); 
end

if (find(Types_oe == type_oe) == [])
  CL__error("Invalid orbital element type"); 
end

[cjd, mean_oe, N1] = CL__checkInputs(cjd,1, mean_oe, 6); 

N = size(tle); 

if ~(N == N1 | (N > 0 & N1 == 1))
  CL__error("Invalid size of input arguments"); 
end

// resize to size of tle
if (N1 == 1)
  cjd = repmat(cjd, 1, N)
  mean_oe = repmat(mean_oe, 1, N)
end

// Nothing done if empty sizes
if (N == 0); return; end

// Constants used: 
cst = CL_tle_getConst(); 

// if frame <> "native": 
// - computes mean Keplerian (TEME)
// - converts cjd to UTC
// if frame == "native": 
// - only converts to Keplerian elements

if (frame == "native")
  // keplerian elements / TEME
  kep = CL_oe_convert(type_oe, "kep", mean_oe, cst.mu); 

else
  // converts to position and velocity 
  pv = CL_oe_convert(type_oe, "pv", mean_oe, cst.mu); 

  // change of frame (-> TEME)
  // NB: time scale is TREF
  [pv(1:3,:), pv(4:6,:)] = CL_fr_convert(frame, "TEME", cjd, pv(1:3,:), pv(4:6,:), ut1_tref=ut1_tref, tt_tref=tt_tref); 

  // converts to keplerian elements / TEME
  kep = CL_oe_convert("pv", "kep", pv, cst.mu); 

  // UTC
  cjd = CL_dat_scaleConvert("TREF", "UTC", cjd, ut1_tref, tt_tref); 

end


// mean motion from sma
Inok = find(kep(1,:) <= 0); 
kep(:,Inok) = %nan; 

// Mean motion
n = CL__tle_sma("a2n", kep(1,:), kep(2,:), kep(3,:), whichconst); 

tle = CL_tle_set(tle, "epoch_cjd", cjd, "ecc", kep(2,:), "inc", kep(3,:), "argp", kep(4,:), "raan", kep(5,:), "ma", kep(6,:), "n", n, "ndot", 0, "nddot", 0); 

     
endfunction
