//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


function [tle] = CL_tle_new(num)
// Creation of TLE structure
//
// Calling Sequence
// tle = CL_tle_new([num])
//
// Description
// <itemizedlist>
// <listitem>
// <p>This function creates a TLE structure with all fields initialized with default values. Epoch and orbital elements are set to %nan. The drag coefficient (bstar) is set to 0.  </p>
// <p>After the structure has been created, the values can be altered for instance by using <link linkend="CL_tle_set">CL_tle_set</link>.  </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Propagation models">Propagation models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// num: Number of elements in the TLE structure. Default is 1
//
// Authors
// CNES - DCT/SB/MS
//
// See also
// CL_tle_set
// CL_tle_parse
// CL_tle_load
//
// Examples
// // Create a TLE structure containing 1 element
// tle = CL_tle_new() 
// // Create a TLE structure containing 10 elements
// tle = CL_tle_new(10); 
// tle(10)



// Default value for num 
if (~exists("num", "local")); num = 1; end

// Check that num is a valid number
if (round(num) <> num | num < 0)
  CL__error("Invalid input argument (integer >= 0 expected)"); 
end


// Create structure with default values
N = num; 
classif = repmat("U",1,N); 
intldesg = repmat("00000000",1,N); // 8 characters
ephtype = zeros(1,N); 
elnum = zeros(1,N); 
revnum = zeros(1,N); 
ndot = zeros(1,N); 
nddot = zeros(1,N); 
status = zeros(1,N); 
desc = emptystr(1,N);
satnum = zeros(1,N); 
epoch_cjd = %nan * ones(1,N); 
ecc = %nan * ones(1,N); 
inc = %nan * ones(1,N); 
argp = %nan * ones(1,N); 
raan = %nan * ones(1,N); 
ma = %nan * ones(1,N); 
n = %nan * ones(1,N); 
bstar = zeros(1,N); 

tle = CL__tle_create(satnum, classif, intldesg, ephtype, elnum, epoch_cjd, revnum, ecc, inc, argp, raan, ma, n, ndot, nddot, bstar, desc, status);


endfunction
