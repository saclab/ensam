// ------------------------------
// CL_cw_lambert : 
// ------------------------------

TEST_OK = [];

////////////////////////////////////////////////////
// Test1: cross validation with CL_cw_propagation //
////////////////////////////////////////////////////
p1 = [10.e3;10.e3;100.e3];
p2 = [-10.e3;100.e3;20.e3];
delta_t = 1000; // sec
alt = 450.e3;
acc = [0.0028535;-0.0000791;-0.0035475]; // differential inertial acceleration

[v1,v2] = CL_cw_lambert(p1,p2,delta_t,alt,acc);
// Check
t1 = 0; 
t2 = delta_t/86400; // days

pv = CL_cw_propagate(t1,[p1;v1],t2,alt,acc);

TEST_OK($+1) = CL__isEqual([p2;v2],pv);

///////////////////////////////////////////////////////////////////////////////
// Test2: cross validation with CL_cw_propagation with inputs in vector form //
///////////////////////////////////////////////////////////////////////////////
p1 = [[10.e3;10.e3;100.e3],[20.e3;20.e3;200.e3]];
p2 = [[-10.e3;100.e3;20.e3],[-20.e3;200.e3;10.e3]];
delta_t = [500,1000]; // sec
alt = [400.e3,450.e3];

acc = [[0.0028535;-0.0000791;-0.0035475],[0.003;0.00004;-0.005]]; // differential inertial acceleration

[v1,v2] = CL_cw_lambert(p1,p2,delta_t,alt,acc);
// Check
t1 = 0; 
t2 = delta_t./86400; // days

pv = CL_cw_propagate(t1,[p1;v1],t2,alt,acc);

TEST_OK($+1)= CL__isEqual([p2;v2],pv);

///////////////////////////////
// Test3: acc optional input //
///////////////////////////////
// Check if no acc input will lead same results as if acc = 0 is used
p1 = [10.e3;10.e3;100.e3];
p2 = [-10.e3;100.e3;20.e3];
delta_t = 1000; // sec
alt = 450.e3;
acc = zeros(3,1); // differential inertial acceleration

pv = CL_cw_lambert(p1,p2,delta_t,alt);
pv_acc = CL_cw_lambert(p1,p2,delta_t,alt,acc);

TEST_OK($+1) = CL__isEqual(pv,pv_acc);

///////////////////////////////
// Test4: acc optional input //
///////////////////////////////
// acc in vectorial form
p1 = [[10.e3;10.e3;100.e3],[20.e3;20.e3;200.e3]];
p2 = [[-10.e3;100.e3;20.e3],[-20.e3;200.e3;10.e3]];
delta_t = [500,1000]; // sec
alt = [400.e3,450.e3];
acc = [0.0028535 ; -0.0000791 ; -0.0035475]; // differential inertial acceleration

[v1,v2] = CL_cw_lambert(p1,p2,delta_t,alt,acc);
// Check
t1 = 0; 
t2 = delta_t./86400; // days

pv = CL_cw_propagate(t1,[p1;v1],t2,alt,acc);

TEST_OK($+1)= CL__isEqual([p2;v2],pv);