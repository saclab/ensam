// ------------------------------------------
// Consistency test of functions CL_oe_car2kep
// and CL_oe_kep2car for hyperbolic orbits: 
// ------------------------------------------

TEST_OK = [];

// Internal function to check that jacob is (almost) the identity 
// precision is the highest number any term can have (besides the diagonal which should be ones)
// Note: If there are %nan values, they are ignored (i.e they are accepted)
function [is_identity, max_prec] = test_jacob(jacob,precision)
  nd = length(size(jacob)); // number of dimensions
  if (nd == 3)
    N = size(jacob,3);
  else
    N = 1; // if jacob is a matrix
  end
  
  max_prec = 0;
  for k = 1 : N
    // Matrix with only the diagonal
    Idiag = diag(diag(jacob(:,:,k))');
    
    // Jacobian without its diagonal (which should be ones)
    J = jacob(:,:,k)-Idiag;
    
    // Maximum value outside the diagonal
    max_prec = max([max(J),max_prec]);
    
  end
  
  is_identity = ~(max_prec > precision);
  
endfunction

// ------------------------------------------
// TEST 1 : 
// Hyperbolic orbit (non equatorial):
// ------------------------------------------
a = 7000.e3;
e = 1.5;
inc = 0.1;
pom = 0.4;
gom = 0.2;
anm = 0.3;

kep = [a;e;inc;pom;gom;anm];

               
// Check: car <-> kep
[pos1,vel1, jacob] = CL_oe_kep2car(kep);
[kep1, jacob1] = CL_oe_car2kep(pos1,vel1);
TEST_OK($+1) = CL__isEqual(kep, kep1) & ...
               test_jacob(jacob*jacob1,2e-11);

                        
// ------------------------------------------
// TEST 2 : 
// Hyperbolic, near-equatorial orbit:
// In these tests, when going to keplerian form gom is not defined, gom+anm is defined
// Also some terms of the jacobians are not defined.
// The treshold is at inc = 1e-10
// ------------------------------------------
inc = [1e-9,1e-11,1e-13,1e-15,0];
a = 7000.e3 * ones(inc);
e = 1.5 * ones(inc);
pom = 0.4 * ones(inc);
gom = 0.2 * ones(inc);
anm = 0.3 * ones(inc);

kep = [a;e;inc;pom;gom;anm];
                    
// Check: car <-> kep
[pos1,vel1, jacob] = CL_oe_kep2car(kep);
[kep1, jacob1] = CL_oe_car2kep(pos1,vel1);


TEST_OK($+1) = CL__isEqual(kep([1,3,6],:), kep1([1,3,6],:), 1e-13) & ...
               CL__isEqual(kep(4,:)+kep(5,:), kep1(4,:)+kep1(5,:), 1e-13) & ...
               test_jacob(jacob1*jacob,1e-6);
               
               
// ------------------------------------------
// TEST 3 : 
// Hyperbolic, near-parabolic orbit:
// In these tests, everything is defined but the precision 
// is not perfect for the jacobian
// ------------------------------------------
e = 1+[1e-5,1e-6,1e-7,1e-8,1e-9,1e-11];
a = 7000.e3 * ones(e);
inc = 98*%CL_deg2rad * ones(e);
pom = 0.4 * ones(e);
gom = 0.2 * ones(e);
anm = 0.3 * ones(e);

kep = [a;e;inc;pom;gom;anm];
                    
// Check: car <-> kep
[pos1,vel1, jacob] = CL_oe_kep2car(kep);
[kep1, jacob1] = CL_oe_car2kep(pos1,vel1);


TEST_OK($+1) = CL__isEqual(kep, kep1, 1e-10) & ...
               test_jacob(jacob*jacob1,3e-3);
               
// ------------------------------------------
// TEST 4 :  NOT HANDLED ANYMORE !!
// Parabolic orbit:
// Warning, first row of "kep" is the parameter p!
// ------------------------------------------
// e = 1;
// p = 7000.e3 * ones(e);
// inc = 98*%CL_deg2rad * ones(e);
// pom = 0.4 * ones(e);
// gom = 0.2 * ones(e);
// anm = 0.3 * ones(e);

// kep = [p;e;inc;pom;gom;anm];
                    
// // Check: car <-> kep
// [pos1,vel1, jacob] = CL_oe_kep2car(kep);
// [kep1, jacob1] = CL_oe_car2kep(pos1,vel1);


// TEST_OK($+1) = CL__isEqual(kep, kep1) & ...
               // test_jacob(jacob1*jacob,1e-8);
               
