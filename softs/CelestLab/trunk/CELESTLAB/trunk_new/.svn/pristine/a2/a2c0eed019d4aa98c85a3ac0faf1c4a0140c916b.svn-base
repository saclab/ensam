// -------------------------------------------------------------------
// CL_dat_scaleConvert
// Test with reference results = analytical solution of the equation
// Internal coherence test
// Check function header notes
// -------------------------------------------------------------------
// define TREF (TAI minus UTC = 35 seconds) 
%CL_TT_TREF = 67.184; 
%CL_UT1_TREF = 0; 


TEST_OK = [];

//////////////////////////////
// Test 1: UTC to TAI = 35s //
//////////////////////////////
cjd_utc = CL_dat_cal2cjd(2013,1,1);
tai_utc = (CL_dat_scaleConvert("UTC","TAI",cjd_utc,0,0)- cjd_utc) * 86400; 
tai_utc_expected = 35; 
TEST_OK($+1) = CL__isEqual(tai_utc, tai_utc_expected, 1.e-8);

/////////////////////////////////
// Test 2: TT to TAI = 32.184s //
/////////////////////////////////
cjd_tt = CL_dat_cal2cjd(2013,1,1);
tai_tt = (CL_dat_scaleConvert("TT","TAI",cjd_tt)- cjd_tt) * 86400;
tai_tt_expected = -CL_dataGet("TT_TAI");
TEST_OK($+1) = CL__isEqual(tai_tt, tai_tt_expected, 1.e-8);

//////////////////////////////////////////
// Test 3: TAI to UTC at 1/1/2013 = 35s //
//////////////////////////////////////////
cjd_tai = CL_dat_cal2cjd(2013,1,1);
utc_tai = (CL_dat_scaleConvert("TAI","UTC",cjd_tai)- cjd_tai) * 86400; 
utc_tai_expected = -35;
TEST_OK(1+$) = CL__isEqual(utc_tai, utc_tai_expected, 1.e-8);

/////////////////////////////
// Test 4: UT1 to TREF = 0s //
/////////////////////////////
cjd_ut1 = CL_dat_cal2cjd(2013,1,1);
tref_ut1 = (CL_dat_scaleConvert("UT1","TREF",cjd_ut1)- cjd_ut1) * 86400;
tref_ut1_expected = -CL_dataGet("UT1_TREF");;
TEST_OK($+1) = CL__isEqual(tref_ut1, tref_ut1_expected, 1.e-8);

//////////////////////////////////////////////
// Test 5: TCB to TAI at 1/1/1977 = 32.184s //
//////////////////////////////////////////////
cjd_tcb = CL_dat_cal2cjd(1977,1,1,0,0,32);
tai_tcb = (CL_dat_scaleConvert("TCB","TAI",cjd_tcb) - cjd_tcb) *  86400;
tai_tcb_expected = -CL_dataGet("TT_TAI"); 
TEST_OK(1+$) = CL__isEqual(tai_tcb, tai_tcb_expected, 1.e-4);

////////////////////////////////////////////////////////////////
// Test 6: Offsets between TREF and various time scales (sec) //
////////////////////////////////////////////////////////////////
cjd_tref = CL_dat_cal2cjd(2013,1,1); 

    //----UTC = 0s
    utc_tref = (CL_dat_scaleConvert("TREF","UTC",cjd_tref) - cjd_tref) *  86400;
    TEST_OK($+1) = CL__isEqual(utc_tref,0,1e-14);
    
    //----TAI = 35s
    tai_tref = (CL_dat_scaleConvert("TREF","TAI",cjd_tref) - cjd_tref) *  86400;
    tai_tref_expected = 35; 
    TEST_OK($+1) = CL__isEqual(tai_tref, tai_tref_expected, 1e-8);
    
    //----TT = 67.184s
    tt_tref = (CL_dat_scaleConvert("TREF","TT",cjd_tref) - cjd_tref) *  86400;
    tt_tref_expected = CL_dataGet("TT_TREF"); 
    TEST_OK($+1) = CL__isEqual(tt_tref, tt_tref_expected, 1e-8);
    
    //----UT1 = 0s
    ut1_tref = (CL_dat_scaleConvert("TREF","UT1",cjd_tref) - cjd_tref) *  86400;
    ut1_tref_expected = CL_dataGet("UT1_TREF");
    TEST_OK($+1) = CL__isEqual(ut1_tref, ut1_tref_expected, 1e-14);
    
    //----TDB = 67.184s
    tdb_tref = (CL_dat_scaleConvert("TREF","TDB",cjd_tref) - cjd_tref) *  86400;
    tdb_tref_expected = CL_dataGet("TT_TREF");
    TEST_OK($+1) = CL__isEqual(tdb_tref, tdb_tref_expected, 1e-8);
    
    //----TCB = 67.184s
    cjd_tref = CL_dat_cal2cjd(1977,1,1,0,0,32);
    tcb_tref = (CL_dat_scaleConvert("TREF","TCB",cjd_tref) - cjd_tref) *  86400;
    tcb_tref_expected = CL_dataGet("TT_TREF");
    TEST_OK($+1) = CL__isEqual(tcb_tref, tcb_tref_expected, 1e-6);

//////////////////////////////////////////////////
// Test 7: String expected for ts_in and ts_out //
//////////////////////////////////////////////////
ts_in = 23000;
ts_out = "TAI"
try
    cjd_tai = CL_dat_scaleConvert(ts_in,ts_out,23500);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

////////////////////////////////
// Test 8: Wrong dimension(s) //
////////////////////////////////
ts_in = ["UTC","UT1"];
ts_out = "TAI";
try
    cjd_tai = CL_dat_scaleConvert(ts_in,ts_out,23500);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

///////////////////////////////////////
// Test 9: Unrecognized time scales //
///////////////////////////////////////
ts_in = "TCG";
ts_out = "TAI";
try
    cjd_tai = CL_dat_scaleConvert(ts_in,ts_out,23500);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

/////////////////////////////
// Test 10: Handle [] case //
/////////////////////////////
cjd = [];
cjd_tai = CL_dat_scaleConvert("UTC","TAI",cjd);

TEST_OK($+1) = CL__isEqual(cjd_tai,[]);

//////////////////////////////////
// Test 11: case ts_in = ts_out //
//////////////////////////////////
ts_in = "UTC";
ts_out = "UTC";
cjd = 23000;
cjd_utc = CL_dat_scaleConvert("UTC","UTC",cjd);

TEST_OK($+1) = CL__isEqual(cjd_utc,cjd);

//////////////////////////////////////
// Test 12: Internal coherence test //
//////////////////////////////////////
cjd_tt = CL_dat_cal2cjd(2013,1,1);
cjd_tai = CL_dat_scaleConvert("TT","TAI",cjd_tt);
cjd_ut1 = CL_dat_scaleConvert("TAI","UT1",cjd_tai);
cjd_tcb = CL_dat_scaleConvert("UT1","TCB",cjd_ut1);
cjd_utc = CL_dat_scaleConvert("TCB","UTC",cjd_tcb);
cjd_tt2 = CL_dat_scaleConvert("UTC","TT",cjd_utc);

TEST_OK($+1) = CL__isEqual(cjd_tt,cjd_tt2);

//////////////////////////////////////////
// Test 13: check function header notes //
//////////////////////////////////////////
cjd_tai = CL_dat_cal2cjd(2013,1,1);

// ut1_tref = 30s, tt_tref = 60s
cjd_utc = CL_dat_scaleConvert("TAI","UTC",cjd_tai,ut1_tref = 30, tt_tref = 60);
// ut1_tref = Default %CL_UT1_TREF, tt_tref = Default %CL_TT_TREF
cjd_utc2 = CL_dat_scaleConvert("TAI","UTC",cjd_tai); 

TEST_OK($+1) = CL__isEqual(cjd_utc,cjd_utc2);