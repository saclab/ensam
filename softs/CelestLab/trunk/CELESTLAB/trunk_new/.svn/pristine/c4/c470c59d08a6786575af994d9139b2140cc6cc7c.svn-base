//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
// ========================
// Test CLx_stela_deriv
//
// Tests:
// derivative of state vector at cjd0 + delta_t(:,$)
//
// Nota: reference derivatives obtained using finite differences and Lagrange interpolation (see file "procedure.txt")
// ========================
tlbx_path = CLx_home();
exec(fullfile(tlbx_path, "tests", "unit_tests", "CLx_create_extrapolation_inputdata.sci"));
// Dates cjd (TREF) for propagation
delta_t = (0 : 10) * 86400;
// Exrapolation
[x, xosc, dxd0, info] = CLx_stela_extrap(cjd0, x0, delta_t, params, res=0, ut1_tref, tt_tref);
// Expected value at delta_t = 10 (days)
dxdt11_expected = [
   -4.00002062406201
   -0.00041267609054
    0.00522525666402  
   -0.00002912119132  
   -0.00064422833757  
    16.2971912772838]*1e-5;   
// Compute derivatives using CLx  
[dxdt, info] = CLx_stela_deriv(cjd0 + delta_t/86400, x, params, res=0, ut1_tref, tt_tref);
// Old second argument [6,4]: test failed (=> changed to [6,1])
assert_checkequal(size(dxdt), [6, 11]); 
epsilon = 1D-6;
assert_checkalmostequal(dxdt(:,11), dxdt11_expected, epsilon, epsilon, "element");
