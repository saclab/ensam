//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [desc] = CLx__configGetDesc()
// Returns a structure with configuration information
//
// Calling Sequence
// [desc] = CLx__configGetDesc()
//
// Description
// <itemizedlist>
// <listitem>
// <p>The structure fields are: </p>
// <p>- names = vector of config options</p>
// <p>- types = vector of types (string) : "integer", "boolean", "double", string" 
//(always only one value) </p>
// <p>- accval = list of vectors of possible values. Empty => no check of value</p>
// <p>- accemptyval = vector of booleans: %t if [] is an accepted value </p>
// <p>- settable = vector of indicators: changeable or not by CL_configSet</p>
// <p>- writechange = vector of indicators: update config file if value has changed</p>
// </listitem>
// </itemizedlist>
//
// Authors
// CNES - DCT/SB
//

// Declarations:

// Code:

desc = struct(); 

// names of possible options
desc.names = ["LOG_VERBOSITY", "STELA_LOG", "LOG_FILE", "STELA_ROOT", ..
              "STELA_VERSION_NCHECK", "CL_VERSION", "CL_VERSION_NCHECK"]; 

// types
desc.types = ["string", "boolean", "string", "string", "integer", "string", "integer"]; 

// names of possible values for each option
desc.accval = list(["ALL", "DEBUG", "ERROR", "FATAL", "INFO", "TRACE", "WARN", "OFF"], ..
              [%t, %f], [], [], ..
              [0,1,2,3], [], [0,1,2,3]); 

// %t if [] is an accepted value (meaning varies depending on option)
desc.accemptyval = [%f, %f, %f, %t, %f, %f, %f];  

// changeable by configSet ? 
desc.settable = [%t, %t, %t, %t, %f, %f, %f]; 

// Write current value in config file ? 
desc.writechange = [%t, %t, %f, %t, %f, %f, %f]; 

 
endfunction
