//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Sun beta angle: 
//> It is the angle between the Sun direction and its projection onto the 
//> orbit plane (by convention: positive if the angle between the Sun direction 
//> and the orbit angular momentum is less than 90 degrees). 
//> Notes: 
//> - Colors show the absolute values of the beta angle (|beta angle|), 
//> - Coutour levels show the signed values.  
//
// Author: A. Lamy
// -----------------------------------------------------------

// --------------------------------------------------
// Inputs
// --------------------------------------------------

inc = 70 * %CL_deg2rad; // inclination

desc_param = list(..
   CL_defParam("Inclinaison", inc, units=['rad', 'deg'], valid='$x>=0 & $x<=180')..
);

[inc] = CL_inputParam(desc_param);


// --------------------------------------------------
// Computation
// --------------------------------------------------

nbpts = 100;

hlocv = linspace(0,24,nbpts);
decl = linspace(-23.5,23.5,nbpts) * %CL_deg2rad;
[Hlocv, Decl] = ndgrid(hlocv, decl); 

betaa = CL_gm_raan2beta(inc, 0, -(matrix(Hlocv,1,-1)-12)*%pi/12, matrix(Decl,1,-1));

// result : matrix, unit=deg
result = matrix(betaa, size(Hlocv)) * %CL_rad2deg;


// --------------------------------------------------
// Plot
// --------------------------------------------------

f=scf();
f.visible="on";
f.immediate_drawing="off";

Nmap = 64;
f.color_map = 0.4+0.6*jetcolormap(Nmap);

Noir = addcolor([1,1,1]*0);
Gris = addcolor([1,1,1]*0.3);
GrisF = addcolor([1,1,1]*0.2);

a=gca();

zmin=min(abs(result)); // grayplot min/max
zmax=max(abs(result));

colorbar(zmin,zmax,colminmax=[1,Nmap],fmt="%.1f");
Sgrayplot(hlocv, decl*%CL_rad2deg, abs(result), colminmax=[1,Nmap],zminmax=[zmin,zmax]);

[levels] = CL_autoLevels(min(result), max(result));

contour2d(hlocv, decl*%CL_rad2deg, result, levels, style=Gris*ones(levels));

x_ticks = 0:2:24;
a.x_ticks = tlist(['ticks','locations','labels'], x_ticks, string(x_ticks));

// general setting
CL_g_stdaxes(a)

a.data_bounds = [0,min(decl*%CL_rad2deg);24,max(decl*%CL_rad2deg)];
a.tight_limits="on";
a.title.text = "Sun beta angle (deg) - Inc = " + msprintf("%.5g", inc * %CL_rad2deg) + " deg";
a.x_label.text = "True local time of the ascending node (h)";
a.y_label.text = "Sun declination (deg)";

// adjustments
h = CL_g_select(a, "Text");
h.font_style=8;
h.font_size=2;
h.font_foreground=GrisF;

f.immediate_drawing="on";
f.visible = "on";

