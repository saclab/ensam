//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// ------------------------------
// CL_tle_genEphem
// Check consistency when choosing various frames
// ------------------------------

TEST_OK = [];


// Sample tle
str = [..
"1 00005U 58002B   00179.78495062  .00000023  00000-0  28098-4 0  4753"; ..
"2 00005  34.2682 348.7242 1859667 331.7664  19.3264 10.82419157413667" ];

tle = CL_tle_parse(str);

// Propagation dates
cjd = round(tle.epoch_cjd) : round(tle.epoch_cjd) + 5; // TREF

// Propagates, results in ECI
[pos_eci, vel_eci] = CL_tle_genEphem(tle, cjd, frame = "ECI"); // ECI
kep1 = CL_oe_car2kep(pos_eci, vel_eci);

// Propagates, results in TOD => converts to ECI
[pos_tod, vel_tod] = CL_tle_genEphem(tle, cjd, frame = "TOD"); // TOD
[pos_eci2, vel_eci2] = CL_fr_convert("TOD", "ECI", cjd, pos_tod, vel_tod);
kep2 = CL_oe_car2kep(pos_eci2, vel_eci2);

TEST_OK($+1)= CL__isEqual(kep1, kep2, prec = 1.e-12);