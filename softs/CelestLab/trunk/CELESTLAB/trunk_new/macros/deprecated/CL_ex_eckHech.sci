//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_cir_t2,osc_cir_t2] = CL_ex_eckHech(t1,mean_cir_t1,t2, er,mu,j1jn)
// Eckstein-Hechler propagation model - DEPRECATED
//
// Calling Sequence
// [mean_cir_t2,osc_cir_t2] = CL_ex_eckHech(t1,mean_cir_t1,t2 [,er,mu,j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_ex_propagate">CL_ex_propagate</link></p>
// <p></p>
// </listitem>
// <listitem>
// <p>Propagates orbital elements using Eckstein-Hechler analytical model. </p>
// <p></p>
// <p>The original algorithm has been modified so that the perigee of a frozen orbit remains perfectly constant over time. 
// This resulted in a change of the mean eccentricity vector (x component) of about 4.e-7, which is small compared to the 
// model's accuracy. </p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p>- There can be 1 or N initial times, and 1 or N final times. </p>
// <p> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn". </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Propagation models">Propagation models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// t1: Initial time [days] (1x1 or 1xN)
// mean_cir_t1: Mean orbital elements at time t1 (6x1 or 6xN). 
// t2: Final time [days] (1xN or 1x1). 
// er: (optional) Equatorial radius [m]. Default is %CL_eqRad
// mu: (optional) Gravitational constant [m^3/s^2]. Default is %CL_mu
// j1jn: (optional) Vector of zonal harmonics. Default is %CL_j1jn (Nz x 1)
// mean_cir_t2: Mean orbital elements at t2  (6xN)
// osc_cir_t2: Osculating orbital elements at t2 (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_meanEckHech
//
// Examples
// // Example 1: one orbit, several final time instants:
// t1 = 12584;
// mean_cir_t1 = [7.e6; 0; 1.e-3; 1; 0.1; 0.2]; 
// t2 = 12587:1:12590
// [mean_cir_t2,osc_cir_t2] = CL_ex_eckHech(t1,mean_cir_t1,t2)
//
// // Example 2: one orbit, several final time instants:
// t1 = 12584;
// mean_cir_t1 = [7.e6; 0; 1.e-3; 0.1; 0.2; 0.3] * [1,1]; 
// t2 = [12587, 12588];
// [mean_cir_t2,osc_cir_t2] = CL_ex_eckHech(t1,mean_cir_t1,t2)


// Declarations:

// Code:
CL__warnDeprecated(); // deprecated function

if (~exists("mu", "local")); mu = CL__dataGetEnv("mu"); end
if (~exists("er", "local")); er = CL__dataGetEnv("eqRad"); end
if (~exists("j1jn", "local")); j1jn = CL__dataGetEnv("j1jn"); end

compute_osc = %t; 
if (argn(1) == 1); compute_osc = %f; end

[mean_cir_t2,osc_cir_t2] = CL__ex_propag_eckhech(t1,mean_cir_t1,t2,er,mu,j1jn,compute_osc); 

endfunction
