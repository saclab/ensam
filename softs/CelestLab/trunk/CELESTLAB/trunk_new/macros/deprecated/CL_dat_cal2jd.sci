//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [jd] = CL_dat_cal2jd(varargin)
// Calendar date to Julian day - DEPRECATED
//
// Calling Sequence
// jd = CL_dat_cal2jd(year,month,day [,hour,minute,second])
// jd = CL_dat_cal2jd(cal) // cal=[year;month;day [;hour;minute;second]]
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_dat_convert">CL_dat_convert</link></p>
// <p></p></listitem>
// <listitem>
// <p>Converts a calendar date into a Julian day.</p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>- Years are counted astronomically. The year 1-BC is 0, 2-BC is -1, ... </p>
// <p>- The dates are relative to the Julian calendar before 4th october 1582 24h (Julian calendar), and to the Gregorian calendar after 5th October 1582 0h (Julian calendar), that is, after 15th october 1582 0h (Gregorian calendar). </p>
// <p>- Julian day 0 is 1st January -4712 12h (Julian calendar) </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Dates and time scales">Dates and time scales</link> for more details.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// year: (integer) Year. (1xN)
// month: (integer in [1,12]) Month. (1xN). 
// day: (integer in [1,31]) Day. (1xN) 
// hour: (optional) Hours. Default is 0. (1xN or 1x1)
// minute: (optional) Minutes. Default is 0. (1xN or 1x1)
// second: (optional) Seconds. Default is 0. (1xN or 1x1)
// jd: Julian day (number of days since 1st January -4712, 12h) (1xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Jean Meeus - Astronomical Algorithms - 1991
// 2) IMCCE: <ulink url="http://www.imcce.fr">http://www.imcce.fr</ulink>
//
// See also
// CL_dat_jd2cal
// CL_dat_cal2cjd
//
// Examples
// // Example 1
// jd = CL_dat_cal2jd(2000,1,1)
// jd = CL_dat_cal2jd(2010,2,20,12,3,45)
// jd = CL_dat_cal2jd([2000;1;1])
// jd = CL_dat_cal2jd([2010;2;20;12;3;45])
//
// // Example 2
// year=[2000,2010]
// month=[1,2]
// day=[1,20]
// hour=[0,12]
// minute=[0,3]
// second=[0,45]
// jd = CL_dat_cal2jd(year,month,day,hour,minute,second)
// jd = CL_dat_cal2jd([year;month;day;hour;minute;second])

// Declarations:


// Code:

CL__warnDeprecated(); // deprecated function

[lhs, rhs] = argn();

if ~(rhs == 1 | (rhs >= 3 & rhs <= 6))
  CL__error("Wrong number of input arguments");
end
if ~(lhs == 1)
  CL__error("Wrong number of output arguments");
end

hour = 0; 
minute = 0; 
second = 0; 

if (rhs == 1) 
  Nin = size(varargin(1),1); 
  if (Nin < 3 | Nin > 6) 
     CL__error("Wrong size of input argument");
  end
  year = varargin(1)(1,:);
  month = varargin(1)(2,:);
  day = varargin(1)(3,:);
  if (Nin >= 4); hour = varargin(1)(4,:); end
  if (Nin >= 5); minute = varargin(1)(5,:); end
  if (Nin >= 6); second = varargin(1)(6,:); end
else
  year = varargin(1);
  month = varargin(2);
  day = varargin(3);
  if (rhs >= 4); hour = varargin(4); end
  if (rhs >= 5); minute = varargin(5); end
  if (rhs >= 6); second = varargin(6); end
end


// check inputs
// no test on day as no impact on algorithm 
Ny = size(year,2); 
Nm = size(month,2); 
Nd = size(day,2); 
N = max(Ny, Nm, Nd);

if min(Ny, Nm, Nd) <> max(Ny, Nm, Nd) 
  CL__error("Wrong sizes of inputs (year, month or day)");
end
/// should check hour, minute... as well...

I = find(month < 1 | month > 12 | year < -4712 | ...
    month<>round(month) | year<>round(year) | day<>round(day));
if ~isempty(I)
  CL__error("Invalid inputs");
end


// algorithm (see Meeus)
// Note on int: int(1.5)=1, int(-1.5)=-1

k = find(month <= 2); 
year(k) = year(k) - 1; 
month(k) = month(k)+12; 
 
A = int(year / 100.0); 
B = 2 - A + int(A / 4); 

// julian day (julian calendar)
jd = int(365.25 * (year + 4716)) + int(30.6001 * (month + 1)) + day - 1524.5; 
frac = (hour*3600 + minute*60 + second)/86400;   

k = find(jd+frac >= 2299160.5); // 2299160.5 = 5 oct 0h julian calendar
jd(k) = jd(k) + B(k); // gregorian calendar

jd = jd + frac;


endfunction

