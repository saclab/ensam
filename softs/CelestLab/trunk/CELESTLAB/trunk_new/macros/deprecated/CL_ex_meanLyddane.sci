//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_kep] = CL_ex_meanLyddane(osc_kep, er,mu,j1jn)
// Lyddane propagation model - mean elements (mean = secular) - DEPRECATED
//
// Calling Sequence
// mean_kep = CL_ex_meanLyddane(osc_kep [,er,mu,j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_ex_osc2mean">CL_ex_osc2mean</link></p>
// <p></p>
// </listitem>
// <listitem>
// <p>Computes the mean orbital elements from the osculating orbital elements, using Lyddane model. </p>
// <p>The mean elements for this model include secular effects only. </p>
// <p></p></listitem>
// <listitem> 
// <p>Notes:</p>
// <p> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn". </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Propagation models">Propagation models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// osc_kep: Osculating orbital elements (6xN)
// er: (optional) Equatorial radius [m]. Default is %CL_eqRad
// mu: (optional) Gravitational constant [m^3/s^2]. Default is %CL_mu
// j1jn: (optional) Vector of zonal harmonics. Default is %CL_j1jn (Nz x 1)
// mean_kep: Mean orbital elements (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_lyddane
//
// Examples
// mean_kep = [7.e6; 1.e-4; 1; %pi/2; 0.2; 0.3] 
// [mean_kep2,osc_kep] = CL_ex_lyddane(0,mean_kep,0); 
// CL_ex_meanLyddane(osc_kep) // => mean_kep
//

// Declarations:

// Code:
CL__warnDeprecated(); // deprecated function

if (~exists("er", "local")); er = CL__dataGetEnv("eqRad"); end
if (~exists("mu", "local")); mu = CL__dataGetEnv("mu"); end
if (~exists("j1jn", "local")); j1jn = CL__dataGetEnv("j1jn"); end

mean_kep = CL__ex_convMean_lydsec(osc_kep, er, mu, j1jn); 

endfunction
