//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
 
function [psoE] = CL_kp_M2Ecir(ex,ey,psoM)
// Kepler's equation for circular or equinoctial orbital elements - DEPRECATED
//
// Calling Sequence
// psoE = CL_kp_M2Ecir(ex,ey,psoM)
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_kp_anomConvertCir">CL_kp_anomConvertCir</link></p>
// <p></p></listitem>
// <listitem>
// <p>Solves the 'generalized' Kepler's equation for elliptical orbits. </p>
// <p></p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ex: x component of eccentricity vector (PxN or Px1)
// ey: y component of eccentricity vector (PxN or Px1)
// psoM: Mean argument of latitude (=w+M) or mean longitude (=w+raan+M) [rad] (PxN or Px1)
// psoE: Eccentric argument of latitude (=w+E) or eccentric longitude (=w+raan+E) ([rad] (PxN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Procedures for solving Kepler's Equation - A.W. Odell and R.H Gooding - 1986
//
// See also
// CL_kp_M2E
//
// Examples
// pom = 2*%pi/3;
// ecc = 0.001;
// ex = ecc * cos(pom);
// ey = ecc * sin(pom);
// M = (0:4)*%pi/4;
// E = CL_kp_M2Ecir(ex,ey,pom+M) - pom; 
//
// // Now using CL_kp_M2E :
// E = CL_kp_M2E(ecc,M) 

// Declarations:

// Code:

ecc = sqrt(ex.^2+ey.^2);
pom = atan(ey,ex);

if (find(ecc >= 1) <> []) 
  CL__error("Invalid eccentricity (parabolic or hyperbolic orbit)");
end

psoE = CL_kp_M2E(ecc,psoM-pom) + pom;

endfunction

