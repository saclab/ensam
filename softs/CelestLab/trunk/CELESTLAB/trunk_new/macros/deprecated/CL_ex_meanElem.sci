//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_oe] = CL_ex_meanElem(mod, type_oe, osc_oe, er, mu, j1jn)
// Mean orbital elements (all analytical models) - DEPRECATED
//
// Calling Sequence
// mean_oe = CL_ex_meanElem(mod, type_oe, osc_oe [, er, mu, j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_ex_osc2mean">CL_ex_osc2mean</link></p>
// <p></p>
// </listitem>
// <listitem>
// <p>Computes mean orbital elements from osculating orbital elements.</p>
// <p></p>
// <p>This function is a wrapper for all available analytical models. </p>
// <p></p>
// <p>The available models are: </p>
// <p>"central": Central force (osculating elements = mean elements) </p>
// <p>"j2sec": Secular effects of J2 (osculating elements = mean elements by convention)</p>
// <p>"lydsec": Lyddane (mean elements include secular effects only) </p>
// <p>"lydlp": Lyddane (mean elements include secular and long period effects) </p>
// <p>"eckhech": Eckstein-Hechler  (mean elements include secular and long period effects) </p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p> - Conversions take place if the type of orbital elements is not the "natural" type for the model. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Propagation models">Propagation models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// mod: (string) Model name: "central", "j2sec", "lydsec", "lydlp", "eckhech". (1x1)
// type_oe: (string) Type of orbital elements used for input/output: "kep", "cir", "cireq" or "equin" (1x1)
// osc_oe: Osculating orbital elements (6xN)
// er: (optional) Equatorial radius [m]. Default is %CL_eqRad
// mu: (optional) Gravitational constant [m^3/s^2]. Default is %CL_mu
// j1jn: (optional) Vector of zonal harmonics. Default is %CL_j1jn (Nz x 1)
// mean_oe: Mean orbital elements (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_propagate
//
// Examples
// mean_kep = [7.e6; 1.e-3; 1; %pi/2; 0.1; 0.2] 
// osc_kep = CL_ex_propagate("eckhech","kep",0,mean_kep,0,res="o"); 
// CL_ex_meanElem("eckhech","kep",osc_kep) // => mean_kep

// Declarations:

// Code:
CL__warnDeprecated(); // deprecated function

if (~exists("er", "local")); er = CL__dataGetEnv("eqRad"); end
if (~exists("mu", "local")); mu = CL__dataGetEnv("mu"); end
if (~exists("j1jn", "local")); j1jn = CL__dataGetEnv("j1jn"); end

mean_oe = CL_ex_osc2mean(mod, type_oe, osc_oe, er, mu, j1jn); 

endfunction
