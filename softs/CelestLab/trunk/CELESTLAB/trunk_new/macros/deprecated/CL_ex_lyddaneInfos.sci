//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [infos] = CL_ex_lyddaneInfos(mean_kep, er,mu,j1jn)
// Informations about Lyddane orbit propagation analytical model - DEPRECATED
//
// Calling Sequence
// infos = CL_ex_lyddaneInfos(mean_kep [,er,mu,j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>This function is deprecated. </p>
// <p>Replacement function: <link linkend="CL_ex_getInfo">CL_ex_getInfo</link></p>
// <p></p>
// </listitem>
// <listitem>
// <p>Computes data related to Lyddane orbit propagation analytical model.</p>
// <p> These data are: </p>
// <p> - Eccentricity vector of frozen orbit </p>
// <p> - Secular drifts of some angular orbital elements </p>
// <p></p>
// <p>The output is a structure with the following fields:</p>
// <p> - infos.eccf: Mean eccentricity of frozen orbit</p>
// <p> - infos.pomf: Mean argument of periapsis of frozen orbit</p>
// <p> - infos.dgomdt: Secular drift of mean right ascension of ascending node [rad/s]</p>
// <p> - infos.dpsodt: Secular drift of mean argument of latitude [rad/s]</p>
// <p></p></listitem>
// <listitem> 
// <p><b>Note:</b></p>  
// <p>The outputs only depend on semi major-axis, eccentricity and inclination. </p>
// <p></p></listitem>
// <listitem> 
// <p>The orbital elements used are the following:</p>  
// <p><inlinemediaobject><imageobject><imagedata fileref="kep_par.gif"/></imageobject></inlinemediaobject></p>
// <p></p></listitem>
// <listitem> 
// <p>See <link linkend="CL_ex_lyddane">CL_ex_lyddane</link> for more details on the propagation model.</p>
// <p></p></listitem>
// <listitem>
// <p>Warning :</p>
// <p> - The input argument "zonals" is deprecated as of CelestLab v3.0.0. It has been replaced by "j1jn".</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// mean_kep: Mean orbital elements (6xN). 
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// j1jn: (optional) Vector of zonal coefficients J1 to Jn (troncated to J6) to be used (default is %CL_j1jn(1:6)) (1xNz)
// infos: (structure) Data for the given orbital elements (containing (1xN) vectors)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_lyddane
// CL_ex_meanLyddane
//
// Examples
// // Example :
// mean_kep = [7.e6; 1.e-2; 1.7; 0; 0; 0 ]; // sma, ecc, inc... 
// infos = CL_ex_lyddaneInfos(mean_kep);


// Declarations:

// Code:
CL__warnDeprecated(); // deprecated function

if (~exists("er", "local")); er = CL__dataGetEnv("eqRad"); end
if (~exists("mu", "local")); mu = CL__dataGetEnv("mu"); end
if (~exists("j1jn", "local")); j1jn = CL__dataGetEnv("j1jn", 1:5); end

infos = CL__ex_getInfo_lydsec(mean_kep, er, mu, j1jn); 

endfunction



