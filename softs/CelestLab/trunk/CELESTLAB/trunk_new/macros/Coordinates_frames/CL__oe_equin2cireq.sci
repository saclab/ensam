//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cireq,jacob] = CL__oe_equin2cireq(equin)
// Equinoctial to circular equatorial orbital elements  
//
// Calling Sequence
// [cireq, jacob] = CL__oe_equin2cireq(equin)
//
// Description
// <itemizedlist><listitem>
// <p>Converts equinoctial orbital elements to circular equatorial orbital elements.</p>
// <p>The transformation jacobian is optionally computed.</p>
// <p>See <link linkend="Orbital elements">Orbital elements</link> for more details</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// equin: Equinoctial orbital elements [sma;ex;ey;hx;hy;L] [m,rad] (6xN)
// cireq: Circular equatorial orbital elements [sma;ex;ey;ix;iy;L] [m,rad] (6xN)
// jacob: (optional) Transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Example 1
// equin = [7000.e3; 0.1; 0.2; 0.3; 0.4; 1];
// cireq = CL__oe_equin2cireq(equin);
//
// // Example 2
// equin = [7000.e3; 0.1; 0.2; 0.3; 0.4; 1];
// [cireq, jacob1] = CL__oe_equin2cireq(equin);
// [equin2, jacob2] = CL__oe_cireq2equin(cireq);
// equin2 - equin // => zero
// jacob2 * jacob1 // => identity


// Declarations:

// Code:

// Handle [] cases
if (equin == [])
  cireq = [];
  jacob = [];
  return;
end

// Check validity of input
[isvalid,type_orbit] = CL__oe_isValid("equin",equin);
if (~isvalid); CL__error("Invalid orbital elements"); end;
if (find(type_orbit <> 1) <> []); CL__error("Invalid orbital elements (parabolic or hyperbolic orbit)"); end;


// Conversion formulas: (equin = [a; ex; ey; ix; iy; L]
// ix_cireq = sin(inc/2) * cos(gom) = ix / sqrt(1 + ix^2 + iy^2)
// iy_cireq = sin(inc/2) * sin(gom) = iy / sqrt(1 + ix^2 + iy^2)
// (other elements: identical)

cireq = equin;
D = sqrt(1 + equin(4,:).^2 + equin(5,:).^2); 

cireq(4,:) = equin(4,:) ./ D; 
cireq(5,:) = equin(5,:) ./ D; 


// Jacobian computation (dcireq/dequin)
jacob = []; 

if (argn(1) == 2)
  // jacob(i,j) = d(cireq_i)/d(equin_j)
  //
  // Formulas used: equin = [a; ex; ey; ix; iy; L]
  // da_cireq/da = 1
  // dex_cireq/dex = 1
  // dey_cireq/dey = 1
  // dix_cireq/dix = (1 + iy^2) / (1 + ix^2 + iy^2)^(3/2)
  // dix_cireq/diy = -ix*iy / sqrt(1 + ix^2 + iy^2)^(3/2)
  // diy_cireq/dix = -ix*iy / sqrt(1 + ix^2 + iy^2)^(3/2)
  // diy_cireq/diy = (1 + ix^2) / (1 + ix^2 + iy^2)^(3/2)
  // dL_cireq/dL = 1
  
  N = size(equin,2);
  jacob = zeros(6,6,N);

  jacob(1,1,:) = 1;  // da_equin/da
  jacob(2,2,:) = 1;  // dex_equin/dex
  jacob(3,3,:) = 1;  // dey_equin/dey
  
  jacob(4,4,:) = (1 + equin(5,:).^2) ./ D.^3;  // dix_equin/dix
  jacob(4,5,:) = -(equin(4,:) .* equin(5,:)) ./ D.^3;  // dix_equin/diy
     
  jacob(5,4,:) = -(equin(4,:) .* equin(5,:)) ./ D.^3;  // diy_equin/dix
  jacob(5,5,:) = (1 + equin(4,:).^2) ./ D.^3;  // diy_equin/diy

  jacob(6,6,:) = 1;  // dpso/dL

end

endfunction
