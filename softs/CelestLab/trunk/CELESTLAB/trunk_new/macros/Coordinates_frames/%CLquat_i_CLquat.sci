//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q2(I) = q1 
// I = integers (row vector) or ":" or 1:$ or $ or etc...
// NB: Either I and q1 have the same size or size(q1) == 1
// No effect if I contains no values whatever the size of q1

function [q2] = %CLquat_i_CLquat(I,q1,q2)

N1 = size(q1);
N2 = size(q2);

if (typeof(I) == "implicitlist" | typeof(I) == "size implicit" | typeof(I) == "polynomial" | isequal(I,:))
  N = size(q2(I));

else
  // I: set of values (1xN expected or [])
  if (I <> [] & (size(I,1) > 1 | min(I) <= 0.5 | max(I) >= N2+0.5))
    CL__error("Invalid index"); 
  end
  N = size(I,2); 
  
end

if (N == 0 | N2 == 0)
  return; 
end

if (N1 <> 1 & N1 <> N)
  CL__error("Inconsistent sizes"); 
end

r = q2.r;
i = q2.i;

r1 = q1.r; 
i1 = q1.i; 

if (N1 < N)
  r1 = r1 * ones(1,N);
  i1 = i1 * ones(1,N);
end

r(:,I) = r1;
i(:,I) = i1;

q2 = CL__defQuat(r, i);

endfunction
