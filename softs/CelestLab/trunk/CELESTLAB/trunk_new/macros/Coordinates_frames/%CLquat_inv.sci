//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q2 = inv(q) 
// such that : q * q2 = [1;0;0;0]

function [q2] = %CLquat_inv(q)

q2 = conj(q) ./ (norm(q).^2); 

endfunction
