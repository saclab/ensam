//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q2 = q1 * a 
// a: scalar or (a:row vector and size(q1) = 1)

function [q2] = %CLquat_m_s(q1,a)

N1 = size(q1);
sa = size(a); 

if (sa(1) > 1 | (N1 > 1 & sa(2) > 1))
  CL__error("Invalid arguments sizes"); 
end

q2 = CL__defQuat(q1.r * a, q1.i * a);

endfunction
