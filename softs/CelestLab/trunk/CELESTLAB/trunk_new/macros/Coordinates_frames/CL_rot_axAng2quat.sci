//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [q] = CL_rot_axAng2quat(u,alpha)
// Rotation axis and angle to quaternion
//
// Calling Sequence
// q = CL_rot_axAng2quat(u,alpha)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the quaternion <b>q</b> that corresponds to the 
// rotation of angle <b>alpha</b> around the  axis <b>u</b>.</p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> - The output quaternion has a norm equal to 1.  </p>
// <p> - For compatibility reasons, the argument order (alpha, u) is also 
// possible. A warning message is issued in this case.  </p>
// <p> - The vector u may not have a norm equal to 1. In the particular case where u is equal to [0;0;0], the output quaternion 
// q corresponds to Identity (by convention). </p>
// <p> - See <link linkend="Data types">Data types</link> for more details on quaternions. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// u : Rotation axis [ux; uy; uz] (3xN or 3x1)
// alpha : Rotation angle [rad] (1xN or 1x1)
// q : Corresponding quaternion (dim N)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_rot_quat2axAng
//
// Examples
// // 90 degree rotation around Z axis :
// q = CL_rot_axAng2quat([0;0;1], %pi/2)
//

// Declarations:


// Code:

// Code for compatibility (inversion of arguments): 
if (size(alpha,1) == 3 & size(u,1) == 1) 
  CL__warning("Wrong order of arguments in  CL_rot_axAng2quat"); 
  tmp = alpha; 
  alpha = u; 
  u = tmp;
end

// return [] if both arguments are empty
if (u == [] & alpha == [])
  q = CL__defQuat([], []); 
  return; // <= RETURN
end

// error if one argument is empty (and not both)
if ((u == [] & alpha <> []) | (u <> [] & alpha == []))
  CL__error("Invalid arguments (empty)");  
end

// Check arguments sizes and resize if necessary
[u, alpha, N] = CL__checkInputs(u,3, alpha,1);

[axis, u_norm] = CL_unitVector(u);  

q = CL__defQuat(cos(alpha/2), CL_dMult(sin(alpha/2), axis));


// Note: if ||u|| == 0  (axis == %nan) => Identity 
I = find(isnan(CL_dot(axis)));  
if (I <> [])
  q(I) = CL__defQuat(1, [0;0;0]);
end

// Make sure %nan in input are correctly propagated
I = find(isnan(CL_dot(u)) | isnan(alpha));  
if (I <> [])
  q(I) = CL__defQuat(%nan, %nan * [1;1;1]);
end


endfunction
