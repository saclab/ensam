//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M, omega] = CL__fr_cirs2tirs(jd, args, comega)
// CIRS to TIRS frame transformation matrix and angular velocity
//
// Calling Sequence
// [M, omega] = CL__fr_cirs2tirs(jd ,args [,comega])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>M</b> from CIRS to TIRS.
// By convention, multiplying <b>M</b> by coordinates relative to CIRS yields coordinates relative to TIRS.</p> 
// <p>Optionaly computes the angular velocity vector <b>omega</b> of TIRS relative to CIRS, with coordinates relative to CIRS.
// See <link linkend="Data types">Data types</link> for more details on the definition of angular velocity vectors and frame transformation matrix.</p>
// <p></p></listitem>
// <p>args is a structure containing the field "ut1_tt" which is the difference between
// UT1 time scale and TT time scale in seconds.</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// args: Structure of arguments. See description for more details.
// comega: (boolean, optional) Option to compute omega. If comega is %f, omega will be set to []. Default is %t. (1x1)
// M: transformation matrix (3x3xN)
// omega: (optional) Angular velocity vector [rad/s] (3xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010
//
// See also
// CL__fr_gcrs2cirs
//

// Declarations:


// Code:

if ~isstruct(args); CL__error("args should be a structure"); end;
if (~isfield(args,"ut1_tt")); CL__error("Field(s) missing in args"); end;

if (~exists("comega","local")); comega = %t; end;
if (argn(1) <= 1); comega = %f; end;

N = size(jd,2);

// Convert from TT to UT1
jd_ut1 = [jd(1,:) ; ...
          jd(2,:) + args.ut1_tt/86400];


[era, eradot] = CL__iers_era2000(jd_ut1, comega);

// eq 5.5, paragraph 5.4.2, p48
// (M = R')
M = CL_rot_angles2matrix(3, era); 

if (comega)
  omega = zeros(3,N);
  omega(3,:) = eradot;
end

endfunction
