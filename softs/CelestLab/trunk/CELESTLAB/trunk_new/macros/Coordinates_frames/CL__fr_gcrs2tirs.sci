//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M, omega] = CL__fr_gcrs2tirs(jd, args, comega)
// GCRS to TIRS frame transformation matrix and angular velocity
//
// Calling Sequence
// [M, omega] = CL__fr_gcrs2tirs(jd ,args [,comega])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>M</b> from GCRS to TIRS.
// By convention, multiplying <b>M</b> by coordinates relative to GCRS yields coordinates relative to TIRS.</p> 
// <p>Optionaly computes the angular velocity vector <b>omega</b> of TIRS relative to GCRS, with coordinates relative to GCRS.
// See <link linkend="Data types">Data types</link> for more details on the definition of angular velocity vectors and frame transformation matrix.</p>
// <p></p></listitem>
// <p>args is a structure containing the following fields:</p>
// <p>- "model": (string) Available values are :
// <p>-          - "06a" for IAU2006 precession and IAU2006a nutation.</p>
// <p>-          - "06_series" for series based on IAU2006 precession and IAU2006a nutation.</p>
// <p>-          - "00b" for IAU2006 precession and IAU2000b nutation</p>
// <p>-          - "interp_06a" for interpolation of IAU2006 precession and IAU2000b nutation 
//                  for dates inside [2000-2100]. Exact values are used for dates outside this range</p>
// <p>- "ut1_tt" which is the difference between UT1 time scale and TT time scale in seconds.</p>
// <p>- "dx06" : Correction to the X coordinate of CIP. (not needed if model=="00b") (radians)</p>
// <p>- "dy06" : Correction to the Y coordinate of CIP. (not needed if model=="00b") (radians)</p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Reference frames">Reference frames</link> for more details on the definition of reference frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// jd: Two-part julian day (Time scale: TT) (2xN)
// args: Structure of arguments. See description for more details.
// comega: (boolean, optional) Option to compute omega. If comega is %f, omega will be set to []. Default is %t. (1x1)
// M: transformation matrix (3x3xN)
// omega: (optional) Angular velocity vector [rad/s] (3xN)
//
// Authors
// CNES - DCT/SB
//
// Bibliography
// 1) Technical Note 36, IERS, 2010
//
// See also
// CL__fr_gcrs2cirs
// CL__fr_cirs2pef
//

// Declarations:


// Code:

if (~exists("comega","local")); comega = %t; end;
if (argn(1) <= 1); comega = %f; end;

if ~isstruct(args); CL__error("args should be a structure"); end;
// NB: Tests of validity of fields of 'args', are done by called functions.

// om1 = omega(CIRS/GCRS), coordinates in GCRS
[M1,om1] = CL__fr_gcrs2cirs(jd, args, comega);
// om2 = omega(TIRS/CIRS), coordinates in CIRS
[M2,om2] = CL__fr_cirs2tirs(jd, args, comega);

omega = [];
if (comega)
  // GCRS-->CIRS-->TIRS
  [M,omega] = CL_rot_compose(M1,om1,1, M2,om2,1);
else
  // GCRS-->CIRS-->TIRS
  M = M2 * M1;
end

endfunction
