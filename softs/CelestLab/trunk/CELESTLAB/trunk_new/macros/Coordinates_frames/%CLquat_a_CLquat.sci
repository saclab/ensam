//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// q = q1 + q2
// resize q1 or q2 if dimension == 1
// OK if q1 or q2 is empty

function [q] = %CLquat_a_CLquat(q1,q2)

N1 = size(q1); 
N2 = size(q2);
N = max(N1, N2);

if (N1 > 1 & N2 > 1 & N1 <> N2)
  CL__error("Invalid arguments sizes");
end

if (N1 < N)
  q1.r = q1.r * ones(1,N); 
  q1.i = q1.i * ones(1,N); 
end

if (N2 < N)
  q2.r = q2.r * ones(1,N); 
  q2.i = q2.i * ones(1,N); 
end

q = CL__defQuat(q1.r + q2.r, q1.i + q2.i);

endfunction
 
