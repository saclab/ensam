//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [cireq,jacob] = CL__oe_cir2cireq(cir)
// Circular to circular equatorial orbital elements
//
// Calling Sequence
// [cireq, jacob] = CL__oe_cir2cireq(cir)
//
// Description
// <itemizedlist><listitem>
// <p>Converts circular orbital elements to circular equatorial orbital elements.</p>
// <p>The transformation jacobian is optionally computed.</p>
// <p>See <link linkend="Orbital elements">Orbital elements</link> for more details</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cir: Orbital elements adapted to near-circular orbits [sma;ex;ey;inc;gom;pso] [m,rad] (6xN)
// cireq: Circular equatorial orbital elements [sma;ex;ey;ix;iy;L] [m,rad] (6xN)
// jacob: (optional) transformation jacobian (See <link linkend="Orbital elements">Orbital elements</link> for more details) (6x6xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Example 1
// cir = [7000.e3; 0.1; 0.2; 1; 2; 3];
// cireq = CL__oe_cir2cireq(cir);
//
// // Example 2
// cir = [7000.e3; 0.1; 0.2; 1; 2; 3];
// [cireq, jacob1] = CL__oe_cir2cireq(cir);
// [cir2, jacob2] = CL__oe_cireq2cir(cireq);
// cir2 - cir // => zero
// jacob2 * jacob1 // => identity


// Declarations:


// Code:

// Handle [] cases
if (cir == [])
  cireq = [];
  jacob = [];
  return;
end

// Check validity of input
[isvalid,type_orbit] = CL__oe_isValid("cir",cir);
if (~isvalid); CL__error("Invalid orbital elements"); end;
if (find(type_orbit <> 1) <> []); CL__error("Invalid orbital elements (parabolic or hyperbolic orbit)"); end;

cireq = zeros(cir)

// Conversion formulas: (cir = [a;ex;ey;inc;gom;pso]
// a_cireq = a
// ex_cireq = e*cos(pom+gom) = ex*cos(gom) - ey*sin(gom)
// ey_cireq = e*sin(pom+gom) = ex*sin(gom) + ey*cos(gom)
// ix = sin(inc/2)*cos(gom)
// iy = sin(inc/2)*sin(gom)
// L = gom + pso

cosgom = cos(cir(5,:)); 
singom = sin(cir(5,:));
cosinc2 = cos(cir(4,:)/2); // cos(inc/2)
sininc2 = sin(cir(4,:)/2); // sin(inc/2)

cireq(1,:) = cir(1,:);
cireq(2,:) = cir(2,:) .* cosgom - cir(3,:) .* singom;
cireq(3,:) = cir(2,:) .* singom + cir(3,:) .* cosgom;
cireq(4,:) = sininc2 .* cosgom;
cireq(5,:) = sininc2 .* singom;
cireq(6,:) = cir(5,:) + cir(6,:);

// Jacobian computation (dcireq/dcir)
if (argn(1) == 2)
  // jacob(i,j) = d(cireq_i)/d(cir_j)
  //
  // Formulas used:
  // da_cireq/da = 1
  // dex_cireq/dex = cos(gom)
  // dex_cireq/dey = -sin(gom)
  // dex_cireq/dgom = -ex*sin(gom)-ey*cos(gom) = -ey_cireq
  // dey_cireq/dex = sin(gom)
  // dey_cireq/dey = cos(gom)
  // dey_cireq/dgom = ex*cos(gom)-ey*sin(gom) = ex_cireq
  // dix/di = cos(i/2)*cos(gom)/2
  // dix/dgom = -iy
  // diy/di = cos(i/2)*sin(gom)/2
  // diy/dgom = ix
  // dL/dgom = 1
  // dL/dpso = 1

  N = size(cir,2);
  jacob = zeros(6,6,N);

  jacob(1,1,:) = 1; // da_cireq/da
  
  jacob(2,2,:) = cosgom; // dex_cireq/dex
  jacob(2,3,:) = -singom; // dex_cireq/dey
  jacob(2,5,:) = -cireq(3,:); // dex_cireq/dgom
  
  jacob(3,2,:) = singom; // dey_cireq/dex
  jacob(3,3,:) = cosgom; // dey_cireq/dey
  jacob(3,5,:) = cireq(2,:); // dey_cireq/dgom
  
  jacob(4,4,:) = cosinc2 .* cosgom/2; // dix/di
  jacob(4,5,:) = -cireq(5,:); // dix/dgom
  
  jacob(5,4,:) = cosinc2 .* singom/2; // diy/di
  jacob(5,5,:) = cireq(4,:); // diy/dgom
  
  jacob(6,5,:) = 1; // dL/dgom
  jacob(6,6,:) = 1; // dL/dpso

end

endfunction
