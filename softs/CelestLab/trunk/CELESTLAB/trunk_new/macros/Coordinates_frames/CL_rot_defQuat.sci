//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


function [q] = CL_rot_defQuat(varargin)
// Quaternion definition from real and imaginary components
//
// Calling Sequence
// q = CL_rot_defQuat(r,i1,i2,i3)
// q = CL_rot_defQuat(r,i)
// q = CL_rot_defQuat(ri)
// q = CL_rot_defQuat(ri,order)
//
// Description
// <itemizedlist><listitem>
// <p>Returns a quaternion defined by its real and imaginary components. </p>
// <p>The quaternion components may be defined in several ways: </p>
// <p> - real part (r) and imaginary part (i1,i2,i3) = 4 arguments.  </p>
// <p> - real part (r) and imaginary part (i) = 2 arguments.  </p>
// <p> - real and imaginary part (ri) = 1 argument.  </p>
// <p> - real and imaginary part (ri) + order = 2 arguments. The order helps determine the 
// position of the real part in the 4-component vector. </p>
// <p> => order = "ri": the input vector corresponds to [r,i1,i2,i3] (real part first). </p>
// <p> => order = "ir": the input vector corresponds to [i1,i2,i3,r] (real part last). </p>
// </listitem>
// <listitem>
// <p><b>Notes</b>:</p>
// <p> - The quaternion as defined is NOT normalized. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// r, i1, i2, i3 : Real and Imaginary components (1xN)
// r, i : Real part (1xN) and Imaginary part (3xN)
// ri : Real and imaginary part (4xN)
// ri, order : Real and imaginary part (4xN) and order ('ri' or 'ir' : see above).
// q : Quaternion (dim N)
//
// Authors
// CNES - DCT/SB
//
// Examples
// // Example 1: All the following calls return the same quaternion: 
// q = CL_rot_defQuat(1, 2, 3, 4) 
// q = CL_rot_defQuat(1, [2; 3; 4]) 
// q = CL_rot_defQuat([1; 2; 3; 4]) 
// q = CL_rot_defQuat([1; 2; 3; 4], "ri" ) // (real part first)
// q = CL_rot_defQuat([2; 3; 4; 1], "ir" ) // (real part last)
//
// // Example 2: 1000 random quaternions 
// q = CL_rot_defQuat(rand(4,1000));


// Declarations:


// Code:

rhs = argn(2);

select rhs

  case 4
    // ----------------------------------------------
    // 4 arguments : r,i1, i2, i3
    // ----------------------------------------------
    r = varargin(1);
    i1 = varargin(2);
    i2 = varargin(3);
    i3 = varargin(4);

    // check dimensions of imaginary part only
    // CL__defQuat will check that real and imaginary parts are of valid sizes
    si1 = size(i1);
    si2 = size(i2);
    si3 = size(i3);
 
    if ( si1(1) <> si2(1) | si1(2) <> si2(2) | ..
         si1(1) <> si3(1) | si1(2) <> si3(2))
       CL__error("Invalid argument sizes"); 
    end

    q = CL__defQuat(r,[i1;i2;i3]);


  case 2
    if (typeof(varargin(2)) == 'string') 
      // ----------------------------------------------
      // 2 arguments : ri, order
      // ----------------------------------------------
      ri = varargin(1);
      order = varargin(2);

      sri = size(ri);

      if ( sri(1) <> 4 & sri(1) <> 0 ) 
        CL__error("Invalid argument sizes"); 
      end

      if (order == "ir") then
        q = CL__defQuat(ri(4,:), ri(1:3,:));

      elseif (order == "ri")
        q = CL__defQuat(ri(1,:), ri(2:4,:));

      else
        CL__error("Invalid value: ir or ri expected"); 
      end


    else
      // ----------------------------------------------
      // 2 arguments : r, i
      // ----------------------------------------------
      r = varargin(1);
      i = varargin(2);

      q = CL__defQuat(r, i);

    end
    
  case 1
    // ----------------------------------------------
    // 1 argument : ri 
    // ----------------------------------------------
    ri = varargin(1);

    sri = size(ri);
    if ( sri(1) <> 4 & sri(1) <> 0)
      CL__error("Invalid argument sizes"); 
    end
   
    q = CL__defQuat(ri(1,:),ri(2:4,:));

  else
    CL__error("Invalid number of input arguments"); 

end

endfunction
