//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [M] = CL_fr_lvlhMat(pos_car,vel_car)
// Inertial frame to LVLH local orbital frame transformation matrix
//
// Calling Sequence
// M = CL_fr_lvlhMat(pos_car,vel_car)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the frame transformation matrix <b>M</b> from the inertial reference frame to 
// the LVLH local orbital frame.</p>
// <p> The inertial frame is implicitly the frame relative to which the satellite's position and velocity are defined. The "qsw" local frame is built using these position and velocity vectors. </p>
// <p> By convention, multiplying <b>M</b> by coordinates relative to the inertial frame yields coordinates relative to the LVLH frame. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Local frames">Local frames</link> for  more details on the definition of local frames.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// pos_car: satellite's position vector relative to the inertial frame [m] (3xN)
// vel_car: satellite's velocity vector relative to the inertial frame [m/s] (3xN)
// M : inertial frame to LVLH local frame transformation matrix (3x3xN)
//
// Bibliography
// 1) Mecanique spatiale, CNES - Cepadues 1995, Tome II, section 16.3.2.1
// 2) Orbital Mechanics for engineering students, H D Curtis, Chapter 7
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_fr_qswMat
// CL_fr_tnwMat
//
// Examples
// // Inertial to LVLH :
// pos_car = [[3500.e3;2500.e3;5800.e3] , [4500.e3;2100.e3;6800.e3]];
// vel_car = [[1.e3;3.e3;7.e3] , [2.e3;3.e3;6.e3]];
// [M] = CL_fr_lvlhMat(pos_car,vel_car)
// pos_lvlh = M*pos_car;
//
// // LVLH to inertial :
// pos_car = M'*pos_lvlh;

// Declarations:


// Code:

[lhs,rhs] = argn()

if rhs~=2 then CL__error('check number of input arguments'); end

N = size(pos_car,2);


z = -CL_unitVector(pos_car);
y = -CL_unitVector(CL_cross(pos_car,vel_car));
x = CL_cross(y,z);


// Matrix M :
M = hypermat([3 3 N] , [x(1,:); y(1,:); z(1,:); ...
            x(2,:); y(2,:); z(2,:); ...
            x(3,:); y(3,:); z(3,:)]);
if(N == 1)
    M = M(:,:,1);
end


endfunction
