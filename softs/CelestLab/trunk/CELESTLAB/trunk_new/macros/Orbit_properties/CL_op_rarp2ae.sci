//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [sma,ecc] = CL_op_rarp2ae(ra,rp)
// Apoapsis radius and periapsis radius to semi major axis and eccentricity (elliptic orbits)
//
// Calling Sequence
// [sma,ecc] = CL_op_rarp2ae(ra,rp)
//
// Description
// <itemizedlist><listitem>
// <p>Computes semi major axis and eccentricity from
// apoapsis radius and periapsis radius (elliptic orbits only).</p>
// <p></p></listitem>
// <listitem>
// <p><b>Note:</b></p>
// <p>CL_op_rarp2ae(rp,ra) is the same as: CL_op_rarp2ae(ra,rp)</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ra: Apoapsis radius [m] (1xN or 1x1)
// rp: Periapsis radius [m] (1xN or 1x1)
// sma: Semi major axis [m] (1xN)
// ecc: Eccentricity (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_op_rava2ae
// CL_op_rpvp2ae
//
// Examples
// ra = 7178.e3;
// rp = 7078.e3;
// [sma,ecc] = CL_op_rarp2ae(ra,rp)

// Declarations:

// Code:

Nr = size(ra,1); 
[ra, rp] = CL__checkInputs(ra, Nr, rp, Nr); 

if (find(ra <= 0 | rp <= 0) <> []); CL__error("Positive arguments expected"); end;

sma = (ra+rp)/2; 
ecc = abs(ra-rp) ./ (ra+rp); 

endfunction
