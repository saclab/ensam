//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_oe, cjd] = CL_tle_getElem(tle, type_oe, frame, ut1_tref, tt_tref)
// TLE epoch and associated mean orbital elements (no propagation)
//
// Calling Sequence
// [mean_oe, cjd] = CL_tle_getElem(tle, type_oe [, frame, ut1_tref, tt_tref])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Returns the mean orbital elements in a TLE structure. </p>
// <p>The orbital elements can be returned in various forms: Keplerian elements, position and velocity... </p>
// <p></p>
// <p>In a standard situation, the orbital elements are converted to the specified reference <b>frame</b>. The epoch (internally in UTC) is converted to the TREF time scale. Note that if the frame is not a true equator frame, the transformation may not as accurate as should be
// as the frame transformation arguments that can be provided are limited to ut1_tref and tt_tref. </p>
// <p></p>
// <p>It is also possible to give <b>frame</b> a special value: "native". It means that the output reference frame AND 
// time scale are those natively used for TLEs: TEME and UTC repectively. No conversion is performed. </p> 
// </listitem>
// </itemizedlist>
// 
// Parameters
// tle: TLE structure (size N)
// type_oe: (string) Type of output orbital elements: "kep", "cir", "cireq", "equin" or "pv" (1x1)
// frame: (string, optional) Output frame, see above for details. Default is "ECI". 
// ut1_tref: (optional) UT1-TREF [seconds]. Default is %CL_UT1_TREF (1xN or 1x1)
// tt_tref: (optional) TT-TREF [seconds]. Default is %CL_TT_TREF (1xN or 1x1)
// mean_oe: Mean orbital elements (of type given by "type_oe") relative to the specified frame [m, m/s, rad] (6xN)
// cjd: TLE epoch (modified julian date from 1950.0, TREF time scale except if frame == "native") (1xN)
//
// Authors
// CNES - DCT/SB/MS
//
// See also
// CL_tle_setElem
//
// Examples
// str = [..
// "1 00005U 58002B   00179.78495062  .00000023  00000-0  28098-4 0  4753"; ..
// "2 00005  34.2682 348.7242 1859667 331.7664  19.3264 10.82419157413667" ];
// tle = CL_tle_parse(str); 
// // cjd in TREF, mean_kep relative to ECI
// [mean_kep, cjd] = CL_tle_getElem(tle, "kep") 


// Declarations:

// Code
if (~exists("frame", "local")); frame="ECI"; end
if (~exists("ut1_tref", "local")); ut1_tref = CL__dataGetEnv("UT1_TREF"); end 
if (~exists("tt_tref", "local")); tt_tref = CL__dataGetEnv("TT_TREF"); end 

// TLE propagation options from configuration
whichconst = CL_configGet("TLE_WHICHCONST"); 
opsmode = CL_configGet("TLE_OPSMODE");

// Orbital elements types
Types_oe = [ "kep", "cir", "cireq", "equin", "pv" ]; 

// check validity of TLE struct
CL__tle_checkValid(tle); 

if (typeof(frame) <> "string" | size(frame, "*") <> 1)
  CL__error("Invalid argument type or size for frame"); 
end

if (typeof(type_oe) <> "string" | size(type_oe, "*") <> 1)
  CL__error("Invalid argument type or size for type_oe"); 
end

if (find(Types_oe == type_oe) == [])
  CL__error("Invalid orbital element type"); 
end

N = size(tle); 

cjd = []; 
pos = []; 
vel = [];

// Nothing done if empty sizes
if (N == 0); return; end

// Constants used: 
cst = CL_tle_getConst(); 

// epoch 
cjd = tle.epoch_cjd; // UTC

// Mean Keplerian elements (TEME)
// frame = TEME
// if n <= 0 || status <> 0 => %nan
// sma = defined by n2*a3 = mu
n = tle.n; // rad/s
Inok = find(n <= 0 | tle.status <> 0); 
n(Inok) = %nan; 
kep = [zeros(n); tle.ecc; tle.inc; tle.argp; tle.raan; tle.ma]; 
// compute sma (unkozai)
kep(1,:) = CL__tle_sma("n2a", n, tle.ecc, tle.inc, whichconst); 
kep(:, Inok) = %nan; 



// if frame == "native" : 
// - converts type of orbital elements
// if frame <> "native" : 
// - converts date: UTC => TREF 
// - convert frame: TEME => frame

if (frame == "native")
  // kep -> orbital elements 
  mean_oe = CL_oe_convert("kep", type_oe, kep, cst.mu); 

else
  // epoch: UTC -> TREF
  cjd = CL_dat_scaleConvert("UTC", "TREF", cjd, ut1_tref, tt_tref); 

  // converts to position and velocity 
  pv = CL_oe_convert("kep", "pv", kep, cst.mu); 

  // change frame
  [pv(1:3,:), pv(4:6,:)] = CL_fr_convert("TEME", frame, cjd, pv(1:3,:), pv(4:6,:), ut1_tref=ut1_tref, tt_tref=tt_tref); 

  // converts type of orbital elements
  mean_oe = CL_oe_convert("pv", type_oe, pv, cst.mu); 

end

      
endfunction
