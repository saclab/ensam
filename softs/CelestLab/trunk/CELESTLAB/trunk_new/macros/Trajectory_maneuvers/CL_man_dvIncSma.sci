//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [varargout] = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf,posman,icheck,mu,res)
// Inclination + semi-major axis maneuver (elliptical orbits)
//
// Calling Sequence
// [deltav,dv,anv] = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf [,posman,icheck,mu])
// man = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf [,posman,icheck,mu], res="s")
//
// Description
// <itemizedlist><listitem>
// <p>Computes the velocity increment needed to change the inclination and the semi major axis.</p>
// <p>The maneuver can be performed at the ascending or descending node only. </p>
// <p>The output argument <b>dv</b> is the velocity increment vector in the "qsw" local orbital frame. </p>
// <p><b>anv</b> is the true anomaly at the maneuver position.</p>
// <p>The optional flag <b>posman</b> can be used to define the position of the maneuver 
// (maneuver at ascending node, descending node). </p>
// <p>The optional flag <b>icheck</b> is used to enforce no checking 
// on the final inclination value. If the targetted inclination is less 
// than 0 or more than pi, the ascending node will rotate 180 degrees. 
// If icheck is true, the right ascension of the ascending node is not changed by the maneuver. </p>
// <p>If the argument <b>res</b> is present and is equal to "s", all the output data are returned in a structure. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ai : Semi major-axis [m] (1xN or 1x1)
// ei : Eccentricity (1xN or 1x1)
// inci : Initial inclination [rad] (1xN or 1x1)
// pomi : Argument of periapsis [rad] (1xN or 1x1)
// af : Final semi major-axis [m] (1xN or 1x1)
// incf : Final inclination [rad] (1xN or 1x1)
// posman : (optional) Flag specifying the position of the maneuver: 1 or "an" -> ascending node, -1 or "dn" -> descending node. Default is at the ascending node. (1xN or 1x1)
// icheck: (optional, boolean) Flag specifying if incf must be checked in the standard range for inclination values ([0, pi]). Default is %t. (1x1)
// mu : (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)
// res : (string, optional) Type of output: "d" or "s" for . Default is "d".
// deltav : Norm of velocity increment. [m/s] (1xN)
// dv : Velocity increment (in cartesian coordinates) in the "qsw" local frame [m/s] (3xN)
// anv : True anomaly at maneuver position [rad] (1xN)
// man : Structure containing all the output data. 
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_man_dvInc
// CL_man_dvSma
//
// Examples
// // Typical transfer from GTO to GEO (at apogee = ascending node)
// rai = 42164.e3; // radius at apogee
// rpi = 6578.e3; // radius at perigee
// [ai, ei] = CL_op_rarp2ae(rai, rpi); 
// inci = 7 * %pi/180;
// pomi = %pi; // apogee = ascending node
// af = 42164.e3; 
// incf = 0;
// [deltav,dv,anv] = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf,posman="an")
//
// // Check results:
// anm = CL_kp_v2M(ei,anv); // mean anomaly
// kep = [ai ; ei ; inci ; pomi ; 0 ; anm];
// CL_man_applyDvKep(kep,dv)


// Declarations:

// Code:
if (~exists("posman", "local")); posman = 1; end   // default: AN 
if (~exists("icheck", "local")); icheck = %t; end 
if (~exists("mu", "local")); mu = CL__dataGetEnv("mu"); end
if (~exists("res", "local")); res = "d"; end

// check "res" argument
if (res <> "d" & res <> "s"); 
  CL__error("Invalid value for argument ''res''"); 
end
if (argn(1) > 1 & res == "s"); 
  CL__error("Invalid number of output arguments"); 
end

// convert posman type to "real"
if (typeof(posman) == "string")
  str = posman; 
  posman = %nan * ones(str); 
  posman(find(str == "an")) = 1; 
  posman(find(str == "dn")) = -1; 
end

// checks arguments sizes are OK / resizes
[ai,ei,inci,pomi,af,incf,posman] = CL__checkInputs(ai,1,ei,1,inci,1,pomi,1,af,1,incf,1,posman,1); 

if (find(ai <= 0 | ei < 0 | ei >= 1 | inci < 0 | inci > %pi | af <= 0) <> [])
  CL__error("Invalid input arguments (orbital elements)"); 
end

if (icheck & find(incf < 0 | incf > %pi ) <> [])
  CL__error("Invalid input arguments (final inclination)"); 
end

if (find(posman <> 1 & posman <> -1) <> [])
  CL__error("Invalid value for ''posman''"); 
end


// 1) Inclination maneuver (AN or DN)
[deltav,dvinc,anv] = CL_man_dvInc(ai,ei,inci,pomi,incf,posman=posman,icheck=icheck,mu=mu); 


// 2) Semi major axis maneuver - tangential maneuver
// (Supposed performed after the inclination maneuver)
[deltav,dvsma] = CL_man_dvSmaT(ai,ei,af,anv,mu=mu) 


// 3) Combined velocity vector (inc + sma maneuver at the same time)
// dvsma is considered given in the "qsw" frame of the orbit obtained after the inclination maneuver. 
// => convert dvsma to qsw frame of initial orbit by applying the rotation 
// of angle inci-incf (case "AN") or incf-inci (case "DN") 
// around the radius vector (axis number = 1) 
ang = inci - incf; 
I = find(posman == -1); 
ang(I) = -ang(I); 

dv = dvinc + CL_rot_angles2matrix(1, ang) * dvsma; 
deltav = CL_norm(dv); 


// output
if (res == "d")
  varargout = list(deltav, dv, anv); 
else
  varargout(1) = struct("deltav",deltav, "dv", dv, "anv", anv); 
end


endfunction

