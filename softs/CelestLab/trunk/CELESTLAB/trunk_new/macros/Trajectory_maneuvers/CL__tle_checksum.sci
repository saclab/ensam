//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// ----------------------------
// Internal function: computes the checksum using the "TLE" algorithm
// str: string (any size, any length)
// chks: checksum, same size as str
//
// Notes for calculation: 
// - <=> 1
// letter or non numeric (including "." or " ") <=> 0
// number <=> value
// checksum = sum modulo 10
//
// ----------------------------
function [val] = CL__tle_checksum(str)

// special case for []
val = []; 
if (str == [])
  return; 
end

// resize to one column 
sz = size(str)
str = matrix(str, -1, 1); 

// considers same number of characters in all strings
nb = max(length(str)); 
str = part(str, 1:nb);

asc = matrix(ascii(str), nb, -1); 
val = zeros(asc); 

I = find(asc == ascii("-")); 
val(I) = 1; 

I = find(asc >= ascii("0") & asc <= ascii("9")); 
if (I <> []); val(I) = asc(I) - ascii("0"); end

// computes and restore initial size
val = modulo(sum(val, "r"), 10)'; 
val = matrix(val, sz); // initial size

endfunction

