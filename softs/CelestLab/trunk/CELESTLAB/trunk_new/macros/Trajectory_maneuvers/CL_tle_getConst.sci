//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


function [cst] = CL_tle_getConst()
// Physical constants used in TLE propagation 
//
// Calling Sequence
// cst = CL_tle_getConst()
//
// Description
// <itemizedlist>
// <listitem>
// <p>This function returns the physical constant values used by the SGP4/SDP4 propagation functions: gravitational constant, equatorial radius, zonal terms. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// cst: Structure containg the constants used. Units are USI (m, sec, rad). 
//
// Authors
// CNES - DCT/SB/MS
//
// Examples
// cst = CL_tle_getConst()
// cst.mu


// check presence of extension module
CL__checkExtensionModule(); 

// TLE propagation options from configuration
whichconst = CL_configGet("TLE_WHICHCONST"); 

cst = CLx_tle_getConst(whichconst); 

endfunction
