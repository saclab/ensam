//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


function [tle] = CL_tle_load(fnames)
// Loads TLEs from files
//
// Calling Sequence
// tle = CL_tle_load(fnames)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Loads two-line elements from files. </p>
// <p>The file names are considered in sequence. The files should contain standard two-line elements. </p>
// <p></p>
// <p>The TLE lines are those that begin with "1" or "2". The line above a line that begins with "1" is considered as containing the description if it does not itself begin with 1 ou 2. </p>
// <p>The status field is set to a non-zero value if parsing errors occur (see <link linkend="CL_tle_parse">CL_tle_parse</link>). </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// fnames: (string) File names or paths (1xP)
// tle: Structure containing all the TLEs present in the files (size N)
//
// Authors
// CNES - DCT/SB/MS
//
// See also
// CL_tle_parse
// CL_tle_new
//
// Examples
// fnames = CL_path("tle_examples.txt", CL_home(), "all"); 
// tle = CL_tle_load(fnames); 
// // show 2nd element of TLE structure
// tle(2)
// // show all eccentricities 
// tle.ecc
// // select TLE indices such that eccentricity is more than 0.01
// I = find(tle.ecc > 0.01); 
// tle2 = tle(I)



// internal function: loads TLEs from ONE file 
// fname: string (1x1)
function [tle] = tle_load(fname)

  if (~isfile(fname(k)))
    CL__error("File not found: " + fname); 
  end

  M = stripblanks(mgetl(fname(k)), %t);

  I1 = find(part(M,1) == "1"); 
  I2 = find(part(M,1) == "2"); 

  if (length(I1) <> length(I2))
    CL__error("Invalid TLE file"); 
  end

  if (I1 == [])
    tle = CL__tle_create(); 
    return; 
  end

  if (find(I2-I1 <> 1) <> [])
    CL__error("Invalid TLE file"); 
  end

  str = [M(I1), M(I2)]'; 

  Idesc = max(I1-1,1); 
  desc = M(Idesc)'; 
  I = find(Idesc == I1 | Idesc == I2); 
  if (I <> []); desc(I) = ""; end

  tle = CL_tle_parse(str, desc); 

endfunction


// empty by default
tle = CL__tle_create(); 

if (fnames == [])
  return;  
end

if (typeof(fnames) <> "string")
  CL__error("Wrong type for input argument ''fnames''"); 
end

for k = 1 : size(fnames,2)
  fname = fnames(k); 

  // creates TLE
  tle1 = tle_load(fname); 

  // add to previously read TLEs
  tle = [tle, tle1]; 

end

endfunction
