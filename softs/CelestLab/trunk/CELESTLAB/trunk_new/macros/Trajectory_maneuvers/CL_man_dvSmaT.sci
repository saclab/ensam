//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [varargout] = CL_man_dvSmaT(ai,ei,af,anvman,mu,res)
// Change of semi-major axis by a tangential maneuver (elliptical orbits)
//
// Calling Sequence
// [deltav,dv,anv] = CL_man_dvSmaT(ai,ei,af,anvman [,mu])
// man = CL_man_dvSmaT(ai,ei,af,anvman [,mu], res="s")
//
// Description
// <itemizedlist><listitem>
// <p>Delta-v required to change the semi major axis by 
// one tangential maneuver at a specified position.</p>
// <p>The output argument <b>dv</b> is the velocity increment in cartesian coordinates in the "qsw" local frame. </p>
// <p>If the argument <b>res</b> is present and is equal to "s", all the output data are returned in a structure. </p>
// </listitem>
// <listitem>
// <p> Note: The results are set to %nan if the targeted semi major-axis is unreachable.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// ai : Initial semi major axis [m] (1xN or 1x1)
// ei : Initial eccentricity (1xN or 1x1)
// af : Final semi major axis [m] (1xN or 1x1)
// anvman : True anomaly at the position of the maneuver. (1xN or 1x1)
// mu : (optional) Gravitational constant. [m^3/s^2] (default value is %CL_mu)
// res : (string, optional) Type of output: "d" or "s" for . Default is "d".
// deltav : Norm of velocity increment. [m/s] (1xN)
// dv : Velocity increment (in cartesian coordinates) in the "qsw" local frame [m/s] (3xN)
// anv : True anomaly at the position of the maneuver [rad] (1xN)
// man : Structure containing all the output data. 
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_man_dvSma
//
// Examples
// ai = 7200.e3;
// ei = 0.1;
// af = 7000.e3;
// anvman = %pi/4; 
// [deltav,dv,anv] = CL_man_dvSmaT(ai,ei,af,anvman)
//
// // Check results :
// anm = CL_kp_v2M(ei,anv); // mean anomaly
// kep = [ai ; ei ; %pi/2 ; 0 ; 0 ; anm];
// CL_man_applyDvKep(kep,dv)

// Declarations:

// Code:
if (~exists("mu", "local")); mu = CL__dataGetEnv("mu"); end
if (~exists("res", "local")); res = "d"; end

// check "res" argument
if (res <> "d" & res <> "s"); 
  CL__error("Invalid value for argument ''res''"); 
end
if (argn(1) > 1 & res == "s"); 
  CL__error("Invalid number of output arguments"); 
end

// checks arguments sizes are OK / resizes
[ai,ei,af,anv] = CL__checkInputs(ai,1,ei,1,af,1,anvman,1); 

if (find(ai <= 0 | ei < 0 | ei >= 1 | af <= 0) <> [])
  CL__error("Invalid input arguments (orbital elements)"); 
end

// Radius at maneuver position
r = ai .* (1-ei.^2) ./ (1 + ei .* cos(anv));  

// Initial velocity
Vi = sqrt(mu .* (2 ./ r - 1 ./ ai)); 

// Final velocity
// NB: condition on final semi major axis so that Vf exists
Vf = real(sqrt(mu .* (2 ./ r - 1 ./ af))); 
I = find(Vf <= 0); 
Vf(I) = %nan; 

// Slope of velocity (for computation of components in qsw)
slope = atan(ei .* sin(anv), 1 + ei .* cos(anv)); 

// components in "qsw"
// NB: 3rd component is 0 or %nan if Vf is %nan
dv = [(Vf - Vi) .* sin(slope); (Vf - Vi) .* cos(slope); zeros(slope) .* Vf]; 

// Norm of velocity increment
deltav = CL_norm(dv); 

// output
if (res == "d")
  varargout = list(deltav, dv, anv); 
else
  varargout(1) = struct("deltav",deltav, "dv", dv, "anv", anv); 
end


endfunction
