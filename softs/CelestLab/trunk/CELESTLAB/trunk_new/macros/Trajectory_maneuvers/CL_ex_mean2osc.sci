//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [osc_oe] = CL_ex_mean2osc(mod, type_oe, mean_oe, er, mu, j1jn)
// Mean to osculating orbital elements (all analytical models)
//
// Calling Sequence
// osc_oe = CL_ex_mean2osc(mod, type_oe, mean_oe [, er, mu, j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>Computes osculating orbital elements from mean orbital elements.</p>
// <p></p>
// <p>The available models are: </p>
// <p>"central": Central force (osculating elements = mean elements) </p>
// <p>"j2sec": Secular effects of J2 (osculating elements = mean elements by convention)</p>
// <p>"lydsec": Lyddane (mean elements include secular effects only) </p>
// <p>"lydlp": Lyddane (mean elements include secular and long period effects) </p>
// <p>"eckhech": Eckstein-Hechler (mean elements include secular and long period effects) </p>
// <p></p></listitem>
// <listitem>
// <p>Notes:</p>
// <p> - Conversion of orbital type takes place if the type of orbital elements is not the "natural" type 
// for the model and if a conversion is necessary. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Propagation models">Propagation models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// mod: (string) Model name: "central", "j2sec", "lydsec", "lydlp", "eckhech". (1x1)
// type_oe: (string) Type of orbital elements used for input/output: "kep", "cir", "cireq" or "equin" (1x1)
// mean_oe: Mean orbital elements (6xN)
// er: (optional) Equatorial radius [m]. Default is %CL_eqRad
// mu: (optional) Gravitational constant [m^3/s^2]. Default is %CL_mu
// j1jn: (optional) Vector of zonal harmonics. Default is %CL_j1jn (Nz x 1)
// osc_oe: Osculating orbital elements (6xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_osc2mean
// CL_ex_propagate
//
// Examples
// mean_kep = [7.e6; 1.e-3; 1; %pi/2; 0.1; 0.2] 
// osc_kep = CL_ex_mean2osc("eckhech", "kep", mean_kep) 
// CL_ex_osc2mean("eckhech", "kep", osc_kep) // => mean_kep

// Declarations:

// Code:
if (~exists("er", "local")); er = CL__dataGetEnv("eqRad"); end
if (~exists("mu", "local")); mu = CL__dataGetEnv("mu"); end
if (~exists("j1jn", "local")); j1jn = CL__dataGetEnv("j1jn"); end

Models = [ "central", "j2sec", "lydsec", "lydlp", "eckhech" ];  
// "natural" types for each model ("-" means "any") 
Types_oe_nat = ["-", "-", "kep", "kep", "cir"]; 

if (~or(type_oe == ["kep", "cir", "cireq", "equin"]))
  CL__error("Invalid type of orbital elements"); 
end

imod = find(mod == Models); 
if (imod == [])
  CL__error("Invalid model name"); 
end

nat_type = Types_oe_nat(imod); 
convert = (type_oe <> nat_type & nat_type <> "-"); 

if (convert)
  // converts to "natural" type
  mean_oe = CL_oe_convert(type_oe, nat_type, mean_oe, mu); 
end


if (imod == 1) 
  // central force (osc = mean)
  osc_oe = mean_oe; 

elseif (imod == 2) 
  // Secular J2 (osc = mean by convention) 
  osc_oe = mean_oe; 

elseif (imod == 3) 
  // Lyddane (mean = secular)
  [oe1, osc_oe] = CL__ex_propag_lydsec(0, mean_oe, 0, er, mu, j1jn, %t); 
  
elseif (imod == 4) 
  // Lyddane (mean = secular + long periods)
  [oe1, osc_oe] = CL__ex_propag_lydlp(0, mean_oe, 0, er, mu, j1jn, %t); 

elseif (imod == 5) 
  // Eckstein-Hechler
  [oe1, osc_oe] = CL__ex_propag_eckhech(0, mean_oe, 0, er, mu, j1jn, %t); 

end

if (convert)
  // converts back to "initial" type 
  osc_oe = CL_oe_convert(nat_type, type_oe, osc_oe, mu); 
end

endfunction
