//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


function [str, desc] = CL_tle_format(tle)
// Two-line string generation from TLE structure
//
// Calling Sequence
// [str, desc] = CL_tle_format(tle)
//
// Description
// <itemizedlist>
// <listitem>
// <p>This function creates a 2-line string from a TLE structure. </p>
// <p>The "desc" field in the structure is returned in the <b>desc</b> argument. </p>
// <p></p>
// <p>The output string is generated even if the "status" value is not 0. </p>
// <p>If a value does not fit with the format, the slot in the string is filled with "#" characters. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// tle: TLE structure (size N). 
// str: (string) Standard two-line element (2xN)
// desc: (string) Description or empty string (1xN)
//
// Authors
// CNES - DCT/SB/MS
//
// See also
// CL_tle_parse
//
// Examples
// str = [..
// "1 00005U 58002B   00179.78495062  .00000023  00000-0  28098-4 0  4753"; ..
// "2 00005  34.2682 348.7242 1859667 331.7664  19.3264 10.82419157413667" ];
// tle = CL_tle_parse(str)
// str2 = CL_tle_format(tle)




// not valid if one element is %nan
// x: NxP
function [valid] = tle_valid(x)
  valid = ~isnan(sum(x, "c")); 
endfunction

// transforms cjd into epochyr, epoch_days
// NB: epochdays: from 0 jan 1950 0h. 
// Manages %nan
function [epochyr, epochdays] = tle_cjd2yd(cjd)
  epochyr = %nan * ones(cjd); 
  epochdays = %nan * ones(cjd); 

  I = find(~isnan(cjd)); 
  if (I <> [])
    // everything here has size I
    cjd = cjd(I); 
    cal = CL_dat_cjd2cal(cjd); 
    year = cal(1,:); 
    cjd1 = CL_dat_cal2cjd(year, 1, 1); 
    ep_yr = year - 2000; 
    J = find(ep_yr < 0); 
    if (J <> []); ep_yr(J) = ep_yr(J) + 100; end
    ep_days = cjd - cjd1 + 1; 
    epochyr(I) = ep_yr; 
    epochdays(I) = ep_days
  end
endfunction


// write x using format in string. 
// x: column vector
// The total length of the result should be lg.
// If data does not fit with format => string becomes "###..."
function [str] = tle_toString1(x, form, lg)
  str = emptystr(size(x,1),1);
  I = find((typeof(x) == "constant" & ~isnan(sum(x,"r"))) | typeof(x) == "string");
  str(I) = sprintf(form + "\n", x(I)); 
  I = find(length(str) <> lg); 
  if (I <> [])
    s = strcat(repmat("#", 1, lg), ""); 
    s = repmat(s, length(I), 1); 
    str(I) = s; 
  end   
endfunction


function [str] = tle_toString(form, lg, varargin)
  N = size(varargin); 
  if (N == 1); str = sprintf(form + "\n", varargin(1)); 
  elseif (N == 2); str = sprintf(form + "\n", varargin(1), varargin(2)); 
  elseif (N == 3); str = sprintf(form + "\n", varargin(1), varargin(2), varargin(3)); 
  elseif (N == 4); str = sprintf(form + "\n", varargin(1), varargin(2), varargin(3), varargin(4)); 
  else CL__error("Too many arguments"); end

  // looks for %nan (real)
  nan = zeros(varargin(1)); 
  for i = 1 : length(varargin)
    I = find(typeof(varargin(i)) == "constant" & isnan(varargin(i))); 
    nan(I) = 1;  
  end
  I = find(length(str) <> lg | nan); 
  if (I <> [])
    s = strcat(repmat("#", 1, lg), ""); 
    s = repmat(s, length(I), 1); 
    str(I) = s; 
  end   
endfunction


// check structure is valid
CL__tle_checkValid(tle); 

str = []; 
desc = []; 

// N = number of TLEs in struct
N = size(tle); 
if (N == 0)
  return; 
end


// Notes: 
// Format: 
// 1 NNNNNC NNNNNAAA NNNNN.NNNNNNNN +.NNNNNNNN +NNNNN-N +NNNNN-N N NNNNN
// 2 NNNNN NNN.NNNN NNN.NNNN NNNNNNN NNN.NNNN NNN.NNNN NN.NNNNNNNNNNNNNN
// 
// To ensure that the total lenght is correct (69 characters, including checksum), 
// each field is formatted individually (with the a guaranteed length), and the whole string is obtained by catenating all the fields. 


// ----------
// line 1
// ----------
 
// num1: 1-1
s_num1 = tle_toString("%d", 1, ones(N,1)); 

// satnum: 3-7
s_satnum = tle_toString("%05d", 5, tle.satnum'); 

// classif: 8-8
s_classif = tle_toString("%s", 1, tle.classif'); 

// international designator: 10-17
s_intldesg = tle_toString("%-8s", 8, tle.intldesg'); 

// epoch: 19-32 -> YYDDD.DDDDDDDD
[epochyr, epochdays] = tle_cjd2yd(tle.epoch_cjd); 
epochdays_i = floor(epochdays); 
epochdays_f = epochdays-floor(epochdays); 

s_epoch = tle_toString("%02d%03d.%08d", 14, epochyr', epochdays_i', round(epochdays_f'*1.e8)); 

// ndot/2 (rev/day^2): 34-43 -> S.NNNNNNNN (S = sign)
ndot = tle.ndot * (86400^2) * (180/%pi); 
s_sgn = repmat(" ", 1, N); 
I = find(ndot <= 0); 
s_sgn(I) = "-"; 
s_ndot = tle_toString("%1s.%08d", 10, s_sgn', round(abs(ndot)'*1.e8)); 

// nddot/6 (rev/day^3): 45-52 -> S[.]NNNNNSN ([] = implicit)
nddot = tle.nddot * (86400^3) * (180/%pi); 
nexp = zeros(nddot); 
I = find(abs(nddot) > 0); 
nexp(I) = int(log10(abs(nexp(I)))); // rounds towards 0
nddot = nddot ./ (10 .^ nexp); 
s_sgn = repmat(" ", 1, N); 
I = find(nddot < 0); 
s_sgn(I) = "-"; 
s_sgnexp = repmat(" ", 1, N); 
I = find(nexp <= 0); 
s_sgnexp(I) = "-"; 

s_nddot = tle_toString("%1s%05d%1s%1d", 8, s_sgn', round(abs(nddot'))*1.e5, s_sgnexp', abs(nexp)'); 

// bstar (unit=1/Earth radii): 54-61 -> S[.]NNNNNSN
bstar = tle.bstar; 
ibexp = zeros(bstar); 
I = find(abs(bstar) > 0); 
ibexp(I) = int(log10(abs(bstar(I)))); // rounds towards 0
bstar = bstar ./ (10 .^ ibexp); 
s_sgn = repmat(" ", 1, N); 
I = find(bstar < 0); 
s_sgn(I) = "-"; 
s_sgnexp = repmat(" ", 1, N); 
I = find(ibexp <= 0); 
s_sgnexp(I) = "-"; 
s_bstar = tle_toString("%1s%05d%1s%1d", 8, s_sgn', round(abs(bstar')*1.e5), s_sgnexp', abs(ibexp)'); 

// ephemeris type: 63-63
s_ephtype = tle_toString("%1d", 1, tle.ephtype');

// element number: 65-68
s_elnum = tle_toString("%4d", 4, tle.elnum');

// line1 (68 characters)
str1 = s_num1 + " " + s_satnum + s_classif + " " + ..
       s_intldesg + " " + s_epoch + " " + ..
       s_ndot + " " + s_nddot + " " + s_bstar + " " + ..
       s_ephtype + " " + s_elnum; 

// Compute checksum and add it to str1
chks = CL__tle_checksum(str1); 
str1 = str1 + tle_toString("%1d", 1, chks); 


// ----------
// line 2
// ----------

// num1: 1-1
s_num2 = tle_toString("%1d", 1, 2*ones(N,1)); 

// satnum: 3-7
s_satnum = tle_toString("%05d", 5, tle.satnum'); 

// inclination (rad): 9-16 -> NNN.NNNN
inc = tle.inc * 180/%pi;
s_inc = tle_toString("%8.4f", 8, inc');

// RAAN (rad): 18-25 -> NNN.NNNN
raan = CL_rMod(tle.raan * 180/%pi, 0, 360); // deg
s_raan = tle_toString("%8.4f", 8, raan');

// eccentricity: 27-33 ->  [.]NNNNNNN
s_ecc = tle_toString("%07d", 7, round(tle.ecc' * 1.e7));

// arg of perigee (deg): 35-42 -> NNN.NNNN
argp = CL_rMod(tle.argp * 180/%pi, 0, 360); 
s_argp = tle_toString("%8.4f", 8, argp');

// mean anom (deg): 44-51 -> NNN.NNNN
anm = CL_rMod(tle.ma * 180/%pi, 0, 360); 
s_anm = tle_toString("%8.4f", 8, anm');

// mean motion (rev/day): 53-63 -> NN.NNNNNNNN
n = tle.n * 86400/(2*%pi); 
s_n = tle_toString("%11.8f", 11, n');

// revolution number: 64-68  
s_revnum = tle_toString("%05d", 5, tle.revnum');

// line2 (68 characters)
str2 = s_num2 + " " + s_satnum + " " + s_inc + " " + ..
       s_raan + " " + s_ecc + " " + s_argp + " " + ..
       s_anm + " " + s_n + s_revnum; 

// Compute checksum and add it to str2
chks = CL__tle_checksum(str2); 
str2 = str2 + tle_toString("%1d", 1, chks); 


str = [str1, str2]'; 


// ----------
// desc
// ----------

desc = tle.desc'; 


endfunction



