//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Internal function: creation of a TLE structure
// fields: come from standard TLE 
// NB: epoch_cjd: from 1/1/1950 0h

// can be called with 0 arguments

function [tle] = CL__tle_create(satnum, classif, intldesg, ephtype, elnum, epoch_cjd, revnum, ..
             ecc, inc, argp, raan, ma, n, ndot, nddot, ..
             bstar, desc, status)

// number of input arguments
nbargs = argn(2); 

if (nbargs <> 0 & nbargs <> 18)
  CL__error("Invalid number of input arguments"); 
end

// No args => initializes args to []
if (nbargs == 0)
  satnum = []; 
  classif = []; 
  intldesg = []; 
  ephtype = []; 
  elnum = []; 
  epoch_cjd = []; 
  revnum = []; 
  ecc = []; 
  inc = []; 
  argp = []; 
  raan = []; 
  ma = []; 
  n = []; 
  ndot = []; 
  nddot = []; 
  bstar = []; 
  desc = []; 
  status = []; 
end

tle = mlist(["CLtle", "satnum", "classif", "intldesg", "ephtype", "elnum", ..
             "epoch_cjd", "revnum", ..
             "ecc", "inc", "argp", "raan", "ma", "n", "ndot", "nddot", "bstar", "desc", "status"], ..
             satnum, classif, intldesg, ephtype, elnum, epoch_cjd, revnum, ..
             ecc, inc, argp, raan, ma, n, ndot, nddot, bstar, desc, status); 

endfunction

