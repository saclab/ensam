//  Copyright (c) CNES 2013
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

function [cst] = CL_stela_getConst()
// Physical constants used by STELA 
//
// Calling Sequence
// cst = CL_stela_getConst()
//
// Description
// <itemizedlist>
// <listitem>
// <p>Returns a structure of the physical contants used by STELA. </p>
// <p></p>
// </listitem>
// <listitem>
// <p><b>Note</b>:</p>
// <p>The returned values depend on the user configuration.</p>
// <p></p>
// </listitem>
// <listitem>
// <p>See the <link linkend="STELA">STELA</link> page for more details.</p>
// <p></p>
// </listitem>
// </itemizedlist>
// 
// Authors
// CNES - DCT/SB/MS 
//
// Examples
// cst = CL_stela_getConst(); 
// cst.mu
// cst.j1jn


// check extension module
CL__checkExtensionModule(); 

// Manage constants
CL__stela_manageConst(); 

// Get all available constants
cst = CLx_stela_getConst(0); 

endfunction
