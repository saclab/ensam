//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [info] = CL_ex_getInfo(mod, type_oe, mean_oe, er, mu, j1jn)
// Information about a propagation model
//
// Calling Sequence
// [info] = CL_ex_getInfo(mod, type_oe, mean_oe [, er, mu, j1jn])
//
// Description
// <itemizedlist><listitem>
// <p>Computes data related to the specified orbit propagation model and for the
// given orbital elements.</p>
// <p></p>
// <p>The available models are: </p>
// <p>"central": Central force (osculating elements = mean elements) </p>
// <p>"j2sec": Secular effects of J2 (osculating elements = mean elements by convention)</p>
// <p>"lydsec": Lyddane (mean elements include secular effects only) </p>
// <p>"lydlp": Lyddane (mean elements include secular and long period effects) </p>
// <p>"eckhech": Eckstein-Hechler  (mean elements include secular and long period effects) </p>
// <p></p>
// <p>The computed information is gathered in a structure (<b>info</b>): </p>
// <p>- info.mod: Model name </p>
// <p>Additional information: </p>
// <p><b>eckhech</b>: </p>
// <p> - info.exf: Mean eccentricity vector x-component (e * cos(w)) of frozen orbit </p>
// <p> - info.eyf: Mean eccentricity vector y-component (e * sin(w)) of frozen orbit </p>
// <p> - info.dgomdt: Mean secular drift of right ascension of ascending node [rad/s]</p>
// <p> - info.dpsodt: Mean secular drift of argument of latitude (w+M) [rad/s]</p>
// <p><b>lydsec</b>, <b>j2sec</b> or <b>central</b>:</p>
// <p> - info.eccf: Mean eccentricity of frozen orbit</p>
// <p> - info.pomf: Mean argument of periapsis of frozen orbit</p>
// <p> - info.dgomdt: Mean secular drift of right ascension of ascending node [rad/s]</p>
// <p> - info.dpsodt: Mean secular drift of argument of latitude (w+M) [rad/s]</p>
// <p></p></listitem>
// <listitem> 
// <p><b>Note:</b></p>  
// <p>The outputs depend on semi major-axis, eccentricity and inclination only. </p>
// <p></p></listitem>
// <listitem>
// <p>See <link linkend="Propagation models">Propagation models</link> for more details.</p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// mod: (string) Model name: "central", "j2sec", "lydsec", "lydlp", "eckhech". (1x1)
// type_oe: (string) Type of orbital elements used for input/output: "kep", "cir", "cireq" or "equin" (1x1)
// mean_kep: Mean orbital elements (6xN). 
// er: (optional) Equatorial radius [m] (default is %CL_eqRad)
// mu: (optional) Gravitational constant [m^3/s^2] (default value is %CL_mu)
// j1jn: (optional) Vector of zonal coefficients J1 to Jn (troncated to J6) to be used (default is %CL_j1jn(1:6)) (1xNz)
// infos: (structure) Information data for the given orbital elements (containing (1xN) vectors)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_ex_propagate
//
// Examples
// // Example :
// mean_kep = [7.e6; 1.e-2; 1.7; 0; 0; 0 ]; // sma, ecc, inc... 
// info = CL_ex_getInfo("lydsec", "kep", mean_kep)

// Declarations:

// Code:
if (~exists("er", "local")); er = CL__dataGetEnv("eqRad"); end
if (~exists("mu", "local")); mu = CL__dataGetEnv("mu"); end
if (~exists("j1jn", "local")); j1jn = CL__dataGetEnv("j1jn"); end

// model
if (typeof(mod) <> "string" | size(mod, "*") <> 1)
  CL__error("Invalid size or type for argument mod"); 
end

MODELS = [ "central", "j2sec", "lydsec", "lydlp", "eckhech" ];  
if (find(mod == MODELS) == [])
  CL__error("Invalid model name"); 
end

if (mod == "eckhech")
  if (type_oe <> "cir")
    // convert to "natural" type of model (= cir)
    mean_oe = CL_oe_convert(type_oe, "cir", mean_oe, mu); 
  end
  info = CL__ex_getInfo_eckhech(mean_oe, er, mu, j1jn); 

elseif (mod == "lydsec")
  if (type_oe <> "kep")
    // convert to "natural" type of model ( = kep)
    mean_oe = CL_oe_convert(type_oe, "kep", mean_oe, mu); 
  end
  info = CL__ex_getInfo_lydsec(mean_oe, er, mu, j1jn); 

elseif (mod == "j2sec")
  if (type_oe <> "kep")
    // convert to "natural" type of model ( = kep)
    mean_oe = CL_oe_convert(type_oe, "kep", mean_oe, mu); 
  end
  j1jn = [matrix(j1jn,-1,1); 0; 0]; 
  info = CL__ex_getInfo_j2sec(mean_oe, er, mu, j1jn(2)); 

elseif (mod == "central")
  if (type_oe <> "kep")
    // convert to "natural" type of model ( = kep)
    mean_oe = CL_oe_convert(type_oe, "kep", mean_oe, mu); 
  end
  info = CL__ex_getInfo_central(mean_oe, mu); 

else
  info = struct(); 
end

// add model name in structure
info.mod = mod; 

endfunction



