//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mean_kep] = CL__ex_convMean_lydlp(osc_kep, er,mu,j1jn)
// Lyddane propagation model - mean elements (mean = secular + LP)
//
// Calling Sequence
// mean_kep = CL__ex_convMean_lydlp(osc_kep,er,mu,j1jn)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the mean orbital elements from the osculating orbital elements, using Lyddane model. </p>
// <p>The mean elements for this model include secular and long period effects. </p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// osc_kep: Osculating Keplerian elements [sma;ecc;inc;pom;raan;anm] (6xN)
// er: Equatorial radius [m]. 
// mu: Gravitational constant [m^3/s^2]. 
// j1jn: Vector of zonal harmonics. (Nzx1)
// mean_kep: Mean Keplerian elements [sma;ecc;inc;pom;raan;anm] (6xN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// er = 6378136.3; 
// mu = 3.98600442e+14; 
// j1jn = [0; 0.001082626613; -0.000002532393; -0.000001619137; -0.000000227742; 0.000000538219]; 
// mean_kep = [7.e6; 1.e-3; 1; 0.1; 0.2; 0.3] 
// [mean_kep2,osc_kep] = CL__ex_propag_lydlp(0, mean_kep, 0, er, mu, j1jn, %t); 
// CL__ex_convMean_lydlp(osc_kep) // => mean_kep
//

// Check number of input arguments
if (argn(2) <> 4)
  CL__error("Invalid number of input arguments"); 
end

// Secular => osculating
// NB: I is not used
function [osc_kep] = lyd2_fct_osc(secu_kep, I, args)
  [X, Y, osc_kep] = CL__ex_lyddane(0, secu_kep, 0, args.er, args.mu, args.j1jn, 3); 
endfunction 

// Arguments for lyd2_fct_osc
args = struct(); 
args.er = er; 
args.mu = mu; 
args.j1jn = j1jn; 

// Compute secular elements from osculating elements
secu_kep = CL__ex_inverse("kep", osc_kep, lyd2_fct_osc, args);

// Converts secular elements to mean elements
[X, mean_kep, Y] = CL__ex_lyddane(0, secu_kep, 0, er, mu, j1jn, 2); 

// adjust angles mod 2*pi as default values may be wrong.
mean_kep(4:6,:) = CL_rMod(mean_kep(4:6,:), osc_kep(4:6,:)-%pi, osc_kep(4:6,:)+%pi); 


endfunction
