//  Copyright (c) CNES 2013
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [params] = CL_stela_params()
// Model parameters for STELA
//
// Calling Sequence
// [params] = CL_stela_params()
//
// Description
// <itemizedlist>
// <listitem>
// <p>Generates a structure of model parameters for STELA. </p>
// <p>Optional parameters are not included. </p>
// <p></p>
// </listitem>
// <listitem>
// <p>See the <link linkend="STELA">STELA</link> page for more details.</p>
// <p></p>
// </listitem>
// </itemizedlist>
//
// Examples
// // Get default values ...
// params = CL_stela_params()
// 
// // then change some of them ...
// params.drag_enabled = %f;  
// params.ref_frame = "ECI"; 
// disp(params); 
// 
// // or add new ones ...
// params.reentryAltitude = 100.e3;  
// disp(params); 

// Authors
// CNES - DCT/SB/MS

// Defines default structure
function [params] = stela_params_init()

  params = struct();

  params.mass = 1000.0;

    // Gravitation - Earth 
  params.central_enabled = %t;

  params.zonal_enabled = %t;
  params.zonal_maxDeg = 7;

  params.tesseral_enabled = %t;
  params.tesseral_maxDeg = 7;
  params.tesseral_minPeriod = 5 * 86400;

  // Gravitation - Third body
  params.thirdbody_enabled = %t;
  params.thirdbody_bodies = ["Sun", "Moon"];

  // Atmospheric drag
  params.drag_enabled = %t;
  params.drag_coefType = "constant";
  params.drag_area = 10;
  params.drag_coef = 2.2;

  // Solar activity
  params.drag_solarActivityType = "constant";
  params.drag_solarActivityFlux = 140.0;
  params.drag_solarActivityAp = 15.0;

  // SRP
  params.srp_enabled = %t;
  params.srp_area = 10;
  params.srp_coef = 1.5;

    // Reference frame
  params.ref_frame = "ICRS";  

  // integrator
  params.integrator_step = 86400;

endfunction


// check number of input arguments: must be 0
nbargs = argn(2); 

if (nbargs <> 0)
  CL__error("Invalid number of input arguments (0 expected)"); 
end

// default initialization 
params = stela_params_init(); 
  
endfunction


