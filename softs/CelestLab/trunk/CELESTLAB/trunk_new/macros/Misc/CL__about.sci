//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Menu CelestLab : about

function [] = CL__about()

  [v, dat] = CL_version("str"); 
  link_celestlab = "http://atoms.scilab.org/toolboxes/celestlab"; 

  clx_status = "NOT AVAILABLE"; 
  clx_homedir = []; 

  if (CL__checkExtensionModule(%f)) // %f => no error generated if not present
    clx_status ="AVAILABLE"; 
    clx_homedir = ["CelestLabX home directory: "; CLx_home(); ""]; 
  end
  
  
  
  msg = [ ..
    "----------------------------------------------------------"; 
    "CelestLab - Space Mechanics Toolbox for Scilab"; 
    "(c) CNES - DCT/SB"; 
    "----------------------------------------------------------"; 
    " "; 
    "Version: " + v;
    "Date: " + dat; 
    "CelestLabX: " + clx_status; 
    " "; 
    "CelestLab home directory: "; 
    CL_home(); 
    " ";
    clx_homedir;     
    "Web page: "; 
    link_celestlab;  
    " " ];  

  // show 
  messagebox(msg, "About CelestLab");
 
endfunction
