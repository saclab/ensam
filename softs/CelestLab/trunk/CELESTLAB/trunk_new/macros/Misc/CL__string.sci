//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [str] = CL__string(data, str_format, printscreen, nb_col_max)
// This function converts data into a command code that can be copy pasted into scilab to define the data

  // data can be a matrix, hypermatrix, struct, mlist (exemple : data = [3.222;2.111])
  // str: (string) the code that defines the matrix or hypermatrix (exemple: "[3.2;2.1]")
  // str_format: (string, optional) format to be used. Default is "%.17g"
  // printscreen: (boolean, optional) Prints the string in Scilab's console if set to %t. Default is %f.
  // nb_col_max: (optional) Maximum number of character for the output string.
  //             (If the output string is longer that nb_col_max, it is split into 
  //              several lines and continuation characters ("..") are added)
  //              Default is 256.
    
    
    
  function str_out = CL__splitString1(str_in, nb_col_max)
  // Splits a string into multiple lines (column of strings)
  // The function chooses the "," or ";" that is closer to nb_col_max to split the string
  // Continuation lines ".." are added at the end of each lines
    
  // str_in : string to be split (1x1)
  // nb_col_max: maximum length that a line should not reach (1x1)
  // str_out: same string, splitted into multiple lines (Px1)
  // Example :
  // str_in = "abcdefg,hijklmnopqrs,tuvwxyz";
  // nb_col_max = 10;
  // str_out = CL__splitString1(str_in, nb_col_max)
  
  // Number of splits that should be done
  nb_splits = floor(length(str_in) / nb_col_max);
  
  // Possible positions of splits (commas and semicolons)
  pos_splits = [ strindex(str_in , ",") , strindex(str_in , ";")];
  
  str_out = [];
  deb = 1;
  for k = 1 : nb_splits
    // Locate the possible split position that is closest to nb_col_max
    [val,I] = min( abs(pos_splits - k * nb_col_max) );
    if(deb <=pos_splits(I))
      str_out = [str_out ; part(str_in , deb:pos_splits(I)) + ".." ];
    end
    deb = pos_splits(I)+1;
  end
    
  str_out = [str_out ; part(str_in , deb:length(str_in)) ];
       
  endfunction
    
  function str_out = CL__splitString(str_in, nb_col_max)
  // Splits a column of strings into another column of strings
  // Each time an elements of str_in is longer that nb_col_max, the function transforms it in a column of strings.
  // The function chooses the "," or ";" that is closer to nb_col_max to split the string
    
  // str_in : Column of strings to be split (Nx1)
  // nb_col_max: maximum length that a line should not reach (1x1)
  // str_out: same string, splitted into more lines (Px1)
    
  I = find( length(str_in) > nb_col_max ) ;
  str_out = [];
  if ( I ~= [] )
    deb = 1;
    for k = 1 : length(I)
      str_out = [str_out ; str_in(deb:I(k)-1)];
      str_out = [str_out ; CL__splitString1(str_in(I(k)),nb_col_max)];
      deb = I(k) + 1;
    end
    str_out = [str_out ; str_in( I($)+1 : $) ];
  else
    str_out = str_in;
  end  
    

  endfunction
        
        
    
    
    
    
    
    
  // **************
  // MAIN FUNCTION  
  // **************
  
  if (~exists("str_format","local")); str_format = "%.17g"; end;
  if (~exists("printscreen","local")); printscreen = %f; end;
  if (~exists("nb_col_max","local")); nb_col_max = 256; end;
    
  // Case matrix
  if (typeof(data) == "constant")
    s = size(data);
    if ( s(1) == 1 & s(2) == 1) // if it is a scalar (1x1)
      str = "[" + msprintf(str_format,data)+ "]";
    elseif ( s(1) == 1) // if it is a row vector
      str = "[" + msprintf(str_format+",",data(1,1:$-1)') + msprintf(str_format,data(1,$)) + "]";
    elseif ( s(2) == 1) // if it is a column vector
      str = "[" + msprintf(str_format+";",data(1:$-1,1)) + msprintf(str_format,data($,1))+ "]";
    else // if it is a matrix
      
      str_fmt = str_format;
      for k = 2 : s(2)
        str_fmt = str_fmt + " " + str_format;
      end
      str_fmt = str_fmt + "\n";
       
      str = msprintf(str_fmt, data);

      str(1) = "[" + str(1);
      str($) = str($) + "]";
    end
      
  // Case hypermatrix
  elseif (typeof(data) == "hypermat")
    s = size(data);
    str = "hypermat([" + msprintf("%d ",s') + "] , [";
    str = str + msprintf(str_format+",",data.entries(1:$-1)); 
    str = str + msprintf(str_format,data.entries($)) + "])";
      
  // Case of CelestLab quaternion : CLquat
  elseif (typeof(data) == "CLquat")
    str = CL__string( [data.r ; data.i] ); 
    str(1) = "CL_rot_defQuat(" + str(1);
    str($) = str($) + ")";
    
  // Case structure    
  elseif (typeof(data) == "st")
    f = fieldnames(data);
    s = size(f);
    str = [];
    for k = 1 : s(1)
      str_field_k = CL__string( data(f(k)) );
      str_field_k(1) = """" + f(k) + """" + "," + str_field_k(1);
      if ( k == 1 ); str = str_field_k; 
      else; str($) = str($) + " , .."; end;
      if ( k ~= 1); str = [str ;  str_field_k]; end;
    end
    str(1) = "struct(" + str(1);
    str($) = str($) + ")";
  else
    CL__error("Only matrix and hypermatrix are handled");
  end
    
  // If string is too long, split it into multiple lines :
  str = CL__splitString(str, nb_col_max)
  
  // Replace special numbers (Nan, inf etc..)
  if( typeof(data) == "constant" | typeof(data) == "hypermat")
    str = strsubst(str,"Nan","%nan");
    str = strsubst(str,"Inf","%inf");
  end
  
  if (printscreen)
    mprintf("%s\n",str);
  end
    
endfunction
