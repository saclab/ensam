//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function CL__touchVersionFile(dir_CL)
// This function does the same as "touch" unix function on etc/celestlab.version file
//
// This is useful so that the file etc/celestlab.version is always committed and its revision number
// and revision date stays up to date.
//
// Note: The file actually adds/remove a blank space at the end of the file
//
// dir_CL: (string) Full file path to CelestLab root directory (1x1)

// Read file celestlab.version
fic_version = fullfile(dir_CL, "etc", "celestlab.version");
txt = mgetl(fic_version);

if (txt == []); txt = ""; end

// add blanks at end of last line (to to some maximum value)
// so that file is changed before next "commit" 
// (with high enough probability)
if (stripblanks(txt($),%t) <> ""); txt = [txt; ""]; end
txt($) = txt($) + " "; 
if (length(txt($)) > 1); txt($) = ""; end

// Rewrite celestlab.version file
mputl(txt, fic_version);

endfunction
