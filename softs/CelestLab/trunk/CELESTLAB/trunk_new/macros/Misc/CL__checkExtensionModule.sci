//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [installed] = CL__checkExtensionModule(err)
// Check the presence of CelestLab's extension module (CelestLabX) 
//
// Calling Sequence
// installed = CL__checkExtensionModule([err])
//
// Description
// <itemizedlist>
// <listitem>
// <p>Checks if the extension module is available. Returns %t if present. If err==%t, generates an error (default). </p>
// </listitem>
// </itemizedlist>
//
// Parameters
//
// Authors
// CNES - DCT/SB
//
// Examples
// CL__checkExtensionModule() 

// Declarations:

// Code: 
if (~exists("err", "local")); err=%t; end

installed = %f; 

if (exists("CLx_installed"))

  // Check compatibility of CelestLabX
  version_num = CL_version("num"); 
  if (CLx_installed(version_num))
    installed = %t; 
  end
end

if (~installed & err)
  CL__error("This function is not available (extension module required)");  
end

endfunction


