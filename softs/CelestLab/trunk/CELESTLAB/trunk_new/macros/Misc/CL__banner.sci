//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Displays CelestLab menu 
function CL__banner()

  txt = "CelestLab - Version " + CL_version("str"); 
  aut = "CNES - DCT/SB"; 
  l = max(length(txt) + 10, 36); 

  mprintf("\n");
  for i=1:l; mprintf("-"); end
  mprintf("\n");

  for i=1:(l-length(txt ))/2; mprintf(" "); end
  mprintf("%s\n", txt);

  for i=1:(l-length(aut))/2; mprintf(" "); end
  mprintf("%s\n", aut);

  for i=1:l; mprintf("-"); end
  mprintf("\n\n");
 
endfunction
