//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Vect] = CL__3b_monodroVectProp(M)
// This function determines the eigen vectors of a Matrix M.
// It only determines the eigen vector associated with the divergent mode (The biggest eigen value)
// Inputs:   
// M: Matrix 6x6
// Outputs:  
// Vect: Vector 1x6
//
// Author
//  R.Alacevich, B. Meyssignac (CNES - DCT/SB/MO)
//  A.Blazquez ( CNES - DCT/SB/MO)
//  AL: simplifications

// Code:

s = spec(M);
s_norme = abs(s);

[s_max, ind_max] = max(s_norme);

// [s_min, ind_min] = min(s_norme);

// on utilise le kernel, en imposant l'ordre des valeurs propres
// pour pouvoir apres connaitre l'ordre des vecteurs propres
// en particulier a proximite d'un point colineaire on trouvera parmis les six valeurs propres:
//       une valeur propre de norme>1 (la divergente) classee la premiere
//       une valeur propre de norme<1 (la convergente) classee la deuxieme
//       et quatre valeur propres de norme 1.

Vect = kernel(M - real(s(ind_max)) * eye());

// Cette partie sert a calculer la matrice P de changement de Repere.
//Val(1) = s(ind_max);
//Vect(:,2) = kernel(M-real(s(ind_min))*eye());
//Val(2) = s(ind_min);
//v=3;
//for j=1:6
//  if j<>ind_max
//     if j<>ind_min
//  Vect(:,v) = kernel(M-s(j)*eye());
//  Val(v) = s(j);
//  v=v+1;
//     end
//  end
//end
//P = Vect;
//invP = inv(P);

endfunction
