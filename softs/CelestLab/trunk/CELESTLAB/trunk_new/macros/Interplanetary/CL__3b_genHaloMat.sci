//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [correction,Xt,omegahalo] = CL__3b_genHaloMat(X0,omegacorr,t0,dmax,step,MU)
// Author:
// B. Meyssignac (CNES DCT/SB/MO)

//fonction qui calcule la trajectoire de la Halo que l'on peut obtenir avec les parametres en entree
//et les corrections numeriques en x et vy  necessaires pour la stabiliser a chaque demi tour:
// entrees:date initiale
//   temps total de trajectoire sur la Halo a calculer
// sortie:  matrice (2,n) correction qui contient dans chaque colonne
//   le module (en x et en vy) de la correction fournie pour la stabilisation
//    matrice (7,n+1) qui contient dans chaque colonne
//    point (positions-vitesses-date) ou on a fait une correction
//    avec les valeures deja stabilisees
//    omegahalo : la pulsation de l'orbite de halo

// calcul de l'orbite de halo

// Declarations:


// Code:

Xt = [];
correction = [];
omegahalo = [];


tf = t0 + dmax; // end
init = 1;

while (tf - t0 >= 0)

  [Xstab,corrnum,Xtraj,ttraj,omegatraj,deltaY] = CL__3b_stabHaloNewton(X0,omegacorr,t0,step,MU);
  
  if (deltaY == []); break; end // No convergence
  if (abs(deltaY) <= %eps); break; end // Anomaly in convergence

  if (init == 1)
    omegahalo = omegatraj; 
    init = 0;
  end

  Xti = [Xstab; t0];
  Xt = [Xt, Xti];
  correction = [correction, corrnum];

  X0 = Xtraj(:,$);
  t0 = ttraj($);

end

if (deltaY == [])
  CL__error("Halo orbit cannot be determined\n");
end

if (abs(deltaY) <= %eps)
  CL__error("Halo orbit cannot be determined\n");
end

Xtfin = [Xtraj(:,$); ttraj($)];
Xt = [Xt, Xtfin];

endfunction
