//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [Traj_dir] = CL__3b_dirDirConv(orb,T,MU)
//Author
// 12/08/07 R.Alacevich et B. Meyssignac (CNES, DCT/SB/MO)
// 12/02/09 A. Blazquez(CNES, DCT/SB/MO)

// Computes (for each point of the input trajectory) the eigen vector of the convergent an divergent 
// manifolds from the monodromy matrix computed at time T. 
// Inputs: 
// orb: Orbit (position, velocity, time) (7xN)
// T: Time for estimating the monodromy (1x1)
// sortie: 
// Traj_dir: 
//   rows 1-7: same as orb
//   8-13: normalized divergent direction for corresponding pos/vel 
//   14-19: normalized convergent direction for corresponding pos/vel 
// Traj_dir is [] if problems occur. 

// Declarations:


// Code:

n = size(orb,2);

Traj_dir = zeros(19, n); 
Traj_dir(1:7,:) = orb; 

for (k = 1 : n)
  //calcul de la monodromie a T pour la direction divergente
  //et a -T pour la direction convergente
  [XTpos, monodTpos] = CL__3b_monodromy(orb(1:6,k),  T, MU);
  [XTneg, monodTneg] = CL__3b_monodromy(orb(1:6,k), -T, MU);

  // diagonalisation des matrices de monodromie
  // AB A modifier apartir d'ici il a besoin que de vecteur propre de Lambda
  VectPropPos = CL__3b_monodroVectProp(monodTpos);
  VectPropNeg = CL__3b_monodroVectProp(monodTneg);

  // Problem ! 
  if (VectPropPos == [] | VectPropNeg == [])
    Traj_dir = []; 
    return; // <== RETURN
  end
  
  div = real(VectPropPos);
  signe_div = sign(div(1));
  div_dir = signe_div*div/norm(div);

  convv = real(VectPropNeg);
  signe_conv = sign(convv(1));
  conv_dir = signe_conv*convv/norm(convv);

  Traj_dir(8:13,k) = div_dir; 
  Traj_dir(14:19,k) = conv_dir; 

end

endfunction
