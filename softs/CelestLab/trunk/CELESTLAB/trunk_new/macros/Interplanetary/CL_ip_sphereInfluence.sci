//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [rsoi1] = CL_ip_sphereInfluence(mu1,mu2,dist)
// Radius of sphere of influence
//
// Calling Sequence
// rsoi1 = CL_ip_sphereInfluence(mu1,mu2,dist)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the radius of the sphere of influence around one celestial body. </p>
// <p>The sphere of influence (SOI) is the spherical region around a celestial body where 
// the gravitational effect of other bodies can be neglected. </p>
// <p> This is usually used to describe the region around planets in the solar system 
// where it is possible to neglect the gravitational effect of the Sun. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// mu1: Gravitational constant of body 1 (lightest) [km^3/s^2] (1xN or 1x1)
// mu2: Gravitational constant of body 2 (heaviest) [km^3/s^2] (1xN or 1x1)
// dist: Distance between the two bodies [m] (1xN or 1x1)
// rsoi1: Radius of sphere of influence around body 1 [m] (1xN)
//
// Authors
// CNES - DCT/SB
// 
// Bibliography
// 1) Orbital Mechanics for engineering students, H. D. Curtis, chapter 8 (interplanetary trajectories)
// 
// Examples
// // Example 1: Sphere of influence of the Earth
// mu1 = CL_dataGet("body.Earth.mu");
// mu2 = CL_dataGet("body.Sun.mu");
// dist = 1 * CL_dataGet("au");
// 
// rsoi1 = CL_ip_sphereInfluence(mu1,mu2,dist)
//
// // Example 2: Sphere of influence of all planets in the Solar system
// bodies = CL_dataGet("body");
// mu1 = [bodies.Mercury.mu, bodies.Venus.mu, bodies.Earth.mu,..
//        bodies.Mars.mu, bodies.Jupiter.mu, bodies.Saturn.mu,..
//        bodies.Uranus.mu, bodies.Neptune.mu];
// mu2 = bodies.Sun.mu; 
// dist = [0.39, 0.73, 1, 1.52, 5.2, 9.5, 19.2, 30.7] * CL_dataGet("au");
// 
// rsoi1 = CL_ip_sphereInfluence(mu1,mu2,dist)

// Check size 
//  Note: resize would not be necessary, as the formula would be OK,
// but done to make checks work 
[mu1, mu2, dist] = CL__checkInputs(mu1,1, mu2,1, dist,1); 

if (find(mu1 <= 0 | mu2 <= 0 | dist <= 0) <> [])
  CL__error("Invalid arguments values"); 
end

// %nan returned if mu ratio not valid
dist(mu1 > mu2) = %nan; 

rsoi1 = dist .* (mu1 ./ mu2).^(2 / 5); 

endfunction




