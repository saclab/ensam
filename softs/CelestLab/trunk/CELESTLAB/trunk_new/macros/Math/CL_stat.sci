//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [meanv,covm] = CL_stat(samples)
// Statistics on samples
//
// Calling Sequence
// [meanv,covm] = CL_stat(samples)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the mean value and the covariance matrix of samples.</p> 
// </listitem>
// </itemizedlist>
//
// Parameters
// samples: Set of drawn vectors. Each vector (= column vector) is made of P indidual variables. The number of samples (N) is the number of columns. (PxN)
// meanv: Estimated mean value (Px1)
// covm: Estimated covariance matrix (PxP)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cor2cov
//
// Examples
// // 2 independent random variables 
// // (Gaussian law, mean = 0, stdev = 1)
// samples = grand(2,1000,"nor", 0, 1); 
// [meanv,covm] = CL_stat(samples)
// [cor,sd] = CL_cov2cor(covm)

// Declarations:


// Code:

meanv = mean(samples,2);

if (CL__scilabVersion() >= 550 )
  covm = cov(samples', 1); // normalised by number of samples
  
else 
  // previous Scilab versions
  covm = mvvacov(samples');
  covm = (covm + covm') / 2; // forces symmetric result
  
end
  
endfunction
