//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [C] = CL_dMult(A,B)
// Dot multiplication (matrices or hypermatrices)
//
// Calling Sequence
// C = CL_dMult(A,B)
//
// Description
// <itemizedlist><listitem>
// <p>Multiplies matrices (or hypermatrices) elementswise (extends ".*"). </p>
// <p></p>
// <p>The matrices (or hypermatrices) are first expanded so that 
// if the size in one dimension is 1, the values are duplicated to match the maximum 
// size between the 2 matrices (or hypermatrices) in that dimension. </p>
// <p></p>
// <p> If A or B is [], the result is []. </p> 
// <p></p>
// </listitem>
// <listitem>
// <p><b>Notes</b>:</p>
// <p>- The function works for matrices or hypermatrices of any size. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// A: Matrix or hypermatrix (NxPx... or 1xPx... or Nx1x... etc...)
// B: Matrix or hypermatrix (NxPx... or 1xPx... or Nx1x... etc...)
// C: Result (NxPx...) 
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_dot
//
// Examples
// M = [1, 2, 3; 4, 5, 6; 7, 8, 9];
// f = [1; 2; 3];
// 
// // Multiply rows of M by factors in f
// CL_dMult(f, M) // => same as [f(1)*M(1,:); f(2)*M(2,:); f(3)*M(3,:)]
// 
// // Multiply columns of M by factors in f
// CL_dMult(f', M) // => same as [f(1)*M(:,1), f(2)*M(:,2), f(3)*M(:,3)]
// 
// // Other cases with matrices or vectors
// CL_dMult(M, f) // => same as CL_dMult(f, M)
// CL_dMult(f, f') // => same as f * f'
// CL_dMult(f', f) // => same as CL_dMult(f, f')
// 
// // Other cases with hypermatrices 
// CL_dMult(f, ones(3,2,4)) // => same as repmat(f, [1,2,4])
// CL_dMult(M, ones(3,3,4)) // => same as repmat(M, [1,1,4])


// Declarations:


// Code:

// resize matrices (and check consistency)
// => resulting matrix have same size (or one at least is []) 
[A, B] = CL__adjustSizes(A, B); 

// dot multiplication
if (A == [] | B == [])
  C = []; 
else
  C = A .* B; 
end

endfunction

