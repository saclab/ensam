//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [ngcd] = CL_gcd(n1,n2)
// Greatest common divisor (gcd or pgcd)
//
// Calling Sequence
// ngcd = CL_gcd(n1,n2)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the greatest common divisor of 2 integers using Euclide's algorithm.</p>
// </listitem></itemizedlist>
//
// Parameters
// n1: Integer (1 x N)
// n2: Integer(1 x N)
// ngcd: Greatest common divisor of n1 and n2 (1 x N)
//
// Authors
// CNES - DCT/SB
//
// Examples
// n1 = [12,15,19];
// n2 = [4,5,7];
// [ngcd]=CL_gcd(n1,n2)

  // Algorithme d'Euclide
  // Le pgcd est le dernier reste non nul des divisions successives.

// Declarations:


// Code:

  ngcd = zeros(1,size(n1,'c'));
  reste = zeros(1,size(n1,'c'));
  indices = 1:size(n1,'c');
  while (isempty(indices) == %f) // on sort quand plus aucun Ngcd ne vaut 0
    reste(indices) = modulo(n1(indices),n2(indices));
    n1(indices) = n2(indices) ;
  ind = find(reste == 0);
  reste(ind) =1;
  ngcd(ind) = n2(ind);
  n2(indices) = reste(indices);
  indices = find(ngcd == 0);
  end

endfunction

