//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [ires] = CL_intervInters(varargin)
// Intersection of sets of intervals
//
// Calling Sequence
// ires = CL_intervInters(i1,i2 [,i3,...,ip])
//
// Description
// <itemizedlist><listitem>
// <p>Computes the intersection of sets of intervals.</p>
// <p> The intervals are gathered in different sets (i1, i2,...,ip). </p>
// <p> A real number x belongs to the intersection of (i1, i2,...,ip) if there exists at least one interval in each set which x belongs to.</p>
// <inlinemediaobject><imageobject><imagedata fileref="intersection.gif"/></imageobject></inlinemediaobject>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p> - The intervals in each set should have empty intersections (use CL_intervUnion if needed). </p>
// <p> - Intervals of length equal to 0 are discarded. (length = end of interval minus beginning of interval) </p>
// <p> - Sets of intervals may be empty: CL_intervInters(i1,[]) = []. </p>
// <p> - Resulting set of intervals are sorted in ascending order (first row) </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// i1: Set of intervals [start ; end] (2xN)
// i2: Set of intervals [start ; end] (2xN2)
// ip: Set of intervals [start ; end] (2xNp)
// ires: Intesection of i1,i2...,ip (2xM)
//
// See also
// CL_intervUnion
// CL_intervInv
//
// Authors
// CNES - DCT/SB
//
// Examples
// i1=[ [1;3] , [5;6] , [10;12]];
// i2=[ [2;4] , [5.5;5.7] , [5.8;15]];
// ires = CL_intervInters(i1,i2);
//
// i1=[ [1;3] , [5;6] , [10;12]];
// i2=[ [2;4] , [5.5;5.7] , [5.8;15]];
// i3=[ [1.1;1.2] , [3.5;7] , [11;20]];
// ires = CL_intervInters(i1,i2,i3);

// Declarations:


// Code:

if (size(varargin) < 2)
  CL__error("Invalid number of input arguments");
end

for i=1:size(varargin)
  if (varargin(i) <> [])
    if (size(varargin(i),1) <> 2)
      CL__error('Interval '+string(i)+' must be of size 2xN');
    end
  end
end

// 2 intervalles
if (size(varargin) == 2)

  // sorting (increasing lower bound)
  i1 = CL_sortMat(varargin(1),varargin(1)(1,:));
  i2 = CL_sortMat(varargin(2),varargin(2)(1,:));
  
  i1(:, find(i1(2,:)-i1(1,:)==0)) = []; // suppression of intervals of zero length
  i2(:, find(i2(2,:)-i2(1,:)==0)) = []; // suppression of intervals of zero length
  
  if (varargin(1) == [] | varargin(2) == [])
    ires = [];
    
  else
    N1 = size(i1,2);
    N2 = size(i2,2);

    ires = [];
    
    // Parcours des intervalles de i1 :
    for k = 1 : N1
      // Il y a intersection si une de ces conditions est respectee :
      // - le debut de i2 est (strictement) inclus dans i1 OU
      // - la fin de i2 est (strictement) inclue dans i1 OU
      // - i1 est entierement inclu dans i2
      
      J = find( (i2(1,:) > i1(1,k) & i2(1,:) < i1(2,k))  | ...
                (i2(2,:) > i1(1,k) & i2(2,:) < i1(2,k))  | ...
                (i1(1,k) >= i2(1,:) & i1(2,k) <= i2(2,:)) );
      if (J <> [])
        ires = [ires, [ max(i2(1,J),i1(1,k)) ; ...  //  deb_inters = max(deb_i1,deb_i2)
                        min(i2(2,J),i1(2,k))] ];    //  fin_inters = min(fin_i1,fin_i2)
      end

    end
  end

  
// More than 2 intervals => intersections 
elseif (size(varargin) > 2)
  ires = CL_intervInters(varargin(1),varargin(2));
  for k = 3 : size(varargin)
    ires = CL_intervInters(ires,varargin(k)); 
  end
end


endfunction
