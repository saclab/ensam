//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// adjust size of A and/or B
// if size is 1 in one dimension => repmat(...) to max size between A and B
// A and B can be of any size (matrices or hypermatrices with 3 or more dimensions)
// If at least one size in one dimension is 0 <=> []
// If one of the arguments is [], the other one is not changed
function [A, B] = CL__adjustSizes(A, B)

// sizes (size vectors adjusted so that A and B have the same number of dimensions)
szA = size(A);
szB = size(B);

// protection in case [] is represented in various ways
if (min(szA) == 0)
  A = []; 
  szA = [0,0]; 
end
if (min(szB) == 0)
  B = []; 
  szB = [0,0]; 
end

// ndim: 2 for matrix or vector (or empty matrix)
ndimA = size(szA, 2); 
ndimB = size(szB, 2); 

if (ndimA <> ndimB)
  ndim = max(ndimA, ndimB); 
  // Warning: "1" is added even if empty matrix
  if (ndimA < ndim); szA = [szA, ones(1, ndim-ndimA)]; end
  if (ndimB < ndim); szB = [szB, ones(1, ndim-ndimB)]; end 
end

szmin = min(szA, szB); 
szmax = max(szA, szB); 

// check consistency: 
// for each dimension, size must be 0 or 1 or max between A and B
if (find((szA <> szmax & szA > 1) | (szB <> szmax & szB > 1)) <> [])
  CL__error("Incompatible arguments sizes");
end

if (A <> [])
  // expand A to max sizes in dimensions for which size is 1
  Nrep = ones(szA); // replication numbers
  I = find(szA == 1); 
  Nrep(I) = szmax(I); 
  if (max(Nrep) > 1)
    A = repmat(A, Nrep); 
  end
end

if (B <> [])
  Nrep = ones(szB); // replication numbers
  I = find(szB == 1); 
  Nrep(I) = szmax(I); 
  if (max(Nrep) > 1)
    B = repmat(B, Nrep); 
  end
end

endfunction

