//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [values] = CL_covDraw(means,covm,num)
// Random values from covariance 
//
// Calling Sequence
// values = CL_covDraw(means,covm,num)
//
// Description
// <itemizedlist><listitem>
// <p>Returns randomly drawn samples of the random variable X, knowing the expectation of X and its 
// covariance matrix. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// means : Vector of expectations (Nx1)
// covm: Covariance matrix (NxN)
// num : (integer) Number of random samples to draw
// values : Matrix containing the random samples. Each sample is a column vector. The number of columns is num. (NxP)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_cov2cor
//
// Examples
// means =  [10; 20]; 
// covm = [ 4, 1; 1, 5];
// values = CL_covDraw(means, covm, 1000);  // drawn values 
//
// // estimate mean values and covariance from samples
// [means2, covm2] = CL_stat(values); 
//

// Declarations:


// Code:
if (size(means,2) <> 1 | size(means,1) <> size(covm,1) | size(covm,1) <> size(covm,2))
  CL__error("Invalid size of input arguments");
end

if (num <= 0)
  CL__error("invalid number of draws");
end

N = size(means,1); // size of cov matrix and vector

[corm, sd] = CL_cov2cor(covm);
R = chol(corm)';
X = grand(N, num, "nor", 0, 1);
values = (R*X) .* (sd * ones(1,num)) + means * ones(1,num);

endfunction
