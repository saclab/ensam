//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [y, nrev] = CL_rMod(x, a, b)
// Modulo with result in range
//
// Calling Sequence
// [y, nrev] = CL_rMod(x, a, b)
// [y, nrev] = CL_rMod(x, a)
//
// Description
// <itemizedlist><listitem>
// <p>Computes the modulo in a given range. </p>
// <p></p>
// <p>The result y is such that y = x modulo [b-a] with y in [a,b[. </p>
// <p>That is to say: y = x + nrev * (b-a) with nrev: integer such that y belongs to [a, b[. </p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>- x,a,b can be of any size or of size 1x1, but the sizes must be consistent 
// (the function does not check the sizes). </p>
// <p>- CL_rMod(x, a) is the same as CL_rMod(x, 0, a) </p>
// <p>- CL_rMod([], a, b) = [] </p>
// </listitem></itemizedlist>
//
// Parameters
// x: Vector or matrix of real values (any size)
// a: Minimum value (1x1 or same size as x)
// b: Maximum value (strictly greater than a) (1x1 or same size as a)
// y: Result of "x modulo (b-a)" belonging to [a,b[ (maximum size between x, a, b)
// nrev: (integer) Number of times "b-a" has been added to x (same size as y)
//
// Authors
// CNES - DCT/SB
//
// Examples
// CL_rMod(2*%pi, -%pi, %pi)
// [y, nrev] = CL_rMod(1:20, -5, 5);
// y - nrev * 10 // => 1 : 20
// 
// x = 1:20; 
// CL_rMod(1, 2*x - 3, 2*x + 3)
// CL_rMod(2*x, x - 3, x + 3)


// Declarations:


// Code:

// default initialisation ([]) 
y = [];
nrev = []; 

if (~exists("b","local")) 
  b = a;
  a = 0 * ones(a); // [] if "a" is [] 
end

// check consistency of [] values
if ((a == [] & b <> []) | (b == [] & a <> [])) 
  CL__error("Invalid interval bounds (inconsistent empty values)");
end

if (a == [] & x <> []) 
  CL__error("Inconsistent input arguments (empty values)");
end

if (find(b - a <= 0) <> []) 
  CL__error("Invalid interval bounds");
end

if (x <> [])
  delta = b - a;
  nrev = -1 * floor((x - a) ./ delta);
  y = x + nrev .* delta;
end
 
endfunction

