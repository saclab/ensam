//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [mat] = CL_intervLinspace(interv, n)
// Sub-division of intervals
//
// Calling Sequence
// mat = CL_intervLinspace(interv, n) 
//
// Description
// <itemizedlist><listitem>
// <p>Divide each interval in sub-intervals of equal length. </p> 
// </listitem>
// <listitem>
// <p><b>Note:</b></p>
// <p>- The first (resp. last) row in mat is guaranteed identical to the first (resp. last) 
// row in interv. </p>
// <p></p></listitem>
// </itemizedlist>
//
// Parameters
// interv: Set of intervals. (2xN)
// n: (integer) Number of points (>= 2) in each interval, bounds included.  
// mat: Resulting matrix. (nxN)
//
// Authors
// CNES - DCT/SB
//
// Examples
// interv = [[-1;1], [1;2]];  
// CL_intervLinspace(interv, 11) 


// -------------------------------------------------
// Argument Check
// -------------------------------------------------
if (size(interv,1) <> 0 & size(interv,1) <> 2)
  CL__error("Invalid size for argument interv"); 
end

if (round(n) <> n | n < 2)
  CL__error("Invalid value for argument n"); 
end

mat = []; 

if (interv == [])
  return; // <= RETURN 
end

// x: column vector: from 0 to 1, n values (=> size = nx1) 
x = matrix((0 : n-1), -1, 1) / (n-1);  
mat = ones(x) * interv(1,:) + x * (interv(2,:)-interv(1,:)); 

// Force bounds (=> avoids numerical errors in case that matters)
mat(1,:) = interv(1,:); 
mat($,:) = interv(2,:); 
 
endfunction
