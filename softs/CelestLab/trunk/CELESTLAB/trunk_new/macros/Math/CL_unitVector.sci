//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [v,norms] = CL_unitVector(u)
// Vector normalization
//
// Calling Sequence
// [v,norms] = CL_unitVector(u)
//
// Description
// <itemizedlist><listitem>
// <p>Normalizes vectors. </p>
// <p> Given a vector or matrix of vectors <b>u</b>, it computes the corresponding 
// normalized vector (or matrix of vectors) <b>v</b> = u / ||u||. </p>
// <p>This function also returns the norms (<b>norms</b>) of the input vectors as second 
// output argument. </p>
// <p></p></listitem>
// <listitem>
// <p><b>Notes:</b></p>
// <p>- If a vector is a null vector, the corresponding unit vector is undefined (%nan). </p>
// <p>- If <b>u</b> is an hypermatrix (of dimension 3), v(:,:,i) = CL_unitVector(u(:,:,i)).</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// u: Vector or matrix of column vectors (PxNxK). u is an hypermatrix if K>1. 
// v: Vector or matrix of column vectors with norms equal to 1 (%nan if norm(u) is 0). (PxNxK)
// norms: Norms of input vectors. (1xNxK)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_norm
// CL_dMult
//
// Examples
// // With a matrix :
// u = rand(3,15);
// [v,norms] = CL_unitVector(u);
//
// // With an hypermatrix :
// u = rand(3,15,3);
// [v,norms] = CL_unitVector(u);


// Declarations:


// Code:

u_type = typeof(u);
v = %nan * ones(u);
if u_type=='constant'
  norms = CL_norm(u);
  ind = find(norms ~= 0); // gestion des vecteur nuls
  v(:,ind) = CL_dMult(1 ./ norms(ind) , u(:,ind));
elseif u_type=='hypermat'
  norms = zeros(u(1,:,:));
  for i=1:v.dims(3)
    [v(:,:,i),norms(:,:,i) ] = CL_unitVector(u(:,:,i));
  end
else
  CL__error('type not valid');
end

endfunction
