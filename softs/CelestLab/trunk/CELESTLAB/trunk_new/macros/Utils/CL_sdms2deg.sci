//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [degs] = CL_sdms2deg(sdms)
// Sign, degrees, arcminutes, arcseconds to decimal degrees conversion 
//
// Calling Sequence
// degs = CL_sdms2deg(sdms)
//
// Description
// <itemizedlist><listitem>
// <p>Conversion from sign, degrees, arcminutes, arcseconds to decimal degrees.</p>
// <p>The input argument sdms is such that:</p>
// <p>- The first row is the sign of the angle (0 or 1 => positive, -1 => negative).</p>
// <p>- The second row is the number of degrees.</p>
// <p>- The third row is the number of arcminutes.</p>
// <p>- The fourth row is the number of arcseconds.</p>
// <p></p>
// <p>Notes:</p> 
// <p>- The number of degrees and arcminutes should be integers but non-integers are allowed as well.</p>
// <p>- The function only checks that all values (except for the sign) are positive.</p>
// </listitem></itemizedlist>
//
// Parameters
// sdms: Sign, degrees, arcminutes and arcseconds (4xN)
// degs: Decimal degrees (1xN)
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_deg2sdms
//
// Examples
// // -40 deg 19' 30.2''
// sdms = [-1; 40 ; 19 ; 30.2];
// degs = CL_sdms2deg(sdms)

// Declarations:

// Code:

[P,N] = size(sdms);
if (P <> 4); CL__error("Invalid input argument (4 rows expected)"); end;

// Check that sign is 0,1 or -1
if (find(sdms(1,:) <> 0 & sdms(1,:) <> 1 & sdms(1,:) <> -1) <> [])
  CL__error("Invalid input argument (sign must be 0,1 or -1)");
end 
// Replace 0 with 1
I = find(sdms(1,:) == 0);
sdms(1,I) = 1;

// Check that all values are positive
if (find(sdms(2:4,:) < 0) <> []); CL__error("Invalid input argument value (negative value)"); end

degs = sdms(1,:) .* (sdms(2,:) + sdms(3,:)/60 + sdms(4,:)/3600);

endfunction
