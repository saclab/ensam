//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function CL_g_set(h,prop,val)
// Sets graphic properties
//
// Calling Sequence
// CL_g_set(h,prop,val)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Sets graphic properties (as 'set' does), but handles the case of multiple values.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// h: Vector of graphic entities (1xN).
// prop: Property to be set.
// val: Values (1xP) or (Px1). If P < N, the values are considered modulo N.
//
// Authors
// CNES - DCT/SB (AL)
//
// Examples
// f=scf();
// a=gca();
// x = 0:0.1:5;
// y = x;
// contour2d(x, y, x' * y, [1,2,3,4,5]);
// h = CL_g_select(a, "Text");
// CL_g_set(h, "text", string(-1*strtod(h.text)));


// Declarations:


// Code:

  m = max(size(val,1), size(val,2)); // size of array val [1xm or mx1]

  for k = 1:length(h)
    h(k)(prop) = val(modulo(k-1,m)+1);
  end

endfunction
