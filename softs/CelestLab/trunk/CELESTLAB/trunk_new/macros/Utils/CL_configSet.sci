//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function [] = CL_configSet(name, val)
// Set the value of configuration parameters
//
// Calling Sequence
// CL_configSet(name, val)
//
// Description
// <itemizedlist>
// <listitem>
// <p>This function sets the value of a configuration parameter. </p>
// <p>For compatibility reasons, the name of the parameter is case insensitive, 
// but uppercase characters should be preferred. </p>
// <p></p>
// </listitem>
// <listitem>
// <p>See <link linkend="Configuration">Configuration</link> for the list 
// of available configuration parameters.</p>
// </listitem>
// </itemizedlist>
//
// Parameters
// name: (string) Name of configuration parameter. (1x1)
// val: (string) Value of configuration parameter. (1x1)  
//
// Authors
// CNES - DCT/SB
//
// See also
// CL_configGet
//
// Examples
// warning_mode = CL_configGet("WARNING_MODE")
// CL_configSet("WARNING_MODE", "silent");
// CL_configGet("WARNING_MODE")
// CL_configSet("WARNING_MODE", warning_mode); // restore value

// Declarations:
global %CL__PRIV;
      
// Code
if (typeof(name) <> "string" | typeof(val) <> "string" | ..
    size(name, "*") <> 1 | size(val, "*") <> 1)
  CL__error("Invalid inputs (strings of size 1x1 expected)");
end

// NB: %CL__PRIV.PREF must have been created before.
// Done by CL__configInit in etc/celestlab.start file

// Check that %CL__PRIV is a structure and %CL__PRIV.PREF exists
if (~isstruct(%CL__PRIV)); 
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab."); 
end
if (~isfield(%CL__PRIV, "PREF"))
  CL__error("Invalid global variable %CL__PRIV. Restart CelestLab.")
end

// Upper case for compatibility 
name = convstr(name, "u"); 

// get the configuration description
config_desc = CL__configGetDesc(); 

// Check that name of config param exists
// ind = index of "name" in config_desc
ind = find(name == config_desc.names)
if (ind == [])
  CL__error("Invalid configuration parameter");
end

// Check that config parameter can be modified
if (~config_desc.settable(ind))
  CL__error("This configuration parameter cannot be modified by this function. See help for more details.");
end

// Check that the new value for this config param is valid
if (find(val == config_desc.accval(ind)) == [])
  CL__error("Invalid value for this configuration parameter");
end

// Set the new value
%CL__PRIV.PREF(name) = val;

endfunction
