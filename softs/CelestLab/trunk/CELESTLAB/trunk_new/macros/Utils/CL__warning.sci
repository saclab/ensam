//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Prints CelestLab warning
// str = (string) warning message (1x1)
function CL__warning(str)
    
warning_mode = CL_configGet("WARNING_MODE");
  
// NB : if warning_mode == "silent": do nothing
  
if (warning_mode == "standard")
  str = strsubst(str,"%","%%");
  mprintf("*** CelestLab warning: " + str + "\n");
  
elseif (warning_mode == "error")
  CL__error(str);
end

endfunction
