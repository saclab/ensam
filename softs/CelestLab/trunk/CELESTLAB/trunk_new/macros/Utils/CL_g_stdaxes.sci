//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function CL_g_stdaxes(a,colg,ft,fl,fg)
// Sets 'standard' graphic properties
//
// Calling Sequence
// CL_g_stdaxes(a,colg,ft,fl,fg)
//
// Description
// <itemizedlist>
// <listitem>
// <p>Sets some commonly used graphic properties: grid (present), subticks, size of texts, box ("on").</p> 
// <p>The subticks values are recomputed as the default values don't always seem well chosen. </p>
// </listitem>
// </itemizedlist>
//
// Parameters
// a: (optional) Handle of axes. Default is current axes (gca()).
// colg: (optional) Color index of grid. Default is 0 (black).
// ft: (optional) Title size. By default: proportionnal to figure size.
// fl: (optional) Size of x-axis and y-axis labels. By default: proportionnal to figure size.
// fg: (optional) Size of text (font_size). By default: proportionnal to figure size.
//
// Authors
// CNES - DCT/SB (AL)
//
// Examples
// f=scf();
// a=gca();
// x = 0:0.1:5;
// y = x;
// plot2d(x, y);
// CL_g_stdaxes(a);

// Declarations:
  // Adjusted size of characters function of window size 
  // result always integer
  function [n] = Sizecar(x)
    n = max(1, round(x * (size_ref/600))); 
  endfunction

// Code:

  if (~exists('a', 'local'))
     a=gca();
  end

  if exists('colg', "local")
    a.grid = [colg,colg,colg];
  else
    a.grid = [0,0,0];
  end

  size_ref = max(a.parent.axes_size); // figure size (x) 
  
  if (~exists('ft', "local")); ft = Sizecar(3); end
  if (~exists('fl', "local")); fl = Sizecar(2); end
  if (~exists('fg', "local")); fg = Sizecar(2); end

  // val: ticks locations
  // logflags: "n" or "l"
  function [subticks] = stdaxes_subticks(val, logflags)
    subticks = 0; 
    N = size(val, "*"); 
    if (size(val, "*") > 1)
      val = gsort(val, option = 'g', direction='i');
      if (logflags == "n")
        step = mean(val(2:$) - val(1:$-1));
      else
        step = log10(mean(val(2:$) ./ val(1:$-1)));
      end
      [vmin, vmax, nb] = graduate(0, step, 1);
      subticks = nb-1;
      if ((N-1) * subticks > 30); subticks = 0; end
    end
  endfunction

  a.box = "on";
  
  // compensate for an imperfection in Scilab (number of sub-ticks) in older versions
  if (CL__scilabVersion() < 550)
    a.sub_ticks = [ stdaxes_subticks(a.x_ticks.locations, part(a.log_flags,1)), ..
                    stdaxes_subticks(a.y_ticks.locations, part(a.log_flags,2))]; 
  end
  a.grid_position = "foreground";
  a.title.font_size = ft;
  a.title.font_style = 8;
  a.x_label.font_size = fl;
  a.y_label.font_size = fl;
  a.font_size = fg;

endfunction
