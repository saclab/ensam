//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

$CL_pathmacros = get_absolute_file_path("buildmacros.sce");

// Sub-directories of directory "macros" that contain ".sci" files and do not start with a "."
for $CL_d = listfiles($CL_pathmacros)'
  if (isempty(strindex($CL_d,'.'))  &  length(findfiles( fullfile($CL_pathmacros,$CL_d), "*.sci")) <> 0)
     exec( fullfile( $CL_pathmacros, $CL_d, "buildmacros.sce")); 
  end
end

clear $CL_pathmacros $CL_d 
