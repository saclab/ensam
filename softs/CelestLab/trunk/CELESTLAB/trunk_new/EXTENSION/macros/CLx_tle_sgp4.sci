//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


// Internal function:
// Generates position and velocity (in TEME) from TLE data
//
// cjd: modified julian date from 1 jan 1950 0h
// ecc, inc, argp, raan, ma: eccentricity, inclination ... mean anomaly (rad)
// mm: mean motion (rad/s)
// bstar
// delta_t: propagation time from epoch (s)
// whichconst: "wgs72old" or "wgs72" or "wgs84";
// opsmode: "afspc" or "impr"

function [pos, vel, kep] = CLx_tle_sgp4(cjd, ecc, inc, argp, raan, ma, mm, bstar, delta_t, whichconst, opsmode)

whichconst_names = ["wgs72old", "wgs72", "wgs84"];
whichconst_values = [721, 72, 84];

opsmode_names = ["afspc", "impr"];
opsmode_values = [0, 1];

// converts values for whichconst and opsmode
I = find(whichconst_names == whichconst);
if (I == []); CLx__error("Bad value for whichconst"); end
nconst = whichconst_values(I);

I = find(opsmode_names == opsmode);
if (I == []); CLx__error("Bad value for opsmode"); end
nopsmode = opsmode_values(I);


nb = length(delta_t);
pos = %nan * zeros(3,nb);
vel = %nan * zeros(3,nb);
kep = %nan * zeros(6,nb);

opt = 1;
if (size(cjd, 2) == size(delta_t, 2)); opt = 2; end

[p, v, oe, status, ierr] = CLx_tle_intern_sgp4( ..
      int32(opt), ..
      cjd, ..
      ecc, ..
      inc, ..
      argp, ..
      raan, ..
      ma, ..
      mm, ..
      bstar, ..
      int32(nconst), ..
      int32(nopsmode), ..
      delta_t, ..
      int32(nb));

if (ierr <> 0)
  CLx__error("Unable to process TLE");
end

I = find(status == 0);
pos(:,I) = p(:,I);
vel(:,I) = v(:,I);
kep(:,I) = oe(:,I);

endfunction

