//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Find files recursively with given suffix and excluding files matching a pattern
// returns res = relative path from "path"  (column vector)
// path: initial search path (1x1)
// suffix: (string) suffix of looked-for files
// excl: (string) part of file names to be excluded. [] => don't exclude any file

function [res] = CLx__findfiles(path, suffix, excl)
  res = []; 
  
  // all files in "path"
  d = matrix(findfiles(path), -1, 1); // column vector
  if (d == []); return; end
  
  // files matching pattern in path
  I = find(fileext(d) == "." + suffix); 
  res = d(I); 
  if (excl <> [])
    N = size(res, "*"); 
    I = setdiff(1:N, grep(res, excl)); 
    res = res(I); 
  end
  
  // search in sub-directories
  I = find(isdir(fullfile(path, d))); // NB: d must be Nx1
  for (i = I)
    f = CLx__findfiles(fullfile(path, d(i))); 
    if (f <> [])
      res = [res; fullfile(d(i), f)]; 
    end
  end
endfunction 
