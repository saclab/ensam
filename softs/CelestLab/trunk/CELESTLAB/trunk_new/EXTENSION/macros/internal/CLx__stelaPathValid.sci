//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Check the Stela installation directory 
// stelaRoot : path of Stela installation directory. 
// Check stela version (compares with version of Stela jars in CelestLabX) 
// Use configuration to decide if versions are compatible (number of digits to check. 
// stelaRoot: (string) Path to check (if empty: not valid) 
// valid: %t if path is valid
// effective version: version of Stela in stelaRoot ("" if path not valid)

function [valid, effectiveVersion] = CLx__stelaPathValid(stelaRoot)
  
  // path: directory containing stela jars (supposedly)   
  function [numver, strver] = getStelaVersion(path)
    numver = []; 
    strver = ""; 
    l = listfiles(fullfile(path, "stela-processing*.jar")); 
    if (size(l, "*") == 1)
      [numver, strver] = CLx__extractVersion(l(1), 0); 
    end
  endfunction
  
  // default output
  valid = %f; 
  effectiveVersion = ""; 
  
  if (stelaRoot == [])
    return;  
  end

  // version of Stela embedded in CLx
  path = fullfile(CLx_home(), "thirdparty", "stela"); 
  [numver_ref, strver_ref] = getStelaVersion(path); 
  
  // version of Stela in stelaRoot (if path valid)
  path = fullfile(stelaRoot, "lib"); 
  [numver, strver] = getStelaVersion(path); 
  
  // Note: numver_ref should not be empty
  if (numver_ref == [] | numver == [])
    return; 
  end

  // number of digits that should be equal for version check
  nd = CLx__configGet("STELA_VERSION_NCHECK"); 
  
  // Check compatibility 
  if (CLx__versionCompatible(numver, numver_ref, nd))
    valid = %t; 
    effectiveVersion = strver; 
  end
     
endfunction
