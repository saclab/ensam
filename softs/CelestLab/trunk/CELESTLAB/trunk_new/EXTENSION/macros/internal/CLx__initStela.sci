//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

// Initialize the Stela engine
function CLx__initStela()

  STELA_ROOT = CLx__configGet("STELA_ROOT"); 
  if (STELA_ROOT == [])
    return
  end
  
  try
    jimport fr.cnes.celestlab.celestlabx.stela.Initializer;

    // Init Stela
    Initializer.initStela(STELA_ROOT);
    
    // mark STELA as available
    CLx__configSet("STELA_AVAILABLE", %t); 
    
  catch
    CLx__error("Cannot initialize STELA");
  end
  
endfunction
