//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

// Initialize the Java interface
function CLx__initJava()
  
  logVerbosity = CLx__configGet("LOG_VERBOSITY");
  logFile = CLx__configGet("LOG_FILE");
  stelaLog = CLx__configGet("STELA_LOG");

  try
    jimport fr.cnes.celestlab.celestlabx.stela.Initializer;
    Initializer.initLog(logVerbosity, stelaLog, logFile);
  catch
    CLx__error("Error in log initialization");
  end
  
endfunction
