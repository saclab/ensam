//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// reset the user settings for some configuration parameters
// NB: 
// Stela path (STELA_ROOT) unchanged. 

function CLx_setting_reset()
  // default config file 
  config_path = fullfile(CLx_home(), "config_default.txt");
  
  // loads the values 
  [conf, errmsg] = CLx__configLoad(config_path); 
  if (~isempty(errmsg))
    CLx__error(errmsg);
  end
  
  for name = ["LOG_VERBOSITY", "STELA_LOG", "LOG_FILE"]
    CLx_setting_set(name, conf(name)); 
  end
      
endfunction

