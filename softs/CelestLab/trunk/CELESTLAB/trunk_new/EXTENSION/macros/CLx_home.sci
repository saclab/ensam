//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// Returns the home directory of CelestLabX
function [path] = CLx_home()
  // this function's (source) path
  my_name = "CLx_home"; 
  my_path = get_function_path(my_name); 

  // up 2 directories to get home directory from source location
  path = dirname(dirname(my_path)); 

  // check path OK (in case...)
  if (path == "" | path == "." | size(path, "*") <> 1)
    CLx__error("Invalid CelestLabX home directory")
  end

  // long path name (if windows)
  if (getos() == "Windows")
    path = getlongpathname(path); 
  end
  
endfunction
