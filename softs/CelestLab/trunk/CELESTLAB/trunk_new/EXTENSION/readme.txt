====================================
CelestLabX - Extension for CelestLab
====================================

This is CelestLab extension module.
It provides interfaces to existing functions, possibly in  
various languages (Java, C, C++).

CelestLab is a CNES Space mechanics toolbox for Scilab.
See CelestLab documentation for help.

Note that CelestLab and CelestLabX share the same version number. 