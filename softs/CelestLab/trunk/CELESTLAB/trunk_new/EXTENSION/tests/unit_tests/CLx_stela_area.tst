//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

//========================
// Test CLx_stela_area
//========================

// Simple cubesat with rectanglar solar array along the Y axis
// Both the body and the solar array have the z-axis has orientation vector (the two part-frame are identical)

// CLx model-structure 
obj = struct();

obj.id =["Body", "GS"];

obj.shap = ["Cuboid", "Rectangle"];
  
obj.dim =[
  1.    1.;   
  1.    1.;   
  1.    %nan];

obj.z_axis = [
  0.    0.;  
  0.    0.;  
  1.    1.];
  
obj.x_axis = [
  1.    1.;  
  0.    0.;  
  0.    0.];
  
obj.orig = [ 
  0.    0.;  
  0.   -1.;  
  0.    0.];


opts = struct("resol", 10000);
// Ecart absolue max (STELA GUI maximum display precision)
epsilon = 1.e-2;

//================== 
// Test 1: fixed direction
//==================

// observation direction  
direction = [0;0;1];
  
results = CLx_stela_area(obj, "fix", list(direction, opts));

expected_results = 1.98; // STELA value
assert_checkalmostequal(results, expected_results, epsilon, epsilon);

//================== 
// Test 2: random tumbling
//==================

// Number of projection planes (1000 is the default value on STELA)
nbp = 100; 
results = CLx_stela_area(obj, "tumb", list(nbp, opts));

expected_results = 1.84; // STELA value
assert_checkalmostequal(results, expected_results, epsilon, epsilon);

//================== 
// Test 3: 
// Spinning mode
//==================

// Number of projection planes (1000 is the default value on STELA)
nbp = 100; 

// spin axis
axis      = [0;0;1];
// observation direction
direction = [1;0;0];

results = CLx_stela_area(obj, "spin", list(direction, axis, nbp, opts));

expected_results = 1.26; // valeur STELA
assert_checkalmostequal(results, expected_results, epsilon, epsilon);

//================== 
// Test 4: 
// 90 deg rotation of solar array about the Y axis  
//==================

// observation direction
direction = [1;0;0];

// input parameter defining the rotation of a sub-part
rotdesc = list(struct("id", "GS", "axis", [0;1;0], "ang", %pi/2));   
results = CLx_stela_area(obj, "fixr", list(direction, rotdesc, opts));

// valeur STELA du test 1 car même geometrie
expected_results = 1.98;
assert_checkalmostequal(results, expected_results, epsilon, epsilon);
