//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.


function [] = extrap_and_check()
  // Propagates state vector and check "info" structure

  [x, xosc, dxd0, info] = CLx_stela_extrap(cjd0, x0, delta_t, params, 0, ut1_tref, tt_tref);
  checkExtrapolationError(info, []);

endfunction

tlbx_path = CLx_home();
exec(fullfile(tlbx_path, "tests", "unit_tests", "CLx_create_extrapolation_inputdata.sci"));
exec(fullfile(tlbx_path, "tests", "unit_tests", "CLx_extrap_utils.sci"));

params_backup = params;
// Check parameter coefType invalid value
params.drag_coefType = "invalid";
extrap_and_check();

// Check parameter drag_atmosphericModel invalid value
params = params_backup;
params.drag_atmosphericModel = "invalid";
extrap_and_check();

// Check parameter drag_solarActivityType invalid value
params = params_backup;
params.drag_solarActivityType = "invalid";
extrap_and_check();

// Check parameter thirdbody_bodies invalid value
params = params_backup;
params.thirdbody_bodies = ["Sun", "invalid"];
extrap_and_check();

// Check parameter delta_t
delta_t = [0 10 30];
extrap_and_check();

delta_t = [-10 0 10];
extrap_and_check();

// Check STELA validity control
delta_t = [0 10 20];
params = params_backup;
params.mass = -1000;
extrap_and_check();
