//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

tlbx_path = CLx_home();
exec(fullfile(tlbx_path, "tests", "unit_tests", "CLx_create_simulation_params.sci"));


// Initial Julian Date
cjd0 = 20000;

// Mean orbital elements (Frame = Mean Of Date)
// sma (m), ex, ey, ix, iy, M (rad)
x0 = [24678136.0; ..
      0.7172341; ..
      0.0; ..
      0.0874887; ..
      0.0; ..
      0.0];

// Dates cjd (TREF)
delta_t = 86400 * [0, 2, 4, 6, 8, 10];
