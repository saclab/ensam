//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// ================================================
// Utilitary functions for CLx_stela_ tests
// ================================================

// check status is OK
function checkExtrapolationResOK(info)
    // Check simulation status and informations
    assert_checkequal(info.status, 0);
    assert_checkequal(info.status_msg, "OK");
    assert_checkequal(info.err_msg, "");
endfunction

// check XML result
function checkExtrapolationXML(sim_xml, expected_sim_xml_name)
    expected_sim_xml = mgetl(fullfile(tlbx_path, "tests", "unit_tests", expected_sim_xml_name));
    sim_xml = info.sim_xml;
    // Remove extrapolation duration line that varies
    sim_xml = sim_xml(find(strstr(sim_xml, "extrapolationDuration") == ""));
    expected_sim_xml = expected_sim_xml(find(strstr(expected_sim_xml, "extrapolationDuration") == ""));
    assert_checkequal(size(sim_xml), size(expected_sim_xml));
    if (max(size(sim_xml) - size(expected_sim_xml)) == 0)
       assert_checkequal(sim_xml, expected_sim_xml);
    end
endfunction

function checkExtrapolationError(info, errorMessage)
    assert_checkequal(info.status, -1);
    assert_checkequal(info.status_msg, "Error");
    if (errorMessage <> [])
      assert_checkequal(info.err_msg, errorMessage);
    end
endfunction

function checkExtrapolationResult(expected_size, x, expected_last_x, varargin)
    epsilon = 1D-9;

    // Check mean params
    assert_checkequal(size(x), [6, expected_size]);
    last_x = x(:,expected_size);

    assert_checkalmostequal(last_x, expected_last_x, epsilon, epsilon, "element");

    // Check transition matrix
    if (argn(2) > 5) then
        dxd0 = varargin(1);
        expected_last_dxd0 = varargin(2);

        assert_checkequal(size(dxd0), [6, 8, expected_size]);

        last_dxd0 = dxd0(:,:,expected_size);
        assert_checkalmostequal(last_dxd0, expected_last_dxd0, epsilon, epsilon, "element");
    end
endfunction
