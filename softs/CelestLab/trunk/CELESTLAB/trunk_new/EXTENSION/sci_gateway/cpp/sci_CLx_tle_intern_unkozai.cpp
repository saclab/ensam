//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

extern "C"
{
/* ==================================================================== */
  #include "api_scilab.h"
  #include "Scierror.h"
  #include "MALLOC.h"
  #include <localization.h>
  #include "tle_interf.h"

  static int getInputMatrixDouble(int argPos, double **pdValues, char *fname);
  static int getInputInteger(int argPos, int *piValue, char *fname);
  static int setOutputMatrixInteger(int argPos, int *piValues, int iRows, int iCols, char *fname);
  static int setOutputMatrixDouble(int argPos, double *pdValues, int iRows, int iCols, char *fname);
  static int setOutputInteger(int argPos, int iValue, char *fname);
/* ==================================================================== */
  int sci_CLx_tle_intern_unkozai(char *fname, int fname_len)
  {
    double *mm = NULL;
    double *ecc = NULL;
    double *inc = NULL;
    int nconst = 0;
    int nb = 0; 
    double *sma_brw = NULL; 
    double *mm_brw = NULL; 
    int err = 0; 

    /* Check the number of input arguments */
    CheckInputArgument(pvApiCtx, 5, 5);

    /* Check the number of output arguments */
    CheckOutputArgument(pvApiCtx, 3, 3);

    // Input arguments
    if (getInputMatrixDouble(1, &mm, fname))
        return 0;

    if (getInputMatrixDouble(2, &ecc, fname))
        return 0;

    if (getInputMatrixDouble(3, &inc, fname))
        return 0;

    if (getInputInteger(4, &nconst, fname))
        return 0;

    if (getInputInteger(5, &nb, fname))
        return 0;

    // Call
    sma_brw = (double*) MALLOC(nb * sizeof(double));
    mm_brw = (double*) MALLOC(nb * sizeof(double));

    CLx_tle_unkozai(mm, ecc, inc, &nconst, &nb, 
                    sma_brw, mm_brw, &err); 

    // Output arguments
    if (setOutputMatrixDouble(1, sma_brw, 1, nb, fname)) goto end; 

    if (setOutputMatrixDouble(2, mm_brw, 1, nb, fname)) goto end; 

    if (setOutputInteger(3, err, fname)) goto end; 

end: 
    FREE(sma_brw);
    FREE(mm_brw);

    return 0;
  }
} /* extern "C" */
/* ==================================================================== */

static int getInputMatrixDouble(int argPos, double **pdValues, char *fname)
{
    int *piAddressVar = NULL;
    SciErr sciErr;
    int iRows = 0;
    int iCols = 0;

    sciErr = getVarAddressFromPosition(pvApiCtx, argPos, &piAddressVar);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (!isDoubleType(pvApiCtx, piAddressVar))
    {
        Scierror(999, "%s: Wrong type for input argument #%d: A double expected.\n", fname, argPos);
        return 1;
    }
    sciErr = getMatrixOfDouble(pvApiCtx, piAddressVar, &iRows, &iCols, pdValues);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return sciErr.iErr;
    }
    return 0;
}

static int getInputInteger(int argPos, int *piValue, char *fname)
{
    int *piAddressVar = NULL;
    SciErr sciErr;

    sciErr = getVarAddressFromPosition(pvApiCtx, argPos, &piAddressVar);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }
    if (!isIntegerType(pvApiCtx, piAddressVar))
    {
        Scierror(999, "%s: Wrong type for input argument #%d: An integer expected.\n", fname, argPos);
        return 1;
    }
    if (getScalarInteger32(pvApiCtx, piAddressVar, piValue))
    {
        Scierror(999, "%s: Wrong size for input argument #%d: A scalar expected.\n", fname, argPos);
        return 1;
    }
    return 0;
}

static int setOutputMatrixDouble(int argPos, double *pdValues, int iRows, int iCols, char *fname)
{
    SciErr sciErr;
    sciErr = createMatrixOfDouble(pvApiCtx, nbInputArgument(pvApiCtx) + argPos, iRows, iCols, pdValues);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return sciErr.iErr;
    }
    AssignOutputVariable(pvApiCtx, argPos) = nbInputArgument(pvApiCtx) + argPos;
    return 0;
}

static int setOutputMatrixInteger(int argPos, int *piValues, int iRows, int iCols, char *fname)
{
    SciErr sciErr;
    sciErr = createMatrixOfInteger32(pvApiCtx, nbInputArgument(pvApiCtx) + argPos, iRows, iCols, piValues);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return sciErr.iErr;
    }
    AssignOutputVariable(pvApiCtx, argPos) = nbInputArgument(pvApiCtx) + argPos;
    return 0;
}

static int setOutputInteger(int argPos, int iValue, char *fname)
{
    if (createScalarInteger32(pvApiCtx, nbInputArgument(pvApiCtx) + argPos, iValue))
    {
        Scierror(999, _("%s: Memory allocation error.\n"), fname);
        return 1;
    }
    AssignOutputVariable(pvApiCtx, argPos) = nbInputArgument(pvApiCtx) + argPos;
    return 0;
}

