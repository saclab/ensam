//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

extern "C"
{
/* ==================================================================== */
  #include "api_scilab.h"
  #include "Scierror.h"
  #include "MALLOC.h"
  #include <localization.h>
  #include "clx_util.h"
/* ==================================================================== */
  int sci_CLx_intern_installed(char *fname, int fname_len)
  {
    CLx_installed();
    return 0;
  }
} /* extern "C" */
/* ==================================================================== */