//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

function builder_gateway()

    gateway_dir = get_absolute_file_path("builder_gateway.sce");
    languages = ["cpp"];

    tbx_builder_gateway_lang(languages, gateway_dir);
    tbx_build_gateway_loader(languages, gateway_dir);
    tbx_build_gateway_clean(languages, gateway_dir);

endfunction

builder_gateway()
clear builder_gateway; // remove builder_gateway on stack
