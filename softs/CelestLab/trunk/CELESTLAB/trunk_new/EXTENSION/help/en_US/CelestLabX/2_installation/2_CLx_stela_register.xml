<?xml version="1.0" encoding="UTF-8"?>
<!--
 Copyright (c) CNES  2008
 
 This software is part of CelestLab, a CNES toolbox for Scilab
 
 This software is governed by the CeCILL license under French law and
 abiding by the rules of distribution of free software.  You can  use,
 modify and/ or redistribute the software under the terms of the CeCILL
 license as circulated by CEA, CNRS and INRIA at the following URL
 'http://www.cecill.info'.
 -->
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:ns5="http://www.w3.org/1999/xhtml" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:id="CLx_stela_register" xml:lang="en">
    <refnamediv>
        <refname>CLx_stela_register</refname>
        <refpurpose>Register Stela installation directory</refpurpose>
    </refnamediv>
    <refsynopsisdiv>
        <title>Calling Sequence</title>
        <synopsis>CLx_stela_register(stela_path)</synopsis>
    </refsynopsisdiv>
    
    <refsection>
        <title>Description</title>
        <para> Registers the STELA installation directory path and stores it 
            in the CelestLabX configuration file. 
        </para>
        
        <para><emphasis role="bold">Notes</emphasis>:        
        <itemizedlist>

        <listitem>
        If the path is empty, STELA is "unregistered" and becomes unavailable.         
        <para/>
        </listitem>

        <listitem>
        The STELA version has to be compatible with the one included in CelestLabX. By default, 
        the first 2 digits of the version numbers must be identical.         
        <para/>
        </listitem>
        
        <listitem>
        If the path was not already registered, Scilab has to be restarted so that the new setting 
        can be taken into account. 
        <para/>
        </listitem>
        
        <listitem>
        A call to CLx_stela_register can be added to the Scilab start-up file (scilab.ini), although that 
        is not mandatory. 
        <para/>
        </listitem>

        <listitem>
        The registered path can be retrieved by: <literal>CLx_setting_get("STELA_ROOT")</literal>. 
        <para/>
        </listitem>

        <listitem>
        STELA can be downloaded from the 
        <ulink url="https://logiciels.cnes.fr/content/stela?language=en"> CNES freeware server</ulink>. 
        <para/>
        </listitem>
        </itemizedlist>
        </para>
        
        </refsection>

    <refsection>
    <title>Arguments</title>
    <variablelist>
    <varlistentry><term>stela_path:</term>
      <listitem><para> (string) STELA installation (absolute) path.</para><para/></listitem></varlistentry>
    </variablelist>
    </refsection>

    <refsection>
        <title>Examples</title>
        <programlisting role="example"><![CDATA[ 
// The following call will generate an error as the path is not a valid STELA path. 
CLx_stela_register("/home/stela/apps/STELA_v2.5.1");
 ]]></programlisting>
    </refsection>
</refentry>
