//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

#ifndef _tle_interf_
#define _tle_interf_

extern "C" {

void CLx_tle_sgp4(int *opt, double *cjd, double *ecc, double *inc,  double *argp, double *raan, double *ma, double *mm, double *bstar, int *nconst, int *nopsmode, double *cjd_eph, int *nb, double *pos, double *vel, double *kep, int *status, int *err);

void CLx_tle_getconst(int *nconst, double *er, double *mu,  double *j2, double *j3, double *j4, int *err);

void CLx_tle_unkozai(double *mm, double *ecc, double *inc, int *nconst, int *nb, double *sma_brw, double *mm_brw, int *err);

}

#endif



