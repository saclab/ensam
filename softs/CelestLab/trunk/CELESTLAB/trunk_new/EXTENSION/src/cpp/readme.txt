---------------------------------------------------
CelestLab - source code contents
---------------------------------------------------

--------------------
sgp4unit.cpp:
sgp4unit.h:

  Original code by D. Vallado: 
       companion code for
       fundamentals of astrodynamics and applications
       2007
       by david vallado

   Current version: July 16, 2012 from AIAA-2006-6753
   Package: AIAA-2006-6753
   http://www.centerforspace.com/downloads/files/sgp4/sgp4Code.zip    

   
--------------------
tle_interf.cpp: 
tle_interf.h:
   Specific CelestLab code: 
   SGP4 interface code for Scilab
   
   
--------------------
tle_util.cpp: 
tle_util.h: 
   Specific CelestLab code: 
   Additional functions used in tle_interf.cpp
   (in fact: not necessarily specific to TLE)

