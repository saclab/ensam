//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.area;

import java.util.List;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import fr.cnes.los.stela.commons.logic.exception.CommonException;
import fr.cnes.los.stela.commons.logic.message.CommonMessage;
import fr.cnes.los.stela.commons.model.mathematics.JavaMathAdapter;
import fr.cnes.los.stela.slib.logic.jibx.SlibXmlBindingHelper;
import fr.cnes.los.stela.slib.model.object.shape.Cuboid;
import fr.cnes.los.stela.slib.model.object.shape.Rectangle;
import fr.cnes.los.stela.slib.model.object.shape.Sphere;
import fr.cnes.los.stela.slib.model.object.shape.StelaShape;
import fr.cnes.los.stela.slib.model.object.shape.Triangle;
import fr.cnes.los.stela.slib.model.object.shape.TriangleCoord;
import fr.cnes.los.stela.slib.model.object.shape.TruncatedCone;
import fr.cnes.los.stela.slib.model.object.shape.TypeShape;
import fr.cnes.los.stela.slib.model.scene.Scene;
import fr.cnes.los.stela.slib.model.scene.SceneBuilder;


// Extension to class Scene 
// => additional features: load, save, center...

public class AreaScene {
	// Stela Scene
    private Scene _scene = SceneBuilder.createNewScene(); 

	// Initialize scene with another scene 
	private void init(Scene scene) {
        
		_scene = SceneBuilder.createNewScene();
		
		for (StelaShape shape: scene.getShapeList()) {
			StelaShape shape2 = (StelaShape) shape.clone(); 
			shape2.setId(shape.getId()); // *** BUG STELA => set id  ***
			_scene.addStelaShape(shape2);
		}
		
	}

	public AreaScene() {
	}
	
	// not public! 
	AreaScene(Scene scene) {
		init(scene);  
	}
		    
	  
    // Returns reference to the scene 
	// (accessible in package only)
	Scene getScene() {
		return _scene; 
	}
	
	// Load scene from XML file
	public AreaInfo load(String fname) 
	{
        AreaInfo info = new AreaInfo(); 

		try {
			// No xml validation yet.
			Scene scene = SlibXmlBindingHelper.load(Scene.class, fname, null);
			init(scene); 

		} catch (final CommonException e) {
			final CommonMessage msg = CommonMessage.fileLoadError(fname);
			info = new AreaInfo(-1, msg.getFullMessage()); 
		}

		return info; 
	}


	// Save scene in XML file
	public AreaInfo save(String fname) 
	{
        AreaInfo info = new AreaInfo(); 
        
		try {
			// No xml validation yet.
			SlibXmlBindingHelper.save(_scene, fname, null);

		} catch (final CommonException e) {
			final CommonMessage msg = CommonMessage.fileSaveError(fname);
			info = new AreaInfo(-1, msg.getFullMessage()); 
		}

		return info; 
	}

	
	// Center scene around (0,0,0) for more accurate results
	// new center = weighted average (weight = shape "radius" + distance to center)
	public void center() 
	{
		Vector3D center = new Vector3D(0,0,0); 
		Vector3D som; 
		double som_w, max_w; 

		for (int iter = 1; iter <= 5; iter++) {
			som = new Vector3D(0,0,0); 
			som_w = 0; 
			max_w = 0; 

			for (StelaShape shape: _scene.getShapeList()) {
				Vector3D p = new Vector3D(shape.getXPosition(), shape.getYPosition(), shape.getZPosition()); 
				Vector3D dp = p.subtract(center); 
				double w = shape.getMaxDimension() + dp.getNorm(); 
				som = som.add(dp.scalarMultiply(w)); // som = som + w*(p-cen)
				som_w =  som_w + w; 
				max_w = Math.max(w, max_w); 
			}
			Vector3D dcenter = som.scalarMultiply(1.0/som_w); 
			center = center.add(dcenter); 
			if (dcenter.getNorm() < 0.1 * max_w) break; 
		}

		// update positions 
		for (StelaShape shape: _scene.getShapeList()) {
			shape.setPosition(shape.getXPosition() - center.getX(), shape.getYPosition() - center.getY(), 
					shape.getZPosition() - center.getZ()); 
		}

	}

    // ---------------------------------
	// getters/setters for shapes
    // ---------------------------------
	

	// Returns all ids
	public String [] getShapeIds() {
		List<StelaShape> shapes = _scene.getShapeList(); 

		String [] id = new String[shapes.size()]; 
		
		for (int k = 0; k < shapes.size(); k++) {
			id[k] = new String(shapes.get(k).getId()); 
		}
		return id; 
	}
	
	
	// Returns all shape types
	public String [] getShapeTypes() {
		List<StelaShape> shapes = _scene.getShapeList(); 

		String [] typ = new String[shapes.size()]; 
		
		for (int k = 0; k < shapes.size(); k++) {
			typ[k] = new String(shapes.get(k).getTypeShape().name()); 
		}
		return typ; 
	}

	
	// Returns all positions
	public double [][] getShapePositions() {
		List<StelaShape> shapes = _scene.getShapeList(); 

		double [][] pos = new double[shapes.size()][3]; 
		
		for (int k = 0; k < shapes.size(); k++) {
			pos[k][0] = shapes.get(k).getXPosition(); 
			pos[k][1] = shapes.get(k).getYPosition(); 
			pos[k][2] = shapes.get(k).getZPosition(); 
		}
		return pos; 
	}

	// Returns all orientations
	public double [][] getShapeOrientations() {
		List<StelaShape> shapes = _scene.getShapeList(); 

		double [][] pos = new double[shapes.size()][3]; 
		
		for (int k = 0; k < shapes.size(); k++) {
			pos[k][0] = shapes.get(k).getXRotation(); 
			pos[k][1] = shapes.get(k).getYRotation(); 
			pos[k][2] = shapes.get(k).getZRotation(); 
		}
		return pos; 
	}
	
	// Returns all angles
	public double [] getShapeAngles() {
		List<StelaShape> shapes = _scene.getShapeList(); 
		// ArrayList<StelaShape> shapes = new ArrayList<StelaShape>(_scene.getShapeListAsArray()); 

		double [] angs = new double[shapes.size()]; 
		
		for (int k = 0; k < shapes.size(); k++) {
			angs[k] = JavaMathAdapter.toRadians(shapes.get(k).getAngleOrientationInDegrees()); 
		}
		return angs; 
	}
	
	// Returns all X axes
	public double [][] getShapeXaxes() {
		List<StelaShape> shapes = _scene.getShapeList(); 

		double [][] X = new double[shapes.size()][3]; 
		
		for (int k = 0; k < shapes.size(); k++) {
			Vector3D orient = new Vector3D(shapes.get(k).getXRotation(), shapes.get(k).getYRotation(), shapes.get(k).getZRotation()); 
			double ang = JavaMathAdapter.toRadians(shapes.get(k).getAngleOrientationInDegrees()); 

			AreaTransform t = new AreaTransform(orient, ang, Vector3D.ZERO); 
			Vector3D Xaxis = t.getXaxis(); 
			X[k][0] = Xaxis.getX(); 
			X[k][1] = Xaxis.getY(); 
			X[k][2] = Xaxis.getZ(); 
		}
		return X; 
	}
	
	// Returns all shape dimensions
	// NB: dimensions same order as in respective creation functions  
	public double [][] getShapeDimensions() {
		List<StelaShape> shapes = _scene.getShapeList(); 

		double [][] dim = new double[shapes.size()][3]; 
		
		for (int k = 0; k < shapes.size(); k++) {
			TypeShape type = shapes.get(k).getTypeShape(); 
			switch (type) {
				case Cuboid: 
					dim[k][0] = ((Cuboid) shapes.get(k)).getLength(); 
					dim[k][1] = ((Cuboid) shapes.get(k)).getHeight();
					dim[k][2] = ((Cuboid) shapes.get(k)).getDepth();
					break;
				case Rectangle: 
					dim[k][0] = ((Rectangle ) shapes.get(k)).getLength(); 
					dim[k][1] = ((Rectangle ) shapes.get(k)).getWidth();
					dim[k][2] = Double.NaN;
					break;
				case Sphere: 
					dim[k][0] = ((Sphere) shapes.get(k)).getRadius(); 
					dim[k][1] = Double.NaN;
					dim[k][2] = Double.NaN;
					break;
				case Triangle: 
					dim[k][0] = ((Triangle) shapes.get(k)).getLengthLeft(); 
					dim[k][1] = ((Triangle) shapes.get(k)).getLengthRight();
					dim[k][2] = ((Triangle) shapes.get(k)).getHeight();
					break;
				case TruncatedCone: 
					dim[k][0] = ((TruncatedCone) shapes.get(k)).getBottomRadius();
					dim[k][1] = ((TruncatedCone) shapes.get(k)).getTopRadius(); 
					dim[k][2] = ((TruncatedCone) shapes.get(k)).getHeight();
					break;
				case TriangleCoord: 
					dim[k][0] = ((TriangleCoord) shapes.get(k)).getLengthLeft(); 
					dim[k][1] = ((TriangleCoord) shapes.get(k)).getLengthRight();
					dim[k][2] = ((TriangleCoord) shapes.get(k)).getHeight();
					break;
				default: 
					dim[k][0] = Double.NaN; 
					dim[k][1] = Double.NaN;
					dim[k][2] = Double.NaN;
					break; 
			}
		}
		
		return dim; 
	}


	// Initializes scene with all description data
	public AreaInfo setShapes(String ids[], String types[], double [][] dims) {
		
        AreaInfo info = new AreaInfo(); 

		if (ids.length != types.length ||
			ids.length != dims.length) {
			info = new AreaInfo(-1, "Inconsistent argument sizes"); 
			return info; 
		}

		Scene scene = SceneBuilder.createNewScene();
				
		try {

			for (int k = 0; k < types.length; k++) {
				TypeShape type = TypeShape.valueOf(types[k]); 
				StelaShape shape; 
				
				switch (type) {
					case Cuboid: 
						shape = new Cuboid(dims[k][0], dims[k][1], dims[k][2]); 
						break;
					case Rectangle: 
						shape = new Rectangle(dims[k][0], dims[k][1]); 
						break;
					case Sphere: 
						shape = new Sphere(dims[k][0]); 
						break;
					case Triangle: 
						shape = new Triangle(dims[k][0], dims[k][1], dims[k][2]); 
						break;
					case TruncatedCone: 
						shape = new TruncatedCone(dims[k][0], dims[k][1], dims[k][2]); 
						break;
					case TriangleCoord: 
						// becomes a triangle ! 
						shape = new Triangle(dims[k][0], dims[k][1], dims[k][2]); 
						break;
					default: 
						throw new Exception();  // no info, but does not matter
				}
				
				shape.setId(ids[k]);
				scene.addStelaShape(shape);
			}
		}
		catch (Exception e) {
			info = new AreaInfo(-1, "Invalid scene description"); 
			return info; 
		}
		
		init(scene); 
		return info; 
	}

	
	// Initializes orientations andf positions - 1st way
	public AreaInfo setShapeFrames(double [][] orientations, double [] angles, double [][] positions) {
		
        AreaInfo info = new AreaInfo(); 
		List<StelaShape> shapes = _scene.getShapeList(); 

		if (shapes.size() != orientations.length ||
			shapes.size() != angles.length ||
			shapes.size() != positions.length) {
			info = new AreaInfo(-1, "Inconsistent argument sizes"); 
			return info; 
		}
				
		for (int k = 0; k < orientations.length; k++) {
			shapes.get(k).setRotationInDegrees(orientations[k][0], orientations[k][1], orientations[k][2], JavaMathAdapter.toDegrees(angles[k]));
			shapes.get(k).setPosition(positions[k][0], positions[k][1], positions[k][2]);
		}
		
		return info; 
	}

	// Initializes orientations - 2nd way: X axis instead of angle
	public AreaInfo setShapeFrames(double [][] orientations, double [][] X, double [][] positions) {
		
        AreaInfo info = new AreaInfo(); 
		List<StelaShape> shapes = _scene.getShapeList(); 

		if (shapes.size() != orientations.length ||
			shapes.size() != X.length ||
			shapes.size() != positions.length) {
			info = new AreaInfo(-1, "Inconsistent argument sizes"); 
			return info; 
		}
				
		for (int k = 0; k < orientations.length; k++) {
			AreaTransform t = new AreaTransform(new Vector3D(orientations[k]), new Vector3D(X[k]), Vector3D.ZERO); 
			double ang = t.getAngle(); 
			shapes.get(k).setRotationInDegrees(orientations[k][0], orientations[k][1], orientations[k][2], JavaMathAdapter.toDegrees(ang));
			shapes.get(k).setPosition(positions[k][0], positions[k][1], positions[k][2]);
		}
		
		return info; 
	}

}
