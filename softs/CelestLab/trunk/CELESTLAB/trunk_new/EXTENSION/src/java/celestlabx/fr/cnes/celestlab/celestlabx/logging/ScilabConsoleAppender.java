//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

package fr.cnes.celestlab.celestlabx.logging;

import org.scilab.modules.gui.console.ScilabConsole;
import org.scilab.modules.gui.console.Console;

import org.apache.log4j.Logger;
import org.apache.log4j.AppenderSkeleton;
import org.apache.log4j.spi.LoggingEvent;

/**
 * Class ScilabConsoleAppender
 * Overrides LOG4J appender to display logs into Scilab Console
 */
public class ScilabConsoleAppender extends AppenderSkeleton
{
    private static ScilabConsoleAppender instance;

    private static final String LOG_SOURCE_CLX = "CLx";
    private static final String LOG_SOURCE_STELA = "STELA";

    /**
     * Get the singleton instance of the {@link ScilabConsoleAppender} logger.
     * @return a {@link ScilabConsoleAppender} logger.
     */
    public static ScilabConsoleAppender getInstance()
    {
        if (instance == null)
        {
            instance = new ScilabConsoleAppender();
            instance.setName("ScilabConsoleAppender");
        }
        return instance;
    }

    /**
     * Requires a layout (log message format): in our case no log layout is required
     */
    @Override
    public boolean requiresLayout()
    {
        return false;
    }

    /**
     * Appends a new log: in our case, adds a log to the Scilab Console
     */
    @Override
    public void append(LoggingEvent loggingEvent)
    {
        Console console = ScilabConsole.getConsole();
        if (console == null)
            return;

        // Adds the log if it comes from CelestLabX, if it is a STELA log adds it if STELA logging activated
        String source = getSource(loggingEvent);
        if ((source == LOG_SOURCE_CLX) || (LoggingConfigurator.getLogStela()))
        {
            // Be sure the message level is grater than the configured log level
            if (loggingEvent.getLevel().isGreaterOrEqual(Logger.getRootLogger().getLevel()))
            {
                String s = String.format("%s - %s: %s", source, loggingEvent.getLevel().toString(),
                    loggingEvent.getMessage());
                console.display(s + System.getProperty("line.separator"));
            }
        }
    }

    /**
     * Close the appender: in our case, Scilab Console stays opened
     */
    @Override
    public void close() {}

    /**
     * Returns the source of the log: CelestLabX or STELA
     * @return the source {@link java.lang.String}
     */
    private String getSource(LoggingEvent loggingEvent)
    {
        if (loggingEvent.getLoggerName().startsWith("fr.cnes.celestlab"))
            return LOG_SOURCE_CLX;
        else
            return LOG_SOURCE_STELA;
    }
}
