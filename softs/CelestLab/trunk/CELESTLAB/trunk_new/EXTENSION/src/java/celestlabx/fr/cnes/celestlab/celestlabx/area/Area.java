//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.area;

import java.util.ArrayList;

import org.apache.commons.math3.geometry.euclidean.threed.Vector3D;

import fr.cnes.los.stela.commons.model.mathematics.JavaMathAdapter;
import fr.cnes.los.stela.slib.gui.business.compute.PerformController;
import fr.cnes.los.stela.slib.model.object.shape.StelaShape;
import fr.cnes.los.stela.slib.model.scene.OrientationModel;
import fr.cnes.los.stela.slib.model.scene.Scene;

 
// Class that encapsulates area computation
public class Area 
{

	// structure describing the scene (with addition features: save, load...) 
	private AreaScene _areascene = new AreaScene(); 

	// Resolution
	private int _resolution = 10000; 

	// Computed areas
	private double [] _area = new double[0];  

	// Save prefix ("" => nothing saved)
	private String _savePrefix = ""; 

	// Lists of shapes with specific ids 
	// (used when applying specific rotations)
	private ArrayList<StelaShape> l_shp; 
	private ArrayList<StelaShape> l_shp_ref; 


	// info (error) message
	private String msg = ""; 
	
	
	// Initializes rotation of subparts
	// Create list of shapes with specified ids 
	// + create list of copies (containing initial values)
	// return 0 if OK. 
	private int initRotId(String[] ids) {
		l_shp = new ArrayList<StelaShape>();    
		l_shp_ref = new ArrayList<StelaShape>();      
		
		// Stela Scene
		Scene scene = _areascene.getScene(); 

		for (String id: ids) {
			boolean found = false; 
			for (StelaShape shape: scene.getShapeList()) {
				if (shape.getId().equals(id)) {
					l_shp.add(shape); 
					l_shp_ref.add((StelaShape) shape.clone()); 
					found = true;
					break; 
				}
			}

			// id not found
			if (!found) {
				msg = "Cannot find shape with id: " + id; 
				return -1; 
			}
		}

		return 0; 
	}


	// Apply additional rotations to shapes with specific ids (see initRotId)
	// Take initial values (orientation,position,angle) from reference shape
	// Modify shape with new values
	// NB: initRotId must have been called
	// rotaxis: double[N][3]
	// rotang: double[N]
	// rotcen: double[N][3]
	private void setRotId(double[][] rotaxis, double[] rotang, double[][] rotcen) 
	{

		int nb = l_shp.size(); 
		StelaShape shape; 
		StelaShape refshape; 

		for (int k = 0; k < nb; k++) {
			shape = l_shp.get(k); 
			refshape = l_shp_ref.get(k); 

			double rx = refshape.getXRotation(); 
			double ry = refshape.getYRotation(); 
			double rz = refshape.getZRotation(); 
			double ang = JavaMathAdapter.toRadians(refshape.getAngleOrientationInDegrees()); 

			double px = refshape.getXPosition(); 
			double py = refshape.getYPosition(); 
			double pz = refshape.getZPosition(); 

			// Apply rot
			AreaTransform t = new AreaTransform(new Vector3D(rx,ry,rz), ang, new Vector3D(px,py,pz)); 
			AreaTransform t2 = t.applyRot(new Vector3D(rotaxis[k]), rotang[k], new Vector3D(rotcen[k])); 

			Vector3D orient2 = t2.getOrientation(); 
			double ang2 = t2.getAngle(); 
			Vector3D position2 = t2.getPosition(); 

			shape.setRotationInDegrees(orient2.getX(), orient2.getY(), orient2.getZ(), JavaMathAdapter.toDegrees(ang2)); 
			shape.setPosition(position2.getX(), position2.getY(), position2.getZ());

		}
	}

	
	// Set prefix for saving XML files (relative or absolute path)
	public void setSavePrefix(String prefix) 
	{
		_savePrefix = new String(prefix); 
	}


	// Set resolution
	public void setResolution(int resolution) 
	{
		_resolution = resolution; 
	}


	// Returns computed mean area
	public double [] getValue() 
	{
		return _area.clone(); 
	}

    // Load scene from XML file
	public AreaInfo load(String fname) 
	{
		return _areascene.load(fname); 
	}

	// Save scene in XML file
	public AreaInfo save(String fname) 
	{
		return _areascene.save(fname); 
	}

	// Initialize the scene with another scene  
	public void setScene(AreaScene ascene) 
	{
		_areascene = new AreaScene(ascene.getScene()); 
	}
	
	public void centerScene() 
	{
		_areascene.center(); 
	}


	// Compute mean area in tumbling mode
	// nbp: number of planes
	// => returns 0 if OK.
	public AreaInfo computeTumbling(int nbp) 
	{
		AreaInfo info = new AreaInfo(); 
		
		// compute mean area
		PerformController feedee = new PerformController(null);

		// Stela Scene
		Scene scene = _areascene.getScene(); 

		// set parameters
		scene.setOrientationModel(OrientationModel.RAND_MODEL);
		scene.setNumberOfPlanes(nbp);
		scene.setResolution(_resolution);

		// Mean area
		_area = new double[1]; 
		_area[0] = scene.computeMeanArea(feedee); 
		
		if (! _savePrefix.isEmpty()) {
			String num = String.format("%04d", 1); 
			info = save(_savePrefix + "_" + num + "_shap.xml"); 
		}
		
		return info; 
	}


	// Compute mean area in spinning mode around for specified axis
	// dirs: double[N][3]
	// axes: double[N][3]
	// nbp: number of planes
	// => returns 0 if OK.	
	public AreaInfo computeSpinning(double [][] dirs, double [][] axes, int nbp) 
	{
		AreaInfo info = new AreaInfo(); 

		// compute mean area
		PerformController feedee = new PerformController(null);
		
		// Stela Scene
		Scene scene = _areascene.getScene(); 

		// set parameters
		scene.setOrientationModel(OrientationModel.SPIN_MODEL);
		scene.setNumberOfPlanes(nbp);
		scene.setResolution(_resolution);

		int n = axes.length; 

		_area = new double[n]; 

		for (int k = 0; k < n; k++) { 
			scene.setRotX(axes[k][0]);
			scene.setRotY(axes[k][1]);
			scene.setRotZ(axes[k][2]);

			scene.setDragX(dirs[k][0]);
			scene.setDragY(dirs[k][1]);
			scene.setDragZ(dirs[k][2]);

			_area[k] = scene.computeMeanArea(feedee);
			
			if (! _savePrefix.isEmpty()) {
				String num = String.format("%04d", k+1); 
				info = save(_savePrefix + "_" + num + "_shap.xml");  
				if (info.getStatus() != 0) break; 
			}

		}

		return info; 
	}


	// Compute mean area in direction "dirs" 
	// dir: double[N][3] : projection direction
	// => returns 0 if OK.	
	public AreaInfo computeFixedDir(double [][] dirs) 
	{
		AreaInfo info = new AreaInfo(); 

		// compute mean area
		PerformController feedee = new PerformController(null);

		// Stela Scene
		Scene scene = _areascene.getScene(); 

		// set parameters
		scene.setOrientationModel(OrientationModel.FIXD_MODEL);
		scene.setNumberOfPlanes(1);
		scene.setResolution(_resolution);

		int n = dirs.length; 

		_area = new double[n]; 

		for (int k = 0; k < n; k++) {
			scene.setDragX(dirs[k][0]);
			scene.setDragY(dirs[k][1]);
			scene.setDragZ(dirs[k][2]);

			_area[k] = scene.computeMeanArea(feedee);
			
			if (! _savePrefix.isEmpty()) {
				String num = String.format("%04d", k+1); 
				info = save(_savePrefix + "_" + num + "_shap.xml");  
				if (info.getStatus() != 0) break; 
			}

		}

		return info; 
	}


	// Computes mean area in direction "dirs", rotating elements "ids" an angle "ang" around axes
	// dir: double[N][3] : projection direction
	// ids: String[P] : ids for specific rotations
	// axes[N*P,3]  (for id1, then id2, then... idP) : rot axes 
	// angs[N*P]  (for id1, then id2, then... idP) : rot angles
	// cens[N*P,3]  (for id1, then id2, then... idP) : rot delta centers
	// => returns 0 if OK.	
	public AreaInfo computeFixedDirRot(double [][] dirs, String [] ids, double [][] axes, 
			double [] angs, double [][] cens) 
	{
		AreaInfo info = new AreaInfo(); 

		// compute mean area
		PerformController feedee = new PerformController(null);

		// Stela Scene
		Scene scene = _areascene.getScene(); 

		// set parameters
		scene.setOrientationModel(OrientationModel.FIXD_MODEL);
		scene.setNumberOfPlanes(1);
		scene.setResolution(_resolution);


		// create list of shapes with specified ids 
		if (initRotId(ids) != 0) {
			info = new AreaInfo(-1, msg); 
			return info; 
		}

		int nbid = ids.length; 
		double [][] axes_k = new double[nbid][]; 
		double [] angs_k = new double[nbid]; 
		double [][] cens_k = new double[nbid][]; 

		int n = dirs.length; 
		_area = new double[n]; 

		for (int k = 0; k < n; k++) { 

			// projection direction
			scene.setDragX(dirs[k][0]);
			scene.setDragY(dirs[k][1]);
			scene.setDragZ(dirs[k][2]);

			// apply specific (additional) rotation to shape
			for (int i = 0; i < nbid; i++) {
				axes_k[i] = axes[n*i+k]; 
				angs_k[i] = angs[n*i+k]; 
				cens_k[i] = cens[n*i+k]; 
			}

			setRotId(axes_k, angs_k, cens_k); 

			// Mean area
			_area[k] = scene.computeMeanArea(feedee); 

			if (! _savePrefix.isEmpty()) {
				String num = String.format("%04d", k+1); 
				info = save(_savePrefix + "_" + num + "_shap.xml");
				if (info.getStatus() != 0) break; 
			}
		}

		return info; 
	}

}



