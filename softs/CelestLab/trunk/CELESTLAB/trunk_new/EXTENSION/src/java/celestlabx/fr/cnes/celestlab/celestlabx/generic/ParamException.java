//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.
//

package fr.cnes.celestlab.celestlabx.generic;

/**
 * Class ParamException
 * For exceptions that occur during Params manipulation
 */
@SuppressWarnings("serial")
public class ParamException extends Exception
{
    /**
     * Constructor
     * @param paramName name of parameter source of exception
     * @param message exception message
     */
    public ParamException(String paramName, String message)
    {
        super(String.format("Parameter %s : %s", paramName, message));
    }
}
