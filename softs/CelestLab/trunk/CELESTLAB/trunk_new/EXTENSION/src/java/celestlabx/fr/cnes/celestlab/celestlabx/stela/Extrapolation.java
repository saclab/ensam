//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.stela;

import fr.cnes.celestlab.celestlabx.generic.Params;
import fr.cnes.los.stela.commons.logic.date.StelaDate;
import fr.cnes.los.stela.commons.model.ephemeris.Bulletin;
import fr.cnes.los.stela.commons.model.ephemeris.FrameTypes;
import fr.cnes.los.stela.commons.model.ephemeris.NatureTypes;
import fr.cnes.los.stela.commons.model.ephemeris.Type8PosVel;
import fr.cnes.los.stela.elib.business.framework.simulation.IExtrapolationFeedback;
import fr.cnes.los.stela.elib.business.implementation.simulation.gtosimulation.GTOSimulation;

import org.apache.log4j.Logger;


/**
 * Class Extrapolation
 * This class provides an interface with Scilab for Stela orbital propagation
 */
public class Extrapolation
{
	private static final Logger logger = Logger.getLogger(Extrapolation.class);

	private Simulation simulation; // simulation context
	private ExtrapolationResults extrapolationResults; // results

	/**
	 * Constructor
	 */
	public Extrapolation()
	{
		simulation = new Simulation();
	}

	/**
	 * This method extrapolates a satellite state vector at given time steps and parameters.
	 * @param cjd0: initial epoch, (CJD, TREF time scale)
	 * @param x0: initial orbital elements position ("cireq" type, CelestLab convention)
	 * @param delta_t: times from cjd0 where the results are computed (s)
	 * @param params: structure containing the model parameters
	 * @param res if 1, the transition matrix is computed (0 otherwise)
	 * @param ut1_tref: UT1 minus TREF (s)
	 * @param tt_tref: TT minus TREF (s)
	 * @return info: status of extrapolation
	 * @throws Exception
	 */
	
	public Information extrapolate(double cjd0, double[] x0, double[] delta_t, Params params,
			int res, double ut1_tref, double tt_tref) {

    Information info = _extrapolate(cjd0, x0, delta_t, params, res, ut1_tref, tt_tref); 
    simulation.clean();
    return info; 
		
	}

	// same interface - low level	
	private Information _extrapolate(double cjd0, double[] x0, double[] delta_t, Params params,
			int res, double ut1_tref, double tt_tref) 
	{
		logger.debug("Entering extrapolate function");

		// Initialise simulation context
		Information informations = simulation.initialize(params, ut1_tref, tt_tref);
		
		if (informations.getStatus() != 0)
			return informations;

		if (params == null)
			return new Information(simulation, -1, "Parameters object is null");

		// Check we have time steps
		if (delta_t == null || delta_t.length < 1)
			return new Information(simulation, -1, "Time steps array is empty or null");

		// Check time steps are positive values
		for (double timeStep: delta_t)
		{
			if (timeStep < 0)
				return new Information(simulation, -1, "One or more time steps are negative");
		}

		// Check integration step is strictly positive 
		double integratorStep = simulation.getGTOSimulation().getIntegratorStep(); 

		if (integratorStep <= 0)
			return new Information(simulation, -1, "Integration step not positive");

		// Check time steps are multiples of integration steps
		// (required accuracy = microsecond)
		for (int i = 0; i < delta_t.length; i++) {
			long k = Math.round(delta_t[i] / integratorStep); 
			if (Math.abs(delta_t[i] - k * integratorStep) >= 1.e-6)
				return new Information(simulation, -1, "Time steps not multiple of integration step");
		}

		// Check time steps are increasing
		if (delta_t.length > 1)
		{
			for (int i = 1; i < delta_t.length; i++) {
				if (delta_t[i] <= delta_t[i-1])
					return new Information(simulation, -1, "Time steps not increasing");
			}
		}

		// centralEnabled must be true (always included in propagation)
		if (! simulation.getCentralEnabled()) {
			return new Information(simulation, -1, "Central body must be taken into account");
		}

		// Set simulation initial state (type8, mean elements, CIRF)
		Bulletin bulletin_init = makeBulletin(cjd0, x0); 
		simulation.setInitialState(bulletin_init);

		// Set simulation duration
		// margin included : difference between years of 365.25 days and 
		// years from same calendar dates + additional margin (0.5 days) 
		// because of the special STELA way of counting years
		double simulationDuration = delta_t[delta_t.length-1] / 86400;
		simulationDuration += 2.5 + Math.floor(simulationDuration/36525);
		logger.debug(String.format("Set simulation duration: %s (days)", simulationDuration));
		simulation.getGTOSimulation().setSimulationDuration(simulationDuration / 365.25); 
		
		// Run simulation
		logger.info("Starting extrapolation...");

		// default information object returned (no results in XML string)
		informations = new Information(simulation, 0, "");       

		boolean computeTransitionMatrix = (res == 1 ? true : false);

		Information info = simulate(computeTransitionMatrix, false,
				new ExtrapolationFeedback(), delta_t[delta_t.length-1]);

		if (info.getStatus() != 0) {
			// generate information object with already generated XML data
			informations = new Information(info.getStatus(), info.getErrorMessage(),
					informations.getSimulationXML()); 	
		}

		logger.info("Extrapolation finished.");

		extrapolationResults = new ExtrapolationResults(simulation, delta_t);
		extrapolationResults.computeParamMatrices();

		return informations;
	}

		
	/**
	 * Returns the extrapolation results
	 * @return the Extrapolation results
	 */
	public ExtrapolationResults getExtrapolationResults()
	{
		return extrapolationResults;
	}
	
	/**
	 * Runs the simulation
	 * @param transitionMatrix if true, transition matrix is computed
	 * @param timeDerivatives if true, derivatives are computed
	 * @param feedback for feedback on extrapolation
	 * @param last_time propagation time wanted (s)
	 * @return the informations about the simulation
	 */
	private Information simulate(boolean transitionMatrix, boolean timeDerivatives,
			IExtrapolationFeedback feedback, double last_time)
	{
		int status = 0;
		String errMsg = "";
		GTOSimulation gtosim = simulation.getGTOSimulation(); 

		// Transition matrix
		logger.debug(String.format("Transition matrix flag: %s", transitionMatrix));
		gtosim.setTransitionMatrixFlag(transitionMatrix);

		// Derivatives
		gtosim.setTimeDerivativesFlag(timeDerivatives);

		try
		{
			// Run simulation
			if (!gtosim.extrapolate(feedback))
			{
				Bulletin initialState = gtosim.getEphemerisManager().getInitialState();
				double cjd0 = initialState.getDate().getCnesJulianDate();

				Bulletin finalState = gtosim.getEphemerisManager().getFinalState();
				double cjdf = finalState.getDate().getCnesJulianDate();

				logger.debug(String.format("Simulation effective duration: %s (days)", cjdf - cjd0));

				// "decayed" status if last state <= last time
				if ((cjdf - cjd0)*86400 < last_time)
					status = 1;
				else
					status = 0;
			}
			else
			{
				status = -1;
				errMsg = "Abnormal termination of extrapolation";
			}
		}
		catch (Exception e)
		{
			status = -1;
			errMsg = e.getMessage();
			logger.error(e.getMessage());
		}

		return new Information(simulation, status, errMsg);
	}

	/**
	 * Make a "Bulletin" from date and position/velocity in CIRF
	 * @param cjd : date (TREF time scale)
	 * @param x : orbital elements ("cireq" type, CelestLab convention, frame = CIRF)
	 * @param nature: "MEAN" 
	 * @return the Bulletin
	 * NB: time scale: converted to UT1 (Stela convention)
	 */
	private Bulletin makeBulletin(double cjd, double[] x)
	{
		double ut1_tref = simulation.getUT1minusTREF(); 
		// Date is set to UT1 time scale
		StelaDate stelaDate = new StelaDate(cjd + ut1_tref / 86400);

		Type8PosVel param = new Type8PosVel(x[0], x[5], x[1], x[2], x[3], x[4],
				NatureTypes.MEAN, FrameTypes.CIRF);

		return new Bulletin(stelaDate, param);
	}

}
