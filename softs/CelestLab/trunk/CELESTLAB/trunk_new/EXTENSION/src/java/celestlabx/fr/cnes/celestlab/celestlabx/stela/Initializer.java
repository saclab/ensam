//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

package fr.cnes.celestlab.celestlabx.stela;

import fr.cnes.celestlab.celestlabx.logging.LoggingConfigurator;
import fr.cnes.celestlab.celestlabx.logging.ScilabConsoleAppender;

import fr.cnes.los.stela.commons.logic.log.business.StelaLoggerAgent;
import fr.cnes.los.stela.commons.logic.log.service.LoggerAgentFactory;
import fr.cnes.los.stela.commons.logic.log.service.LoggerException;
import fr.cnes.los.stela.commons.model.Prop;

import org.scilab.modules.core.Scilab;

import org.apache.log4j.Logger;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.PatternLayout;
import org.apache.log4j.Level;

/**
 * Class Initializer
 * This class is used to initialize the STELA library and CelestLabX logs
 */
public class Initializer
{
    private static final Logger logger = Logger.getLogger(Initializer.class);

    /*
     * Inits the CelestLabX logger : binds the log to the Scilab console
     */
    public static void initLog(String logVerbosity, boolean logStela, String logFile)
    {
        // In GUI mode bind logger to the Scilab console otherwise use the standard console
        if (Scilab.getMode() == 2)
        {
            Logger rootLogger = Logger.getRootLogger();
            ScilabConsoleAppender scilabConsoleAppender = ScilabConsoleAppender.getInstance();
            if (!rootLogger.isAttached(scilabConsoleAppender))
                rootLogger.addAppender(scilabConsoleAppender);
        }
        else
            addConsoleAppender();

        // Configure the log system with the given arguments
        LoggingConfigurator.setLogVerbosity(logVerbosity);
        LoggingConfigurator.setLogStela(logStela);
        LoggingConfigurator.setLogFile(logFile);
    }

    /**
     * Initializes the STELA library (activates the STELA logger, loads the translation messages, etc...)
     * @param StelaRoot: the root path of STELA
     * @throws Exception
     */
    public static void initStela(String StelaRoot) throws Exception
    {
        // Ignore STELA useless info logs at startup
        Level originalLevel = Logger.getRootLogger().getLevel();
        Logger.getRootLogger().setLevel(Level.ERROR);
        try
        {
            Prop.defineROOT(StelaRoot);

            Prop.defineLANG("en");

            InitStelaLogger();
        }
        finally
        {
            Logger.getRootLogger().setLevel(originalLevel);
        }
    }

    /**
     * Initializes the STELA logger agent
     * @throws LoggerException
     */
    private static void InitStelaLogger() throws LoggerException
    {
        try
        {
            StelaLoggerAgent agent = (StelaLoggerAgent) LoggerAgentFactory.getLoggerAgent();
            agent.shutdown();
            agent.init("stela_service_log_config.xml");
            agent.newLogger("StelaMainLog");
        }
        catch (LoggerException e)
        {
            logger.error("Cannot initialize STELA logger: " + e.getMessage());
            throw e;
        }
    }

    /**
     * Adds a Log4j Console Appender
     */
    private static void addConsoleAppender()
    {
        PatternLayout layout = new PatternLayout("%c - %-5p: %m%n");
        Logger.getRootLogger().addAppender(new ConsoleAppender(layout));
    }
}

