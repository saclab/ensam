// ------------------------------
// CL_man_thrustDuration
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.type_output = "dt";
inputs.dm = [283.09409796389849,339.67443764582049];
inputs.F = 0.14e5;
inputs.isp = 0.45e3;

// Outputs
outputs = struct();
outputs.dt = [89.235152222067796,107.07005326948024];

TEST_OK = [];

//=========================
// Compute output
//=========================
[dt] = CL_man_thrustDuration(inputs.type_output,inputs.dm,inputs.F,inputs.isp);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(dt, outputs.dt, relative = %t, compar_crit = "element", prec = 1e-014);
