// ------------------------------
// CL_ex_meanEckHech
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
osc1 = [42166712,-7.900e-02,1.100e-02,1.1,2.2,5.300]';  // lesser precision (0.1>e>5e-3)
osc2 = [42166712,-0.900e-03,1.100e-03,0.2,2.3,5.300]';
inputs.osc = [osc1,osc2];
inputs.er = 6378136.3;
inputs.mu = 3.986004415e14;
inputs.j1jn = [0.
          0.00108262669059782
          -2.53243547943241e-06
          -1.61933127174051e-06
          -2.27716112895682e-07
          5.39648498594285e-07];

// Outputs
outputs = struct();
outputs.mean_cir = [[42166807.511226058;-0.078986393918240638;0.01099807076877321;1.1000022443413358;2.1980476218359413;5.301027797348004],..
[42166739.794569895;-0.00091868718732910943;0.001128896297233551;0.2000015783608908;2.3000175010142256;..
5.299984151077056]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[mean_cir] = CL_ex_meanEckHech(inputs.osc,inputs.er,inputs.mu,inputs.j1jn);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(mean_cir, outputs.mean_cir, relative = %t, compar_crit = "element", prec = 1e-014);
