// ------------------------------
// CL_man_apsidesLine
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.ai = 0.7e7;
inputs.ei = 0.3e-1;
inputs.pomi = CL_deg2rad(0.9e2);
inputs.pomf = CL_deg2rad(0.85e2);
inputs.posman = 0;

// Outputs
outputs = struct();
outputs.dv = [0;0;19.758146452324759];
outputs.anv = [6.239552075879728];

TEST_OK = [];

//=========================
// Compute output
//=========================
[dv,anv] = CL_man_apsidesLine(inputs.ai,inputs.ei,inputs.pomi,inputs.pomf,inputs.posman);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(dv, outputs.dv, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(anv, outputs.anv, relative = %t, compar_crit = "element", prec = 1e-014);
