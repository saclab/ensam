// ------------------------------
// CL_man_inclination
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.a1 = 0.7e7;
inputs.e1 = 0.3e-1;
inputs.inc1 = CL_deg2rad(0.3e2);
inputs.pom1 = CL_deg2rad(0.9e2);
inputs.inc2 = CL_deg2rad(0.35e2);
inputs.posman = 1;

// Outputs
outputs = struct();
outputs.dv = [4.7123889803846897;1.5271630954950384;658.60488174415957];
outputs.anv = [4.7123889803846897];

TEST_OK = [];

//=========================
// Compute output
//=========================
[dv,anv] = CL_man_inclination(inputs.a1,inputs.e1,inputs.inc1,inputs.pom1,inputs.inc2,inputs.posman);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(dv, outputs.dv, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(anv, outputs.anv, relative = %t, compar_crit = "element", prec = 1e-014);
