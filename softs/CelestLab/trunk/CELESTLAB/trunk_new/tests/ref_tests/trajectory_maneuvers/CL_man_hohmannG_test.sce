// ------------------------------
// CL_man_hohmannG
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.ai = 7000.e3;
inputs.ei = 0.01;
inputs.af = 7200.e3;
inputs.ef = 0.015;
inputs.posman1 = 1;
inputs.rotation = 1;

// Outputs
outputs = struct();
outputs.delta_v = [105.308226547566846];
outputs.dv1 = [1.5707963267948966;0;99.5274917926126363];
outputs.dv2 = [1.5707963267948966;0;5.78073475495421008];
outputs.anv1 = [3.1415926535897931];
outputs.anv2 = [3.1415926535897931];

TEST_OK = [];

//=========================
// Compute output
//=========================
[delta_v,dv1,dv2,anv1,anv2] = CL_man_hohmannG(inputs.ai,inputs.ei,inputs.af,inputs.ef,inputs.posman1,inputs.rotation);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(delta_v, outputs.delta_v, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(dv1, outputs.dv1, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(dv2, outputs.dv2, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(anv1, outputs.anv1, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(anv2, outputs.anv2, relative = %t, compar_crit = "element", prec = 1e-014);
