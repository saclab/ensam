// ------------------------------
// CL_man_hohmann
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.ai = 7000000;
inputs.af = 7200000;

// Outputs
outputs = struct();
outputs.delta_v = [105.53917014472518];
outputs.dv1 = [1.5707963267948966;0;52.955409598505867];
outputs.dv2 = [1.5707963267948966;0;52.58376054621931];
outputs.anv1 = [0];
outputs.anv2 = [3.1415926535897931];

TEST_OK = [];

//=========================
// Compute output
//=========================
[delta_v,dv1,dv2,anv1,anv2] = CL_man_hohmann(inputs.ai,inputs.af);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(delta_v, outputs.delta_v, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(dv1, outputs.dv1, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(dv2, outputs.dv2, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(anv1, outputs.anv1, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(anv2, outputs.anv2, relative = %t, compar_crit = "element", prec = 1e-014);
