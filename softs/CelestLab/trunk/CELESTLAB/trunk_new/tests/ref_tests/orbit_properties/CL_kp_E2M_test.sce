// ------------------------------
// CL_kp_E2M
// validation:
// Orbital Mechanics for Engineering Students, H D Curtis, Chapter 3, example 3.1
// ------------------------------

// Inputs
inputs = struct();
inputs.E = [1.7281 0.4735499e-1];
inputs.e = [0.37255 1.158];

// Outputs
outputs = struct();
outputs.M = [0.28624997455264417,1.1146227061961482];

TEST_OK = [];

//=========================
// Compute output
//=========================
[M] = CL_kp_E2M(inputs.E,inputs.e);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M, outputs.M, relative = %t, compar_crit = "element", prec = 1e-014);
