// ------------------------------
// CL_op_rpvinf2ae
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.rp = 7995000.5;
inputs.vinf = 2000;

// Outputs
outputs = struct();
outputs.sma = [99650110.375];
outputs.ecc = [1.0802307239792659];

TEST_OK = [];

//=========================
// Compute output
//=========================
[sma,ecc] = CL_op_rpvinf2ae(inputs.rp,inputs.vinf);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(sma, outputs.sma, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(ecc, outputs.ecc, relative = %t, compar_crit = "element", prec = 1e-014);
