// ------------------------------
// CL_kp_M2v
// validation:
// ---------
// ------------------------------

// Inputs
inputs = struct();
inputs.ecc = 0.1;
inputs.M = 0.2;

// Outputs
outputs = struct();
outputs.v = [0.24522926541671447];

TEST_OK = [];

//=========================
// Compute output
//=========================
[v] = CL_kp_M2v(inputs.ecc,inputs.M);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(v, outputs.v, relative = %t, compar_crit = "element", prec = 1e-014);
