// ========================================================================== //
// TEST ms_regPolHarm
// ========================================================================== //

// Inputs
inputs = struct();
inputs.mltan = 14;
inputs.cjd0 = CL_dat_cal2cjd(2010,1,1);
inputs.cjdf = CL_dat_cal2cjd(2011,1,1);
inputs.kep0 = [7266456.9; 0; 1.7275628; 0; 5.4010372; 0];

// Outputs
outputs = struct();
outputs.didt = [2.29342373798e-11];

TEST_OK = [];

//=========================
// Compute output
//=========================

cjd = linspace(inputs.cjd0, inputs.cjdf, 101);
kep = CL_ex_propagate("j2sec", "kep", inputs.cjd0, inputs.kep0, cjd, "m");
pos_sun = CL_eph_sun(cjd);
didt = CL_op_incdotSsoCirc(inputs.mltan, kep(1,:), kep(3,:));

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(didt, outputs.didt*ones(didt), relative = %t, compar_crit = "element", prec = 1e-013);
