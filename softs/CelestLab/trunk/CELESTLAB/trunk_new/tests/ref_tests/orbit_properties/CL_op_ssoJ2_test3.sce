// ------------------------------
// CL_op_ssoJ2_3
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.par_calc='e'
inputs.par1=7031.7216e3	//semi major axis
inputs.par2=CL_deg2rad(98)	//inclination

// Optional inputs (names of arguments have to strictly match names of function arguments !)
inputs_opt = struct();
inputs_opt.er = 6378.1363e3;
inputs_opt.j2 = 0.00108262669059782;
inputs_opt.rotr_pla_sun = CL_deg2rad(0.9856091185226477137241974226)/86400;

// Outputs
outputs = struct();
outputs.par3 = [0.0016633119218289055];

TEST_OK = [];

//=========================
// Compute output
//=========================
[par3] = CL_op_ssoJ2(inputs.par_calc,inputs.par1,inputs.par2,er=inputs_opt.er,j2=inputs_opt.j2,rotr_pla_sun=inputs_opt.rotr_pla_sun);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(par3, outputs.par3, relative = %t, compar_crit = "element", prec = 1e-014);
