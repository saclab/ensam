// ------------------------------
// CL_op_rarp2ae
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.ra = 8008e3;
inputs.rp = 7992e3;

// Outputs
outputs = struct();
outputs.sma = [8000000];
outputs.ecc = [0.001];

TEST_OK = [];

//=========================
// Compute output
//=========================
[sma,ecc] = CL_op_rarp2ae(inputs.ra,inputs.rp);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(sma, outputs.sma, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(ecc, outputs.ecc, relative = %t, compar_crit = "element", prec = 1e-014);
