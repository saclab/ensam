// ------------------------------
// CL_op_locTimeG50
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = 15890;
inputs.type_par1 = 'mlh';
inputs.par1 = 0.1e2;
inputs.type_par2 = 'all';

// Outputs
outputs = struct();
outputs.par2 = struct("ra",[1.2471214970324871] , ..
"lon",[2.617993877991494] , ..
"tsa",[5.7407065742352588] , ..
"msa",[5.7595865315812871] , ..
"tlh",[9.9278838751123715] , ..
"mlh",[10] , ..
"sidt",[4.9123129262205794] , ..
"stra",[1.7896002299768148] , ..
"smra",[1.7707202726307862] , ..
"tlt",[5.7407065742352588] , ..
"mlt",[5.7595865315812871]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[par2] = CL_op_locTimeG50(inputs.cjd,inputs.type_par1,inputs.par1,inputs.type_par2);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(par2, outputs.par2, relative = %t, compar_crit = "element", prec = 1e-014);
