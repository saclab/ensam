// ------------------------------
// CL_colNorm
// validation:
// Comparison with 'norm' scilab function
// ------------------------------

// Inputs
inputs = struct();
v = [1;2;3;4];
inputs.M = [v v+1 v+2 v+3 v+4];

// Outputs
outputs = struct();
outputs.norms = [5.4772255750516612,7.3484692283495345,9.2736184954957039,11.224972160321824,13.19090595827292];

TEST_OK = [];

//=========================
// Compute output
//=========================
[norms] = CL_colNorm(inputs.M);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(norms, outputs.norms, relative = %t, compar_crit = "element", prec = 1e-014);
