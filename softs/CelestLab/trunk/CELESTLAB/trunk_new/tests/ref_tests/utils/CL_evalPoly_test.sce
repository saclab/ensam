// ------------------------------
// CL_evalPoly
// validation:
// Luca Cerri (DCT/SB/OR)
// ------------------------------

// Inputs
inputs = struct();
inputs.COEFF = [-0.7616491,0.4529708,0.2546697;0.6755537,0.7223316,-1.5417209];
inputs.t = -0.05:0.05:0.05;

// Outputs
outputs = struct();
outputs.F = [[-0.78366096574999999;0.63558281775000003],[-0.76164909999999997;0.67555370000000003],[-0.73836388575;..
0.70781597775000005]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[F] = CL_evalPoly(inputs.COEFF,inputs.t);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(F, outputs.F, relative = %t, compar_crit = "element", prec = 1e-014);
