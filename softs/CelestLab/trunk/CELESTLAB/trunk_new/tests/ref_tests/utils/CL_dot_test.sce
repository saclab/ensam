// ------------------------------
// CL_dot
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.u = [1;1;0];
inputs.v = [1;0;1];

// Outputs
outputs = struct();
outputs.c = [1];

TEST_OK = [];

//=========================
// Compute output
//=========================
[c] = CL_dot(inputs.u,inputs.v);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(c, outputs.c, relative = %t, compar_crit = "element", prec = 1e-014);
