// ------------------------------
// CL_quantile
// validation:
// Matlab's quantile function
// ------------------------------

// Inputs
inputs = struct();
inputs.X = [1 -3 %pi sqrt(2) 12.6 19.6];
inputs.p = [0, 0.0005, 0.09, %pi/10, %pi/4, 0.9, 0.99, 1];

// Outputs
outputs = struct();
outputs.Y = [-3	-3	-2.84000000000000	1.15945382718150	14.0867228626928	18.9	19.6	19.6];


TEST_OK = [];

//=========================
// Compute output
//=========================
[Y] = CL_quantile(inputs.X,inputs.p);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(Y, outputs.Y, relative = %t, compar_crit = "element", prec = 1e-014);
