// ------------------------------
// CL_cov2cor
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.cov = [0.000001  1.000D-09; 1.000D-09 1.000D-08];

// Outputs
outputs = struct();
outputs.cor = [[1;0.01],[0.010000000000000002;1]];
outputs.sd = [0.001;0.0001];

TEST_OK = [];

//=========================
// Compute output
//=========================
[cor,sd] = CL_cov2cor(inputs.cov);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(cor, outputs.cor, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(sd, outputs.sd, relative = %t, compar_crit = "element", prec = 1e-014);
