// ------------------------------
// CL_cor2cov
// validation:
// 
// 05-05-2014
// outputs.cov renamed outputs.covv
// (avoids function redefinition)
// ------------------------------

// Inputs
inputs = struct();
inputs.cor = [1.0  0.01; 0.01 1.0];
inputs.sd = [0.001 ; 0.0001];

// Outputs
outputs = struct();
outputs.covm = [[9.9999999999999995e-007;1.0000000000000003e-009],[1.0000000000000001e-009;1e-008]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[covm] = CL_cor2cov(inputs.cor,inputs.sd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(covm, outputs.covm, relative = %t, compar_crit = "element", prec = 1e-014);
