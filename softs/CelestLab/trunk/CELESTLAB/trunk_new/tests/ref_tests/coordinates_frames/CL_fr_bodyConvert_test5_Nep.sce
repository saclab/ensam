// -------------------------------------------------------
// CL_fr_bodyConvert
// Neptune: BCF --> ICRS
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Neptune";
inputs.frame1 = "BCF";
inputs.frame2 = "ICRS";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [2102972.6;-3904081.4;4043087.3];
outputs.vel2 = [4692.5239;-4354.5907;2255.9677];
outputs.jacob = [-0.7096198,0.6060932,0.3592920,0.,0.,0.;
-0.6608709,-0.3957333,-0.6376870,0.,0.,0.;
-0.2443140,-0.6899610,0.6813696,0.,0.,0.;
0.0000657,0.0000769,2.204D-12,-0.7096198,0.6060932,0.3592920;
-0.0000429,0.0000716,1.352D-12,-0.6608709,-0.3957333,-0.6376870;
-0.0000747,0.0000265,1.029D-13,-0.2443140,-0.6899610,0.6813696];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
