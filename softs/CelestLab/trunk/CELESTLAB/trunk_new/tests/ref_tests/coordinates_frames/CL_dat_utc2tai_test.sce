// ------------------------------
// CL_dat_utc2tai
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
TAI_UTC_DATA = CL_dataGet("TAI_UTC_DATA"); 

jj = TAI_UTC_DATA($-5:$, 1)'; // UTC
nsec = TAI_UTC_DATA($-5:$, 3)'; // TAI - UTC (from jj) 

inputs.cjdutc = [jj - 0.1, jj + 0.1]; 

// Outputs
outputs = struct();
outputs.cjdtai = inputs.cjdutc + [nsec - 1, nsec] / 86400;
                  

TEST_OK = [];

//=========================
// Compute output
//=========================
[cjdtai] = CL_dat_utc2tai(inputs.cjdutc);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(cjdtai, outputs.cjdtai, relative = %t, compar_crit = "element", prec = 1.e-014);
