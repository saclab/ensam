// ------------------------------
// CL_fr_J20002G50
// validation:
// Validation with PSIMU
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = [21010 , 21011];
inputs.pos_J2000 = [ [3952930.5;3127929.25;4128420.75] , [3945680.5;3125978.25;5687420.75]];

// Outputs
outputs = struct();
outputs.pos_G50 = [[3984586.6055055475;3083410.2263899669;4131477.9170275037],[3976152.3565499308;3081494.5526188705;5690473.8972045677]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_G50] = CL_fr_J20002G50(inputs.cjd,inputs.pos_J2000);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_G50, outputs.pos_G50, relative = %t, compar_crit = "element", prec = 1e-014);
