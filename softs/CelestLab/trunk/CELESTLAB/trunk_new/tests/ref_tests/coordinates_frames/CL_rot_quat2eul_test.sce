// ------------------------------
// CL_rot_quat2eul
// validation:
// Manual Calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.q = [];
q = [0.21938548075747005;0.54841581059185096;0.014355331441165948;0.80678623809836469];
inputs.q = CL_rot_defQuat([q,q]);

// Outputs
outputs = struct();
outputs.phi = [0.5,0.5];
outputs.theta = [1.1,1.1];
outputs.psi = [2.3,2.3];

TEST_OK = [];

//=========================
// Compute output
//=========================
[phi,theta,psi] = CL_rot_quat2eul(inputs.q);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(phi, outputs.phi, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(theta, outputs.theta, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(psi, outputs.psi, relative = %t, compar_crit = "element", prec = 1e-014);
