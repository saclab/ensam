// -------------------------------------------------------
// CL_fr_bodyConvert
// Jupiter: ICRS --> BCI
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Jupiter";
inputs.frame1 = "ICRS";
inputs.frame2 = "BCI";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [98924.772;2613500.2;5400993.5];
outputs.vel2 = [-1101.1919;5258.8835;4139.0241];
outputs.jacob = [0.9994245,-0.0339225,0.,0.,0.,0.;
0.0306171,0.9020411,0.4305629,0.,0.,0.;
-0.0146058,-0.4303151,0.9025606,0.,0.,0.;
1.048D-15,3.087D-14,3.944D-31,0.9994245,-0.0339225,0.;
-2.674D-14,3.423D-14,-6.981D-14,0.0306171,0.9020411,0.4305629;
1.566D-14,6.932D-14,3.330D-14,-0.0146058,-0.4303151,0.9025606];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
