// -------------------------------------------------------
// CL_fr_bodyConvert
// Sun: BCF --> ICRS
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Sun";
inputs.frame1 = "BCF";
inputs.frame2 = "ICRS";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [634199.49;-2568686.3;5386143.5];
outputs.vel2 = [1656.3853;-5224.5329;3995.3141];
outputs.jacob = [-0.9922121,-0.0233421,0.1223535,0.,0.,0.;
-0.0308668,-0.9055701,-0.4230721,0.,0.,0.;
0.1206751,-0.4235539,0.8977971,0.,0.,0.;
-6.688D-08,0.0000028,0.,-0.9922121,-0.0233421,0.1223535;
-0.0000026,8.844D-08,0.,-0.0308668,-0.9055701,-0.4230721;
-0.0000012,-0.0000003,0.,0.1206751,-0.4235539,0.8977971];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
