// ------------------------------
// CL_dat_cjd2cal
// validation:
// year,month,day validation by CNES - MSLIB FORTRAN 90, Volume D (md_julien_calend)
// ------------------------------

// Inputs
inputs = struct();
cjd_day = [6633 729 1094 1096 1186 1277 2282 2400];
cjd_sec = [0 8640 864 86399 8.640 43200 2282 20000];
inputs.cjd = cjd_day + cjd_sec/3600/24;

// Outputs
outputs = struct();
outputs.year = [1968,1951,1952,1953,1953,1953,1956,1956];
outputs.month = [2,12,12,1,4,7,4,7];
outputs.day = [29,31,30,1,1,1,1,28];
outputs.hour = [0,2,0,23,0,12,0,5];
outputs.minute = [0,24,14,59,0,0,38,33];
outputs.second = [0,1.964508555829525e-009,23.999999999214197,59.00000000197906,8.6399999978311826,0,2.0000000021536835,..
19.999999983992893];

TEST_OK = [];

//=========================
// Compute output
//=========================
[year,month,day,hour,minute,second] = CL_dat_cjd2cal(inputs.cjd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(year, outputs.year, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(month, outputs.month, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(day, outputs.day, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(hour, outputs.hour, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(minute, outputs.minute, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(second, outputs.second, relative = %t, compar_crit = "element", prec = 1e-014);
