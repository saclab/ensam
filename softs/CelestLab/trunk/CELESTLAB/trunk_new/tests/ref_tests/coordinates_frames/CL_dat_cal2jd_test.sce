// ------------------------------
// CL_dat_cal2jd
// validation:
// Orbital Mechanics for Engineering Students, H D Curtis, Chapter 5, example 5.4
// ------------------------------

// Inputs
inputs = struct();
inputs.year = [2004,1996];
inputs.month = [5,10];
inputs.day = [12,26];
inputs.hour = [14,14];
inputs.minute = [45,20];
inputs.second = [30,0];

// Outputs
outputs = struct();
outputs.jd = [2453138.1149305557,2450383.097222222];

TEST_OK = [];

//=========================
// Compute output
//=========================
[jd] = CL_dat_cal2jd(inputs.year,inputs.month,inputs.day,inputs.hour,inputs.minute,inputs.second);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(jd, outputs.jd, relative = %t, compar_crit = "element", prec = 1e-014);
