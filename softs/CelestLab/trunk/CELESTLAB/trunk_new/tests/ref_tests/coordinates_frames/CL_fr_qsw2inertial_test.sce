// ------------------------------
// CL_fr_qsw2inertial
// validation:
// CNES - MSLIB FORTRAN 90, Volume O (mo_qsw_geo)
// ------------------------------

// Inputs
inputs = struct();
pos_car = [1,1,1]';
vit_car = [1,1,0]';
vect_qsw = [sqrt(3),0,0]';
//to test vectorization:
inputs.pos_car = [pos_car pos_car];
inputs.vit_car = [vit_car vit_car];
inputs.vect_qsw = [vect_qsw vect_qsw];

// Outputs
outputs = struct();
outputs.vect_geo = [[1;1;1],[1;1;1]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[vect_geo] = CL_fr_qsw2inertial(inputs.pos_car,inputs.vit_car,inputs.vect_qsw);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(vect_geo, outputs.vect_geo, relative = %t, compar_crit = "element", prec = 1e-014);
