// -------------------------------------------------------
// CL_fr_bodyConvert
// Sun: ICRS --> BCI
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Sun";
inputs.frame1 = "ICRS";
inputs.frame2 = "BCI";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [104397.91;2643387.1;5386325.8];
outputs.vel2 = [-127.18079;5479.2426;3995.2129];
outputs.jacob = [0.9606338,0.2778177,0.,0.,0.,0.;
-0.2494239,0.8624543,0.4404093,0.,0.,0.;
0.1223535,-0.4230721,0.8977971,0.,0.,0.;
0.,0.,0.,0.9606338,0.2778177,0.;
0.,0.,0.,-0.2494239,0.8624543,0.4404093;
0.,0.,0.,0.1223535,-0.4230721,0.8977971];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
