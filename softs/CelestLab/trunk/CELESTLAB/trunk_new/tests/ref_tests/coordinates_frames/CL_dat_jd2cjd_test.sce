// ------------------------------
// CL_dat_jd2cjd
// validation:
// Manual calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.jd = [2451546 2451000];

// Outputs
outputs = struct();
outputs.cjd = [18263.5,17717.5];

TEST_OK = [];

//=========================
// Compute output
//=========================
[cjd] = CL_dat_jd2cjd(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(cjd, outputs.cjd, relative = %t, compar_crit = "element", prec = 1e-014);
