// -------------------------------------------------------
// CL_fr_bodyConvert
// Mercury: ICRS --> BCI
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Mercury";
inputs.frame1 = "ICRS";
inputs.frame2 = "BCI";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [103888.14;2879975.6;5263634.5];
outputs.vel2 = [-408.85047;5624.3663;3768.2018];
outputs.jacob = [0.9816059,0.1909185,0.,0.,0.,0.;
-0.1676452,0.8619465,0.4784803,0.,0.,0.;
0.0913507,-0.4696791,0.8780983,0.,0.,0.;
3.463D-14,-1.781D-13,0.,0.9816059,0.1909185,0.;
1.588D-13,1.768D-14,2.380D-14,-0.1676452,0.8619465,0.4784803;
-8.066D-14,-3.993D-14,-1.297D-14,0.0913507,-0.4696791,0.8780983];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
