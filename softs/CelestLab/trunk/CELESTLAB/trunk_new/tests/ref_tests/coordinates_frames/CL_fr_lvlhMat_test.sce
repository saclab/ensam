// ------------------------------
// CL_fr_lvlhMat
// validation:
// Orbital Mechanics for engineering students, H D Curtis, example 7.2
// ------------------------------

// Inputs
inputs = struct();
ephem_tg = [0;1622390;5305100;3717440;-7299.77;492.357;2483.18];
inputs.pos = [ephem_tg(2:4),ephem_tg(2:4)+1000.e3];
inputs.vel = [ephem_tg(5:7),ephem_tg(5:7)+1.e3];

// Outputs
outputs = struct();
outputs.M = hypermat([3 3 2 ] , [-0.94479878944923434,-0.21984680549841565,-0.24294573379132042,0.063725031915945168,0.60402274553306512,..
-0.79441528383208349,0.32139441152977921,-0.766044323124217,-0.55667021408243778,-0.91593371179937,-0.24745518132974353,..
-0.31596102421421002,0.087983324243834427,0.64432276570516511,-0.75967565990299524,0.39156655914982469,-0.723611848180439,..
-0.5683850129344159]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[M] = CL_fr_lvlhMat(inputs.pos,inputs.vel);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M, outputs.M, relative = %t, compar_crit = "element", prec = 1e-014);
