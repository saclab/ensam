// ------------------------------
// CL_fr_H0n2J2000Mat
// validation:
// Validation with PSIMU
// ------------------------------

// Inputs
inputs = struct();
//H0-3 frame for Arianne5 ATV launch
inputs.kourou_longitude = CL_deg2rad(-52.7686020300);
inputs.launch_date=CL_dat_cal2cjd(2006,4,14,19,13,43);

// Outputs
outputs = struct();
outputs.M = [[0.20110789956388753;0.97956908125970787;-0.00016665237209789751],[-0.97956890766620952;0.20110796344229181;..
0.00058495617193033062],[0.00060652009907042519;4.5608175022108427e-005;0.99999981502661484]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[M] = CL_fr_H0n2J2000Mat(inputs.kourou_longitude,inputs.launch_date);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M, outputs.M, relative = %t, compar_crit = "element", prec = 1e-014);
