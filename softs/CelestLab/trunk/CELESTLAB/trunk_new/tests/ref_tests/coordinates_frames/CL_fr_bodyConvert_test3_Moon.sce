// -------------------------------------------------------
// CL_fr_bodyConvert
// Moon: BCI --> ICRS
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Moon";
inputs.frame1 = "BCI";
inputs.frame2 = "ICRS";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [248743.67;-2303417.1;5535638.8];
outputs.vel2 = [-1024.9289;354.46982;6695.0605];
outputs.jacob = [0.9979273,-0.0592454,0.0251214,0.,0.,0.;
0.0643514,0.9187463,-0.3895691,0.,0.,0.;
0.,0.3903782,0.9206546,0.,0.,0.;
-1.697D-10,-2.405D-09,1.068D-09,0.9979273,-0.0592454,0.0251214;
2.631D-09,-4.264D-10,-5.710D-10,0.0643514,0.9187463,-0.3895691;
2.068D-25,6.386D-10,-2.708D-10,0.,0.3903782,0.9206546];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
