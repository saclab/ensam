// -------------------
// CL__fr_tirs2itrs
//
// Reference values : SIRIUS
// -------------------

TEST_OK = [];

// -------------------
// -------------------
// TIRS -> ITRS (SIRIUS)
// -------------------
// -------------------
// Date J2000 (--> sp=0)
cjd = CL_dat_cal2cjd(2000,1,1,12,0,0);
ut1_tref = 0;
tt_tref = 0;
xp = -4.79771618825996E-08;
yp = 1.62325801522775E-06;

// TEST 1 
pos_itrf = [1;0;0]
pos_tirf_ref = [0.999999999999997; 9.5587500390868E-18; -4.79771618827239E-08];
M_ITRS_TIRS = CL_fr_convertMat("ITRS", "TIRS", cjd, ut1_tref, tt_tref, xp, yp);
pos_tirf = M_ITRS_TIRS * pos_itrf;
// Ecart = 10-18 rad --> OK
ang = CL_vectAngle(pos_tirf,pos_tirf_ref);
TEST_OK($+1) = max(ang) < 1e-15;

// TEST 2
pos_itrf = [0;1;0]
pos_tirf_ref = [-7.78888713240151E-14; 0.999999999998681; -1.62325801522702E-06];
M_ITRS_TIRS = CL_fr_convertMat("ITRS", "TIRS", cjd, ut1_tref, tt_tref, xp, yp);
pos_tirf = M_ITRS_TIRS * pos_itrf;
// Ecart = 10-18 rad --> OK
ang = CL_vectAngle(pos_tirf,pos_tirf_ref)
TEST_OK($+1) = max(ang) < 1e-15;

// TEST 3 
pos_itrf = [0;0;1]
pos_tirf_ref = [4.79771618826606E-08; 1.62325801522702E-06; 0.99999999999868];
M_ITRS_TIRS = CL_fr_convertMat("ITRS", "TIRS", cjd, ut1_tref, tt_tref, xp, yp);
pos_tirf = M_ITRS_TIRS * pos_itrf;
// Ecart = 10-18 rad --> OK
ang = CL_vectAngle(pos_tirf,pos_tirf_ref)
TEST_OK($+1) = max(ang) < 1e-15;

