// ------------------------------
// CL_rot_rotVect
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.q = [];
inputs.q = CL_rot_defQuat(0.96125628387668982, 0.12628517271716791, 0.12611650708648509, 0.21007864836692952);
inputs.v = [1;2;3];

// Outputs
outputs = struct();
outputs.w = [1.0224326923807561;1.6260200151342841;3.2110263623853568];

TEST_OK = [];

//=========================
// Compute output
//=========================
[w] = CL_rot_rotVect(inputs.q,inputs.v);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(w, outputs.w, relative = %t, compar_crit = "element", prec = 1e-014);
