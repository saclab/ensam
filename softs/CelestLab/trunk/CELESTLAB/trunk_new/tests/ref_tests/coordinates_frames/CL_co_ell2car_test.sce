// ------------------------------
// CL_co_ell2car
// validation:
// CNES - MSLIB FORTRAN 90, Volume T (mt_geod_car)
// ------------------------------

// Inputs
inputs = struct();
inputs.pos_geod = [0.0423149994747243e-1,0.852479154923577,111.6]';
inputs.er=6378136.3;
inputs.apla=1/298.25781;

// Outputs
outputs = struct();
outputs.pos_car = [4205593.2176206503;17796.073695989431;4779203.1650464851];
outputs.jacob = [[-17796.073695989431;4205593.2176206503;0],[-4797365.5924982401;-20300.173415013229;4193390.5540476479],[0.65811268708881043;..
0.0027848204221529922;0.75291429516775799]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_car,jacob] = CL_co_ell2car(inputs.pos_geod,inputs.er,inputs.apla);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_car, outputs.pos_car, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "element", prec = 1e-014);
