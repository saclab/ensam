// ------------------------------
// CL_oe_kep2cirEqua
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.kep=[24464560;0.7311;0.122138;3.10686;1.00681;0.048363]

// Outputs
outputs = struct();
outputs.cirequa = [24464560;-0.41203680288762567;-0.60393119067170553;0.065249441736882866;0.10315845008486389;4.1620330000000001];
outputs.jacob = [[1;0;0;0;0;0],[0;-0.56358473928002417;-0.82605825560348178;0;0;0],[0;0;0;0.5335629149926816;0.84355546757566024;0],[0;..
0.60393119067170553;-0.41203680288762567;0;0;1],[0;0.60393119067170553;-0.41203680288762567;-0.10315845008486389;..
0.065249441736882866;1],[0;0;0;0;0;1]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[cirequa,jacob] = CL_oe_kep2cirEqua(inputs.kep);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(cirequa, outputs.cirequa, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "element", prec = 1e-014);
