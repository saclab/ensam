// -------------------
// CL__fr_cirs2teme
//
// Reference values : STELA
// -------------------

TEST_OK = [];

// -------------------
// -------------------
// TIRS -> TEME
// -------------------
// -------------------
// TEST 1
cjd = [CL_dat_convert("jd","cjd",2488070.0) + ...
       (18852.9280140569062+66.184)/86400.0]; // TT
jd = [2488070.0 ; ...
       (18852.9280140569062+66.184)/86400.0];
// valeur de TU1-TT (ut1_tref)
ut1_tref = -66.184;
tt_tref = 0;

//Note : le test ci-dessous (comment�) donne une pr�cision de 10-11 rad (du a la date cod�e sur une variable)
// [M, omega] = CL_fr_convertMat("TIRS", "TEME", cjd, ut1_tref, tt_tref);
args.ut1_tt = ut1_tref;
args.tt_tref = tt_tref;
[M1,omega1] = CL__fr_cirs2teme(jd, args);
[M2,omega2] = CL__fr_cirs2tirs(jd, args);

// TIRS-->CIRS-->TEME
[M,omega] = CL_rot_compose(M2,omega2,-1, M1,omega1,1);


omega_ref  = [0 ;0 ; -0.00007292115855736 ];
M_ref  = [1.,                   -0.00000000000012765,   0. ; ...
          0.00000000000012765,   1.,                    0. ; ...
          0.,                    0.,                    1. ];
// 0 rad : OK         
ang = CL_vectAngle(omega, omega_ref);
// 10-18 rad : OK
[ax,ang] = CL_rot_quat2axAng(CL_rot_matrix2quat(M*M_ref'));
TEST_OK($+1) = max(ang) < 1e-15;


// TEST 2
cjd = [CL_dat_convert("jd","cjd",2488070.0) + ...
       0.1]; // TT
jd = [2488070.0 ; ...
       0.1];
omega_ref  = [0 ;0 ; -0.00007292115855736 ];
M_ref  = [0.73198547805519898,    0.68132023301550482,    0. ; ... 
          -0.68132023301550482,   0.73198547805519898,    0. ; ...
          0.,                     0.,                     1.] ;          
//Note : le test ci-dessous (comment�) donne une pr�cision de 10-11 rad (du a la date cod�e sur une variable)
// [M, omega] = CL_fr_convertMat("TIRS", "TEME", cjd, ut1_tref, tt_tref);
args.ut1_tt = ut1_tref;
args.tt_tref = tt_tref;
[M1,omega1] = CL__fr_cirs2teme(jd, args);
[M2,omega2] = CL__fr_cirs2tirs(jd, args);

// TIRS-->CIRS-->TEME
[M,omega] = CL_rot_compose(M2,omega2,-1, M1,omega1,1);

// 10-16 rad : OK
[ax,ang] = CL_rot_quat2axAng(CL_rot_matrix2quat(M*M_ref'));
TEST_OK($+1) = max(ang) < 1e-15;