// -------------------------------------------------------
// CL_fr_bodyConvert
// Venus: ICRS --> BCF
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Venus";
inputs.frame1 = "ICRS";
inputs.frame2 = "BCF";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [2296390.6;-519023.1;5519792.2];
outputs.vel2 = [5205.1146;-74.021691;4347.7371];
outputs.jacob = [-0.2217306,0.8971936,0.3819413,0.,0.,0.;
-0.9749288,-0.211484,-0.0691976,0.,0.,0.;
0.0186908,-0.3877088,0.9215924,0.,0.,0.;
0.0000003,6.329D-08,2.071D-08,-0.2217306,0.8971936,0.3819413;
-6.635D-08,0.0000003,0.0000001,-0.9749288,-0.211484,-0.0691976;
0.,0.,0.,0.0186908,-0.3877088,0.9215924];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
