// ------------------------------
// CL_rot_angularVelocity
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.axes=[1,3,1];
inputs.angles=[0.4091067,0.4091245;-0.0000743,-0.0000033;-0.4090851,-0.4090789]
inputs.angles_der=1.0D-12*[1.5561604,0.7275215;-2.4406169,-6.9235201;0.0719211,0.0719211]
inputs.angles_der2=1.0D-21*[-8698.7127,-12811.718;-24113.456,1954.845;0.0000004,0.0000002]

// Outputs
outputs = struct();
outputs.om = [1.628081495704616e-012 7.9944259999603874e-013
9.7091047983038664e-013 2.7539320143951371e-012
-2.2391837142337721e-012 -6.3522428350307354e-012];
outputs.omp = [-8.6987126758716121e-018 -1.2811717999746862e-017
9.5910218412136451e-018 -7.7760237222105604e-019
-2.2123992874676863e-017 1.7935310218016267e-018];

TEST_OK = [];

//=========================
// Compute output
//=========================
[om,omp] = CL_rot_angularVelocity(inputs.axes,inputs.angles,inputs.angles_der,inputs.angles_der2);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(om, outputs.om, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(omp, outputs.omp, relative = %t, compar_crit = "element", prec = 1e-014);
