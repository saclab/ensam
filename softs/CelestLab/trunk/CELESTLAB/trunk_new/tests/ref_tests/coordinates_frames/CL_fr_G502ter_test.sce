// ------------------------------
// CL_fr_G502ter
// validation:
// --
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = [21010 , 21011];
inputs.pos_G50 = [ [3952930.5;3127929.25;4128420.75] , [3945680.5;3125978.25;5687420.75]];

// Outputs
outputs = struct();
outputs.pos_ter = [[-1770648.4834141585;4719576.7690566229;4128420.75],[-1689689.7931687224;4741843.842896427;5687420.75]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_ter] = CL_fr_G502ter(inputs.cjd,inputs.pos_G50);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_ter, outputs.pos_ter, relative = %t, compar_crit = "element", prec = 1e-014);
