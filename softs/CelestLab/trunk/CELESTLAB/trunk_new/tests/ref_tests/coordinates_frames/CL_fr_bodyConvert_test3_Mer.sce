// -------------------------------------------------------
// CL_fr_bodyConvert
// Mercury: BCI --> ICRS
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Mercury";
inputs.frame1 = "BCI";
inputs.frame2 = "ICRS";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [641235.6;-2773124.2;5282944.2];
outputs.vel2 = [-936.43709;-423.15355;6704.0306];
outputs.jacob = [0.9816059,-0.1676452,0.0913507,0.,0.,0.;
0.1909185,0.8619465,-0.4696791,0.,0.,0.;
0.,0.4784803,0.8780983,0.,0.,0.;
3.463D-14,1.588D-13,-8.066D-14,0.9816059,-0.1676452,0.0913507;
-1.781D-13,1.768D-14,-3.993D-14,0.1909185,0.8619465,-0.4696791;
0.,2.380D-14,-1.297D-14,0.,0.4784803,0.8780983];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
