// -------------------
// CL__fr_cirs2tirs
//
// Reference values : OREKIT
// -------------------

TEST_OK = [];

// -------------------
// -------------------
// CIRS -> TIRS (OREKIT)
// -------------------
// -------------------
// Date (TT) = 2050/1/1 02:05:05
jd = CL_dat_convert("cal", "jd", [2050;1;1])
jd = [jd ; CL_hms2sec([02;05;05])/86400];

// Reference values : OREKIT
pos_CIRS = [2000 ; 3000;  4000] ;
vel_CIRS = [2000;  3000;  4000] ;
pos_TIRS_ref = [934.9828211047743; -3482.2129636538543; 4000.000000000001];
vel_TIRS_ref = [934.728894125811;  -3482.281143677771;  4000.000000000001];

args = struct();
ut1_utc = -0.1; // s
tai_utc = 35; // s
tt_tai = 32.184; // s
args.ut1_tt = ut1_utc-tai_utc-tt_tai;

[M, omega] = CL__fr_cirs2tirs(jd,args);

// Conversion en prenant en compte le omega.
[pos_TIRS, vel_TIRS] = CL_rot_pvConvert(pos_CIRS,vel_CIRS, M,omega);

// Ecart = 10-12 rad --> OK
ang = CL_vectAngle(pos_TIRS,pos_TIRS_ref);
TEST_OK($+1) = max(ang) < 1e-11;
ang = CL_vectAngle(vel_TIRS,vel_TIRS_ref);
TEST_OK($+1) = max(ang) < 1e-11;

// -------------------
// -------------------
// CIRS -> TIRS (PATRIUS 1.2.1) (TestIERS.java)
// -------------------
// -------------------
omega_ref = [0.0;-0.0;7.292115146706468E-5];

// Precision = 0 (exactement identique)
TEST_OK($+1) = CL_vectAngle(omega,omega_ref) < 1e-15;

