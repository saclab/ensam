// ------------------------------
// CL_rot_quatSlerp
// validation:
// Manual Calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.q1 = [];
inputs.q1 = CL_rot_defQuat([0.95 0.08 0.16 0.24;0.1 0.2 0.3 0.4]');
inputs.q2 = [];
inputs.q2 = CL_rot_defQuat([0.7 0.32 0.4 0.48;0.1 0.2 0.3 0.4]');
inputs.t = 0.5;

// Outputs
outputs = struct();
outputs.q = []; outputs.q = CL_rot_defQuat([[0.85581773843550368;0.20783821560914886;0.29085621919844179;0.37387422278773469],[0.18257418583505536;..
0.36514837167011072;..
0.54772255750516607;0.73029674334022143]]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[q] = CL_rot_quatSlerp(inputs.q1,inputs.q2,inputs.t);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(q, outputs.q, relative = %t, compar_crit = "element", prec = 1e-014);
