// =============================================================
// Test CL_fr_bodyConvertMat (=> coherence of "omega")
// cross-validates using CL_rot_quat2matrix 
// =============================================================
TEST_OK = [];

// ---------------------------------
// Computes angular velocity using distinct functions and test coherence of the results
//
// omega: (3x1) angular velocity at cjd (computed using q and qdot)
// omega_ref (3x1) : reference angular velocity at cjd
// ---------------------------------
function [omega, omega_ref] = test(cjd, body)

  [M, omega_ref] = CL_fr_bodyConvertMat(body, "ICRS", "BCF", cjd, tt_tref=0);
  q = CL_rot_matrix2quat(M);
  qdot = (q(3) - q(1)) / ((cjd(3) - cjd(1)) * 86400);
  [M, omega] = CL_rot_quat2matrix(q(2), qdot);
  
  omega_ref = omega_ref(:,2);
 
endfunction

// relative precision
RELTOL_OMEGA = 1.e-7;

// ----------------------------------------------
// Test
// ----------------------------------------------

dt = 1.e-5;
cjd = 25000 + [-dt, 0, dt]; // [t-dt, t, t+dt]
bodynames = [ "Mercury", "Venus", "Mars", "Jupiter", "Saturn", "Uranus", "Neptune", "Sun", "Moon"];

for ibody = 1 : size(bodynames, 2)

  [omega, omega_ref] = test(cjd, bodynames(ibody));
  TEST_OK($+1) = CL__isEqual(omega, omega_ref, compar_crit = "norm", prec = RELTOL_OMEGA);
  
end
