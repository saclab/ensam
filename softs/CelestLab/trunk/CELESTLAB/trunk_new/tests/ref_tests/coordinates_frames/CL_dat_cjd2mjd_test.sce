// ------------------------------
// CL_dat_cjd2mjd
// validation:
// Manual Calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = [20000,20001];

// Outputs
outputs = struct();
outputs.mjd = [53282,53283];

TEST_OK = [];

//=========================
// Compute output
//=========================
[mjd] = CL_dat_cjd2mjd(inputs.cjd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(mjd, outputs.mjd, relative = %t, compar_crit = "element", prec = 1e-014);
