// -------------------
// CL__fr_mod2gcrs
//
// Reference values : STELA v2.2.1
// -------------------

// -------------------
// -------------------
// MOD -> GCRS (STELA)
// -------------------
// -------------------
TEST_OK = [];

// Date (TT) = 2050/1/1 02:05:05
jd = CL_dat_convert("cal", "jd", [2050;1;1])
jd = [jd ; CL_hms2sec([02;05;05])/86400];

pos_MOD = [2000; 3000; 4000];
vel_MOD = [2000; 3000; 4000];
pos_GCRS_ref = [2052.83011759; 2977.33884819; 3990.15562245];
vel_GCRS_ref = [2052.83011759; 2977.33884819; 3990.15562245];

args = struct();
args.precession_model = "1976";
[M, omega] = CL__fr_gcrs2mod(jd, args);

// STELA ne convertit pas la vitesse avec le omega...
pos_GCRS = M' * pos_MOD;

// Ecart = 10-13 rad --> OK
ang = CL_vectAngle(pos_GCRS,pos_GCRS_ref);
TEST_OK($+1) = max(ang) < 1e-12;
