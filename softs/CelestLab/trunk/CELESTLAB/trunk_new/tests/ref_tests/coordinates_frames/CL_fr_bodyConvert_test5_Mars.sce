// -------------------------------------------------------
// CL_fr_bodyConvert
// Mars: BCF --> ICRS
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Mars";
inputs.frame1 = "BCF";
inputs.frame2 = "ICRS";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [2738256.7;-2354244.5;4792742.7];
outputs.vel2 = [482.76052;-1144.9326;6671.05];
outputs.jacob = [0.7570156,-0.4773689,0.4461460,0.,0.,0.;
0.6465955,0.6455778,-0.4063786,0.,0.,0.;
-0.0940295,0.5961110,0.7973771,0.,0.,0.;
-0.0000338,-0.0000537,-3.991D-14,0.7570156,-0.4773689,0.4461460;
0.0000458,-0.0000458,-4.426D-13,0.6465955,0.6455778,-0.4063786;
0.0000423,0.0000067,-2.033D-13,-0.0940295,0.5961110,0.7973771];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
