// ------------------------------
// CL_dat_jd2cal
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.jd = [2449877.3458762,2451603.5];

// Outputs
outputs = struct();
outputs.year = [1995,2000];
outputs.month = [6,2];
outputs.day = [8,29];
outputs.hour = [20,0];
outputs.minute = [18,0];
outputs.second = [3.703690767288208,0];

TEST_OK = [];

//=========================
// Compute output
//=========================
[year,month,day,hour,minute,second] = CL_dat_jd2cal(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(year, outputs.year, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(month, outputs.month, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(day, outputs.day, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(hour, outputs.hour, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(minute, outputs.minute, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(second, outputs.second, relative = %t, compar_crit = "element", prec = 1e-014);
