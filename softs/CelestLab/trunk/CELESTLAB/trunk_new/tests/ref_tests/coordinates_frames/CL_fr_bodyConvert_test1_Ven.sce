// -------------------------------------------------------
// CL_fr_bodyConvert
// Venus: ICRS --> BCI
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Venus";
inputs.frame1 = "ICRS";
inputs.frame2 = "BCI";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [101328.57;2352132.4;5519792.2];
outputs.vel2 = [-854.38262;5134.9014;4347.7371];
outputs.jacob = [0.99884,0.0481525,0.,0.,0.,0.;
-0.0443769,0.9205233,0.3881591,0.,0.,0.;
0.0186908,-0.3877088,0.9215924,0.,0.,0.;
0.,0.,0.,0.99884,0.0481525,0.;
0.,0.,0.,-0.0443769,0.9205233,0.3881591;
0.,0.,0.,0.0186908,-0.3877088,0.9215924];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
