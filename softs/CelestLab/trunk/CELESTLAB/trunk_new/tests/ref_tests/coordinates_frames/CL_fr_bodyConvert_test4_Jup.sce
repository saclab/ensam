// -------------------------------------------------------
// CL_fr_bodyConvert
// Jupiter: BCI --> BCF
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Jupiter";
inputs.frame1 = "BCI";
inputs.frame2 = "BCF";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [-92291.787;-48808.054;6000000.];
outputs.vel2 = [1553.7633;-2733.1472;6000.];
outputs.jacob = [-0.9810477,0.1937662,0.,0.,0.,0.;
-0.1937662,-0.9810477,0.,0.,0.,0.;
0.,0.,1.,0.,0.,0.;
-0.0000341,-0.0001725,0.,-0.9810477,0.1937662,0.;
0.0001725,-0.0000341,0.,-0.1937662,-0.9810477,0.;
0.,0.,0.,0.,0.,1.];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
