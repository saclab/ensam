// -------------------------------------------------------
// CL_fr_bodyConvert
// Uranus: ICRS --> BCF
// Manual validation
// -------------------------------------------------------
// define TREF
%CL_TT_TREF = 67.184; 

COMPAR_CRIT = "norm";
RELATIVE_PRECISION = 1e-7;
// Inputs
inputs = struct();
inputs.body = "Uranus";
inputs.frame1 = "ICRS";
inputs.frame2 = "BCF";
inputs.cjd = CL_dat_cal2cjd(2010,02,03,05,35,25); 
inputs.pos1 = [1e5;3e4;6e6];
inputs.vel1 = [-1e3;3e3;6e3];

// Outputs
outputs = struct();
outputs.pos2 = [-3871242.5;-4289499.1;-1620055.2];
outputs.vel2 = [-1680.362;-5293.8013;-4183.2864];
outputs.jacob = [-0.6965955,0.3334574,-0.6352644,0.,0.,0.;
0.6854276,0.0476707,-0.7265787,0.,0.,0.;
-0.2119996,-0.9415592,-0.2617681,0.,0.,0.;
-0.0000694,-0.0000048,0.0000736,-0.6965955,0.3334574,-0.6352644;
-0.0000705,0.0000338,-0.0000643,0.6854276,0.0476707,-0.7265787;
3.388D-21,0.,3.388D-21,-0.2119996,-0.9415592,-0.2617681];
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos2,vel2,jacob] = CL_fr_bodyConvert(inputs.body,inputs.frame1,inputs.frame2,inputs.cjd,inputs.pos1,inputs.vel1);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos2, outputs.pos2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel2, outputs.vel2, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(jacob, outputs.jacob, relative = %t, compar_crit = "norm", prec = 1e-007);
