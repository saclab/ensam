// ------------------------------
// CL__iers_xys2006A_cla
// validation:
// SOFA function: xys06a
//
// 30-04-2014
// old precision: 1e-11
// error: 6.01e-08
// new precision 1e-07 
// ------------------------------

RELATIVE_PRECISION = 1e-07;

// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();
outputs.x = [-0.0096237690596503774,0.0048864988090864432];
outputs.y = [-0.00012296306347986397,-5.3391887451181841e-005];
outputs.s = [-2.568427210149939e-007,1.0577289709238948e-007];
TEST_OK = [];

//=========================
// Compute output
//=========================
[x,y,s] = CL__iers_xys2006A_cla(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(x, outputs.x, relative = %t, compar_crit = "element", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(y, outputs.y, relative = %t, compar_crit = "element", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(s, outputs.s, relative = %t, compar_crit = "element", prec = 1e-007);
