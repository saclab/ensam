// ------------------------------
// CL__iers_nuta2000AR06
// validation:
// SOFA function: nut06a
//
// 30-04-2014
// old precision: 1e-14
// error: 1.95e-07
// new precision 1e-06 
// ------------------------------

RELATIVE_PRECISION = 1e-06

// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();
outputs.dpsi = [8.227697143070062e-005,7.3578233601493176e-005];
outputs.deps = [-1.650548940693912e-005,-2.5813138507302485e-005];
TEST_OK = [];

//=========================
// Compute output
//=========================
[dpsi,deps] = CL__iers_nuta2000AR06(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(dpsi, outputs.dpsi, relative = %t, compar_crit = "element", prec = 1e-006);

TEST_OK($+1) = CL__isEqual(deps, outputs.deps, relative = %t, compar_crit = "element", prec = 1e-006);
