// ------------------------------
// CL__iers_s2006
// validation:
// SOFA function: s06
// ------------------------------


// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];
inputs.x = [-0.0096237690625814876,0.0048864988082478761];
inputs.y = [-0.00012296306242869348,-5.3391883238452195e-005];

// Outputs
outputs = struct();
outputs.s = [-2.5684271613709199e-007,1.0577288677725396e-007];
TEST_OK = [];

//=========================
// Compute output
//=========================
[s] = CL__iers_s2006(inputs.jd,inputs.x,inputs.y);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(s, outputs.s, relative = %t, compar_crit = "element", prec = 1e-014);
