// ------------------------------
// CL_mod_moonSunG50
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = [19814 19815];

// Outputs
outputs = struct();
outputs.r_moon = [[-0.70386669097793941;0.61206682734316242;0.36048006907156616],[-0.84381036487088079;0.45346662723169817;..
0.28697053180349175]];
outputs.rl = [391239158.39242828,385931084.68792546];
outputs.r_sun = [[0.98170657464564604;0.17275438217893707;0.080050763490964197],[0.97828521815410008;0.18823741141673234;..
0.086745079885330478]];
outputs.rs = [149494387834.7128,149537387178.39081];

TEST_OK = [];

//=========================
// Compute output
//=========================
[r_moon,rl,r_sun,rs] = CL_mod_moonSunG50(inputs.cjd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(r_moon, outputs.r_moon, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(rl, outputs.rl, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(r_sun, outputs.r_sun, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(rs, outputs.rs, relative = %t, compar_crit = "element", prec = 1e-014);
