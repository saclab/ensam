// ------------------------------
// CL__iers_prec1976
// validation:
// SOFA function: prec76
// ------------------------------


// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();
outputs.zeta = [-0.011109400761382017,0.0055907552819218507];
outputs.z = [-0.011105606179074998,0.0055917162890128987];
outputs.theta = [-0.0096580934252306818,0.0048579991319257238];
TEST_OK = [];

//=========================
// Compute output
//=========================
[zeta,z,theta] = CL__iers_prec1976(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(zeta, outputs.zeta, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(z, outputs.z, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(theta, outputs.theta, relative = %t, compar_crit = "element", prec = 1e-014);
