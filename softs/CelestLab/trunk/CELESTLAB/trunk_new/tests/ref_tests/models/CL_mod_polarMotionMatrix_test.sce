// ------------------------------
// CL_mod_polarMotionMatrix
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
// polar coordinates usually given in arceseconds
xp = 0.1074 * %pi /(3600 *180);
yp = 0.4824 * %pi /(3600 *180);
inputs.xp = [xp xp];
inputs.yp = [yp yp];

// Outputs
outputs = struct();
outputs.W = hypermat([3 3 2 ] , [0.99999999999986444,0,5.2068989351161809e-007,1.2177589051661668e-012,0.99999999999726519,-2.3387411976699522e-006,-5.2068989351019412e-007,2.3387411976702694e-006,0.99999999999712963,0.99999999999986444,0,5.2068989351161809e-007,..
1.2177589051661668e-012,0.99999999999726519,-2.3387411976699522e-006,-5.2068989351019412e-007,2.3387411976702694e-006,0.99999999999712963]);

TEST_OK = [];

//=========================
// Compute output
//=========================
[W] = CL_mod_polarMotionMatrix(inputs.xp,inputs.yp);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(W, outputs.W, relative = %t, compar_crit = "element", prec = 1e-014);
