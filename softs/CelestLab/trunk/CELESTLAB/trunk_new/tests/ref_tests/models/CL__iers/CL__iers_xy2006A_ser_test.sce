// ------------------------------
// CL__iers_xy2006A_ser
// validation:
// SOFA function: xy06
// ------------------------------

RELATIVE_PRECISION = 1e-13

// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();
outputs.x = [-0.0096237690625814876,0.0048864988082478761];
outputs.y = [-0.00012296306242869348,-5.3391883238452195e-005];
TEST_OK = [];

//=========================
// Compute output
//=========================
[x,y] = CL__iers_xy2006A_ser(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(x, outputs.x, relative = %t, compar_crit = "element", prec = 1e-013);

TEST_OK($+1) = CL__isEqual(y, outputs.y, relative = %t, compar_crit = "element", prec = 1e-013);
