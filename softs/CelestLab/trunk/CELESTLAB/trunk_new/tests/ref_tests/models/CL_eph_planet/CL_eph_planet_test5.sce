// -------------------------------------------------------
// CL_eph_planet
// Jupiter
//
// The reference values are extracted from the file vsop87.chk available at the URL :
// ftp://ftp.imcce.fr/pub/ephem/planets/vsop87/
// The series used is VSOP87D (heliocentric, spherical coordinates, ecliptic of date)
//
// # Col 1 = Julian date (TDB)
// # Col 2 = longitude l (rad)
// # Col 3 = latitude b (rad)
// # Col 4 = distance r (au)
// # Col 5 = l' (rad/d)
// # Col 6 = b' (rad/d)
// # Col 7 = r' (au/d)
//
// 2451545 0.6334614186 -0.0205001039 4.9653813154 0.0015914696 1.57673e-005 0.000130408 
// 2415020 4.0927527024 0.0161446618 5.3850276671  0.0013527902 -2.1884e-005 -0.0002468357 
// 2378495 1.5255696771 -0.0043606936 5.1318457604 0.0014891769 3.35365e-005 0.000351397 
// 2341970 4.8888943125 -0.0011098085 5.1888133656 0.0014564559 -3.35011e-005 -0.0003701137 
// 2305445 2.3348832684 0.0140523907 5.3439455032  0.0013734853 2.52124e-005 0.0002899715 
// 2268920 5.7527666852 -0.0188346311 5.0018007395 0.0015680371 -2.12635e-005 -0.0002114848 
// 2232395 3.088951535 0.0231157947 5.4491570191   0.001321399 3.9589e-006 4.68252e-005 
// 2195870 0.377650343 -0.0222448936 4.9715071036  0.0015874932 1.16588e-005 0.0001158213 
// 2159345 3.8455069137 0.0185554473 5.3896206945  0.0013506892 -1.95538e-005 -0.0002148837 
// 2122820 1.2695066546 -0.007533574 5.1193587362  0.0014965116 3.35173e-005 0.0003415381
//
// NB : The reference values were written using %.10f (poor precision)
//      So the number of significant digits in the reference results depend on the absolute value
//      of the result. 
//      For position there is about 10 significant digits (avg value close to 1)
//      For velocity there is about 7 significant digits  (avg value close to 0.001)
//      --> The test will be done at a relative precision of 10-7
// ------------------------------
// -------------------------------------------------------

RELATIVE_PRECISION = 1e-7;
COMPAR_CRIT = "norm";
 
// Inputs
inputs.planet = "Jupiter";
jd = [2451545:-36525:2122820];
inputs.cjd = CL_dat_convert("jd","cjd",jd);
inputs.tt_tref = 0;
inputs.accuracy = "full";

// Outputs
pos_sph = [ 0.6334614186 -0.0205001039 4.9653813154 
            4.0927527024 0.0161446618 5.3850276671  
            1.5255696771 -0.0043606936 5.1318457604 
            4.8888943125 -0.0011098085 5.1888133656 
            2.3348832684 0.0140523907 5.3439455032  
            5.7527666852 -0.0188346311 5.0018007395 
            3.088951535 0.0231157947 5.4491570191   
            0.377650343 -0.0222448936 4.9715071036  
            3.8455069137 0.0185554473 5.3896206945  
            1.2695066546 -0.007533574 5.1193587362  ]';
vel_sph = [ 0.0015914696 1.57673e-005 0.000130408  
            0.0013527902 -2.1884e-005 -0.0002468357  
            0.0014891769 3.35365e-005 0.000351397 
            0.0014564559 -3.35011e-005 -0.0003701137 
            0.0013734853 2.52124e-005 0.0002899715  
            0.0015680371 -2.12635e-005 -0.0002114848 
            0.001321399 3.9589e-006 4.68252e-005  
            0.0015874932 1.16588e-005 0.0001158213  
            0.0013506892 -1.95538e-005 -0.0002148837  
            0.0014965116 3.35173e-005 0.0003415381]';

[pos,vel] = CL_co_sph2car(pos_sph,vel_sph);

outputs.pos = %CL_au * pos;
outputs.vel = %CL_au /86400 * vel;
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos,vel] = CL_eph_planet(inputs.planet,inputs.cjd,inputs.tt_tref,inputs.accuracy);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos, outputs.pos, relative = %t, compar_crit = "norm", prec = 1e-007);

TEST_OK($+1) = CL__isEqual(vel, outputs.vel, relative = %t, compar_crit = "norm", prec = 1e-007);
