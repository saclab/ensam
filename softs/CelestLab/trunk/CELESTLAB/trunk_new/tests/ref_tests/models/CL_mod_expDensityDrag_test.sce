// ------------------------------
// CL_mod_expDensityDrag
// validation:
// manual calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.alt = [350000 360000 350000 360000];
inputs.cbal = [2.2 2.3 2.3 2.2];

// Outputs
outputs = struct();
outputs.acc = [-0.0029860066081721622,-0.0026748772260445715,-0.0031217341812708964,-0.002558578216216547];

TEST_OK = [];

//=========================
// Compute output
//=========================
[acc] = CL_mod_expDensityDrag(inputs.alt,inputs.cbal);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(acc, outputs.acc, relative = %t, compar_crit = "element", prec = 1e-014);
