// ------------------------------
// CL__iers_gmst1982
// validation:
// SOFA function: gmst82
// ------------------------------

RELATIVE_PRECISION = 1e-13;

// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();
outputs.gmst82 = [2.5391749605073244,0.69108511738484424];
TEST_OK = [];

//=========================
// Compute output
//=========================
[gmst82] = CL__iers_gmst1982(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(gmst82, outputs.gmst82, relative = %t, compar_crit = "element", prec = 1e-013);
