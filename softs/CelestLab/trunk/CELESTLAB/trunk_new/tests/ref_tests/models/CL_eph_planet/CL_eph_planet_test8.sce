// -------------------------------------------------------
// CL_eph_planet
// Neptune
//
// The reference values are extracted from the file vsop87.chk available at the URL :
// ftp://ftp.imcce.fr/pub/ephem/planets/vsop87/
// The series used is VSOP87D (heliocentric, spherical coordinates, ecliptic of date)
//
// # Col 1 = Julian date (TDB)
// # Col 2 = longitude l (rad)
// # Col 3 = latitude b (rad)
// # Col 4 = distance r (au)
// # Col 5 = l' (rad/d)
// # Col 6 = b' (rad/d)
// # Col 7 = r' (au/d)
//
// 2451545 5.3045629252 0.0042236789 30.1205328392  0.0001046473 -3.1841e-006 -3.51213e-005 
// 2415020 1.4956195225 -0.021961003 29.8710345051  0.000106171 2.3201e-006 1.48205e-005 
// 2378495 3.9290537977 0.0310692112 30.3209192288  0.000103184 -3.128e-007 5.2319e-006 
// 2341970 0.0815199679 -0.0260752533 29.8685860491 0.0001065815 -1.846e-006 -8.9715e-006 
// 2305445 2.5537079778 0.010237401 30.1360158724   0.0001048359 3.1071e-006 2.64309e-005 
// 2268920 4.9678695785 0.0116907777 30.1785350169  0.0001044173 -3.0582e-006 -3.03637e-005 
// 2232395 1.1523661584 -0.0273547725 29.8326055236 0.0001065954 1.7305e-006 6.3316e-006 
// 2195870 3.5930943433 0.0316878975 30.310911496   0.0001032055 4.694e-007 6.8902e-006 
// 2159345 6.020359658 -0.0215169842 29.9065506848  0.0001061056 -2.5258e-006 -1.57107e-005 
// 2122820 2.2124988267 0.0027498093 30.065369361   0.0001051888 3.373e-006 3.14401e-005
//
// NB : The reference values were written using %.10f (poor precision)
//      So the number of significant digits in the reference results depend on the absolute value
//      of the result. 
//      For position there is about 10 significant digits (avg value close to 1)
//      For velocity there is about 7 significant digits  (avg value close to 0.0001)
//      --> The test will be done at a relative precision of 10-6
// ------------------------------
// -------------------------------------------------------

RELATIVE_PRECISION = 1e-6;
COMPAR_CRIT = "norm";
 
// Inputs
inputs.planet = "Neptune";
jd = [2451545:-36525:2122820];
inputs.cjd = CL_dat_convert("jd","cjd",jd);
inputs.tt_tref = 0;
inputs.accuracy = "full";

// Outputs
pos_sph = [ 5.3045629252 0.0042236789 30.1205328392 
            1.4956195225 -0.021961003 29.8710345051 
            3.9290537977 0.0310692112 30.3209192288 
            0.0815199679 -0.0260752533 29.8685860491
            2.5537079778 0.010237401 30.1360158724  
            4.9678695785 0.0116907777 30.1785350169 
            1.1523661584 -0.0273547725 29.8326055236
            3.5930943433 0.0316878975 30.310911496  
            6.020359658 -0.0215169842 29.9065506848 
            2.2124988267 0.0027498093 30.065369361  ]';
vel_sph = [ 0.0001046473 -3.1841e-006 -3.51213e-005  
            0.000106171 2.3201e-006 1.48205e-005  
            0.000103184 -3.128e-007 5.2319e-006 
            0.0001065815 -1.846e-006 -8.9715e-006 
            0.0001048359 3.1071e-006 2.64309e-005  
            0.0001044173 -3.0582e-006 -3.03637e-005 
            0.0001065954 1.7305e-006 6.3316e-006  
            0.0001032055 4.694e-007 6.8902e-006  
            0.0001061056 -2.5258e-006 -1.57107e-005  
            0.0001051888 3.373e-006 3.14401e-005]';

[pos,vel] = CL_co_sph2car(pos_sph,vel_sph);

outputs.pos = %CL_au * pos;
outputs.vel = %CL_au /86400 * vel;
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos,vel] = CL_eph_planet(inputs.planet,inputs.cjd,inputs.tt_tref,inputs.accuracy);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos, outputs.pos, relative = %t, compar_crit = "norm", prec = 1e-006);

TEST_OK($+1) = CL__isEqual(vel, outputs.vel, relative = %t, compar_crit = "norm", prec = 1e-006);
