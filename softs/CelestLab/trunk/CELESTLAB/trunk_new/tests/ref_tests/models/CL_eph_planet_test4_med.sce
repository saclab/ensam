// -------------------------------------------------------
// CL_eph_planet
// Mars
//
// The reference values are extracted from the file vsop87.chk available at the URL :
// ftp://ftp.imcce.fr/pub/ephem/planets/vsop87/
// The series used is VSOP87D (heliocentric, spherical coordinates, ecliptic of date)
//
// # Col 1 = Julian date (TDB)
// # Col 2 = longitude l (rad)
// # Col 3 = latitude b (rad)
// # Col 4 = distance r (au)
// # Col 5 = l' (rad/d)
// # Col 6 = b' (rad/d)
// # Col 7 = r' (au/d)
//
// 2451545 6.2735389983 -0.0247779824 1.3912076925 0.0109247619 0.0002261122 0.0005173281 
// 2415020 4.9942005211 -0.0271965869 1.4218777705 0.0104598895 -0.0001821239 -0.0009706919 
// 2378495 3.8711855478 0.0034969939 1.5615140011  0.0086666187 -0.0002784034 -0.0012229676 
// 2341970 2.916664869 0.0280268149 1.6584697082   0.0076889546 -0.0001236824 -0.0003706795 
// 2305445 2.0058210394 0.0300702181 1.6371997207  0.0078914116 9.36274e-005 0.000723099 
// 2268920 1.0050966939 0.0066676098 1.512362269   0.0092394655 0.0002924831 0.0013005711 
// 2232395 6.0979760762 -0.0266794243 1.3925964529 0.010904495 0.0001995751 0.0005353359 
// 2195870 4.8193924948 -0.0255031923 1.4208707215 0.0104742113 -0.0002087747 -0.0009489904 
// 2159345 3.6939294875 0.0065885509 1.5593802008  0.0086913497 -0.0002756376 -0.0012209924 
// 2122820 2.7367104344 0.0295522719 1.6571002307  0.0077025917 -0.0001021869 -0.000386008
//
// NB : The reference values were written using %.10f (poor precision)
//      So the number of significant digits in the reference results depend on the absolute value
//      of the result. 
//      For position there is about 10 significant digits (avg value close to 1)
//      For velocity there is about 8 significant digits  (avg value close to 0.01)
//      --> The test will be done at a relative precision of 10-2
// To check the reliability of the results the following test has been performed:
// 1.- cjd = [2000:10:2100]
// 2.- tt_tref =0
// 3.- [pos1,vel1]=CL_eph_planet("Mars",cjd,tt_tref,model="full")
// 4.- [pos2,vel2]=CL_eph_planet("Mars",cjd,tt_tref,model="med")
// 5.- theta1=acos((pos1'*pos2)/(norm(pos1)*norm(pos2))) < 40arcsec
// ------------------------------
// -------------------------------------------------------

RELATIVE_PRECISION = 1e-2;
COMPAR_CRIT = "norm";
 
// Inputs
inputs.planet = "Mars";
jd = [2451545:-36525:2122820];
inputs.cjd = CL_dat_convert("jd","cjd",jd);
inputs.tt_tref = 0;
inputs.accuracy = "med";

// Outputs
pos_sph = [ 6.2735389983 -0.0247779824 1.3912076925
            4.9942005211 -0.0271965869 1.4218777705
            3.8711855478 0.0034969939 1.5615140011 
            2.916664869 0.0280268149 1.6584697082  
            2.0058210394 0.0300702181 1.6371997207 
            1.0050966939 0.0066676098 1.512362269  
            6.0979760762 -0.0266794243 1.3925964529
            4.8193924948 -0.0255031923 1.4208707215
            3.6939294875 0.0065885509 1.5593802008 
            2.7367104344 0.0295522719 1.6571002307 ]';
vel_sph = [ 0.0109247619 0.0002261122 0.0005173281  
            0.0104598895 -0.0001821239 -0.0009706919  
            0.0086666187 -0.0002784034 -0.0012229676 
            0.0076889546 -0.0001236824 -0.0003706795 
            0.0078914116 9.36274e-005 0.000723099  
            0.0092394655 0.0002924831 0.0013005711 
            0.010904495 0.0001995751 0.0005353359  
            0.0104742113 -0.0002087747 -0.0009489904  
            0.0086913497 -0.0002756376 -0.0012209924  
            0.0077025917 -0.0001021869 -0.000386008]';

[pos,vel] = CL_co_sph2car(pos_sph,vel_sph);

outputs.pos = %CL_au * pos;
outputs.vel = %CL_au /86400 * vel;
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos,vel] = CL_eph_planet(inputs.planet,inputs.cjd,inputs.tt_tref,inputs.accuracy);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos, outputs.pos, relative = %t, compar_crit = "norm", prec = 1e-002);

TEST_OK($+1) = CL__isEqual(vel, outputs.vel, relative = %t, compar_crit = "norm", prec = 1e-002);
