// ------------------------------
// CL_eph_moon (Meeus only)
// validation:
// Exemple 47.a, page 342, Astronomical Algorithms. J.Meeus - Second edition
// ------------------------------

// Inputs
inputs = struct();
inputs.cjd = CL_dat_cal2cjd(1992,4,12,0,0,0);
inputs.tt_tref = 0;
inputs.frame = "EOD";
inputs.accuracy = "high"; // Corresponds to the full MEEUS model.

// Outputs
outputs = struct();
// Values from the book:
// lon = 133.162655 deg
// lat = -3.229126 deg
// dist = 368409.7 km
outputs.pos_moon = CL_co_sph2car([133.1626546851579 * %CL_deg2rad; 
                                  -3.229126419223187 * %CL_deg2rad;
                                  368409684.8161262]);
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos_moon] = CL_eph_moon(inputs.cjd,inputs.tt_tref,inputs.frame,inputs.accuracy);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos_moon, outputs.pos_moon, relative = %t, compar_crit = "element", prec = 1e-014);
