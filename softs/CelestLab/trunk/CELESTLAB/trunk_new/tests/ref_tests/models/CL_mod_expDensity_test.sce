// ------------------------------
// CL_mod_expDensity
// validation:
// manual calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.altitude = [350000 452000 426000];
inputs.prctl = 99;

// Outputs
outputs = struct();
outputs.ro = [3.3290000000000002e-011,9.5774014786272363e-012,1.2949351505609892e-011];

TEST_OK = [];

//=========================
// Compute output
//=========================
[ro] = CL_mod_expDensity(inputs.altitude,inputs.prctl);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(ro, outputs.ro, relative = %t, compar_crit = "element", prec = 1e-014);
