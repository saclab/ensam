// ------------------------------
// CL__iers_fundArgsPL
// validation:
// SOFA functions: fame03, fave03, fae03, fama03, faju03, fasa03, faur03, fane03, fapa03
// ------------------------------

RELATIVE_PRECISION = 1e-12;
// Due to numerical errors (fmod <> CL_rMod)

// Inputs
inputs = struct();
inputs.jd = [2415249   , 2469807 ;
             0.0002211 , 0.3303271 ];

// Outputs
outputs = struct();

// SOFA uses a modulo, we use CL_rMod (same as pmodulo)
outputs.Fpl = CL_rMod( [ -5.6422522440399163 1.88310317674825
                        -0.15616580702360494 4.8976696557033179
                        -0.5794790379457524 1.7450787202780731
                        -5.320852543753567 3.5865471091184347
                        -1.7719677954690312 1.9511073381776924
                        -1.4726060113112815 5.2556876120498295
                        -1.9499802828353738 2.9371537541952044
                        1.5224909328983336 0.9353350474350135
                        -0.024223564550152007 0.01219210843971799] , 2*%pi);

TEST_OK = [];

//=========================
// Compute output
//=========================
[Fpl] = CL__iers_fundArgsPL(inputs.jd);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(Fpl, outputs.Fpl, relative = %t, compar_crit = "element", prec = 1e-012);
