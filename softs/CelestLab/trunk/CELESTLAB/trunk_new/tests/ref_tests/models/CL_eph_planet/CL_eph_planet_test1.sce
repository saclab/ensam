// -------------------------------------------------------
// CL_eph_planet
// Mercury
//
// The reference values are extracted from the file vsop87.chk available at the URL :
// ftp://ftp.imcce.fr/pub/ephem/planets/vsop87/
// The series used is VSOP87D (heliocentric, spherical coordinates, ecliptic of date)
//
// # Col 1 = Julian date (TDB)
// # Col 2 = longitude l (rad)
// # Col 3 = latitude b (rad)
// # Col 4 = distance r (au)
// # Col 5 = l' (rad/d)
// # Col 6 = b' (rad/d)
// # Col 7 = r' (au/d)
//
// 2451545 4.4293481036 -0.0527573409 0.4664714751 0.0479095093 -0.0053006055 0.0003551593 
// 2415020 3.4851161911 0.0565906173 0.4183426275  0.0595923705 -0.0064746087 0.0048383515 
// 2378495 2.0737894888 0.1168184804 0.3233909533  0.1007745969 0.003597698 0.0040779555 
// 2341970 0.1910149587 -0.0682441256 0.3381563139 0.0913397621 0.009272013 -0.0051301893 
// 2305445 5.183642182 -0.1170914848 0.4326517759  0.0563078443 -0.0019460816 -0.0041687376 
// 2268920 4.2636517903 -0.0457048516 0.4661523936 0.0479439034 -0.0054474573 0.0005305577 
// 2232395 3.3115600862 0.0639722347 0.4152385205  0.0605435516 -0.0063076525 0.0049542248 
// 2195870 1.8738888759 0.1126774697 0.3209366232  0.1022284345 0.0047707226 0.0038043935 
// 2159345 6.281982606 -0.0768697084 0.341435425   0.089709822 0.0085059878 -0.005273785 
// 2122820 5.0128397764 -0.1143275808 0.4352063237 0.0556152455 -0.0023509297 -0.0040232429
//
// NB : The reference values were written using %.10f (poor precision)
//      So the number of significant digits in the reference results depend on the absolute value
//      of the result. 
//      For position there is about 10 significant digits (avg value close to 1)
//      For velocity there is about 8 significant digits  (avg value close to 0.01)
//      --> The test will be done at a relative precision of 10-8
// ------------------------------
// -------------------------------------------------------

RELATIVE_PRECISION = 1e-8;
COMPAR_CRIT = "norm";
 
// Inputs
inputs.planet = "Mercury";
jd = [2451545:-36525:2122820];
inputs.cjd = CL_dat_convert("jd","cjd",jd);
inputs.tt_tref = 0;
inputs.accuracy = "full";

// Outputs
pos_sph = [ 4.4293481036 -0.0527573409 0.4664714751
            3.4851161911 0.0565906173 0.4183426275 
            2.0737894888 0.1168184804 0.3233909533 
            0.1910149587 -0.0682441256 0.3381563139
            5.183642182 -0.1170914848 0.4326517759 
            4.2636517903 -0.0457048516 0.4661523936
            3.3115600862 0.0639722347 0.4152385205 
            1.8738888759 0.1126774697 0.3209366232 
            6.281982606 -0.0768697084 0.341435425  
            5.0128397764 -0.1143275808 0.4352063237]';
vel_sph = [ 0.0479095093 -0.0053006055 0.0003551593 
            0.0595923705 -0.0064746087 0.0048383515 
            0.1007745969 0.003597698 0.0040779555 
            0.0913397621 0.009272013 -0.0051301893 
            0.0563078443 -0.0019460816 -0.0041687376
            0.0479439034 -0.0054474573 0.0005305577 
            0.0605435516 -0.0063076525 0.0049542248 
            0.1022284345 0.0047707226 0.0038043935 
            0.089709822 0.0085059878 -0.005273785 
            0.0556152455 -0.0023509297 -0.0040232429]';

[pos,vel] = CL_co_sph2car(pos_sph,vel_sph);

outputs.pos = %CL_au * pos;
outputs.vel = %CL_au /86400 * vel;
TEST_OK = [];

//=========================
// Compute output
//=========================
[pos,vel] = CL_eph_planet(inputs.planet,inputs.cjd,inputs.tt_tref,inputs.accuracy);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pos, outputs.pos, relative = %t, compar_crit = "norm", prec = 1e-008);

TEST_OK($+1) = CL__isEqual(vel, outputs.vel, relative = %t, compar_crit = "norm", prec = 1e-008);
