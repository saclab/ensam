// ------------------------------
// CL_mod_meanObliquity
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.jj_tai = [19500,20500];
inputs.ss_tai = 43200*ones(inputs.jj_tai);
inputs.flag = "s";

// Outputs
outputs = struct();
outputs.eps0 = [0.40908511131303982,0.40907889732985903];
outputs.eps0p = [-7.1921086654825995e-014,-7.1921114515378505e-014];
outputs.eps0pp = [-3.9495304760179475e-028,-2.4996714969203917e-028];

TEST_OK = [];

//=========================
// Compute output
//=========================
[eps0,eps0p,eps0pp] = CL_mod_meanObliquity(inputs.jj_tai,inputs.ss_tai,inputs.flag);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(eps0, outputs.eps0, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(eps0p, outputs.eps0p, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(eps0pp, outputs.eps0pp, relative = %t, compar_crit = "element", prec = 1e-014);
