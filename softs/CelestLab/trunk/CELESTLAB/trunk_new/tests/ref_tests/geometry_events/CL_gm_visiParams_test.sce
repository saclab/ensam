// ------------------------------
// CL_gm_visiParams
// validation:
// AMIGAU V2.1
// ------------------------------

// Inputs
inputs = struct();
inputs.rayobs=0.36e8+%CL_eqRad
inputs.rayvis=0.99e5+%CL_eqRad
inputs.type_par1='elev'
inputs.par1=CL_deg2rad(0.33e2)
inputs.type_par2='incid'

// Outputs
outputs = struct();
outputs.res = [0.99483767363676801];

TEST_OK = [];

//=========================
// Compute output
//=========================
[res] = CL_gm_visiParams(inputs.rayobs,inputs.rayvis,inputs.type_par1,inputs.par1,inputs.type_par2);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(res, outputs.res, relative = %t, compar_crit = "element", prec = 1e-014);
