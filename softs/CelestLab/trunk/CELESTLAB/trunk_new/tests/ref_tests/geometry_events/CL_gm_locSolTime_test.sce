// ------------------------------
// CL_gm_locSolTime
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.pos = [7079000 0 0]';
inputs.sun_dir = [-0.707 0.707 0]';

// Outputs
outputs = struct();
outputs.locSolTime = [-2.3561944901923448];

TEST_OK = [];

//=========================
// Compute output
//=========================
[locSolTime] = CL_gm_locSolTime(inputs.pos,inputs.sun_dir);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(locSolTime, outputs.locSolTime, relative = %t, compar_crit = "element", prec = 1e-014);
