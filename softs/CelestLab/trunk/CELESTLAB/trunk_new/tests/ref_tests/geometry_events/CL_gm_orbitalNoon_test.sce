// ------------------------------
// CL_gm_orbitalNoon
// validation:
// Manual calculation
// ------------------------------

// Inputs
inputs = struct();
inputs.inc = CL_deg2rad(98);
inputs.raan=0;
inputs.alpha_sun = 3.6480931316678333;
inputs.delta_sun = -0.2120424373848992;

// Outputs
outputs = struct();
outputs.pso_orb_noon = [-2.9765256447392603];

TEST_OK = [];

//=========================
// Compute output
//=========================
[pso_orb_noon] = CL_gm_orbitalNoon(inputs.inc,inputs.raan,inputs.alpha_sun,inputs.delta_sun);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(pso_orb_noon, outputs.pso_orb_noon, relative = %t, compar_crit = "element", prec = 1e-014);
