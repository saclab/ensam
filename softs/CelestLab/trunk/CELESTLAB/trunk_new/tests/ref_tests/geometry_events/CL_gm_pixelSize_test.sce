// ------------------------------
// CL_gm_pixelSize
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.h = 800000;
inputs.f = 30*%CL_deg2rad;

// Outputs
outputs = struct();
outputs.K1 = [1.4273738011671209];
outputs.K2 = [1.1799406734842564];

TEST_OK = [];

//=========================
// Compute output
//=========================
[K1,K2] = CL_gm_pixelSize(inputs.h,inputs.f);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(K1, outputs.K1, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(K2, outputs.K2, relative = %t, compar_crit = "element", prec = 1e-014);
