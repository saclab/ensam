// ------------------------------
// CL_gm_lat2locTime
// validation:
// 
// ------------------------------

// Inputs
inputs = struct();
inputs.phi = CL_deg2rad(35);
inputs.ihs = CL_deg2rad(98.6);
inputs.tau_asc_node = 21;

// Outputs
outputs = struct();
outputs.tauasc = [20.594745573528908];
outputs.taudes = [9.405254426471096];

TEST_OK = [];

//=========================
// Compute output
//=========================
[tauasc,taudes] = CL_gm_lat2locTime(inputs.phi,inputs.ihs,inputs.tau_asc_node);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(tauasc, outputs.tauasc, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(taudes, outputs.taudes, relative = %t, compar_crit = "element", prec = 1e-014);
