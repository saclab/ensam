// ------------------------------
// CL_cw_hohmannLine
// validation:
// Spaceflight dynamics, CNES - Cepadues 1995, Tome II, 16.3.3.1.1 and 16.3.3.1.2
// ------------------------------

// Inputs
inputs = struct();

// Outputs
outputs = struct();
outputs.Hl_imp = [0.40137350197914023];
outputs.Hl_cont = [0.20910464382373309];

TEST_OK = [];

//=========================
// Compute output
//=========================
[Hl_imp,Hl_cont] = CL_cw_hohmannLine();

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(Hl_imp, outputs.Hl_imp, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(Hl_cont, outputs.Hl_cont, relative = %t, compar_crit = "element", prec = 1e-014);
