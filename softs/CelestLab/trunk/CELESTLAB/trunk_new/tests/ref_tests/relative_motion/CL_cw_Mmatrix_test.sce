// ------------------------------
// CL_cw_Mmatrix
// validation:
// equivalent scripts
// ------------------------------

// Inputs
inputs = struct();
inputs.alt = 350000;
inputs.delta_t = 100;

// Outputs
outputs = struct();
outputs.M1 = [[1;0;0;0;0;0],[0;0.99346343270780324;0;0;-0.00013058873693154376;0],[0.0014962217212204088;0;1.0196097018765902;..
4.4867069372008906e-005;0;0.00039176621079463131],[99.128077309109486;0;-11.427546990730518;0.97385373083121296;0;-0.22830162393139511],..
[0;99.782019327277368;0;0;0.99346343270780324;0],[11.427546990730518;0;99.782019327277368;0.22830162393139511;..
0;0.99346343270780324]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[M1] = CL_cw_Mmatrix(inputs.alt,inputs.delta_t);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(M1, outputs.M1, relative = %t, compar_crit = "element", prec = 1e-014);
