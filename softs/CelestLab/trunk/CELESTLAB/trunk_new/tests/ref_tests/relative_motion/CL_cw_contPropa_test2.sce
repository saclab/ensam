// ------------------------------
// CL_cw_contPropa_2
// validation:
// equivalent scripts
// ------------------------------

// Inputs
inputs = struct();
inputs.rel_date_ini = 11550;
inputs.rel_pos_vel_ini = [-249.99355 ; 0. ; 0.0000435 ; -0.0000003 ; 0. ;0.0000003];
inputs.gama_diff = [0. ; 0. ;0.0003238];
inputs.alt = 450.e3;
inputs.rel_date_end = inputs.rel_date_ini + 3600;
inputs.delta_t = 720;

// Outputs
outputs = struct();
outputs.rel_dates = [11550,12270,12990,13710,14430,15150];
outputs.rel_pos_vel = [[-249.99355;0;4.35e-005;-2.9999999999999999e-007;0;2.9999999999999999e-007],[-206.35562972015316;0;79.486795455893883;..
0.17788512334999454;0;0.20872217733995846],[66.611795382709118;0;269.08461193947022;0.60219089785352464;0;0.28913847215113603],..
[657.26459292064067;0;452.24402705239186;1.0120880102173362;0;0.19181572373448591],[1458.671252371628;0;516.37338546031583;..
1.1556047320464917;0;-0.023419881310942303],[2234.3457646569395;0;422.05112308898475;0.94451854270812685;0;..
-0.22425882974493147]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[rel_dates,rel_pos_vel] = CL_cw_contPropa(inputs.rel_date_ini,inputs.rel_pos_vel_ini,inputs.gama_diff,inputs.alt,inputs.rel_date_end,inputs.delta_t);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(rel_dates, outputs.rel_dates, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(rel_pos_vel, outputs.rel_pos_vel, relative = %t, compar_crit = "element", prec = 1e-014);
