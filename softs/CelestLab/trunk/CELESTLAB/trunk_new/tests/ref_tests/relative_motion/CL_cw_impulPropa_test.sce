// ------------------------------
// CL_cw_impulPropa
// validation:
// equivalent scripts
// ------------------------------

// Inputs
inputs = struct();
inputs.rel_date_ini=0;
inputs.rel_pos_vel_ini=[0;0;0;0;0;0];
inputs.man_dates=100;
inputs.man=[1;0;0];
inputs.alt=450.e3;
inputs.rel_date_end=600;
inputs.delta_t=100;
inputs.ballistic_coef_chaser = 100 * 1 / 20.e3;
inputs.ballistic_coef_target = 200 * 1 / 400.e3;

// Outputs
outputs = struct();
outputs.rel_dates = [0,100,200,300,400,500,600];
outputs.rel_pos_vel = [[0;0;0;0;0;0],[-0.010549546868179629;0;0.00078977225762178209;0.9997898926038965;0;2.3683278872814395e-005],[99.124135314345168;..
0;-11.171650452887567;0.97457492968950832;0;-0.22323138876905929],[193.24717945288603;0;-44.550799075764857;..
0.89966300939117283;0;-0.44364697925717073],[277.4309909827611;0;-99.714422501032445;0.77599905900824229;0;-0.65846619239098281],..
[346.8730901463021;0;-175.96781309086066;0.60513778242808103;0;-0.86500213027933281],[396.95617737113798;0;..
-272.35247837135745;0.38922421433006166;0;-1.0606714998923941]];

TEST_OK = [];

//=========================
// Compute output
//=========================
[rel_dates,rel_pos_vel] = CL_cw_impulPropa(inputs.rel_date_ini,inputs.rel_pos_vel_ini,inputs.man_dates,inputs.man,inputs.alt,inputs.rel_date_end,inputs.delta_t,inputs.ballistic_coef_chaser,inputs.ballistic_coef_target);

//=========================
// Compare reference and computed output
//=========================

TEST_OK($+1) = CL__isEqual(rel_dates, outputs.rel_dates, relative = %t, compar_crit = "element", prec = 1e-014);

TEST_OK($+1) = CL__isEqual(rel_pos_vel, outputs.rel_pos_vel, relative = %t, compar_crit = "element", prec = 1e-014);
