// ------------------------------
// CL_fo_sphHarmAcc:
// Functional Tests:
// - Reference tests using different orders for the Earth's potential
// Reference results obtained with OREKIT (Patrius 1.2.1 release), with a DrozinerAttractionModel
// ------------------------------

// TEST_OK variable initialization
TEST_OK=[];

pos = [7.e6; 8.e6; 9.e6] * ones(1,1); 

// TEST 1
// Uses a third order Earth's Potential model
acc_ref = [0.000386116958790492 0.000433280051055266 -0.000408165971045882]'; 
acc = CL_fo_sphHarmAcc(pos, [3,3]);
TEST_OK($+1) = CL__isEqual(acc_ref,acc);


// TEST 2
// Uses a fifth order Earth's Potential model
acc_ref = [0.000386739037452241 0.000433329412404457 -0.000408341766748766]'; 
acc = CL_fo_sphHarmAcc(pos, [5,5]);
TEST_OK($+1) = CL__isEqual(acc_ref,acc);


// TEST 3
// Uses a thirty order Earth's Potential
acc_ref = [0.000386649063333150 0.000433336090295187 -0.000408572925432332]'; 
acc = CL_fo_sphHarmAcc(pos, [30,30]);
TEST_OK($+1) = CL__isEqual(acc_ref,acc);


// TEST 
// Uses a seventhy order Earth's Potential model
acc_ref = [0.000386649063333350 0.000433336090295222 -0.000408572925432447]'; 
acc = CL_fo_sphHarmAcc(pos, [70,70]);
TEST_OK($+1) = CL__isEqual(acc_ref,acc);


// TEST 4
// Position vector corresponds to a GEO orbit
// Uses a third order Earth's Potential model
pos = [29814450.3219496 29814450.3219496 0.00000000000000]'; 
acc_ref = [-5.81998568900381e-06 -5.89668797226960e-06 -5.40871136020509e-09]'; 
acc = CL_fo_sphHarmAcc(pos, [3,3]);
TEST_OK($+1) = CL__isEqual(acc_ref,acc);

pos = [29814450.3219496 -29814450.3219496 0.00000000000000 ]'; 
acc_ref = [-5.87827226980930e-06 5.94057615254741e-06 -4.87470713727131e-10]'; 
acc = CL_fo_sphHarmAcc(pos, [3,3]);

TEST_OK($+1) = CL__isEqual(acc_ref,acc);