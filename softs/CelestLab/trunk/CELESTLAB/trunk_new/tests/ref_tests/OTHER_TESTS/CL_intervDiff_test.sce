// ------------------------------
// CL_intervDiff
// validation:
// Manual verification
// ------------------------------

TEST_OK = [];

// Test 1 : Nominal case.
i1=[ [ 0;20] , [21; 25]];
i2=[ [2;4] , [5.5;5.7] , [6.6;15] , [19;20.5]];

[ires] = CL_intervDiff(i1,i2);

ires_res = [ 0 4 5.7 15 21; 2 5.5 6.6 19 25];
TEST_OK($+1) = CL__isEqual( ires , ires_res , 1e-15);

// Test 2: same intervals
i1=[ [ 0;15] , [20; 25]];
i2=[ [ 0;15] , [20; 25]];

[ires] = CL_intervDiff(i1,i2);

ires_res = [ ];
TEST_OK($+1) = CL__isEqual( ires , ires_res , 1e-15);

//Test 3: common border
i1=[2;20];
i2=[ [ 0;15] , [20; 25]];

[ires] = CL_intervDiff(i1,i2);

ires_res = [15;20 ];
TEST_OK($+1) = CL__isEqual( ires , ires_res , 1e-15);
