// ------------------------------
// CL_ex_lyddaneInfos:
// Comparaison avec l'extrapolation
// ------------------------------

TEST_OK = [];

// Cas 1
sma = 7.e6;
inc = 1.7;
ecc = 0.01;
mean_kep_t1 = [sma; ecc;inc; 0.0; 0.0; 0.0]; // sma, ex, ey, inc, raan, pso
infos = CL_ex_lyddaneInfos(mean_kep_t1);
t1 = 12584;
t2 = 12587:0.2:12588;
[mean_kep_t2,osc_cir_t2] = CL_ex_lyddane(t1,mean_kep_t1,t2);

// Verification de dgomdt par difference finie
dgomdt = diff(mean_kep_t2(5,:));
dgomdt = dgomdt(1) / (86400 *0.2);
TEST_OK($+1) = CL__isEqual(dgomdt, infos.dgomdt, 1e-11);

// Verification de dpsodt par difference finie
dpsodt = diff(mean_kep_t2(6,:)+mean_kep_t2(4,:));
dpsodt = dpsodt(1) / (86400 *0.2);
TEST_OK($+1) = CL__isEqual(dpsodt, infos.dpsodt, 1e-11);

