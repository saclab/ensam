// ---------------------------------------------------------
// Consistency tests between CL_fo_acc and CL_fo_pot functions
//
// Check that derivative of potential (computed using finite differences)
// is equal to the acceleration
//
// acc = grad(pot)
// ---------------------------------------------------------

TEST_OK = [];


// ------------
// Central body 
// ------------
pos = 7.e6 * [1;2;3]; 


acc_ref = CL_fo_centralAcc(pos); 
pot0 = CL_fo_centralPot(pos); 

accx = (CL_fo_centralPot(pos+[1;0;0]) - pot0);
accy = (CL_fo_centralPot(pos+[0;1;0]) - pot0); 
accz = (CL_fo_centralPot(pos+[0;0;1]) - pot0); 

acc_res = [accx; accy; accz];

TEST_OK($+1) = CL__isEqual(acc_res,acc_ref,1e-5,%t,"norm");



// ------------
// J2-J6
// ------------
pos = 7.e6 * [1;2;3]; 

for k = 2:6

  acc_ref = CL_fo_zonHarmAcc(pos,k); 
  pot0 = CL_fo_zonHarmPot(pos,k); 

  accx = (CL_fo_zonHarmPot(pos+[1;0;0], k) - pot0);
  accy = (CL_fo_zonHarmPot(pos+[0;1;0], k) - pot0); 
  accz = (CL_fo_zonHarmPot(pos+[0;0;1], k) - pot0); 

  acc_res = [accx; accy; accz];

  TEST_OK($+1) = CL__isEqual(acc_res,acc_ref,1e-5,%t,"norm");

end

// ------------
// Third body
// ------------

pos = [7000.e3; 8000.e3; 9000.e3]; 
pos_b = CL_eph_moon(CL_dat_cal2cjd(2000,3,21)); 
mu_b = CL_dataGet("body.Moon.mu"); 
pot0 = CL_fo_thirdBodyPot(pos, pos_b, mu_b); 
acc_ref = CL_fo_thirdBodyAcc(pos, pos_b, mu_b);

accx = (CL_fo_thirdBodyPot(pos+[1;0;0], pos_b, mu_b) - pot0);
accy = (CL_fo_thirdBodyPot(pos+[0;1;0], pos_b, mu_b) - pot0); 
accz = (CL_fo_thirdBodyPot(pos+[0;0;1], pos_b, mu_b) - pot0);

acc_res = [accx; accy; accz];

TEST_OK($+1) = CL__isEqual(acc_res,acc_ref,1e-5,%t,"norm");


// ------------
// SRP
// ------------

pos = [7000.e3; 8000.e3; 9000.e3]; // ECI
pos_sun = CL_eph_sun(CL_dat_cal2cjd(2000,6,21)); // ECI
coefp = 1.5 * 10 / 1000; 


pot0 = CL_fo_srpPot(pos, pos_sun, coefp, %f); 

delta = 10; 
accx = (CL_fo_srpPot(pos+[delta;0;0], pos_sun, coefp, %f) - pot0)/delta;
accy = (CL_fo_srpPot(pos+[0;delta;0], pos_sun, coefp, %f) - pot0)/delta; 
accz = (CL_fo_srpPot(pos+[0;0;delta], pos_sun, coefp, %f) - pot0)/delta; 

acc_ref = CL_fo_srpAcc(pos, pos_sun, coefp, %f);

acc_res = [accx; accy; accz];

TEST_OK($+1) = CL__isEqual(acc_res,acc_ref,1e-5,%t,"norm");



// ------------
// Centrifugal Force
// ------------

pos = 7.e6 * [1;2;3]; 

omega=[1e-6;1e-6;%CL_rotrBody];
acc_ref = CL_fo_centrifugAcc(pos,omega); 
pot0 = CL_fo_centrifugPot(pos,omega); 

accx = (CL_fo_centrifugPot(pos+[1;0;0]) - pot0);
accy = (CL_fo_centrifugPot(pos+[0;1;0]) - pot0); 
accz = (CL_fo_centrifugPot(pos+[0;0;1]) - pot0); 

acc_res = [accx; accy; accz]; 

TEST_OK($+1) = CL__isEqual(acc_res,acc_ref,1e-5,%t,"norm");



// ------------
// Spherical Harmonics
// ------------

pos = 7.e6 * [1;2;3]; 


acc_ref = CL_fo_sphHarmAcc(pos); 
pot0 = CL_fo_sphHarmPot(pos); 

accx = (CL_fo_sphHarmPot(pos+[1;0;0]) - pot0);
accy = (CL_fo_sphHarmPot(pos+[0;1;0]) - pot0); 
accz = (CL_fo_sphHarmPot(pos+[0;0;1]) - pot0); 

acc_res = [accx; accy; accz]; 

TEST_OK($+1) = CL__isEqual(acc_res,acc_ref,1e-5,%t,"norm");


