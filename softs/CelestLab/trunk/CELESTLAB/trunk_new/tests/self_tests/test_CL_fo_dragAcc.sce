//----------------------------------------------------------
// CL_fo_dragAcc
// Manual validation.
//----------------------------------------------------------

TEST_OK = [];

///////////////////////////////
// Test 1: manual validation //
///////////////////////////////
mu = CL_dataGet("mu");
eqRad = CL_dataGet("eqRad");

// Reference values
alt_ref = 400*1.e3; // altitude
vel_ref = [sqrt(mu./(eqRad+alt_ref)); zeros(alt_ref); zeros(alt_ref)];
rho_ref = CL_mod_atmUS76Density(alt_ref);
coefd_ref = 2.7*10/1000;
acc_ref = [-0.000002224957561896036;0;0];

// Result values
acc_res = CL_fo_dragAcc(vel_ref, rho_ref, coefd_ref);

// Check
TEST_OK($+1)= CL__isEqual(acc_ref,acc_res);


////////////////////////////////////////////////
// Test 2: manual validation (vectorial form) //
////////////////////////////////////////////////
mu = CL_dataGet("mu"); 
eqRad = CL_dataGet("eqRad");

// Reference values
alt_ref = (400:20:800)*1.e3; // altitude
vel_ref = [sqrt(mu./(eqRad+alt_ref)); zeros(alt_ref); zeros(alt_ref)];  
rho_ref = CL_mod_atmUS76Density(alt_ref); 
coefd_ref = (2.7*10/1000)*ones(alt_ref); 
acc_x_ref = [-0.000002224957561896036 -0.000001563143567060535 -0.0000011065122830523 ..
             -0.000000788655255351075 -0.000000565513177495597 -0.000000408061760713412 ..
             -0.000000296135527325685 -0.000000216199614067496 -0.000000158877175624088 ..
             -0.000000117559505708175 -0.000000087652252566594 -0.000000065896189948877 ..
             -0.000000050019885953352 -0.000000038349930079084 -0.000000029731868418532 ..
             -0.00000002333788383574 -0.000000018555967332286 -0.00000001495720522105 ..
             -0.000000012227539822866 -0.000000010135462452536 -0.000000008514427325498];
acc_ref = [acc_x_ref;zeros(alt_ref); zeros(alt_ref)];

// Result values
acc_res = CL_fo_dragAcc(vel_ref, rho_ref, coefd_ref);

// Check
eps = 1.e-14;
for i=1:size(acc_ref,2)
    TEST_OK($+1)= abs(acc_ref(1,i)-acc_res(1,i)) < eps &..
                  abs(acc_ref(2,i)-acc_res(2,i)) < eps &..
                  abs(acc_ref(3,i)-acc_res(3,i)) < eps;
end
