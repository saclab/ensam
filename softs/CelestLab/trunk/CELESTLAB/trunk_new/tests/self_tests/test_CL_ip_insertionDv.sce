// ------------------------------
// CL_ip_insertionDv: 
// Verification du DV d'insertion en appliquant le DV sur l'orbite hyperbolique
// et en verifiant qu'on obtient la bonne orbite elliptique
// ------------------------------

TEST_OK = [];

// Cas 1 : Insertion around the Earth (LEO)
vinf = 6e3;
rph = 6600.e3;
sma = 6700.e3;
anvh = linspace(0,1,20); // Keep the values below infinity true hyperbolic anomaly

// Computing maneuver
[dv,ecc,tanoe] = CL_ip_insertionDv(vinf,rph,sma,anvh);

// Hyperbolic orbit (Keplerian)
[sma_hyp,ecc_hyp] = CL_op_rpvinf2ae(rph,vinf);
anmh = CL_kp_v2M(ecc_hyp,anvh); // mean anomaly (hyperbolic orbit)
kep_hyp = [sma_hyp*ones(anmh); ecc_hyp*ones(anmh); 1*ones(anmh); 2*ones(anmh) ;3*ones(anmh) ; anmh];

// Apply the DV:
DV = [-dv; zeros(dv); zeros(dv)]; // in tnw frame (dv is tangential)
kep_ell = CL_man_applyDvKep(kep_hyp,DV, "tnw");

// New semi-major axis and eccentricity (elliptic)
sma_ell = kep_ell(1,:);
ecc_ell = kep_ell(2,:);

// Check true elliptic anomaly 
// (compare in absolute precision because close to 0 the relative precision is not good)
anv_ell = CL_kp_M2v(ecc_ell, kep_ell(6,:));
TEST_OK($+1) = CL__isEqual(tanoe,anv_ell,1e-12,%f);

// Check sma and eccentricity
TEST_OK($+1) = CL__isEqual(sma_ell,sma*ones(anvh));
TEST_OK($+1) = CL__isEqual(ecc_ell,ecc, 1e-12);


//Cas 3: Negative semi-axis
SemiAxis  = [-350.e3 , %CL_eqRad+700.e3];
try
  [DVMan, EccEll, TrueAnomEll] = CL_ip_insertionDv(Vinfinity, RadPerig, SemiAxis);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  
 
//Cas 4: Negative V-infinity
Vinfinity = [-5 , 6] * 1.e3;
SemiAxis  = [%CL_eqRad+350.e3 , %CL_eqRad+700.e3];
try
  [DVMan, EccEll, TrueAnomEll] = CL_ip_insertionDv(Vinfinity, RadPerig, SemiAxis);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

//Cas 5: Negative Periapsis radius
Vinfinity = [5 , 6] * 1.e3;
RadPerig  = [-250.e3 , %CL_eqRad+500.e3];
try
  [DVMan, EccEll, TrueAnomEll] = CL_ip_insertionDv(Vinfinity, RadPerig, SemiAxis);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

//Cas 6: True anomaly of hyperbolic orbit out of bounds 
RadPerig     = [%CL_eqRad+250.e3 , %CL_eqRad+500.e3];
InitTAnomHyp = [0.0 , 35.0];
try
  [DVMan, EccEll, TrueAnomEll] = CL_ip_insertionDv(Vinfinity, RadPerig, SemiAxis, %CL_mu, InitTAnomHyp);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  
