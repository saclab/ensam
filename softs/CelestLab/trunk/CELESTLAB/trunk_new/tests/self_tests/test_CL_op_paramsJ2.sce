// ------------------------------
// CL_op_paramsJ2 : 
// ------------------------------

TEST_OK = [];

///////////////////////////////////////////////
// Test1: Cross validation with CL_kp_params (in the case j2 = 0)//
///////////////////////////////////////////////
// Note: CL_op_paramsJ2 shall provide same results as CL_kp_params when j2 = 0;

sma = 7078.e3 ;
ecc = 0.0 ;
inc = 0.0;
j2 = 0.0;

[mm_J2,per_J2,nodper_J2,lgap_J2] = CL_op_paramsJ2(['mm' 'per' 'nodper' 'lgap'], ..
                                                    sma,ecc,inc,j2);
[mm_kep,per_kep,lgap_kep] = CL_kp_params (['mm' 'per' 'lgap'],sma);

TEST_OK(1+$)= CL__isEqual(mm_J2,mm_kep) & CL__isEqual(per_J2,per_kep) & ..
              CL__isEqual(lgap_J2,lgap_kep);

//////////////////////////////////////////////////////////////
// Test2: Unknown type of type_output (It's case sensitive) //
//////////////////////////////////////////////////////////////
sma = 7078.e3 ;
ecc = 0.001 ;
inc = CL_deg2rad(98);

try
    [mm,per,nodper,lgap] = CL_op_paramsJ2(['mm' 'PER' 'nodper' 'lgap'], sma,ecc,inc);
    TEST_OK(1+$) = %f;
catch
    TEST_OK(1+$) = %t;
end

//////////////////////////////////////
// Test3: Invalid size for %CL_j1jn //
//////////////////////////////////////

function [test_ok] = test3()
  // This test is nested into a function
  // so that correct result is provided when manually executed
  
  // %CL_j1jn becomes a 3x1 vector producing an error
  %CL_j1jn(3) = 0;
  sma = 7078.e3 ;
  ecc = 0.001 ;
  inc = CL_deg2rad(98);
  
  try 
      CL_op_paramsJ2('lgap',sma,ecc,inc);
      test_ok = %f;
  catch
      test_ok = %t;
  end
  
endfunction 

TEST_OK(1+$) = test3();
