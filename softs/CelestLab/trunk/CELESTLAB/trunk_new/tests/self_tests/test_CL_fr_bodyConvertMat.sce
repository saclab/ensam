// ----------------------------------------------------------------------------------------
// CL_fr_bodyConvertMat
// Test for catching possible errors
// Internal coherence test
// ----------------------------------------------------------------------------------------

TEST_OK = [];

//////////////////////////////////
// Test 1: Not enough arguments //
//////////////////////////////////

try 
    CL_fr_bodyConvertMat("Mars","ICRS","BCI");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

//////////////////////////////////////////////
// Test 2 : Invalid input argument ''body'' //
//////////////////////////////////////////////
Mars = [];
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
try
    [M,omega] = CL_fr_bodyConvertMat(Mars,"ICRS","BCI",cjd);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

///////////////////////////////////////////////
// Test 3: Invalid input argument ''frame1'' //
///////////////////////////////////////////////
ICRS = [];
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
try
    [M,omega] = CL_fr_bodyConvertMat("Mars",ICRS,"BCI",cjd);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

///////////////////////////////////////////////
// Test 4: Invalid input argument ''frame2'' //
///////////////////////////////////////////////
BCI = [];
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
try
    [M,omega] = CL_fr_bodyConvertMat("Mars","ICRS",BCI,cjd);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

//////////////////////////////////////////////////
// Test 5: Invalid frame name  //
//////////////////////////////////////////////////
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
try
    [M,omega] = CL_fr_bodyConvertMat("Mars","ICSR","BCI",cjd);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

/////////////////////////////////////////////////
// Test 6: Invalid body name  //
/////////////////////////////////////////////////
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);
try
    [M,omega] = CL_fr_bodyConvertMat("Masr","ICRS","BCI",cjd);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

/////////////////////////////////////
// Test 7: Internal coherence test //
/////////////////////////////////////
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);

[M_BCI,omega_BCI] = CL_fr_bodyConvertMat("Mars","ICRS","BCI",cjd);
[M_BCF,omega_BCF] = CL_fr_bodyConvertMat("Mars","BCI","BCF",cjd);
[M_ICRS,omega_ICRS] = CL_fr_bodyConvertMat("Mars","BCF","ICRS",cjd);
[M_BCI_1,omega_BCI_1] = CL_fr_bodyConvertMat("Mars","ICRS","BCI",cjd);

TEST_OK(1+$) = CL__isEqual(M_BCI,M_BCI_1)
TEST_OK(1+$) = CL__isEqual(omega_BCI,omega_BCI_1)

//////////////////////////////////////////////////////////////////
// Test 8: omega = zeros[] and M = ones[] when frame1 == frame2 //
//////////////////////////////////////////////////////////////////
cjd = CL_dat_cal2cjd(2010,02,03,05,35,25);

[M,omega]=CL_fr_bodyConvertMat("Moon","BCI","BCI",cjd);

TEST_OK(1+$) = CL__isEqual(M,eye(3,3)) & CL__isEqual(omega,zeros(3,1));