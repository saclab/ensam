// ------------------------------
// CL_man_dvApsidesLines : 
// ------------------------------

TEST_OK = [];

// --- Test 1 : 
ai = 7200.e3;
ei = 0.1;
pomi = 1;
pomf = 1.1;
[deltav,dv,anv] = CL_man_dvApsidesLine(ai,ei,pomi,pomf,posman=1)
// Check results:
anm = CL_kp_v2M(ei,anv);
kep = [ai; ei; %pi/2; pomi; 0.1; anm];
kep1 = CL_man_applyDvKep(kep,dv);


TEST_OK($+1) = CL__isEqual(ai, kep1(1)) & ...            // sma
               CL__isEqual(ei, kep1(2), 1e-14) & ...     // ecc
               CL__isEqual(kep(3), kep1(3)) & ...        // inc
               CL__isEqual(pomf, kep1(4), 1e-14) & ...   // pom
               CL__isEqual(kep(5), kep1(5));             // gom

