// ------------------------------------------
// Test of CL_kp_anomConvertCir
// Check consistency only because it only
// calls other tested functions. 
// ------------------------------------------

// Initialization
TEST_OK = [];
// ------------------------------------------
// TEST 1 : 
// Elliptic orbits only
//------------------------------------------

// Components of eccentricity vector
ex = [1e-11, 0.01, 0.05,0.2, 0.5];
ey = [2e-5, 0.001, 0.004, 0.1, 0.3];

// Eccentric argument of latitude
psoE = [0, 1e-11, %pi-1e-8, %pi, %pi+1e-8] ;

// Check CL_kp_M2E <--> CL_kp_E2M
psoM  = CL_kp_anomConvertCir("E", "M", ex, ey, psoE);
psoE1 = CL_kp_anomConvertCir("M", "E", ex, ey, psoM);

TEST_OK($+1) = CL__isEqual(psoE,psoE1,1e-13, relative = %f);

// Check CL_kp_M2v <--> CL_kp_v2M
psov  = CL_kp_anomConvertCir("M", "v", ex, ey, psoM);
psoM1 = CL_kp_anomConvertCir("v", "M", ex, ey, psov);

TEST_OK($+1) = CL__isEqual(psoM,psoM1,1e-13, relative = %f);

// Check CL_kp_E2v <--> CL_kp_v2E
psov  = CL_kp_anomConvertCir("E", "v", ex, ey, psoE);
psoE1 = CL_kp_anomConvertCir("v", "E", ex, ey, psov);

TEST_OK($+1) = CL__isEqual(psoE,psoE1,1e-13, relative = %f);
