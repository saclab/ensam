// ------------------------------
// CL_gm_inters3dLineEllips : 
// ------------------------------

TEST_OK = [];


// Fonction de verification que la solution est correcte
function OK = verif_solution(pos,u,posc,er,obla, P1,P2)

  OK = %t;

  // Les points doivent se trouver sur l'ellipsoide (i.e leur altitude est nulle)
  pos_ell = CL_co_car2ell(P1-posc , er, obla);
  I = find ( abs(pos_ell(3,:)) > er * 1e-14);
  if (I~=[]); OK = %f; end;
  
  pos_ell = CL_co_car2ell(P2-posc , er, obla);
  I = find ( abs(pos_ell(3,:)) > er * 1e-14);
  if (I~=[]); OK = %f; end;
  
  // La direction pos-->P1,P2 doit etre alignee avec u
  angle = CL_vectAngle(P1-pos,u);
  I = find ( abs(angle) > 1e-15);
  if (I~=[]); OK = %f; end;
  angle = CL_vectAngle(P2-pos,u);
  I = find ( abs(angle) > 1e-15);
  if (I~=[]); OK = %f; end;

endfunction


// Test 1 : Cas ou pos est en dehors de l'ellipsoide, et il y a 2 intersections identiques
// (la demi droite est tangente a l'ellipsoide)
// NB : choix de petites valeurs numeriques, pour eviter les problemes de precision numerique.
er = 1;
obla = 0.5; // pas d'impact
posc = [0;0;0];
pos = [ er ; er ; 0 ] ;
u = [ -1 ; 0 ; 0] ;
[P1,P2] = CL_gm_inters3dLineEllips(pos,u,posc,er,obla);
TEST_OK($+1) = CL__isEqual(P1,[0;1;0]) & CL__isEqual(P2,[0;1;0]);


// Pour le reste des runs : choix d'un ellipsoide quelconque
er = 1.1 * %CL_eqRad ;
obla = 0.8 * %CL_obla;
posc = [10;5;-5];

// Test 2 : Cas ou pos est en dehors de l'ellipsoide, et il y a 2 intersections
pos = [ 10000 ; 100 ; 500 ] * 1000 ;
u = [ -1 ; 0.2 ; 0.6] ;
[P1,P2] = CL_gm_inters3dLineEllips(pos,u,posc,er,obla);
TEST_OK($+1) = verif_solution(pos,u,posc,er,obla, P1,P2);

// Test 3 : Cas ou pos est en dehors de l'ellipsoide, et il y a 0 intersection
pos = [ 10000 ; 100 ; 500 ] * 1000 ;
u = - [ -1 ; 0.2 ; 0.6] ;
[P1,P2] = CL_gm_inters3dLineEllips(pos,u,posc,er,obla);
TEST_OK($+1) = find(~isnan(P1)) == [] & find(~isnan(P2)) == [];

// Test 4 : Cas ou pos est a l'interieur de l'ellipsoide, et il y a 1 intersection
pos = [ 100 ; 100 ; 500 ] * 1000 ;
u = - [ -1 ; 0.2 ; 0.6] ;
[P1,P2] = CL_gm_inters3dLineEllips(pos,u,posc,er,obla);
TEST_OK($+1) = verif_solution(pos,u,posc,er,obla, P1,P1) & find(~isnan(P2)) == [] ;



