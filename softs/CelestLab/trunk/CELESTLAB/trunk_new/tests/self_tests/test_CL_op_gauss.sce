// ------------------------------------------
// Consistency test of function CL_op_gauss 
// ------------------------------------------

TEST_OK = [];

a = 7000.e3;
e = 0.01;
inc = 98*%CL_deg2rad;
pom = 0.4;
gom = 0.2;
anm = 0.3;

kep = [a;e;inc;pom;gom;anm];
cir = [a;e*cos(pom);e*sin(pom);inc;gom;pom+anm];
cireq = [a;e*cos(pom+gom);e*sin(pom+gom);sin(inc/2)*cos(gom);sin(inc/2)*sin(gom);pom+gom+anm];
equin = [a;e*cos(pom+gom);e*sin(pom+gom);tan(inc/2)*cos(gom);tan(inc/2)*sin(gom);pom+gom+anm];

v = CL_kp_M2v(e,anm);
n = sqrt(%CL_mu/a^3);

frames = ["qsw", "tnw"];

// --------------------
// TEST 1/2 :
// Coherence of CIR/KEP
// --------------------
for k = 1 : 2
  [M,N] = CL_op_gauss("cir",cir,frames(k));
  [M2,N2] = CL_op_gauss("kep",kep,frames(k));

  // We should have :
  // N(cir) = N(kep)
  // da/dt (cir) = da/dt (kep)
  // dex/dt (cir) = cos(w)*de/dt -e*sin(w)*dw/dt  (kep)
  // dey/dt (cir) = sin(w)*de/dt + e*cos(w)*dw/dt  (kep)
  // di/dt (cir) = di/dt (kep)
  // dgom/dt (cir) = dgom/dt (kep)
  // dalpha/dt (cir) = dpom/dt + dM/dt(kep)

  TEST_OK($+1) = CL__isEqual(N, N2) & ...
                 CL__isEqual(M(1,:), M2(1,:)) & ...
                 CL__isEqual(M(2,:), cos(pom)*M2(2,:) - e*sin(pom)*M2(4,:))  & ...
                 CL__isEqual(M(3,:), sin(pom)*M2(2,:) + e*cos(pom)*M2(4,:)) & ...
                 CL__isEqual(M(4,:), M2(3,:)) & ...
                 CL__isEqual(M(5,:), M2(5,:)) & ...
                 CL__isEqual(M(6,:), M2(4,:)+M2(6,:), 1e-11);
end
     
     
// --------------------
// TEST 3/4 :
// Coherence of CIREQ/KEP
// --------------------
for k = 1 : 2
  [M,N] = CL_op_gauss("cireq",cireq,frames(k));
  [M2,N2] = CL_op_gauss("kep",kep,frames(k));

  // We should have :
  // N(cireq) = N(kep)
  // da/dt (cireq) = da/dt (kep)
  // dex/dt (cireq) = cos(w+gom)*de/dt -e*sin(w+gom)*dw/dt -e*sin(w+gom)*dgom/dt  (kep)
  // dey/dt (cireq) = sin(w+gom)*de/dt + e*cos(w+gom)*dw/dt + e*cos(w+gom)*dgom/dt  (kep)
  // dix/dt (cireq) = 1/2 * cos(i/2) * cos(gom) * di/dt - sin(i/2)*sin(gom)*dgom/dt (kep)
  // diy/dt (cireq) = 1/2 * cos(i/2) * sin(gom) * di/dt + sin(i/2)*cos(gom)*dgom/dt (kep)
  // dalpha/dt (cireq) = dpom/dt + dgom/dt + dM/dt(kep)
  TEST_OK($+1) = CL__isEqual(N, N2) & ...
                 CL__isEqual(M(1,:), M2(1,:)) & ...
                 CL__isEqual(M(2,:), cos(pom+gom)*M2(2,:) - e*sin(pom+gom)*M2(4,:) - e*sin(pom+gom)*M2(5,:))  & ...
                 CL__isEqual(M(3,:), sin(pom+gom)*M2(2,:) + e*cos(pom+gom)*M2(4,:) + e*cos(pom+gom)*M2(5,:)) & ...
                 CL__isEqual(M(4,:), 1/2 * cos(inc/2) * cos(gom) * M2(3,:) - sin(inc/2)*sin(gom)*M2(5,:)) & ...
                 CL__isEqual(M(5,:), 1/2 * cos(inc/2) * sin(gom) * M2(3,:) + sin(inc/2)*cos(gom)*M2(5,:)) & ...
                 CL__isEqual(M(6,:), M2(4,:)+M2(5,:)+M2(6,:), 1e-11);
end
               
          
// --------------------
// TEST 5/6 :
// Coherence of EQUIN/KEP
// --------------------
for k = 1 : 2
  [M,N] = CL_op_gauss("equin",equin,frames(k));
  [M2,N2] = CL_op_gauss("kep",kep,frames(k));

  // We should have :
  // N(cireq) = N(kep)
  // da/dt (equin) = da/dt (kep)
  // dex/dt (equin) = cos(w+gom)*de/dt -e*sin(w+gom)*dw/dt -e*sin(w+gom)*dgom/dt  (kep)
  // dey/dt (equin) = sin(w+gom)*de/dt + e*cos(w+gom)*dw/dt + e*cos(w+gom)*dgom/dt  (kep)
  // dhx/dt (equin) = 1/2 * (1 + tan(i/2)^2) * cos(gom) * di/dt - tan(i/2)*sin(gom)*dgom/dt (kep)
  // dhy/dt (equin) = 1/2 * (1 + tan(i/2)^2) * sin(gom) * di/dt + tan(i/2)*cos(gom)*dgom/dt (kep)
  // dalpha/dt (cir) = dpom/dt + dgom/dt + dM/dt(kep)

  TEST_OK($+1) = CL__isEqual(N, N2) & ...
                 CL__isEqual(M(1,:), M2(1,:)) & ...
                 CL__isEqual(M(2,:), cos(pom+gom)*M2(2,:) - e*sin(pom+gom)*M2(4,:) - e*sin(pom+gom)*M2(5,:))  & ...
                 CL__isEqual(M(3,:), sin(pom+gom)*M2(2,:) + e*cos(pom+gom)*M2(4,:) + e*cos(pom+gom)*M2(5,:)) & ...
                 CL__isEqual(M(4,:), 1/2 * (1 + tan(inc/2).^2) * cos(gom) * M2(3,:) - tan(inc/2)*sin(gom)*M2(5,:)) & ...
                 CL__isEqual(M(5,:), 1/2 * (1 + tan(inc/2).^2) * sin(gom) * M2(3,:) + tan(inc/2)*cos(gom)*M2(5,:)) & ...
                 CL__isEqual(M(6,:), M2(4,:)+M2(5,:)+M2(6,:), 1e-11);
end

