// ------------------------------
// CL_ev_eclipse
// compute eclipse intervals
// Test1: Event type umbra (typ = "umbc")
// Test2: Event type umbra (typ = "umb")
// ------------------------------

// ===========================================================
// Data generation
// ===========================================================
cjd0 = 22000; 
kep0 = [7.e6; 0; 1; 0; 0; 0]; 
cjd = cjd0 + (0 : 5/1440 : 1); 
kep = CL_ex_secularJ2(cjd0, kep0, cjd); 

pos = CL_oe_kep2car(kep); 
sun = CL_eph_sun(cjd); 

// computation options
opts = struct();
opts.prec = 1.e-4 * %CL_deg2rad; // not the default value


TEST_OK = [];
 
// ===========================================================
// Test1
// typ = "umbc"
// results are checked using "CL_gm_eclipseCheck" 
// ===========================================================

// Start/end of eclipse periods (umbra) for an observer in space:
interv = CL_ev_eclipse(cjd, pos, sun, typ = "umbc", opts = opts);

// Recompute val at beginning/end of eclipses
pos_ecl = CL_interpLagrange(cjd, pos, matrix(interv, 1, -1), n = 8);
sun_ecl = CL_interpLagrange(cjd, sun, matrix(interv, 1, -1), n = 8);
val = CL_gm_eclipseCheck(pos_ecl, sun_ecl, zeros(sun_ecl), %CL_body.Sun.eqRad, %CL_eqRad, "angc");

TEST_OK($+1) = (max(abs(val)) < opts.prec);


// ===========================================================
// Test2
// typ = "umb"
// results are checked using "CL_gm_eclipseCheck"
// ===========================================================

// Start/end of eclipse periods (umbra) for an observer in space:
interv = CL_ev_eclipse(cjd, pos, sun, typ = "umb", opts = opts);

// Recompute val at beginning/end of eclipses
pos_ecl = CL_interpLagrange(cjd, pos, matrix(interv, 1, -1), n = 8);
sun_ecl = CL_interpLagrange(cjd, sun, matrix(interv, 1, -1), n = 8);
val = CL_gm_eclipseCheck(pos_ecl, sun_ecl, zeros(sun_ecl), %CL_body.Sun.eqRad, %CL_eqRad, "angu");

TEST_OK($+1) = (max(abs(val)) < opts.prec);
