// -------------------------------------------------------------------
// CL_covDraw
// Cross validation with CL_stat
// -------------------------------------------------------------------

TEST_OK = [];

///////////////////////////////////////////
// Test 1: Cross validation with CL_stat //
///////////////////////////////////////////
pos = zeros(3,1);
cova = eye(3,3);

// After several manual executions it has been estimated that the
// difference between the reference value and the estimated one is 
// around 0.08-0.09. If other function different from 'rand' is 
// used within CL_stat function, the accuracy would be better.
eps = 0.08;

[drawn_pos] = CL_covDraw(pos,cova,5000);
// estimate mean values and covariance from samples
[pos2, cov2] = CL_stat(drawn_pos);

TEST_OK($+1) = abs(max(abs(pos)-abs(pos2))) < eps;
TEST_OK($+1) = abs(max(abs(cova)-abs(cov2)))< eps;
