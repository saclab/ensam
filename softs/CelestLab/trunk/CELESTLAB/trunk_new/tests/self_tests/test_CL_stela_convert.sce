//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// ------------------------------
// TEST CL_stela_convert
// Test mean => osc and  osc => mean conversion
// for various types of orbital elements
// ------------------------------

TEST_OK = [];


// Internal function: 
// Check consistency: mean -> osculating -> mean
// (mean_kep are first converted using the type in argument)
function [test_ok] = TEST(mean_kep, type_oe)

  cst = CL_stela_getConst(); 

  // Convert from "kep" to type_oe
  mean_params = CL_oe_convert("kep", type_oe, mean_kep, mu=cst.mu);

  // Convert to osculating elements
  osc_params   = CL_stela_convert("mean2osc", type_oe, cjd, mean_params, params);
  
  // Check: convert back to mean elements
  mean_params2 = CL_stela_convert("osc2mean", type_oe, cjd, osc_params, params);
  
  epsilon = 1.e-11;
  test_ok = CL__isEqual(mean_params2, mean_params, prec = epsilon);
  
endfunction


// Date (cjd, TREF) and Keplerian mean orbital elements
cjd = 20000;

// Mean Keplerian elements
mean_kep = [7.e6; 1.e-3; 1.7; 1; 2; 3];

// Default parameters for stela
params = CL_stela_params();


// type_oe == "kep"
TEST_OK($+1) = TEST(mean_kep, "kep");

// type_oe == "cir"
TEST_OK($+1) = TEST(mean_kep, "cir");

// type_oe == "cireq"
TEST_OK($+1) = TEST(mean_kep, "cireq");

// type_oe == "equin"
TEST_OK($+1) = TEST(mean_kep, "equin");

// type_oe == "pv"
TEST_OK($+1) = TEST(mean_kep, "equin");


