// ------------------------------
// CL_ev_visibilityExtr
// (backward compatibility test with old CL_ev_visibility function)
// ------------------------------

TEST_OK = [];

t0 = 21915;
mean_kep0 = [7070.e3 ; 0.001 ; CL_deg2rad([98;90;10;15])];

// Definition of ground stations
sta1 = [CL_deg2rad(2);CL_deg2rad(70);200]; // high latitude
sta2 = [CL_deg2rad(20);CL_deg2rad(0);400]; // equator
stations = [sta1,sta2];
stations_masks = [ CL_deg2rad(30) , CL_deg2rad(5) ];

sim_period = [21915 ; 21916];  // 1 day

// Reference value obtained with :
// [visi_dates_ref] = CL_ev_visibility(t0, mean_kep0, stations, stations_masks, sim_period);
// CL__string(visi_dates_ref,printscreen=%t);


visi_dates_ref = [21915.149238753565 21915.215960184069 21915.273661138857 21915.341542370479 21915.610674978019 21915.662543578084 21915.6776029057 21915.730579484065 21915.746653566781
21915.154738613062 21915.223209209613 21915.276620200399 21915.34448879019 21915.612608519314 21915.669488315129 21915.680877146442 21915.736650007879 21915.748129828418];

ECI_frame = CL_configGet("ECI_FRAME");
// CL_configSet("ECI_FRAME", "Veis"); Not allowed!

// Workaround:
global %CL__PRIV;
%CL__PRIV.PREF.ECI_FRAME = "Veis";

// Note : ut1_tref was not taken into account in CL_ev_visibility
//        so we set it to zero.
ut1_tref = 0;
[visi_dates] = CL_ev_visibilityExtr(t0, "kep", mean_kep0, stations, stations_masks, sim_period, ut1_tref=ut1_tref, propag_model="lydsec");
TEST_OK($+1) = CL__isEqual(visi_dates,visi_dates_ref);

// Restore value for ECI_frame:
global %CL__PRIV;
%CL__PRIV.PREF.ECI_FRAME = ECI_frame;