// ------------------------------
// Consistency tests of functions :
// CL_rot_quat2angles
// CL_rot_angles2quat
// CL_rot_angles2matrix
// CL_rot_matrix2quat
// ------------------------------

TEST_OK = [];

// naxes_tab = all possible rotation orders
naxes_tab = [ [1 2 3] ; [1 3 2] ; [2 1 3] ; [2 3 1] ; [3 1 2] ; [3 2 1] ; [1 2 1] ; [1 3 1] ; [2 1 2] ; [2 3 2] ; [3 1 3] ; [3 2 3]];
angles_tab = [ [4 ; 0.2 ; 2] , [ 4 ; 2 ; 3] , [ 4 ; 4.1 ; 2] ];

TEST_OK = %t;

q_identity = CL_rot_defQuat([1;0;0;0]);
for k = 1 : size(naxes_tab,1)
  for j = 1 : size(angles_tab, 2)
    naxes = naxes_tab(k,:);
    angles1 = angles_tab(:,j);
  
    q1 = CL_rot_angles2quat(naxes, angles1);
    angles2 = CL_rot_quat2angles(q1,naxes);
    q2 = CL_rot_angles2quat(naxes, angles2);
    
    // Check that quat2angles followed by angles2quat return the same initial quaternion
    if(q1 * q2 ~= q_identity) TEST_OK = %f; end;
    
    M1 = CL_rot_angles2matrix(naxes,angles1);
    q1b = CL_rot_matrix2quat(M1);
    // Check that quaternions returned by angles2quat, matrix2quat(angles2matrix) are equal
    if(q1 * q1b ~= q_identity); TEST_OK = %f; end;
    
  end
end
