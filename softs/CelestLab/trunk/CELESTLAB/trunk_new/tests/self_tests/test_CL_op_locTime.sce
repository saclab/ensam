// ------------------------------
// CL_op_locTime
// (backward compatibility test with old CL_op_locTimeG50 function)
// ------------------------------

TEST_OK = [];

cjd = [15890.123155, 25.262323];
type_par1 = "mlh";
par1 = 0.1e2;
type_par2 = 'all';
ut1_utc = 0.25;

// Reference value obtained with : 
// par2_ref = CL_op_locTimeG50(cjd,type_par1,par1,type_par2,ut1_utc);
// CL__string(par2_ref,printscreen=%t);
par2_ref = struct("ra",[1.2492400812328084,4.7992086530793703] , ..
"lon",[1.8441700109728059,0.96975167814318208] , ..
"tsa",[5.7406112343899931,5.7047381752232402] , ..
"msa",[5.7595865315812871,5.7595865315812871] , ..
"tlh",[9.9275197037288194,9.7904947111636247] , ..
"mlh",[10,10] , ..
"sidt",[5.6882553774395888,3.8294569749361882] , ..
"stra",[1.7918141540224011,5.3776557850357163] , ..
"smra",[1.7728388568311075,5.3228074286776694]);



ECI_frame = CL_configGet("ECI_FRAME");
// CL_configSet("ECI_FRAME", "Veis"); Not allowed!

// Workaround:
global %CL__PRIV;
%CL__PRIV.PREF.ECI_FRAME = "Veis";

// Note : tt_tref was not taken into account in CL_op_locTimeG50
//        so we set it to zero.
tt_tref = 0;
par2 = CL_op_locTime(cjd,type_par1,par1,type_par2,ut1_utc,tt_tref);

// Sun model was modified so an error on tsa, tlh, stra is expected
TEST_OK($+1) = CL__isEqual([par2.tsa;par2.tlh;par2.stra],[par2_ref.tsa;par2_ref.tlh;par2_ref.stra], 1e-4);

// There is a difference of s' on other parameters
// (ECI is considered to be TIRS while it was PEF for CL_op_locTimeG50)
// s' is about 1e-10.
TEST_OK($+1) = CL__isEqual([par2.ra;par2.lon;par2.msa;par2.mlh;par2.sidt;par2.smra], ...
                           [par2_ref.ra;par2_ref.lon;par2_ref.msa;par2_ref.mlh;par2_ref.sidt;par2_ref.smra], 1e-10);


// Restore value for ECI_frame:
global %CL__PRIV;
%CL__PRIV.PREF.ECI_FRAME = ECI_frame;