// ==========================================
// Test CL_op_latSwath
// ==========================================
// Tests principles
// 
// - perform verification on all passes conditions
// - compare longitudes and dates with numerically computed values
// in a limited number of cases (the formulas are always the same)
// - verify that the longitude intervals match what is expected
//
// NOTE: dates not checked as added lately !!! 
//
// ==========================================
// Notes:
// - All tests are performed for inc < 90 deg AND inc > 90 deg
// - All tests are performed for positive AND negative latitude EXCEPT
//   those where interpolation is used to verify results (Tests 1-3-4)
//
// ==========================================

// --------------------------------------------
// Auxiliary Functions
// -------------------------------------------

// --------------------------------------------
// Interpolate ascending pass
//
// sma: Semi major axis [m] (1xN)
// inc: Inclination [rad] (1xN)
// lat: Latitude [rad] (1xN)
// angc: Instrument's center angles [rad] (1xN or 2xN)
// dlona: Longitude interval for ascending pass [rad] (2x1)
// -------------------------------------------
function [dlona, dta] = compute_fovProj(sma, inc, lat, angc)

  // Generate orbit
  t0 = 0; // initial time
  cir0 = [sma; 0; 0; inc; 0; 0]; // orbit parameters
  
  nodper = CL_op_paramsJ2(["nodper"],cir0(1), 0, cir0(4)); 
  
  t = t0 + linspace(-nodper/4, nodper/4, 1000) / 86400;  // days
  
  cir = CL_ex_propagate("j2sec", "cir", t0, cir0, t, "m"); // orbit propagation
  
  [pos, vel] = CL_oe_cir2car(cir); // inertial position and velocity
  
  // Generate swath points (arbitrary radius)
  omega = %CL_rotrBody; 
  M =  CL_rot_angles2matrix(3, omega * t * 86400); 
 
  // Express vectors in terrestrial (rotating) frame
  p0 = M * pos; 
  p1 = M * CL_fr_qsw2inertial(pos, vel, [cos(angc); 0;  sin(angc)]);
  p2 = M * CL_fr_qsw2inertial(pos, vel, [cos(angc); 0; -sin(angc)]);
 
  sph1 = CL_co_car2sph(p1); 
  sph2 = CL_co_car2sph(p2); 
  
  t1 = CL_detectZero(t, sph1(2,:) - lat, ytol=1.e-12); 
  lon1 = CL_interpLagrange(t, CL_rMod(sph1(1,:), -%pi, %pi), t1); 
  lat1 = CL_interpLagrange(t, sph1(2,:), t1);
  
  t2 = CL_detectZero(t, sph2(2,:) - lat, ytol=1.e-12); 
  lon2 = CL_interpLagrange(t, CL_rMod(sph2(1,:), -%pi, %pi), t2); 
  lat2 = CL_interpLagrange(t, sph2(2,:), t2);
  
  dlona = [lon1; lon2];
  dta = [t1;t2];

  if (inc > %pi/2)
    dta = [t2; t1];
  end
  
endfunction

// -----------------------------------------
// Tests case: latitude visible for all pso (ncase == 3)
// 
// inc: inclination [rad]
// dlona: Longitude interval for ascending pass [rad] (2x1)
// dlond: Longitude interval for descending pass [rad] (2x1)
// testres: (bool) Test's result 
// -----------------------------------------
function [testres] = test_ncase3(inc, dlona, dlond)
  testres = [];
  
  if (inc <= %pi/2)
    dlon  = [dlond(2,1) - dlona(1,1), dlona(2,2) - dlond(1,2)];
    dlonref = 2*%pi + info.lgap;
    // longitude of mid point of the longitude interval
    dlon_mid = [[dlona(2,1); dlond(1,1)], [dlona(1,2); dlond(2,2)]];
  else
    dlon  = [dlona(2,1) - dlond(1,1), dlond(2,2) - dlona(1,2)];
    dlonref = 2*%pi - info.lgap; // lgap < 0
    // longitude of mid point of the longitude interval
    dlon_mid = [[dlona(1,1); dlond(2,1)], [dlona(2,2); dlond(1,2)]];    
  end
  
  // Checks that the interval for descending pass is adjacent to interval for asc. pass
  testres($+1) = CL__isEqual(dlon_mid(1,:), dlon_mid(2,:));
  // Checks the longitude span which is seen during one orbital period 
  testres($+1) = CL__isEqual(dlon, dlonref);
  // Checks configuration
  testres($+1) = CL__isEqual(info.ncase, [4,4]);

endfunction

// -----------------------------------------
// Test case: small circle is entirely visible (1 passage) (ncase == 4)
//
// inc: inclination [rad]
// dlona: Longitude interval for ascending pass [rad] (2x1)
// dlond: Longitude interval for descending pass [rad] (2x1)
// testres: (bool) Test's result 
// -----------------------------------------
function [testres] = test_ncase4(inc, dlona, dlond)
  
  testres = [];
  
  if (inc <= %pi/2)
    // longitude span at latitude
    dlon = (dlona(2,:) - dlona(1,:))  + (dlond(2,:) - dlond(1,:));
    // longitude of first intersection (compare with interpolated value)
    dlona_test1 = dlona(1,:);
    // Middle point
    dlon_mid = (dlona(1,:) + dlond(2,:)) / 2;
    // commons bounds (to be compared with middle point)
    dlon_midref = [dlona(2,1),dlona(1,2)];
    
  else  
    // longitude span at latitude
    dlon = (dlond(2,:) - dlond(1,:))  + (dlona(2,:) - dlona(1,:));
    // longitude of first intersection (compare with interpolated value)
    dlona_test = dlona(2,:);
    // Middle point
    dlon_mid = (dlond(1,:) + dlona(2,:)) / 2;
    // commons bounds (to be compared with middle point)
    dlon_midref = [dlona(1,1),dlona(2,2)];
  end
  
  // Checks longitude span is 2%pi
  testres($+1) = CL__isEqual(dlon, [2*%pi, 2*%pi]);
  // Checks that interval is centerd about pso = %pi/2
  testres($+1) = CL__isEqual(dlon_mid, dlon_midref, rel = %f, prec = 1e-10);
  // Checks configuration
  testres($+1) = CL__isEqual(info.ncase, [3,3]);
  
endfunction

// -----------------------------------------
// Execute tests (described in the header of this file)
// 
// tabinc: Inclination [rad] (1xN)
// -----------------------------------------
function [test_ok] = test(tabinc)

  test_ok = [];

  // Common sem-major axis values
  sma = %CL_eqRad + 10000.e3; // m

  // -----------------------------------------
  // Test 1
  // 
  // Situation encountered: two intersections with latitude
  //
  // This test proves that formulas are exact up to machine precision
  //
  // For the ascending pass only (since descending pass are obtained by symmetry)
  //
  // - Compares values of the longitudes of intersections' points with numerical computation based on interpolation
  // - Compares value of approximate time of pass with numerical computation based on interpolation
  // - Checks that correct case is detected (info.ncase == 2)
  //
  // ------------------------------------------

  inc = tabinc(1); // rad
  angc = 15 * %CL_deg2rad; // rad  
  latref = 40 * %CL_deg2rad; // rad
  
  [dlona_ref1, dta_ref1] = compute_fovProj(sma, inc, latref(1), angc);
  [dlona, dlond, dta, dtd, info] = CL_op_latSwath(sma, inc,  [-angc; angc], latref,res="lti"); 
  test_ok($+1) = CL__isEqual(dlona, dlona_ref1, rel = %f, prec = 1e-10);
  test_ok($+1) = CL__isEqual(dta, dta_ref1 * 86400, rel = %f, prec = 1e-10);
  
  
  // -----------------------------------------
  // Test 2
  //
  // Situation encountered: one intersection with latitude
  //
  // - Compares value of the longitudes of first intersection point 
  //     with numerical computation based on interpolation
  // - Checks that correct case is detected (info.ncase == 1)
  //
  // ------------------------------------------
  
  inc = tabinc(1); // rad
  latref = [50, -50] * %CL_deg2rad; // rad
  
  [dlona, dlond, dta, dtd, info] = CL_op_latSwath(sma, inc,  [-angc; angc], latref,res="lti");
  [dlona_ref1] = compute_fovProj(sma, inc, latref(1), angc);
  
  if (inc <= %pi/2)
    // longitude span at latitude
    dlon = (dlona(2,:) - dlona(1,:))  + (dlond(2,:) - dlond(1,:));
    // longitude of first intersection (compare with interpolated value)
    dlona_test = dlona(1,1);
    // Middle point
    dlon_mid = (dlona(1,:) + dlond(2,:)) / 2;
    // commons bounds (to be compared with middle point)
    dlon_midref = [dlona(2,1),dlona(1,2)];
  else
    // longitude span at latitude
    dlon = (dlond(2,:) - dlond(1,:))  + (dlona(2,:) - dlona(1,:));
    // longitude of first intersection (compare with interpolated value)
    dlona_test = dlona(2,1);
    // Middle point
    dlon_mid = (dlond(1,:) + dlona(2,:)) / 2;
    // commons bounds (to be compared with middle point)
    dlon_midref = [dlona(1,1),dlona(2,2)];
  end
 
  // Checks longitude of the first intersection
  test_ok($+1) = CL__isEqual(dlona_test, dlona_ref1(1,1), rel = %f, prec = 1e-10);
  // Checks that interval is centerd about pso = %pi/2
  test_ok($+1) = CL__isEqual(dlon_mid, dlon_midref, rel = %f, prec = 1e-10);
  // Checks configuration
  test_ok($+1) = CL__isEqual(info.ncase, [1,1]);
  
  
  // -----------------------------------------
  // Test 3
  //
  // Situation encountered: latitude is entirely visible over 1 pass (polar case)
  //
  // - Checks longitude span of visible interval is 2%pi
  // - Checks that correct case is detected (info.ncase == 3)
  //
  // ------------------------------------------
  
  inc = tabinc(2); // rad
  angc = 15 * %CL_deg2rad; // rad
  latref = [85, -85] * %CL_deg2rad; // rad
  
  [dlona, dlond, dta, dtd, info] = CL_op_latSwath(sma, inc,  [-angc; angc], latref,res="lti");
  
  test_ok = [test_ok; test_ncase4(inc, dlona, dlond)];

  
  // -----------------------------------------
  // Test 4
  //
  // Situation encountered: one intersection with latitude
  //
  // For the case of 1 intersection verifies that formulas give the correct value of the intersection's longitude 
  // when the visible longitude interval is large
  //
  // - Checks ascending intersections with interpolation (proves formulas work when intervals become large)
  // - Checks that correct case is detected (info.ncase == 1)
  //
  // ------------------------------------------
  
  inc = tabinc(3); // rad
  angc = 5 * %CL_deg2rad; // rad  
  // Chose to have a large longitude interval (to verify formula)
  latref = 86.99 * %CL_deg2rad; // rad
  
  [dlona, dlond, dta, dtd, info] = CL_op_latSwath(sma, inc,  [-angc; angc], latref,res="lti");
  [dlona_ref] = compute_fovProj(sma, inc, latref, angc);

  if (inc <= %pi/2)
    // longitude of first intersection (compare with interpolated value)
    dlona_test = dlona(1);
  else
    // longitude of first intersection (compare with interpolated value)
    dlona_test = dlona(2);
  end
  
  // Checks first intersection (beam 1 ascending pass) 
  test_ok($+1) = CL__isEqual(dlona_test, dlona_ref(1,:), rel = %f, prec = 1e-8);
  // Checks configuration
  test_ok($+1) = CL__isEqual(info.ncase, 1);
  
  
  // -----------------------------------------
  // Test 5
  //
  // Situation encountered: two intersections with latitude
  //
  // For the case of 2 intersections verifies that formulas give the correct value of the intersections' longitudes 
  // when the visible longitude interval is large
  // 
  // - Checks ascending intersections with interpolation (proves formula works when intervals become large)
  // - Checks that correct case is detected (info.ncase == 2)
  //
  // ------------------------------------------
  
  inc = tabinc(4); // rad
  angc = 5  * %CL_deg2rad; // rad
  // Chose to have a large longitude interval (to verify formula)
  latref = 84.9 * %CL_deg2rad; // rad
  
  [dlona, dlond, dta, dtd, info] = CL_op_latSwath(sma, inc,  [-angc; angc], latref,res="lti");
  [dlona_ref] = compute_fovProj(sma, inc, latref, angc);
  
  if (inc <= %pi/2)
    // longitude of first intersection (compare with interpolated value)
    dlona_test = dlona(1);
  else
    // longitude of first intersection (compare with interpolated value)
    dlona_test = dlona(2);
  end
  
  // Checks ascending intersections
  test_ok($+1) = CL__isEqual(dlona_test, dlona_ref(1), rel = %f, prec = 1e-8);
  // Checks configuration
  test_ok($+1) = CL__isEqual(info.ncase, 2);
  
  
  // -----------------------------------------
  // Test 6
  //
  // Situation encountered: latitude is entirely visible over 1 orbit
  //
  // - Checks that interval for ascending pass is centered on pso = 0
  // - Checks that the interval for descendig pass is adjacent to interval for asc. pass
  // - Checks the longitude span which is seen during one orbital period
  // - Checks that correct case is detected (info.ncase == 3)
  //
  // ------------------------------------------
  
  inc = tabinc(5); // rad
  angc = 30  * %CL_deg2rad; // rad 
  // Chose to have a large longitude interval (to verify formula)
  latref = [15, -15] * %CL_deg2rad; // rad
  
  [dlona, dlond, dta, dtd, info] = CL_op_latSwath(sma, inc,  [-angc; angc], latref,res="lti");
  
  test_ok = [test_ok; test_ncase3(inc, dlona, dlond)];

  // -----------------------------------------
  // Test 7 (ncase == 0)
  //
  // Situation encountered: latitude is not visible
  //
  // - Checks that %nan is returned
  // - Checks that correct case is detected (info.ncase == 0)
  //
  // -----------------------------------------
  
  inc = tabinc(5); // rad
  angc = 10  * %CL_deg2rad; // rad 
  // Chose to have a large longitude interval (to verify formula)
  latref = [40, -40] * %CL_deg2rad; // rad
  
  [dlona, dlond, dta, dtd, info] = CL_op_latSwath(sma, inc,  [-angc; angc], latref,res="lti");
  
  // Checks that %nan are returned
  test_ok($+1) = (find(~isnan(dlona) & ~isnan(dlona)) == []);
  // Checks configuration
  test_ok($+1) = CL__isEqual(info.ncase, [0,0]);
  
  // -----------------------------------------
  // Test 8
  //
  // Situation encountered: latitude is entirely visible over 1 orbit
  //
  // - Test with large value of center angle (%pi/2)
  // - Latitude is chose so that:  lat > max(%pi/2 - inc, inc)
  //
  // - We expect that latitude is visible for all pso (info.ncase == 4). 
  // - Same verifications as in Test 3
  //
  // -----------------------------------------
  
  inc = tabinc(1); // rad
  angc = 90  * %CL_deg2rad; // rad 
  // Chose so that latitude is always visible (1 pass)
  latref = [70, -70] * %CL_deg2rad; // rad
  
  [dlona, dlond, dta, dtd, info] = CL_op_latSwath(sma, inc,  [-angc; angc], latref,res="lti");
  
  // Perform same check as in test no.   
  test_ok = [test_ok; test_ncase4(inc, dlona, dlond)];

  
  // -----------------------------------------
  // Test 9 
  //
  // Situation encountered: latitude is entirely visible over 1 pass
  //
  // - Test with large value of center angle (%pi/2)
  // - Latitude is chose so that:  lat < min(%pi/2 - inc, inc)
  //
  // - We expect that latitude is entirely visible over 1 pass (info.ncase == 3).
  // - Same verifications as in Test 6
  //
  // -----------------------------------------
  
  inc = tabinc(1); // rad
  angc = 90  * %CL_deg2rad; // rad 
  // Chose so that latitude is always visible (1 pass)
  latref = [25, -25] * %CL_deg2rad; // rad
  
  [dlona, dlond, dta, dtd, info] = CL_op_latSwath(sma, inc,  [-angc; angc], latref,res="lti");
  
  // Perform same check as in test no.   
  test_ok = [test_ok; test_ncase3(inc, dlona, dlond)];

endfunction

// -----------------------
// Test main code
// -----------------------

// Initialization
TEST_OK = [];

// Inclinations of test orbit (some inclinations are used for more then 1 test)
inc = [60, 86, 88, 90, 10] * %CL_deg2rad; // rad

// Test for orbits with 0 < inc < 90 deg
TEST_OK = test(inc);

// Test for 90 deg < inc < 180 deg
TEST_OK = [TEST_OK; test(%pi - inc)];

