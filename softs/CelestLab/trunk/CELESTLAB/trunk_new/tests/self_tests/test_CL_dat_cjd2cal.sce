// -------------------------------------
// CL_dat_cjd2cal_test: error cases
// -------------------------------------

TEST_OK = [];

/////////////////////////////////////////////
// Test 1: Wrong number of input arguments //
/////////////////////////////////////////////
try
    cal = CL_dat_cjd2cal();
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

////////////////////////////
// Test 2: Handle case [] //
////////////////////////////
cjd = [];
cal = CL_dat_cjd2cal(cjd);
calref = [];
TEST_OK($+1) = CL__isEqual(cal,calref);

//////////////////////////////////////////////
// Test 3: Wrong number of output arguments //
// 1, 3 or 6 expected                       //
//////////////////////////////////////////////
cjd = 25000;
try
    [year,month] = CL_dat_cjd2cal(cjd);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

//////////////////////////////////////////////
// Test 4: Wrong number of output arguments //
// 3 only for integer data                  //
//////////////////////////////////////////////

cjd = 25000.65;
try
    [year, month, day] = CL_dat_cjd2cal(cjd);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end

/////////////////////////////////////////////////////////////
// Test 5: For cjd = 0, the corresponding calendar date by //
// definition is January 1st, 1950 0h                      //
/////////////////////////////////////////////////////////////

cjd = 0;

[year, month, day, hour, minute, second] = CL_dat_cjd2cal(cjd);
TEST_OK($+1) = CL__isEqual(year, 1950);
TEST_OK($+1) = CL__isEqual(month,1);
TEST_OK($+1) = CL__isEqual(day,1);
TEST_OK($+1) = CL__isEqual(hour,0);

////////////////////////////////////////////////
// Test 6: Wrong dimension of input arguments //
////////////////////////////////////////////////

cjd = [25000; 25500];
try
    [year, month, day] = CL_dat_cjd2cal(cjd);
    TEST_OK($+1) = %f;
catch
    TEST_OK($+1) = %t;
end
