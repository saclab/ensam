// ------------------------------
// CL_man_dvHohmannG : 
// ------------------------------

TEST_OK = [];

// --- Test 1 : 
// Maneuver at apogee, no rotation
ai = 7200.e3;
af = 7000.e3;
ei = 0.1;
ef = 0.15;
[deltav,dv1,dv2,anv1,anv2] = CL_man_dvHohmannG(ai,ei,af,ef,posman="apo",rotation=0)

// Check results:
kep = [ai ; ei ; %pi/2 ; 0 ; 0 ; anv1];
kep1 = CL_man_applyDvKep(kep,dv1);
kep1(6) = anv2;
kep2 = CL_man_applyDvKep(kep1,dv2);


TEST_OK($+1) = CL__isEqual(af, kep2(1)) & ...        // sma
               CL__isEqual(ef, kep2(2), 1e-14) & ... // ecc
               CL__isEqual(kep(3), kep2(3)) & ...    // inc
               CL__isEqual(kep(5), CL_rMod(kep2(5),-%pi,%pi));  // gom

// --- Test 2 : 
// Maneuver at apogee, with rotation
ai = 7200.e3;
af = 7000.e3;
ei = 0.1;
ef = 0.15;
[deltav,dv1,dv2,anv1,anv2] = CL_man_dvHohmannG(ai,ei,af,ef,posman="apo",rotation=1)

// Check results:
kep = [ai ; ei ; %pi/2 ; 0 ; 0 ; anv1];
kep1 = CL_man_applyDvKep(kep,dv1);
kep1(6) = anv2;
kep2 = CL_man_applyDvKep(kep1,dv2);

TEST_OK($+1) = CL__isEqual(af, kep2(1)) & ...        // sma
               CL__isEqual(ef, kep2(2), 1e-14) & ... // ecc
               CL__isEqual(kep(3), kep2(3)) & ...    // inc
               CL__isEqual(kep(5), CL_rMod(kep2(5),-%pi,%pi));  // gom

