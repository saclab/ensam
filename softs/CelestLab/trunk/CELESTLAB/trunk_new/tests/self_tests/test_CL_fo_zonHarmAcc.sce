// ------------------------------
// CL_fo_zonHarmAcc:
// - This test verifies that general formulation yield the same results as the explicit formulas
//   for the cases nz=[2:6]
// ------------------------------

// TEST_OK variable initialization
TEST_OK=[];

// pos variable initialization
pos = [7.e6; 8.e6; 9.e6] * ones(1,1); 

// Call to functions

// Evaluation using explicit formulas for effects of J2 to J6
acc1 = CL_fo_zonHarmAcc(pos, 2:6);

// Evaluation  using general formulation
acc2 = CL_fo_zonHarmAcc(pos, 1:6);

TEST_OK($+1) = CL__isEqual(acc1,acc2);