// ------------------------------
// CL_man_applyDvKep : 
// ------------------------------

TEST_OK = [];

// --- Test 1 : 
// Manoeuvre : 7200km to 7000km :
ai = 7200.e3;
af = 7000.e3;
[deltav,dv1,dv2,anv1,anv2] = CL_man_dvHohmann(ai,af);

// Check results:
// NB: here, true anomaly = mean anomaly
kep = [ai ; 0 ; %pi/2 ; 0 ; 0.1 ; anv1];
kep1 = CL_man_applyDvKep(kep,dv1,dv_frame="qsw");
kep1(6) = anv2;
kep2 = CL_man_applyDvKep(kep1,dv2,dv_frame="qsw");


TEST_OK($+1) = CL__isEqual(af, kep2(1)) & ...      // sma
               CL__isEqual(kep(2), kep2(2)) & ...  // ecc
               CL__isEqual(kep(3), kep2(3)) & ...  // inc
               CL__isEqual(kep(5), kep2(5));       // gom
