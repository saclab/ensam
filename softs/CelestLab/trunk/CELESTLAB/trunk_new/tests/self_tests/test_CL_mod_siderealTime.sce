// ------------------------------
// CL_mod_siderealTime
// (backward compatibility test with old CL_mod_sidTimeVeis function)
// ------------------------------

TEST_OK = [];

cjd = [15890.123155, 25.262323];
ut1_utc = 0.25;

// Reference value obtained with : 
// [sidt_ref,sidtdot_ref] = CL_mod_sidTimeG50(cjd + ut1_utc/86400);
// CL__string(sidt_ref,printscreen=%t);
// CL__string(sidtdot_ref,printscreen=%t);
sidt_ref = [5.6882553774395888,3.8294569749361882];
sidtdot_ref = [7.2921151467052092e-005,7.2921151467052092e-005];


[sidt,sidtdot] = CL_mod_siderealTime(cjd,ut1_utc, "Veis");

TEST_OK($+1) = CL__isEqual(sidt, sidt_ref, 1e-12);
TEST_OK($+1) = CL__isEqual(sidtdot, sidtdot_ref, 1e-15);


// ------------------------------
// TEST2: CIRS/TIRS (derivative only)
// ------------------------------
cjd = [20000.1, 20001.1];
ut1_utc = 0; 

[sidt,sidtdot] = CL_mod_siderealTime(cjd,ut1_utc, "CIRS");

// makes contiuous
sidt = CL_unMod(sidt, 2*%pi, (cjd-cjd(1))*86400, sidtdot); 

sidtdot2 = (sidt(2) - sidt(1)) / ((cjd(2)-cjd(1))*86400); 

TEST_OK($+1) = CL__isEqual(sidtdot2, sidtdot(1), 1e-12);


// ------------------------------
// TEST3: ECI
// ------------------------------
cjd = [20000.1];
ut1_utc = 0.2; 

[sidt1,sidtdot1] = CL_mod_siderealTime(cjd, ut1_utc);

frame = CL_configGet("ECI_FRAME"); 

[sidt2,sidtdot2] = CL_mod_siderealTime(cjd, ut1_utc, frame);

// makes contiuous
TEST_OK($+1) = CL__isEqual(sidt1, sidt2, 1e-12);
TEST_OK($+1) = CL__isEqual(sidtdot1, sidtdot2, 1e-12);




