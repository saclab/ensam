// ----------------------------------------------------------------------------------------
// CL_fr_convert
// Validation of angular velocity vector (omega)
// with finite differences.
// ----------------------------------------------------------------------------------------

// Random date
cjd = CL_dat_cal2cjd(2080,1,1,1,5,3);
dt = 0.0001; // days


frames = ["GCRS", "EOD", "MOD", "EME2000", "CIRS", "Veis", "TOD", "TEME", "TIRS", "PEF", "ITRS"];

// To test ALL frame combinations:
// nb = size(frames,"*");
// [i,j] = ndgrid(1:nb, 1:nb);

// Selected frame combinations
i = [[1:10], 1, 1];
j = [[2:11], 11, 6];

N = size(i,"*");

err_max = 0;

// Test for all the combinations of frames
for k = 1 : N
  frame1 = frames(i(k));
  frame2 = frames(j(k));
  
  [M1, omega1] = CL_fr_convertMat(frame1, frame2, cjd); 
  [M2, omega2] = CL_fr_convertMat(frame1, frame2, cjd+dt); 

  // uk = vecteur de base dans frame2
  // M' = images des vecteurs de base
  // � comparer avec omega^uk (uk : coordonn�es dans frame1 = M' * uk)
  
  res = (M2' - M1') / (dt * 86400);
  ref = CL_cross(omega1, M1');
  
  // Force extremely low values (numerical errors) to be exactly 0 
  // so that the test with relative precision is possible.
  I = find(CL_norm(res) < 1e-15);
  res(:,I) = 0;
  I = find(CL_norm(ref) < 1e-15);
  ref(:,I) = 0;
  
  // Relative precision between res and ref
  [is_equal,max_err] = CL__isEqual(res,ref, 1e-4,relative=%t,compar_crit="norm");
  err_max = max(err_max, max_err);
  //mprintf("%s %s   %f\n",frame1, frame2,max_err);
end

// Relative precision should be < 0.01
// (this value is enough to detect blatant errors)
TEST_OK = err_max < 0.01;
