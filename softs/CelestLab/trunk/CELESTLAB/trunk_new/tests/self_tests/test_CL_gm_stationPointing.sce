// ---------------------------------------------------------
//
// Consistency tests between CL_gm_stationPointing and CL_fr_topoNMat functions
//
// ---------------------------------------------------------

TEST_OK = [];


//---TEST 1---
// Simple test: one station, one position

// Station position 
station = [0.1 ; 0.4 ; 100]; 

// Conversion from ECF to topoN:
M = CL_fr_topoNMat(station);
pos = [ 1000.e3 ; 6578.e3 ; 2000.e3 ]; 

pos_sta = CL_co_ell2car(station); // station in cartesian coord.
pos_topoN = M * (pos - pos_sta); //  sat position relative to station (Topocentric coordinates)

pos_topoN_sph = CL_co_car2sph(pos_topoN); 

[azim, elev, dist] = CL_gm_stationPointing(station, pos);

delta =  [azim;elev;dist] - pos_topoN_sph;
delta(1) = CL_rMod(delta(1), -%pi, %pi); 

TEST_OK($+1) = CL__isEqual(delta,[0;0;0],1e-14);

// with "res" argument
[dist, azim, elev] = CL_gm_stationPointing(station, pos, res=["dist", "azim", "elev"]);

delta =  [azim;elev;dist] - pos_topoN_sph;
delta(1) = CL_rMod(delta(1), -%pi, %pi); 

TEST_OK($+1) = CL__isEqual(delta,[0;0;0],1e-14);



 


