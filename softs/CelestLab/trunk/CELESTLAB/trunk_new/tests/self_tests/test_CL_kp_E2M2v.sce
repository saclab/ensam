// ------------------------------------------
// Consistency test of functions :
// CL_kp_M2E, CL_kp_M2v etc..
// ------------------------------------------

TEST_OK = [];


// ------------------------------------------
// TEST 1 : 
// Standard elliptic + hyperbolic orbits:
// Maximum accuracy expected (1e-13)
// ------------------------------------------
e_ref = [0.1, 0.8, 1.1, 1.2];
E_ref = [0, 1e-12, 1e-8, 0.2, %pi-1e-8, %pi, %pi+1e-8] ;

[e,E] = ndgrid(e_ref,E_ref);

// Check CL_kp_M2E <--> CL_kp_E2M
M = CL_kp_E2M(e,E);
E1 = CL_kp_M2E(e,M);

TEST_OK($+1) = CL__isEqual(E,E1,1e-13);

// Check CL_kp_M2v <--> CL_kp_v2M
v = CL_kp_M2v(e,M);
M1 = CL_kp_v2M(e,v);

TEST_OK($+1) = CL__isEqual(M,M1,1e-13);
               
// Check CL_kp_E2v <--> CL_kp_v2E
v = CL_kp_E2v(e,E);
E1 = CL_kp_v2E(e,v);

TEST_OK($+1) = CL__isEqual(E,E1,1e-13);

// ------------------------------------------
// TEST 2 : 
// Elliptic orbits almost circular and/or close to parabolic
// Good accuracy expected (1e-9)
// ------------------------------------------
e_ref = [0, 1e-12, 1e-8, 0.2, 1-1e-8, 1-1e-12];
E_ref = [0, 1e-12, 1e-8, 0.2, %pi-1e-8, %pi, %pi+1e-8] ;

[e,E] = ndgrid(e_ref,E_ref);

// Check CL_kp_M2E <--> CL_kp_E2M
M = CL_kp_E2M(e,E);
E1 = CL_kp_M2E(e,M);

TEST_OK($+1) = CL__isEqual(E,E1);

// Check CL_kp_M2v <--> CL_kp_v2M
v = CL_kp_M2v(e,M);
M1 = CL_kp_v2M(e,v);

TEST_OK($+1) = CL__isEqual(M,M1,1e-9);
               
// Check CL_kp_E2v <--> CL_kp_v2E
v = CL_kp_E2v(e,E);
E1 = CL_kp_v2E(e,v);

TEST_OK($+1) = CL__isEqual(E,E1,1e-9);
               
  
  
// ------------------------------------------
// TEST 3 : 
// Hyperbolic orbits almost parabolic
// Poor accuracy (1e-3) in some cases (if E is close to 0) 
// ------------------------------------------
e_ref = [1+1e-12, 1+1e-8];
E_ref = [0, 1e-12, 1e-8, 0.2, %pi-1e-8, %pi, %pi+1e-8] ;

[e,E] = ndgrid(e_ref,E_ref);

// Check CL_kp_M2E <--> CL_kp_E2M
M = CL_kp_E2M(e,E);
E1 = CL_kp_M2E(e,M);

TEST_OK($+1) = CL__isEqual(E,E1,1e-4);

// Check CL_kp_M2v <--> CL_kp_v2M
v = CL_kp_M2v(e,M);
M1 = CL_kp_v2M(e,v);

TEST_OK($+1) = CL__isEqual(M,M1,1e-8);
               
// Check CL_kp_E2v <--> CL_kp_v2E
v = CL_kp_E2v(e,E);
E1 = CL_kp_v2E(e,v);

TEST_OK($+1) = CL__isEqual(E,E1,1e-9);
              
               
