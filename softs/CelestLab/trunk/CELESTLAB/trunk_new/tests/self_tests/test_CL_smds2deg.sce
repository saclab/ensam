// ------------------------------
// CL_smds2deg : 
// ------------------------------

TEST_OK = [];

//////////////////////////////////////////////////
// Test1: cas nominal based on function exemple //
//////////////////////////////////////////////////
sdms = [-1; 40 ; 19 ; 30.2]; // -40 deg 19' 30.2''

degs_res = CL_sdms2deg(sdms);
degs_ref = -40.3250555555555579;

TEST_OK(1+$) = CL__isEqual(degs_res,degs_ref);

/////////////////////////////////////////////////////////////////////////////
// Test2: cas nominal based on function exemple with inputs in vector form //
/////////////////////////////////////////////////////////////////////////////
sdms = [[-1 1 0]; [40 30 20] ; [19 34 11]; [30.2 6.4 2.2]]; // -40 deg 19' 30.2''
                                                            // 30 deg 34' 6.4''
                                                            // 20 deg 11' 2.2''
degs_res = CL_sdms2deg(sdms);
degs_ref = [-40.3250555555555579 30.5684444444444452 20.1839444444444460];

TEST_OK(1+$) = CL__isEqual(degs_res,degs_ref);

/////////////////////////////////////////////////////
// Test3: Invalid input argument (4 rows expected) //
/////////////////////////////////////////////////////
sdms = [-1 40 19 30.2];

try
    CL_sdms2deg(sdms);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

////////////////////////////////////////////////////////////
// Test4: Invalid input argument (sign must be 0,1 or -1) //
////////////////////////////////////////////////////////////
sdms = [2;40;19;30.2];

try
    CL_sdms2deg(sdms);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

//////////////////////////////////////////////////////////
// Test5: Invalid input argument value (negative value) //
//////////////////////////////////////////////////////////
sdms = [-1;-40;19;30.2];

try
    CL_sdms2deg(sdms);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

