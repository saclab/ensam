// ------------------------------
// CL__E_esinE:
// Calcul precis de E - e*sin(E)
// Verif de la validit� de CL__E_esinE par comparaison avec le developpement limit�
// Pour E-->0, on a E - e * sin(E) = E - e * (E - E^3/6 + E^5/120 + ...)
// ------------------------------

TEST_OK = [];

N = 100000;
// Tirage al�atoire de E dans [0, 10-5]
E = rand(1,N) * 1e-5;
// Tirage al�atoire de e dans [0.99, 1]
e = 1 - rand(1,N) * 0.01;

x_ref = E.*(1-e) - e.*( -(E.^3)/6 + (E.^5)/120); // valeur de reference
x1 = E - e.*sin(E);    // calcul standard
x2 = CL__E_esinE(e,E); // calcul precis
prec_standard = max(abs((x1-x_ref) ./ x_ref)); // --> calcul standard : precision 10-9
prec_precis = max(abs((x2-x_ref) ./ x_ref));  // --> calcul precis : precision 10-16

TEST_OK($+1) = prec_precis < 1e-15;