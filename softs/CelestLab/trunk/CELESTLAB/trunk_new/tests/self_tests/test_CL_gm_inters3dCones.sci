// ------------------------------
// CL_gm_inters3dConesSa: 
// ------------------------------

TEST_OK = [];

// Test 1 : standard case
alpha1 = 0.1;
alpha2 = 0.2;
alpha = 0.15;
ang = CL_gm_inters3dConesSa(alpha1,alpha2,alpha);
TEST_OK($+1) = ang > 0;

// Test 2 : no intersection (limit case)
alpha1 = 0.1;
alpha2 = 0.2;
alpha = 0.3;
ang = CL_gm_inters3dConesSa(alpha1,alpha2,alpha);
TEST_OK($+1) = abs(ang) < 1e-15;

// Test 3 : intersection = cone 1
alpha1 = 0.1;
alpha2 = 0.2;
alpha = 0;
ang = CL_gm_inters3dConesSa(alpha1,alpha2,alpha);
TEST_OK($+1) = CL__isEqual(ang , 2*%pi*(1-cos(alpha1)));

// Test 4 : cone 1 and cone 2 identical
alpha1 = 0.2;
alpha2 = 0.2;
alpha = 0;
ang = CL_gm_inters3dConesSa(alpha1,alpha2,alpha);
TEST_OK($+1) = CL__isEqual(ang , 2*%pi*(1-cos(alpha1)));

// Test 5 
alpha12_tab = linspace(0,%pi/2,10);
alpha_tab = linspace(0,%pi,10);
for i = 1 : size(alpha12_tab,2)
  for j = 1 : size(alpha12_tab,2)
    for k = 1 : size(alpha_tab,2)
      alpha1 = alpha12_tab(i);
      alpha2 = alpha12_tab(j);
      alpha = alpha_tab(k);
      ang1 = CL_gm_inters3dConesSa(alpha1,alpha2,alpha);
    end
  end
end
