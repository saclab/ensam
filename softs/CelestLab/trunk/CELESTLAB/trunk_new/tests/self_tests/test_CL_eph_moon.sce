// ------------------------------
// CL_eph_moon: 
// Validation of derivatives with finite differences
// ------------------------------

TEST_OK = [];


cjd = 0;
dt = 200;  // seconds
dtd = dt / 86400; // days
tt_tref = 0;
frame = "EOD";

models = ["med", "high", "ELP"];

for model = models
  // Reference velocity
  [pos_moon_dt, vel_moon_ref] = CL_eph_moon(cjd+dtd/2,tt_tref,frame,model);

  // Compute velocity with finite differences
  [pos_moon] = CL_eph_moon(cjd,tt_tref,frame,model);
  [pos_moon_dt] = CL_eph_moon(cjd+dtd,tt_tref,frame,model);
  vel_moon = (pos_moon_dt-pos_moon) ./ dt;

  TEST_OK($+1) = CL__isEqual(vel_moon, vel_moon_ref,1e-8);
end
