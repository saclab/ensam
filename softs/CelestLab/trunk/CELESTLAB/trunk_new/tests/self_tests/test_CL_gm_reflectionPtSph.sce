// ------------------------------
// CL_gm_reflectionPtSph: 
// ------------------------------

TEST_OK = [];

// Test 1 : standard case

alpha = linspace(-%pi, %pi, 20);
pos1 = 2 * [1;0;0];
pos2 = 2 * [cos(alpha); sin(alpha); zeros(alpha)];
[posr, incid] = CL_gm_reflectionPtSph(pos1, pos2, 1);
theta1 = CL_vectAngle(pos1*ones(alpha)-posr, posr);
theta2 = CL_vectAngle(pos2-posr, posr);

TEST_OK($+1) = CL__isEqual(theta2, theta1,1.e-14);
TEST_OK($+1) = CL__isEqual(theta1, incid,1.e-14);

// Test 2 : pos1 = point within the sphere

alpha = linspace(-%pi, %pi, 5);
pos1 = [0.9;0;0];
pos2 = [cos(alpha); sin(alpha); zeros(alpha)];
srad = 1.0
try 
  [posr, incid] = CL_gm_reflectionPtSph(pos1, pos2, srad);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 3 : srad incorrect
try 
  [posr, incid] = CL_gm_reflectionPtSph(pos1, pos2, 0);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 4 : srad incorrect
pos2_inf = %f
try 
  [posr, incid] = CL_gm_reflectionPtSph(pos1, pos2, 25,pos2_inf=pos2_inf);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 5 : numer of rows of pos1 incorrect
pos1 = 2 * [1;0];
try 
  [posr, incid] = CL_gm_reflectionPtSph(pos1, pos2, 1);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 6 : numer of col. of pos1 incorrect
pos1 = 2 * [1;0;2;4;5;6];
try 
  [posr, incid] = CL_gm_reflectionPtSph(pos1, pos2, 1);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 7 : srad incorrect
srad = [0.5 1 2 3];
try 
  [posr, incid] = CL_gm_reflectionPtSph(pos1, pos2, srad=srad);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  







