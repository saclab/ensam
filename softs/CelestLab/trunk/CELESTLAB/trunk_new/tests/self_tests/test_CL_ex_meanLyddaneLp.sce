//--------------------------------------------------------------------------
// CL_ex_meanLyddaneLp
//--------------------------------------------------------------------------
// Note: cette fonction est aussi validé suite à la validation de la fonction
//       CL_ex_meanElem

TEST_OK = [];

/////////////////////////////////////////////////////////////
// Test1: Cross Validation test with CL_ex_meanLyddane.sce //
/////////////////////////////////////////////////////////////
osc_kep = [7.e6; 1.e-4; 1; %pi/2; 0.2; 0.3];
t0 = 0; //days
t  = 2; //days

// Mean elements with LyddaneLp propagation model
mean_kep_LyddaneLp = CL_ex_meanLyddaneLp(osc_kep);
// Mean elements with Lyddane propagation model; It shall be different from 
// those obtained with LyddaneLpe propagation model
mean_kep_Lyddane = CL_ex_meanLyddane(osc_kep);

// Check: After propagation of mean elements, the osculator elements obtained with 
// both methods shall be the same
osc_kep_LyddaneLp = CL_ex_propagate("lydlp","kep",t0,mean_kep_LyddaneLp,t,"o");
osc_kep_Lyddane = CL_ex_propagate("lydsec","kep",t0,mean_kep_Lyddane,t,"o");

TEST_OK($+1) = CL__isEqual(osc_kep_LyddaneLp,osc_kep_Lyddane);
