// ------------------------------
// CL_cov2cor 
// ------------------------------

// Cas 1: hypermatrix
nfc=5; nhyp=2;
mat = rand(nfc,nfc,nhyp,key="normal");
covm = mat'*mat;  // symetrical
[corm,sd] = CL_cov2cor(covm);
// Retrieving the covariance :
cov2 = CL_cor2cov(corm,sd) ;

TEST_OK($+1) = CL__isEqual( covm, cov2, 1e-15)

// Cas 2: matrix
nfc=4; 
mat = rand(nfc,nfc,key="normal");
covm = mat'*mat;  // symetrical
[corm,sd] = CL_cov2cor(covm);
// Retrieving the covariance :
cov2 = CL_cor2cov(corm,sd) ;

TEST_OK($+1) = CL__isEqual( covm, cov2, 1e-15)

//Cas 3: ncol =/ nrow
nf=3; nc=2;
mat = rand(nf,nc,key="normal");
covm = mat;  
try 
  [corm,sd] = CL_cov2cor(covm);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  
