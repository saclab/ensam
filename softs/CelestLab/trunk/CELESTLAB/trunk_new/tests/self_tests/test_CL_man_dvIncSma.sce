// -------------------------------------------------------
// CL_man_dvIncSma : Cross validation with CL_man_applyDvKep
// -------------------------------------------------------

TEST_OK = [];

/////////////////////////////////////////////////////
// Test 1: Cross validation with CL_man_applyDvKep //
/////////////////////////////////////////////////////

// Typical transfer from GTO to GEO (at apogee = ascending node)
rai = 42164.e3; // radius at apogee
rpi = 6578.e3; // radius at perigee
[ai, ei] = CL_op_rarp2ae(rai, rpi);
inci = 7 * %pi/180;
pomi = %pi; // apogee = ascending node;
af = 42164.e3;
incf = 0;

[deltav,dv,anv] = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf,posman="an");
anm = CL_kp_v2M(ei,anv); // mean anomaly

// Check results :
kep = [ai ; ei ; inci ; pomi ; 0 ; anm];
kep_dv = CL_man_applyDvKep(kep,dv);

TEST_OK($+1) = CL__isEqual(af, kep_dv(1)) & ...        // sma
                 CL__isEqual(kep_dv(2),0,1.e-14) & ...   // ecc == 0
                 CL__isEqual(kep_dv(3),0,1.e-14);        // inc == 0 

////////////////////////////////
// Test 2: optional arguments //
////////////////////////////////
rai = 42164.e3; // radius at apogee
rpi = 6578.e3; // radius at perigee
[ai, ei] = CL_op_rarp2ae(rai, rpi);
inci = 7 * %pi/180;
pomi = 0; // perigee = ascending node;
af = 42164.e3;
incf = 0;

[deltav,dv,anv] = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf,posman="dn");
anm = CL_kp_v2M(ei,anv);
kep = [ai ; ei ; inci ; pomi ; 0 ; anm];
kep_dv = CL_man_applyDvKep(kep,dv);

TEST_OK($+1) = CL__isEqual(af, kep_dv(1)) & ...        // sma
                 CL__isEqual(kep_dv(2),0,1.e-14) & ...   // ecc == 0
                 CL__isEqual(kep_dv(3),0,1.e-14);        // inc == 0 

//////////////////////
// Test 3: res == s //
//////////////////////
rai = 42164.e3; // radius at apogee
rpi = 6578.e3; // radius at perigee
[ai, ei] = CL_op_rarp2ae(rai, rpi);
inci = 7 * %pi/180;
pomi = %pi; // apogee = ascending node;
af = 42164.e3;
incf = 0;

man = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf,res="s");
anm = CL_kp_v2M(ei,man.anv); // mean anomaly

kep = [ai ; ei ; inci ; pomi ; 0 ; anm];
kep_dv = CL_man_applyDvKep(kep,man.dv);

TEST_OK($+1) = CL__isEqual(af, kep_dv(1)) & ...        // sma
                 CL__isEqual(kep_dv(2),0,1.e-14) & ...   // ecc == 0
                 CL__isEqual(kep_dv(3),0,1.e-14);        // inc == 0 

////////////////////////////////////////////////
// Test 4: Invalid value for argument ''res'' //
////////////////////////////////////////////////
rai = 42164.e3; // radius at apogee
rpi = 6578.e3; // radius at perigee
[ai, ei] = CL_op_rarp2ae(rai, rpi);
inci = 7 * %pi/180;
pomi = %pi; // apogee = ascending node;
af = 42164.e3;
incf = 0;

try
    man = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf,res="p");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

////////////////////////////////////////////////
// Test 5: Invalid number of output arguments //
////////////////////////////////////////////////
rai = 42164.e3; // radius at apogee
rpi = 6578.e3; // radius at perigee
[ai, ei] = CL_op_rarp2ae(rai, rpi);
inci = 7 * %pi/180;
pomi = %pi; // apogee = ascending node;
af = 42164.e3;
incf = 0;

try
    [deltav,dv,anv] = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf,res="s");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end 

////////////////////////////////////////////////////////
// Test 6: Invalid input arguments (orbital elements) //
////////////////////////////////////////////////////////
rai = 42164.e3; // radius at apogee
rpi = 6578.e3; // radius at perigee
[ai, ei] = CL_op_rarp2ae(rai, rpi);
inci = -7 * %pi/180;
pomi = %pi; // apogee = ascending node;
af = 42164.e3;
incf = 0;

try
    [deltav,dv,anv] = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

/////////////////////////////////////////////////////////
// Test 7: Invalid input arguments (final inclination) //
/////////////////////////////////////////////////////////
rai = 42164.e3; // radius at apogee
rpi = 6578.e3; // radius at perigee
[ai, ei] = CL_op_rarp2ae(rai, rpi);
inci = 7 * %pi/180;
pomi = %pi; // apogee = ascending node;
af = 42164.e3;
incf = -%pi;

try
    [deltav,dv,anv] = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

/////////////////////////////////////////
// Test 8: Invalid value for ''posman''//
/////////////////////////////////////////
rai = 42164.e3; // radius at apogee
rpi = 6578.e3; // radius at perigee
[ai, ei] = CL_op_rarp2ae(rai, rpi);
inci = 7 * %pi/180;
pomi = %pi; // apogee = ascending node;
af = 42164.e3;
incf = 0;

try
    [deltav,dv,anv] = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf,posman=2);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end 

////////////////////////////////////////////
// Test 9: The norm of dv shall be deltaV //
////////////////////////////////////////////
rai = 42164.e3; // radius at apogee
rpi = 6578.e3; // radius at perigee
[ai, ei] = CL_op_rarp2ae(rai, rpi);
inci = 7 * %pi/180;
pomi = %pi; // apogee = ascending node;
af = 42164.e3;
incf = 0;

[deltav,dv,anv] = CL_man_dvIncSma(ai,ei,inci,pomi,af,incf,posman="an");

deltav_test = sqrt(dv(1)^2 + dv(2)^2 + dv(3)^2);

TEST_OK(1+$) = CL__isEqual(deltav,deltav_test);







