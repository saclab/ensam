// ------------------------------
// CL_ev_stationVisibility
// 
// Test1: Check visibility intervals
//        - External function called
//        - non default value for precision
// Test2: Check visibility intervals
//        - Data are interpolated
//        - non default value for precision
// ------------------------------

TEST_OK = [];

// ===========================================================
// Test1
// - Comparison with analytical solution (equatorial circular trajectory)
// - Tests the use of an external function to compute pos
//
// - Number of visibility intervals is compared to expected value 
// - Visibility intervals start/end times are compared to analytical solution
// - Elevation angle at beginning/end of visibility is checked (== elevmin)
// ===========================================================

//-------------------------
// Position on circular orbit
// rorb: radius of circular trajectory (1x1)
//-------------------------
function [pos] = my_positionFun(t, args)
  pos = [cos(args.omega*t); sin(args.omega*t); zeros(t)] * args.rorb;
endfunction

//-------------------------
// Compute reference visibility interval
// rorb: radius of circular trajectory (1x1)
//-------------------------
function [interv]= compute_intervref(rorb, omega)
  ang = asin(%CL_eqRad/rorb);  
  // swept angles at beginning/end of visibility
  angvisi = [%pi/2 + ang;  3*%pi/2 - ang]; // (2x1)
  interv = [angvisi, 2*%pi * ones(2,1) + angvisi, 4*%pi * ones(2,1) + angvisi]/omega; 
endfunction

// circular motion period
T = 1;
t = linspace(0, 3*T, 100);

// Stations (t=0 => angle(sat, sta) == %pi)
stations = CL_co_car2ell([-%CL_eqRad; 0; 0]); // equator
elevmin = 0;

// Arguments of my_positionFun
args = struct();
args.omega = 2*%pi/T;
args.rorb  = 7.e6; // circular orbit radius

// Reference visibility intervals (analytical)
interv_ref = compute_intervref(args.rorb, args.omega);

opts = struct();
opts.prec = 1.e-6 * %CL_deg2rad; // <> default value

interv  = CL_ev_stationVisibility(t, my_positionFun(t, args), stations, elevmin, opts = opts);

// Compute elevation at beginning/end of passages
t_visi = matrix(interv, 1, -1);
elev   = CL_gm_stationPointing(stations,  my_positionFun(t_visi, args), "elev");


// Check number of computed visi 
TEST_OK($+1) = CL__isEqual(size(interv,2), size(interv_ref,2)); // expected == 3

// Check interval start/end times
TEST_OK($+1) = CL__isEqual(interv, interv_ref, prec = 1.e-6); // precision depends on opts.prec

// Check elevation
TEST_OK($+1) = (max(abs(elev-elevmin)) < opts.prec); 

 
// ===========================================================
// Test2
// Standard use: pos_ecf => 3xN (precomputed)
// Check is performed on elevation at beginning/end of visi (== elevmin)
// ===========================================================

cjd0 = 22000; 
kep0 = [7.e6; 0; 1; 0; 0; 0]; 

cjd = cjd0 + (0 : 30/86400 : 1); 
kep = CL_ex_kepler(cjd0, kep0, cjd); 

pos_eci = CL_oe_kep2car(kep); 
pos_ecf = CL_fr_convert("ECI", "ECF", cjd, pos_eci); 

stations = [0;0;0]; 
elevmin  = 5 * %pi / 180;

opts = struct();
opts.prec = 1.e-4 * %CL_deg2rad; // <> default value

interv = CL_ev_stationVisibility(cjd, pos_ecf, stations, elevmin, opts = opts);

// Compute elevation at beginning/end of passages
cjd_visi = matrix(interv, 1, -1); 
posvisi_ecf = CL_interpLagrange(cjd, pos_ecf, cjd_visi, n = 8);
elev   = CL_gm_stationPointing(stations, posvisi_ecf, "elev");

// Check elevation at beginning/end of passages = elevmin 
TEST_OK($+1) = (max(abs(elev-elevmin)) < opts.prec);
