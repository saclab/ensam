//====================
// Test CL_3b_halo
// This test represents a reference case
// Also test compatibility with previous version for conventions on direction
//====================

// --------------------------------------
// Load reference results (=> obtained with "CL_3b_halo")
// Check non-regression of newer versions
// --------------------------------------
function [orb_retro, orb_pro, omega_retro, omega_pro] = reference_res()
  
  orb_retro = [1.00832467396873420000 1.00943281453844790000 1.01126259766402570000 1.00944844120266140000 1.00832469445051890000 
                0.00000000000000000000 0.00464252026897385080 0.00002265059645792283 -0.00464041102336284000 -0.00005001571141513938 
                0.00090903020198054103 -0.00022972053469174711 -0.00114791011037794810 -0.00023968535479282883 0.00090896233966737227 
                0.00000000000000000000 0.00314870696534780840 0.00002092832232852012 -0.00316169853986310790 -0.00000946923839028025 
                0.01009963206254966900 -0.00038026384581642920 -0.00914617274342482790 -0.00047081724861152628 0.01009895477241485300 
                0.00000000000000000000 -0.00201412274918890250 -0.00000869900083073655 0.00200996163060088770 0.00002767171258312555]; 
  
  omega_retro = 2.02596142136566693;
                
  orb_pro = [1.00832467396873420000 1.00943281453844790000 1.01126259766402570000 1.00944844120266140000 1.00832469445051890000 
              0.00000000000000000000 0.00464252026897385080 0.00002265059645792283 -0.00464041102336284000 -0.00005001571141513938 
              -0.00090903020198054103 0.00022972053469174711 0.00114791011037794810 0.00023968535479282883 -0.00090896233966737227 
              0.00000000000000000000 0.00314870696534780840 0.00002092832232852012 -0.00316169853986310790 -0.00000946923839028025 
              0.01009963206254966900 -0.00038026384581642920 -0.00914617274342482790 -0.00047081724861152628 0.01009895477241485300 
              0.00000000000000000000 0.00201412274918890250 0.00000869900083073655 -0.00200996163060088770 -0.00002767171258312555];
  
  omega_pro = 2.02596142136566693;
  
endfunction

// Main Code

TEST_OK = [];

//===================================
// Input parameters
//===================================
// Build structure for L2 and "Sun-EarthMoon" system:
env = CL_3b_environment("S-EM","L2");

// Generate a Halo orbit:

// Estimate of amplitude along the Z axis
Az = 150.e6 / env.D;  // adimensional
// Times at which the orbit is computed
t_orb = linspace(0,180*86400,5) * env.OMEGA; // adimensional

// Load reference results
[orb_retro_ref, orb_pro_ref, omega_retro_ref, omega_pro_ref] = reference_res();

//===================================
// direction "pro"
//===================================
direction = "pro";
[orb,omega] = CL_3b_halo(env, Az, direction, t_orb);
TEST_OK($+1) = CL__isEqual(orb, orb_pro_ref);
TEST_OK($+1) = CL__isEqual(omega, omega_pro_ref);

// save results for later use
orb_pro = orb;
omega_pro = omega; 


//===================================
// direction "retro"
//===================================
direction = "retro";
[orb,omega] = CL_3b_halo(env, Az, direction, t_orb);
TEST_OK($+1) = CL__isEqual(orb, orb_retro_ref);
TEST_OK($+1) = CL__isEqual(omega, omega_retro_ref);

// save results for later use
orb_retro = orb;
omega_retro = omega; 


//===================================================
// Check compatibility with old convention 
// 0 <=> "retro"
// 1 <=> "pro"
//===================================================
direction = 0;
[orb,omega] = CL_3b_halo(env, Az, direction, t_orb);

TEST_OK($+1) = CL__isEqual(orb, orb_retro);
TEST_OK($+1) = CL__isEqual(omega, omega_retro);

direction = 1;
[orb,omega] = CL_3b_halo(env, Az, direction, t_orb);

TEST_OK($+1) = CL__isEqual(orb, orb_pro);
TEST_OK($+1) = CL__isEqual(omega, omega_pro);


