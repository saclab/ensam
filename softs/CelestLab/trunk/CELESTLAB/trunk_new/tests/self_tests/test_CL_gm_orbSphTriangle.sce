// ------------------------------
// CL_gm_orbSphTriangle : 
// ------------------------------



TEST_OK = [];

function [angname] = getAngName(res,j)
  f = fieldnames(res)
  angname = f(j);
  
  if (angname == "decl")
    aol = CL_rMod(res.aol,-%pi,%pi);
    if (aol >= -%pi/2 & aol <= %pi/2 ) // orbite montante
      angname = "decl_a";
    else
      angname = "decl_d";
    end
  end
  
  if (angname == "bear")
    if (res.decl >= 0) // hemisphere nord.
      angname = "bear_n";
    else
      angname = "bear_s";
    end
  end
endfunction

// ---------
// Test 1 : 
// Take a random angle (name and value) as input, compute the 4 outputs angles from it.
// Then take successively the 4 outputs angles as input and check that it gives the same results
// We compare with absolute precision = 1e-7, because the transformation bear into another parameter
// is numerically not accurate
// ---------
inc_tab = [1, 2];  // inclinations such that there are no singularities 
ang_tab = [-%pi, -2, -%pi/2, -1, 0, 1, %pi/2, 2, %pi];  // domaine definition pour aol,rra,bear
ang2_tab = [-%pi/2, -1, -%pi/4, -0.1, 0, 0.1, %pi/4, 1, %pi/2];  // domaine definition pour decl

angnames = ["aol", "rra", "decl_a", "decl_d", "bear_n", "bear_s"];
resnames = ["aol", "decl", "rra", "bear"];

TEST_OK($+1) = %t;
for inc = inc_tab
  for k = 1 : size(angnames,2)
    for a = 1 : size(ang_tab,2)
    
      if (angnames(k) == "decl_a" | angnames(k) == "decl_d")
        ang = ang2_tab(a);
      else
        ang = ang_tab(a);
      end
      
      res = CL_gm_orbSphTriangle(inc, angnames(k), ang, output="all");
      for j = 1 : size(resnames,2)     
        res2 = CL_gm_orbSphTriangle(inc, getAngName(res,j), res(resnames(j)), output="all");
        // mprintf("inc=%f ; res: %s=%f ; res2: %s=%f\n",inc, angnames(k),ang,getAngName(res,j),res(resnames(j)));
        // disp(res)
        // disp(res2)
        
        if (~CL__isEqual(res,res2,prec=1e-7,relative=%f)) 
          TEST_OK($) = %f;
        end
      end
    end
  end
end

// ------------------------------------
// Test 2 : %nan in input values
//          --> %nan in output values
// ------------------------------------
inc = 1;
aol = linspace(0, %pi/2, 10);
aol(5) = %nan;
res = CL_gm_orbSphTriangle(inc, "aol", aol, output="all");

TEST_OK($+1) = isnan(res.aol(5))  & ...
               isnan(res.decl(5)) & ...
               isnan(res.rra(5))  & ...
               isnan(res.bear(5)) ;

               
// ------------------------------------
// Test 3 :  [] in input values
//       --> [] in output
// ------------------------------------
inc = 1;
aol = [];
res = CL_gm_orbSphTriangle(inc, "aol", aol, output="all");
TEST_OK($+1) = isempty(res.aol)  & ...
               isempty(res.decl) & ...
               isempty(res.rra)  & ...
               isempty(res.bear) ;
