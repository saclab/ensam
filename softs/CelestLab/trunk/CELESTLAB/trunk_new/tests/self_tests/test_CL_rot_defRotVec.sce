// ------------------------------
// CL_rot_defRotVec : 
// ------------------------------

TEST_OK = [];


// Vecteurs canoniques : Liste des vecteurs a tester, dans toutes les combinaisons possibles :
vecteurs_tab = [ [1;0;0] , [0;1;0] , [0;0;1] , [-1;0;0] , [0;-1;0] , [0;0;-1]];
[Iu1, Iv1, Iu2, Iv2] = ndgrid(1:6,1:6,1:6,1:6);
u1 = vecteurs_tab(:, matrix(Iu1,1,-1));
v1 = vecteurs_tab(:, matrix(Iv1,1,-1));
u2 = vecteurs_tab(:, matrix(Iu2,1,-1));
v2 = vecteurs_tab(:, matrix(Iv2,1,-1));

[q , ind] = CL_rot_defRotVec(u1, v1, u2, v2);

// --- Test 1 : Dans tous les cas : image de u1 = u2
I = find(CL_norm( CL_rot_rotVect(q,u1) - u2 ) > 1e-13);
TEST_OK($+1) = (I ==[]);
 

// --- Test 2 : Dans tous les cas non particuliers: image de v1 = v2
// Cas non particuliers = les cas ou u2 perpendiculaire a v2 et u1 perpendiculaire a v1
I = find( CL_norm( CL_cross(u2,v2) ) > 1e-13 & CL_norm( CL_cross(u1,v1) ) > 1e-13);
I2 = find(CL_norm( CL_rot_rotVect(q(I),v1(:,I)) - v2(:,I) ) > 1e-13);
TEST_OK($+1) = (I2 ==[]);

// --- Test 3 : Cas particuliers ou u1 = u2 et v1 = v2 --> identite
I = find( CL_norm( u1-u2 ) < 1e-13 & CL_norm( v1-v2 ) < 1e-13);
[axe, ang] = CL_rot_quat2axAng(q);
I2 = find( ang(I) > 1e-13 );
TEST_OK($+1) = (I2 ==[]);

// --- Test 4 : Cas particuliers ou u1 = u2 et v1 ~= v2 --> l'axe doit etre u1 ou -u1
I = find( CL_norm( u1-u2 ) < 1e-13 & CL_norm( v1-v2 ) > 1e-13);
[axe, ang] = CL_rot_quat2axAng(q);
I2 = find( CL_norm(axe(:,I) - u1(:,I)) > 1e-13 & CL_norm(axe(:,I) + u1(:,I)) > 1e-13 & CL_norm(axe(:,I)) <> 0);
TEST_OK($+1) = (I2 ==[]);


// --- Test 5 : Cas quelconque :
// On construit une rotation puis on verifie que la fonction retrouve bien la meme rotation
N = 100;
axe = CL_unitVector(rand(3,N)-0.5); 
ang = -2; 
q = CL_rot_axAng2quat(axe, ang); 
M = CL_rot_quat2matrix(q); 
u1 = CL_unitVector(rand(3,N)-0.5);
v1 = CL_unitVector(rand(3,N)-0.5);
u2 = M' * u1; 
v2 = CL_unitVector(M' * v1 + 10 * u2); 
q2 = CL_rot_defRotVec(u1, v1, u2, v2);

u2_2 = CL_rot_rotVect(q2,u1);
v2_2 = CL_rot_rotVect(q2,v1);
I = find( CL_norm(u2_2-u2) > 1e-12 | CL_norm(v2_2 - M'*v1) > 1e-12);
TEST_OK($+1) = (I ==[]);

// --- Test 6 : Cas particulier ou u2-u1 ^ v2-v1 == 0 : 
N = 100;
u1 = CL_unitVector(rand(3,N)-0.5);
v1 = CL_unitVector(rand(3,N)-0.5);
axe = u1 + 2 * v1; 
ang = -1; 
q = CL_rot_axAng2quat(axe, ang); 
M = CL_rot_quat2matrix(q); 
u2 = M' * u1; 
v2 = M' * v1; 
CL_cross(u2-u1, v2-v1); // on a bien construit le cas qu'on souhaitait
[q2,ind] = CL_rot_defRotVec(u1, v1, u2, v2); 
[axe2, ang2] = CL_rot_quat2axAng(q2);
M2 = CL_rot_quat2matrix(q2);  
I = find(M - M2 > 1e-12);
TEST_OK($+1) = (I ==[]);
