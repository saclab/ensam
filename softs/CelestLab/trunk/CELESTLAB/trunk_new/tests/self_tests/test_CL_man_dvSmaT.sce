// ------------------------------
// CL_man_dvSmaT : 
// ------------------------------

TEST_OK = [];

///////////////////////////////////////////////////////////////////
// Test 1 : Nominal cas, cross validation with CL_man_applyDvKep //
///////////////////////////////////////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
ei = 0.1;
af = 7000.e3;
anvman = %pi/4; 
[deltav,dv,anv] = CL_man_dvSmaT(ai,ei,af,anvman);

// Check results:
anm = CL_kp_v2M(ei,anv); // mean anomaly
kep = [ai; ei; %pi/2; 0; 0; anm];
kep1 = CL_man_applyDvKep(kep,dv);
               
TEST_OK($+1) = CL__isEqual(af, kep1(1))      & ...                 // sma
                 CL__isEqual(kep(3), kep1(3)) & ...                 // inc
                 CL__isEqual(kep(5), CL_rMod(kep1(5),-%pi,%pi));  // gom

/////////////////////////////////////////////////////
// Test 2: Nominal cas with vector input arguments //
/////////////////////////////////////////////////////
// 7200km to 7000km :
ai = [7200.e3, 7000.e3, 6800.e3];
ei = [0.1, 0.1, 0.1];
af = [7000.e3, 6800.e3, 6600.e3];
anvman = [%pi/4, %pi/4, %pi/4] ;
[deltav,dv,anv] = CL_man_dvSmaT(ai,ei,af,anvman);

// Check results:
anm = CL_kp_v2M(ei,anv); // mean anomaly
kep = [ai ; ei ; %pi/2*ones(ai) ; zeros(ai) ; zeros(ai) ; anm];
kep1 = CL_man_applyDvKep(kep,dv);

TEST_OK($+1) = CL__isEqual(af, kep1(1,:)) & ...                      // sma
                 CL__isEqual(kep(3,:), kep1(3,:)) & ...               // inc
                 CL__isEqual(kep(5,:), CL_rMod(kep1(5,:),-%pi,%pi)); // gom

//////////////////////////////////
// Test 3: Nominal cas res == s //
//////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
ei = 0.1;
af = 7000.e3;
anvman =%pi/4;

man = CL_man_dvSmaT(ai,ei,af,anvman,res="s");

// Check results:
anm = CL_kp_v2M(ei,man.anv); // mean anomaly
kep = [ai ; ei ; %pi/2 ; 0 ; 0 ; anm];
kep1 = CL_man_applyDvKep(kep,man.dv);
               
TEST_OK($+1) = CL__isEqual(af, kep1(1)) & ...                     // sma
                 CL__isEqual(kep(3), kep1(3)) & ...                // inc
                 CL__isEqual(kep(5), CL_rMod(kep1(5),-%pi,%pi)); // gom
               
////////////////////////////////////////////////
// Test 4: Invalid value for argument ''res'' //
////////////////////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
ei = 0.1;
af = 7000.e3;
anvman =%pi/4;

try
    man = CL_man_dvSmaT(ai,ei,af,anvman,res="p");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

////////////////////////////////////////////////
// Test 5: Invalid number of output arguments //
////////////////////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
ei = 0.1;
af = 7000.e3;
anvman =%pi/4;

try
    [deltav,dv,anv] = CL_man_dvSmaT(ai,ei,af,anvman,res="s");
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

////////////////////////////////////////////
// Test 6: The norm of dv shall be deltaV //
////////////////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3;
ei = 0.1;
af = 7000.e3;
anvman =%pi/4;

[deltav_res,dv_res,anv] = CL_man_dvSmaT(ai,ei,af,anvman);
deltav_ref = sqrt(dv_res(1)^2 + dv_res(2)^2 + dv_res(3)^2);

TEST_OK(1+$) = CL__isEqual(deltav_ref,deltav_res);

/////////////////////////////////////
// Test 7: Invalid input arguments //
/////////////////////////////////////
// 7200km to 7000km :
ai = 7200.e3; //same result if ai <=0
ei = 0.1; //same result if ei < 0 | ei >= 1 
af = -7000.e3;
anvman =%pi/4;

try
    CL_man_dvSmaT(ai,ei,af,anvman);
    TEST_OK(1+$)=%f;
catch
    TEST_OK(1+$)=%t;
end

//////////////////////////////////////////////////////////////
// Test 8: The results are set to %nan if the targeted semi //
//         major-axis is unreachable                        //
//////////////////////////////////////////////////////////////
// Note: If the targeted semi major-axis is lower than half the initial 
//       semi major-axis, the results are %nan
ai = 7200.e3;
ei = 0.0;
eps = 0.0001;
af = ai/2 -eps;
anvman =%pi/4;

[deltav,dv,anv] = CL_man_dvSmaT(ai,ei,af,anvman);

TEST_OK($+1) = CL__isEqual(deltav,%nan) & CL__isEqual(dv(1),%nan) & ...
                 CL__isEqual(dv(2),%nan) & CL__isEqual(dv(3),%nan)  & ...
                 CL__isEqual(anv,anvman);