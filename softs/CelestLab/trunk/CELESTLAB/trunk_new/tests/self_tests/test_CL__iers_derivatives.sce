// --------------------------------------------
// Validation of the derivatives of low level functions CL__iers_xxx
// Finite differences.
// --------------------------------------------



// Utility function : angular distance between 2 matrix
function ang = delta_rot(M1,M2)
  [ax,ang] = CL_rot_quat2axAng( CL_rot_matrix2quat(M1) * CL_rot_matrix2quat(M2'));
endfunction

// Reminder : 1 micro arcsecondes = 1/3600/1e6*%CL_deg2rad radians = 4.848D-12 radians 
//                                = 0.0000309 m = 30 microns at the surface of Earth
MUAS = 1/3600/1e6*%CL_deg2rad;

TEST_OK = [];


function A = unMod_vect(A)
  for k = 1 : size(A,1)
    A(k,:) = CL_unMod(A(k,:),2*%pi);
  end
endfunction


// ----------------------------------
// TEST : Luni-solar fundamental arguments derivatives
//        (finite difference)
// ----------------------------------
step = 5;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];

[Fls, Flsdot] = CL__iers_fundArgsLS(jd);
Flsdot_ref = Flsdot(:,N/2);
[Fls_interp, Flsdot_interp] = CL_interpLagrange(dt*86400, unMod_vect(Fls), dt(N/2)*86400);

TEST_OK($+1) = CL__isEqual(Flsdot_interp,Flsdot_ref,1e-11);


// ----------------------------------
// TEST : Planetary fundamental arguments derivatives
//        (finite difference)
// ----------------------------------
step = 10;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];

[Fpl, Fpldot] = CL__iers_fundArgsPL(jd);
Fpldot_ref = Fpldot(:,N/2);
[Fpl_interp, Fpldot_interp] = CL_interpLagrange(dt*86400, unMod_vect(Fpl), dt(N/2)*86400);

TEST_OK($+1) = CL__isEqual(Fpldot_interp,Fpldot_ref,1e-11);


// -------------
// TEST : nuta2000AR06 derivatives
// -------------
step = 1e-4;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];
[dpsi,deps, dpsidot, depsdot] = CL__iers_nuta2000AR06(jd);
dpsidot_ref = dpsidot(:,N/2);
depsdot_ref = depsdot(:,N/2);
[psi_interp, psidot_interp] = CL_interpLagrange(dt*86400, dpsi, dt(N/2)*86400);
[eps_interp, epsdot_interp] = CL_interpLagrange(dt*86400, deps, dt(N/2)*86400);
TEST_OK($+1) = CL__isEqual(psidot_interp,dpsidot_ref,1e-8);
TEST_OK($+1) = CL__isEqual(epsdot_interp,depsdot_ref,1e-7);


// -------------
// TEST : nuta1980 derivatives
// -------------
step = 1e-3;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];
[dpsi,deps, dpsidot, depsdot] = CL__iers_nuta1980(jd);
dpsidot_ref = dpsidot(:,N/2);
depsdot_ref = depsdot(:,N/2);
[psi_interp, psidot_interp] = CL_interpLagrange(dt*86400, dpsi, dt(N/2)*86400);
[eps_interp, epsdot_interp] = CL_interpLagrange(dt*86400, deps, dt(N/2)*86400);
TEST_OK($+1) = CL__isEqual(psidot_interp,dpsidot_ref,1e-8);
TEST_OK($+1) = CL__isEqual(epsdot_interp,depsdot_ref,1e-7);


// -------------
// TEST : prec2006 derivatives
// -------------
step = 5;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];
[gamb,phib,psib,epsa, gambdot,phibdot,psibdot,epsadot] = CL__iers_prec2006(jd);
val = [gamb;phib;psib;epsa];
valdot_ref = [gambdot(:,N/2);phibdot(:,N/2);psibdot(:,N/2);epsadot(:,N/2)];
[val_interp, valdot_interp] = CL_interpLagrange(dt*86400, val, dt(N/2)*86400);
TEST_OK($+1) = CL__isEqual(valdot_interp,valdot_ref,1e-8);


// -------------
// TEST : obli1980 derivatives
// -------------
step = 5;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];

[epsa, epsadot] = CL__iers_obli1980(jd);
epsadot_ref = epsadot(:,N/2);
[epsa_interp, epsadot_interp] = CL_interpLagrange(dt*86400, epsa, dt(N/2)*86400);

TEST_OK($+1) = CL__isEqual(epsadot_interp,epsadot_ref,1e-8);

// -------------
// TEST : obli2006 derivatives
// -------------
step = 5;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];

[epsa, epsadot] = CL__iers_obli2006(jd);
epsadot_ref = epsadot(:,N/2);
[epsa_interp, epsadot_interp] = CL_interpLagrange(dt*86400, epsa, dt(N/2)*86400);

TEST_OK($+1) = CL__isEqual(epsadot_interp,epsadot_ref,1e-8);


// -------------
// TEST : xy2006A_ser derivatives
// -------------
step = 1e-2;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];
[x,y, xdot, ydot] = CL__iers_xy2006A_ser(jd);
xdot_ref = xdot(:,N/2);
ydot_ref = ydot(:,N/2);
[x_interp, xdot_interp] = CL_interpLagrange(dt*86400, x, dt(N/2)*86400);
[y_interp, ydot_interp] = CL_interpLagrange(dt*86400, y, dt(N/2)*86400);
TEST_OK($+1) = CL__isEqual(xdot_interp,xdot_ref,1e-7);
TEST_OK($+1) = CL__isEqual(ydot_interp,ydot_ref,1e-7);


// -------------
// TEST : s2006 derivative
// -------------
[s, sdot] = CL__iers_s2006(jd, x ,y, xdot, ydot);
sdot_ref = sdot(:,N/2);
[s_interp, sdot_interp] = CL_interpLagrange(dt*86400, s, dt(N/2)*86400);
TEST_OK($+1) = CL__isEqual(sdot_interp,sdot_ref,1e-7);



// -------------
// TEST : xys2006A_cla derivatives
// -------------
step = 1e-3;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];
[x,y,s xdot,ydot,sdot] = CL__iers_xys2006A_cla(jd);
xdot_ref = xdot(:,N/2);
ydot_ref = ydot(:,N/2);
sdot_ref = sdot(:,N/2);
[x_interp, xdot_interp] = CL_interpLagrange(dt*86400, x, dt(N/2)*86400);
[y_interp, ydot_interp] = CL_interpLagrange(dt*86400, y, dt(N/2)*86400);
[s_interp, sdot_interp] = CL_interpLagrange(dt*86400, s, dt(N/2)*86400);
TEST_OK($+1) = CL__isEqual(xdot_interp,xdot_ref,1e-6);
TEST_OK($+1) = CL__isEqual(ydot_interp,ydot_ref,1e-6);
TEST_OK($+1) = CL__isEqual(sdot_interp,sdot_ref,1e-6);



// ------------
// TEST : era2000 derivative
// -------------
step = 1e-4;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];
[era, eradot] = CL__iers_era2000(jd);
eradot_ref = eradot(:,N/2);
[era_interp, eradot_interp] = CL_interpLagrange(dt*86400, era, dt(N/2)*86400);
TEST_OK($+1) = CL__isEqual(eradot_interp,eradot_ref,1e-10);


// ------------
// TEST : gmst1982 derivative
// -------------
step = 0.01;
dt = -5*step:step:5*step;
N = size(dt,"*");
jd = [2400000.5 * ones(dt) ; ...
      dt ];
[gmst, gmstdot] = CL__iers_gmst1982(jd);
gmstdot_ref = gmstdot(:,N/2);
[gmst_interp, gmstdot_interp] = CL_interpLagrange(dt*86400, gmst, dt(N/2)*86400);
TEST_OK($+1) = CL__isEqual(gmstdot_interp,gmstdot_ref,1e-11);


// -------------
// TEST : Time derivatives of xys_interp 
// -------------
stacksize(5e7);
N = 3000;
jd = rand(2,N)*5000000; // random dates (some dates may be inside 2000-2100)
jd = [];
jd_interp = [ CL_dat_cal2jd(2000,1,1) + rand(1,N)*365*100; ...// random dates inside 2000-2100
              zeros(1,N)];
jd = [jd , jd_interp];
[x,y,s,xdot,ydot,sdot] = CL__iers_xys_interp( jd );
[x2,y2,s2,xdot2,ydot2,sdot2] = CL__iers_xys_block( jd, "classic" );

[M,omega] = CL__iers_xys2Mat(x,y,s,xdot,ydot,sdot);
[M2,omega2] = CL__iers_xys2Mat(x2,y2,s2,xdot2,ydot2,sdot2);

// Test that absolute error is small enough (norm of omega is around 1e-12)
TEST_OK($+1) = max(CL_norm(omega-omega2)) < 1e-14;

