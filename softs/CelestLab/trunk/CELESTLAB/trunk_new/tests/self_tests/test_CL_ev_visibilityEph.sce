// ------------------------------
// CL_ev_visibilityEph : 
// ------------------------------


TEST_OK = [];


// --- Test 1
t0 = 0; 
param0 = [7.e6;0;1;0;0;0]; 
t = t0 : 1/1440 : t0 + 10; 
param = CL_ex_kepler(t0,param0, t); 
pos_ECI = CL_oe_kep2car(param); 
pos_ECF = CL_fr_G502ter(t, pos_ECI); 

stations = [0;0;0]; 
stations_masks = 0; 
sim_period = [t(1), t($)]; 

visi_min = 60; 
prec = 0.01; 

visi_dates = CL_ev_visibilityEph(t, pos_ECF, stations, stations_masks, sim_period, visi_min, prec);
pos_visi_dates = CL_interpLagrange(t, pos_ECF, visi_dates(:)',n=8);
elev = CL_gm_stationElevation(pos_visi_dates,stations);

TEST_OK($+1) = max(abs(elev)) < 1e-6;
