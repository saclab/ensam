//-------------------------------------------------------
// Test the CL_mod_geomagField function
//-------------------------------------------------------

// Additional Informations
// source: 
// http://www.ngdc.noaa.gov/geomag-web/#igrfwmm

// Latitude: 	45� N
// Longitude: 	0� W
// Altitude: 	0.0 km

// column 1 Date  	
// column 2 Declination( + E  | - W ) 	
// column 3 Inclination( + D  | - U ) 	
// column 4 Horizontal Intensity 	
// column 5 North Comp (+ N  | - S) 	
// column 6 East Comp  (+ E  | - W) 	
// column 7 Vertical Comp(+ D  | - U) 	
// column 8 Total Field
//                                                 N               E          D
// 2005-01-01   -1.61� 	60.59� 	22,788.0 nT 	22,779.0 nT   -639.8 nT 	40,425.3 nT 	46,405.8 nT
// 2006-01-01   -1.48� 	60.58� 	22,806.9 nT 	22,799.3 nT   -590.1 nT 	40,443.9 nT 	46,431.3 nT
// 2007-01-01   -1.36� 	60.57� 	22,826.0 nT 	22,819.6 nT   -540.5 nT 	40,462.6 nT 	46,456.9 nT
// 2008-01-01   -1.23� 	60.56� 	22,845.2 nT 	22,839.9 nT   -490.8 nT 	40,481.2 nT 	46,482.6 nT
// 2009-01-01   -1.11� 	60.55� 	22,864.5 nT 	22,860.3 nT   -441.2 nT 	40,499.8 nT 	46,508.3 nT

TEST_OK = []; 

//-------------------------------------------------------
// TEST CASE 1
//-------------------------------------------------------

cjd = CL_dat_cal2cjd([2005; 01; 01]);
// Reference values in topocentric North frame
res = [22779.0; 639.8; -40425.3] * 1e-9 ; // Tesla

// position - geodetic coordinates (lon,lat,alt)
pos_ell = [0*%pi/180; 45*%pi/180; 0];

// magnetic field in topocentric North frame  
b = CL_mod_geomagField(cjd, CL_co_ell2car(pos_ell));
b_topo = CL_fr_topoNMat(pos_ell) * b;   // Tesla

TEST_OK($+1) = CL__isEqual(b_topo, res, 1.e-9, relative = %f);  // precision absolue (tesla)


//-------------------------------------------------------
// TEST CASE 2
//-------------------------------------------------------

cjd = CL_dat_cal2cjd([2006; 01; 01]);

// Reference values in topocentric North frame
res  = [22799.3; 590.1; -40443.9]*1e-9 ; // Tesla

// position - geodetic coordinates (lon,lat,alt)
pos_ell = [0*%pi/180; 45*%pi/180; 0];

// magnetic field in topocentric North frame - 
b = CL_mod_geomagField(cjd, CL_co_ell2car(pos_ell));
b_topo = CL_fr_topoNMat(pos_ell) * b;   // Tesla

TEST_OK($+1) = CL__isEqual(b_topo, res, 1.e-9, relative = %f); // precision absolue (tesla)
