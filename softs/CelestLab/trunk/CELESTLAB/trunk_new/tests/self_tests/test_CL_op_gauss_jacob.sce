// ------------------------------------------
// Consistency test of functions CL_gauss_xxx
// and jacobians from CL_oe_xxx functions
// ------------------------------------------

TEST_OK = [];

a = 7000.e3;
e = 0.01;
inc = 98*%CL_deg2rad;
pom = 0.4;
gom = 0.2;
anm = 0.3;

kep = [a;e;inc;pom;gom;anm];
cir = [a;e*cos(pom);e*sin(pom);inc;gom;pom+anm];
cireq = [a;e*cos(pom+gom);e*sin(pom+gom);sin(inc/2)*cos(gom);sin(inc/2)*sin(gom);pom+gom+anm];
equin = [a;e*cos(pom+gom);e*sin(pom+gom);tan(inc/2)*cos(gom);tan(inc/2)*sin(gom);pom+gom+anm];

acc = [1 ; 2 ; 3];

v = CL_kp_M2v(e,anm);
n = sqrt(%CL_mu/a^3);

frames = ["qsw", "tnw"];

// --------------------
// TEST 1/2 :
// Coherence of CIR/KEP
// --------------------
for k = 1 : 2
  [M,N] = CL_op_gauss("cir",cir,frames(k));
  dcirdt_ref = M*acc;

  [M2,N2] = CL_op_gauss("kep",kep,frames(k));
  dkepdt_ref = M2*acc;

  // J = dkep/dcir
  [kep2,J] = CL_oe_cir2kep(cir);
  dkepdt = J * dcirdt_ref;

  // J = dcir/dkep
  [cir2,J] = CL_oe_kep2cir(kep);
  dcirdt = J * dkepdt_ref;

  TEST_OK($+1) = CL__isEqual(dkepdt, dkepdt_ref) & ...
                 CL__isEqual(dcirdt, dcirdt_ref);
end

// --------------------
// TEST 3/4 :
// Coherence of CIREQ/KEP
// --------------------
for k = 1 : 2
  [M,N] = CL_op_gauss("cireq",cireq,frames(k));
  dcireqdt_ref = M*acc;

  [M2,N2] = CL_op_gauss("kep",kep,frames(k));
  dkepdt_ref = M2*acc;

  // J = dkep/dcireq
  [kep2,J] = CL__oe_cireq2kep(cireq);
  dkepdt = J * dcireqdt_ref;

  // J = dcireq/dkep
  [cireq2,J] = CL__oe_kep2cireq(kep);
  dcireqdt = J * dkepdt_ref;

  TEST_OK($+1) = CL__isEqual(dkepdt, dkepdt_ref) & ...
                 CL__isEqual(dcireqdt, dcireqdt_ref);
end


// --------------------
// TEST 5/6 :
// Coherence of EQUIN/KEP
// --------------------
for k = 1 : 2
  [M,N] = CL_op_gauss("equin",equin,frames(k));
  dequindt_ref = M*acc;

  [M2,N2] = CL_op_gauss("kep",kep,frames(k));
  dkepdt_ref = M2*acc;

  // J = dkep/dequin
  [kep2,J] = CL__oe_equin2kep(equin);
  dkepdt = J * dequindt_ref;

  // J = dequin/dkep
  [equin2,J] = CL__oe_kep2equin(kep);
  dequindt = J * dkepdt_ref;

  TEST_OK($+1) = CL__isEqual(dkepdt, dkepdt_ref) & ...
                 CL__isEqual(dequindt, dequindt_ref);
end
