// ------------------------------
// CL_man_dvBiElliptic: 
// ------------------------------

TEST_OK = [];

// Test 1 : Nominal case.
ai = 7000.e3;
af = 98000.e3;
rt = 280000.e3;
[deltav,dv1,dv2,dv3,anv1,anv2,anv3] = CL_man_dvBiElliptic(ai,af,rt);

kep = [ai ; 0 ; %pi/2 ; 0 ; 0 ; anv1];
kep1 = CL_man_applyDvKep(kep,dv1);
kep1(6) = anv2;
kep2 = CL_man_applyDvKep(kep1,dv2);
kep2(6) = anv3;
kep3 = CL_man_applyDvKep(kep2,dv3);

TEST_OK($+1) = CL__isEqual(kep3(1), af);

// Test 2 : results in a structure. 
man = CL_man_dvBiElliptic(ai,af,rt,res="s");
TEST_OK($+1) = CL__isEqual(man.dv1, dv1);

// Test 3 : ai = af = rt. 
// --> Everything is zero
ai = 98000.e3;
af = 98000.e3;
rt = 98000.e3;
man_res = CL_man_dvBiElliptic(ai,af,rt,res="s");

man_ref = struct();
man_ref.deltav = 0.
man_ref.dv1 = [0. ; 0.; 0.]
man_ref.dv2 = [0. ; 0.; 0.]
man_ref.dv3 = [0. ; 0.; 0.]
man_ref.anv1 = 0.
man_ref.anv2 = 0.
man_ref.anv3 = 0.

TEST_OK($+1) = CL__isEqual(man_ref, man_res);

// Test 4 : incorrect number of input parameters.
try 
  [deltav,dv1] = CL_man_dvBiElliptic(ai,af,rt,res="s");
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 5 : incorrect value of "res" input parameter.
try 
  man = CL_man_dvBiElliptic(ai,af,rt,res="t");
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Test 6 : incorrect value of "ai" input parameter.
ai = 0;
try 
  man = CL_man_dvBiElliptic(ai,af,rt,res="s");
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  






