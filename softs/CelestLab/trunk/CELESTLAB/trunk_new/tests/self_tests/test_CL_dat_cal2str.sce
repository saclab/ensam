// ------------------------------
// CL_dat_cal2str: 
// ------------------------------

TEST_OK = [];

// Cas 1: Nominal case.
year = 2015;
month = 9;
day = 23;
hour = 21;
mn = 51;
seconds = 13;

str = CL_dat_cal2str([year;month;day;hour;mn;seconds]);

cal = CL_dat_str2cal(str);

TEST_OK($+1) = CL__isEqual( year , cal(1));
TEST_OK($+1) = CL__isEqual( month , cal(2));
TEST_OK($+1) = CL__isEqual( day , cal(3));
TEST_OK($+1) = CL__isEqual( hour , cal(4));
TEST_OK($+1) = CL__isEqual( mn , cal(5));
TEST_OK($+1) = CL__isEqual( seconds , cal(6));

// Cas 2: Incorrect dimension for cal.
cal1 = [2012;1;1;12];
try 
  str = CL_dat_cal2str(cal1);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  

// Cas 3: Cal dimension == 3.
cal2 = [2012;1;1];
str = CL_dat_cal2str(cal2);
TEST_OK($+1) = (str == "2012/01/01 00:00:00.000");

// Cas 4: Cal == [].
cal3 = [];
str = CL_dat_cal2str(cal3);
TEST_OK($+1) = (str == []);

// Cas 5: Cal type not "constant".
cal4 = struct();
try 
  str = CL_dat_cal2str(cal4);
  TEST_OK($+1) = %f;
catch
  TEST_OK($+1) = %t;
end  





