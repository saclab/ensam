//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Enforced linear motion in "qsw" local frame. 
//> -> Computes the additional acceleration (i.e. continuous thrust) 
//> necessary for the relative trajectory of satellite2 to be a straight 
//> line in the qsw local frame of satellite1. 
//> NB: 
//> - Keplerian model => central force only. 
//> - The velocity can be 0, in which case satellite2 is stationary (in 
//>   Satellite1's local frame). 
//
// Author: A. Lamy
// -----------------------------------------------------------

kepref0 = [7000.e3; 0.001; 0; 0; 0; 0]; 
pr0_qsw = [-1.e3; 0; 0]'; // pos in qsw
vr0_qsw = [0; 0; 0]'; // vel in qsw
norb = 1; 
iplot = 1; 

desc_param = list(..
   CL_defParam("sat1: Semi-major axis",kepref0(1), id = '$sma', units = ["m", "km"], valid='$x > 0'),..
   CL_defParam("sat1: Eccentricity", kepref0(2), valid='$x >= 0 & $x < 1'),..
   CL_defParam("sat1: Mean anomaly", kepref0(6), units = ["rad", "deg"]),..
   CL_defParam("sat2/sat1: Initial position vector relative to qsw", pr0_qsw, units = ["m", "km"], dim=3),..
   CL_defParam("sat2/sat1: Velocity vector relative to qsw", vr0_qsw, units = ["m/s"], dim=3),..
   CL_defParam("Number of orbits for propagation", norb, valid='$x>0'),..
   CL_defParam("Plot: 1=components of acceleration, 2=norm", iplot, accv=1:2)..
); 

[kepref0(1),kepref0(2),kepref0(6),pr0_qsw,vr0_qsw,norb,iplot] = CL_inputParam(desc_param);
pr0_qsw = pr0_qsw'; 
vr0_qsw = vr0_qsw'; 


// -------------------------------------------
// utility functions 
// -------------------------------------------

// -------------------------------------------
// Compute supplied acceleration - components in qsw
// kepref0: keplerian parameters of reference satellite (satellite 1)
// pr0_qsw,  vr0_qsw: initial position and velocity relative to qsw
// t: days
// -------------------------------------------
function [dacc, anvref] = compute_acc(t0, kepref0, pr0_qsw, vr0_qsw, t)

  // propagation of reference satellite
  kepref = CL_ex_kepler(t0, kepref0, t); 
  [pref, vref] = CL_oe_kep2car(kepref); 

  // propagation of relative trajectory 
  pr_qsw = pr0_qsw * ones(t) + CL_dMult((t-t0)*86400, vr0_qsw); 
  vr_qsw = vr0_qsw * ones(t); 
  ar_qsw = zeros(pr_qsw); 
 
  Mqsw = CL_fr_qswMat(pref, vref); 
  pr = Mqsw' * pr_qsw; // coord in inertial frame
  vr = Mqsw' * vr_qsw; 
  ar = Mqsw' * ar_qsw; 

  // omega = angular velocity vector of qsw local frame
  omega = CL_dMult(1 ./ CL_dot(pref), CL_cross(pref, vref));
  domegadt = -2 * CL_dMult(CL_dot(pref,vref) ./ CL_dot(pref), omega); 
  
  ve = CL_cross(omega, pr); 
  p = pref + pr; // position of satellite 2

  // inertial accelerations: 
  aref = CL_dMult(-%CL_mu ./ CL_norm(pref).^3, pref); 
  a = CL_dMult(-%CL_mu ./ CL_norm(p).^3, p); 

  // "natural" relative acceleration
  ar_nat = a - aref - CL_cross(omega,ve) - CL_cross(domegadt,pr) - 2 * CL_cross(omega, vr);

  // difference of acceleration => coordinates in qsw
  dacc = Mqsw * (ar - ar_nat); 

  // true anomaly of satellite 1
  anvref = CL_kp_M2v(kepref(2,:), kepref(6,:)); 

endfunction


// -------------------------------------------
// plot acceleration components
// anv: abscissa = true anomaly (rad)
// dacc; acceleration to supply (3xN) (m/s2)
// -------------------------------------------
function plot_acc_comp(anv, dacc)
  f = scf(); 
  f.axes_size = [600, 500]; 
  f.visible = "on"; 
  f.immediate_drawing = "off"; 

  tab_y = [dacc(3,:); dacc(2,:); dacc(1,:)] * 1.e3; // mm/s2
  tab_titles = ["Thrust acceleration / qsw: Z (mm/s2)"; ..
                "Thrust acceleration / qsw: Y (mm/s2)"; ..
                "Thrust acceleration / qsw: X (mm/s2)"]; 
  
  for k = 1 : 3
    subplot(3,1,k); 
    x = anv*%CL_rad2deg; 
    y = tab_y(k,:); 
    plot(x, y, "b", "thickness", 2); 
    
    a = gca();
    a.margins = [0.15,0.1,0.18,0.3];
    
    a.title.text = tab_titles(k); 
    if (k == 3); a.x_label.text = "True anomaly (deg)"; end
    
    // tight limits for x-axis only. 
    db = a.data_bounds; 
    db(:,2) = [min(y) - (max(y)-min(y)) * 0.1 - 1.e-6; max(y) + (max(y)-min(y)) * 0.1 + 1.e-6]; 
    a.data_bounds = db; 
    a.tight_limits(1) = "on"; 

    CL_g_stdaxes(a, fg=1, ft=2, fl=1);
  end

  f.immediate_drawing = "on"; 
  f.visible = "on"; 

endfunction

// -------------------------------------------
// plot acceleration norm
// anv: abscissa = true anomaly (rad)
// dacc; acceleration to supply (3xN) (m/s2)
// -------------------------------------------
function plot_acc_norm(anv, dacc)
  f = scf(); 
  f.visible = "on"; 
  f.immediate_drawing = "off"; 

  x = anv*%CL_rad2deg; 
  y = [CL_norm(dacc)] * 1.e3; // mm/s2 
  plot(x, y, "b", "thickness", 2); 
    
  a = gca(); 
  a.title.text = "Thrust acceleration: norm (mm/s2)"; 
  a.x_label.text = "True anomaly (deg)"; 
    
  // tight limits for x-axis only. 
  a.tight_limits(1) = "on"; 
  CL_g_stdaxes(a);

  f.immediate_drawing = "on"; 
  f.visible = "on"; 

endfunction


// ===========================================
// MAIN: Computation / plot 
// ===========================================

T = CL_kp_params(['per'], kepref0(1)); // seconds 
N = 200; // points per orbit  

dt = linspace(0, norb*T, N*norb+1); ; 
t0 = 0; 
t = t0 + dt / 86400; 

[dacc, anvref] = compute_acc(t0, kepref0, pr0_qsw, vr0_qsw, t); 

if (iplot == 1)
  plot_acc_comp(anvref, dacc); 
else
  plot_acc_norm(anvref, dacc); 
end




