//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> List object descriptions in TLE source 
//> 
//> The TLE are read from a file specified by a path or an URL. By default (empty name), 
//> "tle_examples.txt" in CelestLab data directory is used. 
//> 
//> For up-to-date data, go for instance to http://www.celestrak.com/NORAD/elements. 
//
// Author: A. Lamy
// -----------------------------------------------------------
// Note: CelestLabX needed (no TLE propagation)

fname=""; 

// validate the file name (does not check URL is valid)
function [ok] = validFname(fname)
  ok = ( stripblanks(fname,%t) == ""  | ..
         part(fname, 1:5) == "http:" | ..
         isfile(fname) ); 
endfunction


desc_param = list(..
  CL_defParam("TLE file path or URL (empty = default file)", fname, typ="s", valid='validFname($x)')..
);

[fname] = CL_inputParam(desc_param);


// -----------------------------
// Computation 
// -----------------------------
// Internal function: 
// Load a TLE file
// may return one or more TLEs
function [tle] = loadTLE(fname)
  // default output
  tle = CL_tle_new(0); 
  
  if (stripblanks(fname,%t) == "")
    fpath = CL_path("tle_examples.txt", fullfile(CL_home(), "data", "misc"));
  elseif(part(fname, 1:5) == "http:")
    fpath = getURL(fname, fullfile(TMPDIR, "CL_demos_TLE.txt")); 
  elseif (isfile(fname))
    fpath = fname; 
  else
    fpath = []; 
  end

  if (fpath == [])
    error("Invalid TLE file path or URL"); 
  end
  tle = CL_tle_load(fpath);
endfunction

// Initialize TLE
tle = loadTLE(fname); 

// show info (description)
if (size(tle.desc, "*") == [])
  error("No objects selected"); 
end

// show list of objects
messagebox(tle.desc', "TLE - object descriptions", "info", "OK", "modal"); 
