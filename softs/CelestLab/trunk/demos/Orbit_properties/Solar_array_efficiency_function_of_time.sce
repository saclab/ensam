//  Copyright (c) CNES  2008
//
//  This software is part of CelestLab, a CNES toolbox for Scilab
//
//  This software is governed by the CeCILL  license under French law and
//  abiding by the rules of distribution of free software.  You can  use,
//  modify and/ or redistribute the software under the terms of the CeCILL
//  license as circulated by CEA, CNRS and INRIA at the following URL
//  'http://www.cecill.info'.

// -----------------------------------------------------------
//> Mean solar array efficiency over time: 
//> It is defined as the mean value of f*max(cos(ang),0) over one orbit,
//> where ang is the angle between the normal of the solar array (SA) 
//> and the direction of the Sun. f is a factor taking into account 
//> the actual distance (d) to the Sun: f = (au/d)^2.  
//> The efficiency may be converted to received power (by area unit) by 
//> multiplying it by TSI (total solar irradiance).  
//> 
//> The tilt angle is the angle between the orbit plane and the SA
//> normal, positive if the angle between the SA normal and
//> the orbit's angular momentum is less than 90 degrees.  
//> 
//> - Fixed solar array: 
//> The orientation of the SA normal is defined by the tilt angle and
//> by the rotation angle (alpha) around w (origin = q axis). Alpha and
//> tilt are the spherical coordinates of the SA normal in qsw. 
//> 
//> - Rotating solar array: 
//> The solar array is rotating around the normal of the orbit plane
//> and is oriented towards the Sun as much as possible. 
//> 
//> NB: 
//> - The orbit is circular. 
//> - Eclipses are taken into account (spherical Earth). 
//> - The Sun is supposed fixed / orbit plane over one orbit. 
//
// Author: A. Lamy
// -----------------------------------------------------------

alt = 700.e3;  // altitude (m)
sso = 1; 
inc = 0 * %CL_deg2rad; 
hloc0 = 14; // initial mean local time of AN (h)
sa_type = 2; 
sa_az = 0 * %CL_deg2rad; 
sa_tilts = (10:5:35) * %CL_deg2rad; 
T = CL_unitConvert(1,"yr","day"); // simu duration (days)
optd = 1; 
yaw_flip = 0; 
optres = 1; 

desc_param = list(..
  CL_defParam("Altitude (sma minus equ. radius)", alt, units=['m','km'], valid='$x>=0'),..
  CL_defParam("Sun-synchronous orbit? (1=yes, 0=no)", sso, id='$sso', accv=[0,1]),..
  CL_defParam("Inclination", inc, units=['rad','deg'], id='$inc', valid='$sso == 1 | ($inc>0 & $inc < 180)'),..
  CL_defParam("Initial mean local time of asc. node", hloc0, units=['h'], valid='$x>=0 & $x<=24'),..
  CL_defParam("Solar array type: 1=fixed 2=rotating", sa_type, accv=[1,2]),..
  CL_defParam("SA rotation angle from q, around w (if fixed)", sa_az, units=['rad','deg']),..
  CL_defParam("Values of solar array tilt", sa_tilts, units=['rad','deg'], valid='$x>=-90 & $x<=90', dim=[1, 20]),..
  CL_defParam("Simulation duration", T, units=['day','yr'], valid='$x>=1 & $x<=10'),..
  CL_defParam("Include Earth-Sun distance variations (1=yes, 0=no)", optd, accv=[0,1]),..
  CL_defParam("Include yaw flips (1=yes, 0=no)", yaw_flip, accv=[0,1]),..
  CL_defParam("Plot: 1=efficiency 2=power", optres, accv=[1,2])..
);

[alt, sso, inc, hloc0, sa_type, sa_az, sa_tilts, T, optd, yaw_flip, optres] = CL_inputParam(desc_param);

// -----------------------------------------------------------
// Computation
// -----------------------------------------------------------
function [eff]= efficiency_rotating(betaa, tilt, alpha_ecl)
  // Mean value of max(cos(ang), 0)
  // cos(ang) = cos(beta - tilt) 
  eff = max(cos(betaa - tilt), 0) .* (alpha_ecl/%pi); 
endfunction

function [eff]= efficiency_fixed(betaa, az, tilt, alpha_ecl)
  // Mean value of max(cos(ang), 0)
  // cos(ang) = cos(beta)*cos(tilt)*cos(alpha+az) + sin(beta)*sin(tilt)

  // alpha1, alpha2: such that cos(ang) has same sign in [alpha1, alpha2]
  alpha1 = -az - %pi * ones(betaa); 
  alpha2 = -az + %pi * ones(betaa); 
  
  cond_sign = abs(sin(betaa) * sin(tilt)) < abs(cos(betaa) * cos(tilt)); 
  I = find(cond_sign); 
  if (I <> [])
    x = acos(-tan(betaa(I)) * tan(tilt)); 
    alpha1(I) = -az - x; 
    alpha2(I) = -az + x; 
  end
  
  // check value at middle of interval
  // if value is negative and sign changes => take complementary interval
  // if value is negative and sign does not change => 0 length interval

  I = find(cos(betaa-tilt) < 0 & cond_sign); 
  // does not happen if beta and tilt are in [-%pi/2, %pi/2]
  if (I <> [])
    [alpha1(I), alpha2(I)] = (alpha2(I), alpha1(I)+2*%pi); 
  end
  I = find(cos(betaa-tilt) < 0 & ~cond_sign); 
  if (I <> [])
    alpha1(I) = -az; 
    alpha2(I) = -az; 
  end

  // alpha1_ecl, alpha2_ecl: in shadow and inside [alpha1, alpha2]
  alpha_mid = (alpha1 + alpha2) / 2; 
  alpha0_ecl = CL_rMod(%pi, alpha_mid-%pi, alpha_mid+%pi);
  alpha1_ecl = alpha0_ecl - (%pi - alpha_ecl); 
  alpha2_ecl = alpha0_ecl + (%pi - alpha_ecl); 
  alpha1_ecl = max(alpha1, min(alpha2, alpha1_ecl)); 
  alpha2_ecl = max(alpha1, min(alpha2, alpha2_ecl)); 
  
  eff = (cos(betaa)*cos(tilt).*(sin(alpha2+az) - sin(alpha1+az) - sin(alpha2_ecl+az) + sin(alpha1_ecl+az)) + ..
         sin(betaa)*sin(tilt).*(alpha2 - alpha1 - alpha2_ecl + alpha1_ecl)) / (2*%pi); 

endfunction


// -----------------------------------------------------------
sma = alt + %CL_eqRad; 
ecc = 0; 
if (sso == 1); inc = CL_op_ssoJ2('i', sma, ecc); end

// W/m^2 at 1 au
TSI = CL_dataGet("totalSolarIrradiance"); 

// initial date 
cal = CL_dat_now("cal"); 
cjd0 = CL_dat_cal2cjd(cal(1),1,1,0,0,0);   

t = 0:T; // every day from January 1st

// compute Sun position starting at cjd0 (in ECI)
pos_sun = CL_eph_sun(cjd0 + t);  
pos_sun_sph = CL_co_car2sph(pos_sun); 

// effect of J2 on orbit 
[dpomdt, dgomdt, dmadt] = CL_op_driftJ2(sma, ecc, inc);  

gom0 = CL_op_locTime(cjd0, 'mlh', hloc0, 'ra');  // initial right ascension of RAAN
gom = gom0 + dgomdt * t * 86400; // raan over time
betaa = CL_gm_raan2beta(inc, gom, pos_sun_sph(1,:), pos_sun_sph(2,:));
alpha_ecl = %pi - CL_gm_betaEclipse(sma, betaa); 

// effect of distance / 1 AU
fd = 1; 
if (optd == 1); fd = (%CL_au ./ pos_sun_sph(3,:)).^2; end


result = [];  

for k = 1:length(sa_tilts)
  tiltk = sa_tilts(k); 
  betaak = betaa; 

  if (yaw_flip)
    tiltk = abs(tiltk); 
    betaak = abs(betaak); 
  end    

  if (sa_type == 1)
    eff = efficiency_fixed(betaak, sa_az, tiltk, alpha_ecl); 
  else
    eff = efficiency_rotating(betaak, tiltk, alpha_ecl); 
  end

  if (optres == 1)
    res = 100 * fd .* eff; 
  else
    res = TSI * fd .* eff; 
  end
  
  result = [result; res]; 
end


// -----------------------------------------------------------
// Plot
// -----------------------------------------------------------

f = scf(); //create figure
f.visible = "on";
f.immediate_drawing="off";

nb = length(sa_tilts);
f.color_map = jetcolormap(min(max(nb,5),100));

a = gca();

for k = 1:length(sa_tilts)
  plot(t, result(k,:));
  h = CL_g_select(gce(), "Polyline");
  h.thickness = 2;
  h.foreground = k; 
end

if (sa_type == 1); str = "fixed"; else; str = "rotating"; end
a.title.text = msprintf("Mean (%s) solar array efficiency", str);

if (optres == 1)
  a.y_label.text = "Efficiency (%)";
else
  a.y_label.text = "Received power / unit of area (W/m^2)";
end

a.x_label.text = msprintf("Days from January 1st (%d)", cal(1));

a.data_bounds = [t(1), a.y_ticks.locations(1); 
                 t($), a.y_ticks.locations($)]; 

CL_g_legend(a, header = "SA tilt (deg)", str=string(sa_tilts * %CL_rad2deg));

h=CL_g_select(a, "Polyline");
h.thickness = 2;

a.tight_limits = "on";
CL_g_stdaxes(a);

f.immediate_drawing="on";
f.visible = "on";


